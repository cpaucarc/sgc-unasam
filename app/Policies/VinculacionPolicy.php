<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Vinculacion;
use Illuminate\Auth\Access\HandlesAuthorization;

class VinculacionPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de vinculacion.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Vinculacion $vinculacion
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('vinculacions.all') || $user->can('vinculacions.index');
    }

    /**
     * Determina que usuarios puede ver vinculacion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Vinculacion $vinculacion
     * @return mixed
     */
    public function view(User $user, Vinculacion $vinculacion)
    {
        return $user->can('vinculacions.show') || $user->can('vinculacions.all');
    }

    /**
     * Determina que usuarios puede crear vinculacion.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('vinculacions.create') || $user->can('vinculacions.all');
    }

    /**
     * Determina si el usuario puede modificar vinculacion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Vinculacion $vinculacion
     * @return mixed
     */
    public function update(User $user, Vinculacion $vinculacion)
    {
        return $user->can('vinculacions.update') || $user->can('vinculacions.all');
    }

    /**
     * Determina si el usuario puede eliminar vinculacion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Vinculacion $vinculacion
     * @return mixed
     */
    public function delete(User $user, Vinculacion $vinculacion)
    {
        return $user->can('vinculacions.delete') || $user->can('vinculacions.all');
    }

    /**
     * Determina si el usuario puede restaurar vinculacion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Vinculacion $vinculacion
     * @return mixed
     */
    public function restore(User $user, Vinculacion $vinculacion)
    {
        return $user->can('vinculacions.restore') || $user->can('vinculacions.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente vinculacion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Vinculacion $vinculacion
     * @return mixed
     */
    public function forceDelete(User $user, Vinculacion $vinculacion)
    {
        return $user->can('vinculacions.destroy') || $user->can('vinculacions.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
