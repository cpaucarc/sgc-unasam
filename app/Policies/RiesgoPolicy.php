<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Riesgo;
use Illuminate\Auth\Access\HandlesAuthorization;

class RiesgoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de riesgo.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Riesgo $riesgo
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('riesgos.all') || $user->can('riesgos.index');
    }

    /**
     * Determina que usuarios puede ver riesgo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Riesgo $riesgo
     * @return mixed
     */
    public function view(User $user, Riesgo $riesgo)
    {
        return $user->can('riesgos.show') || $user->can('riesgos.all');
    }

    /**
     * Determina que usuarios puede crear riesgo.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('riesgos.create') || $user->can('riesgos.all');
    }

    /**
     * Determina si el usuario puede modificar riesgo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Riesgo $riesgo
     * @return mixed
     */
    public function update(User $user, Riesgo $riesgo)
    {
        return $user->can('riesgos.update') || $user->can('riesgos.all');
    }

    /**
     * Determina si el usuario puede eliminar riesgo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Riesgo $riesgo
     * @return mixed
     */
    public function delete(User $user, Riesgo $riesgo)
    {
        return $user->can('riesgos.delete') || $user->can('riesgos.all');
    }

    /**
     * Determina si el usuario puede restaurar riesgo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Riesgo $riesgo
     * @return mixed
     */
    public function restore(User $user, Riesgo $riesgo)
    {
        return $user->can('riesgos.restore') || $user->can('riesgos.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente riesgo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Riesgo $riesgo
     * @return mixed
     */
    public function forceDelete(User $user, Riesgo $riesgo)
    {
        return $user->can('riesgos.destroy') || $user->can('riesgos.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
