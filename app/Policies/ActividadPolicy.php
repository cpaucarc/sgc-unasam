<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Actividad;
use Illuminate\Auth\Access\HandlesAuthorization;

class ActividadPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de actividad.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Actividad $actividad
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('actividads.all') || $user->can('actividads.index');
    }

    /**
     * Determina que usuarios puede ver actividad.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Actividad $actividad
     * @return mixed
     */
    public function view(User $user, Actividad $actividad)
    {
        return $user->can('actividads.show') || $user->can('actividads.all');
    }

    /**
     * Determina que usuarios puede crear actividad.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('actividads.create') || $user->can('actividads.all');
    }

    /**
     * Determina si el usuario puede modificar actividad.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Actividad $actividad
     * @return mixed
     */
    public function update(User $user, Actividad $actividad)
    {
        return $user->can('actividads.update') || $user->can('actividads.all');
    }

    /**
     * Determina si el usuario puede eliminar actividad.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Actividad $actividad
     * @return mixed
     */
    public function delete(User $user, Actividad $actividad)
    {
        return $user->can('actividads.delete') || $user->can('actividads.all');
    }

    /**
     * Determina si el usuario puede restaurar actividad.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Actividad $actividad
     * @return mixed
     */
    public function restore(User $user, Actividad $actividad)
    {
        return $user->can('actividads.restore') || $user->can('actividads.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente actividad.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Actividad $actividad
     * @return mixed
     */
    public function forceDelete(User $user, Actividad $actividad)
    {
        return $user->can('actividads.destroy') || $user->can('actividads.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
