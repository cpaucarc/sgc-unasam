<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Variable;
use Illuminate\Auth\Access\HandlesAuthorization;

class VariablePolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de variable.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Variable $variable
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('variables.all') || $user->can('variables.index');
    }

    /**
     * Determina que usuarios puede ver variable.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Variable $variable
     * @return mixed
     */
    public function view(User $user, Variable $variable)
    {
        return $user->can('variables.show') || $user->can('variables.all');
    }

    /**
     * Determina que usuarios puede crear variable.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('variables.create') || $user->can('variables.all');
    }

    /**
     * Determina si el usuario puede modificar variable.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Variable $variable
     * @return mixed
     */
    public function update(User $user, Variable $variable)
    {
        return $user->can('variables.update') || $user->can('variables.all');
    }

    /**
     * Determina si el usuario puede eliminar variable.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Variable $variable
     * @return mixed
     */
    public function delete(User $user, Variable $variable)
    {
        return $user->can('variables.delete') || $user->can('variables.all');
    }

    /**
     * Determina si el usuario puede restaurar variable.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Variable $variable
     * @return mixed
     */
    public function restore(User $user, Variable $variable)
    {
        return $user->can('variables.restore') || $user->can('variables.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente variable.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Variable $variable
     * @return mixed
     */
    public function forceDelete(User $user, Variable $variable)
    {
        return $user->can('variables.destroy') || $user->can('variables.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
