<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Medicion;
use Illuminate\Auth\Access\HandlesAuthorization;

class MedicionPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de medicion.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Medicion $medicion
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('medicions.all') || $user->can('medicions.index');
    }

    /**
     * Determina que usuarios puede ver medicion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Medicion $medicion
     * @return mixed
     */
    public function view(User $user, Medicion $medicion)
    {
        return $user->can('medicions.show') || $user->can('medicions.all');
    }

    /**
     * Determina que usuarios puede crear medicion.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('medicions.create') || $user->can('medicions.all');
    }

    /**
     * Determina si el usuario puede modificar medicion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Medicion $medicion
     * @return mixed
     */
    public function update(User $user, Medicion $medicion)
    {
        return $user->can('medicions.update') || $user->can('medicions.all');
    }

    /**
     * Determina si el usuario puede eliminar medicion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Medicion $medicion
     * @return mixed
     */
    public function delete(User $user, Medicion $medicion)
    {
        return $user->can('medicions.delete') || $user->can('medicions.all');
    }

    /**
     * Determina si el usuario puede restaurar medicion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Medicion $medicion
     * @return mixed
     */
    public function restore(User $user, Medicion $medicion)
    {
        return $user->can('medicions.restore') || $user->can('medicions.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente medicion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Medicion $medicion
     * @return mixed
     */
    public function forceDelete(User $user, Medicion $medicion)
    {
        return $user->can('medicions.destroy') || $user->can('medicions.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
