<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Suministrainfo;
use Illuminate\Auth\Access\HandlesAuthorization;

class SuministrainfoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de suministrainfo.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Suministrainfo $suministrainfo
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('suministrainfos.all') || $user->can('suministrainfos.index');
    }

    /**
     * Determina que usuarios puede ver suministrainfo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Suministrainfo $suministrainfo
     * @return mixed
     */
    public function view(User $user, Suministrainfo $suministrainfo)
    {
        return $user->can('suministrainfos.show') || $user->can('suministrainfos.all');
    }

    /**
     * Determina que usuarios puede crear suministrainfo.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('suministrainfos.create') || $user->can('suministrainfos.all');
    }

    /**
     * Determina si el usuario puede modificar suministrainfo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Suministrainfo $suministrainfo
     * @return mixed
     */
    public function update(User $user, Suministrainfo $suministrainfo)
    {
        return $user->can('suministrainfos.update') || $user->can('suministrainfos.all');
    }

    /**
     * Determina si el usuario puede eliminar suministrainfo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Suministrainfo $suministrainfo
     * @return mixed
     */
    public function delete(User $user, Suministrainfo $suministrainfo)
    {
        return $user->can('suministrainfos.delete') || $user->can('suministrainfos.all');
    }

    /**
     * Determina si el usuario puede restaurar suministrainfo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Suministrainfo $suministrainfo
     * @return mixed
     */
    public function restore(User $user, Suministrainfo $suministrainfo)
    {
        return $user->can('suministrainfos.restore') || $user->can('suministrainfos.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente suministrainfo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Suministrainfo $suministrainfo
     * @return mixed
     */
    public function forceDelete(User $user, Suministrainfo $suministrainfo)
    {
        return $user->can('suministrainfos.destroy') || $user->can('suministrainfos.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
