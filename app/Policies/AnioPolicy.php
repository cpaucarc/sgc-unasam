<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Anio;
use Illuminate\Auth\Access\HandlesAuthorization;

class AnioPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de anio.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Anio $anio
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('anios.all') || $user->can('anios.index');
    }

    /**
     * Determina que usuarios puede ver anio.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Anio $anio
     * @return mixed
     */
    public function view(User $user, Anio $anio)
    {
        return $user->can('anios.show') || $user->can('anios.all');
    }

    /**
     * Determina que usuarios puede crear anio.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('anios.create') || $user->can('anios.all');
    }

    /**
     * Determina si el usuario puede modificar anio.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Anio $anio
     * @return mixed
     */
    public function update(User $user, Anio $anio)
    {
        return $user->can('anios.update') || $user->can('anios.all');
    }

    /**
     * Determina si el usuario puede eliminar anio.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Anio $anio
     * @return mixed
     */
    public function delete(User $user, Anio $anio)
    {
        return $user->can('anios.delete') || $user->can('anios.all');
    }

    /**
     * Determina si el usuario puede restaurar anio.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Anio $anio
     * @return mixed
     */
    public function restore(User $user, Anio $anio)
    {
        return $user->can('anios.restore') || $user->can('anios.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente anio.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Anio $anio
     * @return mixed
     */
    public function forceDelete(User $user, Anio $anio)
    {
        return $user->can('anios.destroy') || $user->can('anios.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
