<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de role.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Role $role
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('roles.all') || $user->can('roles.index');
    }

    /**
     * Determina que usuarios puede ver role.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Role $role
     * @return mixed
     */
    public function view(User $user, Role $role)
    {
        return $user->can('roles.show') || $user->can('roles.all');
    }

    /**
     * Determina que usuarios puede crear role.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('roles.create') || $user->can('roles.all');
    }

    /**
     * Determina si el usuario puede modificar role.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Role $role
     * @return mixed
     */
    public function update(User $user, Role $role)
    {
        return $user->can('roles.update') || $user->can('roles.all');
    }

    /**
     * Determina si el usuario puede eliminar role.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Role $role
     * @return mixed
     */
    public function delete(User $user, Role $role)
    {
        return $user->can('roles.delete') || $user->can('roles.all');
    }

    /**
     * Determina si el usuario puede restaurar role.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Role $role
     * @return mixed
     */
    public function restore(User $user, Role $role)
    {
        return $user->can('roles.restore') || $user->can('roles.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente role.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Role $role
     * @return mixed
     */
    public function forceDelete(User $user, Role $role)
    {
        return $user->can('roles.destroy') || $user->can('roles.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
