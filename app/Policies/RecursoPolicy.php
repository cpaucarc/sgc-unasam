<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Recurso;
use Illuminate\Auth\Access\HandlesAuthorization;

class RecursoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de recurso.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Recurso $recurso
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('recursos.all') || $user->can('recursos.index');
    }

    /**
     * Determina que usuarios puede ver recurso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Recurso $recurso
     * @return mixed
     */
    public function view(User $user, Recurso $recurso)
    {
        return $user->can('recursos.show') || $user->can('recursos.all');
    }

    /**
     * Determina que usuarios puede crear recurso.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('recursos.create') || $user->can('recursos.all');
    }

    /**
     * Determina si el usuario puede modificar recurso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Recurso $recurso
     * @return mixed
     */
    public function update(User $user, Recurso $recurso)
    {
        return $user->can('recursos.update') || $user->can('recursos.all');
    }

    /**
     * Determina si el usuario puede eliminar recurso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Recurso $recurso
     * @return mixed
     */
    public function delete(User $user, Recurso $recurso)
    {
        return $user->can('recursos.delete') || $user->can('recursos.all');
    }

    /**
     * Determina si el usuario puede restaurar recurso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Recurso $recurso
     * @return mixed
     */
    public function restore(User $user, Recurso $recurso)
    {
        return $user->can('recursos.restore') || $user->can('recursos.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente recurso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Recurso $recurso
     * @return mixed
     */
    public function forceDelete(User $user, Recurso $recurso)
    {
        return $user->can('recursos.destroy') || $user->can('recursos.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
