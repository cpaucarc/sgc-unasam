<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Meta;
use Illuminate\Auth\Access\HandlesAuthorization;

class MetaPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de meta.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Meta $meta
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('metas.all') || $user->can('metas.index');
    }

    /**
     * Determina que usuarios puede ver meta.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Meta $meta
     * @return mixed
     */
    public function view(User $user, Meta $meta)
    {
        return $user->can('metas.show') || $user->can('metas.all');
    }

    /**
     * Determina que usuarios puede crear meta.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('metas.create') || $user->can('metas.all');
    }

    /**
     * Determina si el usuario puede modificar meta.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Meta $meta
     * @return mixed
     */
    public function update(User $user, Meta $meta)
    {
        return $user->can('metas.update') || $user->can('metas.all');
    }

    /**
     * Determina si el usuario puede eliminar meta.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Meta $meta
     * @return mixed
     */
    public function delete(User $user, Meta $meta)
    {
        return $user->can('metas.delete') || $user->can('metas.all');
    }

    /**
     * Determina si el usuario puede restaurar meta.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Meta $meta
     * @return mixed
     */
    public function restore(User $user, Meta $meta)
    {
        return $user->can('metas.restore') || $user->can('metas.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente meta.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Meta $meta
     * @return mixed
     */
    public function forceDelete(User $user, Meta $meta)
    {
        return $user->can('metas.destroy') || $user->can('metas.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
