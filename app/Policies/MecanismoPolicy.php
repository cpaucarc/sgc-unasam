<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Mecanismo;
use Illuminate\Auth\Access\HandlesAuthorization;

class MecanismoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de mecanismo.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Mecanismo $mecanismo
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('mecanismos.all') || $user->can('mecanismos.index');
    }

    /**
     * Determina que usuarios puede ver mecanismo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Mecanismo $mecanismo
     * @return mixed
     */
    public function view(User $user, Mecanismo $mecanismo)
    {
        return $user->can('mecanismos.show') || $user->can('mecanismos.all');
    }

    /**
     * Determina que usuarios puede crear mecanismo.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('mecanismos.create') || $user->can('mecanismos.all');
    }

    /**
     * Determina si el usuario puede modificar mecanismo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Mecanismo $mecanismo
     * @return mixed
     */
    public function update(User $user, Mecanismo $mecanismo)
    {
        return $user->can('mecanismos.update') || $user->can('mecanismos.all');
    }

    /**
     * Determina si el usuario puede eliminar mecanismo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Mecanismo $mecanismo
     * @return mixed
     */
    public function delete(User $user, Mecanismo $mecanismo)
    {
        return $user->can('mecanismos.delete') || $user->can('mecanismos.all');
    }

    /**
     * Determina si el usuario puede restaurar mecanismo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Mecanismo $mecanismo
     * @return mixed
     */
    public function restore(User $user, Mecanismo $mecanismo)
    {
        return $user->can('mecanismos.restore') || $user->can('mecanismos.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente mecanismo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Mecanismo $mecanismo
     * @return mixed
     */
    public function forceDelete(User $user, Mecanismo $mecanismo)
    {
        return $user->can('mecanismos.destroy') || $user->can('mecanismos.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
