<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Interesado;
use Illuminate\Auth\Access\HandlesAuthorization;

class InteresadoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de interesado.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Interesado $interesado
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('interesados.all') || $user->can('interesados.index');
    }

    /**
     * Determina que usuarios puede ver interesado.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Interesado $interesado
     * @return mixed
     */
    public function view(User $user, Interesado $interesado)
    {
        return $user->can('interesados.show') || $user->can('interesados.all');
    }

    /**
     * Determina que usuarios puede crear interesado.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('interesados.create') || $user->can('interesados.all');
    }

    /**
     * Determina si el usuario puede modificar interesado.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Interesado $interesado
     * @return mixed
     */
    public function update(User $user, Interesado $interesado)
    {
        return $user->can('interesados.update') || $user->can('interesados.all');
    }

    /**
     * Determina si el usuario puede eliminar interesado.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Interesado $interesado
     * @return mixed
     */
    public function delete(User $user, Interesado $interesado)
    {
        return $user->can('interesados.delete') || $user->can('interesados.all');
    }

    /**
     * Determina si el usuario puede restaurar interesado.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Interesado $interesado
     * @return mixed
     */
    public function restore(User $user, Interesado $interesado)
    {
        return $user->can('interesados.restore') || $user->can('interesados.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente interesado.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Interesado $interesado
     * @return mixed
     */
    public function forceDelete(User $user, Interesado $interesado)
    {
        return $user->can('interesados.destroy') || $user->can('interesados.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
