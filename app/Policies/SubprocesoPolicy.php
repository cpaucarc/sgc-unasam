<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Subproceso;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubprocesoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de subproceso.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Subproceso $subproceso
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('subprocesos.all') || $user->can('subprocesos.index');
    }

    /**
     * Determina que usuarios puede ver subproceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Subproceso $subproceso
     * @return mixed
     */
    public function view(User $user, Subproceso $subproceso)
    {
        return $user->can('subprocesos.show') || $user->can('subprocesos.all');
    }

    /**
     * Determina que usuarios puede crear subproceso.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('subprocesos.create') || $user->can('subprocesos.all');
    }

    /**
     * Determina si el usuario puede modificar subproceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Subproceso $subproceso
     * @return mixed
     */
    public function update(User $user, Subproceso $subproceso)
    {
        return $user->can('subprocesos.update') || $user->can('subprocesos.all');
    }

    /**
     * Determina si el usuario puede eliminar subproceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Subproceso $subproceso
     * @return mixed
     */
    public function delete(User $user, Subproceso $subproceso)
    {
        return $user->can('subprocesos.delete') || $user->can('subprocesos.all');
    }

    /**
     * Determina si el usuario puede restaurar subproceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Subproceso $subproceso
     * @return mixed
     */
    public function restore(User $user, Subproceso $subproceso)
    {
        return $user->can('subprocesos.restore') || $user->can('subprocesos.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente subproceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Subproceso $subproceso
     * @return mixed
     */
    public function forceDelete(User $user, Subproceso $subproceso)
    {
        return $user->can('subprocesos.destroy') || $user->can('subprocesos.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
