<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Restriccion;
use Illuminate\Auth\Access\HandlesAuthorization;

class RestriccionPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de restriccion.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Restriccion $restriccion
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('restriccions.all') || $user->can('restriccions.index');
    }

    /**
     * Determina que usuarios puede ver restriccion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Restriccion $restriccion
     * @return mixed
     */
    public function view(User $user, Restriccion $restriccion)
    {
        return $user->can('restriccions.show') || $user->can('restriccions.all');
    }

    /**
     * Determina que usuarios puede crear restriccion.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('restriccions.create') || $user->can('restriccions.all');
    }

    /**
     * Determina si el usuario puede modificar restriccion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Restriccion $restriccion
     * @return mixed
     */
    public function update(User $user, Restriccion $restriccion)
    {
        return $user->can('restriccions.update') || $user->can('restriccions.all');
    }

    /**
     * Determina si el usuario puede eliminar restriccion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Restriccion $restriccion
     * @return mixed
     */
    public function delete(User $user, Restriccion $restriccion)
    {
        return $user->can('restriccions.delete') || $user->can('restriccions.all');
    }

    /**
     * Determina si el usuario puede restaurar restriccion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Restriccion $restriccion
     * @return mixed
     */
    public function restore(User $user, Restriccion $restriccion)
    {
        return $user->can('restriccions.restore') || $user->can('restriccions.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente restriccion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Restriccion $restriccion
     * @return mixed
     */
    public function forceDelete(User $user, Restriccion $restriccion)
    {
        return $user->can('restriccions.destroy') || $user->can('restriccions.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
