<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Proceso;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProcesoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de proceso.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Proceso $proceso
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('procesos.all') || $user->can('procesos.index');
    }

    /**
     * Determina que usuarios puede ver proceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Proceso $proceso
     * @return mixed
     */
    public function view(User $user, Proceso $proceso)
    {
        return $user->can('procesos.show') || $user->can('procesos.all');
    }

    /**
     * Determina que usuarios puede crear proceso.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('procesos.create') || $user->can('procesos.all');
    }

    /**
     * Determina si el usuario puede modificar proceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Proceso $proceso
     * @return mixed
     */
    public function update(User $user, Proceso $proceso)
    {
        return $user->can('procesos.update') || $user->can('procesos.all');
    }

    /**
     * Determina si el usuario puede eliminar proceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Proceso $proceso
     * @return mixed
     */
    public function delete(User $user, Proceso $proceso)
    {
        return $user->can('procesos.delete') || $user->can('procesos.all');
    }

    /**
     * Determina si el usuario puede restaurar proceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Proceso $proceso
     * @return mixed
     */
    public function restore(User $user, Proceso $proceso)
    {
        return $user->can('procesos.restore') || $user->can('procesos.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente proceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Proceso $proceso
     * @return mixed
     */
    public function forceDelete(User $user, Proceso $proceso)
    {
        return $user->can('procesos.destroy') || $user->can('procesos.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
