<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de permission.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Permission $permission
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('permissions.all') || $user->can('permissions.index');
    }

    /**
     * Determina que usuarios puede ver permission.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Permission $permission
     * @return mixed
     */
    public function view(User $user, Permission $permission)
    {
        return $user->can('permissions.show') || $user->can('permissions.all');
    }

    /**
     * Determina que usuarios puede crear permission.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('permissions.create') || $user->can('permissions.all');
    }

    /**
     * Determina si el usuario puede modificar permission.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Permission $permission
     * @return mixed
     */
    public function update(User $user, Permission $permission)
    {
        return $user->can('permissions.update') || $user->can('permissions.all');
    }

    /**
     * Determina si el usuario puede eliminar permission.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Permission $permission
     * @return mixed
     */
    public function delete(User $user, Permission $permission)
    {
        return $user->can('permissions.delete') || $user->can('permissions.all');
    }

    /**
     * Determina si el usuario puede restaurar permission.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Permission $permission
     * @return mixed
     */
    public function restore(User $user, Permission $permission)
    {
        return $user->can('permissions.restore') || $user->can('permissions.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente permission.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Permission $permission
     * @return mixed
     */
    public function forceDelete(User $user, Permission $permission)
    {
        return $user->can('permissions.destroy') || $user->can('permissions.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
