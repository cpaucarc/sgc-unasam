<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Evidencia;
use Illuminate\Auth\Access\HandlesAuthorization;

class EvidenciaPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de evidencia.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Evidencia $evidencia
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('evidencias.all') || $user->can('evidencias.index');
    }

    /**
     * Determina que usuarios puede ver evidencia.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Evidencia $evidencia
     * @return mixed
     */
    public function view(User $user, Evidencia $evidencia)
    {
        return $user->can('evidencias.show') || $user->can('evidencias.all');
    }
    public function view2(User $user)
    {
        return $user->can('evidencias.show') || $user->can('evidencias.all');
    }

    /**
     * Determina que usuarios puede crear evidencia.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('evidencias.create') || $user->can('evidencias.all');
    }

    /**
     * Determina si el usuario puede modificar evidencia.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Evidencia $evidencia
     * @return mixed
     */
    public function update(User $user, Evidencia $evidencia)
    {
        return $user->can('evidencias.update') || $user->can('evidencias.all');
    }

    /**
     * Determina si el usuario puede eliminar evidencia.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Evidencia $evidencia
     * @return mixed
     */
    public function delete(User $user, Evidencia $evidencia)
    {
        return $user->can('evidencias.delete') || $user->can('evidencias.all');
    }

    public function delete2(User $user)
    {
        return $user->can('evidencias.delete') || $user->can('evidencias.all');
    }

    /**
     * Determina si el usuario puede restaurar evidencia.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Evidencia $evidencia
     * @return mixed
     */
    public function restore(User $user, Evidencia $evidencia)
    {
        return $user->can('evidencias.restore') || $user->can('evidencias.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente evidencia.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Evidencia $evidencia
     * @return mixed
     */
    public function forceDelete(User $user, Evidencia $evidencia)
    {
        return $user->can('evidencias.destroy') || $user->can('evidencias.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
