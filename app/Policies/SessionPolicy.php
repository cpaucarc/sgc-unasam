<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Session;
use Illuminate\Auth\Access\HandlesAuthorization;

class SessionPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de session.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Session $session
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('sessions.all') || $user->can('sessions.index');
    }

    /**
     * Determina que usuarios puede ver session.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Session $session
     * @return mixed
     */
    public function view(User $user, Session $session)
    {
        return $user->can('sessions.show') || $user->can('sessions.all');
    }

    /**
     * Determina que usuarios puede crear session.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('sessions.create') || $user->can('sessions.all');
    }

    /**
     * Determina si el usuario puede modificar session.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Session $session
     * @return mixed
     */
    public function update(User $user, Session $session)
    {
        return $user->can('sessions.update') || $user->can('sessions.all');
    }

    /**
     * Determina si el usuario puede eliminar session.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Session $session
     * @return mixed
     */
    public function delete(User $user, Session $session)
    {
        return $user->can('sessions.delete') || $user->can('sessions.all');
    }

    /**
     * Determina si el usuario puede restaurar session.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Session $session
     * @return mixed
     */
    public function restore(User $user, Session $session)
    {
        return $user->can('sessions.restore') || $user->can('sessions.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente session.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Session $session
     * @return mixed
     */
    public function forceDelete(User $user, Session $session)
    {
        return $user->can('sessions.destroy') || $user->can('sessions.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
