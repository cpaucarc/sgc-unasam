<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Macroproceso;
use Illuminate\Auth\Access\HandlesAuthorization;

class MacroprocesoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de macroproceso.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Macroproceso $macroproceso
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('macroprocesos.all') || $user->can('macroprocesos.index');
    }

    /**
     * Determina que usuarios puede ver macroproceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Macroproceso $macroproceso
     * @return mixed
     */
    public function view(User $user, Macroproceso $macroproceso)
    {

        $permiso_general =  $user->can('macroprocesos.show') || $user->can('macroprocesos.all');
        //$permiso_especifico = ($user->asignacion->area_id === $macroproceso->area->id) || ($user->asignacion->area_id === $macroproceso->area->area_id);

        return $permiso_general;// && $permiso_especifico;
    }

    /**
     * Determina que usuarios puede crear macroproceso.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('macroprocesos.create') || $user->can('macroprocesos.all');
    }

    /**
     * Determina que usuarios puede ver la ficha de caracterizacion de macroproceso.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Macroproceso $macroproceso
     * @return mixed
     */
    // public function viewCaracterizacion(User $user)
    // {
    //     return $user->can('macroprocesos.caracterizacion') || $user->can('macroprocesos.all');
    // }

    /**
     * Determina si el usuario puede modificar macroproceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Macroproceso $macroproceso
     * @return mixed
     */
    public function update(User $user, Macroproceso $macroproceso)
    {
        $permiso_general =  $user->can('macroprocesos.update') || $user->can('macroprocesos.all');
        $permiso_especifico = ($user->asignacion->area_id === $macroproceso->area->id) || ($user->asignacion->area_id === $macroproceso->area->area_id);

        return $permiso_general && $permiso_especifico;
    }

    /**
     * Determina si el usuario puede eliminar macroproceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Macroproceso $macroproceso
     * @return mixed
     */
    public function delete(User $user, Macroproceso $macroproceso)
    {
        return $user->can('macroprocesos.delete') || $user->can('macroprocesos.all');
    }

    /**
     * Determina si el usuario puede restaurar macroproceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Macroproceso $macroproceso
     * @return mixed
     */
    public function restore(User $user, Macroproceso $macroproceso)
    {
        return $user->can('macroprocesos.restore') || $user->can('macroprocesos.all');
    }

    

    /**
     * Determina si el usuario puede eliminar permanentemente macroproceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Macroproceso $macroproceso
     * @return mixed
     */
    public function forceDelete(User $user, Macroproceso $macroproceso)
    {
        return $user->can('macroprocesos.destroy') || $user->can('macroprocesos.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
            return true;
        }
    }
}
