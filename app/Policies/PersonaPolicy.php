<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Persona;
use Illuminate\Auth\Access\HandlesAuthorization;

class PersonaPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de persona.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Persona $persona
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('personas.all') || $user->can('personas.index');
    }

    /**
     * Determina que usuarios puede ver persona.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Persona $persona
     * @return mixed
     */
    public function view(User $user, Persona $persona)
    {
        return $user->can('personas.show') || $user->can('personas.all');
    }

    /**
     * Determina que usuarios puede crear persona.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('personas.create') || $user->can('personas.all');
    }

    /**
     * Determina si el usuario puede modificar persona.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Persona $persona
     * @return mixed
     */
    public function update(User $user, Persona $persona)
    {
        return $user->can('personas.update') || $user->can('personas.all');
    }

    /**
     * Determina si el usuario puede eliminar persona.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Persona $persona
     * @return mixed
     */
    public function delete(User $user, Persona $persona)
    {
        return $user->can('personas.delete') || $user->can('personas.all');
    }

    /**
     * Determina si el usuario puede restaurar persona.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Persona $persona
     * @return mixed
     */
    public function restore(User $user, Persona $persona)
    {
        return $user->can('personas.restore') || $user->can('personas.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente persona.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Persona $persona
     * @return mixed
     */
    public function forceDelete(User $user, Persona $persona)
    {
        return $user->can('personas.destroy') || $user->can('personas.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
