<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Archivo;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArchivoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de archivo.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Archivo $archivo
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('archivos.all') || $user->can('archivos.index');
    }

    /**
     * Determina que usuarios puede ver archivo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Archivo $archivo
     * @return mixed
     */
    public function view(User $user, Archivo $archivo)
    {
        return $user->can('archivos.show') || $user->can('archivos.all');
    }

    /**
     * Determina que usuarios puede crear archivo.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('archivos.create') || $user->can('archivos.all');
    }

    /**
     * Determina si el usuario puede modificar archivo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Archivo $archivo
     * @return mixed
     */
    public function update(User $user, Archivo $archivo)
    {
        return $user->can('archivos.update') || $user->can('archivos.all');
    }

    /**
     * Determina si el usuario puede eliminar archivo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Archivo $archivo
     * @return mixed
     */
    public function delete(User $user, Archivo $archivo)
    {
        return $user->can('archivos.delete') || $user->can('archivos.all');
    }



    /**
     * Determina si el usuario puede cargar archivo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Archivo $archivo
     * @return mixed
     */
    public function upload(User $user, ?Archivo $archivo)
    {
        return $user->can('archivos.upload') || $user->can('archivos.all');
    }

    
    public function upload_general(User $user)
    {
        return $user->can('archivos.upload') || $user->can('archivos.all');
    }

    /**
     * Determina si el usuario puede descargar archivo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Archivo $archivo
     * @return mixed
     */
    public function download(User $user, Archivo $archivo)
    {
        return $user->can('archivos.download') || $user->can('archivos.all');
    }

    public function download_general(User $user)
    {
        return $user->can('archivos.download') || $user->can('archivos.all');
    }
    /**
     * Determina si el usuario puede restaurar archivo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Archivo $archivo
     * @return mixed
     */
    public function restore(User $user, Archivo $archivo)
    {
        return $user->can('archivos.restore') || $user->can('archivos.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente archivo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Archivo $archivo
     * @return mixed
     */
    public function forceDelete(User $user, Archivo $archivo)
    {
        return $user->can('archivos.destroy') || $user->can('archivos.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
