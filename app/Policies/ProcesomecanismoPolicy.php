<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Procesomecanismo;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProcesomecanismoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de procesomecanismo.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Procesomecanismo $procesomecanismo
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('procesomecanismos.all') || $user->can('procesomecanismos.index');
    }

    /**
     * Determina que usuarios puede ver procesomecanismo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Procesomecanismo $procesomecanismo
     * @return mixed
     */
    public function view(User $user, Procesomecanismo $procesomecanismo)
    {
        return $user->can('procesomecanismos.show') || $user->can('procesomecanismos.all');
    }

    /**
     * Determina que usuarios puede crear procesomecanismo.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('procesomecanismos.create') || $user->can('procesomecanismos.all');
    }

    /**
     * Determina si el usuario puede modificar procesomecanismo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Procesomecanismo $procesomecanismo
     * @return mixed
     */
    public function update(User $user, Procesomecanismo $procesomecanismo)
    {
        return $user->can('procesomecanismos.update') || $user->can('procesomecanismos.all');
    }

    /**
     * Determina si el usuario puede eliminar procesomecanismo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Procesomecanismo $procesomecanismo
     * @return mixed
     */
    public function delete(User $user, Procesomecanismo $procesomecanismo)
    {
        return $user->can('procesomecanismos.delete') || $user->can('procesomecanismos.all');
    }

    /**
     * Determina si el usuario puede restaurar procesomecanismo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Procesomecanismo $procesomecanismo
     * @return mixed
     */
    public function restore(User $user, Procesomecanismo $procesomecanismo)
    {
        return $user->can('procesomecanismos.restore') || $user->can('procesomecanismos.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente procesomecanismo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Procesomecanismo $procesomecanismo
     * @return mixed
     */
    public function forceDelete(User $user, Procesomecanismo $procesomecanismo)
    {
        return $user->can('procesomecanismos.destroy') || $user->can('procesomecanismos.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
