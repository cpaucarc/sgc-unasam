<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Formula;
use Illuminate\Auth\Access\HandlesAuthorization;

class FormulaPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de formula.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Formula $formula
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('formulas.all') || $user->can('formulas.index');
    }

    /**
     * Determina que usuarios puede ver formula.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Formula $formula
     * @return mixed
     */
    public function view(User $user, Formula $formula)
    {
        return $user->can('formulas.show') || $user->can('formulas.all');
    }

    /**
     * Determina que usuarios puede crear formula.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('formulas.create') || $user->can('formulas.all');
    }

    /**
     * Determina si el usuario puede modificar formula.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Formula $formula
     * @return mixed
     */
    public function update(User $user, Formula $formula)
    {
        return $user->can('formulas.update') || $user->can('formulas.all');
    }

    /**
     * Determina si el usuario puede eliminar formula.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Formula $formula
     * @return mixed
     */
    public function delete(User $user, Formula $formula)
    {
        return $user->can('formulas.delete') || $user->can('formulas.all');
    }

    /**
     * Determina si el usuario puede restaurar formula.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Formula $formula
     * @return mixed
     */
    public function restore(User $user, Formula $formula)
    {
        return $user->can('formulas.restore') || $user->can('formulas.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente formula.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Formula $formula
     * @return mixed
     */
    public function forceDelete(User $user, Formula $formula)
    {
        return $user->can('formulas.destroy') || $user->can('formulas.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
