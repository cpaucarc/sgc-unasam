<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Documento;
use Illuminate\Auth\Access\HandlesAuthorization;

class DocumentoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de documento.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Documento $documento
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('documentos.all') || $user->can('documentos.index');
    }

    /**
     * Determina que usuarios puede ver documento.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Documento $documento
     * @return mixed
     */
    public function view(User $user, Documento $documento)
    {
        return $user->can('documentos.show') || $user->can('documentos.all');
    }

    /**
     * Determina que usuarios puede crear documento.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('documentos.create') || $user->can('documentos.all');
    }

    /**
     * Determina si el usuario puede modificar documento.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Documento $documento
     * @return mixed
     */
    public function update(User $user, Documento $documento)
    {
        return $user->can('documentos.update') || $user->can('documentos.all');
    }

    /**
     * Determina si el usuario puede eliminar documento.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Documento $documento
     * @return mixed
     */
    public function delete(User $user, Documento $documento)
    {
        return $user->can('documentos.delete') || $user->can('documentos.all');
    }

    /**
     * Determina si el usuario puede restaurar documento.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Documento $documento
     * @return mixed
     */
    public function restore(User $user, Documento $documento)
    {
        return $user->can('documentos.restore') || $user->can('documentos.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente documento.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Documento $documento
     * @return mixed
     */
    public function forceDelete(User $user, Documento $documento)
    {
        return $user->can('documentos.destroy') || $user->can('documentos.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
