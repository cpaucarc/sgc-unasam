<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Objetivo;
use Illuminate\Auth\Access\HandlesAuthorization;

class ObjetivoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de objetivo.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Objetivo $objetivo
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('objetivos.all') || $user->can('objetivos.index');
    }

    /**
     * Determina que usuarios puede ver objetivo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Objetivo $objetivo
     * @return mixed
     */
    public function view(User $user, Objetivo $objetivo)
    {
        return $user->can('objetivos.show') || $user->can('objetivos.all');
    }

    /**
     * Determina que usuarios puede crear objetivo.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('objetivos.create') || $user->can('objetivos.all');
    }

    /**
     * Determina si el usuario puede modificar objetivo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Objetivo $objetivo
     * @return mixed
     */
    public function update(User $user, Objetivo $objetivo)
    {
        return $user->can('objetivos.update') || $user->can('objetivos.all');
    }

    /**
     * Determina si el usuario puede eliminar objetivo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Objetivo $objetivo
     * @return mixed
     */
    public function delete(User $user, Objetivo $objetivo)
    {
        return $user->can('objetivos.delete') || $user->can('objetivos.all');
    }

    public function delete2(User $user)
    {
        return $user->can('objetivos.delete') || $user->can('objetivos.all');
    }

    /**
     * Determina si el usuario puede restaurar objetivo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Objetivo $objetivo
     * @return mixed
     */
    public function restore(User $user, Objetivo $objetivo)
    {
        return $user->can('objetivos.restore') || $user->can('objetivos.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente objetivo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Objetivo $objetivo
     * @return mixed
     */
    public function forceDelete(User $user, Objetivo $objetivo)
    {
        return $user->can('objetivos.destroy') || $user->can('objetivos.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
