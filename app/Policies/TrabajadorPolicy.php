<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Trabajador;
use Illuminate\Auth\Access\HandlesAuthorization;

class TrabajadorPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de trabajador.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Trabajador $trabajador
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('trabajadors.all') || $user->can('trabajadors.index');
    }

    /**
     * Determina que usuarios puede ver trabajador.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Trabajador $trabajador
     * @return mixed
     */
    public function view(User $user, Trabajador $trabajador)
    {
        return $user->can('trabajadors.show') || $user->can('trabajadors.all');
    }

    /**
     * Determina que usuarios puede crear trabajador.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('trabajadors.create') || $user->can('trabajadors.all');
    }

    /**
     * Determina si el usuario puede modificar trabajador.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Trabajador $trabajador
     * @return mixed
     */
    public function update(User $user, Trabajador $trabajador)
    {
        return $user->can('trabajadors.update') || $user->can('trabajadors.all');
    }

    /**
     * Determina si el usuario puede eliminar trabajador.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Trabajador $trabajador
     * @return mixed
     */
    public function delete(User $user, Trabajador $trabajador)
    {
        return $user->can('trabajadors.delete') || $user->can('trabajadors.all');
    }

    /**
     * Determina si el usuario puede restaurar trabajador.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Trabajador $trabajador
     * @return mixed
     */
    public function restore(User $user, Trabajador $trabajador)
    {
        return $user->can('trabajadors.restore') || $user->can('trabajadors.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente trabajador.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Trabajador $trabajador
     * @return mixed
     */
    public function forceDelete(User $user, Trabajador $trabajador)
    {
        return $user->can('trabajadors.destroy') || $user->can('trabajadors.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
