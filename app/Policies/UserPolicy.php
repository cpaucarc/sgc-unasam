<?php

namespace App\Policies;

use App\Models\User;
use App\Models\User as ModelUser;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de user.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('users.all') || $user->can('users.index');
    }

    /**
     * Determina que usuarios puede ver user.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function view(User $user, ModelUser $model_user)
    {
        return $user->can('users.show') || $user->can('users.all');
    }

    /**
     * Determina que usuarios puede crear user.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('users.create') || $user->can('users.all');
    }

    /**
     * Determina si el usuario puede modificar user.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function update(User $user, ModelUser $model_user)
    {
        return $user->can('users.update') || $user->can('users.all');
    }

    /**
     * Determina si el usuario puede eliminar user.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function delete(User $user, ModelUser $model_user)
    {
        return $user->can('users.delete') || $user->can('users.all');
    }

    /**
     * Determina si el usuario puede restaurar user.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function restore(User $user, ModelUser $model_user)
    {
        return $user->can('users.restore') || $user->can('users.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente user.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function forceDelete(User $user, ModelUser $model_user)
    {
        return $user->can('users.destroy') || $user->can('users.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
