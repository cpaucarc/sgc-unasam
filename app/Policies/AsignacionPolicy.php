<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Asignacion;
use Illuminate\Auth\Access\HandlesAuthorization;

class AsignacionPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de asignacion.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Asignacion $asignacion
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('asignacions.all') || $user->can('asignacions.index');
    }

    /**
     * Determina que usuarios puede ver asignacion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Asignacion $asignacion
     * @return mixed
     */
    public function view(User $user, Asignacion $asignacion)
    {
        return $user->can('asignacions.show') || $user->can('asignacions.all');
    }

    /**
     * Determina que usuarios puede crear asignacion.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('asignacions.create') || $user->can('asignacions.all');
    }

    /**
     * Determina si el usuario puede modificar asignacion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Asignacion $asignacion
     * @return mixed
     */
    public function update(User $user, Asignacion $asignacion)
    {
        return $user->can('asignacions.update') || $user->can('asignacions.all');
    }

    /**
     * Determina si el usuario puede eliminar asignacion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Asignacion $asignacion
     * @return mixed
     */
    public function delete(User $user, Asignacion $asignacion)
    {
        return $user->can('asignacions.delete') || $user->can('asignacions.all');
    }

    /**
     * Determina si el usuario puede restaurar asignacion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Asignacion $asignacion
     * @return mixed
     */
    public function restore(User $user, Asignacion $asignacion)
    {
        return $user->can('asignacions.restore') || $user->can('asignacions.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente asignacion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Asignacion $asignacion
     * @return mixed
     */
    public function forceDelete(User $user, Asignacion $asignacion)
    {
        return $user->can('asignacions.destroy') || $user->can('asignacions.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
