<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Entrada;
use Illuminate\Auth\Access\HandlesAuthorization;

class EntradaPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de entrada.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Entrada $entrada
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('entradas.all') || $user->can('entradas.index');
    }

    /**
     * Determina que usuarios puede ver entrada.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Entrada $entrada
     * @return mixed
     */
    public function view(User $user, Entrada $entrada)
    {
        return $user->can('entradas.show') || $user->can('entradas.all');
    }

    /**
     * Determina que usuarios puede crear entrada.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('entradas.create') || $user->can('entradas.all');
    }

    /**
     * Determina si el usuario puede modificar entrada.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Entrada $entrada
     * @return mixed
     */
    public function update(User $user, Entrada $entrada)
    {
        return $user->can('entradas.update') || $user->can('entradas.all');
    }

    /**
     * Determina si el usuario puede eliminar entrada.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Entrada $entrada
     * @return mixed
     */
    public function delete(User $user, Entrada $entrada)
    {
        return $user->can('entradas.delete') || $user->can('entradas.all');
    }

    /**
     * Determina si el usuario puede restaurar entrada.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Entrada $entrada
     * @return mixed
     */
    public function restore(User $user, Entrada $entrada)
    {
        return $user->can('entradas.restore') || $user->can('entradas.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente entrada.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Entrada $entrada
     * @return mixed
     */
    public function forceDelete(User $user, Entrada $entrada)
    {
        return $user->can('entradas.destroy') || $user->can('entradas.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
