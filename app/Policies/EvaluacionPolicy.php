<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Evaluacion;
use Illuminate\Auth\Access\HandlesAuthorization;

class EvaluacionPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de evaluacion.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Evaluacion $evaluacion
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('evaluacions.all') || $user->can('evaluacions.index');
    }

    /**
     * Determina que usuarios puede ver evaluacion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Evaluacion $evaluacion
     * @return mixed
     */
    public function view(User $user, Evaluacion $evaluacion)
    {
        return $user->can('evaluacions.show') || $user->can('evaluacions.all');
    }

    /**
     * Determina que usuarios puede crear evaluacion.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('evaluacions.create') || $user->can('evaluacions.all');
    }

    /**
     * Determina si el usuario puede modificar evaluacion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Evaluacion $evaluacion
     * @return mixed
     */
    public function update(User $user, Evaluacion $evaluacion)
    {
        return $user->can('evaluacions.update') || $user->can('evaluacions.all');
    }

    /**
     * Determina si el usuario puede eliminar evaluacion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Evaluacion $evaluacion
     * @return mixed
     */
    public function delete(User $user, Evaluacion $evaluacion)
    {
        return $user->can('evaluacions.delete') || $user->can('evaluacions.all');
    }

    /**
     * Determina si el usuario puede restaurar evaluacion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Evaluacion $evaluacion
     * @return mixed
     */
    public function restore(User $user, Evaluacion $evaluacion)
    {
        return $user->can('evaluacions.restore') || $user->can('evaluacions.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente evaluacion.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Evaluacion $evaluacion
     * @return mixed
     */
    public function forceDelete(User $user, Evaluacion $evaluacion)
    {
        return $user->can('evaluacions.destroy') || $user->can('evaluacions.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
