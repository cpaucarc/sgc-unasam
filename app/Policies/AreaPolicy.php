<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Area;
use Illuminate\Auth\Access\HandlesAuthorization;

class AreaPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de area.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Area $area
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('areas.all') || $user->can('areas.index');
    }

    /**
     * Determina que usuarios puede ver area.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Area $area
     * @return mixed
     */
    public function view(User $user, Area $area)
    {
        return $user->can('areas.show') || $user->can('areas.all');
    }

    public function manejar(User $user, Area $area)
    {
        return $user->asignacion->area_id === $area->id;
    }

    /**
     * Determina que usuarios puede crear area.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('areas.create') || $user->can('areas.all');
    }

    /**
     * Determina si el usuario puede modificar area.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Area $area
     * @return mixed
     */
    public function update(User $user, Area $area)
    {
        return $user->can('areas.update') || $user->can('areas.all');
    }

    /**
     * Determina si el usuario puede eliminar area.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Area $area
     * @return mixed
     */
    public function delete(User $user, Area $area)
    {
        return $user->can('areas.delete') || $user->can('areas.all');
    }

    /**
     * Determina si el usuario puede restaurar area.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Area $area
     * @return mixed
     */
    public function restore(User $user, Area $area)
    {
        return $user->can('areas.restore') || $user->can('areas.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente area.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Area $area
     * @return mixed
     */
    public function forceDelete(User $user, Area $area)
    {
        return $user->can('areas.destroy') || $user->can('areas.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
