<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Requisito;
use Illuminate\Auth\Access\HandlesAuthorization;

class RequisitoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de requisito.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Requisito $requisito
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('requisitos.all') || $user->can('requisitos.index');
    }

    /**
     * Determina que usuarios puede ver requisito.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Requisito $requisito
     * @return mixed
     */
    public function view(User $user, Requisito $requisito)
    {
        return $user->can('requisitos.show') || $user->can('requisitos.all');
    }

    /**
     * Determina que usuarios puede crear requisito.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('requisitos.create') || $user->can('requisitos.all');
    }

    /**
     * Determina si el usuario puede modificar requisito.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Requisito $requisito
     * @return mixed
     */
    public function update(User $user, Requisito $requisito)
    {
        return $user->can('requisitos.update') || $user->can('requisitos.all');
    }

    /**
     * Determina si el usuario puede eliminar requisito.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Requisito $requisito
     * @return mixed
     */
    public function delete(User $user, Requisito $requisito)
    {
        return $user->can('requisitos.delete') || $user->can('requisitos.all');
    }

    /**
     * Determina si el usuario puede restaurar requisito.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Requisito $requisito
     * @return mixed
     */
    public function restore(User $user, Requisito $requisito)
    {
        return $user->can('requisitos.restore') || $user->can('requisitos.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente requisito.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Requisito $requisito
     * @return mixed
     */
    public function forceDelete(User $user, Requisito $requisito)
    {
        return $user->can('requisitos.destroy') || $user->can('requisitos.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
