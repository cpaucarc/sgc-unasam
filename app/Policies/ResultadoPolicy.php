<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Resultado;
use Illuminate\Auth\Access\HandlesAuthorization;

class ResultadoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de resultado.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Resultado $resultado
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('resultados.all') || $user->can('resultados.index');
    }

    /**
     * Determina que usuarios puede ver resultado.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Resultado $resultado
     * @return mixed
     */
    public function view(User $user, Resultado $resultado)
    {
        return $user->can('resultados.show') || $user->can('resultados.all');
    }

    /**
     * Determina que usuarios puede crear resultado.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('resultados.create') || $user->can('resultados.all');
    }

    /**
     * Determina si el usuario puede modificar resultado.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Resultado $resultado
     * @return mixed
     */
    public function update(User $user, Resultado $resultado)
    {
        return $user->can('resultados.update') || $user->can('resultados.all');
    }

    /**
     * Determina si el usuario puede eliminar resultado.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Resultado $resultado
     * @return mixed
     */
    public function delete(User $user, Resultado $resultado)
    {
        return $user->can('resultados.delete') || $user->can('resultados.all');
    }

    /**
     * Determina si el usuario puede restaurar resultado.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Resultado $resultado
     * @return mixed
     */
    public function restore(User $user, Resultado $resultado)
    {
        return $user->can('resultados.restore') || $user->can('resultados.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente resultado.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Resultado $resultado
     * @return mixed
     */
    public function forceDelete(User $user, Resultado $resultado)
    {
        return $user->can('resultados.destroy') || $user->can('resultados.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
