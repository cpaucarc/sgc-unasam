<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Parametro;
use Illuminate\Auth\Access\HandlesAuthorization;

class ParametroPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de parametro.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Parametro $parametro
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('parametros.all') || $user->can('parametros.index');
    }

    /**
     * Determina que usuarios puede ver parametro.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Parametro $parametro
     * @return mixed
     */
    public function view(User $user, Parametro $parametro)
    {
        return $user->can('parametros.show') || $user->can('parametros.all');
    }

    /**
     * Determina que usuarios puede crear parametro.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('parametros.create') || $user->can('parametros.all');
    }

    /**
     * Determina si el usuario puede modificar parametro.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Parametro $parametro
     * @return mixed
     */
    public function update(User $user, Parametro $parametro)
    {
        return $user->can('parametros.update') || $user->can('parametros.all');
    }

    /**
     * Determina si el usuario puede eliminar parametro.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Parametro $parametro
     * @return mixed
     */
    public function delete(User $user, Parametro $parametro)
    {
        return $user->can('parametros.delete') || $user->can('parametros.all');
    }

    /**
     * Determina si el usuario puede restaurar parametro.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Parametro $parametro
     * @return mixed
     */
    public function restore(User $user, Parametro $parametro)
    {
        return $user->can('parametros.restore') || $user->can('parametros.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente parametro.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Parametro $parametro
     * @return mixed
     */
    public function forceDelete(User $user, Parametro $parametro)
    {
        return $user->can('parametros.destroy') || $user->can('parametros.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
