<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Migration;
use Illuminate\Auth\Access\HandlesAuthorization;

class MigrationPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de migration.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Migration $migration
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('migrations.all') || $user->can('migrations.index');
    }

    /**
     * Determina que usuarios puede ver migration.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Migration $migration
     * @return mixed
     */
    public function view(User $user, Migration $migration)
    {
        return $user->can('migrations.show') || $user->can('migrations.all');
    }

    /**
     * Determina que usuarios puede crear migration.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('migrations.create') || $user->can('migrations.all');
    }

    /**
     * Determina si el usuario puede modificar migration.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Migration $migration
     * @return mixed
     */
    public function update(User $user, Migration $migration)
    {
        return $user->can('migrations.update') || $user->can('migrations.all');
    }

    /**
     * Determina si el usuario puede eliminar migration.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Migration $migration
     * @return mixed
     */
    public function delete(User $user, Migration $migration)
    {
        return $user->can('migrations.delete') || $user->can('migrations.all');
    }

    /**
     * Determina si el usuario puede restaurar migration.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Migration $migration
     * @return mixed
     */
    public function restore(User $user, Migration $migration)
    {
        return $user->can('migrations.restore') || $user->can('migrations.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente migration.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Migration $migration
     * @return mixed
     */
    public function forceDelete(User $user, Migration $migration)
    {
        return $user->can('migrations.destroy') || $user->can('migrations.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
