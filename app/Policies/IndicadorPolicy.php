<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Indicador;
use Illuminate\Auth\Access\HandlesAuthorization;

class IndicadorPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de indicador.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Indicador $indicador
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('indicadors.all') || $user->can('indicadors.index');
    }

    /**
     * Determina que usuarios puede ver indicador.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Indicador $indicador
     * @return mixed
     */
    public function view(User $user, Indicador $indicador)
    {
        return $user->can('indicadors.show') || $user->can('indicadors.all');
    }


    /**
     * Determina que usuarios puede auditar indicador.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Indicador $indicador
     * @return mixed
     */
    public function audit(User $user, Indicador $indicador)
    {
        return $user->can('indicadors.audit') || $user->can('indicadors.all');
    }

    /**
     * Determina que usuarios puede crear indicador.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('indicadors.create') || $user->can('indicadors.all');
    }

    /**
     * Determina si el usuario puede modificar indicador.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Indicador $indicador
     * @return mixed
     */
    public function update(User $user, Indicador $indicador)
    {
        return $user->can('indicadors.update') || $user->can('indicadors.all');
    }

    /**
     * Determina si el usuario puede eliminar indicador.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Indicador $indicador
     * @return mixed
     */
    public function delete(User $user, Indicador $indicador)
    {
        return $user->can('indicadors.delete') || $user->can('indicadors.all');
    }

    /**
     * Determina si el usuario puede restaurar indicador.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Indicador $indicador
     * @return mixed
     */
    public function restore(User $user, Indicador $indicador)
    {
        return $user->can('indicadors.restore') || $user->can('indicadors.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente indicador.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Indicador $indicador
     * @return mixed
     */
    public function forceDelete(User $user, Indicador $indicador)
    {
        return $user->can('indicadors.destroy') || $user->can('indicadors.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
            return true;
        }
    }

    /**
     * Determina si el usuario puede eliminar indicador.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Indicador $indicador
     * @return mixed
     */
    public function verficha(User $user, Indicador $indicador)
    {
        return $user->can('indicadors.ficha') || $user->can('indicadors.all');
    }
}
