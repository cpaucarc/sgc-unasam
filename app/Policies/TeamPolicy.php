<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Team;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeamPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de team.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Team $team
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('teams.all') || $user->can('teams.index');
    }

    /**
     * Determina que usuarios puede ver team.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Team $team
     * @return mixed
     */
    public function view(User $user, Team $team)
    {
        return $user->can('teams.show') || $user->can('teams.all');
    }

    /**
     * Determina que usuarios puede crear team.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('teams.create') || $user->can('teams.all');
    }

    /**
     * Determina si el usuario puede modificar team.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Team $team
     * @return mixed
     */
    public function update(User $user, Team $team)
    {
        return $user->can('teams.update') || $user->can('teams.all');
    }

    /**
     * Determina si el usuario puede eliminar team.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Team $team
     * @return mixed
     */
    public function delete(User $user, Team $team)
    {
        return $user->can('teams.delete') || $user->can('teams.all');
    }

    /**
     * Determina si el usuario puede restaurar team.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Team $team
     * @return mixed
     */
    public function restore(User $user, Team $team)
    {
        return $user->can('teams.restore') || $user->can('teams.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente team.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Team $team
     * @return mixed
     */
    public function forceDelete(User $user, Team $team)
    {
        return $user->can('teams.destroy') || $user->can('teams.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
