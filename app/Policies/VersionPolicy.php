<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Version;
use Illuminate\Auth\Access\HandlesAuthorization;

class VersionPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de version.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Version $version
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('versions.all') || $user->can('versions.index');
    }

    /**
     * Determina que usuarios puede ver version.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Version $version
     * @return mixed
     */
    public function view(User $user, Version $version)
    {
        return $user->can('versions.show') || $user->can('versions.all');
    }

    /**
     * Determina que usuarios puede crear version.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('versions.create') || $user->can('versions.all');
    }

    /**
     * Determina si el usuario puede modificar version.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Version $version
     * @return mixed
     */
    public function update(User $user, Version $version)
    {
        return $user->can('versions.update') || $user->can('versions.all');
    }

    /**
     * Determina si el usuario puede eliminar version.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Version $version
     * @return mixed
     */
    public function delete(User $user, Version $version)
    {
        return $user->can('versions.delete') || $user->can('versions.all');
    }

    /**
     * Determina si el usuario puede restaurar version.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Version $version
     * @return mixed
     */
    public function restore(User $user, Version $version)
    {
        return $user->can('versions.restore') || $user->can('versions.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente version.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Version $version
     * @return mixed
     */
    public function forceDelete(User $user, Version $version)
    {
        return $user->can('versions.destroy') || $user->can('versions.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
