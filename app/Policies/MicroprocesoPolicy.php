<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Microproceso;
use Illuminate\Auth\Access\HandlesAuthorization;

class MicroprocesoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de microproceso.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Microproceso $microproceso
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('microprocesos.all') || $user->can('microprocesos.index');
    }

    /**
     * Determina que usuarios puede ver microproceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Microproceso $microproceso
     * @return mixed
     */
    public function view(User $user, Microproceso $microproceso)
    {
        return $user->can('microprocesos.show') || $user->can('microprocesos.all');
    }

    /**
     * Determina que usuarios puede crear microproceso.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('microprocesos.create') || $user->can('microprocesos.all');
    }

    /**
     * Determina si el usuario puede modificar microproceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Microproceso $microproceso
     * @return mixed
     */
    public function update(User $user, Microproceso $microproceso)
    {
        return $user->can('microprocesos.update') || $user->can('microprocesos.all');
    }

    /**
     * Determina si el usuario puede eliminar microproceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Microproceso $microproceso
     * @return mixed
     */
    public function delete(User $user, Microproceso $microproceso)
    {
        return $user->can('microprocesos.delete') || $user->can('microprocesos.all');
    }

    /**
     * Determina si el usuario puede restaurar microproceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Microproceso $microproceso
     * @return mixed
     */
    public function restore(User $user, Microproceso $microproceso)
    {
        return $user->can('microprocesos.restore') || $user->can('microprocesos.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente microproceso.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Microproceso $microproceso
     * @return mixed
     */
    public function forceDelete(User $user, Microproceso $microproceso)
    {
        return $user->can('microprocesos.destroy') || $user->can('microprocesos.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
