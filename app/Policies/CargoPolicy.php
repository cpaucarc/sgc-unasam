<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Cargo;
use Illuminate\Auth\Access\HandlesAuthorization;

class CargoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de cargo.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Cargo $cargo
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('cargos.all') || $user->can('cargos.index');
    }

    /**
     * Determina que usuarios puede ver cargo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Cargo $cargo
     * @return mixed
     */
    public function view(User $user, Cargo $cargo)
    {
        return $user->can('cargos.show') || $user->can('cargos.all');
    }

    /**
     * Determina que usuarios puede crear cargo.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('cargos.create') || $user->can('cargos.all');
    }

    /**
     * Determina si el usuario puede modificar cargo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Cargo $cargo
     * @return mixed
     */
    public function update(User $user, Cargo $cargo)
    {
        return $user->can('cargos.update') || $user->can('cargos.all');
    }

    /**
     * Determina si el usuario puede eliminar cargo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Cargo $cargo
     * @return mixed
     */
    public function delete(User $user, Cargo $cargo)
    {
        return $user->can('cargos.delete') || $user->can('cargos.all');
    }

    /**
     * Determina si el usuario puede restaurar cargo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Cargo $cargo
     * @return mixed
     */
    public function restore(User $user, Cargo $cargo)
    {
        return $user->can('cargos.restore') || $user->can('cargos.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente cargo.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Cargo $cargo
     * @return mixed
     */
    public function forceDelete(User $user, Cargo $cargo)
    {
        return $user->can('cargos.destroy') || $user->can('cargos.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
