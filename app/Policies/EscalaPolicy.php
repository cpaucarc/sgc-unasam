<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Escala;
use Illuminate\Auth\Access\HandlesAuthorization;

class EscalaPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de escala.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Escala $escala
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('escalas.all') || $user->can('escalas.index');
    }

    /**
     * Determina que usuarios puede ver escala.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Escala $escala
     * @return mixed
     */
    public function view(User $user, Escala $escala)
    {
        return $user->can('escalas.show') || $user->can('escalas.all');
    }

    /**
     * Determina que usuarios puede crear escala.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('escalas.create') || $user->can('escalas.all');
    }

    /**
     * Determina si el usuario puede modificar escala.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Escala $escala
     * @return mixed
     */
    public function update(User $user, Escala $escala)
    {
        return $user->can('escalas.update') || $user->can('escalas.all');
    }

    /**
     * Determina si el usuario puede eliminar escala.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Escala $escala
     * @return mixed
     */
    public function delete(User $user, Escala $escala)
    {
        return $user->can('escalas.delete') || $user->can('escalas.all');
    }

    /**
     * Determina si el usuario puede restaurar escala.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Escala $escala
     * @return mixed
     */
    public function restore(User $user, Escala $escala)
    {
        return $user->can('escalas.restore') || $user->can('escalas.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente escala.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Escala $escala
     * @return mixed
     */
    public function forceDelete(User $user, Escala $escala)
    {
        return $user->can('escalas.destroy') || $user->can('escalas.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
