<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Detalle;
use Illuminate\Auth\Access\HandlesAuthorization;

class DetallePolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de detalle.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Detalle $detalle
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('detalles.all') || $user->can('detalles.index');
    }

    /**
     * Determina que usuarios puede ver detalle.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Detalle $detalle
     * @return mixed
     */
    public function view(User $user, Detalle $detalle)
    {
        return $user->can('detalles.show') || $user->can('detalles.all');
    }

    /**
     * Determina que usuarios puede crear detalle.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('detalles.create') || $user->can('detalles.all');
    }

    /**
     * Determina si el usuario puede modificar detalle.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Detalle $detalle
     * @return mixed
     */
    public function update(User $user, Detalle $detalle)
    {
        return $user->can('detalles.update') || $user->can('detalles.all');
    }

    /**
     * Determina si el usuario puede eliminar detalle.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Detalle $detalle
     * @return mixed
     */
    public function delete(User $user, Detalle $detalle)
    {
        return $user->can('detalles.delete') || $user->can('detalles.all');
    }

    /**
     * Determina si el usuario puede restaurar detalle.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Detalle $detalle
     * @return mixed
     */
    public function restore(User $user, Detalle $detalle)
    {
        return $user->can('detalles.restore') || $user->can('detalles.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente detalle.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Detalle $detalle
     * @return mixed
     */
    public function forceDelete(User $user, Detalle $detalle)
    {
        return $user->can('detalles.destroy') || $user->can('detalles.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
