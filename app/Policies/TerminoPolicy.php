<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Termino;
use Illuminate\Auth\Access\HandlesAuthorization;

class TerminoPolicy
{
    use HandlesAuthorization;

    /**
     * Determina que usuarios puede ver la lista de termino.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Termino $termino
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->can('terminos.all') || $user->can('terminos.index');
    }

    /**
     * Determina que usuarios puede ver termino.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Termino $termino
     * @return mixed
     */
    public function view(User $user, Termino $termino)
    {
        return $user->can('terminos.show') || $user->can('terminos.all');
    }

    /**
     * Determina que usuarios puede crear termino.
     *
     * @param  \App\Models\Auth\Usuario  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can('terminos.create') || $user->can('terminos.all');
    }

    /**
     * Determina si el usuario puede modificar termino.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Termino $termino
     * @return mixed
     */
    public function update(User $user, Termino $termino)
    {
        return $user->can('terminos.update') || $user->can('terminos.all');
    }

    /**
     * Determina si el usuario puede eliminar termino.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Termino $termino
     * @return mixed
     */
    public function delete(User $user, Termino $termino)
    {
        return $user->can('terminos.delete') || $user->can('terminos.all');
    }

    /**
     * Determina si el usuario puede restaurar termino.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Termino $termino
     * @return mixed
     */
    public function restore(User $user, Termino $termino)
    {
        return $user->can('terminos.restore') || $user->can('terminos.all');
    }

    /**
     * Determina si el usuario puede eliminar permanentemente termino.
     *
     * @param  \App\Models\Auth\User $user
     * @param  \App\Models\Termino $termino
     * @return mixed
     */
    public function forceDelete(User $user, Termino $termino)
    {
        return $user->can('terminos.destroy') || $user->can('terminos.all');
    }

    /**
     * Overrides permisos
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->es_administrador) {
              return true;
        }
    }
}
