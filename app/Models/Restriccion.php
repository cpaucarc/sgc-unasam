<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Restriccion extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'restriccion',
        'descripcion',
        'activo',
        'created_user',
        'updated_user',
        'deleted_user'
    ];
}
