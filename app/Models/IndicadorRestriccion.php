<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndicadorRestriccion extends Model
{
    use HasFactory;
    protected $table = 'indicador_restriccion';

    protected $fillable = [
        'id',
        'nombre',
        'indicador_id',
        'restriccion_id',
        'activo',
        'created_user',
        'updated_user',
        'deleted_user',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
