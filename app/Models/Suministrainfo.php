<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Suministrainfo extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['id', 'indicador_id', 'macroproceso_id', 'proceso_id', 'subproceso_id', 'microproceso_id', 'activo', 'created_user', 'updated_user', 'deleted_user'];

    public function macroproceso()
    {
        return $this->belongsTo(Macroproceso::class, 'macroproceso_id', 'id');
    }
    public function proceso()
    {
        return $this->belongsTo(Proceso::class, 'proceso_id', 'id');
    }
    public function subproceso()
    {
        return $this->belongsTo(Subproceso::class, 'subproceso_id', 'id');
    }
    public function microproceso()
    {
        return $this->belongsTo(Microproceso::class, 'microproceso_id', 'id');
    }


    public function getNombreCompletoAttribute()
    {
        if (!!$this->microproceso_id)
            return $this->microproceso->codigo . ': ' . $this->microproceso->nombre;
        if (!!$this->subproceso_id)
            return $this->subproceso->codigo . ': ' . $this->subproceso->nombre;
        if (!!$this->proceso_id)
            return $this->proceso->codigo . ': ' . $this->proceso->nombre;

        return $this->macroproceso->codigo . ': ' . $this->macroproceso->nombre;
    }
}
