<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [];

    protected $casts = [
        'expanded' => 'boolean'
    ];

    protected $appends = ['expanded'];

    public function getExpandedAttribute()
    {
        return false;
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function permissions()
    {
        return $this->hasMany(Permission::class);
    }
}
