<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use HasFactory;
   // use SoftDeletes;

   protected $fillable = [];

   protected $casts = [
       'expanded' => 'boolean'
   ];

   protected $appends  = ['expanded'];

   public function getExpandedAttribute()
   {
       return false;
   }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }
}
