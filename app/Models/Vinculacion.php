<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Vinculacion extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'nivel_proceso',
        'proceso_id',
        'vin_nivel_proceso',
        'vin_proceso_id',
        'activo',
        'created_user',
        'updated_user',
        'deleted_user'
    ];
    public function padre()
    {

        if ($this->vin_nivel_proceso === '15001')
            return $this->belongsTo(Macroproceso::class, 'vin_proceso_id', 'id');

        if ($this->vin_nivel_proceso === '15002')
            return $this->belongsTo(Proceso::class, 'vin_proceso_id', 'id');
        return $this->belongsTo(Proceso::class, 'vin_proceso_id', 'id');
    }
    public function macroproceso()
    {
        return $this->belongsTo(Macroproceso::class, 'vin_proceso_id', 'id');
    }
}
