<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Requisito extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'id',
        'descripcion',
        'param_tiporequisito',
        'macroproceso_id',
        'proceso_id',
        'subproceso_id',
        'microproceso_id',
        'activo',
        'created_user',
        'updated_user',
        'deleted_user'
    ];
    public function tipo()
    {
        return $this->belongsTo(Detalle::class, 'param_tiporequisito', 'id');
    }


}
