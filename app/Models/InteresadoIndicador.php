<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InteresadoIndicador extends Model
{
    use HasFactory;
    protected $table = 'indicador_interesado';

    protected $fillable = [
        'id',
        'interesado_id',
        'indicador_id',
        'param_tiporesponsable',
        'activo',
        'created_user',
        'updated_user',
        'deleted_user',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
