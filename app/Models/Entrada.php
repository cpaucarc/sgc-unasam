<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use ZipStream\Option\Archive;

class Entrada extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'id',
        'controles',
        'duenio_id',
        'nivel_proceso',
        'proceso_id',
        'activo',
        'created_user',
        'updated_user',
        'deleted_user'
    ];
    public function documento()
    {
        return $this->belongsTo(Documento::class);
    }
    public function proceso()
    {
        return $this->belongsTo(Proceso::class);
    }
    public function interesado()
    {
        return $this->belongsTo(Interesado::class, 'duenio_id', 'id');
    }
    public function entrada()
    {
        return $this->belongsTo(Entrada::class, 'salida_id', 'id');
    }
    public function archivos()
    {
        return $this->belongsToMany(Archivo::class, 'entrada_archivo')->whereNull('entrada_archivo.deleted_at')->withPivot('activo', 'created_user', 'updated_user', 'deleted_user')->withTimestamps();
    }
}
