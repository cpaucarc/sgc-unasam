<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Escala extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'id',
        'param_likert',
        'meta_id',
        'valor_ini',
        'valor_fin',
        'activo',
        'created_user',
        'updated_user',
        'deleted_user',
        'created_at',
        'update_at'
    ];


    public function nivel()
    {
        return $this->belongsTo(Detalle::class, 'param_likert', 'id');
    }

    public function getRangoAttribute()
    {
        $inicial = '< ';
        if ($this->param_likert == '14001')
            $inicial = '[ ';

        return $inicial . (string)$this->valor_ini . ' - ' . (string)$this->valor_fin . ' ]';
    }
}
