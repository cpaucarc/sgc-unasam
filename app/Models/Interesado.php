<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Interesado extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['id', 'nombre', 'descripcion', 'interno', 'area_id', 'activo'];

    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id', 'id');
    }
    public function entrada()
    {
        return $this->hasOne(Entrada::class, 'duenio_id', 'id');
    }
}
