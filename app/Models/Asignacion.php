<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Spatie\Permission\Models\Role;

class Asignacion extends Model
{
    use HasFactory;
    use softDeletes;

    protected $fillable =['id',
    'user_id',
    'role_id',
    'area_id',
    'fechaIni',
    'fechaFin',
    'activo'];

    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id', 'id');
    }
    public function rol()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }
    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}

