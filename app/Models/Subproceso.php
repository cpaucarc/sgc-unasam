<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Subproceso extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'codigo',
        'nombre',
        'version_id',
        'descripcion',
        'param_tipo',
        'responsable_area',
        'objetivo',
        'alcance',
        'finalidad',
        'orden',
        'proceso_id',
        'macroproceso_id',
        'activo',
        'created_user',
        'updated_user',
        'deleted_user',
    ];


    public function getNombreCompletoAttribute()
    {
        return $this->codigo . ': ' . $this->nombre;
    }

    public function detalle()
    {
        return $this->belongsTo(Detalle::class, 'param_tipo', 'id');
    }
    public function area()
    {
        return $this->belongsTo(Area::class, 'responsable_area', 'id');
    }
    public function proceso()
    {
        return $this->belongsTo(Proceso::class);
    }
    public function archivos()
    {
        return $this->belongsToMany(Archivo::class, 'proceso_archivo', 'proceso_id', 'archivo_id')->whereNull('proceso_archivo.deleted_at')->withPivot('id', 'activo', 'created_user', 'updated_user', 'deleted_user')->withTimestamps();
    }
    public function microprocesos()
    {
        return $this->hasMany(Microproceso::class);
    }
    public function vinculacions()
    {
        return $this->hasMany(Vinculacion::class, 'proceso_id', 'id');
    }
    public function indicadors()
    {
        return $this->hasMany(Indicador::class);
    }
    public function version()
    {
        return $this->belongsTo(Version::class, 'version_id', 'id');
    }

    public function objetivos()
    {
        return $this->morphMany(Objetivo::class, 'relacion');
    }
    public function mecanismos()
    {
        return $this->morphMany(Mecanismo::class, 'relacion');
    }
}
