<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Indicador extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'codigo',
        'nombre',
        'param_tipoindicador',
        'version_id',
        'definicion',
        'objetivo',
        'es_general',
        'macroproceso_id',
        'proceso_id',
        'subproceso_id',
        'microproceso_id',
        'activo', 'id'
    ];


    public function detalle()
    {
        return $this->belongsTo(Detalle::class, 'param_tipoindicador', 'id');
    }
    public function formula()
    {
        return $this->hasOne(Formula::class);
    }

    public function version()
    {
        return $this->belongsTo(Version::class, 'version_id', 'id');
    }

    public function getVersionesAttribute()
    {
        return $this->with('version')->where('codigo', $this->codigo)->where('codigo', '!=', '')->get();
    }
    public function getVersionesCountAttribute()
    {
        return $this->versiones->count() > 1 ? $this->versiones->count() : 1;
    }

    public function macroproceso()
    {
        return $this->belongsTo(Macroproceso::class, 'macroproceso_id', 'id');
    }
    public function proceso()
    {
        return $this->belongsTo(Proceso::class, 'proceso_id', 'id');
    }
    public function subproceso()
    {
        return $this->belongsTo(Subproceso::class, 'subproceso_id', 'id');
    }
    public function microproceso()
    {
        return $this->belongsTo(Microproceso::class, 'microproceso_id', 'id');
    }
    public function medicion()
    {
        return $this->hasOne(Medicion::class);
    }
    public function meta()
    {
        return $this->hasOne(Meta::class);
    }
    public function metas()
    {
        return $this->hasMany(Meta::class);
    }
    public function resultados()
    {
        return $this->hasMany(Resultado::class);
    }
    public function interesados()
    {
        return $this->belongsToMany(Interesado::class)->withPivot('param_tiporesponsable', 'activo');
    }
    public function responsables()
    {
        return $this->belongsToMany(Interesado::class)->where('param_tiporesponsable', '11004')->withPivot('param_tiporesponsable', 'activo');
    }
    public function usuarios()
    {
        return $this->belongsToMany(Interesado::class)->where('param_tiporesponsable', '11005')->withPivot('param_tiporesponsable', 'activo');
    }
    public function suministraninfos()
    {
        return $this->hasMany(Suministrainfo::class);
    }

    public function getParametrosAttribute()
    {
        if (!!$this->microproceso_id)
            return ['nivel' => 4, 'nombre' => 'MICROPROCESO', 'descripcion' => $this->microproceso->nombre, 'codigo' => $this->microproceso->codigo];
        if (!!$this->subproceso_id)
            return ['nivel' => 3, 'nombre' => 'SUBPROCESO', 'descripcion' => $this->subproceso->nombre, 'codigo' => $this->subproceso->codigo];
        if (!!$this->proceso_id)
            return ['nivel' => 2, 'nombre' => 'PROCESO', 'descripcion' => $this->proceso->nombre, 'codigo' => $this->proceso->codigo];

        return ['nivel' => 1, 'nombre' => 'MACROPROCESO', 'descripcion' => $this->macroproceso->nombre, 'codigo' => $this->macroproceso->codigo];
    }
    public function getLongitudResponsablesInformacionAttribute()
    {
        $areas = $this->responsables->count();
        $procesos = $this->suministraninfos->count();
        $usuarios = $this->usuarios->count();
        $max = collect([$areas, $procesos, $usuarios])->max();

        return $max > 3 ? $max : 3;
    }
    public function getLongitudResponsableCalculoAttribute()
    {
        return ceil($this->longitud_responsables_informacion / 2);
    }
    public function getLongitudResponsableAnalisisAttribute()
    {
        return $this->longitud_responsables_informacion - ceil($this->longitud_responsables_informacion / 2);
    }
    public function getMetaAnioAttribute()
    {
        return $this->meta;
    }
    public function getEscalasAttribute()
    {
        $escalas = [
            'muy_malo' => '',
            'malo' => '',
            'regular' => '',
            'satisfactorio' => '',
            'muy_satisfactorio' => '',
        ];

        if (!$this->meta_anio)
            return $escalas;


        if (!$this->meta_anio->escalas)
            return $escalas;

        $escalas['muy_malo'] = $this->meta_anio->escalas->where('param_likert', '14001')->first() ? $this->meta_anio->escalas->where('param_likert', '14001')->first()->rango : '';
        $escalas['malo'] = $this->meta_anio->escalas->where('param_likert', '14002')->first() ? $this->meta_anio->escalas->where('param_likert', '14002')->first()->rango : '';
        $escalas['regular'] = $this->meta_anio->escalas->where('param_likert', '14003')->first() ? $this->meta_anio->escalas->where('param_likert', '14003')->first()->rango : '';
        $escalas['satisfactorio'] = $this->meta_anio->escalas->where('param_likert', '14004')->first() ? $this->meta_anio->escalas->where('param_likert', '14004')->first()->rango : '';
        $escalas['muy_satisfactorio'] = $this->meta_anio->escalas->where('param_likert', '14005')->first() ? $this->meta_anio->escalas->where('param_likert', '14005')->first()->rango : '';

        return $escalas;
    }
}
