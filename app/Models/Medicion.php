<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medicion extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'param_und_medida',
        'numerador',
        'denominador',
        'param_frec_medicion',
        'param_frec_reporte',
        'param_frec_revision',
        'indicador_id',
        //'anio_id',
        'param_periodo',
        'activo'
    ];

    public function detallemedida()
    {
        return $this->belongsTo(Detalle::class, 'param_und_medida', 'id');
    }

    public function detallemedicion()
    {
        return $this->belongsTo(Detalle::class, 'param_frec_medicion', 'id');
    }
    public function detallereporte()
    {
        return $this->belongsTo(Detalle::class, 'param_frec_reporte', 'id');
    }
    public function detallerevision()
    {
        return $this->belongsTo(Detalle::class, 'param_frec_revision', 'id');
    }
    public function indicador()
    {
        return $this->belongsTo(Indicador::class, 'indicador_id', 'id');
    }
    // public function anio()
    // {
    //     return $this->belongsTo(Anio::class, 'anio_id', 'id');
    // }
    public function periodo()
    {
        return $this->belongsTo(Detalle::class, 'param_periodo', 'id');
    }
    public function frecuencia()
    {
        return $this->belongsTo(Detalle::class, 'param_frec_medicion', 'id');
    }
    public function unidad()
    {
        return $this->belongsTo(Detalle::class, 'param_und_medida', 'id');
    }
}
