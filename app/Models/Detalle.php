<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Detalle extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $fillable = ['id', 'parametro_id', 'detalle', 'abreviatura', 'descripcion', 'activo', 'created_user', 'updated_user', 'deleted_user'];
}
