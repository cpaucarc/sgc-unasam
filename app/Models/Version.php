<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Version extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'numero',
        'fecha_vigencia',
        'descripcion',
        'per_elaboro',
        'per_reviso',
        'per_aprobado',
        'fecha_aprobacion',
        'activo',
        'created_user',
        'updated_user',
        'deleted_user',
        'nivel_proceso'
    ];

    protected $dates = [
        'fecha_vigencia'
    ];

    public function persona_elaboro()
    {
        return $this->belongsTo(Persona::class, 'per_elaboro', 'id');
    }
    public function persona_reviso()
    {
        return $this->belongsTo(Persona::class, 'per_reviso', 'id');
    }
    public function persona_aprobo()
    {
        return $this->belongsTo(Persona::class, 'per_aprobado', 'id');
    }
    public function archivo()
    {
        return $this->hasOne(Archivo::class, 'version', 'id');
    }
    public function detalle_nivel()
    {
        return $this->belongsTo(Detalle::class, 'nivel_proceso', 'id');
    }
}
