<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Variable extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'tipo',
        'variable',
        'nombre',
        'api_url',
        'formula_id'
    ];

    protected $casts = [
    ];


    public function formula (){
        return $this->belongsTo(Formula::class);
    }

    public function resultados()
    {
        return $this->hasMany(ResultadoVariable::class);
    }
    public function resultado()
    {
        return $this->hasOne(ResultadoVariable::class);
    }
}
