<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trabajador extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $fillable = [
        'id', 
        'fecha_inicio',
        'fecha_fin', 
        'persona_id', 
        'cargo_id', 
        'area_id', 
        'activo', 
        'created_user', 
        'updated_user', 
        'deleted_user', 
        'created_at', 
        'update_at'];
}
