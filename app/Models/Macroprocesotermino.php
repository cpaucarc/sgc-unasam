<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Macroprocesotermino extends Model
{
    use HasFactory; 
    use SoftDeletes; 
    protected $table = 'macroproceso_termino';
    protected $fillable =[
        'macroproceso_id',
        'termino_id',
        'activo'
    ];
    public function macroproceso()
    {
        return $this->belongsTo(Macroproceso::class, 'macroproceso_id', 'id');
    }
    public function termino()
    {
        return $this->belongsTo(Termino::class, 'termino_id', 'id');
    }
  
}
