<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cargo extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $fillable = ['id', 'nombre', 'descripcion', 'activo', 'created_user', 'updated_user', 'deleted_user'];
}
