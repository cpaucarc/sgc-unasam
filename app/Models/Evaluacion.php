<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Evaluacion extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['id', 'param_likert','resultado_id', 'propuesta_mejora', 'fecha_propuesta', 'fecha_cumplimiento', 'activo', 'created_user', 'updated_user', 'deleted_user', 'created_at', 'update_at'];
}
