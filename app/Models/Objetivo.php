<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Objetivo extends Model
{
    use SoftDeletes;
    use HasFactory;

    public $fillable = [
        'nombre', 'relacion_id', 'relacion_type', 'created_user', 'mecanismo_id'
    ];

    public function evidencias()
    {
        return $this->hasMany(Evidencia::class);
    }
}
