<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Evidencia extends Model
{
    use SoftDeletes;
    use HasFactory;

    public $fillable = [
        'nombre', 'documento_id', 'created_user'
    ];

    public function documento(){
        return $this->belongsTo(Documento::class);
    }
}
