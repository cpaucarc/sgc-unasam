<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use ZipStream\Option\Archive;

class Macroproceso extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['id', 'codigo', 'nombre', 'version_id', 'descripcion', 'param_tipo', 'responsable_area', 'objetivo', 'alcance', 'finalidad', 'orden', 'activo', 'tiene_api'];



    public $casts = [
        'tiene_api' => 'boolean'
    ];

    public function getNombreCompletoAttribute()
    {
        return $this->codigo . ': ' . $this->nombre;
    }
    public function getNivelAttribute()
    {
        return 1;
    }

    public function getLongitudRequisitosAttribute()
    {
        return 3;
        return $this->clientes->count() + $this->isos->count() + $this->legales->count();
    }
    public function getLongitudRecursosAttribute()
    {
        $longitudes = collect();

        $longitudes->push(ceil($this->talentos_humanos->count() / 2));
        $longitudes->push($this->otros_recursos->count());
        $longitudes->push(ceil($this->infraestructuras->count() / 2));
        $longitudes->push(ceil($this->softwares->count() / 2));

        return $longitudes->max();
    }

    public function procesos()
    {
        return $this->hasMany(Proceso::class);
    }
    public function hijos()
    {
        return $this->hasMany(Proceso::class);
    }

    public function subentradas()
    {
        return $this->hasManyThrough(Entrada::class, Proceso::class)->where('salida', 0);
    }

    public function detalle()
    {
        return $this->belongsTo(Detalle::class, 'param_tipo', 'id');
    }
    public function area()
    {
        return $this->belongsTo(Area::class, 'responsable_area', 'id');
    }
    public function version()
    {
        return $this->belongsTo(Version::class, 'version_id', 'id');
    }

    public function getVersionesAttribute()
    {
        return $this->with('version')->where('codigo', $this->codigo)->where('codigo', '!=', '')->get();
    }

    public function getVersionesCountAttribute()
    {
        return $this->versiones->count() > 1 ? $this->versiones->count() : 1;
    }

    public function archivos()
    {
        return $this->belongsToMany(Archivo::class, 'proceso_archivo', 'proceso_id', 'archivo_id')->whereNull('proceso_archivo.deleted_at')->withPivot('id', 'activo', 'created_user', 'updated_user', 'deleted_user')->withTimestamps();
    }
    public function vinculacions()
    {
        return $this->hasMany(Vinculacion::class, 'proceso_id', 'id');
    }
    public function entradas()
    {
        return $this->hasMany(Entrada::class, 'proceso_id', 'id');
    }
    public function recursos()
    {
        return $this->belongsToMany(Recurso::class, 'proceso_recurso', 'proceso_id', 'recurso_id')->whereNull('proceso_recurso.deleted_at')->withPivot('id', 'activo', 'created_user', 'updated_user', 'deleted_user')->withTimestamps();
    }
    public function indicadors()
    {
        return $this->hasMany(Indicador::class);
    }
    public function terminos()
    {
        return $this->belongsToMany(Termino::class)->whereNull('macroproceso_termino.deleted_at')->withPivot('activo', 'created_user', 'updated_user', 'deleted_user')->withTimestamps();
    }


    public function requisitos()
    {
        return $this->hasMany(Requisito::class);
    }
    public function clientes()
    {
        return $this->hasMany(Requisito::class)->where('param_tiporequisito', 7001)->whereNull('proceso_id');
    }
    public function isos()
    {
        return $this->hasMany(Requisito::class)->where('param_tiporequisito', 7002)->whereNull('proceso_id');
    }
    public function legales()
    {
        return $this->hasMany(Requisito::class)->where('param_tiporequisito', 7003)->whereNull('proceso_id');
    }

    public function talentos_humanos()
    {
        return $this->belongsToMany(Recurso::class, 'proceso_recurso', 'proceso_id', 'recurso_id')
            ->where('nivel_proceso', '15001')->where('param_tiporecurso', '9001')
            ->whereNull('proceso_recurso.deleted_at')->withPivot('id', 'activo', 'created_user', 'updated_user', 'deleted_user')->withTimestamps();
    }

    public function otros_recursos()
    {
        return $this->belongsToMany(Recurso::class, 'proceso_recurso', 'proceso_id', 'recurso_id')
            ->where('nivel_proceso', '15001')->where('param_tiporecurso', '9004')
            ->whereNull('proceso_recurso.deleted_at')->withPivot('id', 'activo', 'created_user', 'updated_user', 'deleted_user')->withTimestamps();
    }

    public function infraestructuras()
    {
        return $this->belongsToMany(Recurso::class, 'proceso_recurso', 'proceso_id', 'recurso_id')
            ->where('nivel_proceso', '15001')->where('param_tiporecurso', '9002')
            ->whereNull('proceso_recurso.deleted_at')->withPivot('id', 'activo', 'created_user', 'updated_user', 'deleted_user')->withTimestamps();
    }

    public function softwares()
    {
        return $this->belongsToMany(Recurso::class, 'proceso_recurso', 'proceso_id', 'recurso_id')
            ->where('nivel_proceso', '15001')->where('param_tiporecurso', '9003')
            ->whereNull('proceso_recurso.deleted_at')->withPivot('id', 'activo', 'created_user', 'updated_user', 'deleted_user')->withTimestamps();
    }

    public function objetivos()
    {
        return $this->morphMany(Objetivo::class, 'relacion');
    }
    public function mecanismos()
    {
        return $this->morphToMany(Mecanismo::class, 'relacion', 'asignacion_mecanismos');
    }
}
