<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Archivo extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'id',
        'nombre',
        'descripcion',
        'anio_id',
        'version',
        'ruta',
        'extension',
        'fechaIniVigencia',
        'fechaFinVigencia',
        'param_periodo',
        'semanal_mes',
        'documento_id',
        'activo',
        'created_user',
        'updated_user',
        'deleted_user'
    ];

    public function macroprocesos()
    {
        return $this->belongsToMany(Macroproceso::class);
    }
    public function procesos()
    {
        return $this->belongsToMany(Proceso::class);
    }
    public function subprocesos()
    {
        return $this->belongsToMany(Subproceso::class);
    }
    public function microprocesos()
    {
        return $this->belongsToMany(Microproceso::class);
    }
    public function documento()
    {
        return $this->belongsTo(Documento::class);
    }
    public function anio()
    {
        return $this->belongsTo(Anio::class);
    }
    public function ver_version()
    {
        return $this->belongsTo(Version::class, 'version', 'id');
    }
    public function detalle()
    {
        return $this->belongsTo(Detalle::class, 'param_periodo', 'id');
    }
    public function periodo()
    {
        return $this->belongsTo(Detalle::class, 'param_periodo', 'id');
    }
    public function semana()
    {
        return $this->belongsTo(Detalle::class, 'semanal_mes', 'id');
    }
}
