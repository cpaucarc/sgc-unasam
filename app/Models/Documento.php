<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documento extends Model
{
    use SoftDeletes;
    use HasFactory;
    //protected $table = "documentos";
    protected $fillable = [
        'id',
        'nombreReferencial',
        'param_tipodocumento',
        'descripcion',
        'es_interno',
        'area_id',
        'general',
        'param_conservacion',
        'placeholder',
        'activo',
        'created_user',
        'updated_user',
        'deleted_user',
        'created_at',
        'update_at'
    ];

    public function entrada()
    {
        return $this->hasOne(Entrada::class);
    }
    public function archivo()
    {
        return $this->hasOne(Archivo::class);
    }
    public function archivos()
    {
        return $this->hasMany(Archivo::class);
    }
    public function area()
    {
        return $this->belongsTo(Area::class);
    }
    public function tipodocumento()
    {
        return $this->belongsTo(Detalle::class, 'param_tipodocumento', 'id');
    }
    public function conservacion()
    {
        return $this->belongsTo(Detalle::class, 'param_conservacion', 'id');
    }
}
