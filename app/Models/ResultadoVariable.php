<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResultadoVariable extends Model
{
    use HasFactory;

    protected $table = "resultado_variable";
    protected $fillable = ['valor', 'resultado_id'];


    public function variable()
    {
        return $this->belongsTo(Variable::class);
    }
}
