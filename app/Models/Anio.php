<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Anio extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'id',
        'nombre',
        'anio',
        'activo',
        'created_user',
        'updated_user',
        'deleted_user'
    ];

    public function archivo()
    {
        return $this->hasOne(Archivo::class);
    }
}
