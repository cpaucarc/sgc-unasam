<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resultado extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['id', 'param_periodo','valor_numerador', 'valor_denominador', 'valor_indicador', 'indicador_id', 'anio_id', 'activo', 'created_user', 'updated_user', 'deleted_user', 'created_at', 'update_at'];

    public function detalleperiodo()
    {
        return $this->belongsTo(Detalle::class, 'param_periodo', 'id');
    }
    public function evaluacion()
    {
        return $this->hasOne(Evaluacion::class, 'resultado_id', 'id');
    }


}
