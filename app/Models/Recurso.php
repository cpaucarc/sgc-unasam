<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Recurso extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'id', 
        'nombre', 
        'param_tiporecurso', 
        'activo', 
        'created_user',
        'updated_user',
        'deleted_user'] ;

    public function detalle()
    {
        return $this->belongsTo(Detalle::class, 'param_tiporecurso', 'id');
    }
    public function macroprocesos()
    {
        return $this->belongsToMany(Macroproceso::class);
    }
}
