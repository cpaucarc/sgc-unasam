<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $fillable = [
        'id',
        'nombre',
        'abreviatura',
        'area_id',
        'param_nivel',
        'param_tipoorgano',
        'activo',
        'created_user',
        'updated_user',
        'deleted_user'
    ];

    public function documento()
    {
        return $this->hasOne(Documento::class);
    }
}
