<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mecanismo extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['id', 'nombre', 'descripcion', 'param_tipomecanismo', 'activo'];

    public function detalle()
    {
        return $this->belongsTo(Detalle::class, 'param_tipomecanismo', 'id');
    }

    public function objetivos()
    {
        return $this->hasMany(Objetivo::class);
    }
}
