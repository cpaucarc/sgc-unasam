<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Meta extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'id',
        'meta',
        'descripción',
        'param_periodo',
        'anio_id',
        'indicador_id',
        'ascendente',
        'activo'
    ];
    public function detalleperiodo()
    {
        return $this->belongsTo(Detalle::class, 'param_periodo', 'id');
    }
    public function indicador()
    {
        return $this->belongsTo(Indicador::class, 'indicador_id', 'id');
    }
    public function anio()
    {
        return $this->belongsTo(Anio::class, 'anio_id', 'id');
    }
    public function escalas()
    {
        return $this->hasMany(Escala::class, 'meta_id', 'id');
    }
}
