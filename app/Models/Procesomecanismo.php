<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Procesomecanismo extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable =[
    'objetivo',
    'mecanismo_id',
    'macroproceso_id',
    'proceso_id',
    'subproceso_id',
    'microproceso_id',
    'activo'];

    public function mecanismo()
    {
        return $this->belongsTo(Mecanismo::class,'mecanismo_id','id');
    }
    public function macroproceso()
    {
        return $this->belongsTo(Macroproceso::class,'macroproceso_id','id');
    }
    public function proceso()
    {
        return $this->belongsTo(Proceso::class,'proceso_id','id');
    }
    public function subproceso()
    {
        return $this->belongsTo(Subproceso::class,'subproceso_id','id');
    }
    public function microproceso()
    {
        return $this->belongsTo(Microproceso::class,'microproceso_id','id');
    }



}
