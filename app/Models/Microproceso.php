<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Microproceso extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'id', 'codigo', 'nombre', 'version_id', 'descripcion', 'param_tipo', 'responsable_area', 'objetivo', 'alcance', 'finalidad', 'subproceso_id',
        'proceso_id', 'macroproceso_id', 'orden', 'nivelfinal', 'activo'
    ];


    public function getNombreCompletoAttribute()
    {
        return $this->codigo . ': ' . $this->nombre;
    }

    public function detalle()
    {
        return $this->belongsTo(Detalle::class, 'param_tipo', 'id');
    }
    public function area()
    {
        return $this->belongsTo(Area::class, 'responsable_area', 'id');
    }
    public function subproceso()
    {
        return $this->belongsTo(Proceso::class, 'proceso_id', 'id');
    }
    public function version()
    {
        return $this->belongsTo(Version::class, 'version_id', 'id');
    }
    public function archivos()
    {
        return $this->belongsToMany(Archivo::class, 'proceso_archivo', 'proceso_id', 'archivo_id')->whereNull('proceso_archivo.deleted_at')->withPivot('id', 'activo', 'created_user', 'updated_user', 'deleted_user')->withTimestamps();
    }
    public function vinculacions()
    {
        return $this->hasMany(Vinculacion::class, 'proceso_id', 'id');
    }
    public function indicadors()
    {
        return $this->hasMany(Indicador::class);
    }

    public function objetivos()
    {
        return $this->morphMany(Objetivo::class, 'relacion');
    }
    public function mecanismos()
    {
        return $this->morphToMany(Mecanismo::class, 'asignacion_mecanismo');
    }
}
