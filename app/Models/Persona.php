<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Persona extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'id',
        'nombres',
        'ape_paterno',
        'ape_materno',
        'nro_documento',
        'param_tipodoc',
        'direccion',
        'celular',
        'email',
        'fecha_nac',
        'genero',
        'ubigeo',
        'param_persona',
        'activo',
        'created_user',
        'updated_user',
        'deleted_user'
    ];
}
