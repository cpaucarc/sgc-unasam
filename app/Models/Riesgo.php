<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Riesgo extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'id',
        'nombre',
        'descripcion',
        'nivel_proceso',
        'proceso_id',
        'activo',
        'created_user',
        'updated_user',
        'deleted_user'
    ];
}
