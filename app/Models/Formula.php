<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Formula extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['numerador', 'denominador', 'indicador_id'];

    public function variables()
    {
        return $this->hasMany(Variable::class);
    }

    public function getFormulaNumeradorSimpleAttribute()
    {
        $formula_numerador = $this->numerador;
        foreach ($this->variables()->whereTipo('numerador')->get() as $vn) {
            $formula_numerador = str_replace($vn['variable'], $vn['variable'] . '#&', $formula_numerador);
        }
        foreach ($this->variables()->whereTipo('numerador')->get() as $vn) {
            $formula_numerador = str_replace($vn['variable'] . '#&', '(' . $vn['nombre'] . ')', $formula_numerador);
        }

        return $formula_numerador;
    }
    public function getFormulaDenominadorSimpleAttribute()
    {
        $formula_denominador = $this->denominador;

        foreach ($this->variables()->whereTipo('denominador')->get() as $vd) {
            $formula_denominador = str_replace($vd['variable'], $vd['variable'] . '#&', $formula_denominador);
        }
        foreach ($this->variables()->whereTipo('denominador')->get() as $vd) {
            $formula_denominador = str_replace($vd['variable'] . '#&', '(' . $vd['nombre'] . ')', $formula_denominador);
        }

        return $formula_denominador;
    }


    public function getFormulaNumeradorAttribute()
    {
        return str_replace(' ', '\ ', $this->formula_numerador_simple);

    }
    public function getFormulaDenominadorAttribute()
    {
        return str_replace(' ', '\ ', $this->formula_denominador_simple);
    }

}
