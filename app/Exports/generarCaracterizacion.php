<?php

namespace App\Exports;

use App\Models\Macroproceso;
use App\Models\Microproceso;
use App\Models\Proceso;
use App\Models\Requisito;
use App\Models\Subproceso;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;

use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

class generarCaracterizacion implements FromCollection, WithMapping, ShouldAutoSize, WithTitle, WithEvents, WithCustomStartCell
{
    /**
     * @return \Illuminate\Support\Collection
     */

    private $item;
    private $nivel_proceso;
    private $proceso_id;
    private $macroproceso;
    private $proceso;
    private $subproceso;
    private $microproceso;
    private $dato;
    private $tipo_proceso;
    private $vinculacions;
    private $entradas;
    /*recurso*/
    private $talentos = [];
    private $infraestructuras = [];
    private $softwares = [];
    private $recursos = [];

    public function __construct(int $nivel, int $proceso)
    {
        $this->nivel_proceso = $nivel;
        $this->proceso_id = $proceso;
        $this->item = 1;
        switch ($this->nivel_proceso) {
            case 15001:
                $this->dato = Macroproceso::find($this->proceso_id);
                $this->tipo_proceso = 'MACROPROCESO';
                $this->vinculacions = Macroproceso::find($this->proceso_id)->vinculacions()->where('nivel_proceso', 15001)->orderBy('vin_proceso_id', 'asc')->get();
                $this->entradas = Proceso::join('entradas', 'entradas.proceso_id', '=', 'procesos.id')
                    ->join('documentos', 'documentos.id', '=', 'entradas.documento_id')
                    ->join('interesados', 'interesados.id', '=', 'entradas.duenio_id')
                    ->where('entradas.salida', 0)
                    ->where('procesos.macroproceso_id', $this->proceso_id)
                    ->select('entradas.id as entrada_id', 'entradas.salida_id', 'entradas.documento_id', 'interesados.nombre as interesado', 'procesos.codigo', 'procesos.nombre', 'documentos.nombreReferencial')
                    ->get();
                $this->talentos = Macroproceso::find($this->proceso_id)->recursos()->where('nivel_proceso', '15001')->where('param_tiporecurso', '9001')->get();
                $this->infraestructuras = Macroproceso::find($this->proceso_id)->recursos()->where('nivel_proceso', '15001')->where('param_tiporecurso', '9002')->get();
                $this->softwares = Macroproceso::find($this->proceso_id)->recursos()->where('nivel_proceso', '15001')->where('param_tiporecurso', '9003')->get();
                $this->recursos = Macroproceso::find($this->proceso_id)->recursos()->where('nivel_proceso', '15001')->where('param_tiporecurso', '9004')->get();
                break;
            case 15002:
                $this->dato = Proceso::find($this->proceso_id);
                $this->tipo_proceso = 'PROCESO';
                break;
            case 15003:
                $this->dato = Subproceso::find($this->proceso_id);
                $this->tipo_proceso = 'SUBPROCESO';
                break;
            case 15004:
                $this->dato = Microproceso::find($this->proceso_id);
                $this->tipo_proceso = 'MICROPROCESO';
                break;
        }
    }

    public function collection()
    {
        return $this->dato;
    }

    public function map($dato): array
    {
        return [
            $this->item++
        ];
    }

    public function registerEvents(): array
    {
        return [
            // BeforeExport::class => function (BeforeExport $event) {
            //     $event->writer->getProperties()->setCreator('Oficina General de Admision');
            //     $event->writer->getProperties()->setTitle("IngresantesAdmision" . $this->nomAdmision);
            // },
            AfterSheet::class => function (AfterSheet $event) {

                $negrita = [
                    'font' => [
                        'bold' => true,
                    ],
                ];
                $header = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                    'font' => [
                        'bold' => true,
                    ],
                ];
                $centrar = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ]
                ];
                $italic = [
                    'font' => [
                        'italic' => true,
                    ]
                ];
                $tamanio = [
                    'font' => [
                        'size' => 16,
                    ]
                ];
                $border = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                ];

                $centrar_vertical = [
                    'alignment' => [
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ]
                ];

                /*StartConfigutacion*/
                // $event->sheet->getDelegate()->getHeaderFooter()->setOddHeader('&C&H&BAPROBADO CON RESOLUCIÓN DE CONSEJO UNIVERSITARIO N°    -2019-UNASAM');
                // $event->sheet->getDelegate()->getHeaderFooter()->setOddFooter('&C&HPágina &P de &N');
                $event->sheet->getDelegate()->getPageSetup()->setHorizontalCentered(true);
                $event->sheet->getDelegate()->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
                $event->sheet->getDelegate()->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
                $event->sheet->getDelegate()->getPageSetup()->setPrintArea('A1:E' . ($this->item + 4));
                $event->sheet->getDelegate()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 3);
                /*EndConfigutacion*/

                $event->sheet->setCellValue('A1', 'SISTEMA DE GESTIÓN DE CALIDAD (SGC-UNASAM)');
                $event->sheet->getDelegate()->getStyle('A1:J1')->applyFromArray($tamanio);
                $event->sheet->getDelegate()->getStyle('A1:J1')->applyFromArray($negrita);
                $event->sheet->getDelegate()->mergeCells('A1:J1');
                $event->sheet->getDelegate()->getStyle('A1:J1')->applyFromArray($centrar);

                $event->sheet->setCellValue('A2', 'FICHA DE CARACTERIZACIÓN DE PROCESOS - NIVEL MACROPROCESO');
                $event->sheet->getDelegate()->getStyle('A2:J2')->applyFromArray($tamanio);
                $event->sheet->getDelegate()->getStyle('A2:J2')->applyFromArray($negrita);
                $event->sheet->getDelegate()->mergeCells('A2:J2');
                $event->sheet->getDelegate()->getStyle('A2:J2')->applyFromArray($centrar);

                $columna3 = [
                    'NOMBRE ' . $this->tipo_proceso . ':', $this->dato->nombre,
                    'TIPO:', $this->dato->detalle->detalle,
                    'CODIGO:', $this->dato->codigo,
                    'VERSIÓN:', '001',
                    'VIGENCIA:', $this->dato->version->fecha_vigencia
                ];
                $event->sheet->getDelegate()->fromArray($columna3, NULL, 'A3');

                $event->sheet->getDelegate()->getStyle('A3:J3')->applyFromArray($header);
                $event->sheet->getDelegate()->getStyle('A3:J3')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('99a3a4');

                $columna4 = [
                    '1. OBJETIVO:', $this->dato->objetivo,
                ];
                $event->sheet->getDelegate()->fromArray($columna4, NULL, 'A4');
                $event->sheet->getDelegate()->mergeCells('B4:J4');

                $inicia = $this->dato->procesos()->orderBy('orden', 'asc')->first();
                $termina = $this->dato->procesos()->orderBy('orden', 'desc')->first();
                $columna5 = [
                    '2. ALCANCE:',
                    'INICIA:', $inicia->codigo . ' ' . $inicia->nombre, '', '', '',
                    'TERMINA:', $termina->codigo . ' ' . $termina->nombre, '', ''
                ];
                $event->sheet->getDelegate()->fromArray($columna5, NULL, 'A5');
                $event->sheet->getDelegate()->mergeCells('C5:F5');
                $event->sheet->getDelegate()->mergeCells('H5:J5');

                $columna6 = [
                    '3. LÍDER DEL MACROPROCESO:', $this->dato->area->nombre,
                ];
                $event->sheet->getDelegate()->fromArray($columna6, NULL, 'A6');
                $event->sheet->getDelegate()->mergeCells('B6:J6');

                $event->sheet->getDelegate()->getStyle('A3:J6')->applyFromArray($border);

                $columna7 = [
                    '4. REQUISITOS', 'DEL CLIENTE:', 'Precisados en el ítem 4.1 del manual ' . $this->dato->codigo . ': ' . $this->dato->nombre
                ];
                $event->sheet->getDelegate()->fromArray($columna7, NULL, 'A7');
                $event->sheet->getDelegate()->mergeCells('C7:J7');
                $event->sheet->getDelegate()->getStyle('A7:J7')->applyFromArray($border);

                $columna8 = [
                    '4. REQUISITOS', 'NORMA ISO:', 'Precisados en el ítem 4.2 del manual ' . $this->dato->codigo . ': ' . $this->dato->nombre
                ];
                $event->sheet->getDelegate()->fromArray($columna8, NULL, 'A8');
                $event->sheet->getDelegate()->mergeCells('C8:J8');
                $event->sheet->getDelegate()->getStyle('A8:J8')->applyFromArray($border);

                $columna9 = [
                    '4. REQUISITOS', 'NORMA ISO:', 'Precisados en el ítem 4.3 del manual ' . $this->dato->codigo . ': ' . $this->dato->nombre
                ];
                $event->sheet->getDelegate()->fromArray($columna9, NULL, 'A9');
                $event->sheet->getDelegate()->mergeCells('C9:J9');
                $event->sheet->getDelegate()->getStyle('A9:J9')->applyFromArray($border);

                $event->sheet->getDelegate()->mergeCells('A7:A9');
                $event->sheet->getDelegate()->getStyle('A7')->applyFromArray($centrar_vertical);

                $item_vinculacion = 9;
                foreach ($this->vinculacions as $vinculacion) {
                    $item_vinculacion++;
                    $columna_vinculacion = [
                        '5. ' . $this->tipo_proceso . 'S DIRECTAMENTE VINCULADOS:', getNombreProceso($vinculacion->nivel_proceso, $vinculacion->vin_proceso_id)
                    ];
                    $event->sheet->getDelegate()->fromArray($columna_vinculacion, NULL, 'A' . $item_vinculacion);
                    $event->sheet->getDelegate()->mergeCells('B' . $item_vinculacion . ':' . 'J' . $item_vinculacion);
                    $event->sheet->getDelegate()->getStyle('A' . $item_vinculacion . ':J' . $item_vinculacion)->applyFromArray($border);
                }
                $event->sheet->getDelegate()->mergeCells('A10:' . 'A' . $item_vinculacion);
                $event->sheet->getDelegate()->getStyle('A10')->applyFromArray($centrar_vertical);

                $descripcion = [
                    '6. DESCRIPCIÓN:', $this->dato->descripcion,
                ];
                $event->sheet->getDelegate()->fromArray($descripcion, NULL, 'A' . ($item_vinculacion + 1));
                $event->sheet->getDelegate()->getStyle('A' . ($item_vinculacion + 1) . ':J' . ($item_vinculacion + 1))->applyFromArray($border);

                $entrada = [
                    'PROVEEDOR', 'ENTRADA', 'PROCESOS', '', '', '', '', '', 'SALIDA', 'CLIENTE'
                ];
                $event->sheet->getDelegate()->fromArray($entrada, NULL, 'A' . ($item_vinculacion + 1));
                $event->sheet->getDelegate()->mergeCells('C' . ($item_vinculacion + 1) . ':H' . ($item_vinculacion + 1));
                $event->sheet->getDelegate()->getStyle('A' . ($item_vinculacion + 1) . ':J' . ($item_vinculacion + 1))->applyFromArray($negrita);
                $event->sheet->getDelegate()->getStyle('A' . ($item_vinculacion + 1) . ':J' . ($item_vinculacion + 1))->applyFromArray($centrar);

                $item_entrada = $item_vinculacion + 1;

                foreach ($this->entradas as $entrada) {
                    $item_entrada++;
                    $salida = salida($entrada->salida_id);
                    $columna_entrada = [
                        $entrada->interesado, $entrada->nombreReferencial, $entrada->codigo . ': ' . $entrada->nombre, '', '', '', '', '', $salida->documento->nombreReferencial, $salida->interesado->nombre
                    ];
                    $event->sheet->getDelegate()->fromArray($columna_entrada, NULL, 'A' . $item_entrada);
                    $event->sheet->getDelegate()->mergeCells('C' . $item_entrada . ':H' . $item_entrada);
                    $event->sheet->getDelegate()->getStyle('A' . $item_entrada . ':J' . $item_entrada)->applyFromArray($border);
                }

                $item_recurso = $item_entrada + 1;
                $recurso = [
                    '7. RECURSOS'
                ];
                $event->sheet->getDelegate()->fromArray($recurso, NULL, 'A' . $item_recurso);
                $event->sheet->getDelegate()->mergeCells('A' . $item_recurso . ':J' . $item_recurso);
                $event->sheet->getDelegate()->getStyle('A' . $item_recurso . ':J' . $item_recurso)->applyFromArray($negrita);
                $event->sheet->getDelegate()->getStyle('A' . $item_recurso . ':J' . $item_recurso)->applyFromArray($border);

                $item_tp_recurso = $item_recurso + 1;
                $tp_recurso = [
                    'Talento Humano', '', 'Otros Recursos', '', 'Infraestructura', '', '', '', 'Software y Comunicaciones'
                ];
                $event->sheet->getDelegate()->fromArray($tp_recurso, NULL, 'A' . $item_tp_recurso);
                $event->sheet->getDelegate()->mergeCells('A' . $item_tp_recurso . ':B' . $item_tp_recurso);
                $event->sheet->getDelegate()->mergeCells('C' . $item_tp_recurso . ':D' . $item_tp_recurso);
                $event->sheet->getDelegate()->mergeCells('E' . $item_tp_recurso . ':H' . $item_tp_recurso);
                $event->sheet->getDelegate()->mergeCells('I' . $item_tp_recurso . ':J' . $item_tp_recurso);
                $event->sheet->getDelegate()->getStyle('A' . $item_tp_recurso . ':J' . $item_tp_recurso)->applyFromArray($negrita);
                $event->sheet->getDelegate()->getStyle('A' . $item_tp_recurso . ':J' . $item_tp_recurso)->applyFromArray($border);
                $event->sheet->getDelegate()->getStyle('A' . $item_tp_recurso . ':J' . $item_tp_recurso)->applyFromArray($centrar);

                $cantidad_recursos = [];
                $num_talento = count($this->talentos);
                array_push($cantidad_recursos, $num_talento);
                $num_infra = count($this->infraestructuras);
                array_push($cantidad_recursos, $num_infra);
                $num_sw = count($this->softwares);
                array_push($cantidad_recursos, $num_sw);
                $num_recurso = count($this->recursos);
                array_push($cantidad_recursos, $num_recurso);

                $num_filas = intval(round(max($cantidad_recursos) / 2));
                // dd($num_filas);
                $item_talento = $item_tp_recurso;
                if ($num_talento > 0) {
                    if ($num_talento < $num_filas) {
                        foreach ($this->talentos as $talento) {
                            $item_talento++;
                            $columna_talento = [
                                $talento->nombre
                            ];
                            $event->sheet->getDelegate()->fromArray($columna_talento, NULL, 'A' . $item_talento);
                        }
                    } else {
                        $ind_talento = 0;
                        for ($i = 0; $i < $num_filas; $i++) {
                            $item_talento++;
                            $columna_talento = [
                                $this->talentos[$i]->nombre
                            ];
                            $event->sheet->getDelegate()->fromArray($columna_talento, NULL, 'A' . $item_talento);
                            $ind_talento++;
                        }
                        $item_talento = $item_tp_recurso;
                        for ($i = $ind_talento; $i < $num_talento; $i++) {
                            $item_talento++;
                            $columna_talento = [
                                $this->talentos[$i]->nombre
                            ];
                            $event->sheet->getDelegate()->fromArray($columna_talento, NULL, 'B' . $item_talento);
                        }
                    }
                }

                $item_otros_r = $item_tp_recurso;
                if ($num_recurso > 0) {
                    foreach ($this->recursos as $recurso) {
                        $item_otros_r++;
                        $columna_talento = [
                            $recurso->nombre, ''
                        ];
                        $event->sheet->getDelegate()->fromArray($columna_talento, NULL, 'C' . $item_otros_r);
                        $event->sheet->getDelegate()->mergeCells('C' . $item_otros_r . ':D' . $item_otros_r);
                    }
                }
                // $event->sheet->getDelegate()->mergeCells('C' . ($item_vinculacion + 1) . ':H' . ($item_vinculacion + 1));
                // $event->sheet->getDelegate()->getStyle('A' . ($item_vinculacion + 1) . ':J' . ($item_vinculacion + 1))->applyFromArray($border);


                // for ($i = 1; $i <= $this->item; $i++) {
                //     $event->sheet->getDelegate()->getStyle('A' . ($i + 2))->applyFromArray($negrita);
                //     $event->sheet->getDelegate()->getStyle('A' . ($i + 2) . ':J' . ($i + 2))->applyFromArray($border);
                //     $event->sheet->getDelegate()->getStyle('A' . ($i + 2))->applyFromArray($centrar);
                //     $event->sheet->getDelegate()->getStyle('E' . ($i + 2))->applyFromArray($centrar);
                // }
                // $event->sheet->setCellValue('A' . ($this->item + 3), 'Fuente: Oficina General de Admisión');
                // $event->sheet->getDelegate()->mergeCells('A' . ($this->item + 3) . ':E' . ($this->item + 3));
                // $event->sheet->getDelegate()->getStyle('A' . ($this->item + 3) . ':E' . ($this->item + 3))->applyFromArray($italic);
            },
        ];
    }

    public function startCell(): string
    {
        return 'A40';
    }

    public function title(): string
    {
        return $this->dato->codigo;
    }
}
