<?php

namespace App\Exports;

use App\Models\Proceso;
use Carbon\PHPStan\Macro;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithDefaultStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithDrawings;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Style\Style;

class CaracterizacionProcesoExport implements FromView, WithCustomStartCell, WithStyles, WithColumnWidths, WithEvents, WithDefaultStyles, WithTitle, WithDrawings
{
    public $proceso;

    public function __construct(Proceso $proceso)
    {
        $this->proceso = $proceso;

        $this->lmp = $proceso->longitud_requisitos + 14; //Longitud 5. Macroprocesos
        $this->lprov = $this->lmp + 5; //Longitud proveedor 6. Descripcion + 3
        $this->lrecursos = $this->lprov + $proceso->subentradas->count() + 2; //Longitud recursos 7. Recursos
        $this->lindicador = $this->lrecursos + $proceso->longitud_recursos + 3; //Longitud indicadores 8. Indicadores
        $this->lversion = $this->lindicador + 5; //Longitud version 10. Control cambios
        $this->laprobacion = $this->lversion + $proceso->versiones_count + 2; //Longitud version 11 Aprobacion


       /* dd([
            'LMP: ' => $this->lmp,
            'LPROV: ' => $this->lprov,
            'SUBENTRADAS: ' => $proceso->subentradas->count(),
            'LRECURSOS: ' => $this->lrecursos,
            'LINDICADOR: ' => $this->lindicador,
            'LVERSION: ' => $this->lversion,
            'Versiones: ' => $proceso->versiones_count,
            'LAprobacion: ' => $this->laprobacion,
        ]);*/
    }

    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('UNASAM');
        $drawing->setDescription('Logo UNASAM');
        $drawing->setPath(public_path('/images/logo/logo_unasam.png'));
        $drawing->setHeight(65);
        $drawing->setCoordinates('B2');
        $drawing->setOffsetX(50);
        $drawing->setOffsetY(12);

        $drawing2 = new Drawing();
        $drawing2->setName('OGCU');
        $drawing2->setDescription('Logo OGCU');
        $drawing2->setPath(public_path('/images/logo/logo_ogcu.png'));
        $drawing2->setHeight(85);
        $drawing2->setWidth(85);
        $drawing2->setOffsetY(5);
        $drawing2->setOffsetX(65);
        $drawing2->setCoordinates('L2');

        return [$drawing, $drawing2];
    }

    public function styles(Worksheet $sheet)
    {

        $default_title_style = [
            'font' => ['bold' => true, 'size' => 9],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'color' => ['rgb' => 'A6A6A6']
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ]
        ];
        $default_subtitle_style = [
            'font' => ['bold' => true, 'size' => 9],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'color' => ['rgb' => 'D9D9D9']
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ]
        ];
        $default_alt_style = [
            'font' => ['bold' => true, 'size' => 8],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'color' => ['rgb' => 'DDEBF7']
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ]
        ];
        return [
            'B2:M3' => [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    ],
                ]
            ],
            'B5:M5' => [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    ],
                    'inside' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ]
            ],
            'C2:K2' => [
                'font' => ['bold' => true, 'size' => 16],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'color' => ['rgb' => 'A6A6A6']
                ],
                'borders' => [
                    'left' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'right' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM
                ]
            ],
            'C3:K3' => [
                'font' => ['bold' => true, 'size' => 14],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'color' => ['rgb' => 'A6A6A6']
                ],
                'borders' => [
                    'left' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'right' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP
                ]
            ],

            'B7:M' . ($this->lrecursos - 2) => [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    ],
                    'inside' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ]
            ],
            'B' . $this->lrecursos . ':M' . ($this->laprobacion + 4) => [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    ],
                    'inside' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ]
            ],
            'B5' => $default_title_style,
            'C5' => $default_alt_style,
            'E5' => $default_title_style,
            'F5' => $default_alt_style,
            'G5' => $default_title_style,
            'H5' => $default_alt_style,
            'J5' => $default_title_style,
            'K5' => $default_alt_style,
            'L5' => $default_title_style,
            'M5' => $default_alt_style,

            'B7' => $default_subtitle_style,
            'B9:C9' => $default_subtitle_style,
            'I9' => $default_subtitle_style,
            'B11' => $default_subtitle_style,
            'B13:C15' => $default_subtitle_style,
            'B' . $this->lmp . ':C' . ($this->lmp + 1) => $default_subtitle_style,
            'B' . ($this->lmp + 2) . ':M' . ($this->lmp + 2) => $default_subtitle_style,

            'B' . $this->lprov . ':M' . $this->lprov => $default_title_style,
            'B' . $this->lrecursos . ':M' . ($this->lrecursos + 1) => $default_title_style,
            'B' . $this->lindicador . ':M' . $this->lindicador => $default_title_style,
            'B' . ($this->lindicador + 1) . ':B' . ($this->lindicador + 3) => $default_title_style,
            'B' . $this->lversion . ':M' . $this->lversion => $default_subtitle_style,
            'B' . ($this->lversion + 1) . ':M' . ($this->lversion + 1) => $default_title_style,
            'B' . $this->laprobacion . ':M' . $this->laprobacion => $default_subtitle_style,
            'B' . ($this->laprobacion + 1) . ':M' . ($this->laprobacion + 1) => $default_title_style,
        ];
    }


    public function defaultStyles(Style $defaultStyle)
    {

        $default_text_style = [
            'font' => ['bold' => false, 'size' => 8],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true
            ]
        ];

        return $default_text_style;
    }


    public function columnWidths(): array
    {
        return [
            'A' => 2,
            'B' => 25,
            'C' => 15,
            'D' => 20,
            'E' => 15,
            'F' => 20,
            'G' => 20,
            'H' => 20,
            'I' => 20,
            'J' => 20,
            'K' => 25,
            'L' => 20,
            'M' => 15,
            'N' => 2,
        ];
    }


    public function view(): View
    {
        $macroproceso = $this->proceso;
        $longitud_total = $this->laprobacion + 5;
        return view('sgc.macroprocesos.caracterizacion.excel', compact('macroproceso', 'longitud_total'));
    }


    public function startCell(): string
    {
        return 'B2';
    }

    /**

     * Write code on Method

     *

     * @return response()

     */

    public function registerEvents(): array
    {
        $row_heights = [
            1 => 10,
            2 => 32,
            3 => 32,
            4 => 4,
            5 => 30,
            6 => 6,
            7 => 35,
            8 => 4,
            10 => 4,
            12 => 4,
            13 => 30,
            14 => 30,
            15 => 30,
            17 => 25,
            18 => 25,
            20 => 25,
            22 => 30,

            $this->lmp - 1 => 4,
            $this->lprov - 1 => 4,
            $this->lrecursos - 1 => 4,
            $this->lindicador - 1 => 4,
            $this->lversion - 1 => 4,
        ];

        return [
            AfterSheet::class => function (AfterSheet $event) use ($row_heights) {
                foreach ($row_heights as $index => $row) {
                    $event->sheet->getDelegate()->getRowDimension($index)->setRowHeight($row);
                }
                $event->sheet->getPageSetup()->setPrintArea('B2:M' . ($this->laprobacion + 5));
                $event->sheet->getPageSetup()->setFitToPage(true);
                $event->sheet->getSheetView()->setZoomScale(90);
                $event->sheet->getSheetView()->setView(\PhpOffice\PhpSpreadsheet\Worksheet\SheetView::SHEETVIEW_PAGE_BREAK_PREVIEW);;
            },

        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->proceso->codigo;
    }
}
