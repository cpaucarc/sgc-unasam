<?php

namespace App\Exports;

use App\Models\Macroproceso;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithDefaultStyles;
use Maatwebsite\Excel\Concerns\Exportable;

use PhpOffice\PhpSpreadsheet\Style\Style;
class MacroprocesoExport implements WithMultipleSheets, WithDefaultStyles
{
    use Exportable;

    protected $macroproceso;

    public function __construct(Macroproceso $macroproceso)
    {
        $this->macroproceso = $macroproceso;
    }

    public function sheets(): array
    {
        $sheets = [];

        foreach ($this->macroproceso->indicadors as $indicador) :
            $sheets[] = new IndicadorExport($indicador);
        endforeach;

        return $sheets;
    }

    public function defaultStyles(Style $defaultStyle)
    {

        $default_text_style = [
            'font' => ['bold' => false, 'size' => 8],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true
            ]
        ];

        return $default_text_style;
    }

}
