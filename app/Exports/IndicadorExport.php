<?php

namespace App\Exports;

use App\Models\Indicador;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithDefaultStyles;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithDrawings;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Style\Style;


class IndicadorExport implements FromView, WithCustomStartCell, WithStyles, WithColumnWidths, WithEvents, WithDefaultStyles, WithTitle, WithDrawings
{

    public $indicador;

    public function __construct(Indicador $indicador)
    {
        $this->indicador = $indicador;

        $this->lri = $this->indicador->longitud_responsables_informacion + 11; // longitud_responsables_informacion 11
        $this->lmed = $this->lri + 11; // longitud_medicion 22
        $this->lgraph = $this->lmed + $this->indicador->resultados->count() + 5; // grafica 36
        $this->laprob = $this->lgraph + $this->indicador->versiones_count + 2; // aprobacion 41


       /* dd([
            'Longi Respon Info: ' => $this->indicador->longitud_responsables_informacion,
            'Resultados count: ' => $this->indicador->resultados->count(),
            'LRI: ' => $this->lri,
            'LMED: ' => $this->lmed
        ]);*/
    }



    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('UNASAM');
        $drawing->setDescription('Logo UNASAM');
        $drawing->setPath(public_path('/images/logo/logo_unasam.png'));
        $drawing->setHeight(65);
        $drawing->setCoordinates('B2');
        $drawing->setOffsetX(50);
        $drawing->setOffsetY(12);

        $drawing2 = new Drawing();
        $drawing2->setName('OGCU');
        $drawing2->setDescription('Logo OGCU');
        $drawing2->setPath(public_path('/images/logo/logo_ogcu.png'));
        $drawing2->setHeight(85);
        $drawing2->setWidth(85);
        $drawing2->setOffsetY(5);
        $drawing2->setOffsetX(65);
        $drawing2->setCoordinates('L2');

        return [$drawing, $drawing2];
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Indicador::all();
    }

    public function styles(Worksheet $sheet)
    {

        $default_title_style = [
            'font' => ['bold' => true, 'size' => 9],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'color' => ['rgb' => 'A6A6A6']
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ]
        ];
        $default_subtitle_style = [
            'font' => ['bold' => true, 'size' => 9],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'color' => ['rgb' => 'D9D9D9']
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ]
        ];
        $default_alt_style = [
            'font' => ['bold' => true, 'size' => 8],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'color' => ['rgb' => 'DDEBF7']
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ]
        ];
        //   dd($this->lri, $this->lmed, $this->lgraph, $this->laprob, $this->indicador->longitud_responsable_calculo + 10);
        return [
            'B2:M3' => [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    ],
                ]
            ],
            'C2:K2' => [
                'font' => ['bold' => true, 'size' => 16],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'color' => ['rgb' => 'A6A6A6']
                ],
                'borders' => [
                    'left' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'right' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM
                ]
            ],
            'C3:K3' => [
                'font' => ['bold' => true, 'size' => 14],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'color' => ['rgb' => 'A6A6A6']
                ],
                'borders' => [
                    'left' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'right' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP
                ]
            ],

            'B5:M5' => [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    ],
                    'inside' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ]
            ],
            'B6:M' . ($this->laprob + 5) => [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                    ],
                    'inside' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ]
            ],
            'B5' => $default_title_style,
            'C5' => $default_alt_style,
            'D5' => $default_title_style,
            'E5' => $default_alt_style,
            'F5' => $default_title_style,
            'G5' => $default_alt_style,
            'H5' => $default_title_style,
            'I5' => $default_alt_style,
            'J5' => $default_title_style,
            'K5' => $default_alt_style,
            'L5' => $default_title_style,
            'M5' => $default_alt_style,

            'B7:B8' => $default_title_style,
            'C7:E7' => $default_subtitle_style,
            'F7:G8' => $default_subtitle_style,

            'B9:M9' => $default_title_style,
            'B10:M10' => $default_subtitle_style,
            'F' . ($this->indicador->longitud_responsable_calculo + 10) . ':J' . ($this->indicador->longitud_responsable_calculo + 10) => $default_subtitle_style,
            'B' . $this->lri . ':M' . $this->lri => $default_title_style,
            'B' . ($this->lri + 1) . ':M' . ($this->lri + 1) => $default_subtitle_style,
            'B' . ($this->lri + 2) . ':B' . ($this->lri + 9) => $default_subtitle_style,
            'C' . ($this->lri + 2) . ':F' . ($this->lri + 2) => $default_subtitle_style,
            'C' . ($this->lri + 6) . ':F' . ($this->lri + 6) => $default_subtitle_style,
            'G' . ($this->lri + 4) . ':J' . ($this->lri + 4) => $default_subtitle_style,
            'G' . ($this->lri + 6) . ':H' . ($this->lri + 6) => $default_subtitle_style,
            'G' . ($this->lri + 8) . ':H' . ($this->lri + 8) => $default_subtitle_style,
            /*
            'B19:M19' => $default_subtitle_style,
            'B20:B27' => $default_subtitle_style,
            'C20:F20' => $default_subtitle_style,
            'C24:F24' => $default_subtitle_style,
            'G22:J22' => $default_subtitle_style,
            'G24:H24' => $default_subtitle_style,
            'G26:H26' => $default_subtitle_style,
*/
            /** Despues de 1er FOR */
            'B' . ($this->lmed) . ':M' . ($this->lmed) => $default_title_style,
            'B' . ($this->lmed + 1) . ':M' . ($this->lmed + 3) => $default_subtitle_style,

            'B' . ($this->lgraph) . ':M' . ($this->lgraph) => $default_title_style,
            'G' . ($this->lgraph + 1) . ':M' . ($this->lgraph + 1) . '' => $default_subtitle_style,

            'G' . ($this->laprob) . ':M' . ($this->laprob) . '' => $default_title_style,
            'G' . ($this->laprob + 1) . ':M' . ($this->laprob + 1) . '' => $default_subtitle_style,

            'B' . ($this->lgraph + 1) . ':F' . ($this->laprob + 4) . '' => $default_alt_style,
            /*
            'B29:M29' => $default_title_style,
            'B30:M32' => $default_subtitle_style,

            'B46:M46' => $default_title_style,
            'G47:M47' => $default_subtitle_style,
            'G51:M51' => $default_title_style,
            'G52:M52' => $default_subtitle_style,

            'B47:F55' => $default_alt_style,*/
        ];
    }


    public function defaultStyles(Style $defaultStyle)
    {

        $default_text_style = [
            'font' => ['bold' => false, 'size' => 8],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true
            ]
        ];

        return $default_text_style;
    }


    public function columnWidths(): array
    {
        return [
            'A' => 2,
            'B' => 25,
            'C' => 25,
            'D' => 20,
            'E' => 23,
            'F' => 6,
            'G' => 20,
            'H' => 14,
            'I' => 13,
            'J' => 13,
            'K' => 25,
            'L' => 20,
            'M' => 15,
            'N' => 2,
        ];
    }


    public function view(): View
    {
        $indicador = $this->indicador;
        $longitud_total = $this->laprob + 5;
        return view('sgc.indicadors.excel', compact('indicador', 'longitud_total'));
    }


    public function startCell(): string
    {
        return 'B2';
    }

    /**

     * Write code on Method

     *

     * @return response()

     */

    public function registerEvents(): array
    {
        $row_heights = [
            1 => 10,
            2 => 32,
            3 => 32,
            4 => 6,
            5 => 35,
            6 => 6,
            7 => 20,
            8 => 20,

            $this->lmed - 1 => 6,

            $this->lgraph - 1 => 6,

            $this->laprob + 2 => 50,
            $this->laprob + 3 => 50,
            $this->laprob + 4 => 50
        ];

        return [
            AfterSheet::class => function (AfterSheet $event) use ($row_heights) {
                foreach ($row_heights as $index => $row) {
                    $event->sheet->getDelegate()->getRowDimension($index)->setRowHeight($row);
                }
                $event->sheet->getPageSetup()->setPrintArea('A1:N' . ($this->laprob + 5));
                $event->sheet->getPageSetup()->setFitToPage(true);
                $event->sheet->getSheetView()->setZoomScale(90);
                $event->sheet->getSheetView()->setView(\PhpOffice\PhpSpreadsheet\Worksheet\SheetView::SHEETVIEW_PAGE_BREAK_PREVIEW);;
            },

        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->indicador->codigo;
    }
}
