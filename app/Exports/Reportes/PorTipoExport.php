<?php

namespace App\Exports\Reportes;

use Illuminate\Contracts\View\View;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Style\Style;

use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithDefaultStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithDrawings;


use Maatwebsite\Excel\Events\AfterSheet;

use DB;
use App\Exports\DefaultStyles;

class PorTipoExport implements FromView, WithStyles, WithDefaultStyles, WithColumnWidths, WithEvents, WithDrawings
{
    use DefaultStyles;
    public $conteo;
    public $tipos;


    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('UNASAM');
        $drawing->setDescription('Logo UNASAM');
        $drawing->setPath(public_path('/images/logo/logo_unasam.png'));
        $drawing->setHeight(45);
        $drawing->setCoordinates('A1');
        $drawing->setOffsetX(5);
        $drawing->setOffsetY(5);

        $drawing2 = new Drawing();
        $drawing2->setName('OGCU');
        $drawing2->setDescription('Logo OGCU');
        $drawing2->setPath(public_path('/images/logo/logo_ogcu.png'));
        $drawing2->setHeight(50);
        $drawing2->setWidth(50);
        $drawing2->setOffsetY(5);
        $drawing2->setCoordinates('D1');

        return [$drawing, $drawing2];
    }

    public function styles(Worksheet $sheet)
    {
        $arr = [
            'A1:D1' =>  [
                'font' => ['bold' => true, 'size' => 16],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'color' => ['rgb' => 'D9D9D9']
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    ],
                ]
            ],
            'A3:D' . $this->conteo + 2 => [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ]
            ],
            'A3:D3' => $this->default_alt_style,
            'A4:D4' => [
                'font' => ['bold' => true, 'size' => 9],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'color' => ['rgb' => 'D9D9D9']
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                ]
            ]
        ];
        $a = [];
        $c = 4;
        foreach ($this->tipos as $t) {
            $conteo = $t->count() + $c + 1;
            $arr['A' . $conteo . ':D' . $conteo] = [
                'font' => ['bold' => true, 'size' => 9],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'color' => ['rgb' => 'D9D9D9']
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                ]
            ];
            $c = $conteo;
        }
        return $arr;
    }

    public function defaultStyles(Style $defaultStyle)
    {

        $default_text_style = [
            'font' => ['bold' => false, 'size' => 8],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true
            ]
        ];

        return $default_text_style;
    }



    public function columnWidths(): array
    {
        return [
            'A' => 100,
            'B' => 25,
            'C' => 25
        ];
    }

    public function view(): View
    {

        $indicadores = collect(DB::select("
            SELECT
            d.detalle AS 'tipo', i.codigo, i.nombre, AVG(r.valor_indicador) AS 'promedio', COUNT(r.valor_indicador) AS 'valores', COUNT(DISTINCT i.nombre) AS 'conteo'
            FROM indicadors i
            INNER JOIN detalles d ON d.id = i.param_tipoindicador
            LEFT JOIN resultados r ON r.indicador_id = i.id
            GROUP BY d.detalle, i.codigo, i.nombre
            ORDER BY d.detalle;
        "));
        $this->tipos = $indicadores->groupBy('tipo');
        $this->conteo = $indicadores->count() + $this->tipos->count() + 1;
        $tipos = $this->tipos;
        return view('livewire.reportes.por-tipo.table', compact('tipos'));
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(40);
            },
        ];
    }

}
