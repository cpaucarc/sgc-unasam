<?php

namespace App\Exports\Reportes;

use App\Models\Indicador;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithDefaultStyles;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithDrawings;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Style\Style;
use App\Exports\DefaultStyles;

class IndicadoresExport implements FromView, WithStyles, WithDefaultStyles, WithColumnWidths, WithEvents, WithDrawings
{

    use DefaultStyles;

    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('UNASAM');
        $drawing->setDescription('Logo UNASAM');
        $drawing->setPath(public_path('/images/logo/logo_unasam.png'));
        $drawing->setHeight(45);
        $drawing->setCoordinates('A1');
        $drawing->setOffsetX(5);
        $drawing->setOffsetY(5);

        $drawing2 = new Drawing();
        $drawing2->setName('OGCU');
        $drawing2->setDescription('Logo OGCU');
        $drawing2->setPath(public_path('/images/logo/logo_ogcu.png'));
        $drawing2->setHeight(50);
        $drawing2->setWidth(50);
        $drawing2->setOffsetY(5);
        $drawing2->setCoordinates('O1');

        return [$drawing, $drawing2];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            'A1:O1' =>  [
                'font' => ['bold' => true, 'size' => 16],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'color' => ['rgb' => 'D9D9D9']
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                ],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    ],
                ]
            ],
            'A3:O' . $this->conteo + 2 => [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ]
            ],
            'A3:O3' => $this->default_alt_style
        ];
    }

    public function defaultStyles(Style $defaultStyle)
    {

        $default_text_style = [
            'font' => ['bold' => false, 'size' => 8],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => false
            ]
        ];

        return $default_text_style;
    }

    public function columnWidths(): array
    {
        return [
            'A' => 5,
            'B' => 15,
            'C' => 40,
            'D' => 40,
            'E' => 40,
            'F' => 25,
            'G' => 25,
            'H' => 10,
            'I' => 10,
            'J' => 15,
            'K' => 15,
            'L' => 35,
            'M' => 10,
            'N' => 10,
            'O' => 20
        ];
    }

    public function view(): View
    {
        $indicadores = Indicador::with('macroproceso', 'proceso', 'metas', 'meta', 'responsables', 'resultados', 'formula')->get();
        $this->conteo = $indicadores->count() + 1;
        return view('livewire.reportes.indicadores.table', compact('indicadores'));
    }


    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(40);
            },
        ];
    }
}
