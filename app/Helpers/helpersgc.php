<?php

use App\Models\Entrada;
use App\Models\Macroproceso;
use App\Models\Microproceso;
use App\Models\Proceso;
use App\Models\Subproceso;
use App\Models\Resultado;
use App\Models\Meta;
use App\Models\Asignacion;
use App\Models\Anio;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

function getNombreProceso($nivel_proceso, $id)
{
    $valor = '-';
    $nivelproceso = '';
    switch ($nivel_proceso) {
        case 15001:
            $nivelproceso = Macroproceso::find($id);
            break;
        case 15002:
            $nivelproceso = Proceso::find($id);
            break;
        case 15003:
            $nivelproceso = Subproceso::find($id);
            break;
        case 15004:
            $nivelproceso = Microproceso::find($id);
            break;
    }
    if ($nivelproceso) {
        $valor = $nivelproceso->codigo . ': ' . $nivelproceso->nombre;
    }
    return $valor;
}
function salida($id)
{
    if ($id) {
        $salida = Entrada::find($id);
        return $salida;
    }
}
function entrada_salida($nivel, $proceso)
{
    $datos = Entrada::whereNivel_proceso($nivel)->whereProceso_id($proceso)->get();
    if ($datos) {
        return $datos;
    }
    return false;
}
function tipoProceso($nivel_proceso)
{
    $nivelproceso = '';
    switch ($nivel_proceso) {
        case 15001:
            $nivelproceso = 'MACROPROCESO';
            break;
        case 15002:
            $nivelproceso = 'PROCESO';
            break;
        case 15003:
            $nivelproceso = 'SUBPROCESO';
            break;
        case 15004:
            $nivelproceso = 'MICROPROCESO';
            break;
        case 15005:
            $nivelproceso = 'ACTIVIDAD';
            break;
    }
    return $nivelproceso;
}
function obtenerDesempenio($idResultado, $tpvalor)
{
    $res = "-";
    $resultado = Resultado::find($idResultado);
    if (!$resultado) {
        return $res;
    }
    $meta = Meta::where('indicador_id', $resultado->indicador_id)
        ->where('anio_id', $resultado->anio_id)
        ->where('param_periodo', $resultado->param_periodo)
        ->whereNull('deleted_at')
        ->first();
    if (!$meta) {
        return $res;
    }

    $valor = round($resultado->valor_numerador / $resultado->valor_denominador, 2);
    if (!$meta->escalas) {
        return $res;
    }
    foreach ($meta->escalas as $escala) {
        if ($valor <= $escala->valor_fin) {
            if ($tpvalor == 1) {
                $res = $escala->nivel->detalle;
            }
            if ($tpvalor == 2) {
                $descripcion = json_decode($escala->nivel->descripcion);
                $res = $descripcion->{'accion'};
            }
            break;
        }
    }
    return $res;
}
function obtenerDesempenioValor($indicador_id, $anio, $periodo, $numerador, $denominador, $tpvalor)
{
    $res = "-";

    $meta = Meta::where('indicador_id', $indicador_id)
        ->where('anio_id', $anio)
        ->where('param_periodo', $periodo)
        ->whereNull('deleted_at')
        ->first();
    if (!$meta) {
        return $res;
    }

    $valor = round($numerador / $denominador, 2);
    if (!$meta->escalas) {
        return $res;
    }
    foreach ($meta->escalas as $escala) {
        if ($valor <= $escala->valor_fin) {
            if ($tpvalor == 1) {
                $res = $escala->nivel->detalle;
            }
            if ($tpvalor == 2) {
                $descripcion = json_decode($escala->nivel->descripcion);
                $res = $descripcion ? $descripcion->{'accion'} : '';
            }
            break;
        }
    }
    return $res;
}
function cambiarRol($id)
{
    /**
     * INGRESA -1 CUANDO  ES DEL LOGIN
     */

    if ($id == -1) {
        //ELIMINAMOS TODAS LAS SESSIONES POR SI LAS DUDAS
        Session::forget('miRolName');
        Session::forget('miRolActual');
        Session::forget('miRolActualPermisos');
        //VALIDAMOS SI POSEE ASIGANACIONES Y ROLES
        if (Auth::user()) {
            if (count(Auth::user()->asignaciones) > 0) {
                //ASIGNAMOS ROL POR DEFECTO EL ULTIMO
                $mirol_actual =  Asignacion::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->take(1)->first();
                //session(['miRolActual' =>  $mirol_actual->rol]);
                //return  $mirol_actual;
                $role_permissions = Role::findByName($mirol_actual->rol->name)->permissions;
                session(['miId' =>  $mirol_actual->id]);
                session(['miRolName' =>  $mirol_actual->rol->name]);
                session(['miRolActual' =>  $mirol_actual]);
                $newRole = array();
                $count = 0;
                foreach ($role_permissions as $permi) {
                    $newRole[$count] = $permi->name;
                    $count++;
                };

                session(['miRolActualPermisos' =>  $role_permissions]);
            } else {
                // INGRESARA COMO INVITADO
                session(['miRolName' =>  'Invitado']);
                session(['miRolActual' =>  null]);
                session(['miRolActualPermisos' =>  null]);
            }
        } else {
            session(['miRolName' =>  null]);
            session(['miRolActual' =>  null]);
            session(['miRolActualPermisos' =>  null]);
        }
    } else {
        session(['miId' =>  $id]);

        $mirol_actual =  Asignacion::find($id);
        $role_permissions = Role::findByName($mirol_actual->rol->name)->permissions;
        session(['miRolName' =>  $mirol_actual->rol->name]);
        session(['miRolActual' =>  $mirol_actual]);
        $newRole = array();
        $count = 0;
        foreach ($role_permissions as $permi) {
            $newRole[$count] = $permi->name;
            $count++;
        };
        session(['miRolActualPermisos' =>  $role_permissions]);


        Auth::user()->asignacion_id = $id;
        Auth::user()->save();

        Auth::user()->syncRoles([$mirol_actual->rol->name]);
    }
}

function validarPermiso($permisoConsulta)
{
    if (Session::get('miRolName') == null) {
        return false;
    } else if (Session::get('miRolName') == 'Invitado') {
        return false;
    } else {
        //SE EVALUA SI TIENE PERMISOS
        $misPermisos = Session::get('miRolActualPermisos');
        foreach ($misPermisos as $permi) {
            if ($permi->name == $permisoConsulta) {
                return true;
                break;
            }
        }
        return false;
    }
}

function validarIdArea($idAreaConsulta)
{
    if (Session::get('miRolName') == null) {
        return false;
    } else if (Session::get('miRolName') == 'Invitado') {
        return false;
    } else {
        //SE EVALUA SI TIENE CON LA AREA DE CONSULTA
        $misRolActual = Session::get('miRolActual');
        if ($misRolActual->area_id == $idAreaConsulta) {
            return true;
        } else {
            return false;
        }
    }
}

function setAnioIndicadorActual($anio)
{
    if ($anio == null) {
        Session::forget('anioIndicadorActual');
    } else {
        session(['anioIndicadorActual' =>  $anio]);
    }
}
function getAnioIndicadorActual()
{
    if (Session::get('anioIndicadorActual') == null) {
        //COMO NO HAY  DEVOLEVERMOS EL  ULTIMO ANIO INGRESADO EN EL SISTEMA
        $miAnio =  Anio::where('activo', '1')->orderBy('id', 'desc')->take(1)->first();
        return $miAnio->id;
    } else {
        return Session::get('anioIndicadorActual');
    }
}

function getValorColor($unidad, $min, $max, $meta, $valor, $color)
{
   // dd("min:" . $min . " max:" . $max);
    //dd($color);
    $denominador = $max - $min;
    // dd("denom:" . $denominador);
     //dd("valor:" . $valor . ' color: ' . $color);

    if ($unidad = 'PORCENTAJE') {
        $porcentaje = $valor / $denominador;
        // dd("denom:" . $porcentaje);
        $duelve = array($porcentaje, $color);
        return $duelve;
    } else {
        $porcentaje = $valor / $denominador;

        $duelve = array($porcentaje, $color);
        return $duelve;
    }
}
