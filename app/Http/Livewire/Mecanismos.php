<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Mecanismo;
use App\Models\Detalle;
use Illuminate\Support\Facades\Auth;

class Mecanismos extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $selected_id, $search, $nombre, $descripcion,$param_tipomecanismo, $activo;
    public $updateMode = false;
    public $sort = 'id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'nombre' => 'required|unique:mecanismos',
        //'descripcion' => 'required',
        'param_tipomecanismo' => 'required',
    ];
    protected $msjError=[
        'nombre.required' => 'El campo nombre es obligatorio.',
        'nombre.unique' => 'El valor del campo nombre ya está en uso.',
        'param_tipomecanismo.required' => 'El campo tipo mecanismo es obligatorio.',       
    ];


    public function render()
    {
        $mecanismos = Mecanismo::where('nombre', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);
        $tipoMecanismos= Detalle::whereparametro_id(8)->whereActivo(1)->get();
        $search = '%' . $this->search . '%';
        return view('livewire.mecanismos.view')->with(compact('mecanismos'))->with(compact('tipoMecanismos'))  ;
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->nombre = null;
        $this->descripcion = null; 
        $this->param_tipomecanismo=null;      
        $this->activo = null;
    }

    public function store()
    {
        $this->validate($this->rules,$this->msjError);

        Mecanismo::create([
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'param_tipomecanismo' => $this->param_tipomecanismo,
            'activo' =>'1',
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();
        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El Mecanismo ha sido creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos); 
    }

    public function edit($id)
    {
        $miMecanismo = Mecanismo::findOrFail($id);

        $this->selected_id = $id;
        $this->nombre = $miMecanismo->nombre;
        $this->descripcion = $miMecanismo->descripcion;  
        $this->param_tipomecanismo=$miMecanismo->param_tipomecanismo;    
        $this->activo = $miMecanismo->activo;
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
            'nombre' => 'required|unique:mecanismos,nombre,'.$this->selected_id,
            //'descripcion' => 'required',
            'param_tipomecanismo' => 'required',           
        ],$this->msjError);
        if ($this->selected_id) {
            $miMecanismo = Mecanismo::findOrFail($this->selected_id);
            $miMecanismo->update([
                'nombre' => $this->nombre,
                'descripcion' => $this->descripcion,
                'param_tipomecanismo' => $this->param_tipomecanismo,
                'activo' =>'1',
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;
            $datos = [
                'tipo' => 'success',
                'mensaje' => 'El Mecanismo ha sido actualizado satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos); 
        }
        
    }

    public function destroy($id)
   {
        if ($id) {
            $record = Mecanismo::find($id);
            $record->deleted_user = Auth::user()->id;
            $record->save();
            $record->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'El Mecanismo ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }  
    
}
