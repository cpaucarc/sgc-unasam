<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Macroproceso;
use App\Models\Detalle;
use App\Models\Area;
use App\Models\Version;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class Macroprocesos extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $openVer = false;
    public $cantidad = 10;
    public $selected_id, $search, $codigo,$nombre,$version_id,$descripcion,$param_tipo,$responsable_area,$objetivo,$alcance,$finalidad,$orden,$activo,$tiene_api;
    public $updateMode = false;
    public $showMode = false;
    public $sort = 'id';
    public $direction = 'desc';

    protected $listeners = ['destroy'];
    protected $rules = [
        'codigo' => 'required|unique:macroprocesos',
        'nombre' => 'required|unique:macroprocesos',
        'version_id' => 'required',
        //'descripcion' => 'required',
    ];
    protected $msjError=[
        'codigo.required' => 'El campo código es obligatorio.',
        'codigo.unique' => 'La combinación de codigo y versión ya está en uso.',
        'nombre.required' => 'El campo nombre es obligatorio.',
        'nombre.unique' => 'El valor del campo nombre ya está en uso.',
        'version_id.required' => 'El campo versión es obligatorio.'
    ];

    public function render()
    {
        $macroprocesos = Macroproceso::where('nombre', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);
        $tipoMacroprocesos= Detalle::whereparametro_id(5)->whereActivo(1)->get();
        $areas= Area::whereActivo(1)->get();
        $versiones= Version::whereActivo(1)->where('nivel_proceso', 15001)->get();

        $search = '%' . $this->search . '%';
        return view('livewire.macroprocesos.view', compact('macroprocesos','tipoMacroprocesos','areas','versiones'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
        $this->openVer = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->openVer = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->codigo=null;
        $this->nombre=null;
        $this->version_id=null;
        $this->descripcion=null;
        $this->param_tipo=null;
        $this->responsable_area=null;
        $this->objetivo=null;
        $this->alcance=null;
        $this->finalidad=null;
        $this->orden=null;
        $this->activo = null;
    }

    public function store()
    {
        $this->validate($this->rules,$this->msjError);

        Macroproceso::create([
        'codigo'=>$this->codigo,
        'nombre'=>$this->nombre,
        'version_id'=>$this->version_id,
        'descripcion'=>$this->descripcion,
        'param_tipo'=>$this->param_tipo,
        'responsable_area'=>$this->responsable_area,
        'objetivo'=>$this->objetivo,
        'alcance'=>$this->alcance,
        'tiene_api' => $this->tiene_api,
        'finalidad'=>$this->finalidad,
        'orden'=>$this->orden,
        'activo' =>'1',
        'created_user' => Auth::user()->id
        ]);

        $this->resetInput();
        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El macroproceso ha sido creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }

    public function edit($id)
    {
        $miMacroproceso = Macroproceso::findOrFail($id);

        $this->selected_id = $id;
        $this->codigo=$miMacroproceso->codigo;
        $this->nombre=$miMacroproceso->nombre;
        $this->version_id=$miMacroproceso->version_id;
        $this->descripcion=$miMacroproceso->descripcion;
        $this->param_tipo=$miMacroproceso->param_tipo;
        $this->responsable_area=$miMacroproceso->responsable_area;
        $this->objetivo=$miMacroproceso->objetivo;
        $this->alcance=$miMacroproceso->alcance;
        $this->finalidad=$miMacroproceso->finalidad;
        $this->orden=$miMacroproceso->orden;
        $this->activo = $miMacroproceso->activo ;
        $this->tiene_api = $miMacroproceso->tiene_api?1:0;

        $this->updateMode = true;
    }

    public function show($id)
    {
        $miMacroproceso = Macroproceso::findOrFail($id);

        $this->selected_id = $id;
        $this->codigo=$miMacroproceso->codigo;
        $this->nombre=$miMacroproceso->nombre;
        $this->version_id=$miMacroproceso->version_id;
        $this->descripcion=$miMacroproceso->descripcion;
        $this->param_tipo=$miMacroproceso->param_tipo;
        $this->responsable_area=$miMacroproceso->responsable_area;
        $this->objetivo=$miMacroproceso->objetivo;
        $this->alcance=$miMacroproceso->alcance;
        $this->finalidad=$miMacroproceso->finalidad;
        $this->orden=$miMacroproceso->orden;
        $this->activo = $miMacroproceso->activo ;
        $this->tiene_api = $miMacroproceso->tiene_api?1:0;

        //$this->updateMode = true;
        $this->openVer = true;
    }

    public function update()
    {
        $this->validate([
            'codigo' => [
                'required',
                Rule::unique('macroprocesos')->ignore($this->selected_id)->where(fn ($query) => $query->where('codigo', $this->codigo)->where('version_id', $this->version_id))
            ],
            'nombre' => 'required|unique:macroprocesos,nombre,'.$this->selected_id,
            'version_id' => 'required',
        ],$this->msjError);
        if ($this->selected_id) {
            $miMacroproceso = Macroproceso::findOrFail($this->selected_id);
            $miMacroproceso->update([
                'codigo'=>$this->codigo,
                'nombre'=>$this->nombre,
                'version_id'=>$this->version_id,
                'descripcion'=>$this->descripcion,
                'param_tipo'=>$this->param_tipo,
                'responsable_area'=>$this->responsable_area,
                'objetivo'=>$this->objetivo,
                'alcance'=>$this->alcance,
                'finalidad'=>$this->finalidad,
                'tiene_api'=>$this->tiene_api,
                'orden'=>$this->orden,
                'activo' =>'1',
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;
            //$this->emit('alert', 'Macroproceso actualizado satisfactoriamente.');
            $datos = [
                'tipo' => 'success',
                'mensaje' => 'El macroproceso ha sido actualizado satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos);
        }

    }

    public function destroy($id)
   {
        if ($id) {
            $record = Macroproceso::find($id);
            $record->deleted_user = Auth::user()->id;
            $record->save();
            $record->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'El macroproceso ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }


}
