<?php

namespace App\Http\Livewire;

use App\Models\Anio;
use App\Models\Detalle;
use App\Models\Proceso;
use Livewire\Component;
use App\Models\Indicador;
use App\Models\Macroproceso;
use App\Models\Meta;
use App\Models\Resultado;
use Illuminate\Support\Facades\DB;

class Dashboard extends Component
{
    public $macroproceso, $proceso, $procesos = [], $indicadors = [], $periodos = [], $escalas = [], $isActivo = '', $index = '';
    public $tipoindicador = '-', $anio, $periodo = NULL, $medicion = '-', $unidad = '-', $meta = '-', $calculo = '-', $analisis = '-', $valor = '-', $indicador_id, $indicador_nombre;
    public $isBloque = false, $isAnio = false, $isPeriodo = false;
    public $metas = [], $resultados = [];
    public $min = 0, $max = 0;
    public $anual = false;
    public $scroll = "";
    public $search;
    public $indicador;
    public $indicacion;
    public $version;
    public $versiones = [];
    public $mensaje_indicador;

    protected $listeners = ['obtenerProcesos', 'obtenerIndicadores'];

    public function render()
    {
        $macroprocesos = Macroproceso::whereActivo(1)->withCount('indicadors')->get();
        $macroprocesos = $macroprocesos->where('indicadors_count', '>', 0);
        $anios = Anio::whereActivo(1)->orderBy('anio', 'asc')->take(12)->get();
        return view('livewire.dashboard.view', compact('macroprocesos', 'anios'));
    }
    public function obtenerProcesos()
    {
        $this->mensaje_indicador = $this->macroproceso;
        $this->indicador = 'macroproceso';
        $this->resetearCondicion();
        $this->indicadors = Indicador::whereMacroproceso_id($this->macroproceso)
            ->where(DB::raw("CONCAT(codigo,' - ',nombre)"), 'LIKE', '%' . $this->search . '%')
            ->whereActivo(1)
            ->select('id', 'nombre', 'codigo', DB::raw('(CASE WHEN proceso_id is NULL THEN 1 ELSE 2 END) AS orden'), 'macroproceso_id', 'proceso_id')
            ->orderBy('orden', 'asc')
            ->orderBy('nombre', 'asc')
            ->get();
        $this->procesos = Proceso::whereMacroproceso_id($this->macroproceso)->whereActivo(1)->get();

        if ($this->indicadors->count() > 7) {
            $this->scroll = "height: 455px; overflow-y: scroll;";
        } else {
            $this->scroll = "";
        }
        $this->emit('mostrarProcesos');
    }
    public function obtenerIndicadores()
    {
        $this->indicador = 'procesos';
        $this->resetearCondicion();
        $this->indicadors = Indicador::whereProceso_id($this->proceso)->where('nombre', 'LIKE', '%' . $this->search . '%')->whereActivo(1)->orderBy('nombre', 'asc')->get();
        if ($this->indicadors->count() > 7) {
            $this->scroll = "height: 455px; overflow-y: scroll;";
        } else {
            $this->scroll = "";
        }
        $this->emit('selectProceso');
    }
    public function activarIndicador($indicador_id, $index = null, $anio = null, $periodo = null)
    {

        $this->index = $index;
        $this->escalas = [];
        $this->indicador_id = $indicador_id;
        $this->isBloque = true;

        $this->valor = '-';

        $indicador = Indicador::find($indicador_id);
        $this->versiones = $indicador->versiones;
        $this->version = $indicador_id;
        $this->indicador_nombre = $indicador->nombre;
        if ($indicador) {
            $this->tipoindicador = $indicador->detalle->abreviatura;
            $anual = false;
            if ($indicador->medicion) {
                $this->medicion = $indicador->medicion->frecuencia->detalle;
                switch ($indicador->medicion->param_frec_medicion) {
                    case '13001':
                        $this->isPeriodo = false;
                        $anual = true;
                        break;
                    case '13002':
                        $this->isPeriodo = true;
                        $this->periodos = Detalle::whereParametro_id(19)->whereActivo(1)->get();
                        $anual = false;
                        break;
                    case '13003':
                        $this->isPeriodo = true;
                        $this->periodos = Detalle::whereParametro_id(20)->whereActivo(1)->get();
                        $anual = false;
                        break;
                    case '13004':
                        $this->isPeriodo = true;
                        $this->periodos = Detalle::whereParametro_id(21)->whereActivo(1)->get();
                        $anual = false;
                        break;
                    case '13005':
                        $this->isPeriodo = true;
                        $this->periodos = Detalle::whereParametro_id(22)->whereActivo(1)->get();
                        $anual = false;
                        break;
                    default:
                        $this->isPeriodo = false;
                        $this->periodos = [];
                        $anual = false;
                        break;
                }
            } else {
                $this->medicion = '-';
            }
            if ($indicador->medicion) {
                $this->unidad = $indicador->medicion->unidad->detalle;
            } else {
                $this->unidad = '-';
            }

            $this->anual = $anual;

            if ($indicador->interesados) {
                $this->calculo = $indicador->interesados->where('pivot.param_tiporesponsable', '11002')->first()?$indicador->interesados->where('pivot.param_tiporesponsable', '11002')->first()->nombre:'-';
                $this->analisis = $indicador->interesados->where('pivot.param_tiporesponsable', '11003')->first()?$indicador->interesados->where('pivot.param_tiporesponsable', '11003')->first()->nombre:'-';
            }
            $metas = $indicador->metas()
                ->join('detalles', 'detalles.id', '=', 'metas.param_periodo')
                ->where('metas.activo', 1)
                ->orderBy('anio_id', 'desc')
                ->orderBy('param_periodo', 'desc')
                ->select('metas.id as meta_id', 'indicador_id', 'param_periodo', 'anio_id', 'detalle', 'meta', 'metas.ascendente')
                ->take(12)
                ->get();

            if (is_null($anio)) {
                $metas = $indicador->metas()
                    ->join('detalles', 'detalles.id', '=', 'metas.param_periodo')
                    ->where('metas.activo', 1)
                    ->orderBy('anio_id', 'desc')
                    ->orderBy('param_periodo', 'desc')
                    ->select('metas.id as meta_id', 'indicador_id', 'param_periodo', 'anio_id', 'detalle', 'meta', 'metas.ascendente')
                    ->take(12)
                    ->get();

                if (count($metas)) {
                    $actual = $metas->first();
                    // dd($actual);
                    $this->anio = $actual->anio_id;
                    $this->periodo = $actual->param_periodo;
                    $this->meta = $actual->meta;
                }
            } else {
                if (is_null($periodo)) {
                    $metas = $indicador->metas()
                        ->join('detalles', 'detalles.id', '=', 'metas.param_periodo')
                        ->where('metas.activo', 1)
                        ->where('anio_id', $this->anio)
                        ->orderBy('anio_id', 'desc')
                        ->orderBy('param_periodo', 'desc')
                        ->select('metas.id as meta_id', 'indicador_id', 'param_periodo', 'anio_id', 'detalle', 'meta', 'metas.ascendente')
                        ->get();

                    if (count($metas)) {
                        $actual = $metas->first();
                        // dd($actual);
                        $this->anio = $actual->anio_id;
                        $this->periodo = $actual->param_periodo;
                        $this->meta = $actual->meta;
                    }
                } else {
                    $actual = $indicador->metas()
                        ->join('detalles', 'detalles.id', '=', 'metas.param_periodo')
                        ->where('metas.activo', 1)
                        ->where('anio_id', $this->anio)
                        ->where('param_periodo', $this->periodo)
                        ->orderBy('anio_id', 'desc')
                        ->orderBy('param_periodo', 'desc')
                        ->select('metas.id as meta_id', 'indicador_id', 'param_periodo', 'anio_id', 'detalle', 'meta', 'metas.ascendente')
                        ->first();

                    if ($actual) :
                        $this->anio = $actual->anio_id;
                        $this->periodo = $actual->param_periodo;
                        $this->meta = $actual->meta;
                    endif;
                }
            }
            //Gauge
            $data_valor  = 0;
            //barchar
            $data_meta = [];
            $data_label = [];
            $data_resultado = [];

            $data_color = [];
            $data_serie = [];
            $all_series = collect();
            $es_descendente = false;
            if (count($metas) && !!$actual) {

                $escalas = Meta::find($actual->meta_id);
                $es_descendente = $actual->ascendente != 1;
                $direccion = 'ASC';
                if ($actual->ascendente != 1) {
                    $direccion = 'DESC';
                }
                $this->escalas = $escalas->escalas()
                    ->join('detalles', 'detalles.id', '=', 'escalas.param_likert')
                    ->select('param_likert', 'valor_ini', 'valor_fin', 'detalle', 'descripcion')
                    ->orderBy('escalas.param_likert', $direccion)
                    ->get();

                $this->min = $this->escalas[0]->valor_ini;
                $this->max = $this->escalas[4]->valor_fin;

                foreach ($this->escalas as $escala) {
                    //array_push($data_color, getValorColor($this->unidad, $this->min, $this->max, $this->meta, $escala->valor_fin, $get_color->{'color'}));
                    array_push($data_serie, $escala->valor_fin);
                    $all_series->push($escala->valor_fin);
                    $all_series->push($escala->valor_ini);
                }

                $this->min = $all_series->min();
                $this->max = $all_series->max();

                foreach ($this->escalas as $escala) {
                    $get_color = json_decode($escala->descripcion);
                    array_push($data_color, getValorColor($this->unidad, $this->min, $this->max, $this->meta, $actual->ascendente != 1 ? $escala->valor_fin : $escala->valor_fin, $get_color->{'color'}));
                }


                $ver_valor = Resultado::where('indicador_id', $this->indicador_id)
                    ->where('anio_id', $this->anio)
                    ->where('param_periodo', $this->periodo)
                    ->select('valor_indicador')
                    ->get();

                if (count($ver_valor)) {
                    $this->valor = $ver_valor->first()->valor_indicador;
                    $data_valor = $this->valor;
                }

                foreach ($metas as $meta) {
                    array_push($data_meta, $meta->meta);
                    if ($anual) {
                        array_push($data_label, $meta->anio_id);
                    } else {
                        array_push($data_label, $meta->anio_id . ' ' . $meta->detalle);
                    }
                    $resultados = Resultado::where('indicador_id', $meta->indicador_id)
                        ->where('anio_id', $meta->anio_id)
                        ->where('param_periodo', $meta->param_periodo)
                        ->where('activo', 1)
                        ->select('valor_indicador')
                        ->get();
                    // dd($meta->indicador_id);
                    if (count($resultados)) {
                        array_push($data_resultado, $resultados->first()->valor_indicador);
                    } else {
                        array_push($data_resultado, 0);
                    }
                }
            }
        } else {
            return;
        }
        $data_serie = $all_series->unique()->sort()->values()->toArray();
        $divisor = $this->obtenerDivisor($data_serie);


        $this->emit('listaGroup', $index);
        $this->emit('selectProceso');
        $this->emit('verVelocimetro', $this->unidad, $data_color, $this->min, $this->max, $data_valor, $data_serie, $divisor, false/* $es_descendente*/);
        $this->emit('verGraficoBarras', $data_label, $data_meta, $data_resultado);
    }

    public function cambiarVersion()
    {
        $this->activarIndicador($this->version, $this->index);
    }

    public function resetearCondicion()
    {
        $this->indicadors = [];
        $this->isBloque = false;
        $this->isAnio = false;
        $this->isPeriodo = false;
    }

    public function verAnio()
    {
        $this->activarIndicador($this->indicador_id, $this->index, $this->anio);
    }
    public function verPeriodo()
    {
        $this->activarIndicador($this->indicador_id, $this->index, $this->anio, $this->periodo);
        /*
        $ver_meta = Meta::where('indicador_id', $this->indicador_id)
            ->where('anio_id', $this->anio)
            ->where('param_periodo', $this->periodo)
            ->select('metas.id as meta_id', 'meta', 'ascendente')
            ->get();

        $data_color = [];
        $data_serie = [];
        $this->escalas = [];

        if (count($ver_meta)) {
            $this->meta = $ver_meta->first()->meta;
            $escalas = Meta::find($ver_meta->first()->meta_id);

            $direccion = 'ASC';
            if ($ver_meta->first()->ascendente != 1) {
                $direccion = 'DESC';
            }

            $this->escalas = $escalas->escalas()
                ->join('detalles', 'detalles.id', '=', 'escalas.param_likert')
                ->select('param_likert', 'valor_ini', 'valor_fin', 'detalle', 'descripcion')
                ->orderBy('escalas.param_likert', $direccion)
                ->get();

            $this->min = $this->escalas[0]->valor_ini;
            $this->max = $this->escalas[4]->valor_fin;

            foreach ($this->escalas as $escala) {
                $get_color = json_decode($escala->descripcion);

                array_push($data_color, getValorColor($this->unidad, $this->min, $this->max, $this->meta, $escala->valor_fin, $get_color->{'color'}));
                array_push($data_serie, $escala->valor_fin);
            }
        } else {
            $this->meta = 0;
        }

        $ver_valor = Resultado::where('indicador_id', $this->indicador_id)
            ->where('anio_id', $this->anio)
            ->where('param_periodo', $this->periodo)
            ->select('valor_indicador')
            ->get();

        if (count($ver_valor)) {
            $this->valor = $ver_valor->first()->valor_indicador;
            $data_valor = $this->valor;
        } else {
            $data_valor = 0;
            $this->valor = $data_valor;
        }

        $this->emit('listaGroup', $this->index);
        $this->emit('verVelocimetro', $this->unidad, $data_color, $this->min, $this->meta, $data_valor, $data_serie);
        $this->emit('selectProceso');
*/
    }

    public function buscarIndicador()
    {
        switch ($this->indicacion) {
            case 1:
                $this->obtenerProcesos();
                break;

            case 2:
                $this->obtenerIndicadores();
                break;
        }
    }

    private function gcd($a, $b)
    {
        if ($a == 0 || $b == 0)
            return abs(max(abs($a), abs($b)));
        $r = $a % $b;
        return ($r != 0) ? $this->gcd($b, $r) : abs($b);
    }

    private function gcd_array($array, $a = 0)
    {
        $b = array_pop($array);
        return ($b === null) ? (int) $a : $this->gcd_array($array, $this->gcd($a, $b));
    }

    private function obtenerDivisor($arr)
    {
        return $this->gcd_array($arr);
    }
}
