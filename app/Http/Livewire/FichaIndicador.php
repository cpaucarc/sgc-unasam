<?php

namespace App\Http\Livewire;

use App\Models\Indicador;
use App\Models\Macroproceso;
use Livewire\Component;
use function view;

class FichaIndicador extends Component
{

    public $versiones = [];

    public function mount()
    {
        $this->members = "members";
    }
    public $showDiv = false;


    public function render()
    {
        return view('sgc.macroprocesos.ficha');
    }

    public function ficha(Macroproceso $macroproceso)
    {
        /*$breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['link' => "/macroprocesos/" . $macroproceso->id, 'name' => "Macroproceso"],
            ['name' => "Caracterización"]
        ];
        return view('sgc.macroprocesos.caracterizacion', compact('macroproceso', 'breadcrumbs'));*/

        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            //['link' => "/indicadores/" . $indicador->id, 'name' => "Indicadores"],
            ['name' => "Indicadores"]
        ];

        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        setAnioIndicadorActual(null);

        $indicadores = Indicador::whereMacroproceso_id($macroproceso->id)
            ->where(function ($q) {
                $q->where('codigo', '=', "''")
                    ->orWhere(function ($q) {
                        $q->where('codigo', '!=', "''")->where('version_id', function ($q) {
                            $q->select('v.id')->from('indicadors as i')->join('versions as v', 'v.id', '=', 'i.version_id')
                                ->whereRaw('i.codigo = indicadors.codigo')->orderBy('v.numero', 'desc')->limit(1);
                        });
                    });
            })->get();

        return view('sgc.macroprocesos.ficha', compact('macroproceso', 'indicadores', 'breadcrumbs', 'pageConfigs'));
    }

    public function showFicha($idIndicador, $anio)
    {
        $ficha = Indicador::findOrFail($idIndicador);
        $macroProcesoFicha = Macroproceso::findOrFail($ficha->macroproceso_id);
        //dd($idIndicador);
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['link' => "/macroprocesos/" . $macroProcesoFicha->id . "/ficha", 'name' => "Indicadores"],
            ['name' => $ficha->nombre]
        ];
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];

        $versiones = Indicador::where('codigo', $ficha->codigo)->where('codigo', '!=', '')->get();

        return view('livewire.ficha-indicador.show-ficha', compact('ficha', 'macroProcesoFicha', 'breadcrumbs', 'pageConfigs', 'versiones'));
    }
}
