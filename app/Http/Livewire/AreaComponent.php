<?php

namespace App\Http\Livewire;

use App\Models\Area;
use App\Models\Detalle;
use App\Models\Interesado;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class AreaComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $orden = 'asc';
    public $selected_id, $search, $nombre, $abreviatura, $param_nivel, $param_tipoorgano, $area_id;
    public $updateMode = false;
    public $showMode = false;
    public $sort = 'areas.id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'nombre'       => 'required|unique:areas',
        'abreviatura'   => 'required|unique:areas',
        //'area_id'   => 'required',
        'param_nivel' => 'required',
        'param_tipoorgano' => 'required'
    ];
    protected $msjError=[
        'nombre.required' => 'El campo de nombre es obligatorio.',
        'abreviatura.required' => 'El campo de abreviatura es obligatorio.',
        'param_nivel.required' => 'El campo de nivel es obligatorio.',
        'param_tipoorgano.required' => 'El campo de Tipo de órgano es obligatorio.'
    ];

    public function render()
    {
        $parametro_nivel = Detalle::whereparametro_id(4)->whereActivo(1)->get();
        $parametro_tipoorgano = Detalle::whereparametro_id(6)->whereActivo(1)->get();
        $param_area = Area::whereActivo(1)->get();

        $areas = Area::select(DB::raw('areas.id as id, areas.nombre, areas.abreviatura as abreviatura, areas.area_id, areas.param_nivel, areas.param_tipoorgano, areas.activo,
        areas.created_user, areas.updated_at, areas.created_at,(SELECT nombre FROM areas t1 WHERE t1.id = areas.area_id) as abreviaturaOrigen'))
        //'areas.created_user', 'areas.updated_at', 'areas.created_at', 'aa.nombre as areaOrigen', 'aa.abreviatura as abreviaturaOrigen')
        //    ->leftjoin('areas as aa', 'areas.id', '=', 'aa.area_id')
            ->where('areas.nombre', 'like', '%' . $this->search . '%')
            ->orwhere('areas.abreviatura', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->orden)
            ->paginate($this->cantidad);

        return view('livewire.areas.view', compact('areas', 'parametro_nivel', 'parametro_tipoorgano', 'param_area'));
    }

    public function index()
    {
        return view('livewire.areas.index');
    }

    private function resetInput()
    {
        $this->open = false;
        $this->nombre = null;
        $this->abreviatura = null;
        $this->param_nivel = null;
        $this->param_tipoorgano = null;
        $this->area_id = null;
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->showMode = false;
        $this->open = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->showMode = false;
    }

    public function store()
    {
        $this->validate($this->rules, $this->msjError);

        $areaSave =Area::create([
            'nombre' => $this->nombre,
            'abreviatura' => $this->abreviatura,
            'param_nivel' => $this->param_nivel,
            'param_tipoorgano' => $this->param_tipoorgano,
            'area_id' => $this->area_id,
            'created_user' => Auth::user()->id
        ]);

        Interesado::create([
            'nombre' => $this->nombre,
            'area_id'=> $areaSave->id,
            'created_user' => Auth::user()->id
        ]);



        $this->resetInput();
        $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Area creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);
    }

    public function edit($id)
    {
        $area = Area::findOrFail($id);
        $this->selected_id = $id;
        $this->nombre = $area->nombre;
        $this->abreviatura = $area->abreviatura;
        $this->area_id = $area->area_id;
        $this->param_nivel = $area->param_nivel;
        $this->param_tipoorgano = $area->param_tipoorgano;
        $this->updateMode = true;
    }

    public function show($id)
    {
        $area = Area::findOrFail($id);
        $this->selected_id = $id;
        $this->nombre = $area->nombre;
        $this->abreviatura = $area->abreviatura;
        $this->area_id = $area->area_id;
        $this->param_nivel = $area->param_nivel;
        $this->param_tipoorgano = $area->param_tipoorgano;
        $this->showMode = true;
    }

    public function update()
    {
        $this->validate([
            'nombre' => 'required|unique:areas,nombre,'.$this->selected_id,
            'abreviatura' => 'required|unique:areas,abreviatura,'.$this->selected_id,
            'param_nivel' => 'required',
            'param_tipoorgano' => 'required'
        ]);

        if ($this->selected_id) {
            $record = Area::find($this->selected_id);
            $record->update([
                'nombre' => $this->nombre,
                'abreviatura' => $this->abreviatura,
                'param_nivel' => $this->param_nivel,
                'param_tipoorgano' => $this->param_tipoorgano,
                'area_id' => $this->area_id,
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;

            $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Área actualizada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Area::where('id', $id);
            $record->delete();
            $datos1 = [
            'tipo' => 'error',
            'mensaje' => 'El Area ha sido eliminado.'
        ];
        $this->emit('alertRespuesta', $datos1);
        }
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->orden == 'desc') {
                $this->orden = 'asc';
            } else {
                $this->orden = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->orden = 'asc';
        }
    }
}
