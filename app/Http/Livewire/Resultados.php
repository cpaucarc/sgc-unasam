<?php

namespace App\Http\Livewire;


use App\Models\Anio;
use App\Models\Detalle;
use App\Models\Indicador;
use App\Models\Resultado;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;

class Resultados extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $selected_id, $search, $param_periodo, $valor_numerador, $valor_denominador, $valor_indicador, $indicador_id, $anio_id, $activo;
    public $updateMode = false;
    public $sort = 'id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];

    protected $rules = [
        'param_periodo' => 'required',
        'valor_numerador' => 'required',
        'valor_denominador' => 'required',
        'valor_indicador' => 'required',
        'indicador_id' => 'required',
        'anio_id' => 'required'
    ];

    public function render()
    {
        $anios = Anio::whereActivo(1)->orderBy('anio', 'asc')->get();
        $periodos = Detalle::whereparametro_id(13)->whereActivo(1)->get();
        $indicadores = Indicador::whereActivo(1)->get();

        $resultados = Resultado::where('param_periodo', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);

        return view('livewire.resultados.view', compact('resultados', 'anios', 'periodos', 'indicadores'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->param_periodo = null;
        $this->valor_numerador = null;
        $this->valor_denominador = null;
        $this->valor_indicador = null;
        $this->indicador_id = null;
        $this->anio_id = null;
        $this->activo = null;
    }

    public function store()
    {
        $this->validate();

        Resultado::create([
            'param_periodo' => $this->param_periodo,
            'valor_numerador' => $this->valor_numerador,
            'valor_denominador' => $this->valor_denominador,
            'valor_indicador' => $this->valor_indicador,
            'indicador_id' => $this->indicador_id,
            'anio_id' => $this->anio_id,
            'activo' => $this->activo,
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();
        $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Resultado creado satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);
    }

    public function edit($id)
    {
        $resultado = Resultado::findOrFail($id);

        $this->selected_id = $id;
        $this->param_periodo = $resultado->param_periodo;
        $this->valor_numerador = $resultado->valor_numerador;
        $this->valor_denominador = $resultado->valor_denominador;
        $this->valor_indicador = $resultado->valor_indicador;
        $this->indicador_id = $resultado->indicador_id;
        $this->anio_id = $resultado->anio_id;
        $this->activo = $resultado->activo;
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate();

        if ($this->selected_id) {
            $resultado = Resultado::find($this->selected_id);
            $resultado->update([
                'param_periodo' => $this->param_periodo,
                'valor_numerador' => $this->valor_numerador,
                'valor_denominador' => $this->valor_denominador,
                'valor_indicador' => $this->valor_indicador,
                'indicador_id' => $this->indicador_id,
                'anio_id' => $this->anio_id,
                'activo' => $this->activo,
                'upated_user' => Auth::user()->id
            ]);

            $this->resetInput();
            $this->updateMode = false;

            $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Resultado actualizado satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $resultado = Resultado::where('id', $id);
            $resultado->delete();
            $datos1 = [
            'tipo' => 'error',
            'mensaje' => 'Resultado ha sido eliminado.'
        ];
        $this->emit('alertRespuesta', $datos1);
        }
    }

    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }
}
