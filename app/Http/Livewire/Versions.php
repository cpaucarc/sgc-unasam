<?php

namespace App\Http\Livewire;

use App\Models\Detalle;
use App\Models\Version;
use App\Models\Persona;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class Versions extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $selected_id, $search, $numero, $fecha_vigencia, $descripcion, $per_elaboro, $per_reviso, $per_aprobado, $fecha_aprobacion, $activo, $nivel_proceso;
    public $updateMode = false;
    public $showMode = false;
    public $sort = 'id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy', 'storeVersion'];
    protected $rules = [
        'nivel_proceso' => 'required',
        'numero' => 'required',
        'fecha_vigencia' => 'required',
        'descripcion' => 'required',
        'per_elaboro' => 'required',
        'per_reviso' => 'required',
        'per_aprobado' => 'required',
        'fecha_aprobacion' => 'required'
    ];
    protected $msjError=[
        'nivel_proceso.required' => 'El campo tipo de version es obligatorio',
        'numero.required' => 'El campo número es obligatorio',
        'fecha_vigencia.required' => 'El campo fecha de vigencia es obligatorio',
        'descripcion.required' => 'El campo descripción es obligatorio.',
        'per_elaboro.required' => 'El campo elaborado por es obligatorio',
        'per_reviso.required' => 'El campo revisado por es obligatorio',
        'per_aprobado.required' => 'El campo aprobado por es obligatorio',
        'fecha_aprobacion.required' => 'El campo fecha de aprobado es obligatorio'
    ];

    public function render()
    {   
        $nivel_procesos= Detalle::whereparametro_id(15)->whereActivo(1)->whereNotIn('id',[15005])->get();
        $parametro_personas = Persona::whereActivo(1)->get();
        $versions = Version::where('numero', 'LIKE', '%' . 
        $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);
        //dd($versions);
        return view('livewire.versions.view', compact('versions','parametro_personas', 'nivel_procesos'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->showMode = false;
        $this->open = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->showMode = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->nivel_proceso = null;
        $this->numero = null;
        $this->fecha_vigencia = null;
        $this->descripcion = null;
        $this->per_elaboro = null;
        $this->per_reviso = null;
        $this->per_aprobado = null;
        $this->fecha_aprobacion = null;
        $this->emit('resetearSelect');
    }

    public function store()
    {
        //dd('Hola');
        $this->validate($this->rules,$this->msjError);

        Version::create([
            'nivel_proceso' => $this->nivel_proceso,
            'numero' => $this->numero,
            'fecha_vigencia' => date('Y-m-d', strtotime($this->fecha_vigencia)),
            'descripcion' => $this->descripcion,
            'per_elaboro' => $this->per_elaboro,
            'per_reviso' => $this->per_reviso,
            'per_aprobado' => $this->per_aprobado,
            'fecha_aprobacion' => date('Y-m-d', strtotime($this->fecha_aprobacion)),
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();

        $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Versión creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);
    }

    public function edit($id)
    {
        $version = Version::findOrFail($id);
        //dd( $version->per_elaboro);

        $this->selected_id = $id;
        $this->nivel_proceso = $version->nivel_proceso;
        $this->numero = $version->numero;
        $this->fecha_vigencia = date("d-m-Y", strtotime($version->fecha_vigencia));
        $this->descripcion = $version->descripcion;
        $this->per_elaboro = $version->per_elaboro;
        $this->per_reviso = $version->per_reviso;
        $this->per_aprobado = $version->per_aprobado;
        $this->fecha_aprobacion = date("d-m-Y", strtotime($version->fecha_aprobacion)) ;
        //$this->activo = $version->activo;
        $this->updateMode = true;

        $this->emit('asignarDatos', $this->per_elaboro, $this->per_reviso, $this->per_aprobado);
    }

    public function update()
    {
        $this->validate();

        if ($this->selected_id) {
            $record = Version::find($this->selected_id);
            $record->update([
                'numero' => $this->numero,
                'fecha_vigencia' => date('Y-m-d', strtotime($this->fecha_vigencia)),
                'descripcion' => $this->descripcion,
                'per_elaboro' => $this->per_elaboro,
                'per_reviso' => $this->per_reviso,
                'per_aprobado' => $this->per_aprobado,
                'fecha_aprobacion' => date('Y-m-d', strtotime($this->fecha_aprobacion)),
                //'activo' => $this->activo,
                'upated_user' => Auth::user()->id
            ]);

            $this->resetInput();
            $this->updateMode = false;

            $datos1 = [
                'tipo' => 'success',
                'mensaje' => 'Versión actualizada satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }

    public function show($id)
    {
        $version = Version::findOrFail($id);
        //dd( $version->per_elaboro);

        $this->selected_id = $id;
        $this->nivel_proceso = $version->nivel_proceso;
        $this->numero = $version->numero;
        $this->fecha_vigencia = date("d-m-Y", strtotime($version->fecha_vigencia));
        $this->descripcion = $version->descripcion;
        $this->per_elaboro = $version->per_elaboro;
        $this->per_reviso = $version->per_reviso;
        $this->per_aprobado = $version->per_aprobado;
        $this->fecha_aprobacion = date("d-m-Y", strtotime($version->fecha_aprobacion)) ;
        //$this->activo = $version->activo;
        $this->showMode = true;

        //$this->emit('asignarDatos', $this->per_elaboro, $this->per_reviso, $this->per_aprobado);
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Version::where('id', $id);
            $record->delete();
            $datos1 = [
                'tipo' => 'error',
                'mensaje' => 'La versión se ha sido eliminado.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }
}
