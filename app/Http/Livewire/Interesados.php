<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Interesado;
use App\Models\Area;
use Illuminate\Support\Facades\Auth;

class Interesados extends Component
{

    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $selected_id, $search, $nombre,$descripcion,$interno,$area_id, $activo;
    public $updateMode = false;
    public $showMode = false;
    public $sort = 'id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'nombre' => 'required|unique:interesados',
        'interno' => 'required'
        //'descripcion' => 'required',
    ];

    protected $msjError=[
        'nombre.required' => 'El campo de nombre es obligatorio.',
        'interno.required' => 'El campo interno es obligatorio.'
    ];

    public function render()
    {
        $interesados = Interesado::where('nombre', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);
        $areas = Area::all();

        $search = '%' . $this->search . '%';
        return view('livewire.interesados.view',compact('interesados','areas'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->showMode = false;
        $this->open = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->nombre = null;
        $this->descripcion = null; 
        $this->interno = null;  
        $this->area_id = null;        
        $this->activo = null;
    }

    public function store()
    {
        $this->validate($this->rules, $this->msjError);

        Interesado::create([
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'interno' => $this->interno,
            'area_id' => NULL,// $this->area_id,
            'activo' =>'1',
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();
        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El Interesado ha sido creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
        
    }

    public function edit($id)
    {
        $miInteresado = Interesado::findOrFail($id);
        $this->selected_id = $id;
        $this->nombre = $miInteresado->nombre;
        $this->descripcion = $miInteresado->descripcion;  
        $this->interno = $miInteresado->interno;
        $this->area_id = $miInteresado->area_id;         
        $this->activo = $miInteresado->activo;
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
            'nombre' => 'required|unique:interesados,nombre,'.$this->selected_id,          
            'interno' => 'required' 
        ]);
        if ($this->selected_id) {
            $miInteresado = Interesado::findOrFail($this->selected_id);
            $miInteresado->update([
                'nombre' => $this->nombre,
                'descripcion' => $this->descripcion,
                'interno' => $this->interno,
                'area_id' => NULL,// $this->area_id,
                'activo' =>'1',
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;
            $datos = [
                'tipo' => 'success',
                'mensaje' => 'El Interesado ha sido actualizado satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos);
        }
        
    }

    public function destroy($id)
   {
        if ($id) {
            $record = Interesado::find($id);
            $record->deleted_user = Auth::user()->id;
            $record->save();            
            $record->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'El Interesado ha sido creadeliminadoa satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }  

    public function show($id)
    {
        $miInteresado = Interesado::findOrFail($id);
        $this->selected_id = $id;
        $this->nombre = $miInteresado->nombre;
        $this->descripcion = $miInteresado->descripcion;  
        $this->interno = $miInteresado->interno;
        $this->area_id = $miInteresado->area_id;         
        $this->activo = $miInteresado->activo;
        $this->showMode = true;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->showMode = false;
    }
   
}
