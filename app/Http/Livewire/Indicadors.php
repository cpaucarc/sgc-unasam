<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Indicador;
use App\Models\Microproceso;
use App\Models\Proceso;
use App\Models\Subproceso;
use App\Models\Macroproceso;
use App\Models\Detalle;
use App\Models\Area;
use App\Models\Version;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class Indicadors extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $openShow = false;
    public $cantidad = 10;
    public $showDiv = false;
    public $selected_id, $search, $codigo, $nombre, $param_tipoindicador, $version_id, $definicion, $objetivo, $es_general, $macroproceso_id, $proceso_id, $subproceso_id, $microproceso_id, $activo;
    public $updateMode = false;
    public $clickUpdate = false;
    public $sort = 'codigo';
    public $direction = 'asc';
    //combos dependienente
    public $audits = [];
    public $procesos = [];
    public $subprocesos = [];
    public $microprocesos = [];
    public $tipo_indicador;

    protected $listeners = ['render', 'destroy', 'storeIndicador'];
    protected $rules = [
        // 'codigo' => 'required|unique:indicadors',
        // 'nombre' => 'required|unique:indicadors',
        'param_tipoindicador' => 'required',
        'version_id' => 'required',
        'macroproceso_id' => 'required',
    ];
    protected $msjError = [
        'codigo.required' => 'El campo código es obligatorio.',
        'codigo.unique' => 'La combinación de codigo y versión ya está en uso.',
        'nombre.required' => 'El campo nombre es obligatorio.',
        'nombre.unique' => 'El valor del campo nombre ya está en uso.',
        'param_tipoindicador.required' => 'El campo tipo indicador es obligatorio.',
        'version_id.required' => 'El campo versión es obligatorio.',
        'macroproceso_id.required' => 'El campo macroproceso es obligatorio.',
    ];

    public function render()
    {
        $indicadores = Indicador::where('nombre', 'LIKE', '%' . $this->search . '%')
            ->orWhere('codigo', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);

        $macroprocesos = Macroproceso::whereActivo(1)->get();
        // $procesos= Proceso::whereActivo(1)->get();
        // $subprocesos= Subproceso::whereActivo(1)->get();
        // $microprocesos= Microproceso::whereActivo(1)->get();
        $tipoIndicadores = Detalle::whereparametro_id(10)->whereActivo(1)->get();
        $versiones = Version::whereActivo(1)->where('nivel_proceso', 15006)->orderBy('numero', 'DESC')->get();
        $search = '%' . $this->search . '%';
        return view('livewire.indicadors.view', compact('indicadores', 'macroprocesos', 'tipoIndicadores', 'versiones'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
        $this->openShow = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->openShow = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->codigo = null;
        $this->nombre = null;
        $this->param_tipoindicador = null;
        $this->version_id = null;
        $this->definicion = null;
        $this->objetivo = null;
        $this->es_general = null;
        $this->macroproceso_id = null;
        $this->proceso_id = null;
        $this->subproceso_id = null;
        $this->microproceso_id = null;
        $this->activo = null;
    }

    public function store()
    {
        $this->validate($this->rules, $this->msjError);

        switch ($this->existeCodigo()) {
            case 0:
                $this->storeIndicador();
                break;
            case 1:
                $this->emit('existeIndicador');
                return "ok";
                break;
            case 2:
                $this->emit('existeIndicadorVersion', $this->codigo);
                break;
        }
    }
    public function storeIndicador()
    {
        Indicador::create([
            'codigo' => $this->codigo,
            'nombre' => $this->nombre,
            'param_tipoindicador' => $this->param_tipoindicador,
            'version_id' => $this->version_id,
            'definicion' => $this->definicion,
            'objetivo' => $this->objetivo,
            'es_general' => $this->es_general,
            'macroproceso_id' => $this->macroproceso_id,
            'proceso_id' => $this->proceso_id,
            'subproceso_id' => $this->subproceso_id,
            'microproceso_id' => $this->microproceso_id,
            'activo' => '1',
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();
        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El Indicador ha sido creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }

    public function edit($id)
    {
        $miIndicador = Indicador::findOrFail($id);
        $this->selected_id = $id;
        $this->codigo = $miIndicador->codigo;
        $this->nombre = $miIndicador->nombre;
        $this->param_tipoindicador = $miIndicador->param_tipoindicador;
        $this->version_id = $miIndicador->version_id;
        $this->definicion = $miIndicador->definicion;
        $this->objetivo = $miIndicador->objetivo;
        $this->es_general = $miIndicador->es_general;
        $this->macroproceso_id = $miIndicador->macroproceso_id;
        $this->proceso_id = $miIndicador->proceso_id;
        $this->subproceso_id = $miIndicador->subproceso_id;
        $this->microproceso_id = $miIndicador->microproceso_id;
        $this->activo = $miIndicador->activo;
        $this->updateMode = true;
        $this->clickUpdate = true;
        $this->setProcesoSelect();
        $this->setSubProcesoSelect();
        $this->setMicroProcesoSelect();
        $this->clickUpdate = false;
    }

    public function show($id)
    {
        $miIndicador = Indicador::findOrFail($id);

        $this->selected_id = $id;
        $this->codigo = $miIndicador->codigo;
        $this->nombre = $miIndicador->nombre;
        $this->param_tipoindicador = $miIndicador->param_tipoindicador;
        $this->version_id = $miIndicador->version_id;
        $this->definicion = $miIndicador->definicion;
        $this->objetivo = $miIndicador->objetivo;
        $this->es_general = $miIndicador->es_general;
        $this->macroproceso_id = $miIndicador->macroproceso_id;
        $this->proceso_id = $miIndicador->proceso_id;
        $this->subproceso_id = $miIndicador->subproceso_id;
        $this->microproceso_id = $miIndicador->microproceso_id;
        $this->activo = $miIndicador->activo;
        $this->openShow = true;

        $this->audits = [];
    }

    public function audit($id)
    {
        $miIndicador = Indicador::findOrFail($id);

        $this->selected_id = $id;
        $this->codigo = $miIndicador->codigo;
        $this->nombre = $miIndicador->nombre;
        $this->param_tipoindicador = $miIndicador->param_tipoindicador;
        $this->version_id = $miIndicador->version_id;
        $this->definicion = $miIndicador->definicion;
        $this->objetivo = $miIndicador->objetivo;
        $this->es_general = $miIndicador->es_general;
        $this->macroproceso_id = $miIndicador->macroproceso_id;
        $this->proceso_id = $miIndicador->proceso_id;
        $this->subproceso_id = $miIndicador->subproceso_id;
        $this->microproceso_id = $miIndicador->microproceso_id;
        $this->activo = $miIndicador->activo;
        $this->openShow = true;

        $this->audits = $miIndicador->audits;

        $this->emit('updateTooltips');
    }

    public function update()
    {
        $this->validate([
            'codigo' => [
                'required',
                Rule::unique('indicadors')->ignore($this->selected_id)->where(fn ($query) => $query->where('codigo', $this->codigo)->where('version_id', $this->version_id))
            ],
            // 'nombre' => 'required|unique:indicadors,nombre,' . $this->selected_id,
            'param_tipoindicador' => 'required',
            'version_id' => 'required',
            //'definicion' => 'required',
            // 'objetivo' => 'required',
            //'es_general' => 'required',
            'macroproceso_id' => 'required',
            // 'proceso_id' => 'required',
            // 'subproceso_id' => 'required',
            // 'microproceso_id' => 'required',
        ], $this->msjError);

        $this->updateIndicador();
    }

    public function updateIndicador()
    {
        if ($this->selected_id) {
            $miIndicador = Indicador::findOrFail($this->selected_id);
            $miIndicador->update([
                'codigo' => $this->codigo,
                'nombre' => $this->nombre,
                'param_tipoindicador' => $this->param_tipoindicador,
                'version_id' => $this->version_id,
                'definicion' => $this->definicion,
                'objetivo' => $this->objetivo,
                'es_general' => $this->es_general,
                'macroproceso_id' => $this->macroproceso_id,
                'proceso_id' => $this->proceso_id,
                'subproceso_id' => $this->subproceso_id,
                'microproceso_id' => $this->microproceso_id,
                'activo' => '1',
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;
            //$this->emit('alert', 'Indicador actualizado satisfactoriamente.');
            $datos = [
                'tipo' => 'success',
                'mensaje' => 'El Indicador ha sido actualizado satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Indicador::find($id);
            $record->deleted_user = Auth::user()->id;
            $record->save();
            $record->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'El Indicador ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }

    public function setProcesoSelect()
    {
        if (!$this->clickUpdate) {
            $this->proceso_id = null;
            $this->procesos = [];
            $this->subproceso_id = null;
            $this->subprocesos = [];
            $this->microproceso_id = null;
            $this->microprocesos = [];
        }
        if ($this->macroproceso_id != null) {
            $this->procesos = Proceso::whereActivo(1)->whereMacroproceso_id($this->macroproceso_id)->get();
        }
    }
    public function setSubProcesoSelect()
    {
        if (!$this->clickUpdate) {
            $this->subproceso_id = null;
            $this->subprocesos = [];
            $this->microproceso_id = null;
            $this->microprocesos = [];
        }
        if ($this->proceso_id != null) {
            $this->subprocesos = Subproceso::whereActivo(1)->whereProceso_id($this->proceso_id)->get();
        }
    }

    public function setMicroProcesoSelect()
    {
        if (!$this->clickUpdate) {
            $this->microproceso_id = null;
            $this->microprocesos = [];
        }
        if ($this->proceso_id != null) {
            $this->microprocesos = Microproceso::whereActivo(1)->whereSubproceso_id($this->subproceso_id)->get();
        }
    }

    public function ficha($id)
    {

        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            //['link' => "/indicadores/" . $indicador->id, 'name' => "Indicadores"],
            ['name' => "Indicadores"]
        ];

        $indicadores = Indicador::whereMacroproceso_id($id)->get();
        //dd($indicadores);
        //$indicador = Indicador::findOrFail($id);
        //$indicador = Indicador::findOrFail($id)->macroproceso()->get();
        return view('sgc.macroprocesos.ficha', compact('indicadores', 'breadcrumbs'));
    }

    public function existeCodigo()
    {
        // return 0 si no existe
        // return 1 si existe el código y la versión
        // return 2 si existe el doigo pero no la versión
        $indicador = Indicador::whereCodigo($this->codigo)->whereVersion_id($this->version_id)->get();
        if ($indicador->count() > 0) {
            return 1;
        } else {
            $version_diferente = Indicador::whereCodigo($this->codigo)->where('version_id', '!=', $this->version_id)->get();
            if ($version_diferente->count() > 0) {
                return 2;
            }
        }
        return 0;
    }
}
