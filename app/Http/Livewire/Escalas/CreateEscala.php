<?php

namespace App\Http\Livewire\Escalas;

use App\Models\Persona;
use Livewire\Component;

class CreateEscala extends Component
{
    public function render()
    {
        return view('livewire.escalas.create-escala');
    }

    public $open = false;
    public $nombres, $ape_paterno, $ape_materno;

    public function save()
    {
        $persona = new Persona;
        $persona->nombres = $this->nombres;
        $persona->ape_paterno = $this->ape_paterno;
        $persona->ape_materno = $this->ape_materno;
        $persona->nro_documento = '12345678';
        $persona->save();

        $this->reset(['open', 'nombres', 'ape_paterno', 'ape_materno']);
        $this->emitTo('livewire.personas.show-personas', 'render');
        $this->emit('alert', "Se ha creado a la persona satisfactoriamente.");
    }
}
