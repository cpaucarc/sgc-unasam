<?php

namespace App\Http\Livewire\Escalas;

use App\Models\Escala;
use Livewire\Component;
use Livewire\WithPagination;

class ShowEscalas extends Component
{
    use WithPagination;

    public $search;
    public $sort = 'id';
    public $detalle = 'desc';
    public $cantidad = 10;

    protected $listiners = ['render' => 'render'];
    public function render()
    {
        $escalas = Escala::where('param_likert', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->detalle)
            ->paginate($this->cantidad);
        return view('livewire.escalas.show-escalas', compact('escalas'));
    }

    public function index()
    {
        return view('sgc.escalas.index');
    }

}
