<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Parametro;
use Illuminate\Support\Facades\Auth;

class Parametros extends Component
{

    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $selected_id, $search, $parametro, $descripcion, $activo;
    public $updateMode = false;
    public $sort = 'id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'parametro' => 'required|unique:parametros',
        //'descripcion' => 'required',
    ];
    protected $msjError=[
        'parametro.required' => 'El campo parámetro es obligatorio.',
        'parametro.unique' => 'El valor del campo parámetro ya está en uso.'
    ];

    public function render()
    {
        $parametros = Parametro::where('parametro', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);

        $search = '%' . $this->search . '%';
        return view('livewire.parametros.view', compact('parametros'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->parametro = null;
        $this->descripcion = null;       
        $this->activo = null;
    }

    public function store()
    {
        $this->validate($this->rules,$this->msjError);

        Parametro::create([
            'parametro' => $this->parametro,
            'descripcion' => $this->descripcion,
            'activo' =>'1',
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();
        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El Parámetro ha sido creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos); 
    }

    public function edit($id)
    {
        $miParametro = Parametro::findOrFail($id);

        $this->selected_id = $id;
        $this->parametro = $miParametro->parametro;
        $this->descripcion = $miParametro->descripcion;      
        $this->activo = $miParametro->activo;
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
            'parametro' => 'required|unique:parametros,parametro,'.$this->selected_id,            
        ],$this->msjError);
        if ($this->selected_id) {
            $miParametro = Parametro::findOrFail($this->selected_id);
            $miParametro->update([
                'parametro' => $this->parametro,
                'descripcion' => $this->descripcion,
                'activo' =>'1',
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;
            $datos = [
                'tipo' => 'success',
                'mensaje' => 'El Parámetro ha sido actualizado satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos); 
        }
        
    }

    public function destroy($id)
   {
        if ($id) {
            $record = Parametro::find($id);
            $record->deleted_user = Auth::user()->id;
            $record->save();
            $record->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'El Parámetro ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }  

    
}
