<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Macroproceso;
use App\Models\Microproceso;
use App\Models\Proceso;
use App\Models\Subproceso;

class ManualProceso extends Component
{
    public $datos;
    public $macroproceso_id = null, $proceso_id = null;
    public $procesos = [], $subprocesos = [];
    public $tipo_proceso = 'MACROPROCESOS';

    protected $listeners = ['verProcesos', 'verSubprocesos'];

    public function mount()
    {
        $this->datos =  Macroproceso::whereActivo(1)->get();
    }

    public function render()
    {
        $macroprocesos = Macroproceso::whereActivo(1)->get();
        return view('livewire.manuales.view', compact('macroprocesos'));
    }
    public function verMacroprocesos()
    {
        $this->tipo_proceso = 'MACROPROCESOS';
        $this->datos =  Macroproceso::whereActivo(1)->get(); 
        $this->emit('resetearSelect');
    }
    public function verProcesos()
    {
        $this->tipo_proceso = 'PROCESOS';
        $this->datos = Proceso::whereActivo(1)->whereMacroproceso_id($this->macroproceso_id)->get();
        $this->emit('mostrarProcesos');
    }
}
