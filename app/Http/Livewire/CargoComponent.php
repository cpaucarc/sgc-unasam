<?php

namespace App\Http\Livewire;

use App\Models\Area;
use App\Models\Cargo;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class CargoComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $showMode = false;
    public $cantidad = 10;
    public $orden = 'asc';
    public $selected_id, $search, $nombre, $descripcion;
    public $updateMode = false;
    public $sort = 'nombre';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'nombre'       => 'required|unique:cargos',
        //'descripcion'   => 'required'
    ];

    protected $msjError=[
        'nombre.required' => 'El campo cargo es obligatorio'
    ];

    public function render()
    {
        $cargos = Cargo::where('nombre', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->orden)
            ->paginate($this->cantidad);
        return view('livewire.cargos.view', compact('cargos'));
    }

    public function index()
    {
        return view('livewire.cargos.index');
    }

    private function resetInput()
    {
        $this->open = false;
        $this->nombre = null;
        $this->descripcion = null;
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->showMode = false;
        $this->open = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->showMode = false;
    }

    public function store()
    {
        $this->validate($this->rules,$this->msjError);
        Cargo::create([
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();
        $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Cargo creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);
    }

    public function edit($id)
    {
        $persona = Cargo::findOrFail($id);
        $this->selected_id = $id;
        $this->nombre = $persona->nombre;
        $this->descripcion = $persona->descripcion;
        $this->updateMode = true;
    }

    public function show($id)
    {
        $persona = Cargo::findOrFail($id);
        $this->selected_id = $id;
        $this->nombre = $persona->nombre;
        $this->descripcion = $persona->descripcion;
        $this->showMode = true;
    }

    public function update()
    {
        $this->validate([
            //'descripcion' => 'required|',
            'nombre' => 'required|unique:cargos,nombre,'.$this->selected_id
        ]);

        if ($this->selected_id) {
            $record = Cargo::find($this->selected_id);
            $record->update([
                'nombre' => $this->nombre,
                'descripcion' => $this->descripcion,
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;
            $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Cargo actualizada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Cargo::where('id', $id);
            $record->delete();
            $datos1 = [
            'tipo' => 'error',
            'mensaje' => 'Cargo eliminado satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);
        }
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->orden == 'desc') {
                $this->orden = 'asc';
            } else {
                $this->orden = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->orden = 'asc';
        }
    }
}
