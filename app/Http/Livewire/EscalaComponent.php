<?php

namespace App\Http\Livewire;

use App\Models\Area;
use App\Models\Detalle;
use App\Models\Escala;
use App\Models\Medicion;
use App\Models\Meta;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class EscalaComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $orden = 'asc';
    public $selected_id, $search, $param_likert, $valor_ini, $valor_fin, $medicion_id, $param_tipoorgano;
    public $updateMode = false;
    public $showMode = false;
    public $sort = 'escalas.id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'param_likert'       => 'required',
        'valor_ini'   => 'required',
        'valor_fin'   => 'required',
        'medicion_id' => 'required'
    ];
    protected $msjError=[
        'param_likert.required' => 'El campo parámetro es obligatorio',
        'valor_ini.required' => 'El campo valor inicial es obligatorio.',
        'valor_fin.required' => 'El campo valor final es obligatorio',
        'medicion_id.required' => 'El campo medición es obligatorio.'
    ];

    public function render()
    {
        $parametro_likerts = Detalle::whereparametro_id(14)->whereActivo(1)->get();
        $parametro_mediciones = Meta::whereActivo(1)->get();

        $escalas = Escala::select(DB::raw('escalas.id as id, escalas.param_likert param_likert, escalas.valor_ini as valor_ini, escalas.valor_fin as valor_fin, escalas.activo,
        escalas.created_user, escalas.updated_at, escalas.created_at,(SELECT detalle FROM detalles t1 WHERE t1.id = escalas.param_likert) as dParamLikert, metas.meta '))
            ->join('metas', 'escalas.meta_id', '=', 'metas.id')
            ->where('escalas.valor_ini', 'like', '%' . $this->search . '%')
            ->orwhere('escalas.valor_fin', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->orden)
            ->paginate($this->cantidad);

        return view('livewire.escalas.view', compact('escalas', 'parametro_likerts', 'parametro_mediciones'));
    }

    public function index()
    {
        return view('livewire.escalas.index');
    }

    private function resetInput()
    {
        $this->open = false;
        $this->param_likert = null;
        $this->medicion_id = null;
        $this->valor_ini = null;
        $this->valor_fin = null;
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->showMode = false;
        $this->open = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->showMode = false;
    }

    public function store()
    {
        $this->validate($this->rules,$this->msjError);
        Escala::create([
            'param_likert' => $this->param_likert,
            'valor_ini' => $this->valor_ini,
            'valor_fin' => $this->valor_fin,
            'meta_id' => $this->medicion_id,
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();
        $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Escala creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);
    }

    public function edit($id)
    {
        $area = Escala::findOrFail($id);
        $this->selected_id = $id;
        $this->valor_ini = $area->valor_ini;
        $this->valor_fin = $area->valor_fin;
        $this->medicion_id = $area->meta_id;
        $this->param_likert = $area->param_likert;
        $this->updateMode = true;
    }

    public function show($id)
    {
        $area = Escala::findOrFail($id);
        $this->selected_id = $id;
        $this->valor_ini = $area->valor_ini;
        $this->valor_fin = $area->valor_fin;
        $this->medicion_id = $area->meta_id;
        $this->param_likert = $area->param_likert;
        $this->showMode = true;
    }

    public function update()
    {
        $this->validate([
            'param_likert'       => 'required',
            'valor_ini'   => 'required',
            'valor_fin'   => 'required',
            'medicion_id' => 'required'
        ]);

        if ($this->selected_id) {
            $record = Escala::find($this->selected_id);
            $record->update([
                'param_likert' => $this->param_likert,
                'valor_ini' => $this->valor_ini,
                'valor_fin' => $this->valor_fin,
                'medicion_id' => $this->medicion_id,
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;

            $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Escala actualizada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Escala::where('id', $id);
            $record->delete();
            $datos1 = [
            'tipo' => 'error',
            'mensaje' => 'Escala ha sido eliminado.'
        ];
        $this->emit('alertRespuesta', $datos1);
        }
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->orden == 'desc') {
                $this->orden = 'asc';
            } else {
                $this->orden = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->orden = 'asc';
        }
    }
}
