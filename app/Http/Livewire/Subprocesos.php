<?php

namespace App\Http\Livewire;

use App\Models\Area;
use App\Models\Proceso;
use App\Models\Version;
use Livewire\Component;
use App\Models\Subproceso;
use App\Models\Macroproceso;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class Subprocesos extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $openShow = false;
    public $cantidad = 10;
    public $macroproceso_id;
    public $selected_id, $search, $codigo, $nombre, $version_id, $descripcion, $param_tipo, $responsable_area, $objetivo, $alcance, $finalidad, $orden, $proceso_id, $activo;
    public $procesos = [];
    public $updateMode = false;
    public $sort = 'id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'codigo' => 'required',
        'nombre' => 'required',
        'version_id' => 'required',
        'descripcion' => 'required',
        // 'param_tipo' => 'required',
        'responsable_area' => 'required',
        'objetivo' => 'required',
        'alcance' => 'required',
        'finalidad' => 'required',
        'orden' => 'required',
        'macroproceso_id' => 'required',
        'proceso_id' => 'required'
    ];


    public function render()
    {
        $areas = Area::whereActivo(1)->get();
        $macroprocesos = Macroproceso::whereActivo(1)->get();
        $versions = Version::whereActivo(1)->where('nivel_proceso', 15003)->get();
        $subprocesos = Subproceso::where('nombre', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);

        return view('livewire.subprocesos.view', compact('subprocesos', 'areas', 'macroprocesos', 'versions'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
        $this->resetValidation();
        $this->openShow = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->openShow = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->codigo = null;
        $this->nombre = null;
        $this->version_id = null;
        $this->descripcion = null;
        $this->param_tipo = null;
        $this->responsable_area = null;
        $this->objetivo = null;
        $this->alcance = null;
        $this->finalidad = null;
        $this->orden = null;
        $this->macroproceso_id = null;
        $this->proceso_id = null;
        $this->activo = null;
    }

    public function store()
    {

        $this->validate();
        $proceso = Proceso::find($this->proceso_id);
        $param_tipo = $proceso->param_tipo;

        Subproceso::create([
            'codigo' => $this->codigo,
            'nombre' => $this->nombre,
            'version_id' => $this->version_id,
            'descripcion' => $this->descripcion,
            'param_tipo' => $param_tipo,
            'responsable_area' => $this->responsable_area,
            'objetivo' => $this->objetivo,
            'alcance' => $this->alcance,
            'finalidad' => $this->finalidad,
            'orden' => $this->orden,
            'macroproceso_id' => $this->macroproceso_id,
            'proceso_id' => $this->proceso_id,
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();

        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El subproceso se ha agregado satisfactoriamente.'
        ];
        $this->emit('alertSubproceso', $datos);
    }

    public function edit($id)
    {
        $subproceso = Subproceso::findOrFail($id);

        $this->selected_id = $id;
        $this->codigo = $subproceso->codigo;
        $this->nombre = $subproceso->nombre;
        $this->version_id = $subproceso->version_id;
        $this->descripcion = $subproceso->descripcion;
        $this->param_tipo = $subproceso->param_tipo;
        $this->responsable_area = $subproceso->responsable_area;
        $this->objetivo = $subproceso->objetivo;
        $this->alcance = $subproceso->alcance;
        $this->finalidad = $subproceso->finalidad;
        $this->orden = $subproceso->orden;
        $this->macroproceso_id = $subproceso->macroproceso_id;
        $this->proceso_id = $subproceso->proceso_id;
        $this->activo = $subproceso->activo;

        $this->updateMode = true;
    }


    public function show($id)
    {
        $subproceso = Subproceso::findOrFail($id);

        $this->selected_id = $id;
        $this->codigo = $subproceso->codigo;
        $this->nombre = $subproceso->nombre;
        $this->version_id = $subproceso->version_id;
        $this->descripcion = $subproceso->descripcion;
        $this->param_tipo = $subproceso->param_tipo;
        $this->responsable_area = $subproceso->responsable_area;
        $this->objetivo = $subproceso->objetivo;
        $this->alcance = $subproceso->alcance;
        $this->finalidad = $subproceso->finalidad;
        $this->orden = $subproceso->orden;
        $this->macroproceso_id = $subproceso->macroproceso_id;
        $this->proceso_id = $subproceso->proceso_id;
        $this->activo = $subproceso->activo;

        $this->openShow = true;
    }

    public function update()
    {
        $this->validate([
            'codigo' =>  [
                'required',
                Rule::unique('subprocesos')->ignore($this->selected_id)->where(fn ($query) => $query->where('codigo', $this->codigo)->where('version_id', $this->version_id))
            ],
            'version_id' => 'required'
        ], [
            'codigo.unique' => 'La combinación de codigo y versión ya está en uso.',
        ]);
        $proceso = Proceso::find($this->proceso_id);
        $param_tipo = $proceso->param_tipo;

        if ($this->selected_id) {
            $subproceso = Subproceso::find($this->selected_id);
            $subproceso->update([
                'codigo' => $this->codigo,
                'nombre' => $this->nombre,
                'version_id' => $this->version_id,
                'descripcion' => $this->descripcion,
                'param_tipo' => $param_tipo,
                'responsable_area' => $this->responsable_area,
                'objetivo' => $this->objetivo,
                'alcance' => $this->alcance,
                'finalidad' => $this->finalidad,
                'orden' => $this->orden,
                'proceso_id' => $this->proceso_id,
                'macroproceso_id' => $this->macroproceso_id,
                'updated_user' => Auth::user()->id
            ]);

            $this->resetInput();
            $this->updateMode = false;

            $datos = [
                'tipo' => 'success',
                'mensaje' => 'El subproceso se ha actualizado satisfactoriamente.'
            ];
            $this->emit('alertSubproceso', $datos);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $subproceso = Subproceso::where('id', $id);
            $subproceso->delete();
        }

        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El subproceso se ha eliminado satisfactoriamente.'
        ];
        $this->emit('alertSubproceso', $datos);
    }

    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }
    public function obtenerProceso()
    {
        $this->proceso_id = null;
        $this->procesos = Proceso::whereActivo(1)->whereMacroproceso_id($this->macroproceso_id)->get();
    }
}
