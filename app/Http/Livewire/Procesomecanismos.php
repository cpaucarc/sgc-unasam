<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Microproceso;
use App\Models\Proceso;
use App\Models\Subproceso;
use App\Models\Macroproceso;
use App\Models\Mecanismo;
use App\Models\Procesomecanismo;
use Illuminate\Support\Facades\Auth;

class Procesomecanismos extends Component
{   
        use WithPagination;

        protected $paginationTheme = 'bootstrap';
        public $open = false;
        public $cantidad = 10;
        public $selected_id, $search,$objetivo, $mecanismo_id, $macroproceso_id, $proceso_id, $subproceso_id, $microproceso_id, $activo;
        public $updateMode = false;
        public $clickUpdate = false;
        
        public $sort = 'id';
        public $direction = 'desc';
         //combos dependienente
        public $procesos = [];
        public $subprocesos = [];
        public $microprocesos = [];
    
        protected $listeners = ['render', 'destroy'];
        protected $rules = [
            //'objetivo'=> 'required',
            'mecanismo_id'=> 'required',
            'macroproceso_id'=> 'required',
            //'proceso_id'=> 'required',
            //'subproceso_id'=> 'required',
            //'microproceso_id'=> 'required',
            ];
        protected $msjError=[
            'mecanismo_id.required' => 'El campo mecanismo es obligatorio.',
            'macroproceso_id.required' => 'El campo macroproceso es obligatorio.',
            ];
    
        public function render()
        {
            $procesomecanismos = Procesomecanismo::where('objetivo', 'LIKE', '%' . $this->search . '%')
                ->orderBy($this->sort, $this->direction)
                ->paginate($this->cantidad);
    
            $macroprocesos= Macroproceso::whereActivo(1)->get();  
            //$procesos= Proceso::whereActivo(1)->get(); 
            //$subprocesos= Subproceso::whereActivo(1)->get();
           // $microprocesos= Microproceso::whereActivo(1)->get();

            $mecanismos= Mecanismo::whereActivo(1)->get();

            $search = '%' . $this->search . '%';
            return view('livewire.procesomecanismos.view', compact('procesomecanismos','macroprocesos','mecanismos'));
            //return view('livewire.procesomecanismos.view', compact('procesomecanismos','microprocesos','subprocesos','procesos','macroprocesos','mecanismos'));
        }
    
        public function cancel()
        {
            $this->resetInput();
            $this->updateMode = false;
            $this->open = false;
        }
    
        private function resetInput()
        {
            $this->open = false;
            $this->objetivo = null;
            $this->mecanismo_id = null;
            $this->macroproceso_id = null;
            $this->proceso_id = null;
            $this->subproceso_id = null;
            $this->microproceso_id = null;     
            $this->activo = null;
        }
    
        public function store()
        {
            $this->validate($this->rules,$this->msjError);
            
            Procesomecanismo::create([ 
                'objetivo'=>$this->objetivo,
                'mecanismo_id'=>$this->mecanismo_id,
                'macroproceso_id'=>$this->macroproceso_id,
                'proceso_id'=>$this->proceso_id,
                'subproceso_id'=>$this->subproceso_id,
                'microproceso_id'=>$this->microproceso_id,  
                'activo'=>'1',
                'created_user' => Auth::user()->id,
            ]);       
            
            $this->resetInput();
            $datos = [
                'tipo' => 'success',
                'mensaje' => 'El Proceso mecanismo ha sido creada satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos); 
            
           
           
        }
    
        public function edit($id)
        {
            $miProcesomecanismo = Procesomecanismo::findOrFail($id);    
            $this->selected_id = $id;
            $this->objetivo=$miProcesomecanismo->objetivo;
            $this->mecanismo_id=$miProcesomecanismo->mecanismo_id;
            $this->macroproceso_id=$miProcesomecanismo->macroproceso_id;
            $this->proceso_id=$miProcesomecanismo->proceso_id;
            $this->subproceso_id=$miProcesomecanismo->subproceso_id;
            $this->microproceso_id=$miProcesomecanismo->microproceso_id;
            $this->activo= $miProcesomecanismo->activo;
            $this->updateMode = true;
            $this->clickUpdate=true;
            $this->setProcesoSelect();
            $this->setSubProcesoSelect();
            $this->setMicroProcesoSelect();
            $this->clickUpdate=false;
        }
    
        public function update()
        {
            $this->validate([        
                'mecanismo_id'=> 'required',
                'macroproceso_id'=> 'required',        
            ],$this->msjError);
            if ($this->selected_id) {
                $miProcesomecanismo = Procesomecanismo::findOrFail($this->selected_id);
                $miProcesomecanismo->update([
                    'objetivo'=>$this->objetivo,
                    'mecanismo_id'=>$this->mecanismo_id,
                    'macroproceso_id'=>$this->macroproceso_id,
                    'proceso_id'=>$this->proceso_id,
                    'subproceso_id'=>$this->subproceso_id,
                    'microproceso_id'=>$this->microproceso_id,  
                    'activo'=>'1',
                    'updated_user' => Auth::user()->id,
                ]);
                $this->resetInput();
                $this->updateMode = false;
                $datos = [
                    'tipo' => 'success',
                    'mensaje' => 'El Proceso mecanismo ha sido actualizado satisfactoriamente.'
                ];
                $this->emit('alertRespuesta', $datos); 
                
            }
            
        }
    
        public function destroy($id)
       {
            if ($id) {
                $record = Procesomecanismo::find($id);
                $record->deleted_user = Auth::user()->id;
                $record->save();
                $record->delete();
            }
            $datos = [
                'tipo' => 'error',
                'mensaje' => 'El Proceso mecanismo ha sido eliminado satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos);
        }
        public function ordenar($sort)
        {
            if ($this->sort == $sort) {
                if ($this->direction == 'desc') {
                    $this->direction = 'asc';
                } else {
                    $this->direction = 'desc';
                }
            } else {
                $this->sort = $sort;
                $this->direction = 'asc';
            }
        }

    public function setProcesoSelect()
    {    
        if(!$this->clickUpdate){
            $this->proceso_id = null;
            $this->procesos=[];
            $this->subproceso_id=null;
            $this->subprocesos=[];
            $this->microproceso_id=null;
            $this->microprocesos=[];
        }   
        if ($this->macroproceso_id != null) {          
           $this->procesos = Proceso::whereActivo(1)->whereMacroproceso_id($this->macroproceso_id)->get();
        }      
       
    }
    public function setSubProcesoSelect()
    {   
        if(!$this->clickUpdate){           
        $this->subproceso_id=null;
        $this->subprocesos=[];
        $this->microproceso_id=null;
        $this->microprocesos=[];
        }
        if ($this->proceso_id != null) {        
           $this->subprocesos = Subproceso::whereActivo(1)->whereProceso_id($this->proceso_id)->get();
        }     
       
    }

     public function setMicroProcesoSelect()
    {   
        if(!$this->clickUpdate){           
        $this->microproceso_id=null;
        $this->microprocesos=[];
        }
        if ($this->proceso_id != null) {        
           $this->microprocesos = Microproceso::whereActivo(1)->whereSubproceso_id($this->subproceso_id)->get();
        }      
       
    }

   

       
    
}
