<?php

namespace App\Http\Livewire;

use App\Models\Actividad;
use App\Models\Detalle;
use App\Models\Proceso;
use Livewire\Component;
use App\Models\Vinculacion;
use App\Models\Macroproceso;
use App\Models\Microproceso;
use App\Models\Subproceso;
use Livewire\WithPagination;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Vinculacions extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $selected_id, $search, $nivel_proceso, $proceso_id = null, $vin_nivel_proceso, $vin_proceso_id = [];
    public $nivelprocesos = [];
    public $vinprocesos = [];
    public $multiselect = [];
    public $procesos;
    public $updateMode = false;
    public $showMode = false;
    public $sort = 'vinculacions.id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy', 'store'];

    protected $rules = [
        'nivel_proceso'       => 'required',
        'proceso_id'   => 'required',
        // 'vin_proceso_id'   => 'required',
    ];

    public function render()
    {
        $niveles_procesos = Detalle::whereParametro_id(15)->whereActivo(1)->get();
        // $macroprocesos = Macroproceso::whereActivo(1)->get();
        $vinculaciones = Vinculacion::join('macroprocesos', 'macroprocesos.id', '=', 'vinculacions.proceso_id')
            ->where('macroprocesos.nombre', 'LIKE', '%' . $this->search . '%')
            ->where('vinculacions.nivel_proceso', 15001)
            ->select('macroprocesos.nombre', 'macroprocesos.codigo', 'vinculacions.id', 'vinculacions.vin_proceso_id', 'vinculacions.activo')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);

        return view('livewire.vinculacions.view', compact('vinculaciones', 'niveles_procesos'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->showMode = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->name = null;
        $this->descripcion = null;
        $this->activo = null;
        $this->nivelprocesos = [];
        $this->vinprocesos = [];
        $this->vin_proceso_id = [];
        $this->multiselect = [];
    }

    public function store()
    {
        $this->validate();
        $vinculaciones = $this->multiselect;
        for ($i = 0; $i < sizeof($vinculaciones); $i++) {
            Vinculacion::create([
                'nivel_proceso' => $this->nivel_proceso,
                'proceso_id' => $this->proceso_id,
                'vin_nivel_proceso' => $this->nivel_proceso,
                'vin_proceso_id' => $vinculaciones[$i]['id'],
                'activo' => 1,
                'created_user' => Auth::user()->id
            ]);
        }

        $this->resetInput();
        $this->emit('alert', 'Vinculación creada satisfactoriamente.');
    }

    public function edit($id)
    {
        $vinculacion = Vinculacion::findOrFail($id);

        $this->nivel_proceso = $vinculacion->nivel_proceso;
        // $this->proceso_id;
        // $this->nivel_proceso;
        // $this->selected_id = $id;
        // $this->name = $vinculacion->name;
        // $this->descripcion = $vinculacion->descripcion;
        // $this->activo = $vinculacion->activo;
        $this->updateMode = true;
    }

    public function show($id)
    {
        $vinculacion = Vinculacion::findOrFail($id);

        $this->nivel_proceso = $vinculacion->nivel_proceso;
        // $this->proceso_id;
        // $this->nivel_proceso;
        // $this->selected_id = $id;
        // $this->name = $vinculacion->name;
        // $this->descripcion = $vinculacion->descripcion;
        // $this->activo = $vinculacion->activo;
        $this->showMode = true;
    }

    public function update()
    {
        $this->validate();

        if ($this->selected_id) {
            $vinculacion = Vinculacion::find($this->selected_id);
            $vinculacion->update([
                'name' => $this->name,
                'guard_name' => $this->descripcion,
                'descripcion' => $this->descripcion,
                'activo' => $this->activo,
                'updated_user' => Auth::user()->id
            ]);

            $this->resetInput();
            $this->updateMode = false;
            $this->emit('alert', 'Vinculación actualizada satisfactoriamente.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $vinculacion = Vinculacion::where('id', $id);
            $vinculacion->delete();
        }
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }
    public function obtenerTipoProcesos()
    {
        switch ($this->nivel_proceso) {
            case '15001':
                $this->nivelprocesos = Macroproceso::whereActivo(1)->select('id', 'codigo', 'nombre')->get();
                break;
            case '15002':
                $this->nivelprocesos = Proceso::whereActivo(1)->select('id', 'codigo', 'nombre')->get();
                break;
            case '15003':
                $this->nivelprocesos = Subproceso::whereActivo(1)->select('id', 'codigo', 'nombre')->get();
                break;
            case '15004':
                $this->nivelprocesos = Microproceso::whereActivo(1)->select('id', 'codigo', 'nombre')->get();
                break;
            case '15005':
                $this->nivelprocesos = Actividad::whereActivo(1)->select('id', 'codigo', 'nombre')->get();
                break;
            default:
                $this->nivelprocesos = [];
                $this->vinprocesos = [];
                $this->vin_proceso_id = [];
                break;
        }
        $this->vinprocesos = [];
        $this->vin_proceso_id = [];
        $this->emit('obtenerTipoProcesos');
    }
    public function obtenerProcesos()
    {
        switch ($this->nivel_proceso) {
            case '15001':
                $this->vinprocesos = Macroproceso::where('id', '!=', $this->proceso_id)->whereActivo(1)->select('id', 'codigo', 'nombre')->get();
                break;
            case '15002':
                $this->vinprocesos = Proceso::where('id', '!=', $this->proceso_id)->whereActivo(1)->select('id', 'codigo', 'nombre')->get();
                break;
            case '15003':
                $this->vinprocesos = Subproceso::where('id', '!=', $this->proceso_id)->whereActivo(1)->select('id', 'codigo', 'nombre')->get();
                break;
            case '15004':
                $this->vinprocesos = Microproceso::where('id', '!=', $this->proceso_id)->whereActivo(1)->select('id', 'codigo', 'nombre')->get();
                break;
            case '15005':
                $this->vinprocesos = Actividad::where('id', '!=', $this->proceso_id)->whereActivo(1)->select('id', 'codigo', 'nombre')->get();
                break;
        }
        $this->emit('obtenerProcesos');
    }
}
