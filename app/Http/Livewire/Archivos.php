<?php

namespace App\Http\Livewire;

use App\Models\Anio;
use App\Models\Archivo;
use App\Models\Detalle;
use App\Models\Version;
use Livewire\Component;
use App\Models\Documento;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use function PHPUnit\Framework\isEmpty;
use function PHPUnit\Framework\isNull;

class Archivos extends Component
{
    use WithFileUploads;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false, $updateMode = false, $showMode = false;
    public $cantidad = 10;
    public $selected_id, $search, $nombre, $descripcion, $anio_id, $version, $fechaIniVigencia, $fechaFinVigencia, $param_periodo, $documento_id, $semana = NULL, $activo;
    public $documento, $nom_archivo, $responsable, $periodo, $anio, $ruta, $placeholder = '';
    public $sort = 'id';
    public $direction = 'desc';
    public $conservacion = '-';
    public $param_conservacion = '-';
    public $periodos = [];
    public $semanas = [], $dias = [];
    public $ocultar1 = '', $ocultar2 = 'd-none', $ocultar3 = 'd-none', $ocultar4 = 'd-none', $ocultar5 = false;
    public $file_upload;
    public $isDisabled = 'disabled';

    protected $listeners = ['openModal', 'store', 'destroy', 'getConservacion'];

    protected $rule1 = [
        'documento_id' => 'required',
        'descripcion' => 'required',
        'anio_id' => 'required',
        'fechaIniVigencia' => 'required',
        'file_upload' => 'required|max:2048'
    ];
    protected $msjError1 = [
        'documento_id.required' => 'El campo documento es obligatorio.',
        'descripcion.required' => 'El campo descripción es obligatorio.',
        'anio_id.required' => 'El campo año es obligatorio.',
        'fechaIniVigencia.required' => 'El campo inicio de vigencia es obligatorio.',
        'file_upload.required' => 'El campo archivo es obligatorio.'
    ];
    protected $rule2 = [
        'documento_id' => 'required',
        'descripcion' => 'required',
        'anio_id' => 'required',
        'param_periodo' => 'required',
        'fechaIniVigencia' => 'required',
        'file_upload' => 'required|max:2048'
    ];
    protected $msjError2 = [
        'documento_id.required' => 'El campo documento es obligatorio.',
        'descripcion.required' => 'El campo descripción es obligatorio.',
        'anio_id.required' => 'El campo año es obligatorio.',
        'param_periodo.required' => 'El campo periodo es obligatorio.',
        'fechaIniVigencia.required' => 'El campo inicio de vigencia es obligatorio.',
        'file_upload.required' => 'El campo archivo es obligatorio.'
    ];
    protected $rule3 = [
        'documento_id' => 'required',
        'descripcion' => 'required',
        'anio_id' => 'required',
        'param_periodo' => 'required',
        'fechaIniVigencia' => 'required',
        'semana' => 'required',
        'file_upload' => 'required|max:2048'
    ];
    protected $msjError3 = [
        'documento_id.required' => 'El campo documento es obligatorio.',
        'descripcion.required' => 'El campo descripción es obligatorio.',
        'anio_id.required' => 'El campo año es obligatorio.',
        'param_periodo.required' => 'El campo periodo es obligatorio.',
        'fechaIniVigencia.required' => 'El campo inicio de vigencia es obligatorio.',
        'semana.required' => 'El campo semana es obligatorio.',
        'file_upload.required' => 'El campo archivo es obligatorio.'
    ];
    protected $ruleUpdate1 = [
        'documento_id' => 'required',
        'descripcion' => 'required',
        'anio_id' => 'required',
        'fechaIniVigencia' => 'required'
    ];
    protected $msjErrorUpdate1 = [
        'documento_id.required' => 'El campo documento es obligatorio.',
        'descripcion.required' => 'El campo descripción es obligatorio.',
        'anio_id.required' => 'El campo año es obligatorio.',
        'fechaIniVigencia.required' => 'El campo inicio de vigencia es obligatorio.'
    ];
    protected $ruleUpdate2 = [
        'documento_id' => 'required',
        'descripcion' => 'required',
        'anio_id' => 'required',
        'param_periodo' => 'required',
        'fechaIniVigencia' => 'required'
    ];
    protected $msjErrorUpdate2 = [
        'documento_id.required' => 'El campo documento es obligatorio.',
        'descripcion.required' => 'El campo descripción es obligatorio.',
        'anio_id.required' => 'El campo año es obligatorio.',
        'param_periodo.required' => 'El campo periodo es obligatorio.',
        'fechaIniVigencia.required' => 'El campo inicio de vigencia es obligatorio.'
    ];
    protected $ruleUpdate3 = [
        'documento_id' => 'required',
        'descripcion' => 'required',
        'anio_id' => 'required',
        'param_periodo' => 'required',
        'fechaIniVigencia' => 'required',
        'semana' => 'required'
    ];
    protected $msjErrorUpdate3 = [
        'documento_id.required' => 'El campo documento es obligatorio.',
        'descripcion.required' => 'El campo descripción es obligatorio.',
        'anio_id.required' => 'El campo año es obligatorio.',
        'param_periodo.required' => 'El campo periodo es obligatorio.',
        'fechaIniVigencia.required' => 'El campo inicio de vigencia es obligatorio.',
        'semana.required' => 'El campo semana es obligatorio.'
    ];

    public function render()
    {
        $archivos = Archivo::where('descripcion', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);

        $anios = Anio::whereActivo(1)->orderBy('anio', 'asc')->take(10)->get();
        $documentos = Documento::whereActivo(1)->get();
        $versiones = Version::whereActivo(1)->get();

        return view('livewire.archivos.view', compact('archivos', 'anios', 'documentos', 'versiones'));
    }
    public function openModal()
    {
        $this->open = true;
        $this->resetInput();
    }
    public function store()
    {
        switch ($this->param_conservacion) {
            case '-':
                $this->validate($this->rule1, $this->msjError1);
                break;
            case '16001':
                $this->validate($this->rule1, $this->msjError1);
                break;
            case '16002':
                $this->validate($this->rule1, $this->msjError1);
                break;
            case '16003':
                $this->validate($this->rule2, $this->msjError2);
                break;
            case '16004':
                $this->validate($this->rule2, $this->msjError2);
                break;
            case '16005':
                $this->validate($this->rule3, $this->msjError3);
                break;
        }

        $file_ruta = $this->file_upload->store('archivos/' . $this->documento_id);
        $nombre_file = $this->file_upload->getClientOriginalName();
        $extension = $this->file_upload->getClientOriginalExtension();
        $created_user = Auth::user()->id;

        $archivo = new Archivo;
        $archivo->nombre = $nombre_file;
        $archivo->descripcion = $this->descripcion;
        $archivo->anio_id = $this->anio_id;
        $archivo->version = $this->version;
        $archivo->ruta = $file_ruta;
        $archivo->fechaIniVigencia = date('Y-m-d', strtotime($this->fechaIniVigencia));
        $archivo->fechaFinVigencia = date('Y-m-d', strtotime($this->fechaFinVigencia));
        $archivo->param_periodo = $this->param_periodo;
        $archivo->semanal_mes = $this->semana;
        $archivo->extension = $extension;
        $archivo->documento_id = $this->documento_id;
        $archivo->created_user = $created_user;
        $archivo->save();

        $this->resetInput();
        $this->open = false;

        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El archivo se ha agregado satisfactoriamente.'
        ];

        $this->emit('alertArchivos', $datos);
    }
    public function edit($id)
    {
        $this->reset('anio_id', 'param_periodo');
        $archivo = Archivo::findOrFail($id);
        $this->selected_id = $id;
        $this->descripcion = $archivo->descripcion;
        $this->anio_id = $archivo->anio_id;
        $this->version = $archivo->version;
        $this->fechaIniVigencia = $archivo->fechaIniVigencia;
        $this->fechaFinVigencia = $archivo->fechaFinVigencia;
        $this->param_periodo = $archivo->param_periodo;
        $this->documento_id = $archivo->documento_id;
        $this->semana = $archivo->semanal_mes;
        $this->file_upload = NULL;
        $this->updateMode = true;
        $this->getConservacion();
        $this->emit('triggerDocumento');
    }
    public function show($id)
    {
        $archivo = Archivo::findOrFail($id);
        $this->selected_id = $id;
        $this->documento =  $archivo->documento->nombreReferencial;
        if ($archivo->documento->area_id) {
            $this->responsable =  $archivo->documento->area->nombre;
        } else {
            $this->responsable =  '-';
        }
        $this->nom_archivo = $archivo->nombre;
        $this->descripcion = $archivo->descripcion;
        $this->anio = $archivo->anio->anio;
        // $this->version = $archivo->ver_version->numero;
        $this->ruta = $archivo->ruta;
        $this->fechaIniVigencia = $archivo->fechaIniVigencia;
        $this->fechaFinVigencia = $archivo->fechaFinVigencia;
        $this->param_periodo = $archivo->periodo->detalle;
        if ($archivo->semanal_mes) {
            $this->semana = $archivo->semana->detalle;
        }
        $this->documento_id = $archivo->documento_id;
        $this->activo = $archivo->activo;
        $this->showMode = true;
    }
    public function update()
    {
        switch ($this->param_conservacion) {
            case '-':
                $this->validate($this->ruleUpdate1, $this->msjErrorUpdate1);
                break;
            case '16001':
                $this->validate($this->ruleUpdate1, $this->msjErrorUpdate1);
                break;
            case '16002':
                $this->validate($this->ruleUpdate1, $this->msjErrorUpdate1);
                break;
            case '16003':
                $this->validate($this->ruleUpdate2, $this->msjErrorUpdate2);
                break;
            case '16004':
                $this->validate($this->ruleUpdate2, $this->msjErrorUpdate2);
                break;
            case '16005':
                $this->validate($this->ruleUpdate3, $this->msjErrorUpdate3);
                break;
        }
        $updated_user = Auth::user()->id;

        if ($this->selected_id) {
            $archivo = Archivo::find($this->selected_id);
            if ($this->file_upload) {
                Storage::delete($archivo->ruta);
                $file_ruta = $this->file_upload->store('archivos/' . $this->documento_id);
                $nombre_file = $this->file_upload->getClientOriginalName();
                $extension = $this->file_upload->getClientOriginalExtension();
                $archivo->nombre = $nombre_file;
                $archivo->ruta = $file_ruta;
                $archivo->extension = $extension;
            }

            $archivo->descripcion = $this->descripcion;
            $archivo->anio_id = $this->anio_id;
            $archivo->version = $this->version;
            $archivo->fechaIniVigencia = date('Y-m-d', strtotime($this->fechaIniVigencia));
            $archivo->fechaFinVigencia = date('Y-m-d', strtotime($this->fechaFinVigencia));
            $archivo->param_periodo = $this->param_periodo;
            $archivo->semanal_mes = $this->semana;
            $archivo->documento_id = $this->documento_id;
            $archivo->updated_user = $updated_user;
            $archivo->save();

            $this->resetInput();
            $this->resetValidation();

            $this->updateMode = false;
            $datos = [
                'tipo' => 'success',
                'mensaje' => 'El archivo se ha actualizado satisfactoriamente.'
            ];
            $this->emit('alertArchivos', $datos);
        }
    }
    public function destroy($id)
    {
        if ($id) {
            $archivo = Archivo::find($id);
            $archivo->deleted_user = Auth::user()->id;
            $archivo->save();
            $archivo->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'El archivo se ha eliminado satisfactoriamente.'
        ];
        $this->emit('alertArchivos', $datos);
    }
    public function descargar($id)
    {
        $archivo = Archivo::find($id);
        $datos = ['tipo' => 'success', 'mensaje' => 'El archivo se ha descargado satisfactoriamente.'];
        $this->emit('alertArchivos', $datos);
        return Storage::response($archivo->ruta, $archivo->nombre);
    }
    public function getConservacion()
    {
        $documento = Documento::find($this->documento_id);

        if ($documento) {
            $this->placeholder = $documento->placeholder;
            $detalle = Detalle::find($documento->param_conservacion);
            $this->param_conservacion = $detalle->id;
            if ($detalle) {
                $this->conservacion = $detalle->detalle;
                switch ($documento->param_conservacion) {
                        //PERMANENTE
                    case 16001:
                        $this->param_periodo = $documento->param_conservacion;
                        $this->ocultar1 = '';
                        $this->ocultar2 = 'd-none';
                        $this->ocultar3 = 'd-none';
                        $this->ocultar4 = 'd-none';
                        $this->isDisabled = 'disabled';
                        $this->ocultar5 = false;
                        break;
                        //ANUAL
                    case 16002:
                        $this->param_periodo = $documento->param_conservacion;
                        $this->ocultar1 = '';
                        $this->ocultar2 = 'd-none';
                        $this->ocultar3 = 'd-none';
                        $this->ocultar4 = 'd-none';
                        $this->isDisabled = 'disabled';
                        $this->ocultar5 = false;
                        break;
                        //SEMESTRAL
                    case 16003:
                        $this->periodos = Detalle::whereParametro_id(19)->get();
                        $this->ocultar1 = '';
                        $this->ocultar2 = 'd-none';
                        $this->ocultar3 = 'd-none';
                        $this->ocultar4 = 'd-none';
                        $this->isDisabled = '';
                        $this->ocultar5 = true;
                        break;
                        // MENSUAL
                    case 16004:
                        $this->periodos = Detalle::whereParametro_id(22)->get();
                        $this->ocultar1 = '';
                        $this->ocultar2 = 'd-none';
                        $this->ocultar3 = 'd-none';
                        $this->ocultar4 = 'd-none';
                        $this->isDisabled = '';
                        $this->ocultar5 = true;
                        break;
                        // SEMANAL
                    case 16005:
                        $this->ocultar1 = 'd-none';
                        $this->ocultar2 = '';
                        $this->ocultar3 = '';
                        $this->ocultar4 = 'd-none';
                        $this->periodos = Detalle::whereParametro_id(22)->get();
                        $this->semanas = Detalle::whereParametro_id(23)->get();
                        $this->isDisabled = '';
                        $this->ocultar5 = true;
                        break;
                        // DIARIO
                        // case 16006:
                        //     $this->ocultar1 = 'd-none';
                        //     $this->ocultar2 = '';
                        //     $this->ocultar3 = '';
                        //     $this->ocultar4 = '';
                        //     $this->periodos = Detalle::whereParametro_id(22)->get();
                        //     $this->semanas = Detalle::whereParametro_id(23)->get();
                        //     $this->dias = Detalle::whereParametro_id(24)->get();
                        //     break;
                    default:
                        $this->periodos = [];
                        break;
                }
            }
        } else {
            $this->conservacion = '-';
            $this->reset('documento_id');
        }
        $this->resetValidation();
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }
    public function cancel()
    {
        $this->updateMode = false;
        $this->open = false;
        $this->showMode = false;
        $this->resetInput();
        $this->resetValidation();
        $this->emit('resetFile');
    }
    private function resetInput()
    {
        $this->descripcion = NULL;
        $this->anio_id = NULL;
        $this->version = NULL;
        $this->fechaIniVigencia = NULL;
        $this->fechaFinVigencia = NULL;
        $this->param_periodo = NULL;
        $this->documento_id = NULL;
        $this->file_upload = NULL;
        $this->semana = NULL;
        $this->ocultar5 = false;
        $this->reset('file_upload');
    }
}
