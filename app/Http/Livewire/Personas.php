<?php

namespace App\Http\Livewire;

use App\Models\Detalle;
use App\Models\Persona;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Personas extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $openShow = false;
    public $updateMode = false;
    public $cantidad = 10;
    public $selected_id, $search, $nombres, $ape_paterno, $ape_materno, $nro_documento, $param_tipodoc, $direccion, $celular, $email, $fecha_nac, $genero, $ubigeo, $param_persona, $activo;
    public $sort = 'id';
    public $direction = 'desc';

    protected $listeners = ['destroy'];

    protected $rules = [
        'nombres'       => 'required',
        'ape_paterno'   => 'required',
        'ape_materno'   => 'required',
        'param_tipodoc' => 'required',
        'nro_documento' => 'required',
        'param_persona' => 'required'
    ];

    public function render()
    {
        $p_tppers = Detalle::whereparametro_id(2)->whereActivo(1)->get();
        $p_tpdocs = Detalle::whereparametro_id(3)->whereActivo(1)->get();

        $personas = Persona::where(DB::raw('CONCAT(personas.ape_paterno," ",personas.ape_materno," ",personas.nombres)'), 'like', '%' . $this->search . '%')
            ->orwhere('celular', 'LIKE', '%' . $this->search . '%')
            ->orwhere('nro_documento', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);

        return view('livewire.personas.view', compact('personas', 'p_tppers', 'p_tpdocs'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
        $this->openShow = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->openShow = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->nombres = null;
        $this->ape_paterno = null;
        $this->ape_materno = null;
        $this->nro_documento = null;
        $this->param_tipodoc = null;
        $this->direccion = null;
        $this->celular = null;
        $this->email = null;
        $this->fecha_nac = null;
        $this->genero = null;
        $this->ubigeo = null;
        $this->param_persona = null;
    }

    public function store()
    {
        $this->validate();

        Persona::create([
            'nombres' => $this->nombres,
            'ape_paterno' => $this->ape_paterno,
            'ape_materno' => $this->ape_materno,
            'nro_documento' => $this->nro_documento,
            'param_tipodoc' => $this->param_tipodoc,
            'direccion' => $this->direccion,
            'celular' => $this->celular,
            'email' => $this->email,
            'fecha_nac' => date('Y-m-d', strtotime($this->fecha_nac)),
            'genero' => $this->genero,
            'ubigeo' => $this->ubigeo,
            'param_persona' => $this->param_persona,
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();
        // $this->emit('alert', 'Persona creada satisfactoriamente.');
        $datos = [
            'tipo' => 'success',
            'mensaje' => 'La persona ha sido creada satisfactoriamente.'
        ];
        $this->emit('alertPersona', $datos);
    }

    public function edit($id)
    {
        $persona = Persona::findOrFail($id);

        $this->selected_id = $id;
        $this->nombres = $persona->nombres;
        $this->ape_paterno = $persona->ape_paterno;
        $this->ape_materno = $persona->ape_materno;
        $this->nro_documento = $persona->nro_documento;
        $this->param_tipodoc = $persona->param_tipodoc;
        $this->direccion = $persona->direccion;
        $this->celular = $persona->celular;
        $this->email = $persona->email;
        $this->fecha_nac = $persona->fecha_nac;
        $this->genero = $persona->genero;
        $this->ubigeo = $persona->ubigeo;
        $this->param_persona = $persona->param_persona;
        // $this->activo = $persona->activo;

        $this->updateMode = true;
    }

    public function show($id)
    {
        $persona = Persona::findOrFail($id);

        $this->selected_id = $id;
        $this->nombres = $persona->nombres;
        $this->ape_paterno = $persona->ape_paterno;
        $this->ape_materno = $persona->ape_materno;
        $this->nro_documento = $persona->nro_documento;
        $this->param_tipodoc = $persona->param_tipodoc;
        $this->direccion = $persona->direccion;
        $this->celular = $persona->celular;
        $this->email = $persona->email;
        $this->fecha_nac = $persona->fecha_nac;
        $this->genero = $persona->genero;
        $this->ubigeo = $persona->ubigeo;
        $this->param_persona = $persona->param_persona;
        // $this->activo = $persona->activo;

        $this->openShow = true;
    }

    public function update()
    {
        $this->validate();

        if ($this->selected_id) {
            $persona = Persona::find($this->selected_id);
            $persona->update([
                'nombres' => $this->nombres,
                'ape_paterno' => $this->ape_paterno,
                'ape_materno' => $this->ape_materno,
                'nro_documento' => $this->nro_documento,
                'param_tipodoc' => $this->param_tipodoc,
                'direccion' => $this->direccion,
                'celular' => $this->celular,
                'email' => $this->email,
                'fecha_nac' => date('Y-m-d', strtotime($this->fecha_nac)),
                'genero' => $this->genero,
                'ubigeo' => $this->ubigeo,
                'param_persona' => $this->param_persona,
                'activo' => $this->activo,
                'updated_user' => Auth::user()->id
            ]);

            $this->resetInput();
            $this->updateMode = false;
            $datos = [
                'tipo' => 'success',
                'mensaje' => 'Persona actualizada satisfactoriamente.'
            ];
            $this->emit('alertPersona', $datos);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $persona = Persona::find($id);
            $persona->deleted_user = Auth::user()->id;
            $persona->save();
            $persona->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'La persona ha sido eliminada satisfactoriamente.'
        ];
        $this->emit('alertPersona', $datos);
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }
    public function updatingSearch()
    {
        $this->resetPage();
    }
}
