<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Microproceso;
use App\Models\Proceso;
use App\Models\Subproceso;
use App\Models\Macroproceso;
use App\Models\Detalle;
use App\Models\Area;
use App\Models\Version;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class Microprocesos extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $openShow = false;
    public $cantidad = 10;
    public $selected_id, $search, $codigo,$nombre,$version_id,$descripcion,$param_tipo,$responsable_area,
        $objetivo,$alcance,$finalidad,$proceso_id,$macroproceso_id,$subproceso_id,$orden,$nivelfinal,$activo;
    public $updateMode = false;
    public $clickUpdate = false;
    public $sort = 'id';
    public $direction = 'desc';
    //combos dependienente
    public $procesos = [];
    public $subprocesos = [];


    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'codigo' => 'required|unique:microprocesos',
        'nombre' => 'required|unique:microprocesos',
        'version_id' => 'required',
        //'descripcion' => 'required',
        'macroproceso_id'=>'required',
        'proceso_id'=>'required',
        'subproceso_id'=>'required',
    ];
    protected $msjError=[
        'codigo.required' => 'El campo código es obligatorio.',
        'codigo.unique' => 'La combinación de codigo y versión ya está en uso.',
        'nombre.required' => 'El campo nombre es obligatorio.',
        'nombre.unique' => 'El valor del campo nombre ya está en uso.',
        'version_id.required' => 'El campo versión es obligatorio.',
        'macroproceso_id.required' => 'El campo macroproceso es obligatorio.',
        'proceso_id.required' => 'El campo proceso es obligatorio.',
        'subproceso_id.required' => 'El campo subproceso es obligatorio.',
    ];


    public function render()
    {
        $microprocesos = Microproceso::where('nombre', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);

        $macroprocesos= Macroproceso::whereActivo(1)->get();
        //$procesos= Proceso::whereActivo(1)->get();
        //$subprocesos= Subproceso::whereActivo(1)->get();
        $tipoMacroprocesos= Detalle::whereparametro_id(5)->whereActivo(1)->get();
        $areas= Area::whereActivo(1)->get();
        $versiones= Version::whereActivo(1)->where('nivel_proceso', 15004)->get();
        $search = '%' . $this->search . '%';
        //return view('livewire.microprocesos.view', compact('microprocesos','subprocesos','procesos','macroprocesos','tipoMacroprocesos','areas','versiones'));
        return view('livewire.microprocesos.view', compact('microprocesos','macroprocesos','tipoMacroprocesos','areas','versiones'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
        $this->openShow = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->openShow = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->codigo=null;
        $this->nombre=null;
        $this->version_id=null;
        $this->descripcion=null;
        $this->param_tipo=null;
        $this->responsable_area=null;
        $this->objetivo=null;
        $this->alcance=null;
        $this->finalidad=null;
        $this->proceso_id=null;
        $this->macroproceso_id=null;
        $this->subproceso_id=null;
        $this->orden=null;
        $this->nivelfinal=null;
        $this->activo = null;
    }

    public function store()
    {
        $this->validate($this->rules,$this->msjError);

        Microproceso::create([
        'codigo'=>$this->codigo,
        'nombre'=>$this->nombre,
        'version_id'=>$this->version_id,
        'descripcion'=>$this->descripcion,
        'param_tipo'=>$this->param_tipo,
        'responsable_area'=>$this->responsable_area,
        'objetivo'=>$this->objetivo,
        'alcance'=>$this->alcance,
        'finalidad'=>$this->finalidad,
        'proceso_id'=>$this->proceso_id,
        'macroproceso_id'=>$this->macroproceso_id,
        'subproceso_id'=>$this->subproceso_id,
        'orden'=>$this->orden,
        'nivelfinal'=>'1',
        'activo' =>'1',
        'created_user' => Auth::user()->id,
        ]);

        $this->resetInput();
        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El Microproceso ha sido creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);



    }

    public function edit($id)
    {
        $miMicroproceso = Microproceso::findOrFail($id);

        $this->selected_id = $id;
        $this->codigo=$miMicroproceso->codigo;
        $this->nombre=$miMicroproceso->nombre;
        $this->version_id=$miMicroproceso->version_id;
        $this->descripcion=$miMicroproceso->descripcion;
        $this->param_tipo=$miMicroproceso->param_tipo;
        $this->responsable_area=$miMicroproceso->responsable_area;
        $this->objetivo=$miMicroproceso->objetivo;
        $this->alcance=$miMicroproceso->alcance;
        $this->finalidad=$miMicroproceso->finalidad;
        $this->proceso_id=$miMicroproceso->proceso_id;
        $this->macroproceso_id=$miMicroproceso->macroproceso_id;
        $this->subproceso_id=$miMicroproceso->subproceso_id;
        $this->orden=$miMicroproceso->orden;
        $this->nivelfinal=$miMicroproceso->nivelfinal;
        $this->activo = $miMicroproceso->activo ;
        $this->updateMode = true;
        $this->clickUpdate=true;
        $this->setProcesoSelect();
        $this->setSubProcesoSelect();
        $this->clickUpdate=false;
    }

    public function show($id)
    {
        $miMicroproceso = Microproceso::findOrFail($id);

        $this->selected_id = $id;
        $this->codigo=$miMicroproceso->codigo;
        $this->nombre=$miMicroproceso->nombre;
        $this->version_id=$miMicroproceso->version_id;
        $this->descripcion=$miMicroproceso->descripcion;
        $this->param_tipo=$miMicroproceso->param_tipo;
        $this->responsable_area=$miMicroproceso->responsable_area;
        $this->objetivo=$miMicroproceso->objetivo;
        $this->alcance=$miMicroproceso->alcance;
        $this->finalidad=$miMicroproceso->finalidad;
        $this->proceso_id=$miMicroproceso->proceso_id;
        $this->macroproceso_id=$miMicroproceso->macroproceso_id;
        $this->subproceso_id=$miMicroproceso->subproceso_id;
        $this->orden=$miMicroproceso->orden;
        $this->nivelfinal=$miMicroproceso->nivelfinal;
        $this->activo = $miMicroproceso->activo ;
        $this->openShow = true;
        //$this->clickUpdate=true;
        $this->setProcesoSelect();
        $this->setSubProcesoSelect();
        //$this->clickUpdate=false;
    }

    public function update()
    {
        $this->validate([
            'codigo' =>  [
                'required',
                Rule::unique('microprocesos')->ignore($this->selected_id)->where(fn ($query) => $query->where('codigo', $this->codigo)->where('version_id', $this->version_id))
            ],
            'nombre' => 'required|unique:microprocesos,nombre,'.$this->selected_id,
            'version_id' => 'required',
            //'descripcion' => 'required',
            'macroproceso_id'=>'required',
            'proceso_id'=>'required',
            'subproceso_id'=>'required',
        ],$this->msjError);
        if ($this->selected_id) {
            $miMicroproceso = Microproceso::findOrFail($this->selected_id);
            $miMicroproceso->update([
                'codigo'=>$this->codigo,
                'nombre'=>$this->nombre,
                'version_id'=>$this->version_id,
                'descripcion'=>$this->descripcion,
                'param_tipo'=>$this->param_tipo,
                'responsable_area'=>$this->responsable_area,
                'objetivo'=>$this->objetivo,
                'alcance'=>$this->alcance,
                'finalidad'=>$this->finalidad,
                'proceso_id'=>$this->proceso_id,
                'macroproceso_id'=>$this->macroproceso_id,
                'subproceso_id'=>$this->subproceso_id,
                'orden'=>$this->orden,
                'nivelfinal'=>'1',
                'activo' =>'1',
                'updated_user' => Auth::user()->id,
            ]);
            $this->resetInput();
            $this->updateMode = false;
            $datos = [
                'tipo' => 'success',
                'mensaje' => 'El Microproceso ha sido actualizado satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos);
        }

    }

    public function destroy($id)
   {
        if ($id) {
            $record = Microproceso::find($id);
            $record->deleted_user = Auth::user()->id;
            $record->save();
            $record->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'El Microproceso ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }

    public function setProcesoSelect()
    {
        if(!$this->clickUpdate){
            $this->param_tipo = null;
            $this->proceso_id = null;
            $this->procesos=[];
            $this->subproceso_id=null;
            $this->subprocesos=[];
        }
        if ($this->macroproceso_id != null) {
            $miMacroproceso= Macroproceso::findOrFail($this->macroproceso_id);
            $this->param_tipo=$miMacroproceso->param_tipo;

            $this->procesos = Proceso::whereActivo(1)->whereMacroproceso_id($this->macroproceso_id)->get();
        }

    }
    public function setSubProcesoSelect()
    {
        if(!$this->clickUpdate){
            $this->subproceso_id=null;
            $this->subprocesos=[];
            }
            if ($this->proceso_id != null) {
               $this->subprocesos = Subproceso::whereActivo(1)->whereProceso_id($this->proceso_id)->get();
            }

    }




}
