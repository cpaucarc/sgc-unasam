<?php

namespace App\Http\Livewire;

use App\Models\Actividad;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;


class ActividadComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $search;
    public $sort = 'id';
    public $direccion = 'desc';
    public $cantidad = 10;
    public $open = false;
    public $openShow = false;
    public $updateMode = false;
    public $selected_id, $nombre, $descripcion;
    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'nombre'       => 'required',
        //'descripcion'   => 'required',
    ];

    public function render()
    {
        $actividades = Actividad::where('nombre', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direccion)
            ->paginate($this->cantidad);
        return view('livewire.actividads.view', compact('actividades'));
    }

    public function index()
    {
        return view('livewire.actividads.index');
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->openShow = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->nombre = null;
        $this->descripcion = null;
    }

    public function store()
    {
        $this->validate();
        Actividad::create([
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();
        //$this->emit('alert', 'Actividad creada satisfactoriamente.');
        $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Actividad creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);
    }

    public function edit($id)
    {
        $persona = Actividad::findOrFail($id);
        $this->selected_id = $id;
        $this->nombre = $persona->nombre;
        $this->descripcion = $persona->descripcion;
        $this->updateMode = true;
    }

    public function show($id)
    {
        $persona = Actividad::findOrFail($id);
        $this->selected_id = $id;
        $this->nombre = $persona->nombre;
        $this->descripcion = $persona->descripcion;
        $this->openShow = true;
    }

    public function update()
    {
        $this->validate([
            'nombre' => 'required',
            //'descripcion' => 'required',
        ]);

        if ($this->selected_id) {
            $record = Actividad::find($this->selected_id);
            $record->update([
                'nombre' => $this->nombre,
                'descripcion' => $this->descripcion,
                'update_user' => Auth::user()->id,
                //'activo' => $this->activo,
            ]);
            $this->resetInput();
            $this->updateMode = false;
            //$this->emit('alert', 'Actvidad actualizada satisfactoriamente.');

            $datos1 = [
                'tipo' => 'success',
                'mensaje' => 'Actvidad actualizada satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Actividad::where('id', $id);
            $record->delete();

            $datos1 = [
                'tipo' => 'error',
                'mensaje' => 'Actvidad eliminado satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }
}
