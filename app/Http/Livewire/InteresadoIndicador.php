<?php

namespace App\Http\Livewire;

use Livewire\Component;

class InteresadoIndicador extends Component
{
    public function render()
    {
        return view('livewire.interesado-indicador');
    }
}
