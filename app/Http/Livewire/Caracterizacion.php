<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Macroproceso;
use App\Models\Microproceso;
use App\Models\Proceso;
use App\Models\Subproceso;
use App\Models\Requisito;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Caracterizacion extends Component
{
    use AuthorizesRequests;

    public $datos;
    public $macroproceso_id = 0, $proceso_id = 0;
    public $macroprocesos = [], $procesos = [], $subprocesos = [];
    public $tipo_proceso = 'MACROPROCESOS';

    protected $listeners = ['verProcesos', 'verSubprocesos'];

    public function iniciar()
    {
        $this->datos =  Macroproceso::whereActivo(1)->get();
        $this->macroprocesos = Macroproceso::whereActivo(1)->get();
    }
    public function mount()
    {
        $this->iniciar();
    }

    public function render()
    {
        return view('livewire.caracterizacion.view');
    }

    public function verMacroprocesos()
    {
        $this->tipo_proceso = 'MACROPROCESOS';
        $this->iniciar();
        $this->macroproceso_id = 0;
        $this->proceso_id = 0;
        $this->emit('mostrarMacroprocesos');
    }
    public function verProcesos()
    {
        $this->tipo_proceso = 'PROCESOS';
        $this->procesos = Proceso::whereActivo(1)->whereMacroproceso_id($this->macroproceso_id)->get();
        $this->datos = Proceso::whereActivo(1)->whereMacroproceso_id($this->macroproceso_id)->get();
        $this->emit('mostrarProcesos');
    }
    public function verSubprocesos()
    {
        $this->tipo_proceso = 'SUBPROCESOS';
        $this->subprocesos = Subproceso::whereActivo(1)->whereProceso_id($this->proceso_id)->get();
        $this->datos = Subproceso::whereActivo(1)->whereProceso_id($this->proceso_id)->get();
        // $this->emit('verProceso', $proceso_id);
    }
    public function verMicroprocesos($subproceso_id)
    {
        $this->tipo_proceso = 'MICROPROCESOS';
        $this->datos = Microproceso::whereActivo(1)->whereSubproceso_id($subproceso_id)->get();
        $this->emit('verProceso', $subproceso_id);
    }
}
