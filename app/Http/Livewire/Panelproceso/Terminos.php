<?php

namespace App\Http\Livewire\Panelproceso;

use Livewire\Component;
use App\Models\Macroproceso;
use App\Models\Termino;
use Livewire\WithPagination;

class Terminos extends Component
{
    public $nivel_proceso, $id_proceso;
    public $cantidad = 10, $search;
    public $terminos_todos;
    public $termino_id = null;
    public $macroproceso;
    public $showTerminos = false;

    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    // protected $listeners = ['obtenerSalidas', 'storeEntrada'];

    public function mount()
    {
        $t = Macroproceso::find($this->id_proceso)->terminos()->pluck('id');
        $this->terminos_todos = Termino::whereNotIn('id', $t->toArray())->get();
        $this->macroproceso = Macroproceso::find($this->id_proceso);
    }

    public function agregarTermino()
    {
        $this->showTerminos = true;
    }

    public function render()
    {
        $terminos = Macroproceso::find($this->id_proceso)->terminos()->where(function ($q) {
            return $q->where('nombre', 'LIKE', '%' . $this->search . '%')->orWhere('definicion', 'LIKE', '%' . $this->search . '%');
        })->paginate($this->cantidad);

        return view('livewire.panelproceso.terminos.view', compact('terminos'));
    }
    public function guardarTermino()
    {
        $this->macroproceso->terminos()->attach($this->termino_id);
        $this->cancel();
    }
    public function eliminarTermino($termino_id)
    {
        $this->macroproceso->terminos()->detach($termino_id);
        $this->cancel();
    }
    public function cancel()
    {
        $this->showTerminos = false;
    }

    public function cerrar()
    {
        $this->cancel();
    }
}
