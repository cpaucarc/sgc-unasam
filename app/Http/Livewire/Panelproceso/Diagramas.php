<?php

namespace App\Http\Livewire\Panelproceso;

use App\Models\Anio;
use App\Models\Archivo;
use App\Models\Documento;
use App\Models\Version;
use Livewire\Component;
use App\Models\Macroproceso;
use App\Models\Microproceso;
use App\Models\Proceso;
use App\Models\Subproceso;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class Diagramas extends Component
{
    use WithFileUploads;

    public $open = false;
    public $updateMode = false;
    public $nivel_proceso;
    public $id_proceso;
    public $selected_id, $nombre, $descripcion, $anio_id, $version, $ruta, $extension, $fechaIniVigencia, $fechaFinVigencia, $param_periodo, $documento_id, $activo;
    public $image;
    public $placeholder = '';

    protected $listeners = ['destroy'];

    protected $rules = [
        'descripcion' => 'required',
        'anio_id' => 'required',
        'fechaIniVigencia' => 'required',
        'image' => 'required|image|max:2048'
    ];
    protected $msjError = [
        'descripcion.required' => 'El campo descripción es obligatorio.',
        'anio_id.required' => 'El campo año es obligatorio.',
        'fechaIniVigencia.required' => 'El campo inicio de vigencia es obligatorio.',
        'image.required' => 'El campo archivo es obligatorio.'
    ];
    protected $rules2 = [
        'descripcion' => 'required',
        'anio_id' => 'required',
        'fechaIniVigencia' => 'required',
    ];
    protected $msjError2 = [
        'descripcion.required' => 'El campo descripción es obligatorio.',
        'anio_id.required' => 'El campo año es obligatorio.',
        'fechaIniVigencia.required' => 'El campo inicio de vigencia es obligatorio.',
    ];

    public function render()
    {
        switch ($this->nivel_proceso) {
            case 15001:
                $diagramas = Macroproceso::find($this->id_proceso)->archivos()->whereDocumento_id(1)->get();
                break;
            case 15002:
                $diagramas = Proceso::find($this->id_proceso)->archivos()->whereDocumento_id(1)->get();
                break;
            case 15003:
                $diagramas = Subproceso::find($this->id_proceso)->archivos()->whereDocumento_id(1)->get();
                break;
            case 15004:
                $diagramas = Microproceso::find($this->id_proceso)->archivos()->whereDocumento_id(1)->get();
                break;
        }
        $anios = Anio::whereActivo(1)->orderBy('anio', 'asc')->take(10)->get();
        $versiones = Version::whereActivo(1)->get();
        return view('livewire.panelproceso.diagramas.view', compact('diagramas', 'anios', 'versiones'));
    }

    public function store()
    {
        $this->validate($this->rules, $this->msjError);

        $documento = Documento::find(1);

        $diagrama = $this->image->store('archivos/1');
        $nombre_file = $this->image->getClientOriginalName();
        $extension = $this->image->getClientOriginalExtension();

        $created_user = Auth::user()->id;

        $archivo = new Archivo;
        $archivo->nombre = $nombre_file;
        // $archivo->version = $this->version;
        $archivo->descripcion = $this->descripcion;
        $archivo->anio_id = $this->anio_id;
        $archivo->ruta = $diagrama;
        $archivo->extension = $extension;
        $archivo->fechaIniVigencia = date('Y-m-d', strtotime($this->fechaIniVigencia));
        $archivo->fechaFinVigencia = date('Y-m-d', strtotime($this->fechaFinVigencia));
        $archivo->param_periodo = $documento->param_conservacion;
        $archivo->documento_id = 1;
        $archivo->created_user = $created_user;
        $archivo->save();

        switch ($this->nivel_proceso) {
            case 15001:
                $macroproceso = Macroproceso::find($this->id_proceso);
                $macroproceso->archivos()->attach($archivo->id, ['nivel_proceso' => $this->nivel_proceso, 'created_user' => $created_user]);
                break;
            case 15002:
                $proceso = Proceso::find($this->id_proceso);
                $proceso->archivos()->attach($archivo->id, ['nivel_proceso' => $this->nivel_proceso, 'created_user' => $created_user]);
                break;
            case 15003:
                $subproceso = Subproceso::find($this->id_proceso);
                $subproceso->archivos()->attach($archivo->id, ['nivel_proceso' => $this->nivel_proceso, 'created_user' => $created_user]);
                break;
            case 15004:
                $microproceso = Microproceso::find($this->id_proceso);
                $microproceso->archivos()->attach($archivo->id, ['nivel_proceso' => $this->nivel_proceso, 'created_user' => $created_user]);
                break;
        }

        $this->resetInput();

        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El diagrama de flujo ha sido creado satisfactoriamente.'
        ];

        $this->emit('alertDiagrama', $datos);
    }

    public function edit($id)
    {
        $archivo = Archivo::findOrFail($id);
        $this->selected_id = $archivo->id;
        $this->descripcion = $archivo->descripcion;
        $this->anio_id = $archivo->anio_id;
        $this->fechaIniVigencia = $archivo->fechaIniVigencia;
        $this->fechaFinVigencia = $archivo->fechaFinVigencia;
        $this->ruta = $archivo->ruta;
        $this->extension = $archivo->extension;
        $this->activo = $archivo->activo;
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate($this->rules2, $this->msjError2);

        $updated_user = Auth::user()->id;

        if ($this->selected_id) {
            $archivo = Archivo::find($this->selected_id);
            if ($this->image) {
                Storage::delete($archivo->ruta);
                $file_ruta = $this->image->store('archivos/1');
                $nombre_file = $this->image->getClientOriginalName();
                $extension = $this->image->getClientOriginalExtension();
                $archivo->nombre = $nombre_file;
                $archivo->ruta = $file_ruta;
                $archivo->extension = $extension;
            }
            $archivo->descripcion = $this->descripcion;
            $archivo->anio_id = $this->anio_id;
            $archivo->fechaIniVigencia = date('Y-m-d', strtotime($this->fechaIniVigencia));
            $archivo->fechaFinVigencia = date('Y-m-d', strtotime($this->fechaFinVigencia));
            $archivo->updated_user = $updated_user;
            $archivo->save();

            switch ($this->nivel_proceso) {
                case 15001:
                    $macroproceso = Macroproceso::find($this->id_proceso);
                    $macroproceso->archivos()->updateExistingPivot($this->selected_id, ['updated_user' => $updated_user, 'updated_at' => date('Y-m-d H:i:s')]);
                    break;
                case 15002:
                    $proceso = Proceso::find($this->id_proceso);
                    $proceso->archivos()->updateExistingPivot($this->selected_id, ['updated_user' => $updated_user, 'updated_at' => date('Y-m-d H:i:s')]);
                    break;
                case 15003:
                    $subproceso = Subproceso::find($this->id_proceso);
                    $subproceso->archivos()->updateExistingPivot($this->selected_id, ['updated_user' => $updated_user, 'updated_at' => date('Y-m-d H:i:s')]);
                    break;
                case 15004:
                    $microproceso = Microproceso::find($this->id_proceso);
                    $microproceso->archivos()->updateExistingPivot($this->selected_id, ['updated_user' => $updated_user, 'updated_at' => date('Y-m-d H:i:s')]);
                    break;
            }

            $this->resetInput();

            $datos = [
                'tipo' => 'success',
                'mensaje' => 'El diagrama de flujo ha sido actualizado satisfactoriamente.'
            ];
            $this->emit('alertDiagrama', $datos);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $deleted_user = Auth::user()->id;
            // $archivo = Archivo::find($id);
            //si se desea eliminar el archivo del servidor descomentar
            // Storage::delete($archivo->ruta);

            switch ($this->nivel_proceso) {
                case 15001:
                    $macroproceso = Macroproceso::find($this->id_proceso);
                    $macroproceso->archivos()->updateExistingPivot($id, ['deleted_user' => $deleted_user, 'deleted_at' => date('Y-m-d H:i:s')]);
                    break;
                case 15002:
                    $proceso = Proceso::find($this->id_proceso);
                    $proceso->archivos()->updateExistingPivot($id, ['deleted_user' => $deleted_user, 'deleted_at' => date('Y-m-d H:i:s')]);
                    break;
                case 15003:
                    $subproceso = Subproceso::find($this->id_proceso);
                    $subproceso->archivos()->updateExistingPivot($id, ['deleted_user' => $deleted_user, 'deleted_at' => date('Y-m-d H:i:s')]);
                    break;
                case 15004:
                    $microproceso = Microproceso::find($this->id_proceso);
                    $microproceso->archivos()->updateExistingPivot($id, ['deleted_user' => $deleted_user, 'deleted_at' => date('Y-m-d H:i:s')]);
                    break;
            }

            $datos = ['tipo' => 'error', 'mensaje' => 'El diagrama de flujo ha sido eliminado satisfactoriamente.'];
            $this->emit('alertDiagrama', $datos);
        }
    }

    public function descargar($id)
    {
        $archivo = Archivo::find($id);
        $datos = ['tipo' => 'success', 'mensaje' => 'El diagrama se ha descargado satisfactoriamente.'];
        $this->emit('alertDiagrama', $datos);
        return Storage::response($archivo->ruta, $archivo->nombre);
    }

    private function resetInput()
    {
        $this->nombre = null;
        $this->descripcion = null;
        $this->anio_id = null;
        $this->fechaIniVigencia = null;
        $this->fechaFinVigencia = null;
        $this->image = null;
        $this->open = false;
        $this->updateMode = false;
        $this->emit('resetFile');
    }

    public function cancel()
    {
        $this->resetValidation();
        $this->resetInput();
    }
}
