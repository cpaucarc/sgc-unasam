<?php

namespace App\Http\Livewire\Panelproceso;

use App\Models\Documento;
use App\Models\Entrada;
use App\Models\Interesado;
use Livewire\Component;
use App\Models\Macroproceso;
use App\Models\Proceso;
use Illuminate\Support\Facades\Auth;

class ControlDocumental extends Component
{
    public $nivel_proceso, $id_proceso;

    // protected $listeners = ['obtenerSalidas', 'storeEntrada'];

    public function render()
    {
        switch ($this->nivel_proceso) {
            case 15001:
                $entradas = Macroproceso::find($this->id_proceso)->entradas;
                break;

            default:
                # code...
                break;
        }

        return view('livewire.panelproceso.controldocumental.view', compact('entradas'));
    }
}
