<?php

namespace App\Http\Livewire\Documentodetalles;

use App\Models\Documentodetalle;
use Livewire\Component;
use Livewire\WithPagination;

class ShowDocumentodetalles extends Component
{
    use WithPagination;

    public $search;
    public $sort = 'id';
    public $detalle = 'desc';
    public $cantidad = 10;

    protected $listiners = ['render' => 'render'];
    public function render()
    {
        $documentoDetalles = Documentodetalle::where('nombre', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->detalle)
            ->paginate($this->cantidad);
        return view('livewire.documentodetalles.show-documentodetalles', compact('documentoDetalles'));
    }

    public function index()
    {
        return view('sgc.documentoDetalles.index');
    }
}
