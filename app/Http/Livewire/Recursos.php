<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Recurso;
use App\Models\Detalle;
use Illuminate\Support\Facades\Auth;

class Recursos extends Component
{
    
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $selected_id, $search, $nombre, $param_tiporecurso, $activo;
    public $updateMode = false;
    public $showMode = false;
    public $sort = 'id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'nombre' => 'required|unique:recursos',
        'param_tiporecurso' => 'required',
    ];
    protected $msjError=[
        'nombre.required' => 'El campo nombre es obligatorio.',
        'nombre.unique' => 'El valor del campo nombre ya está en uso.',
        'param_tiporecurso.required' => 'El campo tipo recurso es obligatorio.'
    ];

    public function render()
    {
        $recursos = Recurso::where('nombre', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);
        $tipoRecursos = Detalle::whereparametro_id(9)->whereActivo(1)->get();

        $search = '%' . $this->search . '%';
        return view('livewire.recursos.view', compact('recursos','tipoRecursos'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
        $this->showMode = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->nombre = null;
        $this->param_tiporecurso = null;       
        $this->activo = null;
    }

    public function store()
    {
        $this->validate($this->rules,$this->msjError);

        Recurso::create([
            'nombre' => $this->nombre,
            'param_tiporecurso' => $this->param_tiporecurso,
            'activo' =>'1',
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();
        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El Recurso ha sido creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos); 
    }

    public function edit($id)
    {
        $miRecurso = Recurso::findOrFail($id);

        $this->selected_id = $id;
        $this->nombre = $miRecurso->nombre;
        $this->param_tiporecurso = $miRecurso->param_tiporecurso;      
        $this->activo = $miRecurso->activo;
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
            'nombre' => 'required|unique:recursos,nombre,'.$this->selected_id,
            'param_tiporecurso' => 'required',
        ],$this->msjError);
        if ($this->selected_id) {
            $miRecurso = Recurso::findOrFail($this->selected_id);
            $miRecurso->update([
                'nombre' => $this->nombre,
                'param_tiporecurso' => $this->param_tiporecurso,
                'activo' =>'1',
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;
            $datos = [
                'tipo' => 'success',
                'mensaje' => 'El Recurso ha sido actualizado satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos);
        }     
    }

    public function destroy($id)
   {
        if ($id) {
            $record = Recurso::find($id);
            $record->deleted_user = Auth::user()->id;
            $record->save();
            $record->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'El Recurso ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }  
    
    public function show($id)
    {
        $miRecurso = Recurso::findOrFail($id);

        $this->selected_id = $id;
        $this->nombre = $miRecurso->nombre;
        $this->param_tiporecurso = $miRecurso->param_tiporecurso;      
        $this->activo = $miRecurso->activo;
        $this->showMode = true;
    }
    
    public function cerrar()
    {
        $this->resetInput();
        $this->showMode = false;
    }
}
