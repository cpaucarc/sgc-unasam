<?php

namespace App\Http\Livewire\Reportes;

use Livewire\Component;
use DB;

class PorTipo extends Component
{

    public $tipos;

    public function mount(){
        $this->iniciaIndicadores();
    }

    public function iniciaIndicadores(){

        $indicadores = collect(DB::select("
            SELECT
            d.detalle AS 'tipo', i.codigo, i.nombre, AVG(r.valor_indicador) AS 'promedio', COUNT(r.valor_indicador) AS 'valores', COUNT(DISTINCT i.nombre) AS 'conteo'
            FROM indicadors i
            INNER JOIN detalles d ON d.id = i.param_tipoindicador
            LEFT JOIN resultados r ON r.indicador_id = i.id
            GROUP BY d.detalle, i.codigo, i.nombre
            ORDER BY d.detalle;
        "));

        $this->tipos = $indicadores->groupBy('tipo');
    }

    public function render()
    {
        return view('livewire.reportes.por-tipo');
    }
}
