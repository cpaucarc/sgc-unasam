<?php

namespace App\Http\Livewire\Reportes;

use Livewire\Component;
use DB;

class PorOrganicas extends Component
{

    public $organicas;

    public function mount(){
        $this->iniciaOrganicas();
    }

    public function iniciaOrganicas(){

        $indicadores = collect(DB::select("
            SELECT
            a.nombre AS 'unidad', i.nombre, d.abreviatura AS 'tipo', AVG(r.valor_indicador) AS 'promedio', COUNT(r.valor_indicador) AS 'valores', COUNT(DISTINCT i.nombre) AS 'conteo'
            FROM indicadors i
            INNER JOIN macroprocesos m ON m.id = i.macroproceso_id
            INNER JOIN areas a ON a.id = m.responsable_area
            INNER JOIN detalles d ON d.id = i.param_tipoindicador
            LEFT JOIN resultados r ON r.indicador_id = i.id
            GROUP BY a.nombre, i.nombre, d.abreviatura;
        "));

        $this->organicas = $indicadores->groupBy('unidad');
    }

    public function render()
    {
        return view('livewire.reportes.por-organicas');
    }
}
