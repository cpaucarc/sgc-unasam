<?php

namespace App\Http\Livewire\Reportes;

use Livewire\Component;
use DB;

class PorProcesos extends Component
{

    public $procesos;

    public function mount(){
        $this->iniciaProcesos();
    }

    public function iniciaProcesos(){
        $procesos = collect(DB::select("
            SELECT
            CONCAT( m.codigo, ': ', m.nombre) AS 'proceso', i.codigo, i.nombre, AVG(r.valor_indicador) AS 'promedio', COUNT(r.valor_indicador) AS 'valores', COUNT(DISTINCT i.nombre) AS 'conteo'
            FROM indicadors i
            INNER JOIN macroprocesos m ON m.id = i.macroproceso_id
            INNER JOIN detalles d ON d.id = i.param_tipoindicador
            LEFT JOIN resultados r ON r.indicador_id = i.id
            WHERE i.proceso_id IS NULL
            GROUP BY	m.codigo, m.nombre, i.codigo, i.nombre
            UNION
            SELECT
            CONCAT( m.codigo, ': ', m.nombre) AS 'proceso', i.codigo, i.nombre, AVG(r.valor_indicador) AS 'promedio', COUNT(r.valor_indicador) AS 'valores', COUNT(DISTINCT i.nombre) AS 'conteo'
            FROM indicadors i
            INNER JOIN procesos m ON m.id = i.proceso_id
            INNER JOIN detalles d ON d.id = i.param_tipoindicador
            LEFT JOIN resultados r ON r.indicador_id = i.id
            GROUP BY	m.codigo, m.nombre, i.codigo, i.nombre
            ORDER BY proceso;
        "));
        $this->procesos = $procesos->groupBy('proceso');
    }

    public function render()
    {
        return view('livewire.reportes.por-procesos');
    }
}
