<?php

namespace App\Http\Livewire\Reportes;

use App\Models\Indicador;
use Livewire\Component;

class Indicadores extends Component
{
    public $indicadores;

    public function mount()
    {
        $this->indicadores = Indicador::with('macroproceso', 'proceso', 'metas', 'meta', 'responsables', 'resultados', 'formula')->get();
    }

    public function render()
    {
        return view('livewire.reportes.indicadores');
    }
}
