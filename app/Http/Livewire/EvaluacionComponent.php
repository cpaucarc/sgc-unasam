<?php

namespace App\Http\Livewire;

use App\Models\Detalle;
use App\Models\Escala;
use App\Models\Evaluacion;
use App\Models\Medicion;
use App\Models\Resultado;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class EvaluacionComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $orden = 'asc';
    public $selected_id, $search, $param_likert, $propuesta_mejora, $fecha_propuesta, $fecha_cumplimiento, $resultado_id;
    public $updateMode = false;
    public $sort = 'evaluacions.id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'param_likert'       => 'required',
        'propuesta_mejora'   => 'required',
        'fecha_propuesta'   => 'required',
        'fecha_cumplimiento' => 'required',
        'resultado_id' => 'required'
    ];
    protected $msjError=[
        'param_likert.required' => 'El campo parámetro es obligatorio',
        'propuesta_mejora.required' => 'El campo propuesta de mejora es obligatorio.',
        'fecha_propuesta.required' => 'El campo fecha de propuesta es obligatorio',
        'fecha_cumplimiento.required' => 'El campo fecha de cumplimiento es obligatorio'
    ];

    public function render()
    {
        $parametro_likerts = Detalle::whereparametro_id(14)->whereActivo(1)->get();
        $parametro_resultados = Resultado::whereActivo(1)->get();

        $evaluaciones = Evaluacion::select(DB::raw('evaluacions.id as id, evaluacions.param_likert param_likert, evaluacions.propuesta_mejora as propuesta_mejora,
        evaluacions.fecha_propuesta as fecha_propuesta, evaluacions.fecha_cumplimiento as fecha_cumplimiento,
        evaluacions.resultado_id resultado_id, evaluacions.activo,
        evaluacions.created_user, evaluacions.updated_at, evaluacions.created_at,(SELECT detalle FROM detalles t1 WHERE t1.id = evaluacions.param_likert) as dParamLikert, resultados.param_periodo '))
            ->join('resultados', 'evaluacions.resultado_id', '=', 'resultados.id')
            ->where('evaluacions.propuesta_mejora', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->orden)
            ->paginate($this->cantidad);

        return view('livewire.evaluacions.view', compact('evaluaciones', 'parametro_likerts', 'parametro_resultados'));
    }

    public function index()
    {
        return view('livewire.evaluacions.index');
    }


    private function resetInput()
    {
        $this->open = false;
        $this->param_likert = null;
        $this->propuesta_mejora = null;
        $this->fecha_cumplimiento = null;
        $this->fecha_propuesta = null;
        $this->resultado_id = null;
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
    }

    public function store()
    {
        $this->validate($this->rules,$this->msjError);
        Evaluacion::create([
            'param_likert' => $this->param_likert,
            'propuesta_mejora' => $this->propuesta_mejora,
            'fecha_cumplimiento' => date('Y-m-d', strtotime($this->fecha_cumplimiento)),
            'fecha_propuesta' => date('Y-m-d', strtotime($this->fecha_propuesta)),
            'resultado_id' => $this->resultado_id,
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();

        $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Evaluación creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);
    }

    public function edit($id)
    {
        $area = Evaluacion::findOrFail($id);
        $this->selected_id = $id;
        $this->resultado_id = $area->resultado_id;
        $this->param_likert = $area->param_likert;
        $this->propuesta_mejora = $area->propuesta_mejora;
        $this->fecha_propuesta = $area->fecha_propuesta;
        $this->fecha_cumplimiento = $area->fecha_cumplimiento;
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
            'param_likert'       => 'required',
            'propuesta_mejora'   => 'required',
            'fecha_propuesta'   => 'required',
            'fecha_cumplimiento' => 'required',
            'resultado_id' => 'required'
        ]);

        if ($this->selected_id) {
            $record = Evaluacion::find($this->selected_id);
            $record->update([
                'param_likert' => $this->param_likert,
                'propuesta_mejora' => $this->propuesta_mejora,
                'fecha_cumplimiento' => date('Y-m-d', strtotime($this->fecha_cumplimiento)),
                'fecha_propuesta' => date('Y-m-d', strtotime($this->fecha_propuesta)),
                'resultado_id' => $this->resultado_id,
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;

            $datos1 = [
                'tipo' => 'success',
                'mensaje' => 'Evaluación actualizada satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Evaluacion::where('id', $id);
            $record->delete();
            $datos1 = [
                'tipo' => 'error',
                'mensaje' => 'La Evaluación ha sido eliminado.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->orden == 'desc') {
                $this->orden = 'asc';
            } else {
                $this->orden = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->orden = 'asc';
        }
    }
}
