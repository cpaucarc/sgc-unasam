<?php

namespace App\Http\Livewire;

use App\Models\Detalle;
use App\Models\Parametro;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class DetalleComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $orden = 'asc';
    public $selected_id, $search, $abreviatura, $detalle, $descripcion, $parametro_id;
    public $updateMode = false;
    public $sort = 'id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'abreviatura'   => 'required|unique:detalles',
        'detalle'   => 'required|unique:detalles',
        'parametro_id' => 'required'
    ];
    protected $msjError=[
        'abreviatura.required' => 'El campo abreviatura es obligatorio.',
        'parametro_id.required' => 'El campo parámetro es obligatorio.',
        'detalle.required' => 'El campo detalle es obligatorio.'
    ];

    public function render()
    {
        $parametros = Parametro::all();
        $detalles = Detalle::where('detalle', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->orden)
            ->paginate($this->cantidad);
        return view('livewire.detalles.view', compact('detalles','parametros'));
    }

    public function index()
    {
        return view('livewire.detalles.index');
    }

    private function resetInput()
    {
        $this->open = false;
        $this->detalle = null;
        $this->parametro_id = null;
        $this->abreviatura = null;
        $this->descripcion = null;
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
    }

    public function store()
    {
        $this->validate($this->rules,$this->msjError);
        Detalle::create([
            'abreviatura' => $this->abreviatura,
            'detalle' => $this->detalle,
            'descripcion' => $this->descripcion,
            'parametro_id' => $this->parametro_id,
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();

        $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Detalle creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);
    }

    public function edit($id)
    {
        $persona = Detalle::findOrFail($id);
        $this->selected_id = $id;
        $this->detalle = $persona->detalle;
        $this->abreviatura = $persona->abreviatura;
        $this->descripcion = $persona->descripcion;
        $this->parametro_id = $persona->parametro_id;
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
            'abreviatura' => 'required|unique:detalles,detalle,'.$this->selected_id,
            'detalle' => 'required|unique:detalles,detalle,'.$this->selected_id,
            'parametro_id' => 'required'
        ]);

        if ($this->selected_id) {
            $record = Detalle::find($this->selected_id);
            $record->update([
                'detalle' => $this->detalle,
                'abreviatura' => $this->abreviatura,
                'parametro_id' => $this->parametro_id,
                'descripcion' => $this->descripcion,
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;

            $datos1 = [
                'tipo' => 'success',
                'mensaje' => 'Detalle actualizada satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Detalle::where('id', $id);
            $record->delete();
            $datos1 = [
                'tipo' => 'error',
                'mensaje' => 'El detalle ha sido eliminado.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->orden == 'desc') {
                $this->orden = 'asc';
            } else {
                $this->orden = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->orden = 'asc';
        }
    }
}
