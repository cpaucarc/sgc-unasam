<?php

namespace App\Http\Livewire\Mecanismo;

use App\Models\Documento;
use App\Models\Evidencia;
use App\Models\Mecanismo;
use App\Models\Objetivo;
use Livewire\Component;

class Edit extends Component
{
    public $type;
    public $relacionado_type;
    public $relacionado_id;
    public $macroproceso;
    public $mecanismos;
    public $mecanismos_todos;
    public $mecanismos_visibles;
    public $documentos;
    public $showMecanismos = false;
    public $showDocumento = false;
    public $show = false;
    public $objetivo;
    public $documento_id;
    public $mecanismo_id;
    public $nombre_evidencia;
    public $evidencia;

    public function mount()
    {
        $this->type = 'App\\Models\\' . $this->relacionado_type;
        $this->relacionado = $this->type::findOrFail($this->relacionado_id);
        $this->refreshData();
        $this->refreshMecanismos();
        $this->obtenerDocumentos();
        $this->documento_id = 0;
        $this->mecanismos_visibles = collect();
    }

    public function render()
    {
        return view('livewire.mecanismo.edit');
    }

    public function agregarMecanismo()
    {
        $this->showMecanismos = true;
        $this->refreshData();
    }
    public function guardarMecanismo()
    {
        $this->mecanismos_visibles->push($this->mecanismo_id);
        $this->cancel();

    }
    public function agregarObjetivo(Mecanismo $mecanismo)
    {
        $this->relacionado->objetivos()->create([
            'nombre' => '-',
            'mecanismo_id' => $mecanismo->id,
            'relacion_id' => $this->relacionado->id,
            'relacion_type' => get_class($this->relacionado),
            'created_user' => request()->user()->id
        ]);
        $this->cancel();
    }
    public function eliminarObjetivo(Objetivo $objetivo)
    {
        $objetivo->evidencias()->delete();
        $objetivo->delete();
        $this->refreshData();
    }
    public function agregarEvidencia(Objetivo $objetivo)
    {
        $this->show = true;
        $this->objetivo = $objetivo;
        $this->refreshData();
    }
    public function eliminarEvidencia(Evidencia $evidencia)
    {
        $evidencia->delete();
        $this->refreshData();
    }

    public function guardarEvidencia()
    {
        $this->objetivo->evidencias()->create([
            'documento_id' => $this->documento_id == 0 ? null : $this->documento_id,
            'nombre' => $this->nombre_evidencia,
            'created_user' => request()->user()->id
        ]);
        $this->show = false;
        $this->refreshData();
    }

    public function verEvidencia(Evidencia $evidencia)
    {
        $this->evidencia = $evidencia;
        $this->showDocumento = true;
    }

    public function refreshData()
    {

        $this->mecanismos = Mecanismo::with(['detalle', 'objetivos' => function ($q) {
            return $q->where('relacion_type', $this->type)->where('relacion_id', $this->relacionado_id);
        }, 'objetivos.evidencias'])->get();

        $mecanismos_visibles = $this->mecanismos->pluck('objetivos')->flatten()->pluck('mecanismo_id')->merge($this->mecanismos_visibles);

        $this->mecanismos = $this->mecanismos->whereIn('id', $mecanismos_visibles)->groupBy('detalle.detalle')->toBase();
    }
    public function refreshMecanismos()
    {
        $this->mecanismos_todos = Mecanismo::whereNotIn('id', $this->mecanismos->flatten()->pluck('id'))->get();
        $this->mecanismo_id = $this->mecanismos_todos->count() > 0 ? $this->mecanismos_todos->first()->id : 0;
    }

    public function obtenerDocumentos()
    {
        $this->documentos = Documento::with('archivos')->orderByDesc('id')->get();
    }
    public function cancel()
    {
        $this->show = false;
        $this->showDocumento = false;
        $this->showMecanismos = false;
        $this->refreshData();
    }
    public function cerrar()
    {
        $this->cancel();
    }
}
