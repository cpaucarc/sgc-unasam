<?php

namespace App\Http\Livewire\Mecanismo;

use App\Models\Objetivo as ModelsObjetivo;
use Livewire\Component;

class Objetivo extends Component
{
    public $nombre;
    public $objetivo_id;

    protected $rules = [
        'objetivo.nombre' => 'required'
    ];

    public function mount(){
        $this->objetivo = ModelsObjetivo::findOrFail($this->objetivo_id);
    }

    public function render()
    {
        return view('livewire.mecanismo.objetivo');
    }

    public function actualizar (){
        $this->objetivo->save();
    }
}
