<?php

namespace App\Http\Livewire;

use App\Models\Termino;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;

class Terminos extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $selected_id, $search, $nombre, $definicion, $activo;
    public $updateMode = false;
    public $showMode = false;
    public $sort = 'nombre';
    public $direction = 'asc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'nombre'       => 'required',
        'definicion'   => 'required',
        'activo'   => 'required',
    ];

    public function render()
    {
        $terminos = Termino::where('nombre', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);

        return view('livewire.terminos.view', compact('terminos'));

        // $this->authorize('viewAny', \App\Models\Anio::class);

        // $anios = Anio::where('anio', 'like', '%' . $this->search . '%')
        //     ->orderBy($this->sort, $this->orden)
        //     ->paginate($this->cantidad);
        // return view('livewire.anios.view', compact('anios'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
        $this->showMode = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->nombre = null;
        $this->definicion = null;
        $this->activo = null;
    }

    public function store()
    {
        $this->validate();

        Termino::create([
            'nombre' => $this->nombre,
            'definicion' => $this->definicion,
            'activo' => $this->activo,
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();

        $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Termino creado satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);
    }
    
    public function edit($id)
    {
        $rol = Termino::findOrFail($id);

        $this->selected_id = $id;
        $this->nombre = $rol->nombre;
        $this->definicion = $rol->definicion;
        $this->activo = $rol->activo;
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate();

        if ($this->selected_id) {
            $termino = Termino::find($this->selected_id);
            $termino->update([
                'nombre' => $this->nombre,
                'definicion' => $this->definicion,
                'activo' => $this->activo,
                'updated_user' => Auth::user()->id
            ]);

            $this->resetInput();
            $this->updateMode = false;

            $datos1 = [
                'tipo' => 'success',
                'mensaje' => 'Termino actualizado satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $termino = Termino::where('id', $id);
            $termino->delete();

            $datos1 = [
                'tipo' => 'error',
                'mensaje' => 'El término se ha sido eliminado.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }

    public function show($id)
    {
        $rol = Termino::findOrFail($id);

        $this->selected_id = $id;
        $this->nombre = $rol->nombre;
        $this->definicion = $rol->definicion;
        $this->activo = $rol->activo;
        $this->showMode = true;
    }
    
    public function cerrar()
    {
        $this->resetInput();
        $this->showMode = false;
    }

}
