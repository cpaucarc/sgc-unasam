<?php

namespace App\Http\Livewire\Caracterizacion;

use App\Models\Proceso;
use Livewire\Component;
use App\Models\Requisito;
use App\Models\Macroproceso;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Requisitos extends Component
{
    use WithPagination;
    use AuthorizesRequests;

    protected $paginationTheme = 'bootstrap';

    public $nivel_proceso, $id_proceso;
    public $tab = 'cliente';
    public $cant_clientes = 10, $cant_isos = 10, $cant_legals = 10;
    public $tprequisito, $selected_id, $descripcion;
    public $openRequisito = false, $updateRequisito = false;
    public $area;

    protected $listeners = ['destroy'];

    protected $rules = [
        'descripcion'       => 'required'
    ];

    public function render()
    {
        switch ($this->nivel_proceso) {
            case 15001:
                $clientes = Requisito::whereMacroproceso_id($this->id_proceso)
                    ->whereParam_tiporequisito(7001)
                    ->where('proceso_id', NULL)
                    ->select('id', 'descripcion', 'param_tiporequisito')
                    ->paginate($this->cant_clientes);
                $isos = Requisito::whereMacroproceso_id($this->id_proceso)
                    ->whereParam_tiporequisito(7002)
                    ->where('proceso_id', NULL)
                    ->select('id', 'descripcion', 'param_tiporequisito')
                    ->paginate($this->cant_isos);
                $legals = Requisito::whereMacroproceso_id($this->id_proceso)
                    ->whereParam_tiporequisito(7003)
                    ->where('proceso_id', NULL)
                    ->select('id', 'descripcion', 'param_tiporequisito')
                    ->paginate($this->cant_legals);
                break;
            case 15002:
                $clientes = Requisito::whereProceso_id($this->id_proceso)
                    ->whereParam_tiporequisito(7001)
                    ->where('subproceso_id', NULL)
                    ->select('id', 'descripcion', 'param_tiporequisito')
                    ->paginate($this->cant_clientes);
                $isos = Requisito::whereProceso_id($this->id_proceso)
                    ->whereParam_tiporequisito(7002)
                    ->where('subproceso_id', NULL)
                    ->select('id', 'descripcion', 'param_tiporequisito')
                    ->paginate($this->cant_isos);
                $legals = Requisito::whereProceso_id($this->id_proceso)
                    ->whereParam_tiporequisito(7003)
                    ->where('subproceso_id', NULL)
                    ->select('id', 'descripcion', 'param_tiporequisito')
                    ->paginate($this->cant_legals);
                break;
            case 15003:
                $clientes = Requisito::whereSubproceso_id($this->id_proceso)
                    ->whereParam_tiporequisito(7001)
                    ->where('microproceso_id', NULL)
                    ->select('id', 'descripcion', 'param_tiporequisito')
                    ->paginate($this->cant_clientes);
                $isos = Requisito::whereSubproceso_id($this->id_proceso)
                    ->whereParam_tiporequisito(7002)
                    ->where('microproceso_id', NULL)
                    ->select('id', 'descripcion', 'param_tiporequisito')
                    ->paginate($this->cant_isos);
                $legals = Requisito::whereSubproceso_id($this->id_proceso)
                    ->whereParam_tiporequisito(7003)
                    ->where('microproceso_id', NULL)
                    ->select('id', 'descripcion', 'param_tiporequisito')
                    ->paginate($this->cant_legals);
                break;
            case 15004:
                $clientes = Requisito::whereMicroproceso_id($this->id_proceso)
                    ->whereParam_tiporequisito(7001)
                    ->select('id', 'descripcion', 'param_tiporequisito')
                    ->paginate($this->cant_clientes);
                $isos = Requisito::whereMicroproceso_id($this->id_proceso)
                    ->whereParam_tiporequisito(7002)
                    ->select('id', 'descripcion', 'param_tiporequisito')
                    ->paginate($this->cant_isos);
                $legals = Requisito::whereMicroproceso_id($this->id_proceso)
                    ->whereParam_tiporequisito(7003)
                    ->select('id', 'descripcion', 'param_tiporequisito')
                    ->paginate($this->cant_legals);
                break;
        }
        return view('livewire.caracterizacion.requisitos.view', compact('clientes', 'isos', 'legals'));
    }
    public function openModalRequisito($tprequisito)
    {
        $this->tprequisito = $tprequisito;
        $this->openRequisito = true;
    }
    public function storeRequisito()
    {
        $this->validate();
        switch ($this->nivel_proceso) {
            case 15001:
                Requisito::create([
                    'descripcion' => $this->descripcion,
                    'param_tiporequisito' => $this->tprequisito,
                    'macroproceso_id' => $this->id_proceso,
                    'created_user' => Auth::user()->id
                ]);
                break;
            case 15002:
                Requisito::create([
                    'descripcion' => $this->descripcion,
                    'param_tiporequisito' => $this->tprequisito,
                    'proceso_id' => $this->id_proceso,
                    'created_user' => Auth::user()->id
                ]);
                break;
            case 15003:
                Requisito::create([
                    'descripcion' => $this->descripcion,
                    'param_tiporequisito' => $this->tprequisito,
                    'subproceso_id' => $this->id_proceso,
                    'created_user' => Auth::user()->id
                ]);
                break;
            case 15004:
                Requisito::create([
                    'descripcion' => $this->descripcion,
                    'param_tiporequisito' => $this->tprequisito,
                    'microproceso_id' => $this->id_proceso,
                    'created_user' => Auth::user()->id
                ]);
                break;
        }
        $this->resetInputRequisito();

        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El requisito ha sido agregado satisfactoriamente.'
        ];
        $this->emit('alertRequisito', $datos);
    }
    public function editRequisito($id, $tprequisito)
    {
        $this->tprequisito = $tprequisito;
        $requisito = Requisito::findOrFail($id);
        $this->selected_id = $requisito->id;
        $this->descripcion = $requisito->descripcion;
        $this->updateRequisito = true;
    }
    public function updateRequisito()
    {
        $this->validate();

        if ($this->selected_id) {
            $requisito = Requisito::find($this->selected_id);
            $requisito->descripcion = $this->descripcion;
            $requisito->updated_user = Auth::user()->id;
            $requisito->save();

            $this->resetInputRequisito();

            $datos = [
                'tipo' => 'success',
                'mensaje' => 'El requisito ha sido actualizado satisfactoriamente.'
            ];
            $this->emit('alertRequisito', $datos);
        }
    }
    public function destroy($id)
    {
        if ($id) {
            $requisito = Requisito::find($id);
            $requisito->deleted_user = Auth::user()->id;
            $requisito->save();
            $requisito->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'El requisito ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertRequisito', $datos);
    }
    public function cancel()
    {
        $this->openRequisito = false;
        $this->updateRequisito = false;
        $this->resetInputRequisito();
    }
    public function resetInputRequisito()
    {
        $this->openRequisito = false;
        $this->updateRequisito = false;
        $this->descripcion = null;
    }
}
