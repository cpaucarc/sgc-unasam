<?php

namespace App\Http\Livewire\Caracterizacion;

use App\Models\Macroproceso;
use Livewire\Component;

class Versiones extends Component
{
    public $macroproceso;
    public $versiones;
    public $version;

    public function mount(){
        $this->versiones =  Macroproceso::with('version')->where('codigo', $this->macroproceso->codigo)->where('codigo', '!=', '')->get();
        $this->version =  $this->macroproceso->id;
    }

    public function render()
    {
        return view('livewire.caracterizacion.versiones');
    }

    public function seleccionarVersion()
    {
        return redirect()->route('macroprocesos.caracterizacion', ['macroproceso' => $this->version]);
    }
}
