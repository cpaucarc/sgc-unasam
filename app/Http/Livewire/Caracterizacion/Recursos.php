<?php

namespace App\Http\Livewire\Caracterizacion;

use Livewire\Component;
use App\Models\Macroproceso;
use App\Models\Microproceso;
use App\Models\Proceso;
use App\Models\Recurso;
use App\Models\Subproceso;
use Illuminate\Support\Facades\Auth;

class Recursos extends Component
{
    public $nivel_proceso, $id_proceso;
    public $tab_recurso = 'talento';
    public $openRecurso = false, $updateRecurso = false;
    public $tipoRecurso;
    public $selectRecurso = [];
    public $datos_recursos = [];
    public $lista_recursos = [];
    public $area;

    protected $listeners = ['storeRecurso', 'destroy'];

    public function render()
    {
        $this->datos_recursos = [];
        switch ($this->nivel_proceso) {
            case 15001:
                $talentos = Macroproceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15001')->where('param_tiporecurso', '9001')->get();
                $infraestructuras = Macroproceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15001')->where('param_tiporecurso', '9002')->get();
                $softwares = Macroproceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15001')->where('param_tiporecurso', '9003')->get();
                $recursos = Macroproceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15001')->where('param_tiporecurso', '9004')->get();
                break;
            case 15002:
                $talentos = Proceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15002')->where('param_tiporecurso', '9001')->get();
                $infraestructuras = Proceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15002')->where('param_tiporecurso', '9002')->get();
                $softwares = Proceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15002')->where('param_tiporecurso', '9003')->get();
                $recursos = Proceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15002')->where('param_tiporecurso', '9004')->get();
                break;
            case 15003:
                $talentos = Subproceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15003')->where('param_tiporecurso', '9001')->get();
                $infraestructuras = Subproceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15003')->where('param_tiporecurso', '9002')->get();
                $softwares = Subproceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15003')->where('param_tiporecurso', '9003')->get();
                $recursos = Subproceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15003')->where('param_tiporecurso', '9004')->get();
                break;
            case 15004:
                $talentos = Microproceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15004')->where('param_tiporecurso', '9001')->get();
                $infraestructuras = Microproceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15004')->where('param_tiporecurso', '9002')->get();
                $softwares = Microproceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15004')->where('param_tiporecurso', '9003')->get();
                $recursos = Microproceso::find($this->id_proceso)->recursos()->where('nivel_proceso', '15004')->where('param_tiporecurso', '9004')->get();
                break;
        }
        switch ($this->tipoRecurso) {
            case '9001':
                foreach ($talentos as $talento) {
                    array_push($this->datos_recursos, $talento->id);
                }
                break;
            case '9002':
                foreach ($infraestructuras as $infraestructura) {
                    array_push($this->datos_recursos, $infraestructura->id);
                }
                break;
            case '9003':
                foreach ($softwares as $software) {
                    array_push($this->datos_recursos, $software->id);
                }
                break;
            case '9004':
                foreach ($recursos as $recurso) {
                    array_push($this->datos_recursos, $recurso->id);
                }
                break;
        }
        $this->lista_recursos = Recurso::whereNotIn('id', $this->datos_recursos)->whereParam_tiporecurso($this->tipoRecurso)->select('id', 'nombre', 'param_tiporecurso')->get();;


        return view('livewire.caracterizacion.recursos.view', compact('talentos', 'recursos', 'infraestructuras', 'softwares'));
    }
    public function storeRecurso()
    {
        $recursos = $this->selectRecurso;
        $created_user = Auth::user()->id;

        for ($i = 0; $i < sizeof($recursos); $i++) {
            switch ($this->nivel_proceso) {
                case 15001:
                    $macroproceso = Macroproceso::find($this->id_proceso);
                    $macroproceso->recursos()->attach($recursos[$i]['id'], ['nivel_proceso' => $this->nivel_proceso, 'created_user' => $created_user]);
                    break;
                case 15002:
                    $proceso = Proceso::find($this->id_proceso);
                    $proceso->recursos()->attach($recursos[$i]['id'], ['nivel_proceso' => $this->nivel_proceso, 'created_user' => $created_user]);
                    break;
                case 15003:
                    $subproceso = Subproceso::find($this->id_proceso);
                    $subproceso->recursos()->attach($recursos[$i]['id'], ['nivel_proceso' => $this->nivel_proceso, 'created_user' => $created_user]);
                    break;
                case 15004:
                    $microproceso = Microproceso::find($this->id_proceso);
                    $microproceso->recursos()->attach($recursos[$i]['id'], ['nivel_proceso' => $this->nivel_proceso, 'created_user' => $created_user]);
                    break;
            }
        };

        $this->openRecurso = false;

        //falta reset

        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El recurso ha sido agregado satisfactoriamente.'
        ];
        $this->emit('alertRecurso', $datos);
    }
    public function destroy($id)
    {
        if ($id) {
            $deleted_user = Auth::user()->id;
            switch ($this->nivel_proceso) {
                case 15001:
                    $macroproceso = Macroproceso::find($this->id_proceso);
                    $macroproceso->recursos()->updateExistingPivot($id, ['deleted_user' => $deleted_user, 'deleted_at' => date('Y-m-d H:i:s')]);
                    break;
                case 15002:
                    $proceso = Proceso::find($this->id_proceso);
                    $proceso->recursos()->updateExistingPivot($id, ['deleted_user' => $deleted_user, 'deleted_at' => date('Y-m-d H:i:s')]);
                    break;
                case 15003:
                    $subproceso = Subproceso::find($this->id_proceso);
                    $subproceso->recursos()->updateExistingPivot($id, ['deleted_user' => $deleted_user, 'deleted_at' => date('Y-m-d H:i:s')]);
                    break;
                case 15004:
                    $microproceso = Microproceso::find($this->id_proceso);
                    $microproceso->recursos()->updateExistingPivot($id, ['deleted_user' => $deleted_user, 'deleted_at' => date('Y-m-d H:i:s')]);
                    break;
            }

            $datos = ['tipo' => 'error', 'mensaje' => 'El recurso ha sido eliminado satisfactoriamente.'];
            $this->emit('alertRecurso', $datos);
        }
    }

    public function openRecurso($tipoRecurso)
    {
        $this->tipoRecurso = $tipoRecurso;
        $this->openRecurso = true;
        $this->emit('obtenerRecursos');
    }
    public function cancel()
    {
        $this->openRecurso = false;
        $this->updateRecurso = false;
    }
}
