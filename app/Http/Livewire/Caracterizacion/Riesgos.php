<?php

namespace App\Http\Livewire\Caracterizacion;

use App\Models\Riesgo;
use App\Models\Proceso;
use Livewire\Component;
use App\Models\Macroproceso;
use Illuminate\Support\Facades\Auth;

class Riesgos extends Component
{
    public $nivel_proceso, $id_proceso;
    public $openRiesgo = false, $updateRiesgo = false;
    public $nombre, $descripcion;
    public $area;

    protected $listeners = ['destroy'];

    protected $rules = [
        'nombre'       => 'required'
    ];

    public function render()
    {
        $riesgos = Riesgo::whereNivel_proceso($this->nivel_proceso)->whereProceso_id($this->id_proceso)->get();
        return view('livewire.caracterizacion.riesgos.view', compact('riesgos'));
    }
    public function modalRiesgo()
    {
        $this->openRiesgo = true;
    }
    public function storeRiesgo()
    {
        $this->validate();
        Riesgo::create([
            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'nivel_proceso' => $this->nivel_proceso,
            'proceso_id' => $this->id_proceso,
            'created_user' => Auth::user()->id
        ]);
        $this->resetInputRiesgo();

        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El Riesgo ha sido agregado satisfactoriamente.'
        ];
        $this->emit('alertRiesgo', $datos);
    }
    public function editRiesgo($id)
    {
        $riesgo = Riesgo::findOrFail($id);
        $this->selected_id = $riesgo->id;
        $this->nombre = $riesgo->nombre;
        $this->descripcion = $riesgo->descripcion;
        $this->updateRiesgo = true;
    }
    public function updateRiesgo()
    {
        $this->validate();

        if ($this->selected_id) {
            $riesgo = Riesgo::find($this->selected_id);
            $riesgo->nombre = $this->nombre;
            $riesgo->descripcion = $this->descripcion;
            $riesgo->updated_user = Auth::user()->id;
            $riesgo->save();

            $this->resetInputRiesgo();

            $datos = [
                'tipo' => 'success',
                'mensaje' => 'El riesgo ha sido actualizado satisfactoriamente.'
            ];
            $this->emit('alertRiesgo', $datos);
        }
    }
    public function destroy($id)
    {
        if ($id) {
            $riesgo = Riesgo::find($id);
            $riesgo->deleted_user = Auth::user()->id;
            $riesgo->save();
            $riesgo->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'El riesgo ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertRiesgo', $datos);
    }
    public function cancel()
    {
        $this->resetInputRiesgo();
    }
    public function resetInputRiesgo()
    {
        $this->openRiesgo = false;
        $this->updateRiesgo = false;
        $this->nombre = null;
        $this->descripcion = null;
    }
}
