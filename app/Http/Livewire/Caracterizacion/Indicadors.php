<?php

namespace App\Http\Livewire\Caracterizacion;

use Livewire\Component;
use App\Models\Macroproceso;
use App\Models\Microproceso;
use App\Models\Proceso;
use App\Models\Recurso;
use App\Models\Subproceso;
use Illuminate\Support\Facades\Auth;

class Indicadors extends Component
{
    public $nivel_proceso, $id_proceso;
    // public $tab_recurso = 'talento';
    // public $openRecurso = false, $updateRecurso = false;
    // public $tipoRecurso;
    // public $selectRecurso = [];
    // public $datos_recursos = [];
    // public $lista_recursos = [];

    protected $listeners = ['storeRecurso', 'destroy'];

    public function render()
    {
        $this->datos_recursos = [];
        switch ($this->nivel_proceso) {
            case 15001:
                $eficacias = Macroproceso::find($this->id_proceso)->indicadors()->where('param_tipoindicador', '10001')->where('proceso_id', NULL)->get();
                $eficiencias = Macroproceso::find($this->id_proceso)->indicadors()->where('param_tipoindicador', '10002')->where('proceso_id', NULL)->get();
                $efectividads = Macroproceso::find($this->id_proceso)->indicadors()->where('param_tipoindicador', '10003')->where('proceso_id', NULL)->get();
                break;
            case 15002:
                $eficacias = Subproceso::find($this->id_proceso)->indicadors()->where('param_tipoindicador', '10001')->where('subproceso_id', NULL)->get();
                $eficiencias = Subproceso::find($this->id_proceso)->indicadors()->where('param_tipoindicador', '10002')->where('subproceso_id', NULL)->get();
                $efectividads = Subproceso::find($this->id_proceso)->indicadors()->where('param_tipoindicador', '10003')->where('subproceso_id', NULL)->get();
                break;
        }

        return view('livewire.caracterizacion.indicadors.view', compact('eficacias', 'eficiencias', 'efectividads'));
    }
    // public function storeRecurso()
    // {
    //     $recursos = $this->selectRecurso;
    //     $created_user = Auth::user()->id;

    //     for ($i = 0; $i < sizeof($recursos); $i++) {
    //         switch ($this->nivel_proceso) {
    //             case 15001:
    //                 $macroproceso = Macroproceso::find($this->id_proceso);
    //                 $macroproceso->recursos()->attach($recursos[$i]['id'], ['nivel_proceso' => $this->nivel_proceso, 'created_user' => $created_user]);
    //                 break;
    //             case 15002:
    //                 $proceso = Proceso::find($this->id_proceso);
    //                 $proceso->recursos()->attach($recursos[$i]['id'], ['nivel_proceso' => $this->nivel_proceso, 'created_user' => $created_user]);
    //                 break;
    //             case 15003:
    //                 $subproceso = Subproceso::find($this->id_proceso);
    //                 $subproceso->recursos()->attach($recursos[$i]['id'], ['nivel_proceso' => $this->nivel_proceso, 'created_user' => $created_user]);
    //                 break;
    //             case 15004:
    //                 $microproceso = Microproceso::find($this->id_proceso);
    //                 $microproceso->recursos()->attach($recursos[$i]['id'], ['nivel_proceso' => $this->nivel_proceso, 'created_user' => $created_user]);
    //                 break;
    //         }
    //     };

    //     $this->openRecurso = false;

    //     //falta reset

    //     $datos = [
    //         'tipo' => 'success',
    //         'mensaje' => 'El recurso ha sido agregado satisfactoriamente.'
    //     ];
    //     $this->emit('alertRecurso', $datos);
    // }
    // public function destroy($id)
    // {
    //     if ($id) {
    //         $deleted_user = Auth::user()->id;
    //         switch ($this->nivel_proceso) {
    //             case 15001:
    //                 $macroproceso = Macroproceso::find($this->id_proceso);
    //                 $macroproceso->recursos()->updateExistingPivot($id, ['deleted_user' => $deleted_user, 'deleted_at' => date('Y-m-d H:i:s')]);
    //                 break;
    //             case 15002:
    //                 $proceso = Proceso::find($this->id_proceso);
    //                 $proceso->recursos()->updateExistingPivot($id, ['deleted_user' => $deleted_user, 'deleted_at' => date('Y-m-d H:i:s')]);
    //                 break;
    //             case 15003:
    //                 $subproceso = Subproceso::find($this->id_proceso);
    //                 $subproceso->recursos()->updateExistingPivot($id, ['deleted_user' => $deleted_user, 'deleted_at' => date('Y-m-d H:i:s')]);
    //                 break;
    //             case 15004:
    //                 $microproceso = Microproceso::find($this->id_proceso);
    //                 $microproceso->recursos()->updateExistingPivot($id, ['deleted_user' => $deleted_user, 'deleted_at' => date('Y-m-d H:i:s')]);
    //                 break;
    //         }

    //         $datos = ['tipo' => 'error', 'mensaje' => 'El recurso ha sido eliminado satisfactoriamente.'];
    //         $this->emit('alertRecurso', $datos);
    //     }
    // }

    // public function openRecurso($tipoRecurso)
    // {
    //     $this->tipoRecurso = $tipoRecurso;
    //     $this->openRecurso = true;
    //     $this->emit('obtenerRecursos');
    // }
    // public function cancel()
    // {
    //     $this->openRecurso = false;
    //     $this->updateRecurso = false;
    // }
}
