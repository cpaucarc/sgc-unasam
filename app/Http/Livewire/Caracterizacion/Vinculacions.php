<?php

namespace App\Http\Livewire\Caracterizacion;

use Livewire\Component;
use App\Models\Vinculacion;
use App\Models\Macroproceso;
use App\Models\Microproceso;
use App\Models\Proceso;
use App\Models\Subproceso;
use Illuminate\Support\Facades\Auth;

class Vinculacions extends Component
{
    public $nivel_proceso, $id_proceso, $multiselect;
    public $vinculos, $vinculacion;
    public $datos = [];
    public $datosDelete = [];
    public $open = false;
    public $area;

    protected $listeners = ['store', 'destroy'];

    public function render()
    {
        $this->datos = [];
        switch ($this->nivel_proceso) {
            case 15001:
                $vinculaciones = Macroproceso::find($this->id_proceso)->vinculacions()->where('nivel_proceso', 15001)->orderBy('vin_proceso_id', 'asc')->get();
                array_push($this->datos, $this->id_proceso);
                foreach ($vinculaciones as $vinculacion) {
                    array_push($this->datos, $vinculacion->vin_proceso_id);
                }
                $this->vinculos = Macroproceso::whereNotIn('id', $this->datos)->select('id', 'nombre', 'codigo')->get();
                break;
            case 15002:
                $vinculaciones = Proceso::find($this->id_proceso)->vinculacions()->where('nivel_proceso', 15002)->orderBy('vin_proceso_id', 'asc')->get();
                array_push($this->datos, $this->id_proceso);
                foreach ($vinculaciones as $vinculacion) {
                    array_push($this->datos, $vinculacion->vin_proceso_id);
                }
                $this->vinculos = Proceso::whereNotIn('id', $this->datos)->select('id', 'nombre', 'codigo')->get();
                break;
            case 15003:
                $vinculaciones = Subproceso::find($this->id_proceso)->vinculacions()->where('nivel_proceso', 15003)->orderBy('vin_proceso_id', 'asc')->get();
                array_push($this->datos, $this->id_proceso);
                foreach ($vinculaciones as $vinculacion) {
                    array_push($this->datos, $vinculacion->vin_proceso_id);
                }
                $this->vinculos = Subproceso::whereNotIn('id', $this->datos)->select('id', 'nombre', 'codigo')->get();
                break;
            case 15004:
                $vinculaciones = Microproceso::find($this->id_proceso)->vinculacions()->where('nivel_proceso', 15004)->orderBy('vin_proceso_id', 'asc')->get();
                array_push($this->datos, $this->id_proceso);
                foreach ($vinculaciones as $vinculacion) {
                    array_push($this->datos, $vinculacion->vin_proceso_id);
                }
                $this->vinculos = Microproceso::whereNotIn('id', $this->datos)->select('id', 'nombre', 'codigo')->get();
                break;
        }

        return view('livewire.caracterizacion.vinculacions.view', compact('vinculaciones'));
    }
    public function store()
    {
        $vinculaciones = $this->multiselect;
        for ($i = 0; $i < sizeof($vinculaciones); $i++) {
            Vinculacion::create([
                'nivel_proceso' => $this->nivel_proceso,
                'proceso_id' => $this->id_proceso,
                'vin_nivel_proceso' => $this->nivel_proceso,
                'vin_proceso_id' => $vinculaciones[$i]['id'],
                'activo' => 1,
                'created_user' => Auth::user()->id
            ]);
        };
        $this->open = false;
        $this->resetInput();

        $datos = [
            'tipo' => 'success',
            'mensaje' => 'La vinculación se ha creado satisfactoriamente.'
        ];
        $this->emit('alertVinculacion', $datos);
    }

    public function destroy($id)
    {
        if ($id) {
            $deleted_user = Auth::user()->id;
            $vinculo = Vinculacion::find($id);
            $vinculo->deleted_user = $deleted_user;
            $vinculo->save();
            $vinculo->delete();
        }

        $this->resetInput();

        $datos = [
            'tipo' => 'error',
            'mensaje' => 'La vinculación ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertVinculacion', $datos);
    }

    public function openModal()
    {
        $this->open = true;
        $this->emit('obtenerVinculos');
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->multiselect = [];
    }
}
