<?php

namespace App\Http\Livewire\Caracterizacion;

use App\Models\Documento;
use App\Models\Entrada;
use App\Models\Interesado;
use Livewire\Component;
use App\Models\Macroproceso;
use App\Models\Microproceso;
use App\Models\Proceso;
use App\Models\Subproceso;
use Illuminate\Support\Facades\Auth;

class Actividads extends Component
{
    public $nivel_proceso, $id_proceso;
    public $openEntrada = false, $updateEntrada = false;
    public $documento_salidas = [];
    public $proceso_id, $documento_entrada, $documento_salida, $duenio_entrada, $duenio_salida;

    protected $listeners = ['obtenerSalidas', 'storeEntrada'];

    protected $rules = [
        'proceso_id' => 'required',
        'duenio_entrada' => 'required',
        'duenio_salida' => 'required',
        'documento_entrada' => 'required',
        'documento_salida' => 'required'
    ];

    public function render()
    {
        switch ($this->nivel_proceso) {
            case 15002:
                $entradas = Proceso::where('procesos.id', $this->id_proceso)
                    ->join('proceso_actividad', 'proceso_actividad.proceso_id', '=', 'procesos.id')
                    ->join('actividads', 'actividads.id', '=', 'proceso_actividad.actividad_id')
                    ->join('entradas', 'entradas.proceso_id', '=', 'proceso_actividad.id')
                    ->join('documentos', 'documentos.id', '=', 'entradas.documento_id')
                    ->join('interesados', 'interesados.id', '=', 'entradas.duenio_id')
                    ->where('entradas.nivel_proceso', '15005')
                    ->where('entradas.salida', 0)
                    ->orderBy('proceso_actividad.orden', 'asc')
                    ->select('actividads.descripcion', 'phva', 'nombreReferencial', 'interesados.nombre as interesado', 'controles', 'salida_id')
                    ->get();
                $actividads = Proceso::find($this->id_proceso)->actividads()->orderBy('orden', 'asc')->get();
                break;
            case 15003:
                $entradas = Subproceso::where('subproceso.id', $this->id_proceso)
                    ->join('proceso_actividad', 'proceso_actividad.proceso_id', '=', 'procesos.id')
                    ->join('actividads', 'actividads.id', '=', 'proceso_actividad.actividad_id')
                    ->join('entradas', 'entradas.proceso_id', '=', 'proceso_actividad.id')
                    ->join('documentos', 'documentos.id', '=', 'entradas.documento_id')
                    ->join('interesados', 'interesados.id', '=', 'entradas.duenio_id')
                    ->where('entradas.nivel_proceso', '15005')
                    ->where('entradas.salida', 0)
                    ->orderBy('proceso_actividad.orden', 'asc')
                    ->select('actividads.descripcion', 'phva', 'nombreReferencial', 'interesados.nombre as interesado', 'controles', 'salida_id')
                    ->get();
                $actividads = Subproceso::find($this->id_proceso)->actividads()->orderBy('orden', 'asc')->get();
                break;
            case 15004:
                $entradas = Microproceso::where('microproceso.id', $this->id_proceso)
                    ->join('proceso_actividad', 'proceso_actividad.proceso_id', '=', 'procesos.id')
                    ->join('actividads', 'actividads.id', '=', 'proceso_actividad.actividad_id')
                    ->join('entradas', 'entradas.proceso_id', '=', 'proceso_actividad.id')
                    ->join('documentos', 'documentos.id', '=', 'entradas.documento_id')
                    ->join('interesados', 'interesados.id', '=', 'entradas.duenio_id')
                    ->where('entradas.nivel_proceso', '15005')
                    ->where('entradas.salida', 0)
                    ->orderBy('proceso_actividad.orden', 'asc')
                    ->select('actividads.descripcion', 'phva', 'nombreReferencial', 'interesados.nombre as interesado', 'controles', 'salida_id')
                    ->get();
                $actividads = Microproceso::find($this->id_proceso)->actividads()->orderBy('orden', 'asc')->get();
                break;
        }
        $documentos = Documento::whereActivo(1)->get();
        $interesados = Interesado::whereActivo(1)->get();
        return view('livewire.caracterizacion.entradas.actividads.view', compact('entradas', 'actividads', 'documentos', 'interesados'));
    }
    public function modalEntrada()
    {
        $this->openEntrada = true;
    }
    public function storeEntrada()
    {
        $this->validate();
        $entrada = new Entrada;
        $entrada->duenio_id = $this->duenio_entrada;
        $entrada->nivel_proceso = '15005';
        $entrada->proceso_id = $this->proceso_id;
        $entrada->salida = 0;
        $entrada->documento_id = $this->documento_entrada;
        $entrada->created_user = Auth::user()->id;
        $entrada->save();

        $salida = new Entrada;
        $salida->duenio_id = $this->duenio_salida;
        $salida->nivel_proceso = '15005';
        $salida->proceso_id = $this->proceso_id;
        $salida->salida = 1;
        $salida->documento_id = $this->documento_salida;
        $salida->created_user = Auth::user()->id;
        $salida->save();

        $entrada->salida_id = $salida->id;
        $entrada->save();
        $this->resetInputEntradas();
        $datos = [
            'tipo' => 'success',
            'mensaje' => 'La entrada - actividad - salida ha sido agregado satisfactoriamente.'
        ];
        $this->emit('resetearSelect');
        $this->emit('alertEntrada', $datos);
    }
    public function obtenerSalidas()
    {
        $this->documento_salida = null;
        $this->documento_salidas = Documento::where('id', '!=', $this->documento_entrada)->whereActivo(1)->get();
        $this->emit('imprimirSalidas');
    }
    public function cancel()
    {
        $this->updateEntrada = false;
        $this->openEntrada = false;
        $this->resetInputEntradas();
    }
    public function resetInputEntradas()
    {
        $this->openEntrada = false;
        $this->proceso_id = NULL;
        $this->documento_entrada = NULL;
        $this->documento_salida = NULL;
        $this->duenio_entrada = NULL;
        $this->duenio_salida = NULL;
    }
}
