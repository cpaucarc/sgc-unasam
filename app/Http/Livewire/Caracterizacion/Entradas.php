<?php

namespace App\Http\Livewire\Caracterizacion;

use App\Models\Anio;
use App\Models\Archivo;
use App\Models\Detalle;
use App\Models\Entrada;
use App\Models\Proceso;
use Livewire\Component;
use App\Models\Documento;
use App\Models\Interesado;
use App\Models\Subproceso;
use App\Models\Macroproceso;
use App\Models\Microproceso;
use App\Models\Version;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\WithFileUploads;

class Entradas extends Component
{
    use WithFileUploads;

    public $nivel_proceso, $id_proceso;
    public $openEntrada = false, $updateEntrada = false, $showArchivos = false, $showUpload = false;
    public $documento_salidas = [];
    public $proceso_id, $documento_entrada, $documento_salida, $duenio_entrada, $duenio_salida;
    public $nom_documento, $archivos = [], $tipoEntrada, $param_conservacion, $placeholder;
    public $nom_proceso, $entrada_id;
    public $version, $anio_id, $param_periodo, $fechaIniVigencia, $fechaFinVigencia, $descripcion, $file_upload, $documento_id, $conservacion, $nom_conservacion, $semanal_mes;
    public $disabled = '', $disabled2 = '', $ocultar = 'd-none';
    public $periodos = [], $semanas = [];
    public $area;

    protected $listeners = ['obtenerSalidas', 'storeEntrada', 'destroy'];

    protected $rules = [
        'proceso_id' => 'required',
        'duenio_entrada' => 'required',
        'duenio_salida' => 'required',
        'documento_entrada' => 'required',
        'documento_salida' => 'required'
    ];
    protected $rule_archivo = [
        'descripcion' => 'required',
        'anio_id' => 'required',
        'fechaIniVigencia' => 'required',
        'file_upload' => 'required|max:2048'
    ];
    protected $msjError_archivo = [
        'descripcion.required' => 'El campo descripción es obligatorio.',
        'anio_id.required' => 'El campo año es obligatorio.',
        'fechaIniVigencia.required' => 'El campo inicio de vigencia es obligatorio.',
        'file_upload.required' => 'El campo archivo es obligatorio.'
    ];
    protected $rule_archivo2 = [
        'descripcion' => 'required',
        'anio_id' => 'required',
        'fechaIniVigencia' => 'required',
        'param_periodo' => 'required',
        'file_upload' => 'required|max:2048'
    ];
    protected $msjError_archivo2 = [
        'descripcion.required' => 'El campo descripción es obligatorio.',
        'anio_id.required' => 'El campo año es obligatorio.',
        'fechaIniVigencia.required' => 'El campo inicio de vigencia es obligatorio.',
        'param_periodo.required' => 'El campo periodo es obligatorio.',
        'file_upload.required' => 'El campo archivo es obligatorio.'
    ];
    protected $rule_archivo3 = [
        'descripcion' => 'required',
        'anio_id' => 'required',
        'fechaIniVigencia' => 'required',
        'param_periodo' => 'required',
        'semanal_mes' => 'required',
        'file_upload' => 'required|max:2048'
    ];
    protected $msjError_archivo3 = [
        'descripcion.required' => 'El campo descripción es obligatorio.',
        'anio_id.required' => 'El campo año es obligatorio.',
        'fechaIniVigencia.required' => 'El campo inicio de vigencia es obligatorio.',
        'param_periodo.required' => 'El campo periodo es obligatorio.',
        'semanal_mes.required' => 'El campo semana es obligatorio.',
        'file_upload.required' => 'El campo archivo es obligatorio.'
    ];

    public function render()
    {
        switch ($this->nivel_proceso) {
            case 15002:
                $entradas = Proceso::join('entradas', 'entradas.proceso_id', '=', 'procesos.id')
                    ->join('documentos', 'documentos.id', '=', 'entradas.documento_id')
                    ->join('interesados', 'interesados.id', '=', 'entradas.duenio_id')
                    ->where('entradas.salida', 0)
                    ->where('procesos.macroproceso_id', $this->id_proceso)
                    ->select('entradas.id as entrada_id', 'entradas.salida_id', 'entradas.documento_id', 'interesados.nombre as interesado', 'procesos.codigo', 'procesos.nombre', 'documentos.nombreReferencial')
                    ->get();
                $procesos = Macroproceso::find($this->id_proceso)->procesos()->orderBy('orden', 'asc')->get();
                break;
            case 15003:
                $entradas = Subproceso::join('entradas', 'entradas.proceso_id', '=', 'subprocesos.id')
                    ->join('documentos', 'documentos.id', '=', 'entradas.documento_id')
                    ->join('interesados', 'interesados.id', '=', 'entradas.duenio_id')
                    ->where('entradas.salida', 0)
                    ->where('subprocesos.proceso_id', $this->id_proceso)
                    ->select('entradas.id as entrada_id', 'entradas.salida_id', 'entradas.documento_id', 'interesados.nombre as interesado', 'subprocesos.codigo', 'subprocesos.nombre', 'documentos.nombreReferencial')
                    ->get();
                $procesos = Proceso::find($this->id_proceso)->subprocesos()->orderBy('orden', 'asc')->get();
                break;
            case 15004:
                $entradas = Microproceso::join('entradas', 'entradas.proceso_id', '=', 'microprocesos.id')
                    ->join('documentos', 'documentos.id', '=', 'entradas.documento_id')
                    ->join('interesados', 'interesados.id', '=', 'entradas.duenio_id')
                    ->where('entradas.salida', 0)
                    ->where('microprocesos.proceso_id', $this->id_proceso)
                    ->select('entradas.id as entrada_id', 'entradas.salida_id', 'entradas.documento_id', 'interesados.nombre as interesado', 'microprocesos.codigo', 'microprocesos.nombre', 'documentos.nombreReferencial')
                    ->get();
                $procesos = Subproceso::find($this->id_proceso)->microprocesos()->orderBy('orden', 'asc')->get();
                break;
        }
        $documentos = Documento::whereActivo(1)->get();
        $interesados = Interesado::whereActivo(1)->get();
        $versiones = Version::whereActivo(1)->get();
        $anios = Anio::whereActivo(1)->orderBy('anio', 'asc')->take(10)->get();

        return view('livewire.caracterizacion.entradas.view', compact('entradas', 'documentos', 'procesos', 'interesados', 'versiones', 'anios'));
    }
    public function modalEntrada()
    {
        $this->openEntrada = true;
    }
    public function storeEntrada()
    {
        $this->validate();
        $entrada = new Entrada;
        $entrada->duenio_id = $this->duenio_entrada;
        $entrada->nivel_proceso = $this->nivel_proceso;
        $entrada->proceso_id = $this->proceso_id;
        $entrada->salida = 0;
        $entrada->documento_id = $this->documento_entrada;
        $entrada->created_user = Auth::user()->id;
        $entrada->save();

        $salida = new Entrada;
        $salida->duenio_id = $this->duenio_salida;
        $salida->nivel_proceso = $this->nivel_proceso;
        $salida->proceso_id = $this->proceso_id;
        $salida->salida = 1;
        $salida->documento_id = $this->documento_salida;
        $salida->created_user = Auth::user()->id;
        $salida->save();

        $entrada->salida_id = $salida->id;
        $entrada->save();
        $this->resetInputEntradas();
        $datos = [
            'tipo' => 'success',
            'mensaje' => 'La entrada - salida ha sido agregado satisfactoriamente.'
        ];
        $this->emit('alertEntrada', $datos);
    }
    public function destroy($id)
    {
        if ($id) {
            $archivo = Archivo::find($id);
            $archivo->deleted_user = Auth::user()->id;
            $archivo->save();
            $archivo->delete();
            $documento = Documento::find($archivo->documento_id);
            $this->archivos = $documento->archivos;
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'El archivo se ha eliminado satisfactoriamente.'
        ];
        $this->emit('alertEntrada', $datos);
    }
    public function descargar($id)
    {
        $archivo = Archivo::find($id);
        $datos = ['tipo' => 'success', 'mensaje' => 'El archivo se ha descargado satisfactoriamente.'];
        $this->emit('alertEntrada', $datos);
        return Storage::response($archivo->ruta, $archivo->nombre);
    }
    public function obtenerSalidas()
    {
        $this->documento_salida = null;
        $this->documento_salidas = Documento::where('id', '!=', $this->documento_entrada)->whereActivo(1)->get();
        $this->emit('imprimirSalidas');
    }
    public function entradasArchivos($id)
    {
        //falta!!!
        $entrada = Entrada::find($id);
        $this->tipoEntrada = $entrada->salida;
        $documento = Documento::find($entrada->documento_id);
        $this->nom_documento = $documento->nombreReferencial;

        $this->archivos = Entrada::join('entrada_archivo', 'entrada_archivo.entrada_id', '=', 'entradas.id')
            ->join('archivos', 'archivos.id', '=', 'entrada_archivo.archivo_id')
            ->where('entradas.documento_id', $entrada->documento_id)
            ->where('nivel_proceso', $entrada->nivel_proceso)
            ->where('proceso_id', $entrada->proceso_id)
            ->where('archivos.deleted_at', NULL)
            // ->where('salida', $this->tipoEntrada)
            ->get();

        $this->showArchivos = true;
    }
    public function subirArchivo($id)
    {
        $this->entrada_id = $id;
        $entrada = Entrada::find($id);
        $this->tipoEntrada = $entrada->salida;
        $this->proceso_id = $entrada->proceso_id;
        $documento = Documento::find($entrada->documento_id);
        $this->documento_id = $entrada->documento_id;
        $this->nom_documento = $documento->nombreReferencial;
        $this->placeholder = $documento->placeholder;
        if ($documento->param_conservacion) {
            $this->param_conservacion = $documento->param_conservacion;
            $opcion = $documento->param_conservacion;
            $this->conservacion = $documento->conservacion->detalle;
        } else {
            $opcion = '-';
            $this->conservacion = '-';
        }
        $this->archivos = $documento->archivos;

        switch ($opcion) {
            case '-':
                $this->disabled = 'disabled';
                $this->disabled2 = 'disabled';
                $this->ocultar = 'd-none';
                $this->nom_conservacion = '';
                $this->param_periodo = $documento->param_conservacion;
                break;
            case '16001':
                $this->disabled = 'disabled';
                $this->disabled2 = 'disabled';
                $this->ocultar = 'd-none';
                $this->nom_conservacion = '';
                $this->param_periodo = $documento->param_conservacion;
                break;
            case '16002':
                $this->disabled = '';
                $this->disabled2 = 'disabled';
                $this->ocultar = 'd-none';
                $this->nom_conservacion = '';
                break;
            case '16003':
                $this->disabled = '';
                $this->disabled2 = 'disabled';
                $this->ocultar = 'd-none';
                $this->nom_conservacion = 'Semestral';
                $this->periodos = Detalle::whereParametro_id(19)->whereActivo(1)->get();
                break;
            case '16004':
                $this->disabled = '';
                $this->disabled2 = 'disabled';
                $this->ocultar = 'd-none';
                $this->nom_conservacion = 'Mensual';
                $this->periodos = Detalle::whereParametro_id(22)->whereActivo(1)->get();
                break;
            case '16005':
                $this->disabled = '';
                $this->disabled2 = '';
                $this->ocultar = '';
                $this->nom_conservacion = 'Mensual';
                $this->periodos = Detalle::whereParametro_id(22)->whereActivo(1)->get();
                $this->semanas = Detalle::whereParametro_id(23)->whereActivo(1)->get();
                break;
        }
        $this->showUpload = true;
    }
    public function storeArchivo()
    {
        switch ($this->param_conservacion) {
            case '-':
                $this->validate($this->rule_archivo, $this->msjError_archivo);
                break;
            case '16001':
                $this->validate($this->rule_archivo, $this->msjError_archivo);
                break;
            case '16002':
                $this->validate($this->rule_archivo, $this->msjError_archivo);
                break;
            case '16003':
                $this->validate($this->rule_archivo2, $this->msjError_archivo2);
                break;
            case '16004':
                $this->validate($this->rule_archivo2, $this->msjError_archivo2);
                break;
            case '16005':
                $this->validate($this->rule_archivo3, $this->msjError_archivo3);
                break;
        }

        $file_ruta = $this->file_upload->store('archivos/' . $this->documento_id);
        $nombre_file = $this->file_upload->getClientOriginalName();
        $extencion = $this->file_upload->getClientOriginalExtension();
        $created_user = Auth::user()->id;

        $archivo = new Archivo;
        $archivo->nombre = $nombre_file;
        $archivo->descripcion = $this->descripcion;
        $archivo->anio_id = $this->anio_id;
        $archivo->version = $this->version;
        $archivo->ruta = $file_ruta;
        $archivo->fechaIniVigencia = date('Y-m-d', strtotime($this->fechaIniVigencia));
        $archivo->fechaFinVigencia = date('Y-m-d', strtotime($this->fechaFinVigencia));
        $archivo->param_periodo = $this->param_periodo;
        $archivo->semanal_mes = $this->semanal_mes;
        $archivo->extension = $extencion;
        $archivo->documento_id = $this->documento_id;
        $archivo->created_user = $created_user;
        $archivo->save();

        $entrada = Entrada::find($this->entrada_id);
        $entrada->archivos()->attach($archivo->id, ['created_user' => $created_user]);


        $this->showUpload = false;
        $this->resetInputEntradas();
        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El archivo se ha agregado satisfactoriamente.'
        ];
        $this->emit('alertEntrada', $datos);
    }
    public function cancel()
    {
        $this->updateEntrada = false;
        $this->openEntrada = false;
        $this->showUpload = false;
        $this->showArchivos= false;
        $this->resetInputEntradas();
        $this->resetValidation();
    }
    public function resetInputEntradas()
    {
        $this->openEntrada = false;
        $this->proceso_id = NULL;
        $this->documento_entrada = NULL;
        $this->documento_salida = NULL;
        $this->duenio_entrada = NULL;
        $this->duenio_salida = NULL;

        $this->version = NULL;
        $this->anio_id = NULL;
        $this->param_periodo = NULL;
        $this->fechaIniVigencia = NULL;
        $this->fechaFinVigencia = NULL;
        $this->descripcion = NULL;
        $this->file_upload = NULL;
        $this->documento_id = NULL;
        $this->emit('resetearSelect');
    }
}
