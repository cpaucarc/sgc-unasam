<?php

namespace App\Http\Livewire;

use App\Models\Auth\Group;
use App\Models\Auth\Category;
use App\Models\Role;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Roles extends Component
{
    use WithPagination;
    use AuthorizesRequests;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $selected_id, $search, $name, $descripcion, $activo;
    public $updateMode = false;
    public $showMode = false;
    public $sort = 'id';
    public $direction = 'desc';
    public $grupos;
    public $permisos;
    public $rol;

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'name'       => 'required',
        'descripcion'   => 'required',
        'activo'   => 'required',
    ];

    public function mount()
    {
        $this->grupos = Group::with('categories.permissions')->get();
        $this->permisos = request()->user()->asignacion->rol->permissions->pluck('name');
    }

    public function render()
    {
        $this->authorize('viewAny', \App\Models\Role::class);

        $roles = Role::where('name', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);

        return view('livewire.roles.view', compact('roles'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
        $this->showMode = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->showMode = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->name = null;
        $this->descripcion = null;
        $this->activo = null;
    }

    public function store()
    {
        $this->validate();

        Role::create([
            'name' => $this->name,
            'descripcion' => $this->descripcion,
            // 'guard_name' => $this->descripcion,
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();

        $datos = [
            'tipo' => 'success',
            'mensaje' => 'Rol creado satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }

    public function edit($id)
    {
        $rol = Role::findOrFail($id);

        $this->selected_id = $id;
        $this->name = $rol->name;
        $this->descripcion = $rol->descripcion;
        $this->activo = $rol->activo;
        $this->permisos = $rol->permissions->pluck('name');
        $this->rol = $rol;
        $this->updateMode = true;
    }

    public function show($id)
    {
        $rol = Role::findOrFail($id);
        $this->selected_id = $id;
        $this->name = $rol->name;
        $this->descripcion = $rol->descripcion;
        $this->activo = $rol->activo;
        $this->showMode = true;
    }

    public function update()
    {
        $this->validate();

        if ($this->selected_id) {
            $record = Role::find($this->selected_id);
            $record->update([
                'name' => $this->name,
                //'guard_name' => $this->descripcion,
                'descripcion' => $this->descripcion,
                'activo' => $this->activo,
                'updated_user' => Auth::user()->id
            ]);

            $this->resetInput();
            $this->updateMode = false;

            $datos = [
                'tipo' => 'success',
                'mensaje' => 'Rol actualizado satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Role::where('id', $id);
            $record->delete();
            $datos = [
                'tipo' => 'error',
                'mensaje' => 'El rol se ha sido eliminado.'
            ];
            $this->emit('alertRespuesta', $datos);
        }
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }


    public function asignaPermiso($permiso)
    {
        $this->rol->givePermissionTo($permiso);
        $this->permisos = $this->rol->permissions->pluck('name');
    }
    public function quitaPermiso($permiso)
    {
        $this->rol->revokePermissionTo($permiso);
        $this->permisos = $this->rol->permissions->pluck('name');
    }

    public function asignaCategoria($categoria_id)
    {
        $categoria = Category::find($categoria_id);
        foreach ($categoria->permissions as $permission) {
            $this->rol->givePermissionTo($permission->name);
        }

        $this->permisos = $this->rol->permissions->pluck('name');
    }
    public function quitaCategoria($categoria_id)
    {
        $categoria = Category::find($categoria_id);
        foreach ($categoria->permissions as $permission) {
            $this->rol->revokePermissionTo($permission->name);
        }

        $this->permisos = $this->rol->permissions->pluck('name');
    }
}
