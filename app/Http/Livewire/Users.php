<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\User;
use App\Models\Persona;
use App\Models\Role;
use App\Models\Area;
use App\Models\Asignacion;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Validation\Rule;

class Users extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $selected_id, $searchUsuario,
        $name,
        $email,
        $password,
        $email_verified_at,
        $two_factor_secret,
        $two_factor_recovery_codes,
        $remember_token,
        $current_team_id,
        $profile_photo_path,
        $persona_id,
        $activo;

    public $updateMode = false;
    public $asignacion = false;
    public $sort = 'id';
    public $direction = 'desc';
    protected $rulesUsuario = [
        'name' => 'required|unique:users',
        'email' => 'required|email|unique:users',
        'password' => 'required',
        //'descripcion' => 'required',
    ];
    protected $msjErrorUsuario = [
        'name.required' => 'El campo usuario es obligatorio.',
        'name.unique' => 'El valor del campo usuario ya está en uso.',
        'email.required' => 'El campo email es obligatorio.',
        'email.unique' => 'El valor del campo email ya está en uso.',
        'email.email' => 'El valor del campo email no es correcto.',
        'password.required' => 'El campo password es obligatorio.'
    ];
    /*********************************PARA ASIGANACIONES**************** */
    public $areas = [], $roles = [], $asignacions = [];
    //$id
    public $user_id,
        $role_id,
        $area_id,
        $fechaIni,
        $fechaFin,
        $activoAsig;
    protected $msjErrorAsignacion = [
        'role_id.required' => 'El campo rol es obligatorio.',
        'role_id.unique' => 'La combinación de area y rol ya está en uso.'
    ];
    protected $listeners = ['render', 'destroy', 'destroyAsignacion'];

    public function render()
    {
        $personas = Persona::whereActivo(1)->get();
        $areas2 = Area::whereActivo(1)->get();
        $users = User::where('name', 'LIKE', '%' . $this->searchUsuario . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);
        $searchUsuario = '%' . $this->searchUsuario . '%';
        if ($this->asignacion) {
            $this->asignacions = Asignacion::whereUser_id($this->selected_id)->whereActivo(1)->get();
        }


        return view('livewire.users.view', compact('users', 'personas', 'areas2'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
        $this->asignacion = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->name = null;
        $this->email = null;
        $this->password = null;
        $this->persona_id = null;
        $this->activo = null;
        $this->fechaIni = null;
        $this->fechaFin = null;
        
        $this->emit('resetearSelect');
    }

    public function store()
    {
        $this->validate($this->rulesUsuario, $this->msjErrorUsuario);
        User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => Hash::make($this->password),
            'persona_id' => $this->persona_id,
            'activo' => '1',
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();
        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El Usuario ha sido creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
        //$this->emit('alert', 'Usuario creado satisfactoriamente.');
    }

    public function storeAsignacion()
    {
        $this->validate([
            'role_id' => [
                'required',
                Rule::unique('asignacions')->where(fn ($query) => $query->where('user_id', $this->selected_id)->where('area_id', $this->area_id)->where('role_id', $this->role_id)->whereNull('deleted_at'))
            ],
        ], $this->msjErrorAsignacion);
        if (!is_null($this->fechaIni)) {
            $this->fechaIni = date('Y-m-d', strtotime($this->fechaIni)); # code...
        }
        if (!is_null($this->fechaFin)) {
            $this->fechaFin = date('Y-m-d', strtotime($this->fechaFin));
        }

        Asignacion::create([
            'user_id' => $this->selected_id, // el seleccionado al ir a  asignacion
            'role_id' => $this->role_id,
            'area_id' => $this->area_id,
            'fechaIni' => $this->fechaIni,
            'fechaFin' => $this->fechaFin,
            'activo' => '1',
            'created_user' => Auth::user()->id
        ]);
        /*
        ASIGNACION DE ROLES EN LA TABLAS CREADAS POR DEFECTO ACL
         */
        $user = User::find($this->selected_id);
        $rol = Role::find($this->role_id);
        $user->assignRole($rol->name);
        /*
        FIN ASIGNACION DE ROLES EN LA TABLAS CREADAS POR DEFECTO ACL
         */
        $this->resetInput();
        $datos = [
            'tipo' => 'success',
            'mensaje' => 'La Asignación ha sido creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
        //$this->emit('alert', 'Asignación creado satisfactoriamente.');
    }

    public function asignacion($id)
    {
        //$this->areas = Area::whereActivo(1)->get();
        $this->roles = Role::whereActivo(1)->get();
        $this->asignacions = Asignacion::whereUser_id($id)->whereActivo(1)->get();
        //return $areas;
        $miUser = User::findOrFail($id);
        $this->selected_id = $id;
        $fechaIni = Carbon::now();
        $this->asignacion = true;
    }


    public function edit($id)
    {
        $miUser = User::findOrFail($id);
        $this->selected_id = $id;
        $this->name = $miUser->name;
        $this->email = $miUser->email;
        // $this->password=$miUser->password;
        $this->role_id = $miUser->role_id;
        $this->persona_id = $miUser->persona_id;
        $this->activo = $miUser->activo;
        $this->updateMode = true;

        $this->emit('asignarDatos', $this->persona_id);
    }

    public function update()
    {
        $this->validate([
            'name' => 'required|unique:users,name,' . $this->selected_id,
            'email' => 'required|unique:users,email,' . $this->selected_id,
            //'password' => 'required',
        ], $this->msjErrorUsuario);
        //dd($this->persona_id);
        if ($this->selected_id) {
            $miUser = User::findOrFail($this->selected_id);
            $miUser->update([
                'name' => $this->name,
                'email' => $this->email,
                //'password'=>Hash::make($this->password),
                'role_id' => $this->role_id,
                'persona_id' => $this->persona_id,
                'activo' => '1',
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;
            $datos = [
                'tipo' => 'success',
                'mensaje' => 'El Usuario ha sido actualizar satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos);
            //$this->emit('alert', 'Usuario actualizado satisfactoriamente.');
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = User::find($id);
            $record->deleted_user = Auth::user()->id;
            $record->save();
            $record->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'El Usuario ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }
    public function destroyAsignacion($id)
    {

        if ($id) {
            $record = Asignacion::find($id);
            $record->deleted_user = Auth::user()->id;
            $record->save();
            $record->delete();
            /*
            DESASIGNACION DE ROLES EN LA TABLAS CREADAS POR DEFECTO ACL
            */
            $existe =  Asignacion::where('role_id', $record->role_id)->count();
            if ($existe == 0) {
                $user = User::find($record->user_id);
                $rol = Role::find($record->role_id);
                $user->removeRole($rol->name);
            }

            /*
            DESASIGNACION DE ROLES EN LA TABLAS CREADAS POR DEFECTO ACL
            */
        }

        $datos = [
            'tipo' => 'error',
            'mensaje' => 'La asignación ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }
}
