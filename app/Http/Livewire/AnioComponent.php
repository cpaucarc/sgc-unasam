<?php

namespace App\Http\Livewire;

use App\Models\Anio;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class AnioComponent extends Component
{
    use WithPagination;
    use AuthorizesRequests;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $orden = 'desc';
    public $selected_id, $search, $nombre, $anio;
    public $updateMode = false;
    public $showMode = false;
    public $sort = 'anio';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        //'anio'       => 'required|unique:anios',
        'anio'       => 'required',
        'nombre'   => 'required',
    ];

    protected $msjError = [
        'anio.required' => 'El campo de nombre es obligatorio.',
        'nombre.required' => 'El campo de abreviatura es obligatorio.'
    ];

    public function render()
    {
        $this->authorize('viewAny', \App\Models\Anio::class);

        $anios = Anio::where('anio', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->orden)
            ->paginate($this->cantidad);
        return view('livewire.anios.view', compact('anios'));
    }

    public function index()
    {
        return view('livewire.anios.index');
    }


    private function resetInput()
    {
        $this->open = false;
        $this->nombre = null;
        $this->anio = null;
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
        $this->showMode = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->showMode = false;
        $this->open = false;
    }


    public function store()
    {
        $this->validate();
        Anio::create([
            'nombre' => $this->nombre,
            'anio' => $this->anio,
            'created_user' => Auth::user()->id
        ]);

        $this->authorize('create', \App\Models\Anio::class);

        $this->resetInput();

        $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Año creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);
    }

    public function edit($id)
    {
        $anio = Anio::findOrFail($id);

        $this->authorize('update', $anio);

        $this->selected_id = $id;
        $this->nombre = $anio->nombre;
        $this->anio = $anio->anio;
        $this->updateMode = true;
    }

    public function show($id)
    {
        $anio = Anio::findOrFail($id);

        $this->authorize('view', $anio);

        $this->selected_id = $id;
        $this->nombre = $anio->nombre;
        $this->anio = $anio->anio;
        $this->showMode = true;
    }

    public function update()
    {
        $this->validate([
            'nombre' => 'required|',
            //'anio' => 'required|unique:anios,anio,'.$this->selected_id
            'anio' => 'required'
        ]);

        if ($this->selected_id) {
            $record = Anio::find($this->selected_id);

            $this->authorize('update', $record);

            $record->update([
                'nombre' => $this->nombre,
                'anio' => $this->anio,
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;

            $datos1 = [
                'tipo' => 'success',
                'mensaje' => 'Año actualizada satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Anio::where('id', $id);
            $record->delete();

            $this->authorize('delete', $record);

            $datos1 = [
                'tipo' => 'error',
                'mensaje' => 'El año ha sido eliminado.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->orden == 'desc') {
                $this->orden = 'asc';
            } else {
                $this->orden = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->orden = 'asc';
        }
    }
}
