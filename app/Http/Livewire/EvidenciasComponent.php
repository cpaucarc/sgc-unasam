<?php

namespace App\Http\Livewire;

use App\Models\Anio;
use App\Models\Mecanismo;
use App\Models\Proceso;
use App\Models\Procesomecanismo;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class EvidenciasComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $orden = 'desc';
    public $selected_id, $search, $nombre, $mecanismoId, $obtenerProcesoMecanismo, $procesoMecanismo=[];
    public $updateMode = false;
    public $sort = 'anio';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        //'anio'       => 'required|unique:anios',
        'anio'       => 'required',
        'nombre'   => 'required',
    ];

    protected $msjError=[
        'anio.required' => 'El campo de nombre es obligatorio.',
        'nombre.required' => 'El campo de abreviatura es obligatorio.'
    ];

    public function render()
    {
        $anios = Anio::where('anio', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->orden)
            ->paginate($this->cantidad);

        $mecanismos = Mecanismo::whereActivo(1)->get();



        return view('livewire.evidencia.view', compact('anios', 'mecanismos'));
    }

    public function index()
    {
        return view('livewire.evidencia.index');
    }

    public function obtenerProcesoMecanismo() {

        //dd($this->mecanismoId);
        $this->procesoMecanismo = Procesomecanismo::select(DB::raw('procesomecanismos.id as id, procesomecanismos.objetivo as objetivo, mecanismo_id idMecanismo')) //select('procesomecanismos.id as id ','procesomecanismos.objetivo as objetivo')
            ->Join('procesos', 'procesomecanismos.proceso_id', '=', 'procesos.id')
            ->where('procesomecanismos.mecanismo_id', '=', $this->mecanismoId)
            ->orderBy('procesomecanismos.objetivo')->get();

        //dd($this->procesoMecanismo );
    }


}
