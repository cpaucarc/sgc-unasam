<?php

namespace App\Http\Livewire;

use App\Models\Area;
use App\Models\Detalle;
use App\Models\Indicador;
use App\Models\Macroproceso;
use App\Models\Microproceso;
use App\Models\Proceso;
use App\Models\Subproceso;
use App\Models\Suministrainfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class SuministroComponent extends Component
{use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $orden = 'asc';
    public $selected_id, $search, $paramIndicador, $paramMacroproceso, $paramProceso, $paramSubProceso, $paramMicroProceso;
    public $updateMode = false;
    public $sort = 'suministrainfos.indicador_id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'paramIndicador'       => 'required',
        'paramMacroproceso'   => 'required',
    ];
    protected $msjError=[
        'paramIndicador.required' => 'Debe seleccionar el indicador.',
        'paramMacroproceso.required' => 'Debe seleccionar el Macro Proceso.'
    ];

    public function render()
    {
        $param_macro = Macroproceso::whereActivo(1)->get();
        $param_proceso = Proceso::whereActivo(1)->get();
        $param_subproceso = Subproceso::whereActivo(1)->get();
        $param_microproceso = Microproceso::whereActivo(1)->get();
        $param_indicador = Indicador::whereActivo(1)->get();
        // suministrainfos
        $suministros = Suministrainfo::select(DB::raw('suministrainfos.indicador_id as id, suministrainfos.macroproceso_id, suministrainfos.proceso_id, suministrainfos.subproceso_id,
                suministrainfos.microproceso_id, indicadors.codigo, indicadors.nombre, suministrainfos.activo, suministrainfos.created_user, suministrainfos.updated_at,
                suministrainfos.created_at,
                (SELECT tm.NOMBRE FROM bdsgcunasam.macroprocesos tm WHERE tm.id = suministrainfos.macroproceso_id) as nombreMacro,
                (SELECT tp.nombre FROM bdsgcunasam.procesos tp WHERE tp.id = suministrainfos.proceso_id) as nombreProceso,
                (SELECT tsp.nombre FROM bdsgcunasam.subprocesos tsp WHERE tsp.id = suministrainfos.subproceso_id) as nombreSubProceso,
                (SELECT tmi.nombre FROM bdsgcunasam.microprocesos tmi WHERE tmi.id = suministrainfos.subproceso_id) as nomMicroposeso
                '))
            ->Join('indicadors', 'suministrainfos.indicador_id', '=', 'indicadors.id')
            ->where('indicadors.nombre', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->orden)
            ->paginate($this->cantidad);

        return view('livewire.suministroinfos.view', compact('suministros', 'param_macro', 'param_proceso', 'param_subproceso', 'param_microproceso', 'param_indicador'));
    }

    public function index()
    {
        return view('livewire.suministroinfos.index');
    }

    private function resetInput()
    {
        $this->open = false;
        $this->paramIndicador = null;
        $this->paramMacroproceso = null;
        $this->paramProceso = null;
        $this->paramSubProceso = null;
        $this->paramMicroProceso = null;
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
    }

    public function store()
    {
        $this->validate($this->rules, $this->msjError);
        Suministrainfo::create([
            'indicador_id' => $this->paramIndicador,
            'macroproceso_id' => $this->paramMacroproceso,
            'proceso_id' => $this->paramProceso,
            'subproceso_id' => $this->paramSubProceso,
            'microproceso_id' => $this->paramMicroProceso,
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();

        $datos = [
            'tipo' => 'success',
            'mensaje' => 'Suministro creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }

    public function edit($id)
    {
        $area = Suministrainfo::findOrFail($id);
        $this->selected_id = $id;
        $this->paramIndicador = $area->indicador_id;
        $this->paramMacroproceso = $area->macroproceso_id;
        $this->paramProceso = $area->proceso_id;
        $this->paramSubProceso = $area->subproceso_id;
        $this->paramMicroProceso = $area->microproceso_id;
        $this->updateMode = true;
    }

    public function update()
    {
        //dd($this->paramMicroProceso);
        $this->validate([
            'paramIndicador'       => 'required',
            'paramMacroproceso'   => 'required',
        ]);

        if ($this->selected_id) {
            $record = Suministrainfo::find($this->selected_id);
            $record->update([
                'indicador_id' => $this->paramIndicador,
                'macroproceso_id' => $this->paramMacroproceso,
                'proceso_id' => $this->paramProceso,
                'subproceso_id' => $this->paramSubProceso,
                'microproceso_id' => $this->paramMicroProceso,
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;

            $datos = [
                'tipo' => 'success',
                'mensaje' => 'Suministro actualizada satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Suministrainfo::where('id', $id);
            $record->delete();
            $datos = [
                'tipo' => 'error',
                'mensaje' => 'El Suministro ha sido eliminado.'
            ];
            $this->emit('alertRespuesta', $datos);
        }
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->orden == 'desc') {
                $this->orden = 'asc';
            } else {
                $this->orden = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->orden = 'asc';
        }
    }
}
