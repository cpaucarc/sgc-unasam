<?php

namespace App\Http\Livewire;

use App\Models\Area;
use App\Models\Detalle;
use App\Models\Documento;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class Documentos extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $orden = 'asc';
    public $selected_id, $search, $nombreReferencial, $descripcion, $categoria, $es_interno, $area_id, $general, $conservacion, $placeholder;
    public $tipodocumento, $nombre_area, $nombre_conservacion;
    public $updateMode = false;
    public $showMode = false;
    public $sort = 'documentos.id';
    public $direction = 'desc';
    public $esVisible = false;


    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'nombreReferencial' => 'required|unique:documentos',
        'categoria'   => 'required',
        'es_interno' => 'required',
        'general' => 'required',
        'area_id' => 'required',
        'conservacion' => 'required'
    ];
    protected $msjError = [
        'nombreReferencial.required' => 'El campo nombre referencial es obligatorio.',
        'categoria.required' => 'El campo tipo de documento es obligatorio.',
        'es_interno.required' => 'El campo interno es obligatorio.',
        'general.required' => 'El campo general es obligatorio.',
        'area_id.required' => 'El campo área es obligatorio.',
        'conservacion.required' => 'El campo conservación es obligatorio.'
    ];
    public function render()
    {
        $areas = Area::whereActivo(1)->get();
        $conservacions = Detalle::whereparametro_id(16)->whereActivo(1)->get();
        $categorias = Detalle::whereparametro_id(17)->whereActivo(1)->get();
        $documentos = Documento::where('nombreReferencial', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);

        return view('livewire.documentos.view', compact('documentos', 'areas', 'conservacions', 'categorias'));
    }
    private function resetInput()
    {
        $this->open = false;
        $this->nombreReferencial = null;
        $this->categoria = null;
        $this->descripcion = null;
        $this->es_interno = null;
        $this->area_id = null;
        $this->general = null;
        $this->conservacion = null;
        $this->resetValidation();
        $this->emit('resetearDatos');
    }
    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
        $this->showMode = false;
    }
    public function store()
    {
        $this->validate($this->rules, $this->msjError);
        Documento::create([
            'nombreReferencial' => $this->nombreReferencial,
            'descripcion' => $this->descripcion,
            'param_tipodocumento' => $this->categoria,
            'es_interno' => $this->es_interno,
            'area_id' => $this->area_id,
            'general' => $this->general,
            'param_conservacion' => $this->conservacion,
            'placeholder' => $this->placeholder,
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();

        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El documento se ha agregado satisfactoriamente.'
        ];
        $this->emit('alertDocumento', $datos);
    }
    public function edit($id)
    {
        $documento = Documento::findOrFail($id);
        $this->selected_id = $id;
        $this->nombreReferencial = $documento->nombreReferencial;
        $this->descripcion = $documento->descripcion;
        $this->categoria = $documento->param_tipodocumento;
        $this->es_interno = $documento->es_interno;
        $this->general = $documento->general;
        $this->area_id = $documento->area_id;
        $this->conservacion = $documento->param_conservacion;
        $this->updateMode = true;

        // dd($this->area_id);
        $this->emit('asignarDatos', $this->area_id, $this->categoria);
        $this->emit('actualizarSelect');
    }
    public function show($id)
    {
        $documento = Documento::findOrFail($id);
        $this->selected_id = $id;
        $this->nombreReferencial = $documento->nombreReferencial;
        $this->descripcion = $documento->descripcion;
        $this->tipodocumento = $documento->tipodocumento->detalle;
        $this->es_interno = $documento->es_interno;
        $this->general = $documento->general;
        $this->nombre_area = $documento->area->nombre;
        $this->nombre_conservacion = $documento->conservacion->detalle;
        $this->showMode = true;

        // dd($this->area_id);
        // $this->emit('asignarDatos', $this->area_id, $this->categoria);
        // $this->emit('actualizarSelect');
    }
    public function update()
    {
        $this->validate([
            'nombreReferencial' => 'required|unique:documentos,nombreReferencial,' . $this->selected_id,
            'categoria'   => 'required',
            'es_interno' => 'required',
            'area_id' => 'required',
            'general' => 'required'
        ]);

        if ($this->selected_id) {
            $documento = Documento::find($this->selected_id);
            $documento->update([
                'nombreReferencial' => $this->nombreReferencial,
                'descripcion' => $this->descripcion,
                'param_tipodocumento' => $this->categoria,
                'es_interno' => $this->es_interno,
                'area_id' => $this->area_id,
                'general' => $this->general,
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;
            $this->emit('alertDocumento', 'Documento actualizada satisfactoriamente.');
        }
    }
    public function destroy($id)
    {
        if ($id) {
            $documento = Documento::find($id);
            $documento->delete();
        }
    }

    public function viewArchivos($documento)
    {
        $this->esVisible = true;
        $this->emit('verViewArchivo', $documento);
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->orden == 'desc') {
                $this->orden = 'asc';
            } else {
                $this->orden = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->orden = 'asc';
        }
    }
    public function updatingSearch()
    {
        $this->resetPage();
    }
}
