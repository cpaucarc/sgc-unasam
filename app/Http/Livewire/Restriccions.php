<?php

namespace App\Http\Livewire;

use App\Models\Restriccion;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;

class Restriccions extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $selected_id, $search, $restriccion, $descripcion, $activo;
    public $updateMode = false;
    public $showMode = false;
    public $sort = 'id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'restriccion'       => 'required',
        //'descripcion'   => 'required',
        'activo'   => 'required',
    ];
    protected $msjError=[
        'restriccion.required' => 'El campo restricción es obligatorio.',
        //'descripcion.required' => 'El campo descripción es obligatorio.',
        'activo.required' => 'El campo activo es obligatorio.'
    ];

    public function render()
    {
        $restriccions = Restriccion::where('restriccion', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);

        return view('livewire.restriccions.view', compact('restriccions'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
        $this->showMode = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->showMode = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->restriccion = null;
        $this->descripcion = null;
        $this->activo = null;
    }

    public function store()
    {
        $this->validate($this->rules,$this->msjError);

        Restriccion::create([
            'restriccion' => $this->restriccion,
            'descripcion' => $this->descripcion,
            'activo' => $this->activo,
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();

        $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Restricción creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);

    }

    public function edit($id)
    {
        $rol = Restriccion::findOrFail($id);

        $this->selected_id = $id;
        $this->restriccion = $rol->restriccion;
        $this->descripcion = $rol->descripcion;
        $this->activo = $rol->activo;
        $this->updateMode = true;
    }

    public function show($id)
    {
        $rol = Restriccion::findOrFail($id);

        $this->selected_id = $id;
        $this->restriccion = $rol->restriccion;
        $this->descripcion = $rol->descripcion;
        $this->activo = $rol->activo;
        $this->showMode = true;
    }

    public function update()
    {
        $this->validate();

        if ($this->selected_id) {
            $restriccion = Restriccion::find($this->selected_id);
            $restriccion->update([
                'restriccion' => $this->restriccion,
                'descripcion' => $this->descripcion,
                'activo' => $this->activo,
                'updated_user' => Auth::user()->id
            ]);

            $this->resetInput();
            $this->updateMode = false;

            $datos1 = [
                'tipo' => 'success',
                'mensaje' => 'Restricción actualizada satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $restriccion = Restriccion::where('id', $id);
            $restriccion->delete();
            $datos1 = [
                'tipo' => 'error',
                'mensaje' => 'La restricción se ha sido eliminado'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }
}
