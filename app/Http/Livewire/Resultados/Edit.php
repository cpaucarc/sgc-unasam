<?php

namespace App\Http\Livewire\Resultados;

use App\Models\Anio;
use App\Models\Macroproceso;
use App\Models\Meta;
use Livewire\Component;

class Edit extends Component
{

    public $macroprocesos;
    public $macroproceso_id;

    public $indicadores;
    public $indicador_id;

    public $anios;
    public $anio_id;

    public $formula;
    public $numeradores;
    public $denominadores;

    public $tiene_metas;

    protected $queryString = [
        'macroproceso_id', 'indicador_id', 'anio_id'
    ];

    public function mount()
    {
        $this->macroprocesos = Macroproceso::select('id', 'codigo', 'nombre')->withCount('indicadors')->get();
        $this->macroprocesos = $this->macroprocesos->where('indicadors_count', '>', 0);
        $this->macroproceso_id =  $this->macroproceso_id ?? ($this->macroprocesos->first() ? $this->macroprocesos->first()->id : 0);

        if (!$this->macroprocesos->first()) :
            abort(403, 'No hay macroprocesos con indicadores');
        endif;

        $macroproceso = Macroproceso::findOrFail($this->macroproceso_id);
        $this->indicadores = $macroproceso->indicadors()->select('id', 'codigo', 'nombre')->get();
        $this->indicador_id = $this->indicador_id ?? ($this->indicadores->first() ? $this->indicadores->first()->id : 0);

        if (!$this->indicadores->first()) :
            abort(403, 'No hay resultados para este indicador');
        endif;

        $this->anios = Anio::orderBy('anio', 'asc')->get();
        $this->anio_id = $this->anio_id ?? ($this->anios->first() ? $this->anios->first()->anio : 0);

        $this->tiene_metas = Meta::where('indicador_id', $this->indicador_id)
            ->where('anio_id', $this->anio_id)->get()->isNotEmpty();
    }

    public function obtenerIndicadores()
    {
        $macroproceso = Macroproceso::findOrFail($this->macroproceso_id);
        $this->indicadores = $macroproceso->indicadors()->select('id', 'codigo', 'nombre')->get();
        $this->indicador_id = $this->indicadores->first() ? $this->indicadores->first()->id : 0;

        $this->seleccionar();
    }

    public function seleccionar (){
        return redirect("/resultados?macroproceso_id={$this->macroproceso_id}&indicador_id={$this->indicador_id}&anio_id={$this->anio_id}");
    }

    public function render()
    {
        return view('livewire.resultados.edit');
    }
}
