<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Proceso;
use App\Models\Macroproceso;
use App\Models\Detalle;
use App\Models\Area;
use App\Models\Version;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class Procesos extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $openShow = false;
    public $cantidad = 10;
    public $selected_id, $search, $codigo, $nombre, $version_id, $descripcion, $param_tipo, $responsable_area, $objetivo, $alcance, $finalidad, $orden, $nivelfinal, $macroproceso_id, $activo;
    public $updateMode = false;
    public $sort = 'id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'macroproceso_id' => 'required',
        'codigo' => 'required|unique:procesos',
        'nombre' => 'required|unique:procesos',
        'version_id' => 'required',
        //'nivelfinal'=>'required',
        //'descripcion' => 'required',
    ];
    protected $msjError = [
        'macroproceso_id.required' => 'El campo macroproceso es obligatorio.',
        'codigo.required' => 'El campo código es obligatorio.',
        'codigo.unique' => 'La combinación de codigo y versión ya está en uso.',
        'nombre.required' => 'El campo nombre es obligatorio.',
        'nombre.unique' => 'El valor del campo nombre ya está en uso.',
        'version_id.required' => 'El campo versión es obligatorio.'
    ];

    public function render()
    {
        $procesos = Proceso::where('nombre', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);
        $macroprocesos = Macroproceso::whereActivo(1)->get();
        $tipoMacroprocesos = Detalle::whereparametro_id(5)->whereActivo(1)->get();
        $areas = Area::whereActivo(1)->get();
        $versiones = Version::whereActivo(1)->where('nivel_proceso', 15002)->get();

        $search = '%' . $this->search . '%';
        return view('livewire.procesos.view', compact('procesos', 'macroprocesos', 'tipoMacroprocesos', 'areas', 'versiones'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
        $this->openShow = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->openShow = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->codigo = null;
        $this->nombre = null;
        $this->version_id = null;
        $this->descripcion = null;
        $this->param_tipo = null;
        $this->responsable_area = null;
        $this->objetivo = null;
        $this->nivelfinal = null;
        $this->alcance = null;
        $this->finalidad = null;
        $this->orden = null;
        $this->macroproceso_id = null;
        $this->activo = null;
    }

    public function store()
    {
        $this->validate($this->rules, $this->msjError);

        Proceso::create([
            'codigo' => $this->codigo,
            'nombre' => $this->nombre,
            'version_id' => $this->version_id,
            'descripcion' => $this->descripcion,
            'param_tipo' => $this->param_tipo,
            'responsable_area' => $this->responsable_area,
            'objetivo' => $this->objetivo,
            'alcance' => $this->alcance,
            'finalidad' => $this->finalidad,
            'orden' => $this->orden,
            'nivelfinal' => $this->nivelfinal,
            'macroproceso_id' => $this->macroproceso_id,
            'activo' => '1',
            'created_user' => Auth::user()->id,
        ]);

        $this->resetInput();
        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El Proceso ha sido creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }

    public function edit($id)
    {
        $miProceso = Proceso::findOrFail($id);

        $this->selected_id = $id;
        $this->codigo = $miProceso->codigo;
        $this->nombre = $miProceso->nombre;
        $this->version_id = $miProceso->version_id;
        $this->descripcion = $miProceso->descripcion;
        $this->param_tipo = $miProceso->param_tipo;
        $this->responsable_area = $miProceso->responsable_area;
        $this->objetivo = $miProceso->objetivo;
        $this->alcance = $miProceso->alcance;
        $this->finalidad = $miProceso->finalidad;
        $this->orden = $miProceso->orden;
        $this->nivelfinal = $miProceso->nivelfinal;
        $this->macroproceso_id = $miProceso->macroproceso_id;
        $this->activo = $miProceso->activo;
        $this->updateMode = true;
    }


    public function show($id)
    {
        $miProceso = Proceso::findOrFail($id);

        $this->selected_id = $id;
        $this->codigo = $miProceso->codigo;
        $this->nombre = $miProceso->nombre;
        $this->version_id = $miProceso->version_id;
        $this->descripcion = $miProceso->descripcion;
        $this->param_tipo = $miProceso->param_tipo;
        $this->responsable_area = $miProceso->responsable_area;
        $this->objetivo = $miProceso->objetivo;
        $this->alcance = $miProceso->alcance;
        $this->finalidad = $miProceso->finalidad;
        $this->orden = $miProceso->orden;
        $this->nivelfinal = $miProceso->nivelfinal;
        $this->macroproceso_id = $miProceso->macroproceso_id;
        $this->activo = $miProceso->activo;
        $this->openShow = true;
    }

    public function update()
    {
        $this->validate([
            'macroproceso_id' => 'required',
            'codigo' =>  [
                'required',
                Rule::unique('procesos')->ignore($this->selected_id)->where(fn ($query) => $query->where('codigo', $this->codigo)->where('version_id', $this->version_id))
            ],
            'nombre' => 'required|unique:procesos,nombre,' . $this->selected_id,
            'version_id' => 'required',
        ], $this->msjError);
        if ($this->selected_id) {
            $miProceso = Proceso::findOrFail($this->selected_id);
            $miProceso->update([
                'codigo' => $this->codigo,
                'nombre' => $this->nombre,
                'version_id' => $this->version_id,
                'descripcion' => $this->descripcion,
                'param_tipo' => $this->param_tipo,
                'responsable_area' => $this->responsable_area,
                'objetivo' => $this->objetivo,
                'alcance' => $this->alcance,
                'finalidad' => $this->finalidad,
                'orden' => $this->orden,
                'nivelfinal' => $this->nivelfinal,
                'macroproceso_id' => $this->macroproceso_id,
                'activo' => '1',
                'updated_user' => Auth::user()->id,
            ]);
            $this->resetInput();
            $this->updateMode = false;
            $datos = [
                'tipo' => 'success',
                'mensaje' => 'El Proceso ha sido actualizado satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Proceso::find($id);
            $record->deleted_user = Auth::user()->id;
            $record->save();
            $record->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'El Proceso ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }

    public function setProcesoSelect()
    {
        $this->param_tipo = null;
        if ($this->macroproceso_id != null) {
            $miMacroproceso = Macroproceso::findOrFail($this->macroproceso_id);
            $this->param_tipo = $miMacroproceso->param_tipo;
        }
    }
}
