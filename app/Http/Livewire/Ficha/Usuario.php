<?php

namespace App\Http\Livewire\Ficha;

use App\Models\Indicador;
use App\Models\Interesado;
use App\Models\InteresadoIndicador;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use function view;

class Usuario extends Component
{
    public $nivel_proceso, $indicador_id;
    public $paramIndicador;
    public $openUsuario = false;
    public $updateMode = false;

    public $datos = [];
    public $selectUsuarioInteresado;

    protected $paginationTheme = 'bootstrap';
    public $selected_id, $search, $paramInteresado, $paramProceso, $indicadors_id, $paramMicroProceso;

    protected $listeners = ['render', 'destroy', 'store'];
    protected $rules = [
        'paramIndicador'       => 'required',
        'paramInteresado'   => 'required',
    ];
    protected $msjError = [
        'paramIndicador.required' => 'Debe seleccionar el indicador.',
        'paramInteresado.required' => 'Debe seleccionar un área.'
    ];

    public function render()
    {
        /*switch ($this->nivel_proceso) {
            case 15001:
                $entradas = Suministrainfo::find($this->indicador_id)->entradas;
                break;
        }*/
        $this->datos = [];

        $param_indicador = Indicador::whereActivo(1)->get();


        //$responsable_usuarios = InteresadoIndicador::select(DB::raw('areas.id, areas.nombre'))
        $responsable_usuarios = InteresadoIndicador::select(DB::raw('indicador_interesado.indicador_id, indicador_interesado.interesado_id, areas.nombre, areas.abreviatura, areas.id idArea'))
            ->Join('interesados', 'indicador_interesado.interesado_id', '=', 'interesados.id')
            ->Join('areas', 'interesados.area_id', '=', 'areas.id')
            ->where('indicador_interesado.activo', '=', 1)
            ->where('indicador_interesado.indicador_id', '=', $this->indicador_id)
            ->where('param_tiporesponsable', '=', '11005')
            ->orderBy('areas.nombre')->get();

        $indicadorUsuario = InteresadoIndicador::whereActivo(1)->whereParam_tiporesponsable('11005')->get();
        foreach ($indicadorUsuario as $areasSum) {
            $this->datos[] = $areasSum->interesado_id;
        }

        //dd($responsable_usuarios);
        //dd($this->datos);

        $interesados = Interesado::select(DB::raw('interesados.id idInteresado, areas.nombre, areas.abreviatura, areas.id idArea'))
            ->Join('areas', 'interesados.area_id', '=', 'areas.id')
            ->whereNotIn('interesados.id', $this->datos)
            //->rightJoin('areas', 'interesados.area_id', '=', 'areas.id')
            ->orderBy('areas.nombre')->get();

        $this->paramIndicador = $this->indicador_id;
        //$this->updateMode = true;


        return view('livewire.ficha-indicador.usuarios.view', compact('responsable_usuarios', 'param_indicador', 'interesados'));
    }

    public function openModalUsuarioRes()
    {
        $this->openUsuario = true;
        $this->emit('obtenerResponsableCA');
    }

    private function resetInput()
    {
        $this->openUsuario = false;
        $this->paramInteresado = null;
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->openUsuario = false;
    }

    public function store()
    {
        for ($i = 0; $i < sizeof($this->selectUsuarioInteresado); $i++) {
            InteresadoIndicador::create([
                'indicador_id' => $this->indicador_id,
                'interesado_id' => $this->selectUsuarioInteresado[$i]['id'],
                'param_tiporesponsable' => "11005",
                'created_user' => Auth::user()->id
            ]);
        }


        /*$this->validate($this->rules, $this->msjError);
        InteresadoIndicador::create([
            'indicador_id' => $this->paramIndicador,
            'interesado_id' => $this->paramInteresado,
            'param_tiporesponsable' => "11005",
            'created_user' => Auth::user()->id
        ]);*/

        $this->resetInput();
        $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Registro creada satisfactoriamente.'
        ];
        $this->emit('alertUsuarioInteresado', $datos1);
    }


    public function destroy($id)
    {
        if ($id) {
            $asumi = DB::table('indicador_interesado')
                ->where('indicador_interesado.interesado_id', '=', $id)
                ->where('indicador_interesado.indicador_id', '=', $this->indicador_id);
            $asumi->delete();
        }
        $datos1 = [
            'tipo' => 'error',
            'mensaje' => 'Registro ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertUsuarioElimina', $datos1);
    }
}
