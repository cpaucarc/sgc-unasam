<?php

namespace App\Http\Livewire\Ficha;

use App\Models\Indicador;
use App\Models\Interesado;
use App\Models\InteresadoIndicador;
use App\Models\Proceso;
use App\Models\Suministrainfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use function view;

class SuministroProceso extends Component
{
    public $nivel_proceso, $indicador_id;
    public $paramIndicador;
    public $openProceso = false;
    public $updateMode = false;

    public $datos = [];

    protected $paginationTheme = 'bootstrap';
    public $selected_id, $macroproceso_id, $paramInteresado, $paramProceso, $indicadors_id;
    public $selectSuministroProceso = [];

    protected $listeners = ['render', 'destroy', 'store'];
    protected $rules = [
        'paramIndicador'       => 'required',
        //'paramProceso'   => 'required',
        'selectSuministroProceso'   => 'required',
    ];
    protected $msjError=[
        'paramIndicador.required' => 'Debe seleccionar el indicador.',
        'paramProceso.required' => 'Debe seleccionar un proceso.'
    ];

    public function render()
    {
        /*switch ($this->nivel_proceso) {
            case 15001:
                $entradas = Suministrainfo::find($this->indicador_id)->entradas;
                break;
        }*/
        $this->datos=[];

        $param_indicador = Indicador::whereActivo(1)->get();

        $suministro_proceso = Suministrainfo::select(DB::raw('suministrainfos.id, procesos.nombre, procesos.codigo, procesos.id as idProceso'))
            ->Join('procesos', 'suministrainfos.proceso_id', '=', 'procesos.id')
            ->where('suministrainfos.indicador_id','=', $this->indicador_id)
            ->orderBy('procesos.nombre')->get();

        foreach ($suministro_proceso as $procesoSum) {
            $this->datos[] = $procesoSum->idProceso;
        }

        $procesos = Proceso::whereActivo(1)
            ->whereNotIn('id', $this->datos)
            ->orderBy('nombre')->get();
        $this->paramIndicador = $this->indicador_id;
        return view('livewire.ficha-indicador.suministra-proceso.view', compact('suministro_proceso', 'param_indicador', 'procesos'));
    }

    public function openModalSuministroProceso()
    {
        $this->openProceso = true;
        $this->emit('obtenerSuministroProceso');
    }

    private function resetInput()
    {
        $this->openProceso = false;
        $this->paramInteresado = null;
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->openProceso = false;
    }

    public function store()
    {
        // 11004 -> resobonsable de munstrar informacion
        //dd($this->selectSuministroProceso);

        for ($i = 0; $i < sizeof($this->selectSuministroProceso); $i++){
            $this->validate($this->rules, $this->msjError);
            Suministrainfo::create([
                'indicador_id' => $this->paramIndicador,
                'proceso_id' => $this->selectSuministroProceso[$i]['id'],//$this->paramProceso,
                'macroproceso_id' => $this->macroproceso_id,
                'created_user' => Auth::user()->id
            ]);
        }

        $this->resetInput();
        $datosMensaje = [
            'tipo' => 'success',
            'mensaje' => 'Proceso que suministra información creada satisfactoriamente.'
        ];
        $this->emit('alertSuministroProceso', $datosMensaje);
    }

    public function destroy($id)
    {
        //dd($id);
        if ($id) {
            //$deleted_user = Auth::user()->id;
            //$vinculo = InteresadoIndicador::where('indicador_interesado.interesados_id', '=', $id)->where('indicador_interesado.indicadors_id', '=', $this->indicador_id);
            //dd($vinculo);
            $asumi = Suministrainfo::whereId($id);
            //$asumi->deleted_user = $deleted_user;
            //$asumi->save();
            $asumi->delete();
        }
        $datos1 = [
            'tipo' => 'error',
            'mensaje' => 'Proceso que suministra información eliminado satisfactoriamente.'
        ];
        $this->emit('alertSuministroProceso', $datos1);
    }
}
