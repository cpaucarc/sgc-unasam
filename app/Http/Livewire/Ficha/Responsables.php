<?php

namespace App\Http\Livewire\Ficha;

use App\Models\Indicador;
use App\Models\Interesado;
use App\Models\InteresadoIndicador;
use App\Models\Proceso;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use function view;

class Responsables extends Component
{
    public $nivel_proceso, $indicador_id;
    public $paramIndicador;
    public $openResponsable = false;
    public $updateMode = false;

    public $regExiste = [];
    public $datos = [];
    public $datos2 = [];

    protected $paginationTheme = 'bootstrap';
    public $selected_id, $search, $paramInteresado, $paramProceso, $indicadors_id;
    public $tituloModal;
    public $selectParamResponsableCA;
    public $tipoRegistro;

    protected $listeners = ['render', 'destroy', 'store'];
    protected $rules = [
        'paramIndicador'       => 'required',
        //'paramProceso'   => 'required',
    ];
    protected $msjError = [
        'paramIndicador.required' => 'Debe seleccionar el indicador.',
        'paramProceso.required' => 'Debe seleccionar un proceso.'
    ];

    public function render()
    {
        //dd($this->indicador_id);
        $this->datos = [];
        $this->datos2 = [];
        $param_indicador = Indicador::whereActivo(1)->get();

        $responsables = InteresadoIndicador::select('areas.id as id', 'areas.nombre as nombre', 'indicador_interesado.interesado_id as idIntresado')
            ->join('interesados', 'indicador_interesado.interesado_id', '=', 'interesados.id')
            ->join('areas', 'interesados.area_id', '=', 'areas.id')
            ->where('indicador_interesado.activo', '=', 1)
            ->where('indicador_interesado.indicador_id', '=', $this->indicador_id)
            ->where('param_tiporesponsable', '=', '11002')
            ->orderBy('areas.nombre')->get();

        $responsables_analisis = InteresadoIndicador::select('areas.id as id ', 'areas.nombre as nombre', 'indicador_interesado.interesado_id as idIntresado')
            ->Join('interesados', 'indicador_interesado.interesado_id', '=', 'interesados.id')
            ->Join('areas', 'interesados.area_id', '=', 'areas.id')
            ->where('indicador_interesado.activo', '=', 1)
            ->where('indicador_interesado.indicador_id', '=', $this->indicador_id)
            ->where('param_tiporesponsable', '=', '11003')
            ->orderBy('areas.nombre')->get();

        // parametros para registrar
        $interesadoReg = InteresadoIndicador::where('indicador_interesado.activo', '=', 1)
            ->where('indicador_interesado.indicador_id', '=', $this->indicador_id)
            ->whereIn('param_tiporesponsable', ['11003', '11002'])->get();

        foreach ($interesadoReg as $procesoSum) {
            switch ($procesoSum->param_tiporesponsable) {
                case '11002':
                    $this->datos[] = $procesoSum->interesado_id;
                    break;
                case '11003':
                    $this->datos2[] = $procesoSum->interesado_id;
                    break;
            }
        }

        $paramResponsables = Interesado::whereActivo(1)
            ->whereNotIn('id', $this->datos)
            ->orderBy('nombre')->get();

        $paramResponsablesATD = Interesado::whereActivo(1)
            ->whereNotIn('id', $this->datos2)
            ->orderBy('nombre')->get();

        $this->paramIndicador = $this->indicador_id;

        return view('livewire.ficha-indicador.responsables.view', compact('param_indicador', 'responsables', 'responsables_analisis', 'paramResponsables', 'paramResponsablesATD'));
    }

    public function openModalResponsable($accion)
    {
        $this->tipoRegistro = $accion;
        if ($accion == 1) {
            $this->tituloModal = "Agregar Responsable del Cálculo";
        } else {
            $this->tituloModal = "Agregar Responsable del Análisis y Toma de Deciciones";
        }
        $this->openResponsable = true;
        $this->emit('obtenerResponsableCA');
    }

    private function resetInput()
    {
        $this->openResponsable = false;
        $this->paramInteresado = null;
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->openResponsable = false;
    }


    public function store()
    {
        //dd($this->indicador_id);
        $this->validate($this->rules, $this->msjError);
        if ($this->tipoRegistro == 1) {
            for ($i = 0; $i < sizeof($this->selectParamResponsableCA); $i++) {
                InteresadoIndicador::create([
                    'indicador_id' => $this->indicador_id,
                    'interesado_id' => $this->selectParamResponsableCA[$i]['id'],
                    'param_tiporesponsable' => "11002",
                    'created_user' => Auth::user()->id
                ]);
            }
        } elseif ($this->tipoRegistro == 2) {
            for ($i = 0; $i < sizeof($this->selectParamResponsableCA); $i++) {
                InteresadoIndicador::create([
                    'indicador_id' => $this->paramIndicador,
                    'interesado_id' => $this->selectParamResponsableCA[$i]['id'],
                    'param_tiporesponsable' => "11003",
                    'created_user' => Auth::user()->id
                ]);
            }
        }

        $this->resetInput();
        $datosMensaje = [
            'tipo' => 'success',
            'mensaje' => 'Responsable creada satisfactoriamente.'
        ];
        $this->emit('alertResponsable', $datosMensaje);
    }

    public function destroy($id, $tiporesonsable)
    {
        //dd($id);
        if ($id) {
            $responsable = InteresadoIndicador::where('indicador_interesado.interesado_id', '=', $id)
                ->where('indicador_interesado.indicador_id', '=',  $this->indicador_id)
                ->where('indicador_interesado.param_tiporesponsable', '=',  $tiporesonsable);
            $responsable->delete();
        }
        $datos1 = [
            'tipo' => 'error',
            'mensaje' => 'El Responsable ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertResponsable', $datos1);
    }
}
