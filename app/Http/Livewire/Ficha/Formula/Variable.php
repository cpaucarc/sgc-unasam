<?php

namespace App\Http\Livewire\Ficha\Formula;


use Livewire\Component;

class Variable extends Component
{
    public $variable;
    public $editando = false;
    public $index;
    public $original;
    public $tiene_api;

    public $rules = [
        'variable.variable' => 'required',
        'variable.nombre' => 'required',
        'variable.tipo' => 'required',
        'variable.api_url' => 'nullable|url',
        'variable.id' => ''
    ];

    public function mount()
    {
        $this->original = collect([
            'variable' => $this->variable->variable,
            'nombre' => $this->variable->nombre,
            'api_url' => $this->variable->api_url
        ]);
    }

    public function render()
    {
        return view('livewire.ficha-indicador.formula.variable');
    }

    public function edit()
    {
        $this->editando = true;
    }
    public function cancel()
    {
        //  dd($this->original);
        $this->variable->variable = $this->original['variable'];
        $this->variable->nombre = $this->original['nombre'];
        $this->variable->api_url = $this->original['api_url'];
        $this->editando = false;
    }
    public function update()
    {
        $this->original = collect([
            'variable' => $this->variable->variable,
            'nombre' => $this->variable->nombre,
            'api_url' => $this->variable->api_url
        ]);
        $this->emitTo('ficha.formula', 'modificar', $this->index, 'variables_' . $this->variable->tipo, $this->variable);
        $this->editando = false;
    }
    public function destroy()
    {
        // dd($this);
        if (!$this->variable->id) {
            $this->emitTo('ficha.formula', 'quitar', $this->index, $this->variable->arreglo);
        }
        if ($this->variable->id) {
            $this->emitTo('ficha.formula', 'quitar', $this->index, 'variables_' . $this->variable->tipo);
        }

        /* */
    }
}
