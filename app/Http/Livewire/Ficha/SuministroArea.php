<?php

namespace App\Http\Livewire\Ficha;

use App\Models\Indicador;
use App\Models\Interesado;
use App\Models\InteresadoIndicador;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class SuministroArea extends Component
{
    public $nivel_proceso, $indicador_id;
    public $paramIndicador;
    public $open = false;
    public $updateMode = false;
    public $datos = [];
    public $datosProbando;

    protected $paginationTheme = 'bootstrap';
    public $selected_id, $search, $paramInteresado, $paramProceso, $indicadors_id;
    public $selectAreaInteresado = [];

    protected $listeners = ['render', 'destroy', 'store'];
    protected $rules = [
        'paramIndicador'       => 'required',
        //'paramInteresado'   => 'required',
        'selectAreaInteresado'   => 'required',
    ];
    protected $msjError=[
        'paramIndicador.required' => 'Debe seleccionar el indicador.',
        'paramInteresado.required' => 'Debe seleccionar un área.'
    ];

    public function render()
    {
        /*switch ($this->nivel_proceso) {
            case 15001:
                $entradas = Suministrainfo::find($this->indicador_id)->entradas;
                break;
        }*/

        $this->datos=[];
        $this->datosProbando=[];

        $param_indicador = Indicador::whereActivo(1)->get();


        $suministro_area = InteresadoIndicador::select(DB::raw('indicador_interesado.indicador_id, indicador_interesado.interesado_id, areas.nombre, areas.abreviatura, areas.id idArea'))
            ->Join('interesados', 'indicador_interesado.interesado_id', '=', 'interesados.id')
            ->Join('areas', 'interesados.area_id', '=', 'areas.id')
            ->where('indicador_interesado.indicador_id','=', $this->indicador_id)
            ->where('indicador_interesado.param_tiporesponsable','=', 11004)
            ->orderBy('areas.nombre')->get();

        foreach ($suministro_area as $areasSum) {
            $this->datos[] = $areasSum->idArea;
        }

        $this->datosProbando = Interesado::join('areas', 'interesados.area_id', '=', 'areas.id')
            ->whereNotIn('interesados.area_id', $this->datos)
            ->select('interesados.id as idInteresado','areas.nombre as nombre', 'areas.id as idArea')
            ->orderBy('areas.nombre')->get();

        $this->paramIndicador = $this->indicador_id;
        return view('livewire.ficha-indicador.suministro-area.view', compact('suministro_area', 'param_indicador'));
    }

    public function openSuministroArea()
    {
        $this->open = true;
        $this->emit('obtenerAreasSUministro');
        $this->paramInteresado = NULL;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->paramInteresado = NULL;
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
    }

    public function store()
    {
        $this->validate($this->rules, $this->msjError);

        for ($i = 0; $i < sizeof($this->selectAreaInteresado); $i++) {
            //dd($this->selectAreaInteresado[$i]['id']);
            InteresadoIndicador::create([
                'indicador_id' => $this->paramIndicador,
                'interesado_id' => $this->selectAreaInteresado[$i]['id'],
                'param_tiporesponsable' => "11004",
                'created_user' => Auth::user()->id
            ]);
        }

        $this->resetInput();
        $datosMensaje = [
            'tipo' => 'success',
            'mensaje' => 'Area que suministra creada satisfactoriamente.'
        ];
        $this->emit('alertSuministroArea', $datosMensaje);

    }

    public function destroy($id)
    {
        //dd($id);
        if ($id) {
            //$deleted_user = Auth::user()->id;
            //$vinculo = InteresadoIndicador::where('indicador_interesado.interesados_id', '=', $id)->where('indicador_interesado.indicadors_id', '=', $this->indicador_id);
            //dd($vinculo);
            $asumi =DB::table('indicador_interesado')
                ->where('indicador_interesado.interesado_id', '=', $id)
                ->where('indicador_interesado.indicador_id', '=', $this->indicador_id);//->delete();
            //$asumi->deleted_user = $deleted_user;
            //$asumi->save();
            $asumi->delete();
        }
        $datos1 = [
            'tipo' => 'error',
            'mensaje' => 'Area que suministra ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertSuministroArea', $datos1);
    }

}
