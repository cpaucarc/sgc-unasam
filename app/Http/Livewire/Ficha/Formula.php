<?php

namespace App\Http\Livewire\Ficha;

use App\Models\Formula as ModelsFormula;
use Livewire\Component;
use function view;
use App\Models\Medicion;
use App\Models\Indicador;
use App\Models\Variable;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;

use MathParser\StdMathParser;
use MathParser\Interpreting\Evaluator;

class Formula extends Component
{
    //VARIABLES GENERALES
    public $indicador_id, $anio_id;
    public $openCrear = false;
    public $numerador, $denominador;
    public $formula;
    public $variable_numerador;
    public $variables_numerador;
    public $variable_denominador;
    public $variables_denominador;
    public $numeradores;
    public $denominadores;
    public $tiene_api;

    protected $listeners = ['inicializaVariables', 'quitar', 'modificar'];

    protected $rules = [
        'numerador' => 'required',
        'formula.numerador' => 'required',
        'formula.denominador' => 'required',
        'denominador' => 'required',
        'anio_id' => 'required',
        'indicador_id' => 'required',
        'variable_numerador.variable' => 'required',
        'variable_numerador.nombre' => 'required',
        'variable_numerador.api_url' => 'required',
        'variable_denominador.variable' => 'required',
        'variable_denominador.nombre' => 'required',
        'variable_denominador.api_url' => 'required'
    ];

    protected $msjError = [
        'numerador.required' => 'El campo numerador es obligatorio.',
        'denominador.required' => 'El campo denominador es obligatorio.',
        'anio_id.required' => 'El campo año  es obligatorio.',
        'indicador_id.required' => 'El campo indicador es obligatorio.'
    ];

    public function mount()
    {
        $this->variables_numerador = collect();
        $this->variable_numerador = new Variable();
        $this->numeradores = collect();
        $this->variable_denominador = new Variable();
        $this->variables_denominador = collect();
        $this->denominadores = collect();

        $this->inicializaVariables('numerador');
        $this->inicializaVariables('denominador');

        $formula = ModelsFormula::where('indicador_id', $this->indicador_id)->first();
        $indicador = Indicador::findOrFail($this->indicador_id);
        $this->tiene_api = $indicador->macroproceso->tiene_api;

        $this->formula = new ModelsFormula();
        if ($formula) {
            $this->formula = $formula;
            $this->variables_numerador = collect($this->formula->variables()->whereTipo('numerador')->get());
            $this->variables_denominador = collect($this->formula->variables()->whereTipo('denominador')->get());
        }
    }
    public function render()
    {
        $medicion = Medicion::where('indicador_id', $this->indicador_id)
            //->where('anio_id',$this->anio_id)
            ->first();

        if ($medicion && !$this->openCrear) {
            $this->numerador = $medicion->numerador;
            $this->denominador = $medicion->denominador;
        }

        $indicador = Indicador::where('id', $this->indicador_id)->first();

        return view('livewire.ficha-indicador.formula.view', compact('medicion', 'indicador'));
    }

    public  function store()
    {
        $this->validate([
            'formula.numerador' => 'required',
            'formula.denominador' => 'required',
            'anio_id' => 'required',
            'indicador_id' => 'required',
        ], $this->msjError);
        $temporal = Medicion::where('indicador_id', $this->indicador_id)
            //->where('anio_id',$this->anio_id)
            ->first();


        $valores_numerador = collect();
        foreach ($this->variables_numerador as $vn) {
            $valores_numerador->put($vn['variable'], 1);
        }

        $valores_denominador = collect();
        foreach ($this->variables_denominador as $vd) {
            $valores_denominador->put($vd['variable'], 1);
        }


        try {
            $formula_numerador = $this->formula->numerador;
            $valor_numerador = new StdMathParser();
            $evaluador_numerador = new Evaluator();
            $evaluador_numerador->setVariables($valores_numerador->toArray());
            $valor_numerador->parse($formula_numerador)->accept($evaluador_numerador);
            $formula_denominador = $this->formula->denominador;
            $valor_denominador = new StdMathParser();
            $evaluador_denominador = new Evaluator();
            $evaluador_denominador->setVariables($valores_denominador->toArray());
            $valor_denominador->parse($formula_denominador)->accept($evaluador_denominador);
        } catch (Exception $e) {
            $this->emit('alertaFormula', [
                'tipo' => 'error',
                'mensaje' => 'La fórmula ingresada es incorrecta. ' . $e->getMessage()
            ]);

            return;
        }


        $this->formula->indicador_id = $this->indicador_id;
        $this->formula->save();

        $this->formula->variables()->delete();

        foreach ($this->variables_numerador as $vn) {
            $this->formula->variables()->create($vn);
        }

        foreach ($this->variables_denominador as $vd) {
            $this->formula->variables()->create($vd);
        }

        $this->openCrear = false;
        $this->resetInputEntradas();


        $this->emit('alertaFormula', [
            'tipo' => 'success',
            'mensaje' => 'La fórmula ha sido actualizado satisfactoriamente.'
        ]);
        $this->emitTo('ficha.resultado-evaluacion', 'iniciaValores');

    }

    public function modalEntrada()
    {
        $this->openCrear = true;
    }
    public function cancel()
    {
        $this->openCrear = false;
        $this->resetInputEntradas();
    }

    public function resetInputEntradas()
    {
        $this->numerador = NULL;
        $this->denominador = NULL;
    }

    public function agregaVariable($tipo)
    {
        $tipo_variable = 'variable_' . $tipo;
        $arreglo = 'variables_' . $tipo;
        $this->validate([
            $tipo_variable . '.variable' => 'required|size:1',
            $tipo_variable . '.nombre' => 'required',
            $tipo_variable . '.api_url' => 'nullable|url'
        ]);


        $this->$tipo_variable->formula_id = $this->formula->id;
        $this->$tipo_variable->tipo = $tipo;
        $this->$tipo_variable->arreglo = $arreglo;
        $this->$tipo_variable->editando = false;

        $this->$arreglo->push(
            $this->$tipo_variable
        );

        // $this->$tipo_variable->save();

        $this->$tipo_variable = new Variable();
        $this->inicializaVariables($tipo);
    }

    public function inicializaVariables($tipo)
    {
        $grupo = $tipo . 'es';
        $this->$grupo = Variable::whereHas('formula', function ($q) {
            return $q->where('indicador_id', $this->indicador_id);
        })->whereTipo($tipo)->get();
    }

    public function quitar($key, $variables)
    {
        $this->$variables->pull($key);
    }

    public function modificar($key, $variables, $variable)
    {

        $variable = new Variable($variable);
        $this->$variables[$key] = $variable;
    }

    public function getFormulaNumeradorProperty()
    {
        $formula_numerador = $this->formula->numerador;
        foreach ($this->variables_numerador as $vn) {
            $formula_numerador = str_replace($vn['variable'], $vn['variable'] . '#&', $formula_numerador);
        }
        foreach ($this->variables_numerador as $vn) {
            $formula_numerador = str_replace($vn['variable'] . '#&', '(' . $vn['nombre'] . ')', $formula_numerador);
        }
        $formula_numerador = str_replace(' ', '\ ', $formula_numerador);

        return $formula_numerador;
    }
    public function getFormulaDenominadorProperty()
    {
        $formula_denominador = $this->formula->denominador;

        foreach ($this->variables_denominador as $vd) {
            $formula_denominador = str_replace($vd['variable'], $vd['variable'] . '#&', $formula_denominador);
        }
        foreach ($this->variables_denominador as $vd) {
            $formula_denominador = str_replace($vd['variable'] . '#&', '(' . $vd['nombre'] . ')', $formula_denominador);
        }
        $formula_denominador = str_replace(' ', '\ ', $formula_denominador);

        return $formula_denominador;
    }
}
