<?php

namespace App\Http\Livewire\Ficha;

use App\Models\Indicador;
use App\Models\IndicadorRestriccion;
use App\Models\InteresadoIndicador;
use App\Models\Restriccion;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Restricciones extends Component
{
    public $nivel_proceso, $indicador_id;
    public $paramIndicador;
    public $openRestriccion = false;
    public $updateMode = false;

    public $datos = [];

    protected $paginationTheme = 'bootstrap';
    public $cantidad = 10;
    public $orden = 'asc';
    public $selected_id, $search, $paramRestriccion, $paramProceso, $indicadors_id, $nombre;
    public $selectRestreicciones;

    protected $listeners = ['render', 'destroy', 'store'];
    protected $rules = [
        'paramIndicador'       => 'required',
        //'paramRestriccion'   => 'required',
        //'nombre'   => 'required',
    ];
    protected $msjError=[
        'paramIndicador.required' => 'Debe seleccionar el indicador.',
        'paramRestriccion.required' => 'Debe seleccionar una restricción.',
        'nombre.required' => 'Debe ingresar un nombre.'
    ];

    public function render()
    {
        /*switch ($this->nivel_proceso) {
            case 15001:
                $entradas = Suministrainfo::find($this->indicador_id)->entradas;
                break;
        }*/
        $this->datos=[];
        $param_indicador = Indicador::whereActivo(1)->get();

        $restricciones = IndicadorRestriccion::select('indicador_restriccion.id as id', 'indicador_restriccion.nombre as nombre', 'indicador_restriccion.indicador_id as indicador_id',
                    'indicador_restriccion.restriccion_id as restriccion_id', 'restriccions.restriccion as rNombre')
            ->join('restriccions', 'indicador_restriccion.restriccion_id', '=', 'restriccions.id')
            ->where('indicador_restriccion.indicador_id','=', $this->indicador_id)
            ->get();

        foreach ($restricciones as $restriccion) {
            $this->datos[] = $restriccion->restriccion_id;
        }

        $restriccions = Restriccion::whereActivo(1)
            ->whereNotIn('id', $this->datos)
            ->orderBy('restriccion')->get();

        $this->paramIndicador = $this->indicador_id;

        return view('livewire.ficha-indicador.restricciones.view', compact('restricciones', 'restriccions', 'param_indicador'));
    }

    public function openModal()
    {
        $this->openRestriccion = true;
        $this->emit('obtenerRestricciones');
    }

    private function resetInput()
    {
        $this->openRestriccion = false;
        $this->paramInteresado = null;
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->openRestriccion = false;
    }


    public function store()
    {
       // dd( $this->selectRestreicciones);
        $this->validate($this->rules, $this->msjError);

        for ($i = 0; $i < sizeof($this->selectRestreicciones); $i++) {
            IndicadorRestriccion::create([
                'indicador_id' => $this->indicador_id,
                'restriccion_id' => $this->selectRestreicciones[$i]['id'],
                'nombre' => $this->nombre,
                'created_user' => Auth::user()->id
            ]);
        }

        //dd($indicadorr);

        /*IndicadorRestriccion::create([
            'indicador_id' => $this->paramIndicador,
            //'restriccion_id' => $this->paramRestriccion,
            'restriccion_id' => $this->selectRestreicciones,
            //'nombre' => $this->nombre,
            'created_user' => Auth::user()->id
        ]);*/

        $this->resetInput();
        //$this->emit('alert', 'Registro creada satisfactoriamente.');

        $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Registro creada satisfactoriamente.'
        ];
        $this->emit('alertRestriccionFicha', $datos1);
    }


    public function destroy($id)
    {
        if ($id) {
            //$deleted_user = Auth::user()->id;
            //$vinculo = InteresadoIndicador::where('indicador_interesado.interesados_id', '=', $id)->where('indicador_interesado.indicadors_id', '=', $this->indicador_id);
            //dd($vinculo);
            $asumi =IndicadorRestriccion::whereId($id);
            //$asumi->deleted_user = $deleted_user;
            //$asumi->save();
            $asumi->delete();
        }
        $datos1 = [
            'tipo' => 'error',
            'mensaje' => 'Registro ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertRestriccionFicha', $datos1);
    }
}
