<?php

namespace App\Http\Livewire\Ficha;

use App\Models\Indicador;
use App\Models\IndicadorRestriccion;
use App\Models\Meta;
use App\Models\Medicion;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Mediciones extends Component
{
    public $nivel_proceso, $indicador_id;
    public $paramIndicador;
    public $openRestriccion = false;
    public $updateMode = false;

    public $datos = [];

    protected $paginationTheme = 'bootstrap';
    public $cantidad = 10;
    public $orden = 'asc';
    public $selected_id, $search, $paramRestriccion, $paramProceso, $indicadors_id, $nombre;
    public $sort = 'suministrainfos.indicador_id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        'paramIndicador'       => 'required',
        'paramRestriccion'   => 'required',
        'nombre'   => 'required',
    ];
    protected $msjError=[
        'paramIndicador.required' => 'Debe seleccionar el indicador.',
        'paramRestriccion.required' => 'Debe seleccionar una restricción.',
        'nombre.required' => 'Debe ingresar un nombre.'
    ];

    public function render()
    {
        /*switch ($this->nivel_proceso) {
            case 15001:
                $entradas = Suministrainfo::find($this->indicador_id)->entradas;
                break;
        }*/

        $param_indicador = Indicador::whereActivo(1)->get();

        $metas = Meta::where('metas.indicador_id','=', $this->indicador_id)->get();

        $mediciones = Medicion::select(DB::raw('medicions.id,
                    (SELECT detalle FROM detalles d1 WHERE d1.id = medicions.param_und_medida) as paraUnidadMedida,
                    (SELECT detalle FROM detalles d1 WHERE d1.id = medicions.param_frec_medicion) as paraFrecMedicion,
                    (SELECT detalle FROM detalles d1 WHERE d1.id = medicions.param_frec_reporte) as paraFrecReporte,
                    (SELECT detalle FROM detalles d1 WHERE d1.id = medicions.param_frec_revision) as paraFrecRevision,
                    anios.anio as aAnio'))
           // ->join('anios', 'medicions.anio_id', '=', 'anios.id')
            ->where('medicions.indicador_id','=', $this->indicador_id)
            ->get();

        /*$restricciones = IndicadorRestriccion::select(DB::raw('indicador_restriccion.id, indicador_restriccion.nombre, indicador_restriccion.indicador_id,
                    indicador_restriccion.restriccion_id, restriccions.restriccion rNombre'))
            ->join('restriccions', 'indicador_restriccion.indicador_id', '=', 'restriccions.id')
            ->where('indicador_restriccion.indicador_id','=', $this->indicador_id)
            ->get();

        foreach ($restricciones as $restriccion) {
            $this->datos[] = $restriccion->restriccion_id;
        }

        $restriccions = Restriccion::whereActivo(1)
            ->whereNotIn('id', $this->datos)
            ->orderBy('restriccion')->get();

        $this->paramIndicador = $this->indicador_id;*/

        //return view('livewire.ficha-indicador.restricciones.view', compact('restricciones', 'restriccions', 'param_indicador'));
        return view('livewire.ficha-indicador.medicion.view', compact('metas'));
    }

    public function openModal()
    {
        $this->openRestriccion = true;
        $this->emit('obtenerVinculos');
    }

    private function resetInput()
    {
        $this->openRestriccion = false;
        $this->paramInteresado = null;
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->openRestriccion = false;
    }
}
