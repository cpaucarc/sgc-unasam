<?php

namespace App\Http\Livewire\Ficha;

use Livewire\Component;
use App\Models\Meta;
use App\Models\Resultado;

class GraficoBarras extends Component
{
    public $indicador_id;

    public $labels = [], $metas = [], $valores = [];

    protected $listeners = ['render'];

    public function render()
    {
        $this->labels = [];
        $this->metas = [];
        $this->valores = [];
        $resultados = Resultado::leftjoin('metas', function ($join) {
            $join->on('metas.indicador_id', '=', 'resultados.indicador_id')
                ->on('metas.param_periodo', 'resultados.param_periodo')
                ->on('metas.anio_id', 'resultados.anio_id')
                ->whereNull('metas.deleted_at');
        })
            ->leftjoin('detalles', 'resultados.param_periodo', '=', 'detalles.id')
            ->where('resultados.indicador_id', $this->indicador_id)
            ->orderBy('resultados.anio_id', 'DESC')
            ->orderBy('resultados.param_periodo', 'DESC')
            ->take(12)
            ->get();

        foreach ($resultados as $resultado) {
            array_push($this->labels, $resultado->anio_id . " " . $resultado->detalle);
            array_push($this->metas, $resultado->meta);
            array_push($this->valores, $resultado->valor_indicador);
        }
        $labels = $this->labels;
        $metas = $this->metas;
        $valores = $this->valores;

        $this->emit('verGraficoBarras', $this->labels, $this->valores, $this->metas);

        return view('livewire.ficha-indicador.grafico.barras', compact('resultados', 'labels', 'metas', 'valores'));
    }
}
