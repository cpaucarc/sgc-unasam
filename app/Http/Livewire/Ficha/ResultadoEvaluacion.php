<?php

namespace App\Http\Livewire\Ficha;

use Livewire\Component;
use function view;
use App\Models\Medicion;
use Illuminate\Support\Facades\Auth;
use App\Models\Detalle;
use App\Models\Meta;
use App\Models\Escala;
use App\Models\Resultado;
use App\Models\Evaluacion;
use App\Models\Formula;
use App\Models\Indicador;
use App\Models\ResultadoVariable;
use App\Models\Variable;
use MathParser\StdMathParser;
use MathParser\Interpreting\Evaluator;
use Illuminate\Support\Facades\Http;

class ResultadoEvaluacion extends Component
{
    //VARBLES GENERALES
    public $indicador_id, $anio_id;
    protected $listeners = ['destroy', 'iniciaValores'];

    public $openCrearResultadoEvalua = false;
    public $openEditarResultadoEvalua = false;

    public $select_id_resul, $param_periodo_texto, $param_periodo, $valor_numerador, $valor_denominador, $valor_indicador, $meta;

    public $select_id_evaluacion, $resultado_id, $propuesta_mejora, $fecha_propuesta, $fecha_cumplimiento, $valor_desempenio;
    public $resultaPorMedicion = [];
    public $existen = [];
    //chart
    public $chartLabelEvolucion = []; //['BIMESTRE 1', 'BIMESTRE 2', 'BIMESTRE 3', 'BIMESTRE 4', 'BIMESTRE 5', 'BIMESTRE 6'];
    public $chartdataEvolucion = []; //[275, 90, 190, 205, 125,150];
    public $chartmax = 0;
    public $chartStep = 0;
    public $numeradores = [];
    public $denominadores = [];
    public $formula;
    public $tiene_api;
    public $tiene_metas;

    protected $rules = [
        'param_periodo' => 'required',
        'valor_numerador' => 'required|numeric|min:1',
        'valor_denominador' => 'required|numeric|min:1',
        //'valor_desempenio' => 'required',
        //'propuesta_mejora' => 'required',
        // 'fecha_propuesta' => 'required',
        //'fecha_cumplimiento' => 'required',
        'anio_id' => 'required',
        'indicador_id' => 'required',

        'numeradores.*.valor' => 'required',
        'denominadores.*.valor' => 'required'

    ];
    protected $msjError = [
        'param_periodo.required' => 'El campo periodo es obligatorio.',
        'valor_numerador.required' => 'El campo valor numerador es obligatorio.',
        'valor_numerador.numeric' => 'El campo valor numerador debe ser numerico.',
        'valor_numerador.min' => 'El campo valor numerador debe ser numerico minimo 1.',
        'valor_denominador.required' => 'El campo valor denominador es obligatorio.',
        'valor_denominador.numeric' => 'El campo valor denominador  debe ser numerico.',
        'valor_denominador.min' => 'El campo valor denominador  debe ser numerico minimo 1.',
        //'valor_desempenio.required' => 'El campo valor desempeño es obligatorio.',
        'propuesta_mejora.required' => 'El campo  propuesta de mejora es obligatorio.',
        'fecha_propuesta.required' => 'El campo  fecha de propuesta es obligatorio.',
        'fecha_cumplimiento.required' => 'El campo  fecha de cumplimineto es obligatorio.',
        'anio_id.required' => 'El campo año  es obligatorio.',
        'indicador_id.required' => 'El campo indicador es obligatorio.'
    ];

    public function mount()
    {
        $indicador = Indicador::findOrFail($this->indicador_id);
        $this->tiene_api = $indicador->macroproceso->tiene_api;

        $this->iniciaValores();
    }

    public function iniciaValores()
    {

        $this->numeradores = Variable::whereHas('formula', function ($q) {
            return $q->where('indicador_id', $this->indicador_id);
        })->whereTipo('numerador')->get();
        $this->numeradores->map(function ($n) {
            return $n->valor = 0;
        });
        $this->denominadores = Variable::whereHas('formula', function ($q) {
            return $q->where('indicador_id', $this->indicador_id);
        })->whereTipo('denominador')->get();
        $this->denominadores->map(function ($d) {
            return $d->valor = 0;
        });

        $formula = Formula::where('indicador_id', $this->indicador_id)->first();
        $this->formula = $formula ?? new Formula();

        if ($formula) {
            $this->actualizaValores();
        }
    }

    public function render()
    {
        $this->existen = [];
        $this->existenMeta = [];
        $this->resultaPorMedicion = [];
        //ENVIAR EL SELECT DEPENDIENDO QUE UNIDAD DE MEDIDA ES
        $medicionUnidad = Medicion::where('indicador_id', $this->indicador_id)
            //->where('anio_id',$this->anio_id)
            ->first();

        $medicionMeta = Meta::where('indicador_id', $this->indicador_id)
            ->where('anio_id', $this->anio_id)->first();

        $medicionMetaExiste = Meta::where('indicador_id', $this->indicador_id)
            ->where('anio_id', $this->anio_id)->get();

        $resultados = Resultado::where('indicador_id', $this->indicador_id)
            ->where('anio_id', $this->anio_id)->get();

        foreach ($resultados as $resultado) {
            array_push($this->existen, $resultado->param_periodo);
        }
        foreach ($medicionMetaExiste as $resultadoMetaExiste) {
            array_push($this->existenMeta, $resultadoMetaExiste->param_periodo);
        }

        $this->tiene_metas = !$medicionMetaExiste;

        if ($medicionMeta) {
            $this->meta = $medicionMeta->meta;
        }

        if ($medicionUnidad) {
            switch ($medicionUnidad->param_frec_medicion) {
                case 13001:  //ANUAL años (18)
                    $this->resultaPorMedicion = Detalle::whereparametro_id(18)->whereIn('id', $this->existenMeta)->whereNotIn('id', $this->existen)->whereActivo(1)->get();
                    $fuentechartLabel = Detalle::whereparametro_id(18)->whereActivo(1)->get();
                    break;
                case 13002:  // SEMESTRAL  19
                    $this->resultaPorMedicion = Detalle::whereparametro_id(19)->whereIn('id', $this->existenMeta)->whereNotIn('id', $this->existen)->whereActivo(1)->get();
                    $fuentechartLabel = Detalle::whereparametro_id(19)->whereActivo(1)->get();
                    break;
                case 13003:  // TRIMESTRAL 20
                    $this->resultaPorMedicion = Detalle::whereparametro_id(20)->whereIn('id', $this->existenMeta)->whereNotIn('id', $this->existen)->whereActivo(1)->get();
                    $fuentechartLabel = Detalle::whereparametro_id(20)->whereActivo(1)->get();
                    break;
                case 13004:  // BIMESTRAL  21
                    $this->resultaPorMedicion = Detalle::whereparametro_id(21)->whereIn('id', $this->existenMeta)->whereNotIn('id', $this->existen)->whereActivo(1)->get();
                    $fuentechartLabel = Detalle::whereparametro_id(21)->whereActivo(1)->get();
                    break;
                case 13005:   // MESUAL 22
                    $this->resultaPorMedicion = Detalle::whereparametro_id(22)->whereIn('id', $this->existenMeta)->whereNotIn('id', $this->existen)->whereActivo(1)->get();
                    $fuentechartLabel = Detalle::whereparametro_id(22)->whereActivo(1)->get();
                    break;
            }
            //generar chart
            $this->chartLabelEvolucion = [];
            if (isset($fuentechartLabel)) {
                foreach ($fuentechartLabel as $key => $valor) {
                    array_push($this->chartLabelEvolucion, $valor->detalle);
                }
            }
        }
        //para mostrar
        $datos = Resultado::select(
            'resultados.id',
            'detalles.detalle',
            'resultados.valor_numerador',
            'resultados.valor_denominador',
            'resultados.valor_indicador',
            'metas.meta',
            'evaluacions.propuesta_mejora',
            'evaluacions.fecha_propuesta',
            'evaluacions.fecha_cumplimiento'
        )
            ->leftjoin('detalles', 'resultados.param_periodo', '=', 'detalles.id')
            ->leftjoin('metas', function ($join) {
                $join->on('metas.indicador_id', '=', 'resultados.indicador_id')
                    ->on('metas.param_periodo', 'resultados.param_periodo')
                    ->on('metas.anio_id', 'resultados.anio_id')
                    ->whereNull('metas.deleted_at');
                //->on('metas.deleted_at','NULL');
            })
            ->leftjoin('evaluacions', 'evaluacions.resultado_id', '=', 'resultados.id')
            ->where('resultados.indicador_id', $this->indicador_id)
            ->where('resultados.anio_id', $this->anio_id)
            ->orderBy('resultados.param_periodo', 'ASC')
            // ->leftjoin('categories', 'users.idUser', '=', 'categories.user_id')
            ->get();
        $this->chartdataEvolucion = [];
        $this->chartmax = 0;
        foreach ($datos as $key => $valor) {
            array_push($this->chartdataEvolucion, round($valor->valor_numerador / $valor->valor_denominador, 2));
            if (round($valor->valor_numerador / $valor->valor_denominador, 2) > $this->chartmax) {
                $this->chartmax = round($valor->valor_numerador / $valor->valor_denominador, 2);
            }
        }

        return view('livewire.ficha-indicador.resultadoevaluacion.view', compact('resultados', 'datos'));
    }

    public  function store()
    {

        $valor_numerador =  $this->valor_numerador;
        $valor_denominador = $this->valor_denominador;

        $this->validate($this->rules, $this->msjError);

        $dataResulNew = Resultado::create([
            'param_periodo' => $this->param_periodo,
            'valor_numerador' => $valor_numerador,
            'valor_denominador' => $valor_denominador,
            'valor_indicador' => $this->valor_indicador,
            'indicador_id' => $this->indicador_id,
            'anio_id' => $this->anio_id,
            'activo' => '1',
            'created_user' => Auth::user()->id,
        ]);

        foreach ($this->numeradores as $numerador) {
            $numerador->resultados()->create([
                'resultado_id' => $dataResulNew->id,
                'valor' => $numerador['valor']
            ]);
        }
        foreach ($this->denominadores as $denominador) {
            $denominador->resultados()->create([
                'resultado_id' => $dataResulNew->id,
                'valor' => $denominador['valor']
            ]);
        }

        if (!is_null($this->fecha_propuesta)) {
            $this->fecha_propuesta = date('Y-m-d', strtotime($this->fecha_propuesta)); # code...
        }
        if (!is_null($this->fecha_cumplimiento)) {
            $this->fecha_cumplimiento = date('Y-m-d', strtotime($this->fecha_cumplimiento));
        }

        Evaluacion::create([
            'resultado_id' => $dataResulNew->id,
            'propuesta_mejora' => $this->propuesta_mejora,
            'fecha_propuesta' =>  $this->fecha_propuesta,
            'fecha_cumplimiento' =>  $this->fecha_cumplimiento,
            'activo' => '1',
            'created_user' => Auth::user()->id,
        ]);

        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El resultado ha sido registrado satisfactoriamente.'
        ];
        $this->openCrearResultadoEvalua = false;
        $this->resetInputEntradas();
        $this->mount();
        $this->emitTo('ficha.grafico-barras', 'render');
        $this->emit('alertaResultadoEvaluacion', $datos);
    }

    public function editResultadoEvaluacion($id)
    {
        $tempesultado = Resultado::findOrFail($id);

        $this->numeradores = Variable::whereHas('formula', function ($q) {
            return $q->where('indicador_id', $this->indicador_id);
        })->whereTipo('numerador')->get();
        $this->numeradores->map(function ($n) use ($tempesultado) {
            $r = $n->resultados->where('resultado_id', $tempesultado->id)->first();
            return $n->valor = $r ? $r->valor : 0;
        });
        $this->denominadores = Variable::whereHas('formula', function ($q) {
            return $q->where('indicador_id', $this->indicador_id);
        })->whereTipo('denominador')->get();
        $this->denominadores->map(function ($d) use ($tempesultado) {
            $r = $d->resultados->where('resultado_id', $tempesultado->id)->first();
            return $d->valor = $d->resultado ? $d->resultado->valor : 0;
        });

        $this->valor_indicador = $tempesultado->valor_indicador;

        $this->select_id_resul = $tempesultado->id;
        $this->param_periodo_texto = $tempesultado->detalleperiodo->detalle;
        $this->param_periodo = $tempesultado->param_periodo;
        $this->valor_numerador = $tempesultado->valor_numerador;
        $this->valor_denominador = $tempesultado->valor_denominador;
        if ($tempesultado->evaluacion) {
            $this->select_id_evaluacion = $tempesultado->evaluacion->id;
            $this->propuesta_mejora = $tempesultado->evaluacion->propuesta_mejora;
            $this->fecha_propuesta = $tempesultado->evaluacion->fecha_propuesta;
            $this->fecha_cumplimiento = $tempesultado->evaluacion->fecha_cumplimiento;
            $this->valor_desempenio = $tempesultado->evaluacion->valor_desempenio;
        }
        $this->openEditarResultadoEvalua = true;
        $this->openCrearResultadoEvalua = false;
    }

    public function update()
    {
        $this->validate([
            //'param_periodo' => 'required',
            'valor_numerador' => 'required|numeric|min:1',
            'valor_denominador' => 'required|numeric|min:1',
            //'valor_desempenio' => 'required',
            // 'propuesta_mejora' => 'required',
            //'fecha_propuesta' => 'required',
            //'fecha_cumplimiento' => 'required',
            // 'anio_id' => 'required',
            // 'indicador_id' => 'required'

        ], $this->msjError);

        $miResultado = Resultado::findOrFail($this->select_id_resul);
        $miResultado->update([
            'valor_numerador' => $this->valor_numerador,
            'valor_denominador' => $this->valor_denominador,
            'valor_indicador' => $this->valor_indicador,
            'activo' => '1',
            'updated_user' => Auth::user()->id
        ]);

        foreach ($this->numeradores as $numerador) {
            $numerador->resultados()->where('resultado_id', $miResultado->id)->delete();
            $numerador->resultados()->create([
                'resultado_id' => $miResultado->id,
                'valor' => $numerador['valor']
            ]);
        }
        foreach ($this->denominadores as $denominador) {
            $denominador->resultados()->where('resultado_id', $miResultado->id)->delete();
            $denominador->resultados()->create([
                'resultado_id' => $miResultado->id,
                'valor' => $denominador['valor']
            ]);
        }

        // actualizmos  valoracion
        if (!is_null($this->fecha_propuesta)) {
            $this->fecha_propuesta = date('Y-m-d', strtotime($this->fecha_propuesta)); # code...
        }
        if (!is_null($this->fecha_cumplimiento)) {
            $this->fecha_cumplimiento = date('Y-m-d', strtotime($this->fecha_cumplimiento));
        }
        $miEvaluacion = Evaluacion::findOrFail($this->select_id_evaluacion);
        $miEvaluacion->update([
            'propuesta_mejora' => $this->propuesta_mejora,
            'fecha_propuesta' =>  $this->fecha_propuesta,
            'fecha_cumplimiento' =>  $this->fecha_cumplimiento,
            'activo' => '1',
            'updated_user' => Auth::user()->id
        ]);

        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El resultado ha sido  actualizado satisfactoriamente.'
        ];
        $this->openEditarResultadoEvalua = false;
        $this->openCrearResultadoEvalua = false;
        $this->resetInputEntradas();
        $this->emitTo('ficha.grafico-barras', 'render');
        $this->emit('alertaResultadoEvaluacion', $datos);
    }

    public function destroy($id)
    {
        if ($id) {
            $miResul = Resultado::find($id);
            $miResul->evaluacion()->delete();
            $miResul->deleted_user = Auth::user()->id;
            $miResul->save();
            $miResul->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'El resultado ha sido eliminado satisfactoriamente.'
        ];
        $this->emitTo('ficha.grafico-barras', 'render');
        $this->emit('alertaResultadoEvaluacion', $datos);
    }

    public function modalEntrada()
    {
        $this->openCrearResultadoEvalua = true;
        $this->openEditarResultadoEvalua = false;
    }
    public function cancel()
    {
        $this->openEditarResultadoEvalua = false;
        $this->openCrearResultadoEvalua = false;
        $this->resetInputEntradas();
    }
    public function resetInputEntradas()
    {
        $this->select_id_resul = NULL;
        $this->param_periodo_texto = NULL;
        $this->param_periodo = NULL;
        $this->valor_numerador = NULL;
        $this->valor_denominador = NULL;
        $this->valor_indicador = NULL;
        $this->meta = NULL;

        $this->select_id_evaluacion = NULL;
        $this->resultado_id = NULL;
        $this->propuesta_mejora = NULL;
        $this->fecha_propuesta = NULL;
        $this->fecha_cumplimiento = NULL;
        $this->valor_desempenio = NULL;
    }

    public function actualizaValores()
    {
        $formula_numerador = $this->formula->numerador;

        $valor_numerador = new StdMathParser();
        $evaluador_numerador = new Evaluator();


        $valores_numerador = collect();
        foreach ($this->numeradores as $vn) {
            $valores_numerador->put($vn['variable'], is_numeric($vn['valor']) ? $vn['valor'] : 0);
        }

        $valores_denominador = collect();
        foreach ($this->denominadores as $vd) {
            $valores_denominador->put($vd['variable'], is_numeric($vd['valor']) ? $vd['valor'] : 0);
        }

        $evaluador_numerador->setVariables($valores_numerador->toArray());
        $this->valor_numerador =  round($valor_numerador->parse($formula_numerador)->accept($evaluador_numerador), 2);

        $formula_denominador = $this->formula->denominador;

        $valor_denominador = new StdMathParser();
        $evaluador_denominador = new Evaluator();

        $evaluador_denominador->setVariables($valores_denominador->toArray());
        $this->valor_denominador =  round($valor_denominador->parse($formula_denominador)->accept($evaluador_denominador), 2);


        if ($this->valor_denominador != 0) {
            $this->valor_indicador = round($this->valor_numerador / $this->valor_denominador, 2);
            $this->propuesta_mejora = obtenerDesempenioValor($this->indicador_id, $this->anio_id, $this->param_periodo, $this->valor_numerador, $this->valor_denominador, 2);
        }
    }

    public function getFormulaNumeradorProperty()
    {
        $formula_numerador = $this->formula->numerador;

        foreach ($this->numeradores as $vn) {
            $formula_numerador = str_replace($vn['variable'], $vn['variable'] . '#&', $formula_numerador);
        }
        foreach ($this->numeradores as $vn) {
            $formula_numerador = str_replace($vn['variable'] . '#&', '(' . $vn['nombre'] . ')', $formula_numerador);
        }
        $formula_numerador = str_replace(' ', '\ ', $formula_numerador);

        return $formula_numerador;
    }
    public function getFormulaDenominadorProperty()
    {
        $formula_denominador = $this->formula->denominador;

        foreach ($this->denominadores as $vd) {
            $formula_denominador = str_replace($vd['variable'], $vd['variable'] . '#&', $formula_denominador);
        }
        foreach ($this->denominadores as $vd) {
            $formula_denominador = str_replace($vd['variable'] . '#&', '(' . $vd['nombre'] . ')', $formula_denominador);
        }
        $formula_denominador = str_replace(' ', '\ ', $formula_denominador);

        return $formula_denominador;
    }


    public function obtenerDatos()
    {
        foreach ($this->numeradores as $vn) {
            if (!$vn['api_url'])
                $vn['api_url'] = config('app.url') . '/api/random';

            $response =  Http::get($vn['api_url']);

            if ($response->ok())
                $vn['valor'] = $response->body();
        }
        foreach ($this->denominadores as $vd) {
            if (!$vd['api_url'])
                $vd['api_url'] = config('app.url') . '/api/random';

            $response =  Http::get($vd['api_url']);

            if ($response->ok())
                $vd['valor'] = $response->body();
        }

        $this->actualizaValores();
    }


    public function updated()
    {
        $this->actualizaValores();
    }

    public function updatedIndicadorId(){
       // dd($this->indicador_id);
    }
}
