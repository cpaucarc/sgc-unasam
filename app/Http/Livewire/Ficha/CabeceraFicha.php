<?php

namespace App\Http\Livewire\Ficha;

use App\Models\Detalle;
use App\Models\Indicador;
use App\Models\Version;
use App\Models\Anio;
use Livewire\Component;

class CabeceraFicha extends Component
{
    public $ficha_id, $nombreFicha, $codigoFicha, $codigo_macroproceso, $nombre_macroproceso, $definicionFicha, $objetivoFicha;
    public $tipoFicha, $versionDetalleFicha, $fecha_vigencia;
    public $versiones = [];
    public $anios = [];
    public $miParametroAnio, $version;
    public $indicador;

    public function mount(){
        $this->indicador = Indicador::findOrFail($this->ficha_id);
        $this->version = $this->indicador->id;
    }

    public function render()
    {
        $this->anios = [];
        $this->miParametroAnio = getAnioIndicadorActual();
        $detalleFicha = Indicador::findOrFail($this->ficha_id);
        $this->indicador = $detalleFicha;
        $this->nombreFicha = $detalleFicha->nombre;
        $this->codigoFicha = $detalleFicha->codigo;
        $this->objetivoFicha = $detalleFicha->objetivo;
        $this->definicionFicha = $detalleFicha->definicion;

        $tipoFicha = Detalle::findOrFail($detalleFicha->param_tipoindicador);
        $this->tipoFicha = $tipoFicha->detalle;

        $versionFicha = Version::findOrFail($detalleFicha->version_id);
        $this->versionDetalleFicha = $versionFicha->numero;
        $this->fecha_vigencia = $versionFicha->fecha_vigencia;

        //dd($tipoFicha->detalle);
        $this->anios = Anio::where('activo', '1')->orderBy('anio', 'asc')->get();


        $this->versiones = Indicador::with('version')->where('codigo', $detalleFicha->codigo)->where('codigo', '!=', '')->get();

        return view('livewire.ficha-indicador.cabecera.view');
    }

    public function setMiAnioIndicador()
    {
        setAnioIndicadorActual($this->miParametroAnio);
        return redirect()->route('show.ficha', ['indicador' => $this->ficha_id, 'anio' => getAnioIndicadorActual()]);
    }

    public function seleccionarVersion()
    {
        return redirect()->route('show.ficha', ['indicador' => $this->version, 'anio' => getAnioIndicadorActual()]);
    }
}
