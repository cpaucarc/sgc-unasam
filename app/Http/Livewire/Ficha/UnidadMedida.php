<?php

namespace App\Http\Livewire\Ficha;

use Livewire\Component;
use function view;
use App\Models\Medicion;
use Illuminate\Support\Facades\Auth;
use App\Models\Detalle;
use App\Models\Meta;
use App\Models\Escala;
use App\Models\Resultado;

class UnidadMedida extends Component
{
    //VARBLES GENERALES
    public $indicador_id, $anio_id;

    public $openCrearUnidadMedida = false;
    public  $param_und_medida , $param_frec_medicion , $param_frec_reporte , $param_frec_revision ;
  
   public $meta;
   // manejo de escalas
   public $muyMaloMin, $muyMaloMax, $maloMin, $maloMax, $regularMin, $regularMax, $satisfactorioMin, $satisfactorioMax, $muySatisfactorioMin, $muySatisfactorioMax;

   public $msjRespuesta=" ",$hayregistros=false;

   public $escalamuyMalo=[], $escalaMalo=[], $escalaRegular=[], $escalaSatisfactorio=[], $escalamuySatisfactorio=[];
   protected $rules = [
    'param_und_medida' => 'required',
    'param_frec_medicion' => 'required',
    'param_frec_reporte' => 'required',
    'param_frec_revision' => 'required',
    'anio_id' => 'required',
    'indicador_id' => 'required'      
    
    ];
    protected $msjError=[     
        'param_und_medida.required' => 'El campo unidad de medida es obligatorio.',
        'param_frec_medicion.required' => 'El campo frecuencia de medición es obligatorio.',
        'param_frec_reporte.required' => 'El campo frecuencia de reporte es obligatorio.',
        'param_frec_revision.required' => 'El campo frecuencia de revision es obligatorio.',
        'anio_id.required' => 'El campo año  es obligatorio.',
        'indicador_id.required' => 'El campo indicador es obligatorio.',
    ];
    public function render()
    {
        $this->hayregistros=false;
        $medicionUnidad= Medicion::where('indicador_id',$this->indicador_id)
        //->where('anio_id',$this->anio_id)
        ->first();
    
        $metascount= Meta::where('indicador_id',$this->indicador_id)
        ->where('anio_id',$this->anio_id)->count();
        $resultadoscount= Resultado::where('indicador_id',$this->indicador_id)
        ->where('anio_id',$this->anio_id)->count();

        //PARAMETROS SELECCIONABLES
        $unidadmedidas= Detalle::whereparametro_id(12)->whereActivo(1)->get();         
        $tipoFrecMediciones= Detalle::whereparametro_id(13)->whereActivo(1)->get();
        $tipoFrecReportes= Detalle::whereparametro_id(13)->whereActivo(1)->get(); 
        $tipoFrecRevisiones= Detalle::whereparametro_id(13)->whereActivo(1)->get(); 
        if($metascount>0 || $resultadoscount>0 ){
            $this->hayregistros=true;
        }

        if($medicionUnidad && !$this->openCrearUnidadMedida)
        {
            $this->param_und_medida = $medicionUnidad->param_und_medida;
            $this->param_frec_medicion = $medicionUnidad->param_frec_medicion;
            $this->param_frec_reporte = $medicionUnidad->param_frec_reporte;
            $this->param_frec_revision = $medicionUnidad->param_frec_revision;
        } 
       
        return view('livewire.ficha-indicador.unidadmedida.view',
        compact('medicionUnidad','unidadmedidas',
        'tipoFrecMediciones','tipoFrecReportes',
        'tipoFrecRevisiones'));
    }
    public  function store(){
        $this->validate($this->rules,$this->msjError);
        $temporalUnidad= Medicion::where('indicador_id',$this->indicador_id)
        //->where('anio_id',$this->anio_id)
        ->first();


        if(!$temporalUnidad)
        {

            Medicion::create([
                'param_und_medida'=>$this->param_und_medida,               
                'param_frec_medicion'=>$this->param_frec_medicion,
                'param_frec_reporte'=>$this->param_frec_reporte,
                'param_frec_revision'=>$this->param_frec_revision,
                'indicador_id'=>$this->indicador_id,
                //'anio_id'=>$this->anio_id,
                //'param_periodo'=>$this->param_periodo,
                'activo'=>'1',
                'created_user' => Auth::user()->id,
            ]);
           

        }
        else{
            $miMedicion = Medicion::findOrFail($temporalUnidad->id);
            $miMedicion->update([
                'param_und_medida'=>$this->param_und_medida,                
                'param_frec_medicion'=>$this->param_frec_medicion,
                'param_frec_reporte'=>$this->param_frec_reporte,
                'param_frec_revision'=>$this->param_frec_revision,
                'indicador_id'=>$this->indicador_id,
                //'anio_id'=>$this->anio_id,
                'activo'=>'1',
                'updated_user' => Auth::user()->id
            ]);
        }
       

        $datos = [
            'tipo' => 'success',
            'mensaje' => 'El registro ha sido agregado satisfactoriamente.',
        ];

        $this->openCrearUnidadMedida = false;
        $this->resetInputEntradas();      
        $this->emit('alertaUnidadMedida', $datos);
    }


    public function modalEntrada()
    {
        $this->openCrearUnidadMedida = true;
    }
    public function cancel()
    {
        $this->openCrearUnidadMedida = false;
        $this->resetInputEntradas();
    }
    public function resetInputEntradas()
    {   
        $this->param_und_medida = NULL;
        $this->param_frec_medicion = NULL;
        $this->param_frec_reporte = NULL;
        $this->param_frec_revision = NULL;
       
    }

}
