<?php

namespace App\Http\Livewire\Ficha;

use function view;
use App\Models\Meta;
use App\Models\Escala;
use App\Models\Detalle;
use Livewire\Component;
use App\Models\Medicion;
use App\Models\Resultado;
use App\Models\Evaluacion;
use App\Models\Parametro;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class MetaEscala extends Component
{
    //VARBLES GENERALES
    protected $listeners = ['destroy'];
    public $indicador_id, $anio_id;
    public $openCrearMetaEscala = false;
    public $openEditarMetaEscala = false;
    //manejo de meta
    public $selected_id, $param_periodo, $param_periodo_texto, $meta, $ascendente = 1;
    // manejo de escalas
    public $selected_id_muy_malo, $selected_id_malo, $selected_id_regular, $selected_id_satisfactorio, $selected_id_muy_satisfactorio;
    public $muyMaloMin, $muyMaloMax, $maloMin, $maloMax, $regularMin, $regularMax, $satisfactorioMin, $satisfactorioMax, $muySatisfactorioMin, $muySatisfactorioMax;
    public $resultaPorMedicion = [];
    public $existen = [];
    public $datos_escala = [];
    protected $rules = [
        'param_periodo' => 'required',
        'meta' => 'required|numeric',
        'muyMaloMin' => 'required|numeric',
        'muyMaloMax' => 'required|numeric',
        'maloMin' => 'required|numeric',
        'maloMax' => 'required|numeric',
        'regularMin' => 'required|numeric',
        'regularMax' => 'required|numeric',
        'satisfactorioMin' => 'required|numeric',
        'satisfactorioMax' => 'required|numeric',
        'muySatisfactorioMin' => 'required|numeric',
        'muySatisfactorioMax' => 'required|numeric',
        'anio_id' => 'required',
        'indicador_id' => 'required'

    ];
    protected $msjError = [
        'param_periodo.required' => 'El campo periodo es obligatorio.',
        'meta.required' => 'El campo meta es obligatorio.',
        'meta.numeric' => 'El campo meta debe ser numerico.',
        'muyMaloMin.required' => 'El campo muy malo es obligatorio.',
        'muyMaloMin.numeric' => 'El campo muy malo  debe ser numerico.',
        'muyMaloMax.required' => 'El campo muy malo es obligatorio.',
        'muyMaloMax.numeric' => 'El campo muy malo  debe ser numerico.',
        'maloMin.required' => 'El campo  malo es obligatorio.',
        'maloMin.numeric' => 'El campo  malo  debe ser numerico.',
        'maloMax.required' => 'El campo  malo es obligatorio.',
        'maloMax.numeric' => 'El campo  malo  debe ser numerico.',
        'regularMin.required' => 'El campo  regular es obligatorio.',
        'regularMin.numeric' => 'El campo  regular  debe ser numerico.',
        'regularMax.required' => 'El campo  regular es obligatorio.',
        'regularMax.numeric' => 'El campo  regular  debe ser numerico.',
        'satisfactorioMin.required' => 'El campo  satisfactorio es obligatorio.',
        'satisfactorioMin.numeric' => 'El campo  satisfactorio  debe ser numerico.',
        'satisfactorioMax.required' => 'El campo  satisfactorio es obligatorio.',
        'satisfactorioMax.numeric' => 'El campo  satisfactorio  debe ser numerico.',
        'muySatisfactorioMin.required' => 'El campo muy satisfactorio es obligatorio.',
        'muySatisfactorioMin.numeric' => 'El campo muy satisfactorio  debe ser numerico.',
        'muySatisfactorioMax.required' => 'El campo muy satisfactorio es obligatorio.',
        'muySatisfactorioMax.numeric' => 'El campo muy satisfactorio  debe ser numerico.',
        'anio_id.required' => 'El campo año  es obligatorio.',
        'indicador_id.required' => 'El campo indicador es obligatorio.',
    ];

    public function render()
    {
        $this->existen = [];
        //ENVIAR EL SELECT DEPENDIENDO QUE UNIDAD DE MEDIDA ES
        $metas = Meta::where('indicador_id', $this->indicador_id)->where('anio_id', $this->anio_id)->orderBy('param_periodo', 'ASC')->get();

        $medicionUnidad = Medicion::where('indicador_id', $this->indicador_id)
            //->where('anio_id',$this->anio_id)
            ->first();

        foreach ($metas as $meta) {
            array_push($this->existen, $meta->param_periodo);
        }


        if ($medicionUnidad) {
            switch ($medicionUnidad->param_frec_medicion) {
                case 13001:  //ANUAL años (18)
                    // $this->resultaPorMedicion = Detalle::whereparametro_id(18)->whereNotIn('id', $this->existen)->whereActivo(1)->get();
                    $this->resultaPorMedicion = Detalle::whereparametro_id(18)->whereNotIn('id', $this->existen)->select('id', DB::raw("CONCAT(detalle,' '," . $this->anio_id . ") as detalle"))->get();
                    break;
                case 13002:  // SEMESTRAL  19
                    $this->resultaPorMedicion = Detalle::whereparametro_id(19)->whereNotIn('id', $this->existen)->whereActivo(1)->get();
                    break;
                case 13003:  // TRIMESTRAL 20
                    $this->resultaPorMedicion = Detalle::whereparametro_id(20)->whereNotIn('id', $this->existen)->whereActivo(1)->get();
                    break;
                case 13004:  // BIMESTRAL  21
                    $this->resultaPorMedicion = Detalle::whereparametro_id(21)->whereNotIn('id', $this->existen)->whereActivo(1)->get();
                    break;
                case 13005:   // MESUAL 22
                    $this->resultaPorMedicion = Detalle::whereparametro_id(22)->whereNotIn('id', $this->existen)->whereActivo(1)->get();
                    break;
            }
        }
        //REPLICAR
        // if ($this->meta) {
        //     $this->muySatisfactorioMax = $this->meta;
        // } else {
        //     $this->muySatisfactorioMax = null;
        // }

        // if ($this->maloMin) {
        //     $this->muyMaloMax = $this->maloMin;
        // } else {
        //     $this->muyMaloMax = null;
        // }

        // if ($this->regularMin) {
        //     $this->maloMax = $this->regularMin;
        // } else {
        //     $this->maloMax = null;
        // }

        // if ($this->satisfactorioMin) {
        //     $this->regularMax = $this->satisfactorioMin;
        // } else {
        //     $this->regularMax = null;
        // }

        // if ($this->muySatisfactorioMin) {
        //     $this->satisfactorioMax = $this->muySatisfactorioMin;
        // } else {
        //     $this->satisfactorioMax = null;
        // }


        //REPLICAR
        return view('livewire.ficha-indicador.metaescala.view', compact('metas'));
    }
    public  function store()
    {
        $this->validate($this->rules, $this->msjError);
        //INSERTAMOS LA META
        $metacreada = Meta::create([
            'meta' => $this->meta,
            'param_periodo' => $this->param_periodo,
            'anio_id' => $this->anio_id,
            'indicador_id' => $this->indicador_id,
            'meta' => $this->meta,
            'ascendente' => $this->ascendente,
            'activo' => '1',
            'created_user' => Auth::user()->id,
        ]);
        //FIN INSERTAR META
        //CREAMOS LAS ESCALAS PARA LA META INGRESADA
        Escala::create([
            'param_likert' => 14001,
            'meta_id' => $metacreada->id,
            'valor_ini' => $this->muyMaloMin,
            'valor_fin' => $this->muyMaloMax,
            'activo' => '1',
            'created_user' => Auth::user()->id,
        ]);

        Escala::create([
            'param_likert' => 14002,
            'meta_id' => $metacreada->id,
            'valor_ini' => $this->maloMin,
            'valor_fin' => $this->maloMax,
            'activo' => '1',
            'created_user' => Auth::user()->id,
        ]);
        Escala::create([
            'param_likert' => 14003,
            'meta_id' => $metacreada->id,
            'valor_ini' => $this->regularMin,
            'valor_fin' => $this->regularMax,
            'activo' => '1',
            'created_user' => Auth::user()->id,
        ]);
        Escala::create([
            'param_likert' => 14004,
            'meta_id' => $metacreada->id,
            'valor_ini' => $this->satisfactorioMin,
            'valor_fin' => $this->satisfactorioMax,
            'activo' => '1',
            'created_user' => Auth::user()->id,
        ]);

        Escala::create([
            'param_likert' => 14005,
            'meta_id' => $metacreada->id,
            'valor_ini' => $this->muySatisfactorioMin,
            'valor_fin' => $this->muySatisfactorioMax,
            'activo' => '1',
            'created_user' => Auth::user()->id,
        ]);

        //FIN CREAR ESCALAS  PARA LA META Y PERIODO INDICADOS


        $datos = [
            'tipo' => 'success',
            'mensaje' => 'La meta y escala ha sido registrado satisfactoriamente.'
        ];
        $this->openCrearMetaEscala = false;
        $this->resetInputEntradas();
        $this->emit('alertaMetaEscala', $datos);
    }

    public function modalEntrada()
    {
        $this->openCrearMetaEscala = true;
    }
    public function  editMetaEscala($id)
    {

        $medicionMeta = Meta::findOrFail($id);
        $this->selected_id = $medicionMeta->id;
        $this->param_periodo_texto = $medicionMeta->detalleperiodo->detalle;
        $this->meta = $medicionMeta->meta;
        $this->ascendente = $medicionMeta->ascendente;
        //ESCALAS
        //public $selected_id_muy_malo, $selected_id_malo,$selected_id_regular,
        //$selected_id_satisfactorio,$selected_id_muy_satisfactorio;
        /******************************************/
        $escalamuyMalo = Escala::where('meta_id', $medicionMeta->id)
            ->where('param_likert', 14001)->first();
        $this->selected_id_muy_malo = $escalamuyMalo->id;
        $this->muyMaloMin = $escalamuyMalo->valor_ini;
        $this->muyMaloMax = $escalamuyMalo->valor_fin;

        /******************************************/
        $escalaMalo = Escala::where('meta_id', $medicionMeta->id)
            ->where('param_likert', 14002)->first();
        $this->selected_id_malo = $escalaMalo->id;
        $this->maloMin = $escalaMalo->valor_ini;
        $this->maloMax = $escalaMalo->valor_fin;

        /******************************************/
        $escalaRegular = Escala::where('meta_id', $medicionMeta->id)
            ->where('param_likert', 14003)->first();
        $this->selected_id_regular = $escalaRegular->id;
        $this->regularMin = $escalaRegular->valor_ini;
        $this->regularMax = $escalaRegular->valor_fin;

        /******************************************/
        $escalaSatisfactorio = Escala::where('meta_id', $medicionMeta->id)
            ->where('param_likert', 14004)->first();
        $this->selected_id_satisfactorio = $escalaSatisfactorio->id;
        $this->satisfactorioMin = $escalaSatisfactorio->valor_ini;
        $this->satisfactorioMax = $escalaSatisfactorio->valor_fin;

        /******************************************/
        $escalamuySatisfactorio = Escala::where('meta_id', $medicionMeta->id)
            ->where('param_likert', 14005)->first();
        $this->selected_id_muy_satisfactorio = $escalamuySatisfactorio->id;
        $this->muySatisfactorioMin = $escalamuySatisfactorio->valor_ini;
        $this->muySatisfactorioMax = $escalamuySatisfactorio->valor_fin;
        $this->openEditarMetaEscala = true;
    }

    public function update()
    {
        $this->validate([
            'meta' => 'required|numeric',
            'muyMaloMin' => 'required|numeric',
            'muyMaloMax' => 'required|numeric',
            'maloMin' => 'required|numeric',
            'maloMax' => 'required|numeric',
            'regularMin' => 'required|numeric',
            'regularMax' => 'required|numeric',
            'satisfactorioMin' => 'required|numeric',
            'satisfactorioMax' => 'required|numeric',
            'muySatisfactorioMin' => 'required|numeric',
            'muySatisfactorioMax' => 'required|numeric',
            // 'anio_id' => 'required',
            // 'indicador_id' => 'required'  

        ], $this->msjError);
        // ACTULIZAMOS
        $miMeta = Meta::findOrFail($this->selected_id);
        $miMeta->update([
            'meta' => $this->meta,
            'ascendente' => $this->ascendente,
            'activo' => '1',
            'updated_user' => Auth::user()->id
        ]);
        // ACTUALIZAMOS LAS ESCALAS
        $miEscala = Escala::findOrFail($this->selected_id_muy_malo);
        $miEscala->update([
            'param_likert' => 14001,
            'valor_ini' => $this->muyMaloMin,
            'valor_fin' => $this->muyMaloMax,
            'activo' => '1',
            'updated_user' => Auth::user()->id
        ]);
        /********* */
        $miEscala = Escala::findOrFail($this->selected_id_malo);
        $miEscala->update([
            'param_likert' => 14002,
            'valor_ini' => $this->maloMin,
            'valor_fin' => $this->maloMax,
            'activo' => '1',
            'updated_user' => Auth::user()->id
        ]);
        /**************** */
        $miEscala = Escala::findOrFail($this->selected_id_regular);
        $miEscala->update([
            'param_likert' => 14003,
            'valor_ini' => $this->regularMin,
            'valor_fin' => $this->regularMax,
            'activo' => '1',
            'updated_user' => Auth::user()->id
        ]);
        /********************* */
        $miEscala = Escala::findOrFail($this->selected_id_satisfactorio);
        $miEscala->update([
            'param_likert' => 14004,
            'valor_ini' => $this->satisfactorioMin,
            'valor_fin' => $this->satisfactorioMax,
            'activo' => '1',
            'updated_user' => Auth::user()->id
        ]);
        /******************** */
        $miEscala = Escala::findOrFail($this->selected_id_muy_satisfactorio);
        $miEscala->update([
            'param_likert' => 14005,
            'valor_ini' => $this->muySatisfactorioMin,
            'valor_fin' => $this->muySatisfactorioMax,
            'activo' => '1',
            'updated_user' => Auth::user()->id
        ]);

        $datos = [
            'tipo' => 'success',
            'mensaje' => 'La meta y escala ha sido actualizado satisfactoriamente.'
        ];
        $this->openCrearMetaEscala = false;
        $this->openEditarMetaEscala = false;
        $this->resetInputEntradas();
        $this->emit('alertaMetaEscala', $datos);
    }

    public function destroy($id)
    {
        if ($id) {
            $miMeta = Meta::find($id);
            $miMeta->escalas()->delete();
            //   if ($miMeta-> escalas) {
            //     foreach ($miMeta-> escalas as $escala) {
            //         $miEscala = Escala::find($escala->id);
            //         $miEscala->deleted_user = Auth::user()->id;
            //         $miEscala->save();
            //         $miEscala->delete();
            //     }            
            //   } 
            $miMeta->deleted_user = Auth::user()->id;
            $miMeta->save();
            $miMeta->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'La meta y escala ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertaMetaEscala', $datos);
    }

    public function obtenerValores()
    {
        $meta = $this->meta;
        $ascendente = $this->ascendente;

        if ($ascendente == 1) {
            $limites = DB::table('escala_limites')->where('ascendente', 1)->orderBy('param_likert', 'asc')->get();
        } else {
            $limites = DB::table('escala_limites')->where('ascendente', 0)->orderBy('param_likert', 'desc')->get();
        }

        $suma = 0;
        $resta = 0;

        if ($meta) {
            foreach ($limites as $limite) {
                // dd($limite->valor);
                $suma += $limite->valor;
                $resta = 100 - $meta;
                $valor = $limite->valor;
                switch ($limite->param_likert) {
                    case '14001':
                        if ($ascendente == 1) {
                            $this->muyMaloMin = 0;
                            $this->muyMaloMax = number_format(($meta * $suma) / 100, 2);
                        } else {
                            $this->muyMaloMin = $this->maloMax;
                            $this->muyMaloMax = $this->maloMax + number_format(($resta * $valor) / 100, 2);
                        }
                        break;
                    case '14002':
                        if ($ascendente == 1) {
                            $this->maloMin =  $this->muyMaloMax;
                            $this->maloMax = number_format(($meta * $suma) / 100, 2);
                        } else {
                            $this->maloMin = $this->regularMax;
                            $this->maloMax = $this->regularMax + number_format(($resta * $valor) / 100, 2);
                        }
                        break;
                    case '14003':
                        if ($ascendente == 1) {
                            $this->regularMin = $this->maloMax;
                            $this->regularMax = number_format(($meta * $suma) / 100, 2);
                        } else {
                            $this->regularMin = $this->satisfactorioMax;
                            $this->regularMax = $this->satisfactorioMax + number_format(($resta * $valor) / 100, 2);
                        }
                        break;
                    case '14004':
                        if ($ascendente == 1) {
                            $this->satisfactorioMin = $this->regularMax;
                            $this->satisfactorioMax = number_format(($meta * $suma) / 100, 2);
                        } else {
                            $this->satisfactorioMin = $this->muySatisfactorioMax;
                            $this->satisfactorioMax = $this->muySatisfactorioMax + number_format(($resta * $valor) / 100, 2);
                        }
                        break;
                    case '14005':
                        if ($ascendente == 1) {
                            $this->muySatisfactorioMin = $this->satisfactorioMax;
                            $this->muySatisfactorioMax = ($meta * $suma) / 100;
                        } else {
                            $this->muySatisfactorioMin = $meta;
                            $this->muySatisfactorioMax = $meta + number_format(($resta * $valor) / 100, 2);
                        }
                        break;
                }
            }
        }
    }

    public function cancel()
    {
        $this->openCrearMetaEscala = false;
        $this->openEditarMetaEscala = false;
        $this->resetInputEntradas();
    }

    public function resetInputEntradas()
    {
        $this->selected_id = NULL;
        $this->param_periodo = NULL;
        $this->param_periodo_texto = NULL;
        $this->meta = NULL;

        $this->selected_id_muy_malo = NULL;
        $this->selected_id_malo = NULL;
        $this->selected_id_regular = NULL;
        $this->selected_id_satisfactorio = NULL;
        $this->selected_id_muy_satisfactorio = NULL;

        $this->muyMaloMin = NULL;
        $this->muyMaloMax = NULL;
        $this->maloMin = NULL;
        $this->maloMax = NULL;
        $this->regularMin = NULL;
        $this->regularMax = NULL;
        $this->satisfactorioMin = NULL;
        $this->satisfactorioMax = NULL;
        $this->muySatisfactorioMin = NULL;
        $this->muySatisfactorioMax = NULL;
    }
}
