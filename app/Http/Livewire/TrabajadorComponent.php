<?php

namespace App\Http\Livewire;

use App\Models\Area;
use App\Models\Cargo;
use App\Models\Persona;
use App\Models\Trabajador;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class TrabajadorComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $orden = 'asc';
    public $selected_id, $search, $fecha_inicio, $fecha_fin, $persona_id, $area_id, $cargo_id;
    public $updateMode = false;
    public $showMode = false;
    public $sort = 'trabajadors.id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy', 'store'];
    protected $rules = [
        'fecha_inicio'=> 'required',
        //'fecha_fin'   => 'required,
        'persona_id' => 'required',
        'area_id' => 'required',
        'cargo_id' => 'required'
    ];
    protected $msjError=[
        'fecha_inicio.required' => 'El campo fecha de inicio es obligatorio',
        'persona_id.required' => 'El campo persona es obligatorio.',
        'area_id.required' => 'El campo área es obligatorio.',
        'cargo_id.required' => 'El campo cargo es obligatorio.'
    ];

    public function render()
    {
        $areas = Area::whereActivo(1)->get();
        $parametro_personas = Persona::whereActivo(1)->get();
        $parametro_cargos = Cargo::whereActivo(1)->orderBy('nombre','asc')->get();
        $trabajadores = Trabajador::select(DB::raw('trabajadors.id as id, trabajadors.fecha_inicio as fecha_inicio, trabajadors.fecha_fin as fecha_fin,
            trabajadors.persona_id as persona_id, trabajadors.area_id as area_id, trabajadors.cargo_id as cargo_id, trabajadors.activo,
            trabajadors.created_user, trabajadors.updated_at, trabajadors.created_at, CONCAT(personas.ape_paterno," ", personas.ape_materno, " ",personas.nombres) AS pNombres,
            areas.nombre as aNombre, cargos.nombre cCargo'))
            ->Join('personas', 'trabajadors.persona_id', '=', 'personas.id')
            ->Join('areas', 'trabajadors.area_id', '=', 'areas.id')
            ->Join('cargos', 'trabajadors.cargo_id', '=', 'cargos.id')
            //->where('documentos.nombreReferencial', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->orden)
            ->paginate($this->cantidad);
        return view('livewire.trabajadors.view', compact('trabajadores', 'areas', 'parametro_personas', 'parametro_cargos'));
    }

    public function index()
    {
        return view('livewire.trabajadors.index');
    }

    private function resetInput()
    {
        $this->open = false;
        $this->fecha_inicio = null;
        $this->fecha_fin = null;
        $this->persona_id = null;
        $this->area_id = null;
        $this->cargo_id = null;
        $this->emit('resetearSelect');
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
        $this->showMode = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->showMode = false;
    }

    public function store()
    {
        //dd($this->persona_id);
        $this->validate($this->rules,$this->msjError);
        Trabajador::create([
            'fecha_inicio' => date('Y-m-d', strtotime($this->fecha_inicio)),
            'fecha_fin' => date('Y-m-d', strtotime($this->fecha_fin)),
            'persona_id' => $this->persona_id,
            'area_id' => $this->area_id,
            'cargo_id' => $this->cargo_id,
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();
        $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Trabajador creada satisfactoriamente'
        ];
        $this->emit('alertRespuesta', $datos1);
    }

    public function edit($id)
    {
        $persona = Trabajador::findOrFail($id);
        $this->selected_id = $id;
        $this->persona_id = $persona->persona_id;
        $this->cargo_id = $persona->cargo_id;
        $this->area_id = $persona->area_id;
        $this->fecha_inicio = $persona->fecha_inicio;
        $this->fecha_fin = $persona->fecha_fin;
        $this->updateMode = true;

        $this->emit('asignarDatos', $this->persona_id, $this->area_id);
        //$this->emit('actualizarSelect');
    }

    public function show($id)
    {
        $persona = Trabajador::findOrFail($id);
        $this->selected_id = $id;
        $this->persona_id = $persona->persona_id;
        $this->cargo_id = $persona->cargo_id;
        $this->area_id = $persona->area_id;
        $this->fecha_inicio = $persona->fecha_inicio;
        $this->fecha_fin = $persona->fecha_fin;
        $this->showMode = true;
    }

    public function update()
    {
        $this->validate([
            'fecha_inicio'=> 'required',
            'persona_id' => 'required',
            'area_id' => 'required',
            'cargo_id' => 'required'
        ]);

        if ($this->selected_id) {
            $record = Trabajador::find($this->selected_id);
            $record->update([
                'fecha_inicio' => date('Y-m-d', strtotime($this->fecha_inicio)),
                'fecha_fin' => date('Y-m-d', strtotime($this->fecha_fin)),
                'persona_id' => $this->persona_id,
                'area_id' => $this->area_id,
                'cargo_id' => $this->cargo_id,
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;

            $datos1 = [
                'tipo' => 'success',
                'mensaje' => 'Trabajador actualizada satisfactoriamente'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Trabajador::where('id', $id);
            $record->delete();
            $datos1 = [
                'tipo' => 'error',
                'mensaje' => 'El Trabajador ha sido eliminado.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->orden == 'desc') {
                $this->orden = 'asc';
            } else {
                $this->orden = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->orden = 'asc';
        }
    }
}
