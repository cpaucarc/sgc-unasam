<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Indicador;
use App\Models\Microproceso;
use App\Models\Proceso;
use App\Models\Subproceso;
use App\Models\Macroproceso;
use App\Models\Detalle;
use App\Models\Medicion;
use App\Models\Version;
use App\Models\Anio;
use Illuminate\Support\Facades\Auth;

class Medicions extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $selected_id, $search, $param_und_medida , $numerador, $denominador , $param_frec_medicion , $param_frec_reporte , $param_frec_revision , $indicador_id, $anio_id, $param_periodo;
   
    public $updateMode = false;
    public $sort = 'id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        // 'param_und_medida' => 'required',
        // 'numerador' => 'required',
        // 'denominador' => 'required',
        // 'param_frec_medicion' => 'required',
        // 'param_frec_reporte' => 'required',
        'anio_id' => 'required',
        'indicador_id' => 'required',
        ];
    protected $msjError=[
        // 'param_und_medida.required' => 'El campo unidad medida es obligatorio.',
        // 'numerador.required' => 'El campo numerador es obligatorio.',
        // 'denominador.required' => 'El campo denominador es obligatorio.',        
        // 'param_frec_medicion.required' => 'El campo frecuencia medición es obligatorio.',
        // 'param_frec_reporte.required' => 'El campo frecuencia reporte es obligatorio.',
        'anio_id.required' => 'El campo año  es obligatorio.',
        'indicador_id.required' => 'El campo indicador es obligatorio.',
    ];


    public function render()
    {
        $mediciones = Medicion::where('numerador', 'LIKE', '%' . $this->search . '%')
            ->orWhere('denominador', 'LIKE', '%' . $this->search . '%')
            ->orWhere('indicador_id', 'LIKE', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->direction)
            ->paginate($this->cantidad);

    
        $indicadores= Indicador::whereActivo(1)->get(); 
        $unidadmedidas= Detalle::whereparametro_id(12)->whereActivo(1)->get();         
        $tipoFrecMediciones= Detalle::whereparametro_id(13)->whereActivo(1)->get();
        $tipoFrecReportes= Detalle::whereparametro_id(13)->whereActivo(1)->get(); 
        $tipoFrecRevisiones= Detalle::whereparametro_id(13)->whereActivo(1)->get();         
        $tipoPeriodos= Detalle::whereparametro_id(13)->whereActivo(1)->get();  
        $anios= Anio::whereActivo(1)->get(); 
        
        $search = '%' . $this->search . '%';
        return view('livewire.medicions.view', compact('mediciones','tipoFrecMediciones','tipoFrecReportes','tipoFrecRevisiones','unidadmedidas','indicadores','tipoPeriodos','anios'));
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
    }

    private function resetInput()
    {
        $this->open = false;
        $this->param_und_medida= null;
        $this->numerador= null;
        $this->denominador= null;
        $this->param_frec_medicion= null;
        $this->param_frec_reporte= null;
        $this->param_frec_revision= null;
        $this->indicador_id= null;
        $this->anio_id=null;
        $this->param_periodo =null;
        $this->activo = null;
    }

    public function store()
    {
        $this->validate($this->rules,$this->msjError);
        
        Medicion::create([  

            'param_und_medida'=>$this->param_und_medida,
            'numerador'=>$this->numerador,
            'denominador'=>$this->denominador,
            'param_frec_medicion'=>$this->param_frec_medicion,
            'param_frec_reporte'=>$this->param_frec_reporte,
            'param_frec_revision'=>$this->param_frec_revision,
            'indicador_id'=>$this->indicador_id,
            'anio_id'=>$this->anio_id,
            'param_periodo'=>$this->param_periodo,
            'activo'=>'1',
            'created_user' => Auth::user()->id,
        ]);       
        
        $this->resetInput();
        $datos = [
            'tipo' => 'success',
            'mensaje' => 'La Medición ha sido creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
     

       
    }

    public function edit($id)
    {
        $miMedicion = Medicion::findOrFail($id);
        $this->selected_id = $id;
        $this->param_und_medida = $miMedicion->param_und_medida;
        $this->numerador = $miMedicion->numerador;
        $this->denominador = $miMedicion->denominador;
        $this->param_frec_medicion = $miMedicion->param_frec_medicion;
        $this->param_frec_reporte = $miMedicion->param_frec_reporte;
        $this->param_frec_revision = $miMedicion->param_frec_revision;
        $this->indicador_id = $miMedicion->indicador_id;
        $this->anio_id =  $miMedicion->anio_id;
        $this->param_periodo = $miMedicion->param_periodo;
        $this->activo= $miMedicion->activo;
        $this->updateMode = true;
    }

    public function update()
    {
        $this->validate([
            // 'param_und_medida' => 'required',
            // 'numerador' => 'required',
            // 'denominador' => 'required',
            // 'param_frec_medicion' => 'required',
            // 'param_frec_reporte' => 'required',
            'anio_id' => 'required',
            'indicador_id' => 'required',           
        ],$this->msjError);
        if ($this->selected_id) {
            $miMedicion = Medicion::findOrFail($this->selected_id);
            $miMedicion->update([
                'param_und_medida'=>$this->param_und_medida,
                'numerador'=>$this->numerador,
                'denominador'=>$this->denominador,
                'param_frec_medicion'=>$this->param_frec_medicion,
                'param_frec_reporte'=>$this->param_frec_reporte,
                'param_frec_revision'=>$this->param_frec_revision,
                'indicador_id'=>$this->indicador_id,
                'anio_id'=>$this->anio_id,
                'param_periodo'=>$this->param_periodo,
                'activo'=>'1',
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;
            $datos = [
                'tipo' => 'success',
                'mensaje' => 'La Medición ha sido actualizada satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos);
        }
        
    }

    public function destroy($id)
   {
        if ($id) {
            $record = Medicion::find($id);
            $record->deleted_user = Auth::user()->id;
            $record->save();
            $record->delete();
        }
        $datos = [
            'tipo' => 'error',
            'mensaje' => 'La Medición ha sido eliminado satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos);
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->direction == 'desc') {
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }
   

}
