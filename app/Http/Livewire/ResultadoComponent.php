<?php

namespace App\Http\Livewire;

use App\Models\Anio;
use App\Models\Detalle;
use App\Models\Indicador;
use App\Models\Resultado;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class ResultadoComponent extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $open = false;
    public $cantidad = 10;
    public $orden = 'asc';
    public $selected_id, $search, $param_periodo, $valor_numerador, $valor_denominador, $valor_indicador, $indicador_id, $anio_id, $resultado_id;
    public $updateMode = false;
    public $showMode = false;
    public $sort = 'resultados.id';
    public $direction = 'desc';

    protected $listeners = ['render', 'destroy'];
    protected $rules = [
        //'param_periodo'       => 'required',
        //'valor_numerador'   => 'required',
        //'valor_denominador'   => 'required',
        //'valor_indicador' => 'required',
        'anio_id' => 'required',
        'indicador_id' => 'required'
    ];
    protected $msjError=[
        'anio_id.required' => 'Debe seleccionar año.',
        'indicador_id.required' => 'Debe seleccionar un indicador.'
    ];

    public function render()
    {
        $parametro_indicadores = Indicador::whereActivo(1)->get();
        $parametro_anios = Anio::whereActivo(1)->get();
        $parametro_periodos = Detalle::whereparametro_id(16)->whereActivo(1)->get();

        $resultados = Resultado::select(DB::raw('resultados.id as id, resultados.param_periodo, resultados.valor_numerador, resultados.valor_denominador, resultados.valor_indicador,
        resultados.indicador_id, resultados.anio_id, resultados.activo,
        resultados.created_user, resultados.updated_at, resultados.created_at, indicadors.nombre indNombre, anios.anio,
         (SELECT detalle FROM detalles t1 WHERE t1.id = resultados.param_periodo) as dPeriodo'))
            ->join('indicadors', 'resultados.indicador_id', '=', 'indicadors.id')
            ->join('anios', 'resultados.anio_id', '=', 'anios.id')
            //->where('resultados.param_periodo', 'like', '%' . $this->search . '%')
            ->orderBy($this->sort, $this->orden)
            ->paginate($this->cantidad);
        return view('livewire.resultados.view', compact('resultados', 'parametro_indicadores', 'parametro_anios', 'parametro_periodos'));
    }

    public function index()
    {
        return view('livewire.resultados.index');
    }

    private function resetInput()
    {
        $this->open = false;
        $this->param_periodo = null;
        $this->valor_denominador = null;
        $this->valor_indicador = null;
        $this->valor_numerador = null;
        $this->anio_id = null;
        $this->indicador_id = null;
    }

    public function cancel()
    {
        $this->resetInput();
        $this->updateMode = false;
        $this->open = false;
        $this->showMode = false;
    }

    public function cerrar()
    {
        $this->resetInput();
        $this->showMode = false;
    }

    public function store()
    {
        $this->validate($this->rules,$this->msjError);
        Resultado::create([
            'param_periodo' => $this->param_periodo,
            'valor_numerador' => $this->valor_numerador,
            'valor_denominador' => $this->valor_denominador,
            'valor_indicador' => $this->valor_indicador,
            'indicador_id' => $this->indicador_id,
            'anio_id' => $this->anio_id,
            //'resultado_id' => $this->resultado_id,
            'created_user' => Auth::user()->id
        ]);

        $this->resetInput();

        $datos1 = [
            'tipo' => 'success',
            'mensaje' => 'Resultado creada satisfactoriamente.'
        ];
        $this->emit('alertRespuesta', $datos1);

    }

    public function edit($id)
    {
        $area = Resultado::findOrFail($id);
        $this->selected_id = $id;
        $this->anio_id = $area->anio_id;
        $this->indicador_id = $area->indicador_id;
        $this->valor_numerador = $area->valor_numerador;
        $this->valor_denominador = $area->valor_denominador;
        $this->valor_indicador = $area->valor_indicador;
        $this->param_periodo = $area->param_periodo;
        $this->updateMode = true;
    }

    public function show($id)
    {
        $area = Resultado::findOrFail($id);
        $this->selected_id = $id;
        $this->anio_id = $area->anio_id;
        $this->indicador_id = $area->indicador_id;
        $this->valor_numerador = $area->valor_numerador;
        $this->valor_denominador = $area->valor_denominador;
        $this->valor_indicador = $area->valor_indicador;
        $this->param_periodo = $area->param_periodo;
        $this->showMode = true;
    }

    public function update()
    {
        $this->validate([
            'param_periodo'       => 'required',
            'valor_numerador'   => 'required',
            'valor_denominador'   => 'required',
            'valor_indicador' => 'required',
            'anio_id' => 'required',
            'indicador_id' => 'required'
        ]);

        if ($this->selected_id) {
            $record = Resultado::find($this->selected_id);
            $record->update([
                'param_periodo' => $this->param_periodo,
                'valor_numerador' => $this->valor_numerador,
                'valor_denominador' => $this->valor_denominador,
                'valor_indicador' => $this->valor_indicador,
                'indicador_id' => $this->indicador_id,
                'anio_id' => $this->anio_id,
                'updated_user' => Auth::user()->id
            ]);
            $this->resetInput();
            $this->updateMode = false;
            $datos1 = [
                'tipo' => 'success',
                'mensaje' => 'Resultado actualizada satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Resultado::where('id', $id);
            $record->delete();

            $datos1 = [
                'tipo' => 'error',
                'mensaje' => 'Resultado eliminado satisfactoriamente.'
            ];
            $this->emit('alertRespuesta', $datos1);
        }
    }
    public function ordenar($sort)
    {
        if ($this->sort == $sort) {
            if ($this->orden == 'desc') {
                $this->orden = 'asc';
            } else {
                $this->orden = 'desc';
            }
        } else {
            $this->sort = $sort;
            $this->orden = 'asc';
        }
    }
}
