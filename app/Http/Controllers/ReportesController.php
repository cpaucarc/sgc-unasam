<?php

namespace App\Http\Controllers;

use App\Models\Archivo;
use App\Models\Indicador;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;

class ReportesController extends Controller
{
    public function index()
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Resultados"]
        ];
        return view('sgc.reportes.index', compact('pageConfigs', 'breadcrumbs'));
    }

    public function indicadores()
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Resultados"]
        ];
        return view('sgc.reportes.indicadores', compact('pageConfigs', 'breadcrumbs'));
    }

    public function por_tipo()
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Resultados"]
        ];
        return view('sgc.reportes.por_tipo', compact('pageConfigs', 'breadcrumbs'));
    }

    public function por_procesos()
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Resultados"]
        ];

        return view('sgc.reportes.por_procesos', compact('pageConfigs', 'breadcrumbs'));
    }

    public function por_organicas()
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Resultados"]
        ];

        return view('sgc.reportes.por_organicas', compact('pageConfigs', 'breadcrumbs'));
    }


    /** Menú de trazabilidad */
    public function trazabilidad()
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Resultados"]
        ];

        return view('sgc.reportes.trazabilidad', compact('pageConfigs', 'breadcrumbs'));
    }

    public function descargarArchivo($id)
    {
        $archivo = Archivo::find($id);
        $datos = ['tipo' => 'success', 'mensaje' => 'El archivo se ha descargado satisfactoriamente.'];
        return Storage::response($archivo->ruta, $archivo->nombre);
    }
}
