<?php

namespace App\Http\Controllers;

use App\Models\Anio;
use App\Models\Macroproceso;
use App\Models\Proceso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Permission;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Inicio"]
        ];
        return view('/content/layouts/home', compact('pageConfigs', 'breadcrumbs'));
    }
    public function dashboard()
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Dashboard"]
        ];
        return view('/sgc/dashboard/index', compact('pageConfigs', 'breadcrumbs'));
    }

    public function create_policies()
    {

        $permisos = Permission::selectRaw("LEFT(name, LOCATE('.', name) - 1) as name")->where('name', 'NOT LIKE', '%\\_%')->distinct()->pluck('name')->filter()->values();

        foreach ($permisos as $permiso) {
            Artisan::call('generate:resource ' . $permiso . ' --force');
        }
    }

    public function caracterizacion()
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Caracterización"]
        ];
        return view('/sgc/caracterizacion/index', compact('pageConfigs', 'breadcrumbs'));
    }

    public function mProceso()
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Manuales de Proceso"]
        ];
        return view('/sgc/manuales/proceso/index', compact('pageConfigs', 'breadcrumbs'));
    }
    public function mMacroProcesoVer(Macroproceso $macroproceso)
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Manuales de Macroproceso"]
        ];
        $anios = Anio::whereActivo(1)->orderBy('anio', 'asc')->take(10)->get();
        return view('/sgc/manuales/macroproceso/view', compact('pageConfigs', 'breadcrumbs', 'macroproceso', 'anios'));
    }
    public function mProcesoVer(Proceso $proceso)
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Manuales de Proceso"]
        ];
        return view('/sgc/manuales/proceso/view', compact('pageConfigs', 'breadcrumbs', 'proceso'));
    }
    public function mSoftware()
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Manuales de Software"]
        ];
        return view('/sgc/manuales/software/index', compact('pageConfigs', 'breadcrumbs'));
    }
    public function mSoftwareVer($id)
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Manuales de Software"]
        ];
        return view('/sgc/manuales/software/view', compact('pageConfigs', 'breadcrumbs', 'id'));
    }
}
