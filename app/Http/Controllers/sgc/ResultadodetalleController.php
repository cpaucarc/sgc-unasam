<?php

namespace App\Http\Controllers\sgc;

use App\Http\Controllers\Controller;
use App\Models\Resultadodetalle;
use Illuminate\Http\Request;

class ResultadodetalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Resultadodetalle  $resultadodetalle
     * @return \Illuminate\Http\Response
     */
    public function show(Resultadodetalle $resultadodetalle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Resultadodetalle  $resultadodetalle
     * @return \Illuminate\Http\Response
     */
    public function edit(Resultadodetalle $resultadodetalle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Resultadodetalle  $resultadodetalle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resultadodetalle $resultadodetalle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Resultadodetalle  $resultadodetalle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resultadodetalle $resultadodetalle)
    {
        //
    }
}
