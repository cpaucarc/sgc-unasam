<?php

namespace App\Http\Controllers\sgc;

use App\Http\Controllers\Controller;
use App\Models\Procesomecanismo;
use Illuminate\Http\Request;

class ProcesomecanismoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Procesomecanismo  $procesomecanismo
     * @return \Illuminate\Http\Response
     */
    public function show(Procesomecanismo $procesomecanismo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Procesomecanismo  $procesomecanismo
     * @return \Illuminate\Http\Response
     */
    public function edit(Procesomecanismo $procesomecanismo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Procesomecanismo  $procesomecanismo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Procesomecanismo $procesomecanismo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Procesomecanismo  $procesomecanismo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Procesomecanismo $procesomecanismo)
    {
        //
    }
}
