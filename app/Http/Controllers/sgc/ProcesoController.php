<?php

namespace App\Http\Controllers\sgc;

use App\Models\Proceso;
use App\Models\Subproceso;
use App\Models\Requisito;
use App\Models\Procesomecanismo;
use App\Models\Indicador;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Macroprocesotermino;

class ProcesoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Procesos"]
        ];
        return view('livewire.procesos.index', compact('pageConfigs', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Proceso  $proceso
     * @return \Illuminate\Http\Response
     */
    public function show(Proceso $proceso)
    {
        $subprocesos = Subproceso::whereActivo(1)->whereProceso_id($proceso->id)->orderBy('orden', 'asc')->get();
        $requisitos = Requisito::whereActivo(1)->whereProceso_id($proceso->id)
            ->whereNotNull('macroproceso_id')->whereNull('subproceso_id')->whereNull('microproceso_id')
            ->orderBy('id', 'asc')->get();
        $procesoMecanismos = Procesomecanismo::whereActivo(1)->whereProceso_id($proceso->id)
            ->whereNotNull('macroproceso_id')->whereNull('subproceso_id')->whereNull('microproceso_id')
            ->orderBy('id', 'asc')->get();
        $indicadores = Indicador::whereActivo(1)->whereProceso_id($proceso->id)
            ->whereNotNull('macroproceso_id')->whereNull('subproceso_id')->whereNull('microproceso_id')
            ->orderBy('id', 'asc')->get();
        $terminoDefinicions = Macroprocesotermino::whereActivo(1)->whereMacroproceso_id($proceso->macroproceso_id)->get();

        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];

        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['link' => "/macroprocesos/" . $proceso->macroproceso_id, 'name' => "Macroproceso"],
            ['name' => "Proceso"]
        ];

        return view('sgc.procesos.show', compact('proceso', 'subprocesos', 'requisitos', 'procesoMecanismos', 'indicadores', 'terminoDefinicions', 'breadcrumbs', 'pageConfigs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Proceso  $proceso
     * @return \Illuminate\Http\Response
     */
    public function edit(Proceso $proceso)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Proceso  $proceso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proceso $proceso)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Proceso  $proceso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proceso $proceso)
    {
        //
    }
    public function caracterizacion(Proceso $proceso)
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['link' => "/procesos/" . $proceso->id, 'name' => "Proceso"],
            ['name' => "Caracterización"]
        ];
        return view('sgc.procesos.caracterizacion', compact('proceso', 'breadcrumbs', 'pageConfigs'));
    }
}
