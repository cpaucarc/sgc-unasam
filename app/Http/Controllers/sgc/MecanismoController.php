<?php

namespace App\Http\Controllers\sgc;

use App\Http\Controllers\Controller;
use App\Models\Mecanismo;
use Illuminate\Http\Request;

class MecanismoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('livewire.mecanismos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mecanismo  $mecanismo
     * @return \Illuminate\Http\Response
     */
    public function show(Mecanismo $mecanismo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mecanismo  $mecanismo
     * @return \Illuminate\Http\Response
     */
    public function edit(Mecanismo $mecanismo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mecanismo  $mecanismo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mecanismo $mecanismo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mecanismo  $mecanismo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mecanismo $mecanismo)
    {
        //
    }
}
