<?php

namespace App\Http\Controllers\sgc;

use App\Http\Controllers\Controller;
use App\Models\Indicador;
use App\Models\Microproceso;
use Illuminate\Http\Request;
use App\Models\Macroprocesotermino;
use App\Models\Procesomecanismo;
use App\Models\Requisito;

class MicroprocesoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Microprocesos"]
        ];
        return view('livewire.microprocesos.index', compact('pageConfigs', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Microproceso  $microproceso
     * @return \Illuminate\Http\Response
     */
    public function show(Microproceso $microproceso)
    {
        $requisitos = Requisito::whereActivo(1)->whereMicroproceso_id($microproceso->id)
       ->whereNotNull('macroproceso_id')->whereNotNull('proceso_id')->whereNotNull('subproceso_id')
        ->orderBy('id', 'asc')->get();
        $procesoMecanismos=Procesomecanismo::whereActivo(1)->whereMacroproceso_id($microproceso->id)
       ->whereNotNull('macroproceso_id')->whereNotNull('proceso_id')->whereNotNull('subproceso_id')
        ->orderBy('id', 'asc')->get();
        $indicadores=Indicador::whereActivo(1)->whereMacroproceso_id($microproceso->id)
       ->whereNotNull('macroproceso_id')->whereNotNull('proceso_id')->whereNotNull('subproceso_id')
        ->orderBy('id', 'asc')->get();
        $terminoDefinicions=Macroprocesotermino::whereActivo(1)->whereMacroproceso_id($microproceso->macroproceso_id)->get();

        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['link' => "/macroprocesos/".$microproceso->macroproceso_id, 'name' => "Macroproceso"],
            ['link' => "/procesos/".$microproceso->proceso_id, 'name' => "Proceso"],
            ['link' => "/subprocesos/".$microproceso->subproceso_id, 'name' => "Subproceso"],
            ['name' => "Microproceo"]
        ];

        return view('sgc.microprocesos.show', compact('microproceso','requisitos','procesoMecanismos','indicadores','terminoDefinicions','breadcrumbs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Microproceso  $microproceso
     * @return \Illuminate\Http\Response
     */
    public function edit(Microproceso $microproceso)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Microproceso  $microproceso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Microproceso $microproceso)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Microproceso  $microproceso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Microproceso $microproceso)
    {
        //
    }
}
