<?php

namespace App\Http\Controllers\sgc;

use App\Http\Controllers\Controller;
use App\Models\Microproceso;
use App\Models\Subproceso;
use App\Models\Requisito;
use App\Models\Procesomecanismo;
use App\Models\Indicador;
use App\Models\Macroprocesotermino;
use Illuminate\Http\Request;

class SubprocesoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Subproceso"]
        ];

        return view('livewire.subprocesos.index', compact('pageConfigs', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subproceso  $subproceso
     * @return \Illuminate\Http\Response
     */
    public function show(Subproceso $subproceso)
    {
        $microprocesos = Microproceso::whereActivo(1)->whereSubproceso_id($subproceso->id)->orderBy('orden', 'asc')->get();
        $requisitos = Requisito::whereActivo(1)->whereSubproceso_id($subproceso->id)
            ->whereNotNull('macroproceso_id')->whereNotNull('proceso_id')->whereNull('microproceso_id')
            ->orderBy('id', 'asc')->get();
        $procesoMecanismos = Procesomecanismo::whereActivo(1)->whereMacroproceso_id($subproceso->id)
            ->whereNotNull('macroproceso_id')->whereNotNull('proceso_id')->whereNull('microproceso_id')
            ->orderBy('id', 'asc')->get();
        $indicadores = Indicador::whereActivo(1)->whereMacroproceso_id($subproceso->id)
            ->whereNotNull('macroproceso_id')->whereNotNull('proceso_id')->whereNull('microproceso_id')
            ->orderBy('id', 'asc')->get();
        $terminoDefinicions = Macroprocesotermino::whereActivo(1)->whereMacroproceso_id($subproceso->macroproceso_id)->get();

        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['link' => "/macroprocesos/" . $subproceso->macroproceso_id, 'name' => "Macroproceso"],
            ['link' => "/procesos/" . $subproceso->proceso_id, 'name' => "Proceso"],
            ['name' => "Subproceso"]
        ];

        return view('sgc.subprocesos.show', compact('subproceso', 'microprocesos', 'requisitos', 'procesoMecanismos', 'indicadores', 'terminoDefinicions', 'breadcrumbs', 'pageConfigs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subproceso  $subproceso
     * @return \Illuminate\Http\Response
     */
    public function edit(Subproceso $subproceso)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subproceso  $subproceso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subproceso $subproceso)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subproceso  $subproceso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subproceso $subproceso)
    {
        //
    }
    public function caracterizacion(Subproceso $subproceso)
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['link' => "/subprocesos/" . $subproceso->id, 'name' => "Subproceso"],
            ['name' => "Caracterización"]
        ];
        return view('sgc.subprocesos.caracterizacion', compact('subproceso', 'breadcrumbs', 'pageConfigs'));
    }
}
