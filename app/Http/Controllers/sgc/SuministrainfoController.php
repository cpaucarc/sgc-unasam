<?php

namespace App\Http\Controllers\sgc;

use App\Http\Controllers\Controller;
use App\Models\Suministrainfo;
use Illuminate\Http\Request;

class SuministrainfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Suministrainfo  $suministrainfo
     * @return \Illuminate\Http\Response
     */
    public function show(Suministrainfo $suministrainfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Suministrainfo  $suministrainfo
     * @return \Illuminate\Http\Response
     */
    public function edit(Suministrainfo $suministrainfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Suministrainfo  $suministrainfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Suministrainfo $suministrainfo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Suministrainfo  $suministrainfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Suministrainfo $suministrainfo)
    {
        //
    }
}
