<?php

namespace App\Http\Controllers\sgc;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\IndicadorRestriccion;
use App\Models\InteresadoIndicador;
use App\Models\Macroproceso;
use App\Models\Medicion;
use App\Models\Meta;
use App\Models\Proceso;
use App\Models\Requisito;
use App\Models\Procesomecanismo;
use App\Models\Indicador;
use App\Models\Macroprocesotermino;
use App\Models\Suministrainfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MacroprocesoController extends Controller
{
    public $data, $objetivo, $definicion, $selected_id;
    public $updateMode = false;
    public $open = false;

    public function index()
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Macroprocesos"]
        ];
        return view('livewire.macroprocesos.index', compact('pageConfigs', 'breadcrumbs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Macroproceso  $macroproceso
     * @return \Illuminate\Http\Response
     */
    public function show(Macroproceso $macroproceso)
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];

        $procesos = Proceso::whereActivo(1)->whereMacroproceso_id($macroproceso->id)->orderBy('orden', 'asc')->get();
        $requisitos = Requisito::whereActivo(1)->whereMacroproceso_id($macroproceso->id)
            ->whereNull('proceso_id')->whereNull('subproceso_id')->whereNull('microproceso_id')
            ->orderBy('id', 'asc')->get();
        $procesoMecanismos = Procesomecanismo::whereActivo(1)->whereMacroproceso_id($macroproceso->id)
            ->whereNull('proceso_id')->whereNull('subproceso_id')->whereNull('microproceso_id')
            ->orderBy('id', 'asc')->get();
        $indicadores = Indicador::whereActivo(1)->whereMacroproceso_id($macroproceso->id)
            ->whereNull('proceso_id')->whereNull('subproceso_id')->whereNull('microproceso_id')
            ->orderBy('id', 'asc')->get();
        $terminoDefinicions = Macroprocesotermino::whereActivo(1)->whereMacroproceso_id($macroproceso->id)->get();

        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Macroproceso"]
        ];
        return view('sgc.macroprocesos.show', compact('macroproceso', 'procesos', 'requisitos', 'procesoMecanismos', 'indicadores', 'terminoDefinicions', 'breadcrumbs', 'pageConfigs'));
    }


    public function caracterizacion(Macroproceso $macroproceso)
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['link' => "/macroprocesos/" . $macroproceso->id, 'name' => "Macroproceso"],
            ['name' => "Caracterización"]
        ];

        //$this->authorize('view', $macroproceso); 

        return view('sgc.macroprocesos.caracterizacion', compact('macroproceso', 'breadcrumbs', 'pageConfigs'));
    }

    public function ficha(Macroproceso $macroproceso)
    {
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['link' => "/macroprocesos/" . $macroproceso->id, 'name' => "Macroproceso"],
            ['name' => "Caracterización"]
        ];
        return view('sgc.macroprocesos.caracterizacion', compact('macroproceso', 'breadcrumbs'));
    }


    public function fichaCaracterizacion()
    {
        //$procesos = Proceso::whereActivo(1)->whereMacroproceso_id($macroproceso->id)->orderBy('orden', 'asc')->get();
        //return view('sgc.macroprocesos.show', compact('macroproceso', 'procesos'));
        return view('sgc.macroprocesos.fcaracterizacion');
    }

    public function fichaCaracterizacionProcesos()
    {
        //$procesos = Proceso::whereActivo(1)->whereMacroproceso_id($macroproceso->id)->orderBy('orden', 'asc')->get();
        //return view('sgc.macroprocesos.show', compact('macroproceso', 'procesos'));
        return view('sgc.macroprocesos.ficha-caracterizacion');
    }

    public function fichaIndicador($id)
    {
        //dd($id);
        $responsables = InteresadoIndicador::select(DB::raw('areas.id, areas.nombre'))
            ->Join('interesados', 'indicador_interesado.interesado_id', '=', 'interesados.id')
            ->Join('areas', 'interesados.area_id', '=', 'areas.id')
            ->where('indicador_interesado.activo', '=', 1)
            ->where('param_tiporesponsable', '=', '11002')
            ->orderBy('areas.nombre')->get();

        $responsables_analisis = InteresadoIndicador::select(DB::raw('areas.id, areas.nombre'))
            ->Join('interesados', 'indicador_interesado.interesado_id', '=', 'interesados.id')
            ->Join('areas', 'interesados.area_id', '=', 'areas.id')
            ->where('indicador_interesado.activo', '=', 1)
            ->where('param_tiporesponsable', '=', '11003')
            ->orderBy('areas.nombre')->get();

        $responsable_usuarios = InteresadoIndicador::select(DB::raw('areas.id, areas.nombre'))
            ->Join('interesados', 'indicador_interesado.interesado_id', '=', 'interesados.id')
            ->Join('areas', 'interesados.area_id', '=', 'areas.id')
            ->where('indicador_interesado.activo', '=', 1)
            ->where('param_tiporesponsable', '=', '11005')
            ->orderBy('areas.nombre')->get();


        $ficha = Indicador::findOrFail($id);

        $suministro_proceso = Suministrainfo::select(DB::raw('suministrainfos.id, procesos.nombre, procesos.codigo'))
            ->Join('procesos', 'suministrainfos.proceso_id', '=', 'procesos.id')
            ->where('suministrainfos.indicador_id', '=', $id)
            ->orderBy('procesos.nombre')->get();

        $suministro_area = InteresadoIndicador::select(DB::raw('indicador_interesado.indicador_id, areas.nombre, areas.abreviatura'))
            ->Join('interesados', 'indicador_interesado.interesado_id', '=', 'interesados.id')
            ->Join('areas', 'interesados.area_id', '=', 'areas.id')
            ->where('indicador_interesado.indicador_id', '=', $id)
            ->orderBy('areas.nombre')->get();

        $restricciones = IndicadorRestriccion::select(DB::raw('indicador_restriccion.id, indicador_restriccion.nombre, indicador_restriccion.indicador_id,
                    indicador_restriccion.restriccion_id, restriccions.restriccion rNombre'))
            ->join('restriccions', 'indicador_restriccion.indicador_id', '=', 'restriccions.id')
            ->where('indicador_restriccion.indicador_id', '=', $id)
            ->get();

        $metas = Meta::where('metas.indicador_id', '=', $id)->get();

        $mediciones = Medicion::select(DB::raw('medicions.id,
                    (SELECT detalle FROM detalles d1 WHERE d1.id = medicions.param_und_medida) as paraUnidadMedida,
                    (SELECT detalle FROM detalles d1 WHERE d1.id = medicions.param_frec_medicion) as paraFrecMedicion,
                    (SELECT detalle FROM detalles d1 WHERE d1.id = medicions.param_frec_reporte) as paraFrecReporte,
                    (SELECT detalle FROM detalles d1 WHERE d1.id = medicions.param_frec_revision) as paraFrecRevision,
                    anios.anio as aAnio'))
            ->join('anios', 'medicions.anio_id', '=', 'anios.id')
            ->where('medicions.indicador_id', '=', $id)
            ->get();

        $this->selected_id = $id;
        $this->objetivo = $ficha->objetivo;
        $this->definicion = $ficha->definicion;

        return view('sgc.macroprocesos.ficha-indicador', compact(
            'responsables',
            'responsables_analisis',
            'responsable_usuarios',
            'ficha',
            'suministro_proceso',
            'suministro_area',
            'restricciones',
            'metas',
            'mediciones'
        ));
    }

    public function openModal123()
    {
        $this->open = true;
        $this->emit('obtenerVinculos');
    }

    public function edit(Macroproceso $macroproceso)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Macroproceso  $macroproceso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Macroproceso $macroproceso)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Macroproceso  $macroproceso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Macroproceso $macroproceso)
    {
        //
    }

    public function listar(Request $request)
    {
        if ($request->ajax()) {
            return Macroproceso::whereActivo(1)->get();
        } else {
            return redirect('/home');
        }
    }
}
