<?php

namespace App\Http\Controllers\sgc;

use App\Http\Controllers\Controller;
use App\Models\Vinculacion;
use Illuminate\Http\Request;

class VinculacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vinculacion  $vinculacion
     * @return \Illuminate\Http\Response
     */
    public function show(Vinculacion $vinculacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vinculacion  $vinculacion
     * @return \Illuminate\Http\Response
     */
    public function edit(Vinculacion $vinculacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vinculacion  $vinculacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vinculacion $vinculacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vinculacion  $vinculacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vinculacion $vinculacion)
    {
        //
    }
}
