<?php

namespace App\Http\Controllers\sgc;

use App\Http\Controllers\Controller;
use App\Models\Documentodetalle;
use Illuminate\Http\Request;

class DocumentodetalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Documentodetalle  $documentodetalle
     * @return \Illuminate\Http\Response
     */
    public function show(Documentodetalle $documentodetalle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Documentodetalle  $documentodetalle
     * @return \Illuminate\Http\Response
     */
    public function edit(Documentodetalle $documentodetalle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Documentodetalle  $documentodetalle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Documentodetalle $documentodetalle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Documentodetalle  $documentodetalle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Documentodetalle $documentodetalle)
    {
        //
    }
}
