<?php

namespace App\Http\Controllers;

use App\Models\Macroproceso;
use App\Models\Proceso;
use App\Models\Indicador;

use App\Exports\CaracterizacionExport;
use App\Exports\CaracterizacionMacroprocesoExport;
use App\Exports\CaracterizacionProcesoExport;
use App\Exports\MacroprocesoExport;
use App\Exports\IndicadorExport;

use App\Exports\Reportes\PorProcesosExport;
use App\Exports\Reportes\PorTipoExport;
use App\Exports\Reportes\PorOrganicasExport;
use App\Exports\Reportes\IndicadoresExport;
use Illuminate\Http\Request;

use Maatwebsite\Excel\Facades\Excel;

class ExportsController extends Controller
{
    public function porMacroproceso(Macroproceso $macroproceso)
    {
        return Excel::download(new MacroprocesoExport($macroproceso), 'Macroproceso indicadores - ' . $macroproceso->nombre_completo . '.xlsx');
    }
    public function porIndicador(Indicador $indicador)
    {
        return Excel::download(new IndicadorExport($indicador), 'Indicador - ' . $indicador->parametros['codigo'] . '.xlsx');
    }
    public function previstaPorIndicador(Indicador $indicador)
    {
        $longitud_total = 48;
        return view('sgc.indicadors.excelwrapper', compact('indicador', 'longitud_total'));
    }

    public function porCaracterizacion(Macroproceso $macroproceso)
    {
        return Excel::download(new CaracterizacionExport($macroproceso), 'Caracterizacion - ' . $macroproceso->nombre_completo . '.xlsx');
    }
    public function porCaracterizacionMacroproceso(Macroproceso $macroproceso)
    {
        return Excel::download(new CaracterizacionMacroprocesoExport($macroproceso), 'Caracterizacion macroproceso - ' . $macroproceso->nombre_completo . '.xlsx');
    }
    public function porCaracterizacionProceso(Proceso $proceso)
    {
        return Excel::download(new CaracterizacionProcesoExport($proceso), 'Caracterizacion proceso - ' . $proceso->nombre_completo . '.xlsx');
    }
    public function previstaPorCaracterizacion(Macroproceso $macroproceso)
    {
        $pageConfigs = [
            'pageHeader' => true,
            'layoutWidth' => 'full',
            'verticalMenuNavbarType' => 'sticky',
            'footerType' => 'sticky'
        ];
        $breadcrumbs = [
            ['link' => "/home", 'name' => "Home"],
            ['name' => "Indicadores"]
        ];

        $longitud_total = 100;
        return view('sgc.macroprocesos.caracterizacion.wrapper', compact('macroproceso', 'longitud_total', 'pageConfigs', 'breadcrumbs'));
    }

    public function porProcesos()
    {
        return Excel::download(new PorProcesosExport(), 'PorProceso.xlsx');
    }
    public function porTipo()
    {
        return Excel::download(new PorTipoExport(), 'PorTipo.xlsx');
    }
    public function porOrganicas()
    {
        return Excel::download(new PorOrganicasExport(), 'PorOrganicas.xlsx');
    }
    public function porIndicadoresGeneral()
    {
        return Excel::download(new IndicadoresExport(), 'Indicadores.xlsx');
    }
}
