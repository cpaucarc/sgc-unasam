<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RevisaAsignacionDeUsuario
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->user()->asignacion) :
            if(!$request->user()->hasRole($request->user()->asignacion->rol->name)):
                $request->user()->syncRoles([$request->user()->asignacion->rol->name]);
            endif;
            return $next($request);
        endif;

        if ($request->user()->asignaciones->count() > 0) :
            $request->user()->asignacion_id = $request->user()->asignaciones->first()->id;
            $request->user()->save();

            $request->user()->syncRoles([$request->user()->asignaciones->first()->rol->name]);
            return $next($request);
        endif;

        return abort(403, 'Este usuario no tiene ningún rol asignado');
    }
}
