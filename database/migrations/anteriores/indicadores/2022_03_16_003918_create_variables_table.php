<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variables', function (Blueprint $table) {
            $table->id();
            //    $table->integer('indicador_id');
            $table->foreignId('formula_id')->constrained();
            $table->string('variable');
            $table->string('nombre');
           // $table->unsignedFloat('peso');
            $table->string('tipo');
            $table->bigInteger('created_user')->nullable();
            $table->bigInteger('updated_user')->nullable();
            $table->bigInteger('deleted_user')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //  $table->foreign('indicador_id')->references('id')->on('indicadors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variables');
    }
}
