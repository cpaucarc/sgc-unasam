<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTieneApiFieldToIndicadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('macroprocesos', function (Blueprint $table) {
            $table->boolean('tiene_api')->after('activo')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('macroprocesos', function (Blueprint $table) {
            $table->dropColumn('tiene_api');
        });
    }
}
