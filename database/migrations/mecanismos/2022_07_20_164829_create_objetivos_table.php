<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjetivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objetivos', function (Blueprint $table) {
            $table->id();
            $table->integer('mecanismo_id');
            $table->integer('relacion_id');
            $table->string('relacion_type');
            $table->string('nombre', 1000);
            $table->bigInteger('created_user')->nullable();
            $table->bigInteger('updated_user')->nullable();
            $table->bigInteger('deleted_user')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('mecanismo_id')->references('id')->on('mecanismos');
        });
        Schema::dropIfExists('evidencias');
        Schema::create('evidencias', function (Blueprint $table) {
            $table->id();
            $table->foreignId('objetivo_id')->constrained();
            $table->integer('documento_id');
            $table->string('nombre', 1000);
            $table->bigInteger('created_user')->nullable();
            $table->bigInteger('updated_user')->nullable();
            $table->bigInteger('deleted_user')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('documento_id')->references('id')->on('documentos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evidencias');
        Schema::create('evidencias', function (Blueprint $table) {
            $table->id();
            $table->integer('procesomecanismo_id');
            $table->integer('documento_id');
            $table->string('nombre', 1000);
            $table->bigInteger('created_user')->nullable();
            $table->bigInteger('updated_user')->nullable();
            $table->bigInteger('deleted_user')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::dropIfExists('objetivos');
    }
}
