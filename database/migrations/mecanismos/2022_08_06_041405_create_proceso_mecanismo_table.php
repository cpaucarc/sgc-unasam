<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcesoMecanismoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asignacion_mecanismos', function (Blueprint $table) {
            $table->id();
            $table->integer('mecanismo_id');
            $table->integer('relacion_id');
            $table->string('relacion_type');
            $table->timestamps();
            $table->bigInteger('created_user')->nullable();
            $table->bigInteger('updated_user')->nullable();
            $table->bigInteger('deleted_user')->nullable();
            $table->softDeletes();
            $table->foreign('mecanismo_id')->references('id')->on('mecanismos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asignacion_mecanismos');
    }
}
