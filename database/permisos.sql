/*
 Navicat Premium Data Transfer

 Source Server         : Localhost - MySQL
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : bdsgcunasam

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 13/04/2022 17:12:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES (1, 'General', NULL, NULL);


-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `categories_group_id_foreign`(`group_id`) USING BTREE,
  CONSTRAINT `categories_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 1, 'Actividad', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (2, 1, 'Anio', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (3, 1, 'Archivo', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (4, 1, 'Area', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (5, 1, 'Asignacion', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (6, 1, 'Cargo', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (7, 1, 'Detalle', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (8, 1, 'Documento', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (9, 1, 'Entrada', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (10, 1, 'Escala', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (11, 1, 'Evaluacion', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (12, 1, 'Evidencia', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (13, 1, 'Formula', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (14, 1, 'Indicador', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (15, 1, 'Interesado', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (16, 1, 'Macroproceso', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (17, 1, 'Mecanismo', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (18, 1, 'Medicion', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (19, 1, 'Meta', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (20, 1, 'Microproceso', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (21, 1, 'Migration', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (22, 1, 'Parametro', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (23, 1, 'Permission', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (24, 1, 'Persona', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (25, 1, 'Procesomecanismo', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (26, 1, 'Proceso', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (27, 1, 'Recurso', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (28, 1, 'Requisito', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (29, 1, 'Restriccion', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (30, 1, 'Resultado', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (31, 1, 'Riesgo', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (32, 1, 'Role', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (33, 1, 'Session', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (34, 1, 'Subproceso', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (35, 1, 'Suministrainfo', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (36, 1, 'Team', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (37, 1, 'Termino', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (38, 1, 'Trabajador', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (39, 1, 'User', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (40, 1, 'Variable', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (41, 1, 'Version', '2022-04-13 04:25:02', NULL);
INSERT INTO `categories` VALUES (42, 1, 'Vinculacion', '2022-04-13 04:25:02', NULL);


-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` bigint UNSIGNED NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `permissions_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `permissions_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 337 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES (1, 1, 'actividads.all', 'Controlar actividads', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (2, 1, 'actividads.index', 'Listar actividads', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (3, 1, 'actividads.create', 'Crear actividads', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (4, 1, 'actividads.update', 'Modificar actividads', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (5, 1, 'actividads.show', 'Ver actividads', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (6, 1, 'actividads.delete', 'Eliminar actividads', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (7, 1, 'actividads.restore', 'Restaurar actividads', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (8, 1, 'actividads.destroy', 'Destruir actividads', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (9, 2, 'anios.all', 'Controlar anios', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (10, 2, 'anios.index', 'Listar anios', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (11, 2, 'anios.create', 'Crear anios', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (12, 2, 'anios.update', 'Modificar anios', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (13, 2, 'anios.show', 'Ver anios', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (14, 2, 'anios.delete', 'Eliminar anios', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (15, 2, 'anios.restore', 'Restaurar anios', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (16, 2, 'anios.destroy', 'Destruir anios', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (17, 3, 'archivos.all', 'Controlar archivos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (18, 3, 'archivos.index', 'Listar archivos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (19, 3, 'archivos.create', 'Crear archivos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (20, 3, 'archivos.update', 'Modificar archivos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (21, 3, 'archivos.show', 'Ver archivos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (22, 3, 'archivos.delete', 'Eliminar archivos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (23, 3, 'archivos.restore', 'Restaurar archivos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (24, 3, 'archivos.destroy', 'Destruir archivos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (25, 4, 'areas.all', 'Controlar areas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (26, 4, 'areas.index', 'Listar areas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (27, 4, 'areas.create', 'Crear areas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (28, 4, 'areas.update', 'Modificar areas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (29, 4, 'areas.show', 'Ver areas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (30, 4, 'areas.delete', 'Eliminar areas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (31, 4, 'areas.restore', 'Restaurar areas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (32, 4, 'areas.destroy', 'Destruir areas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (33, 5, 'asignacions.all', 'Controlar asignacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (34, 5, 'asignacions.index', 'Listar asignacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (35, 5, 'asignacions.create', 'Crear asignacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (36, 5, 'asignacions.update', 'Modificar asignacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (37, 5, 'asignacions.show', 'Ver asignacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (38, 5, 'asignacions.delete', 'Eliminar asignacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (39, 5, 'asignacions.restore', 'Restaurar asignacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (40, 5, 'asignacions.destroy', 'Destruir asignacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (41, 6, 'cargos.all', 'Controlar cargos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (42, 6, 'cargos.index', 'Listar cargos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (43, 6, 'cargos.create', 'Crear cargos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (44, 6, 'cargos.update', 'Modificar cargos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (45, 6, 'cargos.show', 'Ver cargos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (46, 6, 'cargos.delete', 'Eliminar cargos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (47, 6, 'cargos.restore', 'Restaurar cargos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (48, 6, 'cargos.destroy', 'Destruir cargos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (49, 7, 'detalles.all', 'Controlar detalles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (50, 7, 'detalles.index', 'Listar detalles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (51, 7, 'detalles.create', 'Crear detalles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (52, 7, 'detalles.update', 'Modificar detalles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (53, 7, 'detalles.show', 'Ver detalles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (54, 7, 'detalles.delete', 'Eliminar detalles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (55, 7, 'detalles.restore', 'Restaurar detalles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (56, 7, 'detalles.destroy', 'Destruir detalles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (57, 8, 'documentos.all', 'Controlar documentos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (58, 8, 'documentos.index', 'Listar documentos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (59, 8, 'documentos.create', 'Crear documentos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (60, 8, 'documentos.update', 'Modificar documentos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (61, 8, 'documentos.show', 'Ver documentos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (62, 8, 'documentos.delete', 'Eliminar documentos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (63, 8, 'documentos.restore', 'Restaurar documentos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (64, 8, 'documentos.destroy', 'Destruir documentos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (65, 9, 'entradas.all', 'Controlar entradas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (66, 9, 'entradas.index', 'Listar entradas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (67, 9, 'entradas.create', 'Crear entradas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (68, 9, 'entradas.update', 'Modificar entradas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (69, 9, 'entradas.show', 'Ver entradas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (70, 9, 'entradas.delete', 'Eliminar entradas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (71, 9, 'entradas.restore', 'Restaurar entradas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (72, 9, 'entradas.destroy', 'Destruir entradas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (73, 10, 'escalas.all', 'Controlar escalas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (74, 10, 'escalas.index', 'Listar escalas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (75, 10, 'escalas.create', 'Crear escalas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (76, 10, 'escalas.update', 'Modificar escalas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (77, 10, 'escalas.show', 'Ver escalas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (78, 10, 'escalas.delete', 'Eliminar escalas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (79, 10, 'escalas.restore', 'Restaurar escalas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (80, 10, 'escalas.destroy', 'Destruir escalas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (81, 11, 'evaluacions.all', 'Controlar evaluacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (82, 11, 'evaluacions.index', 'Listar evaluacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (83, 11, 'evaluacions.create', 'Crear evaluacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (84, 11, 'evaluacions.update', 'Modificar evaluacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (85, 11, 'evaluacions.show', 'Ver evaluacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (86, 11, 'evaluacions.delete', 'Eliminar evaluacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (87, 11, 'evaluacions.restore', 'Restaurar evaluacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (88, 11, 'evaluacions.destroy', 'Destruir evaluacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (89, 12, 'evidencias.all', 'Controlar evidencias', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (90, 12, 'evidencias.index', 'Listar evidencias', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (91, 12, 'evidencias.create', 'Crear evidencias', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (92, 12, 'evidencias.update', 'Modificar evidencias', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (93, 12, 'evidencias.show', 'Ver evidencias', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (94, 12, 'evidencias.delete', 'Eliminar evidencias', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (95, 12, 'evidencias.restore', 'Restaurar evidencias', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (96, 12, 'evidencias.destroy', 'Destruir evidencias', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (97, 13, 'formulas.all', 'Controlar formulas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (98, 13, 'formulas.index', 'Listar formulas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (99, 13, 'formulas.create', 'Crear formulas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (100, 13, 'formulas.update', 'Modificar formulas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (101, 13, 'formulas.show', 'Ver formulas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (102, 13, 'formulas.delete', 'Eliminar formulas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (103, 13, 'formulas.restore', 'Restaurar formulas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (104, 13, 'formulas.destroy', 'Destruir formulas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (105, 14, 'indicadors.all', 'Controlar indicadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (106, 14, 'indicadors.index', 'Listar indicadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (107, 14, 'indicadors.create', 'Crear indicadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (108, 14, 'indicadors.update', 'Modificar indicadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (109, 14, 'indicadors.show', 'Ver indicadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (110, 14, 'indicadors.delete', 'Eliminar indicadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (111, 14, 'indicadors.restore', 'Restaurar indicadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (112, 14, 'indicadors.destroy', 'Destruir indicadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (113, 15, 'interesados.all', 'Controlar interesados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (114, 15, 'interesados.index', 'Listar interesados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (115, 15, 'interesados.create', 'Crear interesados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (116, 15, 'interesados.update', 'Modificar interesados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (117, 15, 'interesados.show', 'Ver interesados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (118, 15, 'interesados.delete', 'Eliminar interesados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (119, 15, 'interesados.restore', 'Restaurar interesados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (120, 15, 'interesados.destroy', 'Destruir interesados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (121, 16, 'macroprocesos.all', 'Controlar macroprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (122, 16, 'macroprocesos.index', 'Listar macroprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (123, 16, 'macroprocesos.create', 'Crear macroprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (124, 16, 'macroprocesos.update', 'Modificar macroprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (125, 16, 'macroprocesos.show', 'Ver macroprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (126, 16, 'macroprocesos.delete', 'Eliminar macroprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (127, 16, 'macroprocesos.restore', 'Restaurar macroprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (128, 16, 'macroprocesos.destroy', 'Destruir macroprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (129, 17, 'mecanismos.all', 'Controlar mecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (130, 17, 'mecanismos.index', 'Listar mecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (131, 17, 'mecanismos.create', 'Crear mecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (132, 17, 'mecanismos.update', 'Modificar mecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (133, 17, 'mecanismos.show', 'Ver mecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (134, 17, 'mecanismos.delete', 'Eliminar mecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (135, 17, 'mecanismos.restore', 'Restaurar mecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (136, 17, 'mecanismos.destroy', 'Destruir mecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (137, 18, 'medicions.all', 'Controlar medicions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (138, 18, 'medicions.index', 'Listar medicions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (139, 18, 'medicions.create', 'Crear medicions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (140, 18, 'medicions.update', 'Modificar medicions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (141, 18, 'medicions.show', 'Ver medicions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (142, 18, 'medicions.delete', 'Eliminar medicions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (143, 18, 'medicions.restore', 'Restaurar medicions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (144, 18, 'medicions.destroy', 'Destruir medicions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (145, 19, 'metas.all', 'Controlar metas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (146, 19, 'metas.index', 'Listar metas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (147, 19, 'metas.create', 'Crear metas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (148, 19, 'metas.update', 'Modificar metas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (149, 19, 'metas.show', 'Ver metas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (150, 19, 'metas.delete', 'Eliminar metas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (151, 19, 'metas.restore', 'Restaurar metas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (152, 19, 'metas.destroy', 'Destruir metas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (153, 20, 'microprocesos.all', 'Controlar microprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (154, 20, 'microprocesos.index', 'Listar microprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (155, 20, 'microprocesos.create', 'Crear microprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (156, 20, 'microprocesos.update', 'Modificar microprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (157, 20, 'microprocesos.show', 'Ver microprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (158, 20, 'microprocesos.delete', 'Eliminar microprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (159, 20, 'microprocesos.restore', 'Restaurar microprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (160, 20, 'microprocesos.destroy', 'Destruir microprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (161, 21, 'migrations.all', 'Controlar migrations', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (162, 21, 'migrations.index', 'Listar migrations', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (163, 21, 'migrations.create', 'Crear migrations', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (164, 21, 'migrations.update', 'Modificar migrations', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (165, 21, 'migrations.show', 'Ver migrations', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (166, 21, 'migrations.delete', 'Eliminar migrations', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (167, 21, 'migrations.restore', 'Restaurar migrations', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (168, 21, 'migrations.destroy', 'Destruir migrations', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (169, 22, 'parametros.all', 'Controlar parametros', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (170, 22, 'parametros.index', 'Listar parametros', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (171, 22, 'parametros.create', 'Crear parametros', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (172, 22, 'parametros.update', 'Modificar parametros', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (173, 22, 'parametros.show', 'Ver parametros', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (174, 22, 'parametros.delete', 'Eliminar parametros', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (175, 22, 'parametros.restore', 'Restaurar parametros', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (176, 22, 'parametros.destroy', 'Destruir parametros', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (177, 23, 'permissions.all', 'Controlar permissions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (178, 23, 'permissions.index', 'Listar permissions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (179, 23, 'permissions.create', 'Crear permissions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (180, 23, 'permissions.update', 'Modificar permissions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (181, 23, 'permissions.show', 'Ver permissions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (182, 23, 'permissions.delete', 'Eliminar permissions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (183, 23, 'permissions.restore', 'Restaurar permissions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (184, 23, 'permissions.destroy', 'Destruir permissions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (185, 24, 'personas.all', 'Controlar personas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (186, 24, 'personas.index', 'Listar personas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (187, 24, 'personas.create', 'Crear personas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (188, 24, 'personas.update', 'Modificar personas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (189, 24, 'personas.show', 'Ver personas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (190, 24, 'personas.delete', 'Eliminar personas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (191, 24, 'personas.restore', 'Restaurar personas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (192, 24, 'personas.destroy', 'Destruir personas', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (193, 25, 'procesomecanismos.all', 'Controlar procesomecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (194, 25, 'procesomecanismos.index', 'Listar procesomecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (195, 25, 'procesomecanismos.create', 'Crear procesomecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (196, 25, 'procesomecanismos.update', 'Modificar procesomecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (197, 25, 'procesomecanismos.show', 'Ver procesomecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (198, 25, 'procesomecanismos.delete', 'Eliminar procesomecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (199, 25, 'procesomecanismos.restore', 'Restaurar procesomecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (200, 25, 'procesomecanismos.destroy', 'Destruir procesomecanismos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (201, 26, 'procesos.all', 'Controlar procesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (202, 26, 'procesos.index', 'Listar procesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (203, 26, 'procesos.create', 'Crear procesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (204, 26, 'procesos.update', 'Modificar procesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (205, 26, 'procesos.show', 'Ver procesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (206, 26, 'procesos.delete', 'Eliminar procesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (207, 26, 'procesos.restore', 'Restaurar procesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (208, 26, 'procesos.destroy', 'Destruir procesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (209, 27, 'recursos.all', 'Controlar recursos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (210, 27, 'recursos.index', 'Listar recursos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (211, 27, 'recursos.create', 'Crear recursos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (212, 27, 'recursos.update', 'Modificar recursos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (213, 27, 'recursos.show', 'Ver recursos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (214, 27, 'recursos.delete', 'Eliminar recursos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (215, 27, 'recursos.restore', 'Restaurar recursos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (216, 27, 'recursos.destroy', 'Destruir recursos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (217, 28, 'requisitos.all', 'Controlar requisitos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (218, 28, 'requisitos.index', 'Listar requisitos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (219, 28, 'requisitos.create', 'Crear requisitos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (220, 28, 'requisitos.update', 'Modificar requisitos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (221, 28, 'requisitos.show', 'Ver requisitos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (222, 28, 'requisitos.delete', 'Eliminar requisitos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (223, 28, 'requisitos.restore', 'Restaurar requisitos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (224, 28, 'requisitos.destroy', 'Destruir requisitos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (225, 29, 'restriccions.all', 'Controlar restriccions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (226, 29, 'restriccions.index', 'Listar restriccions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (227, 29, 'restriccions.create', 'Crear restriccions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (228, 29, 'restriccions.update', 'Modificar restriccions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (229, 29, 'restriccions.show', 'Ver restriccions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (230, 29, 'restriccions.delete', 'Eliminar restriccions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (231, 29, 'restriccions.restore', 'Restaurar restriccions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (232, 29, 'restriccions.destroy', 'Destruir restriccions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (233, 30, 'resultados.all', 'Controlar resultados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (234, 30, 'resultados.index', 'Listar resultados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (235, 30, 'resultados.create', 'Crear resultados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (236, 30, 'resultados.update', 'Modificar resultados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (237, 30, 'resultados.show', 'Ver resultados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (238, 30, 'resultados.delete', 'Eliminar resultados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (239, 30, 'resultados.restore', 'Restaurar resultados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (240, 30, 'resultados.destroy', 'Destruir resultados', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (241, 31, 'riesgos.all', 'Controlar riesgos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (242, 31, 'riesgos.index', 'Listar riesgos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (243, 31, 'riesgos.create', 'Crear riesgos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (244, 31, 'riesgos.update', 'Modificar riesgos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (245, 31, 'riesgos.show', 'Ver riesgos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (246, 31, 'riesgos.delete', 'Eliminar riesgos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (247, 31, 'riesgos.restore', 'Restaurar riesgos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (248, 31, 'riesgos.destroy', 'Destruir riesgos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (249, 32, 'roles.all', 'Controlar roles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (250, 32, 'roles.index', 'Listar roles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (251, 32, 'roles.create', 'Crear roles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (252, 32, 'roles.update', 'Modificar roles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (253, 32, 'roles.show', 'Ver roles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (254, 32, 'roles.delete', 'Eliminar roles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (255, 32, 'roles.restore', 'Restaurar roles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (256, 32, 'roles.destroy', 'Destruir roles', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (257, 33, 'sessions.all', 'Controlar sessions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (258, 33, 'sessions.index', 'Listar sessions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (259, 33, 'sessions.create', 'Crear sessions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (260, 33, 'sessions.update', 'Modificar sessions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (261, 33, 'sessions.show', 'Ver sessions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (262, 33, 'sessions.delete', 'Eliminar sessions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (263, 33, 'sessions.restore', 'Restaurar sessions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (264, 33, 'sessions.destroy', 'Destruir sessions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (265, 34, 'subprocesos.all', 'Controlar subprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (266, 34, 'subprocesos.index', 'Listar subprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (267, 34, 'subprocesos.create', 'Crear subprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (268, 34, 'subprocesos.update', 'Modificar subprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (269, 34, 'subprocesos.show', 'Ver subprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (270, 34, 'subprocesos.delete', 'Eliminar subprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (271, 34, 'subprocesos.restore', 'Restaurar subprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (272, 34, 'subprocesos.destroy', 'Destruir subprocesos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (273, 35, 'suministrainfos.all', 'Controlar suministrainfos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (274, 35, 'suministrainfos.index', 'Listar suministrainfos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (275, 35, 'suministrainfos.create', 'Crear suministrainfos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (276, 35, 'suministrainfos.update', 'Modificar suministrainfos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (277, 35, 'suministrainfos.show', 'Ver suministrainfos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (278, 35, 'suministrainfos.delete', 'Eliminar suministrainfos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (279, 35, 'suministrainfos.restore', 'Restaurar suministrainfos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (280, 35, 'suministrainfos.destroy', 'Destruir suministrainfos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (281, 36, 'teams.all', 'Controlar teams', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (282, 36, 'teams.index', 'Listar teams', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (283, 36, 'teams.create', 'Crear teams', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (284, 36, 'teams.update', 'Modificar teams', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (285, 36, 'teams.show', 'Ver teams', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (286, 36, 'teams.delete', 'Eliminar teams', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (287, 36, 'teams.restore', 'Restaurar teams', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (288, 36, 'teams.destroy', 'Destruir teams', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (289, 37, 'terminos.all', 'Controlar terminos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (290, 37, 'terminos.index', 'Listar terminos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (291, 37, 'terminos.create', 'Crear terminos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (292, 37, 'terminos.update', 'Modificar terminos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (293, 37, 'terminos.show', 'Ver terminos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (294, 37, 'terminos.delete', 'Eliminar terminos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (295, 37, 'terminos.restore', 'Restaurar terminos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (296, 37, 'terminos.destroy', 'Destruir terminos', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (297, 38, 'trabajadors.all', 'Controlar trabajadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (298, 38, 'trabajadors.index', 'Listar trabajadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (299, 38, 'trabajadors.create', 'Crear trabajadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (300, 38, 'trabajadors.update', 'Modificar trabajadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (301, 38, 'trabajadors.show', 'Ver trabajadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (302, 38, 'trabajadors.delete', 'Eliminar trabajadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (303, 38, 'trabajadors.restore', 'Restaurar trabajadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (304, 38, 'trabajadors.destroy', 'Destruir trabajadors', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (305, 39, 'users.all', 'Controlar users', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (306, 39, 'users.index', 'Listar users', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (307, 39, 'users.create', 'Crear users', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (308, 39, 'users.update', 'Modificar users', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (309, 39, 'users.show', 'Ver users', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (310, 39, 'users.delete', 'Eliminar users', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (311, 39, 'users.restore', 'Restaurar users', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (312, 39, 'users.destroy', 'Destruir users', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (313, 40, 'variables.all', 'Controlar variables', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (314, 40, 'variables.index', 'Listar variables', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (315, 40, 'variables.create', 'Crear variables', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (316, 40, 'variables.update', 'Modificar variables', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (317, 40, 'variables.show', 'Ver variables', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (318, 40, 'variables.delete', 'Eliminar variables', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (319, 40, 'variables.restore', 'Restaurar variables', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (320, 40, 'variables.destroy', 'Destruir variables', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (321, 41, 'versions.all', 'Controlar versions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (322, 41, 'versions.index', 'Listar versions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (323, 41, 'versions.create', 'Crear versions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (324, 41, 'versions.update', 'Modificar versions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (325, 41, 'versions.show', 'Ver versions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (326, 41, 'versions.delete', 'Eliminar versions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (327, 41, 'versions.restore', 'Restaurar versions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (328, 41, 'versions.destroy', 'Destruir versions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (329, 42, 'vinculacions.all', 'Controlar vinculacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (330, 42, 'vinculacions.index', 'Listar vinculacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (331, 42, 'vinculacions.create', 'Crear vinculacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (332, 42, 'vinculacions.update', 'Modificar vinculacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (333, 42, 'vinculacions.show', 'Ver vinculacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (334, 42, 'vinculacions.delete', 'Eliminar vinculacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (335, 42, 'vinculacions.restore', 'Restaurar vinculacions', 'web', NULL, NULL);
INSERT INTO `permissions` VALUES (336, 42, 'vinculacions.destroy', 'Destruir vinculacions', 'web', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
