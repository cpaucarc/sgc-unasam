
function nitificacion(tipo,mensaje,titulo, duracion) {
    var isRtl = $('html').attr('data-textdirection') === 'rtl';
    toastr[tipo](
        mensaje,
        titulo,
        {
            closeButton: true,
            tapToDismiss: false,
            closeDuration: duracion,
            rtl: isRtl
        }
      );    
}