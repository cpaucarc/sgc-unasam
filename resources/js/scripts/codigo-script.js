function asignarFecha(mifecha,formato,minDate) {
    $(mifecha).flatpickr({
        dateFormat: formato,
        minDate: minDate,  
        allowInput: true,
        allowInvalidPreload: true,
        locale: {
            firstDayOfWeek: 1,
            weekdays: {
                shorthand: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                longhand: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],         
            }, 
            months: {
                shorthand: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Оct', 'Nov', 'Dic'],
                longhand: ['Enero', 'Febreo', 'Мarzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            },
        }
    });	
}
function asignarSelect2(miSelect,width,placeholder) {
    $(miSelect).select2({
        dropdownParent: $(miSelect).parent(),
        width: width,
        placeholder: placeholder,
        language: {
            noResults: function() {
                return "No hay resultado";        
            },
            searching: function() {
                return "Buscando..";
            }
        }
    });
}
function nitificacion(tipo,mensaje,titulo, duracion) {
    var isRtl = $('html').attr('data-textdirection') === 'rtl';
    toastr[tipo](
        mensaje,
        titulo,
        {
            closeButton: true,
            tapToDismiss: false,
            closeDuration: duracion,
            rtl: isRtl
        }
      );    
}