<table class="table table-bordered">
    <thead>
        <tr>
            <th colspan="4" class="text-center">
                REPORTE DE INDICADORES POR TIPO DE INDICADOR
            </th>
        </tr>
        <tr>
            <th colspan="4"></th>
        </tr>
        <tr>
            <th> Tipo de indicador </th>
            <th class="text-center">Cantidad</th>
            <th class="text-center">Valores</th>
            <th class="text-center">Promedio</th>
        </tr>
    </thead>
    @foreach ($tipos as $tipo => $indicadores)
        <tbody x-data="{ expanded: true }">
            <tr class="table-active">
                <th x-on:click="expanded = !expanded"> {{ $tipo }} </th>
                <th x-on:click="expanded = !expanded" class="text-center"> {{ $indicadores->sum('conteo') }} </th>
                <th x-on:click="expanded = !expanded" class="text-center"> {{ $indicadores->sum('valores') }} </th>
                <th x-on:click="expanded = !expanded" class="text-center"> {{ $indicadores->avg('promedio') }} </th>
            </tr>
            @foreach ($indicadores as $indicador)
                <tr x-show="expanded">
                    <td>{{ $indicador->nombre }}</td>
                    <td class="text-center">{{ $indicador->conteo }}</td>
                    <td class="text-center">{{ $indicador->valores }}</td>
                    <td class="text-center">{{ $indicador->promedio }}</td>
                </tr>
            @endforeach
        </tbody>
    @endforeach
</table>
