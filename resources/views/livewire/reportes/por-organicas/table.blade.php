<table class="table table-bordered">
    <thead>
        <tr>
            <th colspan="5" class="text-center">
                REPORTE DE INDICADORES POR UNIDADES ORGÁNICAS
            </th>
        </tr>
        <tr>
            <th colspan="5"></th>
        </tr>
        <tr>
            <th> UNIDADES ORGÁNICAS/ INDICADORES </th>
            <th class="text-center">Cantidad</th>
            <th class="text-center">Valores</th>
            <th class="text-center">Promedio</th>
            <th class="text-center">TIPO DE INDICADOR</th>
        </tr>
    </thead>
    @foreach ($organicas as $unidad => $indicadores)
        <tbody x-data="{ expanded: true }">
            <tr class="table-active">
                <th x-on:click="expanded = !expanded"> {{ $unidad }} </th>
                <th x-on:click="expanded = !expanded" class="text-center"> {{ $indicadores->sum('conteo') }} </th>
                <th x-on:click="expanded = !expanded" class="text-center"> {{ $indicadores->sum('valores') }} </th>
                <th x-on:click="expanded = !expanded" class="text-center"> {{ $indicadores->avg('promedio') }} </th>
                <th x-on:click="expanded = !expanded" class="text-center"> </th>
            </tr>
            @foreach ($indicadores as $indicador)
                <tr x-show="expanded">
                    <td>{{ $indicador->nombre }}</td>
                    <td class="text-center">{{ $indicador->conteo }}</td>
                    <td class="text-center">{{ $indicador->valores }}</td>
                    <td class="text-center">{{ $indicador->promedio }}</td>
                    <td class="text-center">{{ $indicador->tipo }}</td>
                </tr>
            @endforeach
        </tbody>
    @endforeach
</table>
