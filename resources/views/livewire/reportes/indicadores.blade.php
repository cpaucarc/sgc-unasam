<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body table-responsive">
                <div class="col-12 text-end">
                    <a href="/reportes/por_indicadores/excel" class="btn btn-outline-primary round waves-effect">
                        Descargar
                    </a>
                </div>
                <hr>
                @include('livewire.reportes.indicadores.table')
            </div>
        </div>
    </div>
</div>
