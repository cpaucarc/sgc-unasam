<table class="table table-bordered">
    <thead>
        <tr>
            <th colspan="15" class="text-center">
                REPORTE DE INDICADORES POR INDICADORES
            </th>
        </tr>
        <tr>
            <th colspan="15"></th>
        </tr>
        <tr>
            <th>N°</th>
            <th>Código de indicador</th>
            <th>Macroproceso</th>
            <th>Nombre del indicador</th>
            <th>Procesos</th>
            <th>Numerador</th>
            <th>Denominador</th>
            <th>Tipo</th>
            <th>Meta</th>
            <th>Unidad</th>
            <th>Frecuencia de medición</th>
            <th>Responsable de medición</th>
            <th>Valor</th>
            <th>Brecha</th>
            <th>Observaciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($indicadores as $indicador)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{$indicador->codigo}}</td>
                <td>{{$indicador->macroproceso->nombre_completo}}</td>
                <td>{{$indicador->nombre}}</td>
                <td>{{$indicador->proceso?$indicador->proceso->nombre_completo:' - '}}</td>
                <td>{{ $indicador->formula ? ($indicador->formula->formula_numerador_simple) : ' - ' }}</td>
                <td>{{ $indicador->formula ? ($indicador->formula->formula_denominador_simple) : ' - ' }}</td>
                <td>{{ $indicador->detalle->abreviatura }}</td>
                <td>{{ $indicador->meta?$indicador->meta->meta:'-' }}</td>
                <td>{{ $indicador->medicion ? $indicador->medicion->detallemedida->detalle : '' }}</td>
                <td>{{ $indicador->medicion ? $indicador->medicion->detallemedicion->detalle : '' }}</td>
                <td>{{ $indicador->responsables->pluck('nombre')->implode(', ') }}</td>
                <td>{{ $indicador->resultados->avg('valor_indicador') }}</td>
                <td>{{ $indicador->meta?($indicador->meta->meta - $indicador->resultados->avg('valor_indicador')):'-' }}</td>
                <td></td>
            </tr>
        @endforeach
    </tbody>
</table>
