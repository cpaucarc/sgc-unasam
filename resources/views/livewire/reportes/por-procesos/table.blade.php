<table class="table table-bordered">
    <thead>
        <tr>
            <th colspan="4" class="text-center">
                REPORTE DE INDICADORES POR PROCESOS
            </th>
        </tr>
        <tr>
            <th colspan="4"></th>
        </tr>
        <tr>
            <th> PROCESOS/ INDICADORES </th>
            <th class="text-center">Cantidad</th>
            <th class="text-center">Valores</th>
            <th class="text-center">Promedio</th>
        </tr>
    </thead>
    @foreach ($procesos as $proceso => $indicadores)
        <tbody x-data="{ expanded: true }">
            <tr class="table-active">
                <th x-on:click="expanded = !expanded"> {{ $proceso }} </th>
                <th x-on:click="expanded = !expanded" class="text-center"> {{ $indicadores->sum('conteo') }} </th>
                <th x-on:click="expanded = !expanded" class="text-center"> {{ $indicadores->sum('valores') }} </th>
                <th x-on:click="expanded = !expanded" class="text-center"> {{ $indicadores->avg('promedio') }} </th>
            </tr>
            @foreach ($indicadores as $indicador)
                <tr x-show="expanded">
                    <td>{{ $indicador->nombre }}</td>
                    <td class="text-center">{{ $indicador->conteo }}</td>
                    <td class="text-center">{{ $indicador->valores }}</td>
                    <td class="text-center">{{ $indicador->promedio }}</td>
                </tr>
            @endforeach
        </tbody>
    @endforeach
</table>
