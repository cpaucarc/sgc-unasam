<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="col-12 text-end">
                    <a href="/reportes/por_procesos/excel" class="btn btn-outline-primary round waves-effect">
                        Descargar
                    </a>
                </div>
                <hr>
                @include('livewire.reportes.por-procesos.table')
            </div>
        </div>
    </div>
</div>
