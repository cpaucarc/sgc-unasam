<!-- Modal -->
<x-jet-dialog-modal wire:model="open" maxWidth="lg">
    <x-slot name="title">
        <h3>Agregar Nuevo Usuario</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2">

            <div class="col-12 col-md-6">
                <label class="form-label"><strong><span style="color: red">*</span> Usuario </strong> </label>
                <input
                    type="text"
                    wire:model="name"
                    class="form-control"
                    placeholder="Usuario" maxlength="250"
                />
                @error('name')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"><strong><span style="color: red"> *</span> Email</strong></label>
                <input
                    type="text"
                    wire:model="email"
                    class="form-control"
                    placeholder="Email" maxlength="250"
                />
                @error('email')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"><strong><span style="color: red"> *</span> Password</strong></label>
                <input
                    type="password"
                    wire:model="password"
                    class="form-control" autocomplete="new-password"
                    placeholder="Password" maxlength="250"
                />
                @error('password')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            {{-- <div class="col-12 col-md-6">
                <label class="form-label">Rol</label>
                <select wire:model="role_id" class="select2 form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($roles as $rol)
                    <option value="{{$rol->id}}">{{ $rol->name }}</option>
                    @endforeach
                  </select>
               @error('role_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>  --}}
            <div class="col-12 col-md-12">
                <label class="form-label">Persona</label>
                <div wire:ignore>
                    <select wire:model="persona_id" class="form-control selectPersona" >
                        <option value="">--Seleccione--</option>
                        @foreach($personas as $persona)
                        <option value="{{$persona->id}}">{{ trim($persona->ape_paterno) ." ". trim($persona->ape_materno) ." ". trim($persona->nombres) }}</option>
                        @endforeach
                    </select>
                    @error('persona_id')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>


        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="store()" class="btn btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
