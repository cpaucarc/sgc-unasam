@section('title', __('Macroprocesos'))

<div class="card">
	<div class="card-datatable table-responsive">
		<div class="dataTables_wrapper dt-bootstrap5 no-footer">
			<div class="d-flex justify-content-between align-items-center header-actions mx-2 row mt-75">
				<div class="row">
					<!-- <div class="col-sm-12 col-md-6">
						<div class="dataTables_length">
							<label>Mostrar
								<select wire:model="cantidad" class="form-select">
									<option value="5">5</option>
									<option value="10">10</option>
									<option value="25">25</option>
									<option value="50">50</option>
									<option value="100">100</option>
								</select> entradas
							</label>
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						<div class="dt-action-buttons d-flex align-items-center justify-content-lg-end justify-content-center flex-md-nowrap flex-wrap">
							<div class="me-1">
								<div id="DataTables_Table_0_filter" class="dataTables_filter">
									<label>
										Buscar:<input type="search" class="form-control" wire:model="search" placeholder="">
									</label>
								</div>
							</div>
							<div class="dt-buttons btn-group flex-wrap">
								<x-jet-button wire:click="$set('open',true)" class="btn add-new btn-primary mt-50">
									<span>Agregar Usuario</span>
								</x-jet-button>
							</div>
						</div>
					</div> -->
					<div class="col-sm-12 col-md-6">
						<div class="dataTables_length">
							<label>Mostrar
								<select wire:model="cantidad" class="form-select">
									<option value="5">5</option>
									<option value="10">10</option>
									<option value="25">25</option>
									<option value="50">50</option>
									<option value="100">100</option>
								</select> entradas
							</label>
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						<div class="dt-action-buttons d-flex align-items-center justify-content-lg-end justify-content-center flex-md-nowrap flex-wrap">
							<div class="me-1">
								<div id="DataTables_Table_0_filter" class="dataTables_filter">
									<label>
										Buscar:<input type="search" class="form-control" wire:model="search" placeholder="Buscar">
									</label>
								</div>
							</div>
							<div class="dt-buttons btn-group flex-wrap">
								@can('create', App\Models\User::class)
								<x-jet-button wire:click="$set('open',true)" class="btn add-new btn-primary mt-50">
									<span>Agregar Usuarios</span>
								</x-jet-button>
								@endcan
							</div>
						</div>
					</div>
					<div>
						@include('livewire.users.create')
						@include('livewire.users.update')
						@include('livewire.users.asignacion')
					</div>
				</div>
			</div>

			@can('viewAny', App\Models\User::class)
			<div class="text-nowrap">
				<table class="datatables-permissions table dataTable no-footer dtr-column" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style="width: 1441px;">
					<thead class="table-light">
						<tr role="row">
							<th class="sorting cursor-pointer" >Apellidos y Nombres</th>
							<th class="sorting cursor-pointer" >Documento</th>
                            <th class="sorting cursor-pointer" wire:click="ordenar('name')">Usuario</th>
                            <th class="sorting cursor-pointer" wire:click="ordenar('email')">Email</th>
							<th aria-label="Estado">Estado</th>
							<th aria-label="Acciones">Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $user)
							<tr>
                                @if($user->persona_id)
                                  <td>{{$user->persona->ape_paterno ." ". $user->persona->ape_materno ." ". $user->persona->nombres}}</td>
                                  <td>{{$user->persona->nro_documento}}</td>
								@else
                                <td class="text-center">-</td>
                                <td class="text-center">-</td>
                                @endif

								<td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>

								{{-- @if($user->role_id)
                                  <td>{{$user->rol->name}}</td>
								@else
                                	<td class="text-center">-</td>
                                @endif --}}

								@if($user->activo == 1)
									<td>
										<span class="badge rounded-pill badge-light-success" text-capitalized="">Activo</span>
									</td>
								@else
									<td>
										<span class="badge rounded-pill badge-light-secondary" text-capitalized="">Inactivo</span>
									</td>
								@endif
								<td>
									@can('update', $user)
									<button wire:click="edit({{$user->id}})" class="btn btn-icon btn-flat-success waves-effect">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit font-medium-2 text-body">
                                            <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                            <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                        </svg>
                                    </button>
									@endcan
                                	@can('delete', $user)
									<button wire:click="$emit('EliminarUsuario',{{$user->id}})" class="btn btn-icon btn-flat-danger waves-effect">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
                                            <polyline points="3 6 5 6 21 6"></polyline>
                                            <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                        </svg>
                                    </button>
									@endcan
                                	@can('update', $user)
									<button wire:click="asignacion({{$user->id}})" class="btn btn-icon btn-flat-primary waves-effect">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                                    </button>
									@endcan
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				<div class="d-flex justify-content-between mx-2 row mb-1">
					<div class="col-sm-12 col-md-6">
						<div class="dataTables_info" aria-live="polite">
							Mostrar {{$users->firstItem()}} a {{$users->lastItem()}} de {{$users->total()}} entradas
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						{{$users->links()}}
					</div>
				</div>
			</div>
			@endcan
		</div>
	</div>
</div>
<script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
<script>
	window.onload = function() {
		Livewire.on('alertRespuesta', function(datos){
			nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
		});

		Livewire.on('EliminarUsuario', userId=>{
			Swal.fire({
					title: '¿Está seguro?',
					text: "¡No podrás revertir esto!",
					icon: 'error',
					showCancelButton: true,
					// confirmButtonColor: '#3085d6',
					// cancelButtonColor: '#d33',
					confirmButtonText: '¡Sí, elimínalo!'
				}).then((result) => {
				if (result.isConfirmed) {
					Livewire.emitTo('users','destroy',userId)
				}
			})
		});
		Livewire.on('EliminarAsignacion', asignacionId=>{
			Swal.fire({
					title: '¿Está seguro?',
					text: "¡No podrás revertir esto!",
					icon: 'error',
					showCancelButton: true,
					// confirmButtonColor: '#3085d6',
					// cancelButtonColor: '#d33',
					confirmButtonText: '¡Sí, elimínalo!'
				}).then((result) => {
				if (result.isConfirmed) {
					Livewire.emitTo('users','destroyAsignacion',asignacionId)

				}
			})
		});

		selectPersona = $('.selectPersona');
		asignarSelect2(selectPersona,'100%','Buscar persona');
        selectPersona.on('change', function (e) {
            $data = selectPersona.select2("val");
            @this.set('persona_id', $data);
            console.log('persona: '+$data);
        }); 

		selectPersonaEdit = $('.selectPersona-edit');
        asignarSelect2(selectPersonaEdit,'100%','Buscar persona');
        selectPersonaEdit.on('change', function (e) {
            $data = selectPersonaEdit.select2("val");
            @this.set('persona_id', $data);
            console.log('persona: '+$data);
        });

		selectArea = $('.selectArea');
		asignarSelect2(selectArea,'100%','Buscar área');
        selectArea.on('change', function (e) {
            $data = selectArea.select2("val");
            @this.set('area_id', $data);
            console.log('persona: '+$data);
        }); 

		Livewire.on('resetearSelect', function () {   
            selectPersona.val('NULL');
            selectPersona.trigger('change');
            selectPersonaEdit.val('NULL');
            selectPersonaEdit.trigger('change');
            selectArea.val('NULL');
            selectArea.trigger('change');
        });

		Livewire.on('asignarDatos', function(persona_id){               
            selectPersonaEdit.val(persona_id).change();
            selectPersonaEdit.trigger('change');                
        }); 

		$('[data-toggle="tooltip"]').tooltip();
	}
</script>
