<!-- Modal -->
<x-jet-dialog-modal wire:model="asignacion" maxWidth="lg">
    <x-slot name="title">
        <h3>Asignación de roles</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2">

            <div class="col-12 col-md-12">
                <label class="form-label"> <strong><span style="color: red">*</span> Rol </strong> </label>
                <select wire:model="role_id" class="select2 form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($roles as $rol)
                    <option value="{{$rol->id}}">{{ $rol->name }}</option>
                    @endforeach
                  </select>
               @error('role_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label"> <strong>Area </strong></label>
                <div wire:ignore>
                    <select wire:model="area_id" class="form-control selectArea" >
                        <option value="null">--Seleccione--</option>
                        @foreach($areas2 as $area)
                        <option value="{{$area->id}}">{{ $area->nombre }}</option>
                        @endforeach
                    </select>
                    @error('area_id')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
                
            </div>

            <div class="col-12 col-md-6">
                <label class="form-label"><strong>Fecha Inicio </strong> </label>
                <input
                    type="text"
                    wire:model="fechaIni"
                    class="form-control fechaflatpickr"
                    placeholder="Fecha Inicio"
                />
                @error('fechaIni')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"><strong>Fecha Fin </strong> </label>
                <input
                    type="text"
                    wire:model="fechaFin"
                    class="form-control fechaflatpickr"
                    placeholder="Fecha Fin"
                />
                @error('fechaFin')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12 text-center">
                <x-jet-button wire:click.prevent="storeAsignacion()" class="btn btn btn-primary me-1 mt-2">
                    AÑADIR
                </x-jet-button>
                <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                    CANCELAR
                </x-jet-button>
            </div>
          </div>
          <div class="row gy-1 gx-2">
            <div class="card">
                <div class="card-datatable table-responsive">
                    <div class="dataTables_wrapper dt-bootstrap5 no-footer">
                        <div class="text-nowrap">
                            <table class="datatables-permissions table dataTable no-footer dtr-column" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style="width: 1441px;">
                                <thead class="table-light">
                                    <tr role="row">
                                        <th class="sorting cursor-pointer" >AREA</th>
                                        <th class="sorting cursor-pointer" >ROL</th>
                                        <th class="sorting cursor-pointer" >INICIO - FIN</th>
                                        <th aria-label="Acciones">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($asignacions as $asignacion)
                                    <tr>
                                        <td>  @if($asignacion->area_id) {{$asignacion->area->nombre  }}@endif</td>
                                        <td> {{$asignacion->rol->name  }}</td>
                                        <td> {{$asignacion->fechaIni }} - {{$asignacion->fechaFin  }}</td>
                                        <td>
                                             <button   wire:click="$emit('EliminarAsignacion',{{$asignacion->id}})" class="btn btn-icon btn-flat-danger waves-effect">
                                                <span class=" fa fa-check text-primary ">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
                                                        <polyline points="3 6 5 6 21 6"></polyline>
                                                        <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                    </svg>
                                                </span>
                                            </button>

                                        </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

          </div>
    </x-slot>
    <x-slot name="footer">

    </x-slot>
</x-jet-dialog-modal>

