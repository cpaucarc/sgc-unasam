<!-- Modal -->
<x-jet-dialog-modal wire:model="showMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3 class="address-title text-center mb-1"><strong>Actualizar Área</strong></h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        {{--<div class="row gy-1 gx-2 pb-1">

        </div>--}}
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-8">
                <label class="form-label"><strong><span style="color: red">*</span> Nombre del Área </strong></label>
                <input type="text" wire:model="nombre" class="form-control" placeholder="Nombre area" maxlength="250" disabled/>
                @error('nombre')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-4">
                <label class="form-label"><strong><span style="color: red">*</span> Abreviatura </strong></label>
                <input type="text" wire:model="abreviatura" class="form-control" placeholder="Abreviatura" maxlength="20" disabled/>
                @error('abreviatura')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-12">
                <label class="form-label"><strong>Área origen</strong></label>
                <select wire:model="area_id" class="form-select" disabled>
                    <option value="">-- Seleccione --</option>
                    @foreach ($param_area as $area)
                        <option value="{{$area->id}}">{{$area->nombre}}</option>
                    @endforeach
                </select>
                @error('area_id')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-7">
                <label class="form-label"><strong><span style="color: red">*</span> Nivel</strong></label>
                <select wire:model="param_nivel" class="form-select" disabled>
                    <option value="">-- Seleccione --</option>
                    @foreach ($parametro_nivel as $nivel)
                        <option value="{{$nivel->id}}">{{$nivel->detalle}}</option>
                    @endforeach
                </select>
                @error('param_nivel')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-5">
                <label class="form-label"><strong><span style="color: red">*</span> Tipo Órgano</strong></label>
                <select wire:model="param_tipoorgano" class="form-select" disabled>
                    <option value="">-- Seleccione --</option>
                    @foreach ($parametro_tipoorgano as $tipoOrgano)
                        <option value="{{$tipoOrgano->id}}">{{$tipoOrgano->detalle}}</option>
                    @endforeach
                </select>
                @error('param_tipoorgano')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="cerrar()" class="btn btn-primary me-1 mt-2">
                Cerrar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
