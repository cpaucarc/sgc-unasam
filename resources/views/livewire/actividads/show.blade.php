<!-- Modal -->
<x-jet-dialog-modal wire:model="openShow" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3 class="address-title text-center mb-1" id="addNewPersonaTitulo">Detalle Actvidad</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-12">
                <label class="form-label"><strong><span style="color: red">*</span> Nombre de la Actividad </strong></label>
                <textarea wire:model="nombre" class="form-control" placeholder="Alcance" maxlength="700" rows="3" disabled></textarea>
                @error('nombre')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label"><strong>Descripción</strong></label>
                <textarea wire:model="descripcion" class="form-control" placeholder="Alcance" maxlength="700" rows="3" disabled></textarea>
                @error('descripcion')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="cerrar()" class="btn btn-primary me-1 mt-2">
                Cerrar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
