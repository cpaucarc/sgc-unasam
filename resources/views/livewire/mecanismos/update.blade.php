<!-- Modal -->
<x-jet-dialog-modal wire:model="updateMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3>Actualizar Mecanismo</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2">
            <div class="col-12 col-md-12">
                <label class="form-label"> <strong><span style="color:red">*</span> Nombre  </strong> </label>
                <input
                    type="text"
                    wire:model="nombre"
                    class="form-control"
                    placeholder="Nombre" maxlength="220"
                />
                @error('nombre')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label"> <strong> <span style="color:red">*</span> Tipo </strong> </label>
                <select wire:model="param_tipomecanismo" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($tipoMecanismos as $tipo)
                    <option value="{{$tipo->id}}">{{ $tipo->detalle }}</option>
                    @endforeach
                  </select>
              @error('param_tipomecanismo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-12">
                <label class="form-label">descripcion</label>
                <textarea
                    wire:model="descripcion"
                    class="form-control"
                    rows="3"
                    placeholder="Descripcion" maxlength="4000"
                ></textarea>
                @error('descripcion')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="update()" class="btn btn btn-primary me-1 mt-2">
                Actualizar
            </x-jet-button>
            <x-jet-button wire:click.prevent="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
