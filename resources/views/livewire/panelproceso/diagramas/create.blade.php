<x-jet-dialog-modal wire:model="open" maxWidth="lg">
    <x-slot name="title">
        <h3>Agregar Diagrama de Flujo</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <form id="form-diagrama">
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-4">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>Año</strong></label>
                    <select wire:model.defer="anio_id" id="anio" class="form-control form-select">
                        <option value="NULL">-- Seleccione --</option>
                        @foreach ($anios as $anio)
                            <option value="{{$anio->id}}">{{$anio->anio}}</option>
                        @endforeach
                    </select>
                    @error('anio_id')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div> 
               
                <div class="col-12 col-md-4">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>Inicio de Vigencia</strong></label>
                    <input wire:model.defer="fechaIniVigencia" type="text" id="inicioVigencia" placeholder="dd-mm-aaa" class="form-control fechaVigencia">
                    @error('fechaIniVigencia')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div> 
                <div class="col-12 col-md-4">
                    <label class="form-label"><strong>Fin de Vigencia</strong></label>
                    <input wire:model.defer="fechaFinVigencia" type="text" id="finVigencia" placeholder="dd-mm-aaa" class="form-control fechaVigencia">
                    @error('fechaFinVigencia')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div> 
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-12">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>Descripción</strong></label>
                    <textarea 
                        wire:model.defer="descripcion"
                        class="form-control"
                        placeholder="{{$placeholder}}"
                        rows="2"
                    ></textarea>
                    @error('descripcion')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div> 
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-12">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>Seleccione el Archivo</strong></label>
                    <input type="file" wire:model.defer="image" class="form-control file-upload" accept="image/*">
                    @error('image')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div> 
            </div>  
            <div class="row gy-1 gx-2 pb-1">
                <div wire:loading wire:target="image" class="alert alert-danger" role="alert">
                    <h4 class="alert-heading">La imagen se está cargando...</h4>
                    <div class="alert-body">
                        Espere un momento hasta que la imagen se haya guardado.
                    </div>  
                </div>
            </div> 
        </form>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
            <x-jet-button wire:loading.attr="disabled" wire:target="image" wire:click.prevent="store()" class="btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
        </div>
        
    </x-slot>
</x-jet-dialog-modal>