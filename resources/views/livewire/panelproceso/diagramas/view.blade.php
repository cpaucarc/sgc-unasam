<div>
  <div class="row invoice-preview">
    <div class="col-12 text-center">
      @can('create', App\Models\Archivo::class)
      <button wire:click="$set('open',true)" class="btn btn-primary mb-75">Cargar Diagrama de Flujo</button>
      @endcan
    </div>
  </div>
  @if($diagramas->count()>0)
    <div class="row invoice-preview border">
      @foreach ($diagramas as $diagrama)
        <div class="row">
          <div class="col-12 text-center pt-75">
            <h3 class="m-0">{{$diagrama->descripcion}}</h3>
          </div>
          <div class="col-12 text-center m-75">
            @can('update', $diagrama)
            <button wire:click="edit({{$diagrama->id}})" class="btn btn-outline-secondary btn-download-invoice">Editar</button>
            @endcan

            <button wire:click="descargar({{$diagrama->id}})"class="btn btn-outline-secondary btn-download-invoice">Descargar</button>
            
            @can('delete', $diagrama)
            <button wire:click="$emit('Eliminardiagrama',{{$diagrama->id}})" class="btn btn-outline-danger btn-download-invoice">Eliminar</button>
            @endcan
          </div>
          <div class="col-12 text-center">
            <img
                src="/storage/{{$diagrama->ruta}}"
                class="img-fluid"
                alt="{{$diagrama->nombre}}"
              />
          </div>
        </div>
      @endforeach
    </div>
  @else
    <div class="row invoice-preview">
      <div class="col-12">
        No se ha encontrado ningún diagrama.
      </div>
    </div>
  @endif
  <div>
    @include('livewire.panelproceso.diagramas.create')
    @include('livewire.panelproceso.diagramas.update')
  </div>
  <script>
    function scriptDiagramas() {

      Livewire.on('resetFile', function(){
				$(".file-upload").val('');
			});

      asignarFecha('.fechaVigencia','d-m-Y','01-01-1978');
      
      Livewire.on('alertDiagrama', function(datos){
        nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
      });
      Livewire.on('EliminarPA', archivoID=>{
        Swal.fire({
            title: '¿Está seguro?',
            text: "¡No podrás revertir esto!",
            icon: 'error',
            showCancelButton: true,
            confirmButtonText: '¡Sí, elimínalo!',
					  cancelButtonText: 'Cancelar'
          }).then((result) => {
          if (result.isConfirmed) {
            Livewire.emitTo('panelproceso.diagramas','destroy',archivoID)            
          }
        })
      });	    
    }
  </script>
</div>