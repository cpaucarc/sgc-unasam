<div>
    <div class="row invoice-preview">
      <div class="col-12 text-center">
        @can('create', App\Models\Archivo::class)
        <button wire:click="$set('openManual',true)" class="btn btn-primary mb-75">Cargar Manual</button>
        @endcan
      </div>
    </div>
    @if($manuales->count()>0)
      <div class="row invoice-preview border">
        @foreach ($manuales as $manual)
          <div class="row">
            <div class="col-12 text-center pt-75">
              <h3 class="m-0">{{$manual->descripcion}}</h3>
            </div>
            <div class="col-12 text-center m-75">
              @can('update', $manual)
              <button wire:click="edit({{$manual->id}})" class="btn btn-outline-secondary btn-download-invoice">Editar</button>
              @endcan

              <button wire:click="descargar({{$manual->id}})"class="btn btn-outline-secondary btn-download-invoice">Descargar</button>
              
              @can('delete', $manual)
              <button wire:click="$emit('EliminarManual',{{$manual->id}})" class="btn btn-outline-danger btn-download-invoice">Eliminar</button>
              @endcan
            </div>
            <div class="col-12 text-center">
              <embed src="/storage/{{$manual->ruta}}" type="application/pdf" width="100%" height="900px" />
            </div>
          </div>
        @endforeach
      </div>
    @else
      <div class="row invoice-preview">
        <div class="col-12">
          No se ha encontrado ningún manual.
        </div>
      </div>
    @endif
    <div>
      @include('livewire.panelproceso.manuales.create')
      @include('livewire.panelproceso.manuales.update')
    </div>
    <script>
        function scriptManuales() {
          Livewire.on('resetFile', function(){
            $(".file-upload").val('');
          });
          Livewire.on('alertManual', function(datos){
              nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
          });
          Livewire.on('EliminarManual', manualID=>{
          Swal.fire({
              title: '¿Está seguro?',
              text: "¡No podrás revertir esto!",
              icon: 'error',
              showCancelButton: true,
              confirmButtonText: '¡Sí, elimínalo!',
                          cancelButtonText: 'Cancelar'
              }).then((result) => {
                  if (result.isConfirmed) {
                      Livewire.emitTo('panelproceso.manuales','destroy',manualID)
                  }
              })
          });
        }
    </script>
</div>
