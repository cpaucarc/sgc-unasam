<div>
    <div class="row">
        <div class="col-12">
            <button class="btn btn-flat-primary btn-sm mt-2" wire:click="agregarTermino">
                <svg xmlns="http://www.w3.org/2000/svg" class="mx-1" width="16" height="16" fill="none" viewBox="0 0 24 24" stroke="currentColor"
                     stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 4v16m8-8H4" />
                </svg>
                Agregar termino
            </button>
        </div>
    </div>
    <br>
    @if ($terminos->count() != 0)
        <div class="row">
            <div class="col-sm-12 col-md-6 mb-1">
                <div class="d-flex justify-content-lg-start">
                    <div class="d-flex align-items-center">
                        Mostrar
                    </div>
                    <div class="d-flex align-items-center mx-1">
                        <select wire:model="cantidad" class="form-select" style="width: 75px;">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="{{ $terminos->total() }}">Todo</option>
                        </select>
                    </div>
                    <div class="d-flex align-items-center">
                        entradas
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 mb-1">
                <div class="d-flex justify-content-lg-end">
                    <div class="d-flex align-items-center">
                        <label>Buscar:</label>
                    </div>
                    <div class="d-flex align-items-center mx-1 col-10 col-sm-6">
                        <input type="search" class="form-control" wire:model="search" placeholder="">
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered" role="grid">
                <thead>
                    <tr>
                        <th class="text-center px-0" scope="col" style="width: 50px;">Nro.</th>
                        <th class="text-center px-0" rowspan="2">NOMBRE</th>
                        <th class="text-center px-0" rowspan="2">DESCRIPCIÓN</th>
                        <th class="text-center px-0" rowspan="2">ACCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($terminos as $termino)
                        <tr>
                            <td class="py-1 px-0 text-center"><span>{{ $loop->iteration }}</span></td>
                            <td class="py-1 px-1">
                                <strong>{{ $termino->nombre }}</strong>
                            </td>
                            <td class="py-1 px-1">
                                {{ $termino->definicion }}
                            </td>
                            <td>
                                <button wire:click="eliminarTermino({{ $termino->id }})" class="btn btn-icon rounded-circle btn-flat-danger waves-effect">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
                                        <polyline points="3 6 5 6 21 6"></polyline>
                                        <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                    </svg>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-between mx-2 row mt-1">
            <div class="col-sm-12 col-md-6">
                <div class="dataTables_info" aria-live="polite">
                    Mostrar {{ $terminos->firstItem() }} a {{ $terminos->lastItem() }} de {{ $terminos->total() }} entradas
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                {{ $terminos->links() }}
            </div>
        </div>
    @else
        <strong class="text-danger">NO HAY REGISTRO</strong>
    @endif

    <x-jet-dialog-modal wire:model="showTerminos" maxWidth="lg">
        <x-slot name="title">
            <h3 class="address-title text-center mb-1"><strong>Agregar termino</strong></h3>
        </x-slot>
        <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12" wire:ignore>
                    <label for="termino_id">Seleccionar el término</label>
                    <select name="termino_id" id="termino_select" class="form-control" wire:model="termino_id">
                        <option value="">-- Seleccione --</option>

                        @foreach ($terminos_todos as $m)
                            <option value="{{ $m->id }}">{{ $m->nombre }} - <small>{{ substr($m->definicion, 0, 150) }}...</small></option>
                        @endforeach
                    </select>
                </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <div class="col-12 text-center">
                <x-jet-button wire:click.prevent="guardarTermino" class="btn btn-primary me-1 mt-2">
                    Guardar
                </x-jet-button>
                <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                    Cancelar
                </x-jet-button>
            </div>

        </x-slot>
    </x-jet-dialog-modal>

    <script>
        Livewire.on('EliminarTermino', recursoId => {
            Swal.fire({
                title: '¿Está seguro?',
                text: "¡No podrás revertir esto!",
                icon: 'error',
                showCancelButton: true,
                // confirmButtonColor: '#3085d6',
                // cancelButtonColor: '#d33',
                confirmButtonText: '¡Sí, elimínalo!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('procesos', 'destroy', recursoId)
                }
            })
        });
    </script>

</div>
