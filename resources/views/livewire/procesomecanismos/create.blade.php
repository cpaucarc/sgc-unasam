<!-- Modal -->
<x-jet-dialog-modal wire:model="open" maxWidth="lg">
    <x-slot name="title">
        <h3>Agregar Nuevo Proceso Mecanismo</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2">
            <div class="col-12 col-md-6">
                <label class="form-label">Objetivo</label>
                <input
                    type="text"
                    wire:model="objetivo"
                    class="form-control"
                    placeholder="Objetivo" maxlength="4000"
                />
                @error('objetivo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"><span style="color: red">(*)</span> Mecanismo</label>
                <select wire:model="mecanismo_id" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($mecanismos as $mecanismo)
                    <option value="{{$mecanismo->id}}">{{ $mecanismo->nombre }}</option>
                    @endforeach
                  </select>
               @error('mecanismo_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"><span style="color: red">(*)</span> Macroproceso</label>
                <select wire:model="macroproceso_id" wire:change="setProcesoSelect()" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($macroprocesos as $macroproceso)
                    <option value="{{$macroproceso->id}}">{{ $macroproceso->nombre }}</option>
                    @endforeach
                  </select>
               @error('macroproceso_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label">Proceso</label>
                <select wire:model="proceso_id" wire:change="setSubProcesoSelect()" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($procesos as $proceso)
                    <option value="{{$proceso->id}}">{{ $proceso->nombre }}</option>
                    @endforeach
                  </select>
               @error('proceso_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label">Subproceso</label>
                <select wire:model="subproceso_id" wire:change="setMicroProcesoSelect()" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($subprocesos as $subproceso)
                    <option value="{{$subproceso->id}}">{{ $subproceso->nombre }}</option>
                    @endforeach
                  </select>
               @error('subproceso_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label">Microproceso</label>
                <select wire:model="microproceso_id" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($microprocesos as $microproceso)
                    <option value="{{$microproceso->id}}">{{ $microproceso->nombre }}</option>
                    @endforeach
                  </select>
               @error('microproceso_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="store()" class="btn btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
