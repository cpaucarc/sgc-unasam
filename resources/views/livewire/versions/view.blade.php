<div class="card">
	<div class="card-datatable table-responsive">
		<div class="dataTables_wrapper dt-bootstrap5 no-footer">
			<div class="d-flex justify-content-between align-items-center header-actions mx-2 row mt-75">
				<div class="row">
					<div class="col-sm-12 col-md-6">
						<div class="dataTables_length">
							<label>Mostrar
								<select wire:model="cantidad" class="form-select">
									<option value="5">5</option>
									<option value="10">10</option>
									<option value="25">25</option>
									<option value="50">50</option>
									<option value="100">100</option>
								</select> entradas
							</label>
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						<div class="dt-action-buttons d-flex align-items-center justify-content-lg-end justify-content-center flex-md-nowrap flex-wrap">
							<div class="me-1">
								<div id="DataTables_Table_0_filter" class="dataTables_filter">
									<label>
										Buscar:<input type="search" class="form-control" wire:model="search" placeholder="">
									</label>
								</div>
							</div>
							<div class="dt-buttons btn-group flex-wrap">
								@can('create', App\Models\Version::class)
								<x-jet-button wire:click="$set('open',true)" class="btn add-new btn-primary mt-50">
									<span>Agregar Versión</span>
								</x-jet-button>
								@endcan
							</div>
						</div>
					</div>
					<div>
						@include('livewire.versions.create')
						@include('livewire.versions.update')
						@include('livewire.versions.show')
					</div>
				</div>
			</div>

			@can('viewAny', App\Models\Version::class)
			<div class="table-responsive px-25">
				<table class="datatables-permissions table dataTable no-footer dtr-column table-bordered" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style="width: 100%;">
					<thead class="table-light">
						<tr role="row">
							<th class="sorting cursor-pointer" wire:click="ordenar('nivel_proceso')">Tipo</th>
							<th class="sorting cursor-pointer" wire:click="ordenar('numero')">Número</th>
							<th class="sorting cursor-pointer" wire:click="ordenar('fecha_vigencia')">Fecha de Vigencia</th>
							<th class="sorting cursor-pointer" wire:click="ordenar('descripcion')">Descripción</th>
							<th class="sorting cursor-pointer" wire:click="ordenar('per_elaboro')">Elaborado Por</th>
							<th class="sorting cursor-pointer" wire:click="ordenar('per_reviso')">Revisado Por</th>
							<th class="sorting cursor-pointer" wire:click="ordenar('per_aprobado')">Aprobado Por</th>
							<!-- <th class="sorting cursor-pointer" wire:click="ordenar('fecha_aprobacion')">Fecha de Aprobación</th> -->
							<th aria-label="Estado">Estado</th>
							<th aria-label="Acciones">Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($versions as $version)
							<tr>
								<td>{{$version->detalle_nivel->detalle}}</td>
								<td>{{$version->numero}}</td>
								<td>{{ date("d-m-Y", strtotime($version->fecha_vigencia)) }}</td>
								<td>{{$version->descripcion}}</td>
								@if($version->per_elaboro)
									<td>{{ trim($version->persona_elaboro->ape_paterno) ." ". trim($version->persona_elaboro->ape_materno) ." ". trim($version->persona_elaboro->nombres) }}</td>
								@else
									<td class="text-center">-</td>
								@endif
								@if($version->per_reviso)
									<td>{{ trim($version->persona_reviso->ape_paterno) ." ". trim($version->persona_reviso->ape_materno) ." ". trim($version->persona_reviso->nombres) }}</td>
								@else
									<td class="text-center">-</td>
								@endif
								@if($version->per_aprobado)
									<td>{{ trim($version->persona_aprobo->ape_paterno) ." ". trim($version->persona_aprobo->ape_materno) ." ". trim($version->persona_aprobo->nombres) }}</td>
								@else
									<td class="text-center">-</td>
								@endif
								<!-- <td>{{ date("d-m-Y", strtotime($version->fecha_aprobacion)) }}</td> -->
								@if($version->activo == 1)
									<td>
										<span class="badge rounded-pill badge-light-success" text-capitalized="">Activo</span>
									</td>
								@else
									<td>
										<span class="badge rounded-pill badge-light-secondary" text-capitalized="">Inactivo</span>
									</td>
								@endif
								<td>
									@can('view', $version)
									<button wire:click="show({{$version->id}})" class="btn btn-icon rounded-circle btn-flat-primary waves-effect px-0" data-bs-toggle="tooltip" data-bs-placement="top" title="Ver indicador">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye font-medium-2 text-body">
											<path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
											<circle cx="12" cy="12" r="3"></circle>
										</svg>
                                    </button>
									@endcan
									@can('update', $version)
									<button wire:click="edit({{$version->id}})" class="btn btn-icon btn-flat-success waves-effect px-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit font-medium-2 text-body">
                                            <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                            <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                        </svg>
                                    </button>
									@endcan
									@can('delete', $version)
									<button wire:click="$emit('EliminarVersion',{{$version->id}})" class="btn btn-icon btn-flat-danger waves-effect px-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
                                            <polyline points="3 6 5 6 21 6"></polyline>
                                            <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                        </svg>
                                    </button>
									@endcan
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				<div class="d-flex justify-content-between mx-2 row mb-1">
					<div class="col-sm-12 col-md-6">
						<div class="dataTables_info" aria-live="polite">
							Mostrar {{$versions->firstItem()}} a {{$versions->lastItem()}} de {{$versions->total()}} entradas
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						{{$versions->links()}}
					</div>
				</div>
			</div>
			@endcan
		</div>
	</div>
</div>

<script>
	window.onload = function() {

        var fechanac = $(".fechaflatpickr").flatpickr({
            dateFormat: "d-m-Y",
            minDate: '01-01-1940',
            allowInput: true,
            allowInvalidPreload: true,
            locale: {
                firstDayOfWeek: 1,
                weekdays: {
                    shorthand: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                    longhand: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                },
                months: {
                    shorthand: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Оct', 'Nov', 'Dic'],
                    longhand: ['Enero', 'Febreo', 'Мarzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                },
            }
        });

        Livewire.on('alertRespuesta', function(datos){
            nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
        });

		Livewire.on('EliminarVersion', rolID=>{
			Swal.fire({
					title: '¿Está seguro?',
					text: "¡No podrás revertir esto!",
					icon: 'error',
					showCancelButton: true,
					// confirmButtonColor: '#3085d6',
					// cancelButtonColor: '#d33',
					confirmButtonText: '¡Sí, elimínalo!'
				}).then((result) => {
				if (result.isConfirmed) {
					Livewire.emitTo('versions','destroy',rolID)
				}
			})
		});

		selectPersonaElaboro = $('.selectPersonaElaboro');
        asignarSelect2(selectPersonaElaboro,'100%','Buscar persona');
        selectPersonaElaboro.on('change', function (e) {
            $data = selectPersonaElaboro.select2("val");
            @this.set('per_elaboro', $data);
        });

		selectPersonaReviso = $('.selectPersonaReviso');
        asignarSelect2(selectPersonaReviso,'100%','Buscar persona');
        selectPersonaReviso.on('change', function (e) {
            $data = selectPersonaReviso.select2("val");
            @this.set('per_reviso', $data);
        });

		selectPersonaAprobo = $('.selectPersonaAprobo');
        asignarSelect2(selectPersonaAprobo,'100%','Buscar persona');
        selectPersonaAprobo.on('change', function (e) {
            $data = selectPersonaAprobo.select2("val");
            @this.set('per_aprobado', $data);
        });

		selectPersonaElaboroEdit = $('.selectPersonaElaboroEdit');
        asignarSelect2(selectPersonaElaboroEdit,'100%','Buscar persona');
        selectPersonaElaboroEdit.on('change', function (e) {
            $data = selectPersonaElaboroEdit.select2("val");
            @this.set('per_elaboro', $data);
        });

		selectPersonaRevisoEdit = $('.selectPersonaRevisoEdit');
        asignarSelect2(selectPersonaRevisoEdit,'100%','Buscar persona');
        selectPersonaRevisoEdit.on('change', function (e) {
            $data = selectPersonaRevisoEdit.select2("val");
            @this.set('per_reviso', $data);
        });

		selectPersonaAproboEdit = $('.selectPersonaAproboEdit');
        asignarSelect2(selectPersonaAproboEdit,'100%','Buscar persona');
        selectPersonaAproboEdit.on('change', function (e) {
            $data = selectPersonaAproboEdit.select2("val");
            @this.set('per_aprobado', $data);
        });
				
		/*
		Livewire.on('asignarDatos', function(){
            $persona_elaboro = selectPersonaElaboro.select2("val");
            $persona_reviso = selectPersonaReviso.select2("val");
            $persona_aprobo = selectPersonaAprobo.select2("val");
            @this.set('per_elaboro', $persona_elaboro);   
            @this.set('per_reviso', $persona_reviso);   
            @this.set('per_aprobado', $persona_aprobo); 
			console.log('PerElaborofff');  
            //console.log('PerElaboro'+per_elaboro);
            Livewire.emitTo('versions','storeVersion')
        });*/

        Livewire.on('resetearSelect', function () {   
            selectPersonaElaboro.val('NULL');
            selectPersonaElaboro.trigger('change');
			selectPersonaReviso.val('NULL');
            selectPersonaReviso.trigger('change');
			selectPersonaAprobo.val('NULL');
            selectPersonaAprobo.trigger('change');
        });

		Livewire.on('asignarDatos', function(per_elaboro, per_reviso, per_aprobado){
            //console.log("entra? -> area_id" + area_id + "-> "+persona_id);               
            selectPersonaElaboroEdit.val(per_elaboro).change();
            selectPersonaElaboroEdit.trigger('change');
			selectPersonaRevisoEdit.val(per_reviso).change();
            selectPersonaRevisoEdit.trigger('change');selectPersonaAproboEdit.val(per_aprobado).change();
            selectPersonaAproboEdit.trigger('change');                
        }); 

		//$('[data-toggle="tooltip"]').tooltip();
	}
</script>
