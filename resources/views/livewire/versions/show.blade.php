<!-- Modal -->
<x-jet-dialog-modal wire:model="showMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3>Ver Versión</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">

             <div class="col-12 col-md-3">
                <label class="form-label"><strong><span style="color:red"></span> Tipo de Versión</strong></label>
                <select wire:model="nivel_proceso" class="form-select" disabled >
                    <option value="">-- Seleccione --</option>
                    @foreach($nivel_procesos as $nivel)
                        <option value="{{$nivel->id}}">{{ trim($nivel->detalle) }}</option>
                    @endforeach
                </select>
                @error('nivel_proceso')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-3">
                <label class="form-label"><strong><span style="color:red"></span> Número</strong></label>
                <input type="text" wire:model="numero" class="form-control" placeholder="Número" maxlength="20" disabled />
                @error('numero')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-3">
                <label class="form-label"><strong><span style="color:red"></span> Fecha de Vigencia</strong></label>
                <input type="text" wire:model="fecha_vigencia" class="form-control fechaflatpickr" placeholder="dd-mm-aaa" disabled/>
                @error('fecha_vigencia')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-3">
                <label class="form-label"><strong><span style="color:red"></span> Fecha de Aprobación</strong></label>
                <input type="text" wire:model="fecha_aprobacion" class="form-control fechaflatpickr" placeholder="dd-mm-aaa" disabled/>
                @error('fecha_aprobacion')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-12">
                <label class="form-label"><strong><span style="color:red"></span> Elaborado Por</strong></label>
                <div wire:ignore>
                    <select wire:model.defer="per_elaboro" class="form-select" disabled >
                        <option value="">-- Seleccione --</option>
                        @foreach($parametro_personas as $persona)
                            <option value="{{$persona->id}}">{{ trim($persona->ape_paterno) ." ". trim($persona->ape_materno) ." ". trim($persona->nombres) }}</option>
                        @endforeach
                    </select>
                </div>
                @error('per_elaboro')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-12">
                <label class="form-label"><strong><span style="color:red"></span> Revisado Por</strong></label>
                <div wire:ignore>
                    <select wire:model.defer="per_reviso" class="form-select " disabled >
                        <option value="">-- Seleccione --</option>
                        @foreach($parametro_personas as $persona)
                            <option value="{{$persona->id}}">{{ trim($persona->ape_paterno) ." ". trim($persona->ape_materno) ." ". trim($persona->nombres) }}</option>
                        @endforeach
                    </select>
                </div>
                @error('per_reviso')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-12">
                <label class="form-label"><strong><span style="color:red"></span> Aprobado Por</strong></label>
                <div wire:ignore>
                    <select wire:model.defer="per_aprobado" class="form-select" disabled >
                        <option value="">-- Seleccione --</option>
                        @foreach($parametro_personas as $persona)
                            <option value="{{$persona->id}}">{{ trim($persona->ape_paterno) ." ". trim($persona->ape_materno) ." ". trim($persona->nombres) }}</option>
                        @endforeach
                    </select>
                </div>
                @error('per_aprobado')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-12">
                <label class="form-label"><strong><span style="color:red"></span> Descripción</strong></label>
                <textarea  wire:model="descripcion" class="form-control" placeholder="Descripción de la versión" rows="2" maxlength="4000"
                 disabled ></textarea>
                @error('descripcion')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>

    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="cerrar()" class="btn btn-primary me-1 mt-2">
                Cerrar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
