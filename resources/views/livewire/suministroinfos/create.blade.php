<!-- Modal -->
<x-jet-dialog-modal wire:model="open" maxWidth="lg">
    <x-slot name="title">
        <h3 class="address-title text-center mb-1" id="addNewPersonaTitulo"><strong>Agregar Suministro</strong></h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">

            <div class="col-12">
                <label class="form-label"><strong><span style="color: red">*</span> Indicador</strong></label>
                <select wire:model="paramIndicador" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($param_indicador as $paramIndicador)
                        <option value="{{$paramIndicador->id}}">{{$paramIndicador->nombre}}</option>
                    @endforeach
                </select>
                @error('paramIndicador')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12">
                <label class="form-label"><strong><span style="color: red">*</span> Macro Procesos </strong></label>
                <select wire:model="paramMacroproceso" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($param_macro as $macro)
                        <option value="{{$macro->id}}">{{$macro->nombre}}</option>
                    @endforeach
                </select>
                @error('paramMacroproceso')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-12">
                <label class="form-label"><strong>Proceso</strong></label>
                <select wire:model="paramProceso" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($param_proceso as $proceso)
                        <option value="{{$proceso->id}}">{{$proceso->nombre}}</option>
                    @endforeach
                </select>
                @error('paramProceso')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12">
                <label class="form-label"><strong>Sub Proceso</strong></label>
                <select wire:model="paramSubProceso" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($param_subproceso as $subproceso)
                        <option value="{{$subproceso->id}}">{{$subproceso->nombre}}</option>
                    @endforeach
                </select>
                @error('paramSubProceso')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12">
                <label class="form-label"><strong>Micro Proceso </strong></label>
                <select wire:model="paramMicroProceso" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($param_microproceso as $microproceso)
                        <option value="{{$microproceso->id}}">{{$microproceso->nombre}}</option>
                    @endforeach
                </select>
                @error('paramMicroProceso')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="store()" class="btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
