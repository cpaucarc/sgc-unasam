<div class="card">
    <div class="card-datatable table-responsive">
        <div class="dataTables_wrapper dt-bootstrap5 no-footer">
            <div class="d-flex justify-content-between align-items-center header-actions text-nowrap mx-1 row mt-75">
                <div class="col-sm-12 col-lg-4 d-flex justify-content-center justify-content-lg-start">
                    <div class="dataTables_length">
                        <label>Mostrar
                            <select name="DataTables_Table_0_length" wire:model="cantidad" class="form-select">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> entradas
                        </label>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8">
                    <div
                        class="dt-action-buttons d-flex align-items-center justify-content-lg-end justify-content-center flex-md-nowrap flex-wrap">
                        <div class="me-1">
                            <div id="DataTables_Table_0_filter" class="dataTables_filter">
                                <label>
                                    Buscar:<input type="search" class="form-control" wire:model="search" placeholder="">
                                </label>
                            </div>
                        </div>
                        <div class="dt-buttons btn-group flex-wrap">
                            <x-jet-button wire:click="$set('open',true)" class="btn add-new btn-primary mt-50">
                                <span>Agregar Suministro de Información</span>
                            </x-jet-button>
                        </div>
                    </div>
                </div>
                <div>
                    @include('livewire.suministroinfos.create')
                    @include('livewire.suministroinfos.update')
                </div>
            </div>

            <div class="table-responsive px-25">
                <table class="datatables-permissions table dataTable no-footer dtr-column table-bordered" id="DataTables_Table_0"
                       role="grid" aria-describedby="DataTables_Table_0_info" style="width: 1441px;">
                    <thead class="table-light">
                    <tr role="row">
                        <th class="text-center">Nro</th>
                        <th class="sorting cursor-pointer" wire:click="ordenar('nombre')">Nombre Indicador</th>
                        <th class="sorting cursor-pointer" wire:click="ordenar('abreviatura')">Código Indicador</th>
                        <th class="">Nombre Macro Proceso</th>
                        <th class="">Nombre Proceso</th>
                        <th class="">Nombre Subproceso</th>
                        <th class="">Nombre Micro Proceso</th>
                        <th class="">Estado</th>
                        <th class="">Fecha de Creación</th>
                        <th class="" aria-label="Actions">Acciones
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($suministros as $suministro)
                        <tr class="odd">
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td class=" control" tabindex="0" style="display: none;"></td>
                            <td>{{$suministro->nombre}}</td>
                            <td>{{$suministro->codigo}}</td>
                            <td>{{$suministro->nombreMacro}}</td>
                            <td>{{$suministro->nombreProceso}}</td>
                            <td>{{$suministro->nombreSubProceso}}</td>
                            <td>{{$suministro->nomMicroposeso}}</td>
                            <td>
                                @if($suministro->activo == 1)
                                    <span class="badge rounded-pill badge-light-success" text-capitalized="">Activo</span>
                                @else
                                    <span class="badge rounded-pill badge-light-danger" text-capitalized="">Inactivo</span>
                                @endif
                            </td>
                            <td>{{$suministro->created_at}}</td>
                            <td>
                                <button wire:click="edit({{$suministro->id}})" class="btn btn-icon btn-flat-success waves-effect px-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit font-medium-2 text-body">
                                        <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                        <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                    </svg>
                                </button>
                                <button wire:click="$emit('eliminarSuministro',{{$suministro->id}})" class="btn btn-icon btn-flat-danger waves-effect px-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
                                        <polyline points="3 6 5 6 21 6"></polyline>
                                        <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                    </svg>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-flex justify-content-between mx-2 row mb-1">
                <div class="col-sm-12 col-md-6">
                    <div class="dataTables_info" aria-live="polite">Mostrar {{$suministros->firstItem()}}
                        a {{$suministros->lastItem()}} de {{$suministros->total()}} entradas
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    {{$suministros->links()}}
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    window.onload = function() {
        Livewire.on('alertRespuesta', function(datos){
            nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
        });

        Livewire.on('eliminarSuministro', suministroId=>{
            Swal.fire({
                title: '¿Está seguro?',
                text: "¡No podrás revertir esto!",
                icon: 'error',
                showCancelButton: true,
                // confirmButtonColor: '#3085d6',
                // cancelButtonColor: '#d33',
                confirmButtonText: '¡Sí, elimínalo!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('suministro-component','destroy',suministroId)
                }
            })
        });
    }
</script>
