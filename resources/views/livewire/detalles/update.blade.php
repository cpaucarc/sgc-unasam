<!-- Modal -->
<x-jet-dialog-modal wire:model="updateMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3 class="address-title text-center mb-1"><strong>Actualizar Detalle</strong></h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-12">
                <label class="form-label"><strong><span style="color: red">*</span> Parámetro </strong></label>
                <select wire:model="parametro_id" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($parametros as $parametro)
                        <option value="{{$parametro->id}}">{{$parametro->parametro}}</option>
                    @endforeach
                </select>
                @error('parametro_id')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>

        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-8">
                <label class="form-label"><strong><span style="color: red">*</span> Detalle </strong></label>
                <input type="text" wire:model="detalle" class="form-control" placeholder="Detalle" maxlength="150"/>
                @error('detalle')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-4">
                <label class="form-label"><strong><span style="color: red">*</span> Abreviatura </strong></label>
                <input type="text" wire:model="abreviatura" class="form-control" placeholder="Abreviatura" maxlength="25"/>
                @error('abreviatura')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label"><strong>Descripción</strong></label>
                <input type="text" wire:model="descripcion" class="form-control" placeholder="Descripcion" maxlength="4000"/>
                @error('descripcion')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="update()" class="btn btn-primary me-1 mt-2">
                Actualizar
            </x-jet-button>
            <x-jet-button wire:click.prevent="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
