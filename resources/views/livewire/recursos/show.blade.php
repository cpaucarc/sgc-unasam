<!-- Modal -->
<x-jet-dialog-modal wire:model="showMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3>Detalle Recurso</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2">
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> Nombre </strong> </label>
                <input
                    type="text"
                    wire:model="nombre"
                    class="form-control"
                    placeholder="Nombre" disabled
                />  
                @error('nombre')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div> 
            <div class="col-12 col-md-6">                
                <label class="form-label"> <strong> Tipo  </strong> </label>                
                <select wire:model="param_tiporecurso" class="form-select" disabled>
                    <option value="">--Seleccione--</option>
                    @foreach($tipoRecursos as $tipo)
                    <option value="{{$tipo->id}}">{{ $tipo->detalle }}</option>
                    @endforeach
                  </select>               
              @error('param_tiporecurso')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="cerrar()" class="btn btn-primary me-1 mt-2">
                Cerrar
            </x-jet-button>  
        </div>
        
    </x-slot>
</x-jet-dialog-modal>