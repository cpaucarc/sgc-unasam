<!-- Modal -->
<x-jet-dialog-modal wire:model="open" maxWidth="lg">
    <x-slot name="title">
        <h3 class="address-title text-center mb-1" id="addNewPersonaTitulo"><strong>Agregar Trabajador</strong></h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">

            <div class="col-12 col-md-12" >
                <label class="form-label">Persona</label>
                <div wire:ignore>
                    <select  wire:model.defer="persona_id" class="form-control selectPersona" required>
                        <option value="NULL">--Seleccione--</option>
                        @foreach($parametro_personas as $persona)
                            <option value="{{$persona->id}}">{{ trim($persona->ape_paterno) ." ". trim($persona->ape_materno) ." ". trim($persona->nombres) }}</option>
                        @endforeach
                    </select>
                </div>
                @error('persona_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-12">
                <label class="form-label"><strong><span style="color:red">*</span> Área origen</strong></label>
                <div wire:ignore>
                    <select wire:model.defer="area_id" class="form-select selectArea">
                        <option value="NULL">-- Seleccione --</option>
                        @foreach ($areas as $area)
                            <option value="{{$area->id}}">{{$area->nombre}}</option>
                        @endforeach
                    </select>
                </div>
                @error('area_id')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12">
                <label class="form-label"><strong><span style="color:red">*</span> Cargo</strong></label>
                <select wire:model="cargo_id" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($parametro_cargos as $cargo)
                        <option value="{{$cargo->id}}">{{$cargo->nombre}}</option>
                    @endforeach
                </select>
                @error('cargo_id')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-6">
                <label class="form-label"><strong><span style="color:red">*</span> Fecha de Inicio</strong></label>
                <input type="text" wire:model="fecha_inicio" class="form-control fechaflatpickr" placeholder="dd-mm-aaa"/>
                @error('fecha_inicio')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-6">
                <label class="form-label"><strong>Fecha de Fin</strong></label>
                <input type="text" wire:model="fecha_fin" class="form-control fechaflatpickr" placeholder="dd-mm-aaa"/>
                @error('fecha_fin')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

        </div>

    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="store()" class="btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
