<!-- Modal -->
<x-jet-dialog-modal wire:model="showMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3 class="address-title text-center mb-1"><strong>Detalle Trabajador</strong></h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">

            <div class="col-12">
                <label class="form-label"><strong> Persona </strong></label>
                <select wire:model="persona_id" class="form-select" disabled>
                    <option value="">-- Seleccione --</option>
                    @foreach ($parametro_personas as $parametro_persona)
                        <option value="{{$parametro_persona->id}}">{{$parametro_persona->nombres}} {{$parametro_persona->ape_paterno}} {{$parametro_persona->ape_materno}}</option>
                    @endforeach
                </select>

                @error('persona_id')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-12">
                <label class="form-label"><strong> Área origen</strong></label>
                <select wire:model="area_id" class="form-select" disabled>
                    <option value="">-- Seleccione --</option>
                    @foreach ($areas as $area)
                        <option value="{{$area->id}}">{{$area->nombre}}</option>
                    @endforeach
                </select>
                @error('area_id')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12">
                <label class="form-label"><strong> Cargo</strong></label>
                <select wire:model="cargo_id" class="form-select" disabled>
                    <option value="">-- Seleccione --</option>
                    @foreach ($parametro_cargos as $cargo)
                        <option value="{{$cargo->id}}">{{$cargo->nombre}}</option>
                    @endforeach
                </select>
                @error('cargo_id')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-6">
                <label class="form-label"><strong> Fecha de Inicio</strong></label>
                <input type="text" wire:model="fecha_inicio" class="form-control fechaflatpickr" placeholder="dd-mm-aaa" disabled/>
                @error('fecha_inicio')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-6">
                <label class="form-label"><strong>Fecha de Fin</strong></label>
                <input type="text" wire:model="fecha_fin" class="form-control fechaflatpickr" placeholder="dd-mm-aaa" disabled/>
                @error('fecha_fin')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="cerrar()" class="btn btn-primary me-1 mt-2">
                Cerrar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
