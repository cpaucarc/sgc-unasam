<div class="card">
    <div class="card-datatable table-responsive">
        <div class="dataTables_wrapper dt-bootstrap5 no-footer">
            <div class="d-flex justify-content-between align-items-center header-actions text-nowrap mx-1 row mt-75">
                <div class="col-sm-12 col-lg-4 d-flex justify-content-center justify-content-lg-start">
                    <div class="dataTables_length">
                        <label>Mostrar
                            <select name="DataTables_Table_0_length" wire:model="cantidad" class="form-select">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> entradas
                        </label>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8">
                    <div
                        class="dt-action-buttons d-flex align-items-center justify-content-lg-end justify-content-center flex-md-nowrap flex-wrap">
                        <div class="me-1">
                            <div id="DataTables_Table_0_filter" class="dataTables_filter">
                                <label>
                                    Buscar:<input type="search" class="form-control" wire:model="search" placeholder="">
                                </label>
                            </div>
                        </div>
                        <div class="dt-buttons btn-group flex-wrap">
                            @can('create', App\Models\Trabajador::class)
                            <x-jet-button wire:click="$set('open',true)" class="btn add-new btn-primary mt-50">
                                <span>Agregar Trabajador</span>
                            </x-jet-button>
                            @endcan
                        </div>
                    </div>
                </div>
                <div>
                    @include('livewire.trabajadors.create')
                    @include('livewire.trabajadors.update')
                    @include('livewire.trabajadors.show')
                </div>
            </div>

            @can('viewAny', App\Models\Trabajador::class)
            <div class="table-responsive px-25">
                <table class="datatables-permissions table dataTable no-footer dtr-column table-bordered" id="DataTables_Table_0"
                       role="grid" aria-describedby="DataTables_Table_0_info" style="width: 100%;">
                    <thead class="table-light">
                    <tr role="row">
                        <th class="text-center">Nro</th>
                        <th class="sorting cursor-pointer" wire:click="ordenar('nombreReferencial')">Apellidos y Nombres</th>
                        <th class="" >Área a la que pertenece</th>
                        <th class="">Cargo</th>
                        <th class="">Fecha Inicio</th>
                        <th class="">Fecha FIn</th>
                        <th class="">Estado</th>
                        <th class="">Fecha de modificación</th>
                        <th width="10%" class="" aria-label="Actions">Acciones
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($trabajadores as $trabajador)
                        <tr class="odd">
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td class=" control" tabindex="0" style="display: none;"></td>
                            <td>{{$trabajador->pNombres}}</td>
                            <td>{{$trabajador->aNombre}}</td>
                            <td>{{$trabajador->cCargo}}</td>
                            <td>{{$trabajador->fecha_inicio}}</td>
                            <td>{{$trabajador->fecha_fin}}</td>
                            <td>
                                @if($trabajador->activo == 1)
                                    <span class="badge rounded-pill badge-light-success" text-capitalized="">Activo</span>
                                @else
                                    <span class="badge rounded-pill badge-light-secondary" text-capitalized="">Inactivo</span>
                                @endif
                            </td>
                            <td>{{ ($trabajador->updated_at) ? date("d-m-Y H:i:s", strtotime($trabajador->updated_at)) : date("d-m-Y H:i:s", strtotime($trabajador->created_at)) }}</td>
                            <td>
                                @can('view', $trabajador)
                                <button wire:ignore wire:click="show({{$trabajador->id}})" class="btn btn-icon btn-flat-secondary waves-effect px-0" data-bs-toggle="tooltip" data-bs-placement="top" title="Ver trabajador">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>

                                </button>
                                @endcan
                                @can('update', $trabajador)
                                <button wire:click="edit({{$trabajador->id}})" class="btn btn-icon btn-flat-success waves-effect px-25" data-toggle="tooltip" data-placement="top" title="Editar trabajador">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit font-medium-2 text-body">
                                        <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                        <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                    </svg>
                                </button>
                                @endcan
                                @can('delete', $trabajador)
                                <button wire:click="$emit('eliminarTrabajador',{{$trabajador->id}})" class="btn btn-icon btn-flat-danger waves-effect px-25" data-toggle="tooltip" data-placement="top" title="Eliminar trabajador">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
                                        <polyline points="3 6 5 6 21 6"></polyline>
                                        <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                    </svg>
                                </button>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-flex justify-content-between mx-2 row mb-1">
                <div class="col-sm-12 col-md-6">
                    <div class="dataTables_info" aria-live="polite">Mostrar {{$trabajadores->firstItem()}}
                        a {{$trabajadores->lastItem()}} de {{$trabajadores->total()}} entradas
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    {{$trabajadores->links()}}
                </div>
            </div>
            @endcan
        </div>
    </div>
</div>

<script>
    window.onload = function() {


        var fechanac = $(".fechaflatpickr").flatpickr({
            dateFormat: "d-m-Y",
            minDate: '01-01-1940',
            allowInput: true,
            allowInvalidPreload: true,
            locale: {
                firstDayOfWeek: 1,
                weekdays: {
                    shorthand: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                    longhand: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                },
                months: {
                    shorthand: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Оct', 'Nov', 'Dic'],
                    longhand: ['Enero', 'Febreo', 'Мarzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                },
            }
        });


        Livewire.on('alert', function(menssage){
            Swal.fire(
                '¡Buen trabajo!',
                menssage,
                'success'
            )
        });

        Livewire.on('alertRespuesta', function(datos){
            nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
        });

        Livewire.on('eliminarTrabajador', documentoId=>{
            Swal.fire({
                title: '¿Está seguro?',
                text: "¡No podrás revertir esto!",
                icon: 'error',
                showCancelButton: true,
                // confirmButtonColor: '#3085d6',
                // cancelButtonColor: '#d33',
                confirmButtonText: '¡Sí, elimínalo!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('trabajador-component','destroy',documentoId)
                    /*Swal.fire({
                        icon: 'success',
                        title: '¡Eliminado!',
                        text: 'El Trabajador ha sido eliminado.',
                        confirmButtonText: "Aceptar",
                    });*/
                }
            })
        });

        selectPersona = $('.selectPersona');
        asignarSelect2(selectPersona,'100%','Buscar persona');
        selectPersona.on('change', function (e) {
            $data = selectPersona.select2("val");
            @this.set('persona_id', $data);
            console.log('persona: '+$data);
        }); 

        selectArea = $('.selectArea');
        asignarSelect2(selectArea,'100%','Buscar área');
        selectArea.on('change', function (e) {
            $data = selectArea.select2("val");
            @this.set('area_id', $data);
            console.log('area_id: '+$data);
        });
        
        selectPersonaEdit = $('.selectPersona-edit');
        asignarSelect2(selectPersonaEdit,'100%','Buscar persona');
        selectPersonaEdit.on('change', function (e) {
            $data = selectPersonaEdit.select2("val");
            @this.set('persona_id', $data);
            console.log('persona: '+$data);
        }); 

        selectAreaEdit = $('.selectArea-edit');
        asignarSelect2(selectAreaEdit,'100%','Buscar área');
        selectAreaEdit.on('change', function (e) {
            $data = selectAreaEdit.select2("val");
            @this.set('area_id', $data);
            console.log('area_id: '+$data);
        });

        Livewire.on('resetearSelect', function () {   
            selectPersona.val('NULL');
            selectPersona.trigger('change');
            selectPersonaEdit.val('NULL');
            selectPersonaEdit.trigger('change');
            selectArea.val('NULL');
            selectArea.trigger('change');
            selectAreaEdit.val('NULL');
            selectAreaEdit.trigger('change');
        });

        Livewire.on('asignarDatos', function(persona_id, area_id){
            console.log("entra? -> area_id" + area_id + "-> "+persona_id);
            selectAreaEdit.val(area_id).change();
            selectAreaEdit.trigger('change');                
            selectPersonaEdit.val(persona_id).change();
            selectPersonaEdit.trigger('change');                
        });  

        //$('[data-toggle="tooltip"]').tooltip();
    }
</script>
