<!-- Modal -->
<x-jet-dialog-modal wire:model="updateMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3 class="address-title text-center mb-1"><strong>Actualizar Escala</strong></h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-6">
                <label class="form-label"><strong><span style="color:red">*</span> Parámetro Likert </strong></label>
                <select wire:model="param_likert" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($parametro_likerts as $parametro_likert)
                        <option value="{{$parametro_likert->id}}">{{$parametro_likert->detalle}}</option>
                    @endforeach
                </select>
                @error('param_likert')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-6">
                <label class="form-label"><strong><span style="color:red">*</span> Medición</strong></label>
                <select wire:model="medicion_id" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($parametro_mediciones as $parametro_medicion)
                        <option value="{{$parametro_medicion->id}}">{{$parametro_medicion->meta}}</option>
                    @endforeach
                </select>
                @error('medicion_id')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-6">
                <label class="form-label"><strong><span style="color:red">*</span> Valor Inicial </strong></label>
                <input type="number" wire:model="valor_ini" class="form-control" placeholder="Valor Inicial"/>
                @error('valor_ini')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-6">
                <label class="form-label"><strong><span style="color:red">*</span> Valor Fin </strong> </label>
                <input type="number" wire:model="valor_fin" class="form-control" placeholder="Valor Final"/>
                @error('valor_fin')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="update()" class="btn btn-primary me-1 mt-2">
                Actualizar
            </x-jet-button>
            <x-jet-button wire:click.prevent="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
