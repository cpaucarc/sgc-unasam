<!-- Modal -->
<x-jet-dialog-modal wire:model="open" maxWidth="lg">
    <x-slot name="title">
        <h3>Agregar Nuevo Subproceso</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div id="form-subprocesos">
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-2">
                    <label class="form-label"><strong><strong style="color: red">*</strong> Código</strong></label>
                    <input type="text" wire:model.defer="codigo" class="form-control" placeholder="Código" maxlength="13"/>
                    @error('codigo')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-12 col-md-3">
                    <label class="form-label"><strong><strong style="color: red">*</strong> Versión</strong></label>
                    <select wire:model.defer="version_id" class=" form-select">
                        <option value="">-- Seleccione --</option>
                        @foreach ($versions as $version)
                            <option value="{{$version->id}}">{{$version->numero}}</option>
                        @endforeach
                    </select>
                    @error('version_id')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-12 col-md-7">
                    <label class="form-label"><strong><strong style="color: red">*</strong> Nombre</strong></label>
                    <input type="text" wire:model.defer="nombre" class="form-control" placeholder="Nombre" maxlength="120"/>
                    @error('nombre')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-10">
                    <label class="form-label"><strong>Área responsable</strong></label>
                    <div wire:ignore>
                        <select wire:model="responsable_area" id="select2" class="form-select">
                            <option value="">-- Seleccione --</option>
                            @foreach ($areas as $area)
                                <option value="{{$area->id}}">{{$area->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('responsable_area')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-12 col-md-2">
                    <label class="form-label"><strong><strong style="color: red">*</strong> Orden</strong></label>
                    <input type="number" wire:model.defer="orden" class="form-control" placeholder="Orden" min="0"
                           maxlength="2" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"/>
                    @error('orden')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-6">
                    <label class="form-label"><strong><strong style="color: red">*</strong> Macroproceso</strong></label>
                        <select wire:model.defer="macroproceso_id" wire:change="obtenerProceso()" class="form-select">
                            <option value="null">-- Seleccione --</option>
                            @foreach ($macroprocesos as $macroproceso)
                                <option value="{{$macroproceso->id}}">{{$macroproceso->nombre}}</option>
                            @endforeach
                        </select>
                    @error('macroproceso_id')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-12 col-md-6">
                    <label class="form-label"><strong><strong style="color: red">*</strong> Proceso</strong></label>
                    <select wire:model.defer="proceso_id" class="select2 form-select">
                        <option value="null">-- Seleccione --</option>
                        @foreach ($procesos as $proceso)
                            <option value="{{$proceso->id}}">{{$proceso->nombre}}</option>
                        @endforeach
                    </select>
                    @error('proceso_id')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-12">
                    <label class="form-label"><strong><strong style="color: red">*</strong> Descripción</strong></label>
                    <textarea wire:model.defer="descripcion" class="form-control" placeholder="Descripción del subproceso" rows="2" maxlength="250"></textarea>
                    @error('descripcion')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-12">
                    <label class="form-label"><strong><strong style="color: red">*</strong> Objetivo</strong></label>
                    <textarea wire:model.defer="objetivo" class="form-control" placeholder="objetivo del subproceso" rows="2" maxlength="250"></textarea>
                    @error('objetivo')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-12">
                    <label class="form-label"><strong><strong style="color: red">*</strong> Alcance</strong></label>
                    <textarea wire:model.defer="alcance" class="form-control" placeholder="Alcance del subproceso" rows="2" maxlength="250"></textarea>
                    @error('alcance')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-12">
                    <label class="form-label"><strong><strong style="color: red">*</strong> Finalidad</strong></label>
                    <textarea wire:model.defer="finalidad" class="form-control" placeholder="Finalidad del subproceso" rows="2" maxlength="250"></textarea>
                    @error('finalidad')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
            <x-jet-button wire:click.prevent="store()" class="btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
