@section('title', __('Subprocesos'))

<div class="card">
	<div class="card-datatable table-responsive">
		<div class="dataTables_wrapper dt-bootstrap5 no-footer">
			<div class="d-flex justify-content-between align-items-center header-actions mx-2 row mt-75">
				<div class="row">
					<div class="col-sm-12 col-md-6">
						<div class="dataTables_length">
							<label>Mostrar
								<select wire:model="cantidad" class="form-select">
									<option value="5">5</option>
									<option value="10">10</option>
									<option value="25">25</option>
									<option value="50">50</option>
									<option value="100">100</option>
								</select> entradas
							</label>
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						<div class="dt-action-buttons d-flex align-items-center justify-content-lg-end justify-content-center flex-md-nowrap flex-wrap">
							<div class="me-1">
								<div id="DataTables_Table_0_filter" class="dataTables_filter">
									<label>
										Buscar:<input type="search" class="form-control" wire:model="search" placeholder="">
									</label>
								</div>
							</div>
							<div class="dt-buttons btn-group flex-wrap">
								@can('create', App\Models\Subproceso::class)
								<x-jet-button wire:click="$set('open',true)" class="btn add-new btn-primary mt-50">
									<span>Agregar subprocesos</span>
								</x-jet-button>
								@endcan
							</div>
						</div>
					</div>
					<div>
						@include('livewire.subprocesos.create')
						@include('livewire.subprocesos.update')
						@include('livewire.subprocesos.show')
					</div>
				</div>
			</div>

			@can('viewAny', App\Models\Subproceso::class)
			<div class="table-responsive px-25">
				<table class="datatables-permissions table dataTable no-footer dtr-column table-bordered" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style="width: 1441px;">
					<thead class="table-light">
						<tr role="row">
							<th class="sorting cursor-pointer" wire:click="ordenar('codigo')">Código</th>
							<th class="sorting cursor-pointer" wire:click="ordenar('version_id')">Versión</th>
							<th class="sorting cursor-pointer" wire:click="ordenar('nombre')">Nombre</th>
							<th class="sorting cursor-pointer" wire:click="ordenar('param_tipo')">Tipo de Proceso</th>
							<th class="sorting cursor-pointer" wire:click="ordenar('responsable_area')">Área Responsable</th>
							<th class="sorting cursor-pointer" wire:click="ordenar('proceso_id')">Proceso</th>
							<th aria-label="Estado">Estado</th>
							<th aria-label="Acciones">Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($subprocesos as $subproceso)
							<tr>
								<td>{{$subproceso->codigo}}</td>
								<td>{{$subproceso->version_id}}</td>
								<td>{{$subproceso->nombre}}</td>
								@if($subproceso->param_tipo)
									<td>{{$subproceso->detalle->detalle}}</td>
								@else
									<td class="text-center">-</td>
								@endif
								@if($subproceso->responsable_area)
									<td>{{$subproceso->area->nombre}}</td>
								@else
									<td class="text-center">-</td>
								@endif
								<td>{{$subproceso->proceso->codigo}}: {{$subproceso->proceso->nombre}}</td>
								@if($subproceso->activo == 1)
									<td>
										<span class="badge rounded-pill badge-light-success" text-capitalized="">Activo</span>
									</td>
								@else
									<td>
										<span class="badge rounded-pill badge-light-secondary" text-capitalized="">Inactivo</span>
									</td>
								@endif
								<td>
									@can('view', $subproceso)
                                    <button wire:click="show({{$subproceso->id}})" class="btn btn-icon btn-flat-secondary waves-effect px-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>

                                    </button>
									@endcan
									@can('update', $subproceso)
									<button wire:click="edit({{$subproceso->id}})" class="btn btn-icon btn-flat-success waves-effect px-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit font-medium-2 text-body">
                                            <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                            <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                        </svg>
                                    </button>
									@endcan
                                	@can('delete', $subproceso)
									<button wire:click="$emit('EliminarSubproceso',{{$subproceso->id}})" class="btn btn-icon btn-flat-danger waves-effect px-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
                                            <polyline points="3 6 5 6 21 6"></polyline>
                                            <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                        </svg>
                                    </button>
									@endcan
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				<div class="d-flex justify-content-between mx-2 row mb-1">
					<div class="col-sm-12 col-md-6">
						<div class="dataTables_info" aria-live="polite">
							Mostrar {{$subprocesos->firstItem()}} a {{$subprocesos->lastItem()}} de {{$subprocesos->total()}} entradas
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						{{$subprocesos->links()}}
					</div>
				</div>
			</div>
			@endcan
		</div>
	</div>
</div>

<script>
	window.onload = function() {

        selectAjax = $('#select2');
        selectAjaxedit = $('#select2-edit');

		asignarSelect2(selectAjax,'100%','Buscar área');
		asignarSelect2(selectAjaxedit,'100%','Buscar área');

		selectAjax.on('change', function () {
			$data = selectAjax.select2("val");
			@this.set('responsable_area', $data);
			console.log('responsable_area: '+$data);
		}); 

		selectAjaxedit.on('change', function () {
			$data = selectAjaxedit.select2("val");
			@this.set('responsable_area', $data);
			console.log('responsable_area: '+$data);
		}); 

		Livewire.on('alertSubproceso', function(datos){
			nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
		});

		Livewire.on('EliminarSubproceso', subprocesoId=>{
			Swal.fire({
					title: '¿Está seguro?',
					text: "¡No podrás revertir esto!",
					icon: 'error',
					showCancelButton: true,
					confirmButtonText: '¡Sí, elimínalo!'
				}).then((result) => {
				if (result.isConfirmed) {
					Livewire.emitTo('subprocesos','destroy',subprocesoId)
				}
			})
		});
	}
</script>
