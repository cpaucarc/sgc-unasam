<!-- Modal -->
<x-jet-dialog-modal wire:model="showMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h1>Detalle Restricción</h1>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-9">
                <label class="form-label"><strong><span style="color: red">*</span> Nombre de la Restricción </strong></label>
                <input
                    type="text"
                    wire:model="restriccion"
                    class="form-control"
                    placeholder="Restricción" maxlength="150" disabled
                />
                @error('restriccion')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-3">
                <label class="form-label"><strong><span style="color: red">*</span> Estado de la Restricción </strong></label>
                <select wire:model="activo" class="form-select" disabled>
                    <option value="">-- Seleccione --</option>
                    <option value="1">ACTIVO</option>
                    <option value="0">INACTIVO</option>
                </select>
                @error('activo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-12">
                <label class="form-label"><strong> Descripción </strong></label>
                <textarea
                    wire:model="descripcion"
                    class="form-control"
                    placeholder="Descripción de la restricción"
                    rows="2" maxlength="450" disabled
                ></textarea>
                @error('descripcion')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="cerrar()" class="btn btn-primary me-1 mt-2">
                Cerrar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
