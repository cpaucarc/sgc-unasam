<div class="card border-black">
    <div class="text-center p-1">
        <h1 class="text-primary mb-0"><strong>LISTADO DE CARACTERIZACIÓN</strong></h1>
    </div>
    <div class="card-body pt-0">
        <div class="row gx-2 border-black rounded-2 p-2 mb-1">
            <div class="col-12 col-md-6">
                <label class="form-label"><strong class="text-primary">NOMBRE DEL MACROPROCESO</strong></label>
                <div wire:ignore>
                    <select wire:model="macroproceso_id" id="macroproceso" class="form-select">
                        <option value="0">-- Seleccione --</option>
                        @foreach ($macroprocesos as $macroproceso)
                            <option value="{{ $macroproceso->id }}">{{ $macroproceso->codigo }}: {{ $macroproceso->nombre }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <label class="form-label"><strong class="text-primary">NOMBRE DEL PROCESO</strong></label>
                <select wire:model="proceso_id" id="proceso" class="form-select">
                    <option value="0">-- Seleccione --</option>
                    @foreach ($procesos as $proceso)
                        <option value="{{ $proceso->id }}">{{ $proceso->codigo }}: {{ $proceso->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-12 col-md-2 text-end">
                <label class="form-label"><strong class="text-primary">&nbsp;</strong></label><br>
                <button class="btn btn-primary" wire:click="verMacroprocesos">Limpiar</button>
            </div>
        </div>
        <div class="row gx-2 border-black rounded-2 p-2">
            <div class="card card-transaction px-0 mb-0">
                <h2>
                    LISTA DE {{ $tipo_proceso }}
                </h2>
                <table class="table b-table">
                    @foreach ($datos as $dato)
                        <tr>
                            <td><strong>{{ $dato->codigo }}:</strong> {{ $dato->nombre }}</td>
                            @switch($tipo_proceso)
                                @case('MACROPROCESOS')
                                    <td class="d-flex justify-content-between">
                                        <a href="{{ route('macroprocesos.caracterizacion', ['macroproceso' => $dato->id]) }}" class="btn btn-primary"
                                           target="_blank">Ver Macroproceso</a>
                                        <div class="btn-group">
                                            <a href="{{ route('reportes.caracterizacion.excel', $dato->id) }}" class="btn btn-icon btn-outline-success"
                                               data-toggle="tooltip"
                                               data-placement="top" title="Descargar">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" viewBox="0 0 24 24"
                                                     stroke="currentColor" stroke-width="2">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                          d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4" />
                                                </svg>
                                                Caracterización
                                            </a>
                                            <a href="{{ route('reportes.macroprocesos.excel', $dato->id) }}" class="btn btn-icon btn-outline-info"
                                               data-toggle="tooltip"
                                               data-placement="top" title="Descargar">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" viewBox="0 0 24 24"
                                                     stroke="currentColor" stroke-width="2">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                          d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4" />
                                                </svg>
                                                Indicadores
                                            </a>
                                        </div>
                                    </td>
                                @break

                                @case('PROCESOS')
                                    <td><a href="{{ route('procesos.caracterizacion', ['proceso' => $dato->id]) }}" class="btn btn-primary" target="_blank">Ver
                                            Proceso</a></td>
                                @break

                                @case('SUBPROCESOS')
                                    <td><a href="#" class="btn btn-primary" target="_blank">Ver Subproceso</a>

                                    </td>
                                @break
                            @endswitch

                        </tr>
                    @endforeach

                </table>
            </div>
        </div>
    </div>
    <script>
        function scriptCaracterizacion() {
            selectMacroproceso = $('#macroproceso');

            asignarSelect2(selectMacroproceso, '100%', 'Buscar macroproceso');
            selectMacroproceso.on('change', function() {
                $data = selectMacroproceso.select2("val");
                @this.set('macroproceso_id', $data);
                Livewire.emitTo('caracterizacion', 'verProcesos');
                console.log($data);
            });

            selectProceso = $('#proceso');

            asignarSelect2(selectProceso, '100%', 'Buscar proceso');

            Livewire.on('mostrarProcesos', function() {
                asignarSelect2(selectProceso, '100%', 'Buscar proceso');
            });
            Livewire.on('mostrarMacroprocesos', function() {
                asignarSelect2(selectMacroproceso, '100%', 'Buscar proceso');
            });

            selectProceso.on('change', function(e) {
                $data = selectProceso.select2("val");
                @this.set('proceso_id', $data);
                Livewire.emitTo('caracterizacion', 'verSubprocesos');
                console.log($data);
            });

            // Livewire.on('resetearSelect', function () {
            //     selectMacroproceso.val('NULL');
            //     selectMacroproceso.trigger('change');
            // });
        }
    </script>
</div>
