<!-- Modal -->
<x-jet-dialog-modal wire:model="updateRiesgo" maxWidth="lg">
    <x-slot name="title">
        <h3>Actualizar Riesgo</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div id="form-Riesgo">
            <form>
                <div class="row gy-1 gx-2 pb-1">
                    <div class="col-12 col-md-12">
                        <input
                            type="text"
                            wire:model="nombre"
                            class="form-control"
                            placeholder="Nombre del riesgo"
                        />  
                        @error('nombre')
                            <span class="text-sm text-danger">{{$message}}</span>
                        @enderror
                    </div> 
                </div>            
                <div class="row gy-1 gx-2 pb-1">
                    <div class="col-12 col-md-12">
                        <label class="form-label"><strong>Descripción</strong></label>
                        <textarea 
                            wire:model="descripcion"
                            class="form-control"
                            placeholder="Escribir la descripción del riesgo"
                            rows="2"
                        ></textarea>
                        @error('descripcion')
                            <span class="text-sm text-danger">{{$message}}</span>
                        @enderror
                    </div> 
                </div>            
            </form>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click="cancelarRiesgo()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
            <x-jet-button wire:click.prevent="updateRiesgo()" class="btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
        </div>        
    </x-slot>
</x-jet-dialog-modal>