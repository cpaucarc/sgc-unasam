<div class="col-12 col-md-5">
    <div class="table-responsive">
        <table class="table" style="border: 2px solid #636e72;  border-collapse: collapse;">
            <tr style="border: 1px solid black;  border-collapse: collapse;">
                <td class="py-0 px-1" style="background-color: #dfe6e9;border: 2px solid #636e72; border-collapse: collapse;">
                    CÓDIGO
                </td>
                <td class="py-0 px-1" colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                    {{ $macroproceso->codigo }}
                </td>
            </tr>
            <tr>
                <td class="py-0 px-1" style="background-color: #dfe6e9;border: 2px solid #636e72; border-collapse: collapse;">
                    VERSIÓN
                </td>
                <td class="py-0 px-1" class="inputcentrado">
                    <select wire:model="version" class="form-select" wire:change="seleccionarVersion">
                        @if ($versiones)
                            @foreach ($versiones->sortBy('version.numero') as $version)
                                <option value="{{ $version->id }}">{{ $version->version->numero }}</option>
                            @endforeach
                        @endif
                    </select>
                </td>
                <td class="py-0 px-1" style="background-color: #dfe6e9; border: 2px solid #636e72; border-collapse: collapse;">
                    VIGENCIA
                </td>
                <td class="py-0 px-1" class="inputcentrado">{{ date('Y-m-d', strtotime($macroproceso->version->fecha_vigencia)) }}</td>
            </tr>
        </table>
    </div>
</div>
