<div>
    <h4 class="text-primary"><strong>INDICADORES (KPI'S):</strong></h4>
    <div class="table-responsive pb-2">
        <table class="table table-bordered" role="grid">
            <tbody>
                <tr>
                    <td class="border-black" width="10%" style="background-color: #dfe6e9; border-collapse: collapse;" class="text-center">
                        Eficiencia
                    </td>
                    @php
                        $count_eficiencia = 0;
                        $count_eficacia = 0;
                    @endphp
                    <td class="border-black">
                        @if($eficiencias->count()==0)
                            <strong class="text-danger">NO HAY REGISTRO</strong>
                        @endif
                        @foreach ($eficiencias as $eficiencia)
                               
                            <a href="{{route('show.ficha',['indicador'=>$eficiencia->id,'anio'=>date('Y')])}}" target="_blank">({{$loop->iteration}}) {{$eficiencia->nombre}}</a>
                            
                            @if($loop->iteration>1 or $loop->iteration<$eficiencias->count())
                                <br>
                            @endif   
                            @php
                                $count_eficiencia = $loop->iteration;
                            @endphp                   
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td class="border-black" width="10%" style="background-color: #dfe6e9; border-collapse: collapse;" class="text-center">
                        Eficacia                        
                    </td>
                    <td class="border-black">
                        @if($eficacias->count()==0)
                            <strong class="text-danger">NO HAY REGISTRO</strong>
                        @endif
                        @foreach ($eficacias as $eficacia)                            
                              
                            <a href="{{route('show.ficha',['indicador'=>$eficacia->id,'anio'=>date('Y')])}}" target="_blank">({{$count_eficiencia+$loop->iteration}}) {{$eficacia->nombre}}</a>
                            
                            @if($loop->iteration>1 or $loop->iteration<$eficacias->count())
                                <br>
                            @endif   
                            @php
                                $count_eficacia = $loop->iteration;
                            @endphp                                            
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td class="border-black" width="10%" style="background-color: #dfe6e9; border-collapse: collapse;" class="text-center">
                        Efectividad
                    </td>
                    <td class="border-black">
                        @if($efectividads->count()==0)
                            <strong class="text-danger">NO HAY REGISTRO</strong>
                        @endif
                        @foreach ($efectividads as $efectividad)
                            <a href="{{route('show.ficha',['indicador'=>$efectividad->id,'anio'=>date('Y')])}}" target="_blank">({{$count_eficiencia + $count_eficacia + $loop->iteration}}) {{$efectividad->nombre}}</a>
                            @if($loop->iteration>1 or $loop->iteration<$efectividads->count())
                                <br>
                            @endif                        
                        @endforeach
                    </td>
                </tr> 
            </tbody>
        </table>
    </div>
</div>