<!-- Modal -->
<x-jet-dialog-modal wire:model="openRecurso" maxWidth="lg">
    <x-slot name="title">
        @switch($tipoRecurso)
            @case(9001)
                <h3>Agregar Nuevo Talento Humano</h3>
                @break
            @case(9002)
                    <h3>Agregar Nueva Infraestructura</h3>
                @break
            @case(9003)
                    <h3>Agregar Nuevo Software</h3>
                @break
            @case(9004)
                    <h3>Agregar Otros Recursos</h3>
                @break
            @default
        @endswitch
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div id="form-recursos">
            <form>
                <div class="mb-1 row">
                    <label for="colFormLabelLg" class="col-sm-2 col-form-label-lg"><h5>Seleccione:</h5></label>
                    <div class="col-sm-10">
                        <select id="lista-recurso" class="form-control" multiple>
                            <option value="NULL" disabled>--Seleccione--</option>
                            @foreach ($lista_recursos as $lista_recurso)
                                <option value="{{$lista_recurso->id}}">{{$lista_recurso->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </form>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
            <x-jet-button wire:click.prevent="$emit('asignarRecursos')" class="btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
        </div>
    </x-slot>
</x-jet-dialog-modal>
