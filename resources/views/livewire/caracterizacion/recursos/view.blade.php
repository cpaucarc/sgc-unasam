<div>
    <div>
        <ul class="nav nav-pills card-header-pills ms-0" id="tab-recurso" role="tablist">
            <li class="nav-item">
                <a class="nav-link {{ $tab_recurso == 'talento' ? 'active' : '' }}" wire:click="$set('tab_recurso', 'talento')" id="talento-tab" data-bs-toggle="tab" href="#talento"
                    aria-controls="talento" role="tab" aria-selected="true" >Talento Humano</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ $tab_recurso == 'recurso' ? 'active' : '' }}" wire:click="$set('tab_recurso', 'recurso')" id="recurso-tab" data-bs-toggle="tab" href="#recurso"
                    aria-controls="recurso" role="tab" aria-selected="false">Otros recursos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ $tab_recurso == 'infraestructura' ? 'active' : '' }}" wire:click="$set('tab_recurso', 'infraestructura')" id="infraestructura-tab" data-bs-toggle="tab" href="#infraestructura"
                    aria-controls="infraestructura" role="tab" aria-selected="false">Infraestructura</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ $tab_recurso == 'software' ? 'active' : '' }}" wire:click="$set('tab_recurso', 'software')" id="software-tab" data-bs-toggle="tab" href="#software"
                    aria-controls="software" role="tab" aria-selected="false">SW. y Comunicaciones</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane {{ $tab_recurso == 'talento' ? 'active' : '' }}" id="talento-tab" aria-labelledby="talento-tab" role="tabpanel">
                <div class="card-header pt-0" style="padding-left: 0px;">
                    <h3 class="card-title text-secondary"><strong>Lista de Talento Humano:</strong></h3>
                    <div class="heading-elements">            
                        <ul class="list-inline mb-0">
                            <li>
                                @can('create', App\Models\Recurso::class)
                                <button type="button" class="btn btn-outline-primary waves-effect" wire:click="openRecurso('9001')">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                                    <span>Agregar</span>
                                </button>
                                @endcan
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered" role="grid">
                        <thead>
                            <tr>
                                <th class="text-center px-0" scope="col" style="width: 50px;">Nro.</th>
                                <th class="text-center px-0" rowspan="2">Talento Humano</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($talentos as $talento)
                                <tr>
                                    <td class="py-0 px-0 text-center"><span>{{$loop->iteration}}</span></td>
                                    <td class="py-0 px-1">
                                        <div class="d-flex justify-content-between">
                                            <div class="d-flex align-items-center">                                                 
                                                <span>{{$talento->nombre}}</span>
                                            </div>
                                            @can('delete', $talento)
                                            <div class="d-flex align-items-center">
                                                <button wire:click="$emit('EliminarRecurso',{{$talento->id}})" type="button" class="btn btn-icon rounded-circle btn-flat-danger waves-effect">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                </button>                                           
                                            </div> 
                                            @endcan   
                                        </div>                                           
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane {{ $tab_recurso == 'recurso' ? 'active' : '' }}" id="recurso-tab" aria-labelledby="recurso-tab" role="tabpanel">
                <div class="card-header pt-0" style="padding-left: 0px;">
                    <h3 class="card-title text-secondary"><strong>Lista de Otros Recursos:</strong></h3>
                    <div class="heading-elements">            
                        <ul class="list-inline mb-0">
                            <li>
                                @can('create', App\Models\Recurso::class) 
                                <button type="button" class="btn btn-outline-primary waves-effect" wire:click="openRecurso('9004')">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                                    <span>Agregar</span>
                                </button>
                                @endcan
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered" role="grid">
                        <thead>
                            <tr>
                                <th class="text-center px-0" scope="col" style="width: 50px;">Nro.</th>
                                <th class="text-center px-0" rowspan="2">Otros Recursos</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($recursos as $recurso)
                                <tr>
                                    <td class="py-0 px-0 text-center"><span>{{$loop->iteration}}</span></td>
                                    <td class="py-0 px-1">
                                        <div class="d-flex justify-content-between">
                                            <div class="d-flex align-items-center">                                                 
                                                <span>{{$recurso->nombre}}</span>
                                            </div>
                                            @can('delete', $recurso)
                                            <div class="d-flex align-items-center">
                                                <button wire:click="$emit('EliminarRecurso',{{$recurso->id}})" type="button" class="btn btn-icon rounded-circle btn-flat-danger waves-effect">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                </button>                                               
                                            </div>
                                            @endcan
                                        </div>                                           
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane {{ $tab_recurso == 'infraestructura' ? 'active' : '' }}" id="infraestructura-tab" aria-labelledby="infraestructura-tab" role="tabpanel">
                <div class="card-header pt-0" style="padding-left: 0px;">
                    <h3 class="card-title text-secondary"><strong>Lista de Infraestructura:</strong></h3>
                    <div class="heading-elements">            
                        <ul class="list-inline mb-0">
                            <li>
                                @can('create', App\Models\Recurso::class) 
                                <button type="button" class="btn btn-outline-primary waves-effect" wire:click="openRecurso('9002')">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                                    <span>Agregar</span>
                                </button>
                                @endcan
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered" role="grid">
                        <thead>
                            <tr>
                                <th class="text-center px-0" scope="col" style="width: 50px;">Nro.</th>
                                <th class="text-center px-0" rowspan="2">Infraestructura</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($infraestructuras as $infraestructura)
                                <tr>
                                    <td class="py-0 px-0 text-center"><span>{{$loop->iteration}}</span></td>
                                    <td class="py-0 px-1">
                                        <div class="d-flex justify-content-between">
                                            <div class="d-flex align-items-center">                                                 
                                                <span>{{$infraestructura->nombre}}</span>
                                            </div>
                                            @can('delete', $infraestructura)
                                            <div class="d-flex align-items-center">
                                                <button wire:click="$emit('EliminarRecurso',{{$infraestructura->id}})" type="button" class="btn btn-icon rounded-circle btn-flat-danger waves-effect">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                </button>                                               
                                            </div>
                                            @endcan
                                        </div>                                           
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane {{ $tab_recurso == 'software' ? 'active' : '' }}" id="software-tab" aria-labelledby="software-tab" role="tabpanel">
                <div class="card-header pt-0" style="padding-left: 0px;">
                    <h3 class="card-title text-secondary"><strong>Lista de Softwares y Comunicaciones:</strong></h3>
                    <div class="heading-elements">            
                        <ul class="list-inline mb-0">
                            <li>
                                @can('create', App\Models\Recurso::class) 
                                <button type="button" class="btn btn-outline-primary waves-effect" wire:click="openRecurso('9003')">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                                    <span>Agregar</span>
                                </button>
                                @endcan
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered" role="grid">
                        <thead>
                            <tr>
                                <th class="text-center px-0" scope="col" style="width: 50px;">Nro.</th>
                                <th class="text-center px-0" rowspan="2">Software y Comunicaciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($softwares as $software)
                                <tr>
                                    <td class="py-0 px-0 text-center"><span>{{$loop->iteration}}</span></td>
                                    <td class="py-0 px-1">
                                        <div class="d-flex justify-content-between">
                                            <div class="d-flex align-items-center">                                                 
                                                <span>{{$software->nombre}}</span>
                                            </div>
                                            @can('delete', $software)
                                            <div class="d-flex align-items-center">
                                                <button wire:click="$emit('EliminarRecurso',{{$software->id}})" type="button" class="btn btn-icon rounded-circle btn-flat-danger waves-effect">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                </button>                                               
                                            </div>
                                            @endcan
                                        </div>                                           
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('livewire.caracterizacion.recursos.create')
        @include('livewire.caracterizacion.recursos.update')
    </div>
    <script>
        function scriptRecurso() {
            selectRecurso = $('#lista-recurso');  

            Livewire.on('alertRecurso', function(datos){
                nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
            });
            Livewire.on('obtenerRecursos', function(){
                selectRecurso.select2({
                    placeholder: 'Seleccione el Recurso',
                    allowClear: true,
                    language: {
                        noResults: function() {
                            return "No hay resultado";        
                        },
                        searching: function() {
                            return "Buscando...";
                        }
                    }
                });
            });
            Livewire.on('asignarRecursos', function(){
                $valores = selectRecurso.select2('data');
                @this.set('selectRecurso', $valores);
                Livewire.emitTo('caracterizacion.recursos','storeRecurso')
            });	
            Livewire.on('EliminarRecurso', recurdoID=>{
                Swal.fire({
                    title: '¿Está seguro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'error',
                    showCancelButton: true,
                    confirmButtonText: '¡Sí, elimínalo!',
					cancelButtonText: 'Cancelar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emitTo('caracterizacion.recursos','destroy',recurdoID)            
                    }
                })
            });	
        }
    </script>
</div>