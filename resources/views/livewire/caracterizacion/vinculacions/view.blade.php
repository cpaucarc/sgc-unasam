<div>
    <div class="card mb-0">
        <div class="card-header pt-0">
            <h4 class="card-title text-primary"><strong>{{tipoProceso($nivel_proceso)}}S DIRECTAMENTE VINCULADOS:</strong></h4>
            <div class="heading-elements">            
                <ul class="list-inline mb-0">
                    <li>
                        @can('create', App\Models\Vinculacion::class)
                        <button type="button" class="btn btn-outline-primary waves-effect" wire:click="openModal()">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                            <span>Agregar</span>
                        </button>
                        @endcan
                    </li>
                </ul>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered" role="grid">
                <thead>
                    <tr>
                        <th class="text-center px-0" scope="col" style="width: 50px;">Nro.</th>
                        <th class="text-center px-0" rowspan="2">Nombre de Proceso</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($vinculaciones as $vinculacion)
                        <tr>
                            <td class="py-0 px-0 text-center"><span>{{$loop->iteration}}</span></td>
                            <td class="py-0 px-1">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex align-items-center">                                                 
                                        <span>{{getNombreProceso($vinculacion->nivel_proceso,$vinculacion->vin_proceso_id)}}</span>
                                    </div>
                                    
                                    <div>
                                        @can('delete', $vinculacion)
                                        <button wire:click="$emit('EliminarVinculacion',{{$vinculacion->id}})" type="button" class="btn btn-icon rounded-circle btn-flat-danger waves-effect">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                        </button>
                                        @endcan                                               
                                    </div>
                                    
                                </div>                                           
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div>
        @include('livewire.caracterizacion.vinculacions.create')
    </div>
    <script>
        function scriptVinculacion() {
            selectMultiple = $('#vinculos');
            Livewire.on('obtenerVinculos', function(){
                selectMultiple.select2({
                    placeholder: 'Seleccione Macroproceso',
                    allowClear: true,
                    language: {
                        noResults: function() {
                            return "No hay resultado";        
                        },
                        searching: function() {
                            return "Buscando...";
                        }
                    }
                });
            });
            Livewire.on('alertVinculacion', function(datos){
                nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
            });            
            Livewire.on('asignarValorVinculacion', function(){
                $valores = selectMultiple.select2('data');
                @this.set('multiselect', $valores);
                console.log('entra');
                Livewire.emitTo('caracterizacion.vinculacions','store')
            });	
            Livewire.on('EliminarVinculacion', vinculacionID=>{
                Swal.fire({
                    title: '¿Está seguro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'error',
                    showCancelButton: true,
                    confirmButtonText: '¡Sí, elimínalo!',
					cancelButtonText: 'Cancelar'
                }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('caracterizacion.vinculacions','destroy',vinculacionID)            
                }
                })
            });	
        }
    </script>
</div>