<!-- Modal -->
<x-jet-dialog-modal wire:model="open" maxWidth="lg">
    <x-slot name="title">
        <h3>Agregar Nueva Vinculación</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div id="form-vinculaciones">
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12">
                    <label class="form-label"><strong>Macroproceso a Vincular</strong></label>
                    <div>
                        <select wire:model="vinculacion" id="vinculos" class="form-control" multiple="multiple">
                            <option value="NULL" disabled>--Seleccione--</option>
                            @foreach ($vinculos as $vinculo)
                                <option value="{{$vinculo->id}}">{{$vinculo->codigo}}: {{$vinculo->nombre}}</option>
                            @endforeach                 
                        </select>
                    </div>
                    @error('vinculacion')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div> 
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
            <x-jet-button wire:click.prevent="$emit('asignarValorVinculacion')" class="btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
        </div>        
    </x-slot>
</x-jet-dialog-modal>