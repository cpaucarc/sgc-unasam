<!-- Modal -->
<x-jet-dialog-modal wire:model="openRequisito" maxWidth="lg">
    <x-slot name="title">
        @switch($tprequisito)
            @case(7001)
                <h3>Agregar Nuevo Requisito de Cliente</h3>
                @break
            @case(7002)
                    <h3>Agregar Nuevo Requisito de ISO</h3>
                @break
            @case(7003)
                    <h3>Agregar Nuevo Requisito Legal</h3>
                @break
            @default
        @endswitch
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div id="form-requisitos">
            <form>
                <div class="row gy-1 gx-2 pb-1">
                    <div class="col-12 col-md-12">
                        <label class="form-label"><strong>Descripción</strong></label>
                        <textarea 
                            wire:model="descripcion"
                            class="form-control"
                            placeholder="Escribir la descripción de requisito"
                            rows="2"
                        ></textarea>
                        @error('descripcion')
                            <span class="text-sm text-danger">{{$message}}</span>
                        @enderror
                    </div> 
                </div>            
            </form>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
            <x-jet-button wire:click.prevent="storeRequisito()" class="btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
        </div>        
    </x-slot>
</x-jet-dialog-modal>