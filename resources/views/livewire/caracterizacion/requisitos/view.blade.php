<div>
    <div>
        <ul class="nav nav-pills card-header-pills ms-0" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link {{ $tab == 'cliente' ? 'active' : '' }}" wire:click="$set('tab', 'cliente')" id="cliente-tab" data-bs-toggle="tab" href="#cliente"
                   aria-controls="cliente" role="tab" aria-selected="true">Del Cliente</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ $tab == 'iso' ? 'active' : '' }}" wire:click="$set('tab', 'iso')" id="iso-tab" data-bs-toggle="tab" href="#iso"
                   aria-controls="iso"
                   role="tab" aria-selected="false">Norma ISO</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ $tab == 'legal' ? 'active' : '' }}" wire:click="$set('tab', 'legal')" id="legal-tab" data-bs-toggle="tab" href="#legal"
                   aria-controls="about" role="tab" aria-selected="false">Legales</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane {{ $tab == 'cliente' ? 'active' : '' }}" id="cliente" aria-labelledby="cliente-tab" role="tabpanel">
                <div class="card border-primary px-1 py-1 mb-0">
                    <div class="row">
                        <div class="d-flex justify-content-between pb-1">
                            <div class="d-flex align-items-center">
                                <label><strong>Mostrar</strong></label>
                                <select wire:model="cant_clientes" class="form-select mx-1 col-1" style="width: 70px;">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="50">50</option>
                                    <option value="{{ $clientes->total() }}">Todo</option>
                                </select>
                                <label><strong>entradas</strong></label>
                            </div>
                            <div class="d-flex align-items-center">
                                @can('create', App\Models\Requisito::class)
                                    <button type="button" class="btn btn-outline-primary waves-effect" wire:click="openModalRequisito('7001')">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-plus-circle">
                                            <circle cx="12" cy="12" r="10"></circle>
                                            <line x1="12" y1="8" x2="12" y2="16"></line>
                                            <line x1="8" y1="12" x2="16" y2="12"></line>
                                        </svg>
                                        <span>Agregar</span>
                                    </button>
                                @endcan
                            </div>
                        </div>
                        <div class="col-12">
                            @foreach ($clientes as $cliente)
                                <hr style="margin-top: 1px; margin-bottom: 1px;">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex align-items-center">
                                        {{ $cliente->descripcion }}
                                    </div>
                                    <div class="d-flex align-items-center">
                                        @can('update', $cliente)
                                            @can('manejar', $area)
                                                <button wire:click="editRequisito({{ $cliente->id }},'7001')" type="button"
                                                        class="btn btn-icon rounded-circle btn-flat-primary waves-effect">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none"
                                                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                         class="feather feather-edit">
                                                        <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                                        <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                                    </svg>
                                                </button>
                                            @endcan
                                        @endcan
                                        <button wire:click="$emit('EliminarRequisito',{{ $cliente->id }})" type="button"
                                                class="btn btn-icon rounded-circle btn-flat-danger waves-effect">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                 class="feather feather-trash-2">
                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                <line x1="10" y1="11" x2="10" y2="17"></line>
                                                <line x1="14" y1="11" x2="14" y2="17"></line>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            @endforeach
                            <hr style="margin-top: 1px; margin-bottom: 1px;">
                        </div>
                        <div class="d-flex justify-content-between mx-2 row mt-1">
                            <div class="col-sm-12 col-md-6">
                                <div class="dataTables_info" aria-live="polite">
                                    Mostrar {{ $clientes->firstItem() }} a {{ $clientes->lastItem() }} de {{ $clientes->total() }} entradas
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                {{ $clientes->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane {{ $tab == 'iso' ? 'active' : '' }}" id="iso" aria-labelledby="iso-tab" role="tabpanel">
                <div class="card border-primary px-1 py-1 mb-0">
                    <div class="row">
                        <div class="d-flex justify-content-between pb-1">
                            <div class="d-flex align-items-center">
                                <label><strong>Mostrar</strong></label>
                                <select wire:model="cant_isos" class="form-select mx-1 col-1" style="width: 70px;">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="{{ $isos->total() }}">todo</option>
                                </select>
                                <label><strong>entradas</strong></label>
                            </div>
                            <div class="d-flex align-items-center">
                                @can('create', App\Models\Requisito::class)
                                <button type="button" class="btn btn-outline-primary waves-effect" wire:click="openModalRequisito('7002')">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                         class="feather feather-plus-circle">
                                        <circle cx="12" cy="12" r="10"></circle>
                                        <line x1="12" y1="8" x2="12" y2="16"></line>
                                        <line x1="8" y1="12" x2="16" y2="12"></line>
                                    </svg>
                                    <span>Agregar</span>
                                </button>
                                @endcan
                            </div>
                        </div>
                        <div class="col-12">
                            @foreach ($isos as $iso)
                                <hr style="margin-top: 1px; margin-bottom: 1px;">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex align-items-center">
                                        {{ $iso->descripcion }}
                                    </div>
                                    <div class="d-flex align-items-center">
                                        @can('update', $iso)
                                        <button wire:click="editRequisito({{$iso->id}},'7002')" type="button" class="btn btn-icon rounded-circle btn-flat-primary waves-effect">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg>
                                        </button>
                                        @endcan
                                        @can('delete', $iso)
                                        <button wire:click="$emit('EliminarRequisito',{{$iso->id}})" type="button" class="btn btn-icon rounded-circle btn-flat-danger waves-effect">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                        </button>
                                        @endcan
                                    </div>
                                </div>
                            @endforeach
                            <hr style="margin-top: 1px; margin-bottom: 1px;">
                        </div>
                        <div class="d-flex justify-content-between mx-2 row mt-1">
                            <div class="col-sm-12 col-md-6">
                                <div class="dataTables_info" aria-live="polite">
                                    Mostrar {{ $isos->firstItem() }} a {{ $isos->lastItem() }} de {{ $isos->total() }} entradas
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                {{ $isos->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane {{ $tab == 'legal' ? 'active' : '' }}" id="legal" aria-labelledby="legal-tab" role="tabpanel">
                <div class="card border-primary px-1 py-1 mb-0">
                    <div class="row">
                        <div class="d-flex justify-content-between pb-1">
                            <div class="d-flex align-items-center">
                                <label><strong>Mostrar</strong></label>
                                <select wire:model="cant_legals" class="form-select mx-1 col-1" style="width: 70px;">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="{{ $legals->total() }}">todo</option>
                                </select>
                                <label><strong>entradas</strong></label>
                            </div>
                            <div class="d-flex align-items-center">
                                @can('create', App\Models\Requisito::class)
                                <button type="button" id="btn-legal" class="btn btn-outline-primary waves-effect" wire:click="openModalRequisito('7003')">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                         class="feather feather-plus-circle">
                                        <circle cx="12" cy="12" r="10"></circle>
                                        <line x1="12" y1="8" x2="12" y2="16"></line>
                                        <line x1="8" y1="12" x2="16" y2="12"></line>
                                    </svg>
                                    <span>Agregar</span>
                                </button>
                                @endcan
                            </div>
                        </div>
                        <div class="col-12">
                            @foreach ($legals as $legal)
                                <hr style="margin-top: 1px; margin-bottom: 1px;">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex align-items-center">
                                        {{ $legal->descripcion }}
                                    </div>
                                    <div class="d-flex align-items-center">
                                        @can('update', $legal)
                                        <button wire:click="editRequisito({{$legal->id}},'7003')" type="button" class="btn btn-icon rounded-circle btn-flat-primary waves-effect">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg>
                                        </button>
                                        @endcan
                                        @can('delete', $legal)
                                        <button wire:click="$emit('EliminarRequisito',{{$legal->id}})" type="button" class="btn btn-icon rounded-circle btn-flat-danger waves-effect">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                        </button>
                                        @endcan
                                    </div>
                                </div>
                            @endforeach
                            <hr style="margin-top: 1px; margin-bottom: 1px;">
                        </div>
                        <div class="mx-2 row mt-1">
                            <div class="col-sm-12 col-md-12">
                                <div class="dataTables_info" aria-live="polite">
                                    Mostrar {{ $legals->firstItem() }} a {{ $legals->lastItem() }} de {{ $legals->total() }} entradas
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                {{ $legals->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('livewire.caracterizacion.requisitos.create')
        @include('livewire.caracterizacion.requisitos.update')
    </div>
    <script>
        function scriptRequisito() {
            Livewire.on('alertRequisito', function(datos) {
                nitificacion(datos['tipo'], datos['mensaje'], ' ¡Acción realizada!', 2000);
            });
            Livewire.on('EliminarRequisito', requisitoID => {
                Swal.fire({
                    title: '¿Está seguro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'error',
                    showCancelButton: true,
                    confirmButtonText: '¡Sí, elimínalo!',
                    cancelButtonText: 'Cancelar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emitTo('caracterizacion.requisitos', 'destroy', requisitoID)
                    }
                })
            });
        }
    </script>
</div>
