<!-- Modal -->
<x-jet-dialog-modal wire:model="showUpload" maxWidth="lg">
    <x-slot name="title">
        @if($tipoEntrada == 0)
            <h3>Subir Archivos de Entrada</h3>
        @else
            <h3>Subir Archivos de Salida</h3>            
        @endif
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div>
            <strong class="text-primary">{{tipoProceso($nivel_proceso)}}:</strong> <span>{{getNombreProceso($nivel_proceso, $proceso_id)}}</span>
        </div>
        <div>
            <strong class="text-primary">DOCUMENTO:</strong> <span>{{$nom_documento}}</span>
        </div>
        <div>
            <strong class="text-primary">TIEMPO DE CONSERVACIÓN:</strong> <span>{{$conservacion}}</span>
        </div>
        <div class="mt-25">
            <form id="form-archivo">
                <div class="row gy-1 gx-2 pb-1">
                    <div class="col-12 col-md-4">
                        <label class="form-label"><span class="text-danger">* </span><strong>Año</strong></label>
                        <select wire:model="anio_id" id="anio" class="form-select">
                            <option value="">-- Seleccione --</option>
                            @foreach ($anios as $anio)
                                <option value="{{$anio->id}}">{{$anio->anio}}</option>
                            @endforeach
                        </select>
                        @error('anio_id')
                            <span class="text-sm text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="col-12 col-md-4">
                        @if ($disabled == 'disabled')
                            <label class="form-label"><strong>Periodo {{$nom_conservacion}}</strong></label>
                        @else
                            <label class="form-label"><span class="text-danger">* </span><strong>Periodo {{$nom_conservacion}}</strong></label>
                        @endif
                        <select wire:model="param_periodo" id="periodo" {{$disabled}} class="form-select">
                            <option value="">-- Seleccione --</option>
                            @foreach ($periodos as $periodo)
                                <option value="{{$periodo->id}}">{{$periodo->detalle}}</option>
                            @endforeach
                        </select>
                        @error('param_periodo')
                            <span class="text-sm text-danger">{{$message}}</span>
                        @enderror
                    </div> 
                    <div class="col-12 col-md-4 {{$ocultar}}">
                        @if ($disabled2 == 'disabled')
                            <label class="form-label"><strong>Semana</strong></label>
                        @else
                            <label class="form-label"><span class="text-danger">* </span><strong>Semana</strong></label>
                        @endif
                        <select wire:model="semanal_mes" id="periodo" {{$disabled2}} class="form-select">
                            <option value="">-- Seleccione --</option>
                            @foreach ($semanas as $semana)
                                <option value="{{$semana->id}}">{{$semana->detalle}}</option>
                            @endforeach
                        </select>
                        @error('semanal_mes')
                            <span class="text-sm text-danger">{{$message}}</span>
                        @enderror
                    </div> 
                </div>
                <div class="row gy-1 gx-2 pb-1">
                    <div class="col-12 col-md-4">
                        <label class="form-label"><span class="text-danger">* </span><strong>Inicio de Vigencia</strong></label>
                        <input wire:model.defer="fechaIniVigencia" type="text" id="inicioVigencia" placeholder="dd-mm-aaa" class="form-control fechaflatpickr">
                        @error('fechaIniVigencia')
                            <span class="text-sm text-danger">{{$message}}</span>
                        @enderror
                    </div> 
                    <div class="col-12 col-md-4">
                        <label class="form-label"><strong>Fin de Vigencia</strong></label>
                        <input wire:model.defer="fechaFinVigencia" type="text" id="finVigencia" placeholder="dd-mm-aaa" class="form-control fechaflatpickr">
                        @error('fechaFinVigencia')
                            <span class="text-sm text-danger">{{$message}}</span>
                        @enderror
                    </div> 
                </div>
                <div class="row gy-1 gx-2 pb-1">
                    <div class="col-12 col-md-12">
                        <label class="form-label"><span class="text-danger">* </span><strong>Descripción</strong></label>
                        <textarea 
                            wire:model.defer="descripcion"
                            class="form-control"
                            placeholder="{{$placeholder}}"
                            rows="2"
                        ></textarea>
                        @error('descripcion')
                            <span class="text-sm text-danger">{{$message}}</span>
                        @enderror
                    </div> 
                </div>
                <div class="row gy-1 gx-2 pb-1">
                    <div class="col-12 col-md-12">
                        <label class="form-label"><span class="text-danger">* </span><strong>Seleccione el Archivo</strong></label>
                        <input type="file" wire:model="file_upload" class="form-control">
                        @error('file_upload')
                            <span class="text-sm text-danger">{{$message}}</span>
                        @enderror
                    </div> 
                </div>  
                <div class="row gy-1 gx-2 pb-1">
                    <div wire:loading wire:target="file_upload" class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">El archivo se está cargando...</h4>
                        <div class="alert-body">
                            Espere un momento hasta que la archivo se haya guardado.
                        </div>  
                    </div>
                </div> 
            </form>
        </div>
    </x-slot>
    <x-slot name="footer">     
        <div class="col-12 text-center">
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
            <x-jet-button wire:click.prevent="storeArchivo()" class="btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
        </div>     
    </x-slot>
</x-jet-dialog-modal>