<div>    
    <div class="card">
        <div class="card-header" style="padding-left: 0px;">
            <h4 class="card-title text-primary"><strong>ENTRADA - ACTIVIDADES - SALIDA:</strong></h4>
            <div class="heading-elements">            
                <ul class="list-inline mb-0">
                    <li>
                        <button type="button" class="btn btn-outline-primary waves-effect" wire:click="modalEntrada()">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                            <span>Agregar</span>
                        </button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered" role="grid">
                <thead>
                    <tr>
                        <th class="text-center align-middle border-black" rowspan="2">PROVEEDOR</th>
                        <th class="text-center align-middle border-black" colspan="2">ENTRADA</th>
                        <th class="text-center align-middle border-black" colspan="2">ACTIVIDADES</th>
                        <th class="text-center align-middle border-black" colspan="2">SALIDA</th>
                        <th class="text-center align-middle border-black" rowspan="2">CLIENTE</th>
                    </tr>
                    <tr>
                        <th class="text-center align-middle border-black">INSUMO (ENTRADA)</th>
                        <th class="text-center align-middle border-black">CONTROLES</th>
                        <th class="text-center align-middle border-black">PHVA</th>
                        <th class="text-center align-middle border-black">DESCRIPCIÓN</th>
                        {{-- <th class="text-center align-middle border-black">DOCUMENTOS DE REFERENCIA</th> --}}
                        <th class="text-center align-middle border-black">PRODUCTO (SALIDA)</th>
                        <th class="text-center align-middle border-black">CONTROLES</th>
                    </tr>
                </thead>
                <tbody class="border-black">
                    @foreach ($entradas as $entrada)
                        @php
                            $salida = salida($entrada->salida_id);
                        @endphp
                        <tr>
                            <td class="border-black">{{ $entrada->interesado }}</td>                            
                            <td class="border-black">{{ $entrada->nombreReferencial }}</td>                            
                            <td class="border-black">{{ $entrada->controles }}</td>                            
                            <td class="border-black">{{ $entrada->phva }}</td>                            
                            <td class="border-black">{{ $entrada->descripcion }}</td>  
                            {{-- <td class="border-black"> documento referencial</td> --}}
                            <td class="border-black">
                                @if($salida)
                                    {{$salida->documento->nombreReferencial}} 
                                @endif
                            </td>
                            <td class="border-black">
                                @if($salida)
                                    {{$salida->controles}} 
                                @endif
                            </td>                         
                            <td class="border-black">
                                @if($salida)
                                    {{$salida->interesado->nombre}} 
                                @endif
                            </td>                         
                        </tr>                        
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <div>
        @include('livewire.caracterizacion.entradas.actividads.create')
    </div>
    <script>
        function scriptEntrada() {

            selectEntradaActividad = $('#entrada-actividad');
            selectDocumentoEntrada = $('#documento-entrada');
            selectDocumentoSalida = $('#documento-salida');
            selectEntradaProveedor = $('#entrada-proveedor');
            selectSalidaCliente = $('#salida-cliente');
            
            Livewire.on('alertEntrada', function(datos){
                nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
            });
            selectEntradaActividad.select2({
                dropdownParent: selectEntradaActividad.parent(),
                width: '100%',
                placeholder: 'Buscar proceso',
                language: {
                    noResults: function() {
                        return "No hay resultado";        
                    },
                    searching: function() {
                        return "Buscando..";
                    }
                }
            });
            selectDocumentoEntrada.select2({
                dropdownAutoWidth: true,
                dropdownParent: selectDocumentoEntrada.parent(),
                width: '100%',
                placeholder: 'Buscar documento',
                language: {
                    noResults: function() {
                        return "No hay resultado";        
                    },
                    searching: function() {
                        return "Buscando..";
                    }
                }
            });
            selectDocumentoEntrada.on('change', function (e) {
                $data = selectDocumentoEntrada.select2("val");
                @this.set('documento_entrada', $data);
                console.log('documento: '+$data);
                Livewire.emitTo('caracterizacion.actividads','obtenerSalidas')
            });
            selectEntradaProveedor.select2({
                dropdownAutoWidth: true,
                dropdownParent: selectEntradaProveedor.parent(),
                width: '100%',
                placeholder: 'Buscar proveedor',
                language: {
                    noResults: function() {
                        return "No hay resultado";        
                    },
                    searching: function() {
                        return "Buscando..";
                    }
                }
            });
            selectSalidaCliente.select2({
                dropdownAutoWidth: true,
                dropdownParent: selectSalidaCliente.parent(),
                width: '100%',
                placeholder: 'Buscar cliente',
                language: {
                    noResults: function() {
                        return "No hay resultado";        
                    },
                    searching: function() {
                        return "Buscando..";
                    }
                }
            });   
            selectDocumentoSalida.select2({
                dropdownAutoWidth: true,
                dropdownParent: selectDocumentoSalida.parent(),
                width: '100%',
                placeholder: 'Buscar documento',
                language: {
                    noResults: function() {
                        return "No hay resultado";        
                    },
                    searching: function() {
                        return "Buscando...";
                    }
                }
            }); 
            Livewire.on('imprimirSalidas', function () {
                selectDocumentoSalida.select2({
                    dropdownAutoWidth: true,
                    dropdownParent: selectDocumentoSalida.parent(),
                    width: '100%',
                    placeholder: 'Buscar documento',
                    language: {
                        noResults: function() {
                            return "No hay resultado";        
                        },
                        searching: function() {
                            return "Buscando...";
                        }
                    }
                });                
            });
            Livewire.on('asignarDatos', function(){
                $actividad = selectEntradaActividad.select2("val");
                @this.set('proceso_id', $actividad);
                
                $proveedor = selectEntradaProveedor.select2("val");
                @this.set('duenio_entrada', $proveedor);

                $data = selectDocumentoSalida.select2("val");
                @this.set('documento_salida', $data);

                $cliente = selectSalidaCliente.select2("val");
                @this.set('duenio_salida', $cliente);

                console.log('documento-salida: '+$data);
                Livewire.emitTo('caracterizacion.actividads','storeEntrada')
            });
            Livewire.on('resetearSelect', function () {   
                selectEntradaActividad.val('NULL');
                selectEntradaActividad.trigger('change');
                selectDocumentoEntrada.val('NULL');
                selectDocumentoEntrada.trigger('change');
                selectDocumentoSalida.val('NULL');
                selectDocumentoSalida.trigger('change');
                selectEntradaProveedor.val('NULL');
                selectEntradaProveedor.trigger('change');
                selectSalidaCliente.val('NULL');
                selectSalidaCliente.trigger('change');
            });
        }    
    </script>
</div>