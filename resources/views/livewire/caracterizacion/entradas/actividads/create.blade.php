<!-- Modal -->
<x-jet-dialog-modal wire:model="openEntrada" maxWidth="lg">
    <x-slot name="title">
        <h3>Agregar Nueva Entrada-Actividad-Salida</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <form  id="form-entrada">
            <div class="row mb-1">
                <div class="col-12 col-md-12">
                    <h4><strong class="text-danger">* </strong>Seleccione la Actividad</h4>
                    <div wire:ignore>
                        <select id="entrada-actividad" class="form-control">
                            <option value="NULL" disable>--Seleccione--</option>
                            @foreach ($actividads as $actividad)
                                <option value="{{$actividad->id}}">{{$actividad->descripcion}}</option>
                            @endforeach                 
                        </select>
                    </div>
                    @error('proceso_id')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div> 
            </div>
            <div class="row">
                <div class="col-12 col-md-6">
                    <h4 class="card-title mb-1">Datos de la Entrada</h4>
                    <div class="card mb-0">
                        <div class="row gy-1 gx-2 pb-1">
                            <div class="col-12 col-md-12">
                                <strong class="text-danger">* </strong><label class="form-label"><strong>Documento</strong></label>
                                <div wire:ignore>
                                    <select wire:model="documento_entrada" id="documento-entrada" class="form-control">
                                        <option value="NULL">--Seleccione--</option>
                                        @foreach ($documentos as $documento)
                                            <option value="{{$documento->id}}">{{$documento->nombreReferencial}}</option>
                                        @endforeach                 
                                    </select>
                                </div>
                                @error('documento_entrada')
                                    <span class="text-sm text-danger">{{$message}}</span>
                                @enderror
                            </div> 
                            <div class="col-12 col-md-12">
                                <strong class="text-danger">* </strong><label class="form-label"><strong>Proveedor</strong></label>
                                <div wire:ignore>
                                    <select wire:model="duenio_entrada" id="entrada-proveedor" class="form-control">
                                        <option value="NULL">--Seleccione--</option>
                                        @foreach ($interesados as $interesado)
                                            <option value="{{$interesado->id}}">{{$interesado->nombre}}</option>
                                        @endforeach                 
                                    </select>
                                </div>
                                @error('duenio_entrada')
                                    <span class="text-sm text-danger">{{$message}}</span>
                                @enderror
                            </div> 
                        </div>                       
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <h4 class="card-title mb-1">Datos de la Salida</h4>
                    <div class="card mb-0">
                        <div class="row gy-1 gx-2 pb-1">
                            <div class="col-12 col-md-12">
                                <strong class="text-danger">* </strong><label class="form-label"><strong>Documento</strong></label>
                                <div wire:ignore.self>
                                    <select wire:model.defer="documento_salida" wire:click="$emit('select2DocumentoSalida')" id="documento-salida" class="form-control">
                                        <option value="NULL">--Seleccione--</option>
                                        @foreach ($documento_salidas as $documento_salida)
                                            <option value="{{$documento_salida->id}}">{{$documento_salida->nombreReferencial}}</option>
                                        @endforeach                 
                                    </select>
                                </div>
                                @error('documento_salida')
                                    <span class="text-sm text-danger">{{$message}}</span>
                                @enderror
                            </div> 
                            <div class="col-12 col-md-12">
                                <strong class="text-danger">* </strong><label class="form-label"><strong>Cliente</strong></label>
                                <div wire:ignore>
                                    <select wire:model="duenio_salida" id="salida-cliente" class="form-control">
                                        <option value="NULL">--Seleccione--</option>
                                        @foreach ($interesados as $interesado)
                                            <option value="{{$interesado->id}}">{{$interesado->nombre}}</option>
                                        @endforeach                 
                                    </select>
                                </div>
                                @error('duenio_salida')
                                    <span class="text-sm text-danger">{{$message}}</span>
                                @enderror
                            </div> 
                        </div>                       
                    </div>
                </div>
            </div>
        </form>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
            <x-jet-button wire:click.prevent="$emit('asignarDatos')" class="btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
        </div>        
    </x-slot>
</x-jet-dialog-modal>