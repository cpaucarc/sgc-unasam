<div>    
    <div class="card">
        <div class="card-header" style="padding-left: 0px;">
            <h4 class="card-title text-primary"><strong>ENTRADA - PROCESO - SALIDA:</strong></h4>
            <div class="heading-elements">            
                <ul class="list-inline mb-0">
                    <li>
                        @can('create', App\Models\Entrada::class)                        
                        <button type="button" class="btn btn-outline-primary waves-effect" wire:click="modalEntrada()">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                            <span>Agregar</span>
                        </button>
                        @endcan
                    </li>
                </ul>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered" role="grid">
                <thead>
                    <tr>
                        <th class="text-center px-25">Nro.</th>
                        <th class="text-center px-25" width="15%">PROVEEDOR</th>
                        <th class="text-center px-25">ENTRADA</th>
                        <th class="text-center px-25" width="20%">{{tipoProceso($nivel_proceso)}}</th>
                        <th class="text-center px-25">SALIDA</th>
                        <th class="text-center px-25" width="10%">CLIENTE</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($entradas as $entrada)
                        <tr>
                            @php
                                $salida = salida($entrada->salida_id);
                            @endphp
                            <td class="text-center px-25">
                                {{$loop->iteration}}
                            </td>
                            <td class="px-25">
                                {{$entrada->interesado}}
                            </td>
                            <td class="px-25">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex align-items-center"> 
                                        <span wire:ignore wire:click="entradasArchivos({{$entrada->entrada_id}})" class="text-primary cursor-pointer" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Ver archivos del documento">
                                            {{$entrada->nombreReferencial}}
                                        </span>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        @can('upload_general', App\Models\Archivo::class) 
                                        <button wire:ignore wire:click="subirArchivo({{$entrada->entrada_id}})" type="button" class="btn btn-icon rounded-circle btn-flat-primary waves-effect" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Subir Archivos">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-upload-cloud">
                                                <polyline points="16 16 12 12 8 16"></polyline>
                                                <line x1="12" y1="12" x2="12" y2="21"></line>
                                                <path d="M20.39 18.39A5 5 0 0 0 18 9h-1.26A8 8 0 1 0 3 16.3"></path>
                                                <polyline points="16 16 12 12 8 16"></polyline>
                                            </svg>
                                        </button> 
                                        @endcan                                             
                                    </div>
                                </div>            
                            </td>
                            <td class="px-25">
                                {{$entrada->codigo}}: {{$entrada->nombre}}
                            </td>
                            <td class="px-25">
                                @if($salida)
                                    <div class="d-flex justify-content-between">
                                        <div class="d-flex align-items-center"> 
                                            <span wire:ignore wire:click="entradasArchivos({{$salida->documento_id}},1)" class="text-primary cursor-pointer" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Ver archivos del documento">
                                                {{$salida->documento->nombreReferencial}}
                                            </span> 
                                        </div>
                                        <div class="d-flex align-items-center">
                                            @can('upload_general', App\Models\Archivo::class)                                            
                                            <button wire:ignore wire:click="subirArchivo({{$salida->id}})" type="button" class="btn btn-icon rounded-circle btn-flat-primary waves-effect" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Subir Archivos">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-upload-cloud">
                                                    <polyline points="16 16 12 12 8 16"></polyline>
                                                    <line x1="12" y1="12" x2="12" y2="21"></line>
                                                    <path d="M20.39 18.39A5 5 0 0 0 18 9h-1.26A8 8 0 1 0 3 16.3"></path>
                                                    <polyline points="16 16 12 12 8 16"></polyline>
                                                </svg>
                                            </button>   
                                            @endcan                                           
                                        </div>
                                    </div>   
                                @else
                                    -
                                @endif
                            </td>
                            <td class="px-25">
                                @if($salida)
                                    {{$salida->interesado->nombre}} 
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <div>
        @include('livewire.caracterizacion.entradas.create')
        @include('livewire.caracterizacion.entradas.show')
        @include('livewire.caracterizacion.entradas.upload')
    </div>
    <script>
        function scriptEntrada() {

            selectEntradaProceso = $('#entrada-proceso');
            selectDocumentoEntrada = $('#documento-entrada');
            selectDocumentoSalida = $('#documento-salida');
            selectEntradaProveedor = $('#entrada-proveedor');
            selectSalidaCliente = $('#salida-cliente');
            
            Livewire.on('alertEntrada', function(datos){
                nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
            });
            selectEntradaProceso.select2({
                dropdownParent: selectEntradaProceso.parent(),
                width: '100%',
                placeholder: 'Buscar proceso',
                language: {
                    noResults: function() {
                        return "No hay resultado";        
                    },
                    searching: function() {
                        return "Buscando..";
                    }
                }
            });
            selectDocumentoEntrada.select2({
                dropdownAutoWidth: true,
                dropdownParent: selectDocumentoEntrada.parent(),
                width: '100%',
                placeholder: 'Buscar documento',
                language: {
                    noResults: function() {
                        return "No hay resultado";        
                    },
                    searching: function() {
                        return "Buscando..";
                    }
                }
            });
            selectDocumentoEntrada.on('change', function (e) {
                $data = selectDocumentoEntrada.select2("val");
                @this.set('documento_entrada', $data);
                Livewire.emitTo('caracterizacion.entradas','obtenerSalidas')
            });
            selectEntradaProveedor.select2({
                dropdownAutoWidth: true,
                dropdownParent: selectEntradaProveedor.parent(),
                width: '100%',
                placeholder: 'Buscar proveedor',
                language: {
                    noResults: function() {
                        return "No hay resultado";        
                    },
                    searching: function() {
                        return "Buscando..";
                    }
                }
            });
            selectSalidaCliente.select2({
                dropdownAutoWidth: true,
                dropdownParent: selectSalidaCliente.parent(),
                width: '100%',
                placeholder: 'Buscar cliente',
                language: {
                    noResults: function() {
                        return "No hay resultado";        
                    },
                    searching: function() {
                        return "Buscando..";
                    }
                }
            });   
            selectDocumentoSalida.select2({
                dropdownAutoWidth: true,
                dropdownParent: selectDocumentoSalida.parent(),
                width: '100%',
                placeholder: 'Buscar documento',
                language: {
                    noResults: function() {
                        return "No hay resultado";        
                    },
                    searching: function() {
                        return "Buscando...";
                    }
                }
            }); 
            Livewire.on('imprimirSalidas', function () {
                selectDocumentoSalida.select2({
                    dropdownAutoWidth: true,
                    dropdownParent: selectDocumentoSalida.parent(),
                    width: '100%',
                    placeholder: 'Buscar documento',
                    language: {
                        noResults: function() {
                            return "No hay resultado";        
                        },
                        searching: function() {
                            return "Buscando...";
                        }
                    }
                });                
            });
            Livewire.on('asignarDatos', function(){
                $entrada = selectEntradaProceso.select2("val");
                @this.set('proceso_id', $entrada);

                $proveedor = selectEntradaProveedor.select2("val");
                @this.set('duenio_entrada', $proveedor);

                $cliente = selectSalidaCliente.select2("val");
                @this.set('duenio_salida', $cliente);

                $salida = selectDocumentoSalida.select2("val");
                @this.set('documento_salida', $salida);    

                Livewire.emitTo('caracterizacion.entradas','storeEntrada')
            });
            Livewire.on('EliminarArchivo', archivoID=>{
				Swal.fire({
					title: '¿Está seguro?',
					text: "¡No podrás revertir esto!",
					icon: 'error',
					showCancelButton: true,
					confirmButtonText: '¡Sí, elimínalo!',
					cancelButtonText: 'Cancelar'
				}).then((result) => {
					if (result.isConfirmed) {
						Livewire.emitTo('caracterizacion.entradas','destroy',archivoID)            
					}
				})
			});	
            Livewire.on('resetearSelect', function () {   
                selectEntradaProceso.val('NULL');
                selectEntradaProceso.trigger('change');
                selectDocumentoEntrada.val('NULL');
                selectDocumentoEntrada.trigger('change');
                selectDocumentoSalida.val('NULL');
                selectDocumentoSalida.trigger('change');
                selectEntradaProveedor.val('NULL');
                selectEntradaProveedor.trigger('change');
                selectSalidaCliente.val('NULL');
                selectSalidaCliente.trigger('change');
            });

			fechaVigencia = $('.fechaflatpickr');    
			fechaVigencia.flatpickr({
				dateFormat: "d-m-Y",
				minDate: '01-01-1940',  
				allowInput: true,
				allowInvalidPreload: true,
				locale: {
					firstDayOfWeek: 1,
					weekdays: {
						shorthand: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
						longhand: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],         
					}, 
					months: {
						shorthand: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Оct', 'Nov', 'Dic'],
						longhand: ['Enero', 'Febreo', 'Мarzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
					},
				}
			});      
        }    
    </script>
</div>