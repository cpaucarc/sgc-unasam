<!-- Modal -->
<x-jet-dialog-modal wire:model="showArchivos" maxWidth="lg">
    <x-slot name="title">
        <h3>Mostras Archivos</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <h4>Lista de Archivos del {{$nom_documento}}</h4>
        @if(count($archivos))
            <div class="table-responsive">
                <table class="table table-bordered" role="grid">
                    <thead>
                        <tr>
                            <th class="text-center px-0" scope="col" style="width: 50px;">Nro.</th>
                            <th class="text-center px-0" rowspan="2">Descripción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($archivos as $archivo)
                            <tr>
                                <td class="py-0 px-0 text-center"><span>{{$loop->iteration}}</span></td>
                                <td class="py-0 px-1">
                                    <div class="d-flex justify-content-between">
                                        <div class="d-flex align-items-center">                                                 
                                            <span wire:ignore>{{$archivo->descripcion}}</span>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <button wire:click="descargar({{$archivo->id}})" class="btn btn-icon rounded-circle btn-flat-primary waves-effect">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-download">
                                                    <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                    <polyline points="7 10 12 15 17 10"></polyline>
                                                    <line x1="12" y1="15" x2="12" y2="3"></line>
                                                </svg>
                                            </button>
                                            @can('delete', $archivo)
                                            <button wire:click="$emit('EliminarArchivo',{{$archivo->id}})" class="btn btn-icon rounded-circle btn-flat-danger waves-effect">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
                                                    <polyline points="3 6 5 6 21 6"></polyline>
                                                    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                </svg>
                                            </button>     
                                            @endcan                                        
                                        </div>
                                    </div>                                           
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <strong class="text-danger">No hay registro de archivos</strong>
        @endif
    </x-slot>
    <x-slot name="footer">       
    </x-slot>
</x-jet-dialog-modal>