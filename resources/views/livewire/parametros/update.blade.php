<!-- Modal -->
<x-jet-dialog-modal wire:model="updateMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3>Actualizar Parámetro</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2">
            <div class="col-12 col-md-6">
                <label class="form-label"><strong> <span style="color:red">*</span> Parámetro </strong> </label>
                <input
                    type="text"
                    wire:model="parametro"
                    class="form-control"
                    placeholder="Parametro" maxlength="45"
                />
                @error('parametro')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label">Descripción</label>
                <input
                    type="text"
                    wire:model="descripcion"
                    class="form-control"
                    placeholder="Descripción" maxlength="250"
                />
                @error('descripcion')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="update()" class="btn btn btn-primary me-1 mt-2">
                Actualizar
            </x-jet-button>
            <x-jet-button wire:click.prevent="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
