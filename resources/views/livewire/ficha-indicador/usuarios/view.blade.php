<div>
    <div class="card">
        <div class="card-header pt-0">
            <p class="text-primary"><strong>USUARIOS DE LA INFORMACIÓN</strong></p>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li>
                        <button type="button" class="btn btn-outline-primary waves-effect" wire:click="openModalUsuarioRes()">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                            <span>Agregar</span>
                        </button>
                    </li>
                </ul>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-sm border-black" style="width: 100%">
                <thead>
                    <tr>
                        <th style="width: 100px" class="text-center">Nro. </th>
                        <th class="text-center">USUARIOS</th>
                    </tr>
                </thead>
                <tbody class="border-black">
                @foreach ($responsable_usuarios as $resUsuario)
                    <tr class="odd">
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td>
                            <div class="d-flex justify-content-between">
                                <div class="d-flex align-items-center">
                                    <span>{{$resUsuario->nombre}}</span>
                                </div>
                                <div>
                                    <button wire:click="$emit('EliminarUsuarioFicha', {{$resUsuario->interesado_id}})" class="btn btn-icon btn-flat-danger waves-effect">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
                                            <polyline points="3 6 5 6 21 6"></polyline>
                                            <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <div>
        @include('livewire.ficha-indicador.usuarios.create')
    </div>
    <script>
        function scriptUsuariosInteresado() {

            selectUsuarioInteresado = $('#idParamUsuarioInteresado');

            Livewire.on('alertUsuarioInteresado', function(datos){
                nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
            });

            Livewire.on('obtenerResponsableCA', function(){
                selectUsuarioInteresado.select2({
                    placeholder: 'Seleccione el Recurso',
                    allowClear: true,
                    language: {
                        noResults: function() {
                            return "No hay resultado";
                        },
                        searching: function() {
                            return "Buscando...";
                        }
                    }
                });
            });

            Livewire.on('asignarUsuarioInteresado', function(){
                $valores = selectUsuarioInteresado.select2('data');

            @this.set('selectUsuarioInteresado', $valores);
                console.log($valores);
                Livewire.emitTo('ficha.usuario','store')
            });


            Livewire.on('alertUsuarioElimina', function(datos){
                nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
            });

            Livewire.on('EliminarUsuarioFicha', procesoId=>{
                Swal.fire({
                    title: '¿Está seguro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'error',
                    showCancelButton: true,
                    confirmButtonText: '¡Sí, elimínalo!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emitTo('ficha.usuario','destroy', procesoId)
                    }
                })
            });


        }
    </script>
</div>
