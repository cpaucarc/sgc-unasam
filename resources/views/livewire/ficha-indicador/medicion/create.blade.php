<!-- Modal -->
<x-jet-dialog-modal wire:model="openRestriccion" maxWidth="lg">
    <x-slot name="title">
        <h3>Agregar Nueva Restricción - Limitación - Riesgo</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">

            <div class="col-12">
                <label class="form-label"><strong>Indicador<span style="color: red">*</span> </strong></label>
                <select wire:model="paramIndicador" class="form-select" disabled>
                    <option value="">-- Seleccione --</option>
                    @foreach ($param_indicador as $paramIndicador)
                        <option value="{{$paramIndicador->id}}">{{$paramIndicador->nombre}}</option>
                    @endforeach
                </select>
                @error('paramIndicador')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-12">
                <label class="form-label"><strong>Abreviatura <span style="color: red">*</span> </strong></label>
                <input type="text" wire:model="nombre" class="form-control" placeholder="Nombre Restricción - Indicador"/>
                @error('nombre')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12">
                <label class="form-label"><strong>Restricción - Limitación - Riesgo <span style="color: red">*</span> </strong></label>
                <select wire:model="paramRestriccion" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($restriccions as $restriccion)
                        <option value="{{$restriccion->id}}">{{$restriccion->restriccion}}</option>
                    @endforeach
                </select>
                @error('paramRestriccion')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">

            <div class="col-12 text-center">
                <x-jet-button wire:click="cancel()" class="btn btn-danger me-2 mt-2">
                    Cancelar
                </x-jet-button>
                <x-jet-button wire:click.prevent="store()" class="btn btn-primary mt-2">
                    Guardar
                </x-jet-button>
            </div>
        </div>

    </x-slot>
</x-jet-dialog-modal>
