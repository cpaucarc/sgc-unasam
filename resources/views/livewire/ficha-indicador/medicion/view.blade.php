
    <div class="col-6">
        <table class="table table-bordered" style="width: 100%">

            <tr style="border: 1px solid black;  border-collapse: collapse;">
                <td style="background-color: #dfe6e9; width: 220px; border: 2px solid #636e72;  border-collapse: collapse;">
                    Unidad de Medida
                </td>
                <td class="inputcentrado">
                    <div class="col-12 col-md-12">
                        {{--<select wire:model="responsable_calculo" class="form-select">
                            <option value="">-- Seleccione --</option>
                            @foreach ($mediciones as $medicion)
                                <option value="{{$medicion->indicador_id}}">{{$medicion->paraUnidadMedida}}</option>
                            @endforeach
                        </select>--}}
                    </div>
                </td>
            </tr>
            <tr style="border: 1px solid black;  border-collapse: collapse;">
                <td style="background-color: #dfe6e9; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                    Meta
                </td>
                <td class="inputcentrado">

                    <div class="col-12 col-md-12">
                        <select wire:model="medMeta" class="form-select">
                            <option value="">-- Seleccione --</option>
                            @foreach ($metas as $meta)
                                <option value="{{$meta->id}}">{{$meta->meta}}</option>
                            @endforeach
                        </select>
                    </div>
                </td>
            </tr>
            <tr style="border: 1px solid black;  border-collapse: collapse;">
                <td style="background-color: #dfe6e9; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                    Frecuencia de Medición
                </td>
                <td class="inputcentrado">
                    <div class="col-12 col-md-12">
                        {{--<select wire:model="responsable_calculo" class="form-select">
                            <option value="">-- Seleccione --</option>
                            @foreach ($mediciones as $medicion)
                                <option value="{{$medicion->indicador_id}}">{{$medicion->paraFrecMedicion}}</option>
                            @endforeach
                        </select>--}}
                    </div>
                </td>
            </tr>

            <tr style="border: 1px solid black;  border-collapse: collapse;">
                <td style="background-color: #dfe6e9; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                    Frecuencia Reporte
                </td>
                <td class="inputcentrado">
                    <div class="col-12 col-md-12">
                        {{--<select wire:model="responsable_calculo" class="form-select">
                            <option value="">-- Seleccione --</option>
                            @foreach ($mediciones as $medicion)
                                <option value="{{$medicion->indicador_id}}">{{$medicion->paraFrecReporte}}</option>
                            @endforeach
                        </select>--}}
                    </div>
                </td>
            </tr>
            <tr style="border: 1px solid black;  border-collapse: collapse;">
                <td style="background-color: #dfe6e9; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                    Frecuencia Revisión
                </td>
                <td class="inputcentrado">
                    <div class="col-12 col-md-12">
                        {{--<select wire:model="responsable_calculo" class="form-select">
                            <option value="">-- Seleccione --</option>
                            @foreach ($mediciones as $medicion)
                                <option value="{{$medicion->indicador_id}}">{{$medicion->paraFrecRevision}}</option>
                            @endforeach
                        </select>--}}
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <hr>
    <div class="col-12" style="padding-left: 20px; padding-right: 20px">
        <div class="row">
            <div class="col-6">

                <div class="card">
                    <div class="card-header pt-0">
                        <h4 class="card-title text-primary"><strong>ESCALAS DE VALORACIÓN:</strong></h4>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li>
                                    {{--<button type="button" class="btn btn-outline-primary waves-effect" wire:click="$set('open',true)">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                                        <span>Agregar</span>
                                    </button>--}}
                                    <div class="dt-buttons btn-group flex-wrap">
                                        <x-jet-button wire:click="$set('open',true)" class="btn add-new btn-primary mt-50">
                                            <span>Agregar Año</span>
                                        </x-jet-button>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div>
                            {{--@include('sgc.macroprocesos.createFicha')--}}
                            {{--@include('livewire.anios.update')--}}
                        </div>

                    </div>
                    <div class="card-body>">
                        <div class="table-responsive">
                            <table class="table table-bordered" style="width: 100%">
                                <thead>
                                <tr>
                                    <th class="text-center">Likert</th>
                                    <th class="text-center">Valor'Inf.</th>
                                    <th class="text-center">Valor-sup</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-center">Muy malo</td>
                                    <td>0.00</td>
                                    <td>0.24</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center">Malo</td>
                                    <td>0.24</td>
                                    <td>0.48</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center">Regular</td>
                                    <td>0.48</td>
                                    <td>0.72</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center">Satisfactorio</td>
                                    <td>0.72</td>
                                    <td>0.78</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center">Muy Satisfactorio</td>
                                    <td>0.78</td>
                                    <td>0.80</td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    {{--<div>
        @include('livewire.ficha-indicador.restricciones.create')
    </div>--}}
    <script>
        window.onload = function() {
            selectMultiple = $('#vinculos');
            Livewire.on('obtenerVinculos', function(){
                // selectMultiple.select2('destroy');
                selectMultiple.select2({
                    placeholder: 'Seleccione Macroproceso',
                    allowClear: true,
                    language: {
                        noResults: function() {
                            return "No hay resultado";
                        },
                        searching: function() {
                            return "Buscando...";
                        }
                    }
                });
            });
            Livewire.on('alertVinculacion123', function(datos){
                nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
            });

            Livewire.on('asignarValorVinculacion', function(){
                $valores = selectMultiple.select2('data');
                // @this.set('vinculacion', $valores);
            @this.set('multiselect', $valores);
                console.log('entra');
                Livewire.emitTo('caracterizacion.vinculacions','store')
            });
            Livewire.on('EliminarUsuarioFicha', usuarioId=>{
                Swal.fire({
                    title: '¿Está seguro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'error',
                    showCancelButton: true,
                    confirmButtonText: '¡Sí, elimínalo!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emitTo('ficha.usuario','destroy', usuarioId)
                    }
                })
            });

            Livewire.on('alertSproceso', function(datos){
                nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
            });

            Livewire.on('eliminarSproceso', procesoId=>{
                Swal.fire({
                    title: '¿Está seguro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'error',
                    showCancelButton: true,
                    confirmButtonText: '¡Sí, elimínalo!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emitTo('ficha.suministro-proceso','destroy', procesoId)
                    }
                })
            });

            Livewire.on('alertUsuarioSum', function(datos){
                nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
            });

            Livewire.on('EliminarSuministroArea', interesadosId=>{
                Swal.fire({
                    title: '¿Está seguro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'error',
                    showCancelButton: true,
                    confirmButtonText: '¡Sí, elimínalo!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emitTo('ficha.suministro-area','destroy', interesadosId)
                    }
                })
            });

            Livewire.on('alertRestriccionFicha', function(datos){
                nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
            });

            Livewire.on('EliminarrestriccionFicha', restriccionId=>{
                Swal.fire({
                    title: '¿Está seguro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'error',
                    showCancelButton: true,
                    confirmButtonText: '¡Sí, elimínalo!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emitTo('ficha.restricciones','destroy', restriccionId)
                    }
                })
            });
        }
    </script>

