<div>
    <div class="card">
        <div class="card-header pt-0">
            <h4 class="text-primary"><strong>RESTRICCIONES LIMITACIONES RIESGO:</strong></h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li>
                        <button type="button" class="btn btn-outline-primary waves-effect" wire:click="openModal()">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                            <span>Agregar</span>
                        </button>
                    </li>
                </ul>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-sm border-black" style="width: 100%">
                <thead>
                    <tr>
                        <th width="70px" class="text-center">Nro. </th>
                        <th class="text-center">RESTRICCIONES LIMITACIONES RIESGO</th>
                        <!-- <th class="text-center">NOMBRE RESTRICCIONES</th> -->
                        <th width="50px" class="text-center"></th>
                    </tr>
                </thead>
                <tbody class="border-black">
                @foreach ($restricciones as $restriccion)
                    <tr class="odd">
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td class="text-center">{{ $restriccion->rNombre }}</td>
                        {{-- <td>
                            <div class="d-flex justify-content-between">
                                <div class="d-flex align-items-center">
                                    <span>{{$restriccion->nombre}}</span>
                                </div>
                            </div>
                        </td> --}}
                        <td class="text-center"><div>
                                <button wire:click="$emit('EliminarrestriccionFicha', {{$restriccion->id}})" class="btn btn-icon btn-flat-danger waves-effect">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
                                        <polyline points="3 6 5 6 21 6"></polyline>
                                        <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                    </svg>
                                </button>
                            </div></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <div>
        @include('livewire.ficha-indicador.restricciones.create')
    </div>
    <script>
        function scriptRestriccionesRiesgos() {

            selectRestreicciones = $('#idParamRestricciones');

            // Livewire.on('alertUsuarioInteresado', function(datos){
            //     nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
            // });

            Livewire.on('obtenerRestricciones', function(){
                selectRestreicciones.select2({
                    placeholder: 'Seleccione la restricción',
                    allowClear: true,
                    language: {
                        noResults: function() {
                            return "No hay resultado";
                        },
                        searching: function() {
                            return "Buscando...";
                        }
                    }
                });
            });

            Livewire.on('asignarRestrcciones', function(){
                $valores = selectRestreicciones.select2('data');

            @this.set('selectRestreicciones', $valores);
                //console.log($valores);
                Livewire.emitTo('ficha.restricciones','store')
            });




            Livewire.on('alertRestriccionFicha', function(datos){
                nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
            });

            Livewire.on('EliminarrestriccionFicha', restriccionId=>{
                Swal.fire({
                    title: '¿Está seguro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'error',
                    showCancelButton: true,
                    confirmButtonText: '¡Sí, elimínalo!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emitTo('ficha.restricciones','destroy', restriccionId)
                    }
                })
            });

        }
    </script>
</div>
