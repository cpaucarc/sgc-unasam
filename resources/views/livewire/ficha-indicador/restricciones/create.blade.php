<!-- Modal -->
<x-jet-dialog-modal wire:model="openRestriccion" maxWidth="lg">
    <x-slot name="title">
        <h3>Agregar Nueva Restricción - Limitación - Riesgo</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">

            <div class="col-12">
                <label class="form-label"><strong>Indicador<span style="color: red">*</span> </strong></label>
                <select wire:model="paramIndicador" class="form-select" disabled>
                    <option value="">-- Seleccione --</option>
                    @foreach ($param_indicador as $paramIndicador)
                        <option value="{{$paramIndicador->id}}">{{$paramIndicador->nombre}}</option>
                    @endforeach
                </select>
                @error('paramIndicador')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div id="form-recursos">
                <form>
                    <label class="form-label"><strong><span style="color: red">*</span> Restricción</strong></label>
                    <div class="mb-1 row">
                        <div class="col-sm-12">
                            <select id="idParamRestricciones" class="form-control" multiple>
                                <option value="NULL" disabled>--Seleccione--</option>
                                @foreach ($restriccions as $restriccion)
                                    <option value="{{$restriccion->id}}">{{$restriccion->restriccion}}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('paramProceso')
                        <span class="text-sm text-danger">{{$message}}</span>
                        @enderror
                    </div>
                </form>
            </div>

        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">

            <div class="col-12 text-center">
                <x-jet-button wire:click.prevent="$emit('asignarRestrcciones')" class="btn btn-primary me-2 mt-2">
                    Guardar
                </x-jet-button>
                <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                    Cancelar
                </x-jet-button>
            </div>
        </div>

    </x-slot>
</x-jet-dialog-modal>
