<!-- Modal -->
<x-jet-dialog-modal wire:model="openCrearResultadoEvalua" maxWidth="lg">
    <x-slot name="title">
        <h3>Registrar Resultado - Evaluación de medición</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row">
            <div class="col-6 col-md-6">
                <label class="form-label"><strong>Periodo<span style="color: red">*</span></strong></label>
                <select wire:model="param_periodo" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($resultaPorMedicion as $selectPorMedicion)
                        <option value="{{ $selectPorMedicion->id }}">{{ $selectPorMedicion->detalle }}</option>
                    @endforeach

                </select>
                @error('param_periodo')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
            @if($tiene_api)
            <div class="col-6 col-md-6 text-end">
                <label>&nbsp;</label><br>
                <button class="btn btn-primary" wire:click="obtenerDatos" wire:loading.attr="disabled">Obtener datos</button>
            </div>
            @endif
        </div>
        <hr>
        <div class="row">
            <div class="col-6 col-md-6">
                <div class="row">
                    <div class="col-12">
                        <h5>Numerador</h5>
                    </div>
                    <div class="col-12">
                        <table class="table table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th style="width: 70%">Variable</th>
                                    <th>Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($numeradores as $key => $n)
                                    <tr wire:key="numeradores-re-create-{{ $n->id }}">
                                        <td>
                                            <h6>{{ $n->peso }}{{ $n->variable }}<br>
                                                <div class="text-muted"> {{ $n->nombre }}</div>
                                            </h6>
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" wire:model="numeradores.{{ $key }}.valor">
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-6">
                <div class="row">
                    <div class="col-12">
                        <h5>Denominador</h5>
                    </div>
                    <div class="col-12">
                        <table class="table table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th style="width: 70%">Variable</th>
                                    <th>Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($denominadores as $key=>$d)
                                    <tr wire:key="denominadores-re-create-{{ $d->id }}">
                                        <td>
                                            <h6>{{ $d->peso }}{{ $d->variable }}<br>
                                                <div class="text-muted"> {{ $d->nombre }}</div>
                                            </h6>
                                        </td>
                                        <td>
                                            <input type="number" class="form-control" wire:model="denominadores.{{ $key }}.valor">
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-6 col-md-6">
                <label class="form-label"><strong>Valor-Numerador<span style="color: red">*</span></strong></label>
                <input type="text" disabled class="form-control"wire:model="valor_numerador" class="form-control" placeholder="Valor-Numerador" />
                @error('valor_numerador')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-6 col-md-6">
                <label class="form-label"><strong>Valor-Denominador <span style="color: red">*</span></strong></label>
                <input type="text" disabled wire:model="valor_denominador" class="form-control" placeholder="Valor-Denominador" />
                @error('valor_denominador')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-6 col-md-6 text-center mt-1">
                ${{$this->formula_numerador}}$
            </div>
            <div class="col-6 col-md-6 text-center mt-1">
                ${{$this->formula_denominador}}$
            </div>
            {{-- <div class="col-6 col-md-6">
                <label class="form-label"><strong>Valor-Indicador <span style="color: red">*</span></strong></label>
                <input type="text" wire:model="valor_denominador" class="form-control" placeholder="Valor-Indicador" disabled/>
                @error('valor_denominador')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-6 col-md-6">
                <label class="form-label"><strong>Meta <span style="color: red">*</span></strong></label>
                <input type="text" wire:model="meta" class="form-control" placeholder="Meta" disabled/>
                @error('meta')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div> --}}
            <div>
                <hr>
            </div>

            {{-- <div class="col-6 col-md-6">
                <label class="form-label"><strong>Valor de desempeño <span style="color: red">*</span></strong></label>
                <input type="text" wire:model="valor_desempenio" class="form-control" placeholder="Valor de desempeño"/>
                @error('valor_desempenio')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div> --}}
            <div class="col-12 col-md-12">
                <label class="form-label"><strong>Valor Indicador</strong></label>
                <input type="text" wire:model="valor_indicador" class="form-control" placeholder="Valor del Indicador" disabled />
                @error('valor_indicador')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label"><strong>Propuesta</strong></label>
                <textarea type="text" wire:model="propuesta_mejora" class="form-control" placeholder="Propuesta"></textarea>
                @error('propuesta_mejora')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-6 col-md-6">
                <label class="form-label"><strong> Fecha de Propuesta</strong></label>
                <input type="text" wire:model="fecha_propuesta" class="form-control fechaflatpickr" placeholder="Fecha de Propuesta" />
                @error('fecha_propuesta')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-6 col-md-6">
                <label class="form-label"><strong> Fecha de cumplimiento</strong></label>
                <input type="text" wire:model="fecha_cumplimiento"

                       class="form-control fechaflatpickr"
                       placeholder="Fecha de cumplimiento" />
                @error('fecha_cumplimiento')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div wire:loading wire:target="obtenerDatos" class="b-overlay position-absolute text-center" style="inset: 0px; z-index: 10;">
            <div class="position-absolute bg-white rounded-sm" style="inset: 0px; opacity: 0.75;"></div>
            <div class="position-absolute" style="top: 50%; left: 50%; transform: translateX(-50%) translateY(-50%);">
                <span aria-hidden="true" class="spinner-border text-primary">
                </span>
                <br><br>
                <span class="pt-5">Obteniendo datos...</span>
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="store()" class="btn btn-primary me-2 mt-2">
                Guardar
            </x-jet-button>
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>
    </x-slot>
</x-jet-dialog-modal>
