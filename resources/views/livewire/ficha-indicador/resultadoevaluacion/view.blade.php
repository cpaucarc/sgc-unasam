<div>
    <div class="col-lg-12">
        {{-- DISEÑO --}}
        <div class="card">
            <div class="card-header" style="padding-left: 0px;">
                <h4 class="card-title text-primary"><strong>RESULTADOS Y EVALUACIÓN DE MEDICIÓN</strong></h4>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li>
                            @can('create', App\Models\Resultado::class)
                            <button type="button" class="btn btn-outline-primary waves-effect" wire:click="modalEntrada()">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-plus-circle">
                                    <circle cx="12" cy="12" r="10"></circle>
                                    <line x1="12" y1="8" x2="12" y2="16"></line>
                                    <line x1="8" y1="12" x2="16" y2="12"></line>
                                </svg>
                                <span>Agregar resultado</span>
                            </button>
                            @endcan
                        </li>
                    </ul>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered border-black" role="grid">
                    <thead>
                        <tr>
                            <th class="text-center" colspan="3">COMPORTAMIENTO DEL INDICADOR</th>
                            <th class="text-center" colspan="3">RESULTADOS (Número)</th>
                            <th class="text-center align-middle px-0" rowspan="2" style="width: 80px">ACCIÓN</th>
                        </tr>
                        <tr>
                            <th class="text-center">PERIODO</th>
                            <th class="text-center">VALOR - NUMERADOR</th>
                            <th class="text-center">VALOR - DENOMINADOR</th>
                            <th class="text-center">VALOR - INDICADOR</th>
                            <th class="text-center">META</th>
                            <th class="text-center">BRECHA</th>
                        </tr>
                    </thead>
                    <tbody class="border-black">
                        @foreach ($datos as $resultado)
                            @php
                                $resultadoIndicador = round($resultado->valor_numerador / $resultado->valor_denominador, 2);
                            @endphp
                            <tr>
                                <td class="text-center"> {{$anio_id}} {{ $resultado->detalle }} </td>
                                <td class="text-center">{{ $resultado->valor_numerador }} </td>
                                <td class="text-center">{{ $resultado->valor_denominador }}</td>
                                <td class="text-center">{{ $resultadoIndicador }} </td>
                                <td class="text-center">{{ $resultado->meta }} </td>
                                <td class="text-center">{{ $resultado->meta - $resultadoIndicador }} </td>
                                <td style="padding: 0px 0px 0px 0px;">
                                    <div class="d-flex align-items-center">
                                        @can('update', $resultado)
                                        <button wire:click="editResultadoEvaluacion({{ $resultado->id }})" type="button" class="btn btn-icon rounded-circle btn-flat-primary waves-effect">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                 stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit">
                                                <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                                <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                            </svg>
                                        </button>
                                        @endcan
                                        @can('delete', $resultado)
                                        <button wire:click="$emit('EliminarResultadoEvaluacion',{{ $resultado->id }})" type="button" class="btn btn-icon rounded-circle btn-flat-danger waves-effect">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                 stroke-linejoin="round" class="feather feather-trash-2">
                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                <line x1="10" y1="11" x2="10" y2="17"></line>
                                                <line x1="14" y1="11" x2="14" y2="17"></line>
                                            </svg>
                                        </button>
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @endforeach


                    </tbody>
                </table>
            </div>

        </div>

        {{-- FIN DE DISEÑO --}}

    </div>
    <div class="col-lg-12">
        {{-- DISEÑO --}}
        <div class="card">
            <div class="table-responsive">
                <table class="table table-bordered border-black" role="grid">
                    <thead>
                        <tr>
                            <th class="text-center align-middle" rowspan="2">PERIODO</th>
                            <th class="text-center align-middle" rowspan="2">VALORACIÓN DEL DESEMPEÑO</th>
                            <th class="text-center" colspan="3">ACCIONES DE MEJORA</th>
                        </tr>
                        <tr>
                            <th class="text-center">PROPUESTA</th>
                            <th class="text-center">FECHA DE PROPUESTA</th>
                            <th class="text-center">FECHA DE CUMPLIMIENTO</th>

                        </tr>
                    </thead>
                    <tbody class="border-black">
                        @foreach ($datos as $evaluacion)
                            <tr>
                                <td class="text-center">
                                    {{$anio_id}} {{ $evaluacion->detalle }}
                                </td>
                                <td class="text-center">
                                    {{-- {{ obtenerDesempenio($evaluacion->id,1) }} --}}
                                    @if ($evaluacion->valor_indicador)
                                        {{ $evaluacion->valor_indicador }}
                                    @endif
                                </td>
                                <td>
                                    @if ($evaluacion->propuesta_mejora)
                                        {{ $evaluacion->propuesta_mejora }}
                                    @endif
                                    {{-- {{ strtoupper(obtenerDesempenio($evaluacion->id,2)) }} --}}
                                </td>
                                <td class="text-center">
                                    @if ($evaluacion->fecha_propuesta)
                                        {{ $evaluacion->fecha_propuesta }}
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if ($evaluacion->fecha_cumplimiento)
                                        {{ $evaluacion->fecha_cumplimiento }}
                                    @endif
                                </td>

                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>

        </div>

        {{-- FIN DE DISEÑO --}}

    </div>

    {{-- <div class="col-lg-12">

        <div class="card">
            <div
                 class="
              card-header
              d-flex
              justify-content-between
              align-items-sm-center align-items-start
              flex-sm-row flex-column
            ">
                <div class="header-left">
                    <h4 class="card-title text-primary"><strong>EVOLUCIÓN DE INDICADOR </strong></h4>
                </div>
                <div class="header-right d-flex align-items-center mt-sm-0 mt-1">
                </div>
            </div>
            <div class="card-body">
                <canvas class="bar-chart-ex chartjs" data-height="800"></canvas>
            </div>
        </div>



    </div> --}}

    <div>
        @include('livewire.ficha-indicador.resultadoevaluacion.create')
        @include('livewire.ficha-indicador.resultadoevaluacion.update')
    </div>
    <script type="text/javascript" async
            src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/MathJax.js?config=TeX-MML-AM_CHTML">
    </script>
    <script type="text/x-mathjax-config">
        MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
      </script>
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            Livewire.hook('element.updated', (el, component) => {
                MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            })
        });

        var barChartExample;

        function scriptResultadoEvaluacion() {
            Livewire.on('alertaResultadoEvaluacion', function(datos) {
                nitificacion(datos['tipo'], datos['mensaje'], ' ¡Acción realizada!', 2000);
                // //    alert(@json($chartLabelEvolucion));
                // //    alert(@json($chartdataEvolucion));
                // //    alert(@json($chartmax));
                //    barChartExample.data.labels = @json($chartLabelEvolucion);
                //     barChartExample.data.datasets[0].data = @json($chartdataEvolucion); // or you can iterate for multiple datasets
                //    //; // finally update our chart
                //     setTimeout( barChartExample.update(), 5000);

            });
            Livewire.on('EliminarResultadoEvaluacion', resultadoId => {
                Swal.fire({
                    title: '¿Está seguro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'error',
                    showCancelButton: true,
                    confirmButtonText: '¡Sí, elimínalo!',
                    cancelButtonText: 'Cancelar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emitTo('ficha.resultado-evaluacion', 'destroy', resultadoId)
                    }
                })
            });
        }


        // function cargarchartbar() {
        //     /************************grafico*******************/

        //     //alert(@json($chartLabelEvolucion));
        //     // alert(@json($chartdataEvolucion));
        //     // alert(@json($chartmax));

        //     var chartWrapper = $('.chartjs'),
        //         flatPicker = $('.flat-picker'),
        //         barChartEx = $('.bar-chart-ex'),
        //         horizontalBarChartEx = $('.horizontal-bar-chart-ex'),
        //         lineChartEx = $('.line-chart-ex'),
        //         radarChartEx = $('.radar-chart-ex'),
        //         polarAreaChartEx = $('.polar-area-chart-ex'),
        //         bubbleChartEx = $('.bubble-chart-ex'),
        //         doughnutChartEx = $('.doughnut-chart-ex'),
        //         scatterChartEx = $('.scatter-chart-ex'),
        //         lineAreaChartEx = $('.line-area-chart-ex');

        //     // Color Variables
        //     var primaryColorShade = '#836AF9',
        //         yellowColor = '#ffe800',
        //         successColorShade = '#28dac6',
        //         warningColorShade = '#ffe802',
        //         warningLightColor = '#FDAC34',
        //         infoColorShade = '#299AFF',
        //         greyColor = '#4F5D70',
        //         blueColor = '#2c9aff',
        //         blueLightColor = '#84D0FF',
        //         greyLightColor = '#EDF1F4',
        //         tooltipShadow = 'rgba(0, 0, 0, 0.25)',
        //         lineChartPrimary = '#666ee8',
        //         lineChartDanger = '#ff4961',
        //         labelColor = '#6e6b7b',
        //         grid_line_color = 'rgba(200, 200, 200, 0.2)'; // RGBA color helps in dark layout

        //     if (barChartEx.length) {
        //         barChartExample = new Chart(barChartEx, {
        //             type: 'bar',
        //             options: {
        //                 elements: {
        //                     rectangle: {
        //                         borderWidth: 2,
        //                         borderSkipped: 'bottom'
        //                     }
        //                 },
        //                 responsive: true,
        //                 maintainAspectRatio: false,
        //                 responsiveAnimationDuration: 500,
        //                 legend: {
        //                     display: false
        //                 },
        //                 tooltips: {
        //                     // Updated default tooltip UI
        //                     shadowOffsetX: 1,
        //                     shadowOffsetY: 1,
        //                     shadowBlur: 8,
        //                     shadowColor: tooltipShadow,
        //                     backgroundColor: window.colors.solid.white,
        //                     titleFontColor: window.colors.solid.black,
        //                     bodyFontColor: window.colors.solid.black
        //                 },
        //                 scales: {
        //                     xAxes: [{
        //                         display: true,
        //                         gridLines: {
        //                             display: true,
        //                             color: grid_line_color,
        //                             zeroLineColor: grid_line_color
        //                         },
        //                         scaleLabel: {
        //                             display: false
        //                         },
        //                         ticks: {
        //                             fontColor: labelColor
        //                         }
        //                     }],
        //                     yAxes: [{
        //                         display: true,
        //                         gridLines: {
        //                             color: grid_line_color,
        //                             zeroLineColor: grid_line_color
        //                         },
        //                         ticks: {
        //                             stepSize: @json($chartmax) <= 5 ? 10 : Math.trunc(@json($chartmax) / 5),
        //                             min: 0,
        //                             max: @json($chartmax) <= 5 ? 50 : @json($chartmax),
        //                             fontColor: labelColor
        //                         }
        //                     }]
        //                 }
        //             },
        //             data: {
        //                 labels: @json($chartLabelEvolucion),
        //                 datasets: [{
        //                     data: @json($chartdataEvolucion),
        //                     barThickness: 15,
        //                     backgroundColor: successColorShade,
        //                     borderColor: 'transparent'
        //                 }]
        //             }
        //         });
        //     }
        //     /**************************gin*********************/

        // }
    </script>

</div>
