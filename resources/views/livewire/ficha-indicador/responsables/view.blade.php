<div>
    @section('vendor-style')
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    @endsection
    <div class="card">
        <div class="card-header pt-0">
            <p class="text-primary"><strong>RESPONSABLES DE CÁLCULO Y ANÁLISIS</strong></p>            
        </div>
        <div class="col-sm-12">
            
            @if(count($responsables) > 0)
                <div class="table-responsive">
                    <table class="table table-bordered table-sm border-black" style="width: 100%">
                        <thead>
                        <tr>
                            <th class="text-center">Responsable del Cálculo </th>
                        </tr>
                        </thead>
                        <tbody class="border-black">

                        @foreach ($responsables as $resCaculo)
                            <tr class="odd">
                                <td>
                                    <div class="d-flex justify-content-between">
                                        <div class="d-flex align-items-center">
                                            <span>{{$resCaculo->nombre}}</span>
                                        </div>
                                        <div>
                                            <button wire:click="$emit('EliminarResponsable', {{$resCaculo->idIntresado}},'11002')" class="btn btn-icon btn-flat-danger waves-effect">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
                                                    <polyline points="3 6 5 6 21 6"></polyline>
                                                    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="card-header pt-0">
                    <p class="text-primary"><strong>Responsable del Cálculo</strong></p>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <button type="button" class="btn btn-outline-primary waves-effect" wire:click="openModalResponsable('1')">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                                    <span>Agregar</span>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            @endif

            @if(count($responsables_analisis) > 0)
                <div class="table-responsive">
                    <br>
                    <table class="table table-bordered table-sm border-black" style="width: 100%">
                        <thead>
                        <tr>
                            <th class="text-center">Responsable del Análisis y Toma de Deciciones</th>
                        </tr>
                        </thead>
                        <tbody class="border-black">

                        @foreach ($responsables_analisis as $resAnalisis)
                            <tr class="odd">
                                <td>
                                    <div class="d-flex justify-content-between">
                                        <div class="d-flex align-items-center">
                                            <span>{{$resAnalisis->nombre}}</span>
                                        </div>
                                        <div>
                                            <button wire:click="$emit('EliminarResponsable', {{$resAnalisis->idIntresado}},'11003')" class="btn btn-icon btn-flat-danger waves-effect">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
                                                    <polyline points="3 6 5 6 21 6"></polyline>
                                                    <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        {{--<tr>
                            <td class="text-center">
                                <div class="col-12 col-md-12">
                                    <select wire:model="responsable_analisis" class="form-select">
                                        <option value="">-- Seleccione --</option>
                                        @foreach ($responsables_analisis as $resAnalisis)
                                            <option value="{{$resAnalisis->id}}">{{$resAnalisis->nombre}}</option>
                                        @endforeach
                                    </select>
                                    @error('responsable_analisis')
                                    <span class="text-sm text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </td>
                        </tr>--}}
                        </tbody>
                    </table>
                </div>
            @else
                <div class="card-header pt-0">
                    <p class="text-primary"><strong>Responsable del Análisis y Toma de Deciciones</strong></p>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li>
                                <button type="button" class="btn btn-outline-primary waves-effect" wire:click="openModalResponsable('2')">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                                    <span>Agregar</span>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            @endif
        </div>

    </div>
    <div>
        @include('livewire.ficha-indicador.responsables.create')
    </div>

        <script>
            function scriptResponsablesCalAnalisis() {

                //selectParaAreaInteresado = $('#paramInteresado');
                selectParamResponsableCA = $('#idParamResponsableCA');

                // Livewire.on('alertSuministroProceso', function(datos){
                //     nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
                // });

                Livewire.on('obtenerResponsableCA', function(){
                    selectParamResponsableCA.select2({
                        placeholder: 'Seleccione el Recurso',
                        allowClear: true,
                        language: {
                            noResults: function() {
                                return "No hay resultado";
                            },
                            searching: function() {
                                return "Buscando...";
                            }
                        }
                    });
                });

                Livewire.on('asignarResponsableCA', function(){
                    $valores = selectParamResponsableCA.select2('data');

                @this.set('selectParamResponsableCA', $valores);
                    console.log($valores);
                    Livewire.emitTo('ficha.responsables','store')
                });

                Livewire.on('EliminarResponsable', function (idResponsable,tiporesponsable) {
                    Swal.fire({
                        title: '¿Está seguro?',
                        text: "¡No podrás revertir esto!",
                        icon: 'error',
                        showCancelButton: true,
                        confirmButtonText: '¡Sí, elimínalo!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            Livewire.emitTo('ficha.responsables','destroy', idResponsable, tiporesponsable)
                        }
                    })
                });

                Livewire.on('alertResponsable', function(datos){
                    nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
                });

            }
        </script>
</div>
