<tr>
    @if (!$editando)
        <td>{{ $variable->variable }}</td>
    @else
        <td>
            <input type="text" class="form-control" wire:model="variable.variable">
        </td>
    @endif
    @if (!$editando)
        <td class="text-truncate">{{ $variable->nombre }}</td>
    @else
        <td>
            <input type="text" class="form-control" wire:model="variable.nombre">
        </td>
    @endif
    @if ($tiene_api)
        @if (!$editando)
            <td class="text-truncate">{{ $variable->api_url }}</td>
        @else
            <td>
                <input type="text" class="form-control" wire:model="variable.api_url">
            </td>
        @endif
    @endif
    <td class="text-center">
        @if (!$editando)
            <div class="btn-group">
                <button class="btn btn-outline-primary btn-sm waves-effect waves-float waves-light" wire:click="edit" wire:loading.attr="disabled">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-edit-2">
                        <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                    </svg>
                </button>
                <button class="btn btn-outline-danger btn-sm" wire:click="destroy" wire:loading.attr="disabled">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-trash">
                        <polyline points="3 6 5 6 21 6"></polyline>
                        <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                    </svg>
                </button>
            </div>
        @else
            <div class="btn-group">
                <button class="btn btn-outline-primary btn-sm waves-effect waves-float waves-light" wire:click="update" wire:loading.attr="disabled">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-save">
                        <path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path>
                        <polyline points="17 21 17 13 7 13 7 21"></polyline>
                        <polyline points="7 3 7 8 15 8"></polyline>
                    </svg>
                </button>
                <button class="btn btn-outline-danger btn-sm" wire:click="cancel" wire:loading.attr="disabled">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-x">
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </button>
            </div>
        @endif
    </td>
</tr>
