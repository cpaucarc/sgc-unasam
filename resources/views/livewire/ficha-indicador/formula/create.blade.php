<!-- Modal -->
<x-jet-dialog-modal wire:model="openCrear" maxWidth="xl">
    <x-slot name="title">
        <h3>Registrar Fórmula</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="row" x-data="{open: false}">
                    <div class="col-12">
                        <h5>Numerador</h5>
                    </div>
                    @if (!$tiene_api)
                        <div class="col-2">
                            <input type="text" placeholder="Variable" class="form-control" wire:model="variable_numerador.variable" maxlength="1">
                            @error('variable_numerador.variable')
                                <span class="text-sm text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-6">
                            <input type="text" placeholder="Nombre" class="form-control" wire:model="variable_numerador.nombre">
                            @error('variable_numerador.nombre')
                                <span class="text-sm text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    @else
                        <div class="col-2">
                            <input type="text" placeholder="Variable" class="form-control" wire:model="variable_numerador.variable" maxlength="1">
                            @error('variable_numerador.variable')
                                <span class="text-sm text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div x-bind:class="open?'col-4':'col-6'">
                            <input type="text" placeholder="Nombre" class="form-control" wire:model="variable_numerador.nombre">
                            @error('variable_numerador.nombre')
                                <span class="text-sm text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-4" x-show="open">
                            <input type="text" placeholder="Url" class="form-control" wire:model="variable_numerador.api_url">
                            @error('variable_numerador.api_url')
                                <span class="text-sm text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    @endif
                    <div class="col-2 text-center">
                        <button class="btn btn-primary" wire:click="agregaVariable('numerador')">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-plus">
                                <line x1="12" y1="5" x2="12" y2="19"></line>
                                <line x1="5" y1="12" x2="19" y2="12"></line>
                            </svg>
                        </button>
                    </div>

                    @if ($tiene_api)
                    <div class="col-xs-12">
                        <div class="custom-control-primary custom-control custom-checkbox pt-1">
                            <input type="checkbox" class="custom-control-input" x-model="open">
                            <label>¿Tiene URL?</label>
                        </div>
                    </div>
                    @endif
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-hover table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th style="width: 15%">Variable</th>
                                    @if ($tiene_api)
                                        <th style="width: 40%">Nombre</th>
                                        <th style="width: 30%">Url</th>
                                    @else
                                        <th style="width: 50%">Nombre</th>
                                    @endif
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($variables_numerador as $key => $n)
                                    <livewire:ficha.formula.variable :variable="$n" :index="$key" :tiene_api="$tiene_api" :wire:key="'numerador-' . $key">
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="row" x-data="{ open: false }">
                    <div class="col-12">
                        <h5>Denominador</h5>
                    </div>
                    @if (!$tiene_api)
                        <div class="col-2">
                            <input type="text" placeholder="Variable" class="form-control" wire:model="variable_denominador.variable" maxlength="1">
                            @error('variable_denominador.variable')
                                <span class="text-sm text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-6">
                            <input type="text" placeholder="Nombre" class="form-control" wire:model="variable_denominador.nombre">
                            @error('variable_denominador.nombre')
                                <span class="text-sm text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    @else
                        <div class="col-2">
                            <input type="text" placeholder="Variable" class="form-control" wire:model="variable_denominador.variable" maxlength="1">
                            @error('variable_denominador.variable')
                                <span class="text-sm text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div x-bind:class="open?'col-4':'col-6'">
                            <input type="text" placeholder="Nombre" class="form-control" wire:model="variable_denominador.nombre">
                            @error('variable_denominador.nombre')
                                <span class="text-sm text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-4" x-show="open">
                            <input type="text" placeholder="Url" class="form-control" wire:model="variable_denominador.api_url">
                            @error('variable_denominador.api_url')
                                <span class="text-sm text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    @endif
                    <div class="col-2">
                        <button class="btn btn-primary" wire:click="agregaVariable('denominador')">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-plus">
                                <line x1="12" y1="5" x2="12" y2="19"></line>
                                <line x1="5" y1="12" x2="19" y2="12"></line>
                            </svg>
                        </button>
                    </div>
                    @if ($tiene_api)
                    <div class="col-xs-12">
                        <div class="custom-control-primary custom-control custom-checkbox pt-1">
                            <input type="checkbox" class="custom-control-input" x-model="open">
                            <label>¿Tiene URL?</label>
                        </div>
                    </div>
                    @endif
                </div>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <table class="table table-hover table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th style="width: 15%">Variable</th>
                                    @if ($tiene_api)
                                        <th style="width: 40%">Nombre</th>
                                        <th style="width: 30%">Url</th>
                                    @else
                                        <th style="width: 50%">Nombre</th>
                                    @endif
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($variables_denominador as $key => $d)
                                    <livewire:ficha.formula.variable :variable="$d" :index="$key" :tiene_api="$tiene_api" :wire:key="'denominador-' . $key">
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="row">
                    <div class="col-12">
                        <label class="form-label"><strong>Fórmula numerador<span style="color: red">*</span></strong></label>
                        <input type="text" wire:model.lazy="formula.numerador" class="form-control" placeholder="Fórmula del numerador" />
                        @error('numerador')
                            <span class="text-sm text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="row">
                    <div class="col-12">
                        <label class="form-label"><strong>Fórmula denominador<span style="color: red">*</span></strong></label>
                        <input type="text" wire:model.lazy="formula.denominador" class="form-control" placeholder="Fórmula del denominador" />
                        @error('numerador')
                            <span class="text-sm text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-6 text-center">
                <h1 class="mb-0">
                    $ {{ $this->formula_numerador }} \over {{ $this->formula_denominador }} $
                </h1>
            </div>
            <div class="col-6 text-center">
                <h1 class="mb-0">
                    $ {{ $formula->numerador }} \over {{ $formula->denominador }} $
                </h1>
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-end">
            <x-jet-button wire:click.prevent="store()" class="btn btn-primary me-2 mt-2">
                Guardar
            </x-jet-button>
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cerrar
            </x-jet-button>
        </div>
    </x-slot>
</x-jet-dialog-modal>
