<div>
    <div class="card">
        <div class="card-header" style="padding-left: 0px;">
            <h4 class="card-title text-primary"><strong>FÓRMULA</strong></h4>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li>
                        <button type="button" class="btn btn-outline-primary waves-effect" wire:click="modalEntrada()">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-plus-circle">
                                <circle cx="12" cy="12" r="10"></circle>
                                <line x1="12" y1="8" x2="12" y2="16"></line>
                                <line x1="8" y1="12" x2="16" y2="12"></line>
                            </svg>
                            <span>
                                @if ($medicion)
                                    @if ($medicion->numerador)
                                        Editar
                                    @else
                                        Registrar
                                    @endif
                                @else
                                    Registrar
                                @endif

                            </span>
                        </button>
                    </li>
                </ul>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered border-black" style="width: 100%">
                <thead>
                    <tr>
                        <th colspan="3" class="text-center">FORMULA (NUMERADOR/DENOMINADOR)</th>
                    </tr>
                </thead>
                <tbody class="border-black">
                    <tr>
                        <td rowspan="2">
                            <div class="row text-center">
                                @if ($indicador)
                                    <strong> {{ $indicador->nombre }} </strong>
                                @endif
                            </div>
                        </td>
                        <td colspan="2">
                            <div class="table-responsive">
                                <table class="table table-bordered" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Numerador</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">
                                                $ {{ $this->formula_numerador }} $
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="table-responsive">
                                <table class="table table-bordered" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Denominador</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">
                                              $ {{ $this->formula_denominador }} $
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th class="text-center"><h4>Fórmula:</h4> </th>
                        <td colspan="2">
                            <div class="table-responsive">
                                <table class="table table-bordered" style="width: 100%">
                                    <tbody>
                                        <tr>
                                            <th colspan="2" class="text-center">
                                                <h3>  $ {{ $this->formula_numerador }} \over {{ $this->formula_denominador }} $</h3>
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>


    </div>
    <div>
        @include('livewire.ficha-indicador.formula.create')
    </div>
    <script>
        function scriptFormula() {
            Livewire.on('alertaFormula', function(datos) {
                nitificacion(datos['tipo'], datos['mensaje'], datos['tipo']=='error'?'Error':'¡Acción realizada!', 2000);
            });
        }
    </script>

</div>
