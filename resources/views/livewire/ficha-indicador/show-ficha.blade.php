@extends('layouts/contentLayoutMaster')

@section('title', 'Macroprocesos')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection
@section('page-style')
    {{-- Page css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <style>
        .border-black {
            border-color: rgb(77, 74, 74);
        }
    </style>
@endsection

@section('content')
    <!-- Dashboard Ecommerce Starts -->

    <section id="macroprocesos">

        <div class="card mb-0">
            <div class="card-body pb-0">
                <div class="card-header">
                    <h4 class="card-title text-primary">FICHA DE INDICADORES (KPI'S) DE PROCESO - NIVEL 1</h4>
                    <div class="heading-elements">
                        <ul class="list-inline">
                            <li>
                                <a href="{{ route('reportes.indicadores.excel', $ficha->id) }}" class="btn btn-gradient-primary">Descargar</a>
                                {{-- {{route('macroprocesos.ficha',$macroProcesoFicha->id)}} --}}
                            </li>
                            <li>
                                <a href="{{ route('macroprocesos.ficha', $macroProcesoFicha->id) }}" class="btn btn-gradient-primary">Regresar</a>
                                {{-- {{route('macroprocesos.ficha',$macroProcesoFicha->id)}} --}}
                            </li>
                            <li>
                                <a data-action="reload">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                                         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                         stroke-linecap="round" stroke-linejoin="round"
                                         class="feather feather-rotate-cw">
                                        <polyline points="23 4 23 10 17 10"></polyline>
                                        <path d="M20.49 15a9 9 0 1 1-2.12-9.36L23 10"></path>
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                @livewire('ficha.cabecera-ficha', ['ficha_id' => $ficha->id, 'codigo_macroproceso' => $macroProcesoFicha->codigo, 'nombre_macroproceso' => $macroProcesoFicha->nombre])
                <hr>
                <div class="row">
                    <h4 class="text-primary"><strong>RESPONSABLES DE LA INFORMACIÓN:</strong></h4>
                    <div class="col-lg-6 col-sm-12">
                        <div class="card mb-0">
                            <div class="card-body pb-0">
                                <!-- <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="areas-tab" data-bs-toggle="tab" href="#areas"
                                                aria-controls="home" role="tab" aria-selected="true" >Áreas</a>
                                        </li>
                                    </ul> -->
                                <!-- <div class="tab-content">
                                        <div class="tab-pane active" id="areas" aria-labelledby="areas-tab" role="tabpanel"> -->

                                @livewire('ficha.suministro-area', ['indicador_id' => $ficha->id])

                                <!-- </div>
                                    </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <!-- <h4 class="text-primary"><strong>RESPONSABLES DE LA INFORMACIÓN:</strong></h4> -->
                        <div class="card mb-0">
                            <div class="card-body pb-0">
                                <!-- <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" id="procesos-tab" data-bs-toggle="tab" href="#procesos" aria-controls="profile"
                                                role="tab" aria-selected="false">Procesos</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane" id="procesos" aria-labelledby="procesos-tab" role="tabpanel"> -->

                                @livewire('ficha.suministro-proceso', ['indicador_id' => $ficha->id, 'macroproceso_id' => $macroProcesoFicha->id])

                                <!-- </div>
                                    </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-sm-12">
                        <!-- <h4 class="text-primary"><strong>RESPONSABLES DE LA INFORMACIÓN:</strong></h4> -->
                        <div class="card mb-0">
                            <div class="card-body pb-0">
                                <!-- <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" id="responsables-tab" data-bs-toggle="tab" href="#responsables"
                                                aria-controls="about" role="tab" aria-selected="false">Responsables</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane" id="responsables" aria-labelledby="responsables-tab" role="tabpanel"> -->

                                @livewire('ficha.responsables', ['indicador_id' => $ficha->id])

                                <!-- </div>
                                    </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <!-- <h4 class="text-primary"><strong>RESPONSABLES DE LA INFORMACIÓN:</strong></h4> -->
                        <div class="card mb-0">
                            <div class="card-body pb-0">
                                <!-- <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" id="usuarios-tab" data-bs-toggle="tab" href="#usuarios"
                                                aria-controls="about" role="tab" aria-selected="false">Usuarios</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane" id="usuarios" aria-labelledby="usuarios-tab" role="tabpanel"> -->

                                @livewire('ficha.usuario', ['indicador_id' => $ficha->id])

                                <!-- </div>
                                    </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        @livewire('ficha.restricciones', ['indicador_id' => $ficha->id])
                    </div>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-sm-12">
                        @livewire('ficha.formula', ['indicador_id' => $ficha->id, 'anio_id' => getAnioIndicadorActual()])
                    </div>
                    <div class="col-lg-4 col-sm-12">
                        @livewire('ficha.unidad-medida', ['indicador_id' => $ficha->id, 'anio_id' => getAnioIndicadorActual()])
                    </div>
                    <hr>
                </div>
                <div class="row">
                    @livewire('ficha.meta-escala', ['indicador_id' => $ficha->id, 'anio_id' => getAnioIndicadorActual()])
                </div>
                <div class="row">
                    @livewire('ficha.resultado-evaluacion', ['indicador_id' => $ficha->id, 'anio_id' => getAnioIndicadorActual()])
                </div>
                <div class="row">
                    @livewire('ficha.grafico-barras', ['indicador_id' => $ficha->id])
                </div>
            </div>
            <hr>
        </div>
        {{-- <h3>Procesos de {{$macroproceso->nombre}}</h3> --}}
    </section>
    <!-- Dashboard Ecommerce ends -->
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/charts/chart.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/codigo-script.js')) }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2"></script>
@endsection
@yield('scripts')
@section('page-script')
    <script>
        function lanzarScripts() {
            scriptSuministroArea();
            scriptSuministroProceso();
            scriptResponsablesCalAnalisis();
            scriptUsuariosInteresado();
            scriptRestriccionesRiesgos();
            scriptFormula();
            scriptUnidadMedida();
            scriptResultadoEvaluacion();
            scriptMetaEscala();
            // cargarchartbar();
            scriptGraficoBarras();
        }
        window.onload = function() {
            lanzarScripts();

            $(".fechaflatpickr").flatpickr({
                dateFormat: "Y-m-d",
                minDate: '1940-01-01',
                locale: {
                    firstDayOfWeek: 1,
                    weekdays: {
                        shorthand: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                        longhand: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    },
                    months: {
                        shorthand: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Оct', 'Nov', 'Dic'],
                        longhand: ['Enero', 'Febreo', 'Мarzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    },
                },
            });


        }



        //window.onload = lanzarScripts;
    </script>
@endsection

<style>
    #combo {
        font-family: Tahoma, Verdana, Arial;
        font-size: 11px;
        color: #707070;
        background-color: #FFFFFF;
        border-width: 0;
    }

    input:focus,
    select:focus,
    textarea:focus,
    button:focus {
        outline: none;
    }

    .inputcentrado {
        text-align: center
    }

    table {
        width: 100%;
        table-layout: fixed;
        overflow-wrap: break-word;
    }
</style>
