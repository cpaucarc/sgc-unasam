<!-- Modal -->
<x-jet-dialog-modal wire:model="open" maxWidth="lg">
    <x-slot name="title">
        <h3>Agregar Nueva Área ue Suministra Información</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">

            <div class="col-12">
                <label class="form-label"><strong>Indicador<span style="color: red">*</span> </strong></label>
                <select wire:model="paramIndicador" class="form-select" disabled>
                    <option value="">-- Seleccione --</option>
                    @foreach ($param_indicador as $paramIndicador)
                        <option value="{{$paramIndicador->id}}">{{$paramIndicador->nombre}}</option>
                    @endforeach
                </select>
                @error('paramIndicador')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div id="form-recursos">
                <form>
                    <label class="form-label"><strong>Áreas que suministran información <span style="color: red">*</span> </strong></label>
                    <div class="mb-1 row">
                        <div class="col-sm-12">
                            <select id="paramInteresado" class="form-control" multiple>
                                <option value="NULL" disabled>--Seleccione--</option>
                                @foreach ($datosProbando as $lista_recurso)
                                    <option value="{{$lista_recurso->idInteresado}}">{{$lista_recurso->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('selectAreaInteresado')
                        <span class="text-sm text-danger">{{$message}}</span>
                        @enderror
                    </div>
                </form>
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">

            <div class="col-12 text-center">
                <x-jet-button wire:click.prevent="$emit('asignarAreasSuministro')" class="btn btn-primary me-2 mt-2">
                    Guardar
                </x-jet-button>
                <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                    Cancelar
                </x-jet-button>
            </div>
        </div>

    </x-slot>
</x-jet-dialog-modal>
