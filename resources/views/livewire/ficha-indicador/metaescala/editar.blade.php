<!-- Modal -->
<x-jet-dialog-modal wire:model="openEditarMetaEscala" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3>Editar Periodo - Meta - Escala </h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row pb-1">
            <div class="col-6 col-md-6">
                <span style="color: red">* </span><label class="form-label"><strong>Periodo</strong></label>
                <input type="text" wire:model="param_periodo_texto" class="form-control" placeholder="Periodo"/>               
            </div>
            <div class="col-6 col-md-6">
                <span style="color: red">* </span><label class="form-label"><strong>Meta</strong></label>
                <input type="text" wire:model="meta" wire:change="obtenerValores()" class="form-control" placeholder="Meta"/>
                @error('meta')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>   
        </div>
        <div class="row">
            <div class="col-6 col-md-6">
                <span style="color: red">* </span><label class="form-label"><strong>¿Es ascendente?</strong></label>
                <select wire:model="ascendente" wire:click="obtenerValores()" class="form-select">
                    <option value="">--Seleccione--</option> 
                    <option value="1">Sí</option> 
                    <option value="0">No</option> 
                </select>
                @error('meta')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div> 
        </div>
        <div class="row"> 
            <div>
                <hr>
            </div>
            
            <div class="col-12 col-md-12">
                <table class="table table-bordered" style="width: 100%">
                    <thead>
                        <tr>
                            <th class="text-center" colspan="3"><span style="color: red">* </span>ESCALA DE VALORACIÓN</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">
                                Muy Malo
                            </td>
                            <td class="text-center">
                                {{-- <label class="form-label"><strong>Min <span style="color: red">*</span></strong></label>              --}}
                                <input type="text" wire:model="muyMaloMin" class="form-control" placeholder="Min"/>
                                @error('muyMaloMin')
                                <span class="text-sm text-danger">{{$message}}</span>
                                @enderror
                            </td>
                            <td class="text-center">
                                {{-- <label class="form-label"><strong>Max <span style="color: red">*</span></strong></label>   --}}
                                <input type="text" wire:model="muyMaloMax" class="form-control" placeholder="Max"/>
                                    @error('muyMaloMax')
                                    <span class="text-sm text-danger">{{$message}}</span>
                                    @enderror                                                    
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                Malo
                            </td>
                            <td class="text-center">
                                {{-- <label class="form-label"><strong> Min<span style="color: red">*</span></strong></label> --}}
                                <input type="text" wire:model="maloMin" class="form-control" placeholder="Min"/>
                                @error('maloMin')
                                <span class="text-sm text-danger">{{$message}}</span>
                                @enderror
                            </td>
                            <td class="text-center">
                                {{-- <label class="form-label"><strong> Max<span style="color: red">*</span></strong></label> --}}
                                <input type="text" wire:model="maloMax" class="form-control" placeholder="Max"/>
                                @error('maloMax')
                                <span class="text-sm text-danger">{{$message}}</span>
                                @enderror                                                    
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                Regular
                            </td>
                            <td class="text-center">
                                {{-- <label class="form-label"><strong> Min <span style="color: red">*</span></strong></label> --}}
                                <input type="text" wire:model="regularMin" class="form-control" placeholder="Min"/>
                                @error('regularMin')
                                <span class="text-sm text-danger">{{$message}}</span>
                                @enderror
                            </td>
                            <td class="text-center"> 
                                {{-- <label class="form-label"><strong>Max <span style="color: red">*</span></strong></label> --}}
                                <input type="text" wire:model="regularMax" class="form-control" placeholder="Max"/>
                                @error('regularMax')
                                <span class="text-sm text-danger">{{$message}}</span>
                                @enderror                                                   
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                Satisfactorio
                            </td>
                            <td class="text-center">
                                {{-- <label class="form-label"><strong> Min<span style="color: red">*</span></strong></label> --}}
                                <input type="text" wire:model="satisfactorioMin" class="form-control" placeholder="Min"/>
                                @error('satisfactorioMin')
                                <span class="text-sm text-danger">{{$message}}</span>
                                @enderror
                            </td>
                            <td class="text-center"> 
                                {{-- <label class="form-label"><strong>Max <span style="color: red">*</span></strong></label> --}}
                                <input type="text" wire:model="satisfactorioMax" class="form-control" placeholder="Max"/>
                                @error('satisfactorioMax')
                                <span class="text-sm text-danger">{{$message}}</span>
                                @enderror                                                   
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                Muy Satisfactorio
                            </td>
                            <td class="text-center">
                                {{-- <label class="form-label"><strong>Min <span style="color: red">*</span></strong></label> --}}
                                <input type="text" wire:model="muySatisfactorioMin" class="form-control" placeholder="Min"/>
                                @error('muySatisfactorioMin')
                                <span class="text-sm text-danger">{{$message}}</span>
                                @enderror
                            </td>
                            <td class="text-center"> 
                                {{-- <label class="form-label"><strong>Max<span style="color: red">*</span></strong></label> --}}
                                <input type="text" wire:model="muySatisfactorioMax" class="form-control" placeholder="Max"/>
                                @error('muySatisfactorioMax')
                                <span class="text-sm text-danger">{{$message}}</span>
                                @enderror                                                   
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div> 
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="update()" class="btn btn btn-primary me-2 mt-2">
                Actualizar
            </x-jet-button>
            <x-jet-button wire:click.prevent="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>            
        </div>
        
    </x-slot>
</x-jet-dialog-modal>