<div>
    <div class="col-lg-12"> 
        {{-- DISEÑO --}}
        <div class="card">
           <div class="card-header" style="padding-left: 0px;">
               <h4 class="card-title text-primary"><strong>META - ESCALA DE VALORACIÓN</strong></h4>
               <div class="heading-elements">            
                   <ul class="list-inline mb-0">
                       <li>
                           <button type="button" class="btn btn-outline-primary waves-effect" wire:click="modalEntrada()">
                               <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                               <span>Agregar</span>
                           </button>
                       </li>
                   </ul>
               </div>
           </div>
           <div class="table-responsive">
               <table class="table table-bordered border-black">
                   <thead>
                       <tr> 
                           <th class="text-center align-middle" rowspan="2">PERIODO</th> 
                           <th class="text-center align-middle" rowspan="2">META</th>                                      
                           <th class="text-center" colspan="5">ESCALA DE VALORACIÓN</th>
                           <th class="text-center align-middle px-0" rowspan="2" style="width: 80px">ACCIÓN</th>
                       </tr>
                       <tr>                                       
                           <th class="text-center">MUY MALO</th>
                           <th class="text-center">MALO</th>
                           <th class="text-center">REGULAR</th>
                           <th class="text-center">SATISFACTORIO</th> 
                           <th class="text-center">MUY SATISFACTORIO</th>                                           
                       </tr>
                   </thead>
                 <tbody class="border-black">
                    @foreach($metas as $meta)
                    <tr>
                        <td class="text-center"> {{$anio_id}} {{$meta->detalleperiodo->detalle}}</td>                                       
                        <td class="text-center"> {{ $meta->meta }}</td>
                        @if ($meta->escalas)
                        @foreach($meta->escalas as $escala)                      
                        
                        <td class="text-center"> 
                            @if($escala->param_likert == 14001) [ @else < @endif {{$escala->valor_ini }} - {{$escala->valor_fin }} ]
                        </td>                      
                        @endforeach                           
                        @endif
                        <td style="padding: 0px 0px 0px 0px;">
                            <div class="d-flex align-items-center">                              
                                <button wire:click="editMetaEscala({{$meta->id}})" type="button" class="btn btn-icon rounded-circle btn-flat-primary waves-effect">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg>
                                </button>                          
                                <button wire:click="$emit('EliminarMetaEscala',{{$meta->id}})" type="button" class="btn btn-icon rounded-circle btn-flat-danger waves-effect">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                </button>                                
                                 
                            </div>                           
                        </td>                                                                                 
                       
                    </tr>
                    @endforeach                 
                    </tbody>
               </table>
           </div>
    
       </div>
    
        {{-- FIN DE DISEÑO                       --}}
       
    </div> 
    <div>
        @include('livewire.ficha-indicador.metaescala.create')
        @include('livewire.ficha-indicador.metaescala.editar')
    </div>

    <script>
        function scriptMetaEscala() {
            Livewire.on('alertaMetaEscala', function(datos){
                nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
            });

            Livewire.on('EliminarMetaEscala', metaId=>{
                Swal.fire({
                    title: '¿Está seguro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'error',
                    showCancelButton: true,
                    confirmButtonText: '¡Sí, elimínalo!',
					cancelButtonText: 'Cancelar'
                }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('ficha.meta-escala','destroy', metaId)          
                }
                })
            });	
        }    
    </script>

</div>