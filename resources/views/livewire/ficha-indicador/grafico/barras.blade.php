<div class="col-lg-12">
    <div class="card">
        <div class="card-header" style="padding-left: 0px;">
            <h4 class="card-title text-primary"><strong>EVOLUCIÓN DE INDICADOR</strong></h4>
        </div>
        @if(count($resultados))
        <div class="chartCard d-flex justify-content-center">
            <div class="chart-container" style="width: 600px; height:400px;">
                <div class="wrapper">
                    <canvas id="myChart"></canvas>
                </div>                          
            </div>
        </div>
        @else
            <h1>NO HAY GRÁFICO QUE MOSTRAR, INGRESE LOS DATOS NECESARIOS.</h1>
        @endif
    </div>
<script>
    function scriptGraficoBarras() {
        let myChart;

        Livewire.emit('render');

        Livewire.on('verGraficoBarras', function($labels,$resultados,$metas) {
        
            const data = {
                    labels: $labels.reverse(),
                    datasets: [
                        {
                            label: 'Resultado',
                            data: $resultados.reverse(),
                            backgroundColor: 'rgba(54, 162, 235, 0.2)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            borderWidth: 1,
                            datalabels:{
                                color: 'rgba(0, 50, 255 )', 
                                labels: {
                                    title: {
                                        font: {
                                            weight: 'bold'
                                        }
                                    }
                                }
                            }
                        },
                        {
                            label: 'Meta',
                            data: $metas.reverse(),
                            backgroundColor: 'rgba(255, 159, 64, 0.2)',
                            borderColor: 'rgba(255, 159, 64, 1)',
                            borderWidth: 2,
                            datalabels:{
                                color: 'black',
                                labels: {
                                    title: {
                                        font: {
                                            weight: 'bold'
                                        }
                                    }
                                },
                                anchor: 'end',
                                align: 'top',
                                offset: 1
                            },
                            type: "line"

                        }
                    ]
                };
            
            const legendMargin = {
                id: 'legendMargin',
                beforeInit(chart, legend, options){
                    const fitValue = chart.legend.fit;
                    chart.legend.fit  = function fit(){
                        fitValue.bind(chart.legend)();
                        return this.height +=20;
                    }
                }
            };

            // config 
            const config = {
                type: 'bar',
                data,
                plugins: [ChartDataLabels,legendMargin]
            };

            if (myChart) {
                myChart.destroy();
            }

            // render init block
            myChart = new Chart(
                document.getElementById('myChart'),
                config
            );                
        });
    }
</script>
</div>