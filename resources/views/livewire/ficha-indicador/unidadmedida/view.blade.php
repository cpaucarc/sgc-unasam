<div>
     {{-- DISEÑO --}}
     <div class="card">
        <div class="card-header" style="padding-left: 0px;">
            <h4 class="card-title text-primary"><strong>UNIDAD DE MEDIDA</strong></h4>
            <div class="heading-elements">            
                <ul class="list-inline mb-0">
                    <li>
                        <button type="button" class="btn btn-outline-primary waves-effect" wire:click="modalEntrada()">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                            <span>
                                @if ($medicionUnidad)
                                @if ($medicionUnidad->param_und_medida)
                                Editar 
                                @else  
                                Registrar
                                @endif                                  
                                @else
                                Registrar
                                @endif
                                </span>
                        </button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered border-black" role="grid">
                <thead>                                      
                    <tr>                                        
                        <th class="text-center">UNIDAD MEDIDA</th>
                        {{-- <th class="text-center" colspan="2">META</th>                                           --}}
                    </tr>                                       
                </thead>
              <tbody class="border-black">
                <tr>                                        
                    <td class="text-center"> 
                        <strong> 
                            @if ($medicionUnidad)
                                @if ($medicionUnidad->param_und_medida)
                                <strong> {{$medicionUnidad->detallemedida->detalle}}  </strong>                                     
                                @endif
                            @endif
                        
                    </strong> </td>
                    {{-- <td class="text-center" colspan="2">
                       @if ($medicionMeta)
                        <strong> {{$medicionMeta->meta }}  </strong>                                     
                        @endif 
                    </td> --}}
                </tr>
                <tr>
                    <td class="text-center" style=" background-color: #f3f2f7; font-size: 0.857rem; color: #6e6b7b; font-weight: bold;">
                        FRECUENCIA DE MEDICIÓN
                    </td>
                     {{--<td class="text-center" colspan="2" style=" background-color: #f3f2f7; font-size: 0.857rem; color: #6e6b7b; font-weight: bold;">
                       ESCALA DE  VALORACION
                    </td>--}}
                </tr> 
                <tr>
                    <td class="text-center" >
                        @if ($medicionUnidad)
                           @if ($medicionUnidad->param_frec_medicion)
                            <strong> {{$medicionUnidad->detallemedicion->detalle}}  </strong>                                     
                            @endif 
                        @endif                         
                    </td>
                    {{-- <td class="text-center" > Muy Malo</td>
                    <td class="text-center" > 
                        @if ($escalamuyMalo)
                        <strong> [{{$escalamuyMalo->valor_ini}} - {{$escalamuyMalo->valor_fin}}] </strong>                                     
                        @endif
                    </td> --}}
                </tr>
                <tr>
                    <td class="text-center" style=" background-color: #f3f2f7; font-size: 0.857rem; color: #6e6b7b; font-weight: bold;" >FRECUENCIA DE REPORTE</td>
                    {{-- <td class="text-center" >  Malo</td>
                    <td class="text-center" > 
                        @if ($escalaMalo)
                        <strong> <{{$escalaMalo->valor_ini}} - {{$escalaMalo->valor_fin}}] </strong>                                     
                        @endif
                    </td> --}}
                </tr>
                <tr>
                    <td class="text-center" >
                        @if ($medicionUnidad)
                            @if ($medicionUnidad->param_frec_reporte)
                            <strong> {{$medicionUnidad->detallereporte->detalle}}  </strong>                                     
                            @endif
                        @endif
                        
                    </td>
                    {{-- <td class="text-center" >  Regular</td>
                    <td class="text-center" >
                        @if ($escalaRegular)
                        <strong> <{{$escalaRegular->valor_ini}} - {{$escalaRegular->valor_fin}}] </strong>                                     
                        @endif
                    </td> --}}
                </tr>
                <tr>
                    <td class="text-center" style=" background-color: #f3f2f7; font-size: 0.857rem; color: #6e6b7b; font-weight: bold;">FRECUENCIA DE REVISIÓN</td>
                    {{-- <td class="text-center" >  Satisfactorio</td>
                    <td class="text-center" > 
                        @if ($escalaSatisfactorio)
                        <strong> <{{$escalaSatisfactorio->valor_ini}} - {{$escalaSatisfactorio->valor_fin}}] </strong>                                     
                        @endif
                    </td> --}}
                </tr>
                <tr>
                    <td class="text-center" >
                        @if ($medicionUnidad)
                            @if ($medicionUnidad->param_frec_revision)
                            <strong> {{$medicionUnidad->detallerevision->detalle}}  </strong>                                     
                            @endif
                        @endif
                        
                    </td>
                    {{-- <td class="text-center" >  Muy Satisfactorio</td>
                    <td class="text-center" > 
                        @if ($escalamuySatisfactorio)
                        <strong> <{{$escalamuySatisfactorio->valor_ini}} - {{$escalamuySatisfactorio->valor_fin}}] </strong>                                     
                        @endif
                    </td> --}}
                </tr>

                </tbody>
            </table>
        </div>

    </div>

     {{-- FIN DE DISEÑO --}} 
     <div>
        @include('livewire.ficha-indicador.unidadmedida.create')
    </div>

    <script>
        function scriptUnidadMedida(){
           Livewire.on('alertaUnidadMedida', function(datos){
               nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
           });
        }    
       
   </script>


</div>