<!-- Modal -->
<x-jet-dialog-modal wire:model="openCrearUnidadMedida" maxWidth="lg">
    <x-slot name="title">
        <h3>Registrar unidad de medida </h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row">
            <div class="col-6 col-md-6">
                <label class="form-label"><strong>Unidad Medida<span style="color: red">*</span></strong></label>
                <select wire:model="param_und_medida" class="form-select">
                    <option value="">-- Seleccione --</option>  
                    @foreach($unidadmedidas as $unidadmedida)
                    <option value="{{$unidadmedida->id}}">{{ $unidadmedida->detalle }}</option>
                    @endforeach                 
                </select>
                @error('param_und_medida')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            {{-- <div class="col-6 col-md-6">
                <label class="form-label"><strong>Meta <span style="color: red">*</span></strong></label>
                <input type="text" wire:model="meta" class="form-control" placeholder="Meta"/>
                @error('meta')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div> --}}
            <div class="col-6 col-md-6">
                <label class="form-label"><strong>Frecuencia de Medición<span style="color: red">*</span></strong></label>
                <select wire:model="param_frec_medicion"
                 @if($hayregistros)
                    disabled
                @endif class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach($tipoFrecMediciones as $tipoFrecMedicion)
                    <option value="{{$tipoFrecMedicion->id}}">{{ $tipoFrecMedicion->detalle }}</option>
                    @endforeach                   
                </select>
                @error('param_frec_medicion')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-6 col-md-6">
                <label class="form-label"><strong>Frecuencia de Reporte<span style="color: red">*</span></strong></label>
                <select wire:model="param_frec_reporte" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach($tipoFrecReportes as $tipoFrecReporte)
                    <option value="{{$tipoFrecReporte->id}}">{{ $tipoFrecReporte->detalle }}</option>
                    @endforeach                   
                </select>
                @error('param_frec_reporte')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-6 col-md-6">
                <label class="form-label"><strong>Frecuencia de Revision<span style="color: red">*</span></strong></label>
                <select wire:model="param_frec_revision" class="form-select">
                    <option value="">-- Seleccione --</option> 
                    @foreach($tipoFrecRevisiones as $tipoFrecRevision)
                    <option value="{{$tipoFrecRevision->id}}">{{ $tipoFrecRevision->detalle }}</option>
                    @endforeach                  
                </select>
                @error('param_frec_revision')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">            
            <x-jet-button wire:click.prevent="store()" class="btn btn-primary me-2 mt-2">
                Guardar
            </x-jet-button>
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>        
    </x-slot>
</x-jet-dialog-modal>