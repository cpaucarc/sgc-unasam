<div>
    <div class="card-datatable">
        <div class="row">
            <div class="col-12 col-md-7">
                <div class="table-responsive">
                    <table class="table" style="border: 2px solid #636e72;  border-collapse: collapse;">
                        <tr style="border: 1px solid black;  border-collapse: collapse;">
                            <td class="py-0 px-1" class="text-center" style="background-color: #dfe6e9;border: 2px solid #636e72;  border-collapse: collapse; width: 180px;">
                                <div class="col-12 text-center">NOMBRE MACROPROCESO</div>
                            </td>
                            <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                {{ $codigo_macroproceso }}: {{ $nombre_macroproceso }}
                            </td>
                        </tr>
                        <tr style="border: 1px solid black;  border-collapse: collapse;">
                            <td style="background-color: #dfe6e9; width: 40%; border: 2px solid #636e72;  border-collapse: collapse;">
                                <div class="col-12 text-center">NOMBRE DEL INDICADOR</div>
                            </td>
                            <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                {{ $nombreFicha }}
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                <div class="col-12 text-center">TIPO</div>
                            </td>
                            <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                {{ $tipoFicha }}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-12 col-md-5">
                <div class="table-responsive">
                    <table class="table text-nowrap" style="border: 2px solid #636e72;  border-collapse: collapse;">
                        <tr style="border: 1px solid black;  border-collapse: collapse;">
                            <td class="px-1" style="background-color: #dfe6e9; width: 160px; border: 2px solid #636e72;  border-collapse: collapse;">
                                <div class="col-12">CÓDIGO</div>
                            </td>
                            <td class="px-1" colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                {{ $codigoFicha}}
                            </td>
                        </tr>
                        <tr style="border: 1px solid black;  border-collapse: collapse;">
                            <td class="py-0 px-1" style="background-color: #dfe6e9; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                                VERSIÓN
                            </td>
                            <td class="inputcentrado px-1" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                <select wire:model="version" class="form-select" wire:change="seleccionarVersion">
                                @if ($versiones)
                                    @foreach ($versiones->sortBy('version.numero') as $version)
                                        <option value="{{ $version->id }}">{{ $version->version->numero }}</option>
                                    @endforeach
                                @endif
                                </select>
                            </td>
                            <td class="py-0 px-1" style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                VIGENCIA
                            </td>
                            <td class="px-1" style="width: 30%; border: 2px solid #636e72;  border-collapse: collapse;">
                                {{ date('d-m-Y', strtotime($fecha_vigencia)) }}
                            </td>
                        </tr>

                        <tr style="border: 1px solid black;  border-collapse: collapse;">
                            <td class="px-1 text-center" style="background-color: #007af3; color:white; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                                <b>AÑO</b>
                            </td>
                            <td class="inputcentrado" colspan="3">
                                <select wire:model="miParametroAnio" class="form-select" wire:change="setMiAnioIndicador()">
                                    @if ($anios)
                                        @foreach ($anios as $anio)
                                            <option value="{{ $anio->id }}" {{ $anio->id = getAnioIndicadorActual() ? 'selected' : '' }}>{{ $anio->anio }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </td>

                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row py-1">
        <div class="col-lg-6 col-sm-12">
            <h4 class="text-primary"><strong>DEFINICIÓN:</strong></h4>
            <table style="width: 98%; border: 2px solid #d3dadd;  border-collapse: collapse; padding-left: 20px">
                <tr>
                    <td>
                        <div class="col-12 p-1">
                            <input wire:model="definicionFicha" class="inputcentrado" style="width:100%;border-style: hidden;" type="text" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-lg-6 col-sm-12">
            <h4 class="text-primary"><strong>OBJETIVO DEL INDICADOR:</strong></h4>
            <table style="width: 98%; border: 2px solid #d3dadd;  border-collapse: collapse; padding-left: 20px">
                <tr>
                    <td>
                        <div class="col-12 p-1">
                            <input wire:model="objetivoFicha" class="inputcentrado" style="width:100%;border-style: hidden;" type="text"
                                   name="name">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>

</div>
