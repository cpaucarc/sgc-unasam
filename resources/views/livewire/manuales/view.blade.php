<div class="card border-black">
    <div class="text-center p-1">
        <h1 class="text-primary mb-0"><strong>LISTADO DE MANUALES DE PROCESO</strong></h1>
    </div>
    <div class="card-body pt-0">
        <div class="row gx-2 border-black rounded-2 p-2 mb-1">
            <div class="col-12 col-md-4">
                <label class="form-label"><strong class="text-primary">INICIO</strong></label>
                <button wire:click="verMacroprocesos()" class="btn btn-primary form-control">MANUALES DE MACROPROCESO</button>
            </div>
            <div class="col-12 col-md-8">
                <label class="form-label"><strong class="text-primary">MANUALES DEL PROCESO</strong></label>
                <div wire:ignore>
                    <select wire:model="macroproceso_id" id="macroproceso" class="form-select">
                        <option value="NULL">-- Seleccione --</option>
                        @foreach ($macroprocesos as $macroproceso)
                            <option value="{{$macroproceso->id}}">{{$macroproceso->codigo}}: {{$macroproceso->nombre}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row gx-2 border-black rounded-2 p-2">
            <div class="card card-transaction px-0 mb-0">
                <h2>
                    LISTA DE MANUALES DE {{$tipo_proceso}}
                </h2>
                <table class="table b-table">
                    @foreach ($datos as $dato)
                        <tr>
                            <td><strong>{{$dato->codigo}}:</strong> {{$dato->nombre}}</td>
                            @switch($tipo_proceso)
                                @case('MACROPROCESOS')
                                    <td><a href="{{route('manuales.macroproceso.view',$dato->id)}}" class="btn btn-primary" target="_blank">Ver Manual</a></td>
                                    @break

                                @case('PROCESOS')
                                    <td><a href="{{route('manuales.proceso.view',$dato->id)}}" class="btn btn-primary" target="_blank">Ver Manual</a></td>
                                    @break

                            @endswitch

                        </tr>
                    @endforeach

                </table>
            </div>
        </div>
    </div>
    <script>
        function scriptCaracterizacion() {
            selectMacroproceso = $('#macroproceso');

            asignarSelect2(selectMacroproceso,'100%','Buscar macroproceso');

            selectMacroproceso.on('change', function () {
                $data = selectMacroproceso.select2("val");
                @this.set('macroproceso_id', $data);
                //console.log($data);
                if($data !="NULL"){
                    Livewire.emitTo('manual-proceso','verProcesos');
                }                
            });

            Livewire.on('resetearSelect', function () {   
                selectMacroproceso.val('NULL');
                selectMacroproceso.trigger('change');
            });
            
        }
    </script>
</div>
