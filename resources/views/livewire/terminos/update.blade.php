<!-- Modal -->
<x-jet-dialog-modal wire:model="updateMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3>Actualizar Término</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-9">
                <label class="form-label"><strong><span style="color:red">*</span> Nombre del Término</strong></label>
                <input
                    type="text"
                    wire:model="nombre"
                    class="form-control"
                    placeholder="Nombre del término" maxlength="200"
                />
                @error('nombre')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-3">
                <label class="form-label"><strong><span style="color:red">*</span> Estado de Término</strong></label>
                <select wire:model="activo" class="form-select">
                    <option value="">-- Seleccione --</option>
                    <option value="1">ACTIVO</option>
                    <option value="0">INACTIVO</option>
                </select>
                @error('activo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-12">
                <label class="form-label"><strong><span style="color:red">*</span> Definición</strong></label>
                <textarea
                    wire:model="definicion"
                    class="form-control"
                    placeholder="Definición del término"
                    rows="2" maxlength="4000"
                ></textarea>
                @error('definicion')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="update()" class="btn btn-primary me-1 mt-2">
                Actualizar
            </x-jet-button>
            <x-jet-button wire:click.prevent="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
