<!-- Modal -->
<x-jet-dialog-modal wire:model="updateMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3 class="address-title text-center mb-1"><strong>Actualizar Resultado</strong></h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">

            <div class="col-6 col-md-6">
                <label class="form-label"><strong>Período </strong></label>
                <select wire:model="param_periodo" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($parametro_periodos as $parametro_periodo)
                        <option value="{{$parametro_periodo->id}}">{{$parametro_periodo->detalle}}</option>
                    @endforeach
                </select>
                @error('param_periodo')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-6 col-md-6">
                <label class="form-label"><strong>Valor numerador</strong></label>
                <input type="number" wire:model="valor_numerador" class="form-control" placeholder="Valor numerador"/>
                @error('valor_numerador')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-6 col-md-6">
                <label class="form-label"><strong>Valor denominador</strong></label>
                <input type="number" wire:model="valor_denominador" class="form-control" placeholder="Valor denominador"/>
                @error('valor_denominador')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-6 col-md-6">
                <label class="form-label"><strong>Valor indicador</strong></label>
                <input type="number" wire:model="valor_indicador" class="form-control" placeholder="Valor indicador"/>
                @error('valor_indicador')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-6 col-md-6">
                <label class="form-label"><strong><span style="color:red">*</span> Indicador </strong></label>
                <select wire:model="indicador_id" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($parametro_indicadores as $parametro_indicador)
                        <option value="{{$parametro_indicador->id}}">{{$parametro_indicador->nombre}}</option>
                    @endforeach
                </select>
                @error('indicador_id')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-6 col-md-6">
                <label class="form-label"><strong><span style="color:red">*</span> Año</strong></label>
                <select wire:model="anio_id" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($parametro_anios as $parametro_anio)
                        <option value="{{$parametro_anio->id}}">{{$parametro_anio->anio}}</option>
                    @endforeach
                </select>
                @error('anio_id')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="update()" class="btn btn-primary me-1 mt-2">
                Actualizar
            </x-jet-button>
            <x-jet-button wire:click.prevent="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
