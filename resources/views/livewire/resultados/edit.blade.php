<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-sm-5 col-md-5">
                <label class="form-label"><strong class="text-primary">NOMBRE DEL MACROPROCESO</strong></label>
                <select name="macroproceso_id" id="macroproceso_id" class="form-select" wire:model="macroproceso_id" wire:change="obtenerIndicadores"
                        wire:loading.attr="disabled">
                    @foreach ($macroprocesos as $m)
                        <option value="{{ $m->id }}">{{ $m->codigo }} - {{ $m->nombre }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-12 col-sm-5 col-md-5">
                <label class="form-label"><strong class="text-primary">INDICADOR</strong></label>
                <select name="indicador_id" id="indicador_id" class="form-select" wire:change="seleccionar" wire:model="indicador_id"
                        wire:loading.attr="disabled">
                    @foreach ($indicadores as $i)
                        <option value="{{ $i->id }}">{{ $i->codigo }} - {{ $i->nombre }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-12 col-sm-5 col-md-2">
                <label class="form-label"><strong class="text-primary">AÑO</strong></label>
                <select name="anio_id" id="anio_id" class="form-select" wire:model="anio_id" wire:change="seleccionar" wire:loading.attr="disabled">
                    @foreach ($anios as $a)
                        <option value="{{ $a->anio }}">{{ $a->anio }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr>
        <div class="row">
            @if ($tiene_metas)
                <div class="col-12">
                    <livewire:ficha.resultado-evaluacion :indicador_id="$indicador_id" :anio_id="$anio_id"></livewire:ficha.resultado-evaluacion>
                </div>
            @else
                <div class="col-12">
                    <div class="alert alert-primary alert-dismissible">
                        <div class="alert-body d-flex justify-content-between">
                            Este indicador no tiene metas registradas para el año seleccionado.
                            <a href="/macroprocesos/{{$indicador_id}}/{{$anio_id}}/showFicha">Ir a registrar metas</a>
                        </div>
                    </div>
                </div>
            @endif

            <div wire:loading class="b-overlay position-absolute text-center" style="inset: 0px; z-index: 10;">
                <div class="position-absolute bg-white rounded-sm" style="inset: 0px; opacity: 0.75;"></div>
                <div class="position-absolute" style="top: 50%; left: 50%; transform: translateX(-50%) translateY(-50%);">
                    <span aria-hidden="true" class="spinner-border text-primary">
                    </span>
                    <br><br>
                    <span class="pt-5">Obteniendo datos...</span>
                </div>
            </div>
        </div>
    </div>
    <script>
        function scriptResultadoEvaluacion() {
            Livewire.on('alertaResultadoEvaluacion', function(datos) {
                nitificacion(datos['tipo'], datos['mensaje'], ' ¡Acción realizada!', 2000);
        
            });
            Livewire.on('EliminarResultadoEvaluacion', resultadoId => {
                Swal.fire({
                    title: '¿Está seguro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'error',
                    showCancelButton: true,
                    confirmButtonText: '¡Sí, elimínalo!',
                    cancelButtonText: 'Cancelar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emitTo('ficha.resultado-evaluacion', 'destroy', resultadoId)
                    }
                })
            });
        }
    </script>
</div>
