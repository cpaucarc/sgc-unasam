<!-- Modal -->
<x-jet-dialog-modal wire:model="updateMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3>Actualizar Indicador</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 mb-75">
            <div class="col-md-6">
                <div class="row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <strong><span style="color:red">* </span>Código</strong>
                    </label>
                    <div class="col-sm-9">
                        <input type="text" wire:model="codigo" class="form-control" placeholder="Código" maxlength="40" />
                    </div>
                    @error('codigo')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <label for="colFormLabel" class="col-sm-3 col-form-label">
                        <strong><span style="color:red">* </span>Versión</strong>
                    </label>
                    <div class="col-sm-9">
                        <select wire:model="version_id" class="form-select" >
                            <option value="">--Seleccione--</option>
                            @foreach($versiones as $version)
                            <option value="{{$version->id}}">{{ $version->numero }}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('version_id')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row gy-1 gx-2 mb-75">
            <div class="col-lg-12">
                <label class="form-label"> <strong><span style="color:red">*</span> Nombre</strong> </label>
                <input
                    type="text"
                    wire:model="nombre"
                    class="form-control"
                    placeholder="Nombre" maxlength="250"
                />
                @error('nombre')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        {{-- <div class="row gy-1 gx-2 mb-75">
            <div class="col-12 col-md-2">
                <label class="form-label"> <strong> <span style="color: red">*</span>Código </strong></label>
                <input
                    type="text"
                    wire:model="codigo"
                    class="form-control"
                    placeholder="Código" maxlength="40"
                />
                @error('codigo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-2">
                <label class="form-label"> <strong><span style="color: red">*</span> Versión </strong></label>
                <select wire:model="version_id" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($versiones as $version)
                    <option value="{{$version->id}}">{{ $version->numero }}</option>
                    @endforeach
                  </select>
               @error('version_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-8">
                <label class="form-label"> <strong> <span style="color: red">*</span> Nombre </strong></label>
                <input
                    type="text"
                    wire:model="nombre"
                    class="form-control"
                    placeholder="Nombre" maxlength="250"
                />
                @error('nombre')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div> --}}
        <div class="row gy-1 gx-2 mb-75">
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> <span style="color: red">*</span>  Macroproceso </strong></label>
                <select wire:model="macroproceso_id" wire:change="setProcesoSelect()" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($macroprocesos as $macroproceso)
                    <option value="{{$macroproceso->id}}">{{ $macroproceso->nombre }}</option>
                    @endforeach
                  </select>
               @error('macroproceso_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> Proceso </strong></label>
                <select wire:model="proceso_id" wire:change="setSubProcesoSelect()" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($procesos as $proceso)
                    <option value="{{$proceso->id}}">{{ $proceso->nombre }}</option>
                    @endforeach
                  </select>
               @error('proceso_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2 mb-75">
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> Subproceso </strong></label>
                <select wire:model="subproceso_id" wire:change="setMicroProcesoSelect()" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($subprocesos as $subproceso)
                    <option value="{{$subproceso->id}}">{{ $subproceso->nombre }}</option>
                    @endforeach
                  </select>
               @error('subproceso_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> Microproceso </strong></label>
                <select wire:model="microproceso_id" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($microprocesos as $microproceso)
                    <option value="{{$microproceso->id}}">{{ $microproceso->nombre }}</option>
                    @endforeach
                  </select>
               @error('microproceso_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2 mb-75">
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> <span style="color: red">*</span> Tipo </strong></label>
                <select wire:model="param_tipoindicador" class="form-select">
                    <option value="">--Seleccione--</option>
                    @foreach($tipoIndicadores as $tipo)
                    <option value="{{$tipo->id}}">{{ $tipo->detalle }}</option>
                    @endforeach
                  </select>
               @error('param_tipoindicador')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"><strong>General</strong></label>
                <select wire:model="es_general" class="form-select" >
                    <option value="">--Seleccione--</option>
                    <option value="1">SI</option>
                    <option value="0">NO</option>
                  </select>
               @error('es_general')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2 mb-75">
            <div class="col-12 col-md-12">
                <label class="form-label"><strong>Definición</strong></label>
                <textarea
                    wire:model="definicion"
                    class="form-control"
                    placeholder="Definición" maxlength="4000"
                ></textarea>
                @error('definicion')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2 mb-75">
            <div class="col-12 col-md-12">
                <label class="form-label"><strong>Objetivo</strong></label>
                <textarea
                    wire:model="objetivo"
                    class="form-control"
                    placeholder="Objetivo" maxlength="4000"
                ></textarea>
                @error('objetivo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>

    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="update()" class="btn btn btn-primary me-1 mt-2">
                Actualizar
            </x-jet-button>
            <x-jet-button wire:click.prevent="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
