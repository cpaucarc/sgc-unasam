<div class="card">
    <div class="card-datatable table-responsive">
        <div class="dataTables_wrapper dt-bootstrap5 no-footer">
            <div class="d-flex justify-content-between align-items-center header-actions mx-2 row mt-75">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="dataTables_length">
                            <label>Mostrar
                                <select wire:model="cantidad" class="form-select">
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select> entradas
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="dt-action-buttons d-flex align-items-center justify-content-lg-end justify-content-center flex-md-nowrap flex-wrap">
                            <div class="me-1">
                                <div id="DataTables_Table_0_filter" class="dataTables_filter">
                                    <label>
                                        Buscar:<input type="search" class="form-control" wire:model="search" placeholder="">
                                    </label>
                                </div>
                            </div>
                            <div class="dt-buttons btn-group flex-wrap">
                                @can('create', App\Models\Indicador::class)
                                    <x-jet-button wire:click="$set('open',true)" class="btn add-new btn-primary mt-50">
                                        <span>Agregar Indicador</span>
                                    </x-jet-button>
                                @endcan
                            </div>
                        </div>
                    </div>
                    <div>
                        @include('livewire.indicadors.create')
                        @include('livewire.indicadors.update')
                        @include('livewire.indicadors.show')
                    </div>
                </div>
            </div>

            @can('viewAny', App\Models\Indicador::class)
                <div class="table-responsive px-25">
                    <table class="datatables-permissions table dataTable no-footer dtr-column table-bordered " id="DataTables_Table_0" role="grid"
                           aria-describedby="DataTables_Table_0_info" style="width: 100%;">
                        <thead class="table-light">
                            <tr role="row">
                                <th class="sorting cursor-pointer" wire:click="ordenar('codigo')">Código</th>
                                <th class="sorting cursor-pointer" wire:click="ordenar('nombre')">Nombre del Indicador</th>
                                <th class="sorting cursor-pointer" wire:click="ordenar('nombre')">Proceso</th>
                                <th class="sorting cursor-pointer" wire:click="ordenar('version_id')">Versión</th>
                                <th aria-label="Estado">Estado</th>
                                <th class="text-center" aria-label="Acciones" width="100">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($indicadores as $indicador)
                                <tr>
                                    <td>
                                        @if ($indicador->codigo)
                                            {{ $indicador->codigo }}
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>{{ $indicador->nombre }}</td>
                                    <td>
                                        @if ($indicador->macroproceso_id)
                                            <strong>{{ $indicador->macroproceso->codigo }}: </strong>{{ $indicador->macroproceso->nombre }} <br>
                                        @endif
                                        @if ($indicador->proceso_id)
                                            <strong>{{ $indicador->proceso->codigo }}: </strong>{{ $indicador->proceso->nombre }} <br>
                                        @endif
                                        @if ($indicador->subproceso_id)
                                            <strong>{{ $indicador->subproceso->codigo }} : </strong>{{ $indicador->subproceso->nombre }} <br>
                                        @endif
                                        @if ($indicador->microproceso_id)
                                            <strong>{{ $indicador->macriproceso->codigo }}: </strong>{{ $indicador->macriproceso->nombre }} <br>
                                        @endif
                                    </td>
                                    <td>{{ $indicador->version->descripcion }}</td>
                                    @if ($indicador->activo == 1)
                                        <td>
                                            <span class="badge rounded-pill badge-light-success" text-capitalized="">Activo</span>
                                        </td>
                                    @else
                                        <td>
                                            <span class="badge rounded-pill badge-light-secondary" text-capitalized="">Inactivo</span>
                                        </td>
                                    @endif
                                    <td>
                                        <div class="btn-group">
                                            {{--@can('audit', $indicador)
                                                <a wire:click="audit({{ $indicador->id }})" class="btn btn-icon px-0"
                                                   data-bs-toggle="tooltip" data-bs-placement="top" title="Auditar indicador">
                                                    <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1024 1024" fill="currentColor">
                                                        <path
                                                              d="M296 250c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8h384c4.4 0 8-3.6 8-8v-48c0-4.4-3.6-8-8-8H296zm184 144H296c-4.4 0-8 3.6-8 8v48c0 4.4 3.6 8 8 8h184c4.4 0 8-3.6 8-8v-48c0-4.4-3.6-8-8-8zm-48 458H208V148h560v320c0 4.4 3.6 8 8 8h56c4.4 0 8-3.6 8-8V108c0-17.7-14.3-32-32-32H168c-17.7 0-32 14.3-32 32v784c0 17.7 14.3 32 32 32h264c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8zm440-88H728v-36.6c46.3-13.8 80-56.6 80-107.4 0-61.9-50.1-112-112-112s-112 50.1-112 112c0 50.7 33.7 93.6 80 107.4V764H520c-8.8 0-16 7.2-16 16v152c0 8.8 7.2 16 16 16h352c8.8 0 16-7.2 16-16V780c0-8.8-7.2-16-16-16zM646 620c0-27.6 22.4-50 50-50s50 22.4 50 50-22.4 50-50 50-50-22.4-50-50zm180 266H566v-60h260v60z">
                                                        </path>
                                                    </svg>
                                                </a>
                                            @endcan --}}
                                            @can('view', $indicador)
                                                <button wire:click="show({{ $indicador->id }})" class="btn btn-icon px-0"
                                                        data-bs-toggle="tooltip" data-bs-placement="top" title="Ver indicador">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 24 24"
                                                         stroke="currentColor" stroke-width="2">
                                                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                                                    </svg>
                                                </button>
                                            @endcan
                                            @can('update', $indicador)
                                                <button wire:click="edit({{ $indicador->id }})" class="btn btn-icon px-0"
                                                        data-bs-toggle="tooltip" data-bs-placement="top" title="Editar indicador">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 24 24"
                                                         stroke="currentColor" stroke-width="2">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                                    </svg>
                                                </button>
                                            @endcan
                                            @can('verficha', $indicador)
                                                <a href="{{ route('show.ficha', ['indicador' => $indicador->id, 'anio' => date('Y')]) }}" target="_blank"
                                                   class="btn btn-icon px-0" data-bs-toggle="tooltip" data-bs-placement="top"
                                                   title="Ver Ficha de indicador">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 24 24"
                                                         stroke="currentColor" stroke-width="2">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                                                    </svg>
                                                </a>
                                            @endcan
                                            @can('delete', $indicador)
                                                <button wire:click="$emit('EliminarIndicador',{{ $indicador->id }})"
                                                        class="btn btn-icon px-0" data-bs-toggle="tooltip"
                                                        data-bs-placement="top" title="Eliminar indicador">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 24 24"
                                                         stroke="currentColor" stroke-width="2">
                                                        <path stroke-linecap="round" stroke-linejoin="round"
                                                              d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                    </svg>
                                                </button>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-between mx-2 row mb-1">
                        <div class="col-sm-12 col-md-6">
                            <div class="dataTables_info" aria-live="polite">
                                Mostrar {{ $indicadores->firstItem() }} a {{ $indicadores->lastItem() }} de {{ $indicadores->total() }} entradas
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            {{ $indicadores->links() }}
                        </div>
                    </div>
                </div>
            @endcan
        </div>
    </div>
</div>
<script>
    window.onload = function() {
        Livewire.on('alertRespuesta', function(datos) {
            nitificacion(datos['tipo'], datos['mensaje'], ' ¡Acción realizada!', 2000);
        });

        Livewire.on('EliminarIndicador', indicadorId => {
            Swal.fire({
                title: '¿Está seguro?',
                text: "¡No podrás revertir esto!",
                icon: 'error',
                showCancelButton: true,
                confirmButtonText: '¡Sí, elimínalo!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('indicadors', 'destroy', indicadorId)
                }
            })
        });
        Livewire.on('existeIndicador', function() {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                confirmButtonColor: '#3085d6',
                text: '!El código ya existe!'
            })
        });
        Livewire.on('existeIndicadorVersion', function($codigo) {
            Swal.fire({
                title: '¿Está seguro de crear?',
                text: "El indicador con código " + $codigo + " ya existe en otra versión.",
                icon: 'error',
                showCancelButton: true,
                cancelButtonColor: '#ea5455',
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#3085d6',
                confirmButtonText: ' Sí '
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('indicadors', 'storeIndicador')
                }
            })
        });
        Livewire.on('updateIndicadorVersion', function($codigo) {
            Swal.fire({
                title: '¿Está seguro de actualizar?',
                text: "El indicador con código " + $codigo + " ya existe en otra versión.",
                icon: 'error',
                showCancelButton: true,
                cancelButtonColor: '#ea5455',
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#3085d6',
                confirmButtonText: ' Sí '
            }).then((result) => {
                if (result.isConfirmed) {
                    Livewire.emitTo('indicadors', 'storeIndicador')
                }
            })
        });
        Livewire.on('updateTooltips', function($codigo) {
            $('[data-bs-toggle="tooltip"]').tooltip();
            $('[data-bs-toggle="popover"]').popover();
        });
    }
</script>
