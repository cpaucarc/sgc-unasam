<!-- Modal -->
<x-jet-dialog-modal wire:model="openShow" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3>Detalle Indicador</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2">
            <div class="col-12 col-md-2">
                <label class="form-label"> <strong> <span style="color: red">*</span>Código </strong></label>
                <input
                       type="text"
                       wire:model="codigo"
                       class="form-control"
                       placeholder="Código" maxlength="40" disabled />
                @error('codigo')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-12 col-md-2">
                <label class="form-label"> <strong><span style="color: red">*</span> Versión </strong></label>
                <select wire:model="version_id" class="form-select" disabled>
                    <option value="">--Seleccione--</option>
                    @foreach ($versiones as $version)
                        <option value="{{ $version->id }}">{{ $version->numero }}</option>
                    @endforeach
                </select>
                @error('version_id')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-12 col-md-8">
                <label class="form-label"> <strong> <span style="color: red">*</span> Nombre </strong></label>
                <input
                       type="text"
                       wire:model="nombre"
                       class="form-control"
                       placeholder="Nombre" maxlength="250" disabled />
                @error('nombre')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2">
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> <span style="color: red">*</span> Macroproceso </strong></label>
                <select wire:model="macroproceso_id" wire:change="setProcesoSelect()" class="form-select" disabled>
                    <option value="">--Seleccione--</option>
                    @foreach ($macroprocesos as $macroproceso)
                        <option value="{{ $macroproceso->id }}">{{ $macroproceso->nombre }}</option>
                    @endforeach
                </select>
                @error('macroproceso_id')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> Proceso </strong></label>
                <select wire:model="proceso_id" wire:change="setSubProcesoSelect()" class="form-select" disabled>
                    <option value="">--Seleccione--</option>
                    @foreach ($procesos as $proceso)
                        <option value="{{ $proceso->id }}">{{ $proceso->nombre }}</option>
                    @endforeach
                </select>
                @error('proceso_id')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2">
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> Subproceso </strong></label>
                <select wire:model="subproceso_id" wire:change="setMicroProcesoSelect()" class="form-select" disabled>
                    <option value="">--Seleccione--</option>
                    @foreach ($subprocesos as $subproceso)
                        <option value="{{ $subproceso->id }}">{{ $subproceso->nombre }}</option>
                    @endforeach
                </select>
                @error('subproceso_id')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> Microproceso </strong></label>
                <select wire:model="microproceso_id" class="form-select" disabled>
                    <option value="">--Seleccione--</option>
                    @foreach ($microprocesos as $microproceso)
                        <option value="{{ $microproceso->id }}">{{ $microproceso->nombre }}</option>
                    @endforeach
                </select>
                @error('microproceso_id')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2">
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> <span style="color: red">*</span> Tipo </strong></label>
                <select wire:model="param_tipoindicador" class="form-select" disabled>
                    <option value="">--Seleccione--</option>
                    @foreach ($tipoIndicadores as $tipo)
                        <option value="{{ $tipo->id }}">{{ $tipo->detalle }}</option>
                    @endforeach
                </select>
                @error('param_tipoindicador')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"><strong>General</strong></label>
                <select wire:model="es_general" class="form-select" disabled>
                    <option value="">--Seleccione--</option>
                    <option value="1">SI</option>
                    <option value="0">NO</option>
                </select>
                @error('es_general')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2">
            <div class="col-12 col-md-12">
                <label class="form-label"><strong>Definición</strong></label>
                <textarea
                          wire:model="definicion"
                          class="form-control"
                          placeholder="Definición" maxlength="4000" disabled></textarea>
                @error('definicion')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label"><strong>Objetivo</strong></label>
                <textarea
                          wire:model="objetivo"
                          class="form-control"
                          placeholder="Objetivo" maxlength="4000" disabled></textarea>
                @error('objetivo')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>

        @if (count($audits) > 0)
            <hr>
            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr>
                                <th>Evento</th>
                                <th>Desde URL</th>
                                <th>Desde IP</th>
                                <th>Fecha</th>
                                <th>Detalles</th>
                                <th>N° Cambios</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($audits as $audit)
                                <tr>
                                    @if ($audit->event === 'updated')
                                        <td class="text-center">
                                            <span class="badge badge-light-warning">Modificado</span>
                                            <br>
                                            Por: <strong>{{$audit->user?$audit->user->name:'--'}}</strong>
                                        </td>
                                    @elseif($audit->event === 'created')
                                        <td class="text-center">
                                            <div class="badge badge-light-info">Creado</div>
                                            <br>
                                            Por: <strong>{{$audit->user?$audit->user->name:'--'}}</strong>
                                        </td>
                                    @elseif($audit->event === 'deleted')
                                        <td class="text-center">
                                            <div class="badge badge-light-danger">Eliminado</div>
                                            <br>
                                            Por: <strong>{{$audit->user?$audit->user->name:'--'}}</strong>
                                        </td>
                                    @endif
                                    <td><span data-bs-toggle="tooltip" title="{{$audit->url}}">{{ substr($audit->url, 0, 20) }}...</span></td>
                                    <td>{{ $audit->ip_address }}</td>
                                    <td>{{ $audit->created_at }}</td>
                                    <td>
                                        <ul class="list-group">
                                            @foreach ($audit->old_values as $key => $old)
                                                <li class="list-group-item" data-bs-toggle="popover" data-bs-trigger="hover" data-bs-html="true"
                                                data-bs-original-title="{{$key}}" data-bs-placement="top"
                                                data-bs-content="<strong>Valor original: </strong><br> {{ $old }} <br> <strong>Valor nuevo: </strong><br> {{isset($audit->new_values[$key])?$audit->new_values[$key]:'--'}}">{{ $key }}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <td class="text-center">{{ count($audit->old_values) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="cerrar()" class="btn btn btn-primary me-1 mt-2">
                Cerrar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
