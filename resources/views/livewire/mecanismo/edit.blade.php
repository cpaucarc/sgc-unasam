<div class="row" x-data="{ show: @entangle('show') }">
    <div class="col-12">
        @can('create', App\Models\Objetivo::class)
        <button class="btn btn-flat-primary btn-sm mt-2" wire:click="agregarMecanismo">
            <svg xmlns="http://www.w3.org/2000/svg" class="mx-1" width="16" height="16" fill="none" viewBox="0 0 24 24" stroke="currentColor"
                 stroke-width="2">
                <path stroke-linecap="round" stroke-linejoin="round" d="M12 4v16m8-8H4" />
            </svg>
            Agregar mecanismo
        </button>
        @endcan
    </div>

    @foreach ($mecanismos as $grupo => $mecanismo)
        <div class="col-12 mt-2">
            <h5>{{ $grupo }}</h5>
        </div>
        <div class="col-12 mb-2">
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <th width="25%" class="text-center">Mecanismo</th>
                        <th width="45%" class="text-center">Objetivo</th>
                        <th width="35%" class="text-center">Evidencia</th>
                    </tr>
                </thead>
                @foreach ($mecanismo as $k => $m)
                    <tbody>
                        @if (count($m['objetivos']) > 0)
                            @foreach ($m['objetivos'] as $k2 => $o)
                                <tr>
                                    @if ($k2 === 0)
                                        <td class="text-center" rowspan="{{ count($m['objetivos']) > 0 ? count($m['objetivos']) : 1 }}">
                                            {{ $m['nombre'] }}
                                            <br>
                                            @can('create', App\Models\Objetivo::class)
                                            <button class="btn btn-flat-primary btn-sm mt-2" wire:click="agregarObjetivo({{ $m['id'] }})">
                                                Agregar objetivo
                                            </button>
                                            @endcan
                                        </td>
                                    @endif
                                    <td class="text-center">
                                        <div>
                                            <livewire:mecanismo.objetivo :objetivo_id="$o['id']" :key="'objetivo-' . $o['id']"></livewire:mecanismo.objetivo>
                                            <br>
                                            <div class="btn-group d-flex justify-content-between">
                                                @can('delete2', App\Models\Objetivo::class)
                                                <button class="btn btn-flat-danger btn-sm" wire:click="eliminarObjetivo({{ $o['id'] }})">
                                                    Eliminar objetivo
                                                </button>
                                                @endcan
                                                @can('create', App\Models\Evidencia::class)
                                                <button class="btn btn-flat-primary btn-sm" wire:click="agregarEvidencia({{ $o['id'] }})">
                                                    Agregar evidencia
                                                </button>
                                                @endcan
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <ul class="list-group">
                                            @foreach ($o['evidencias'] as $e)
                                                <li class="list-group-item">
                                                    <div class="d-flex justify-content-between">
                                                        <span>
                                                            {{ $e['nombre'] }}
                                                        </span>
                                                        <div class="btn-group">
                                                            @can('view2', App\Models\Evidencia::class)
                                                            <button class="btn btn-icon btn-sm btn-flat-primary" wire:click="verEvidencia({{ $e['id'] }})">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none"
                                                                     viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                                          d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                                                                </svg>
                                                            </button>
                                                            @endcan
                                                            @can('delete2', App\Models\Evidencia::class)
                                                            <button class="btn btn-icon btn-sm btn-flat-danger"
                                                                    wire:click="eliminarEvidencia({{ $e['id'] }})">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none"
                                                                     viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                                          d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                                </svg>
                                                            </button>
                                                            @endcan
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center">
                                    {{ $m['nombre'] }}
                                    <br>
                                    <button class="btn btn-flat-primary btn-sm mt-2" wire:click="agregarObjetivo({{ $m['id'] }})">
                                        Agregar objetivo
                                    </button>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endif
                    </tbody>
                @endforeach
            </table>
        </div>
    @endforeach
    <!-- Modal -->

    <x-jet-dialog-modal wire:model="showMecanismos" maxWidth="lg">
        <x-slot name="title">
            <h3 class="address-title text-center mb-1"><strong>Agregar mecanismo</strong></h3>
        </x-slot>
        <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12" wire:ignore>
                    <label for="mecanismo_id">Seleccionar el mecanismo</label>
                    <select name="mecanismo_id" id="mecanismo_select" class="form-control" wire:model="mecanismo_id">
                        @foreach ($mecanismos_todos as $m)
                            <option value="{{ $m->id }}">{{ $m->nombre }}</option>
                        @endforeach

                    </select>
                </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <div class="col-12 text-center">
                <x-jet-button wire:click.prevent="guardarMecanismo" class="btn btn-primary me-1 mt-2">
                    Guardar
                </x-jet-button>
                <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                    Cancelar
                </x-jet-button>
            </div>

        </x-slot>
    </x-jet-dialog-modal>

    <x-jet-dialog-modal wire:model="show" maxWidth="lg">
        <x-slot name="title">
            <h3 class="address-title text-center mb-1"><strong>Agregar evidencia</strong></h3>
        </x-slot>
        <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12">
                    <label for="nombre_evidencia">Nombre de la evidencia</label>
                    <input type="text" class="form-control" wire:model="nombre_evidencia">
                </div>
                <div class="col-12" wire:ignore>
                    <label for="documento_id">Seleccionar el documento para la evidencia</label>
                    <select name="documento_id" id="documento_select" class="form-control" wire:model="documento_id">
                        <option value="0"> -- Ninguno -- </option>
                        @foreach ($documentos as $d)
                            <option value="{{ $d->id }}">{{ $d->nombreReferencial }}</option>
                        @endforeach

                    </select>
                </div>
                <div class="col-12 d-flex justify-content-between">
                    <a target="_blank" href="/documentos" class="btn btn-flat-primary">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" viewBox="0 0 24 24" stroke="currentColor"
                             stroke-width="2">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M12 4v16m8-8H4" />
                        </svg>
                        Nuevo documento
                    </a>
                    <button class="btn btn-flat-primary" wire:click="obtenerDocumentos">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" viewBox="0 0 24 24" stroke="currentColor"
                             stroke-width="2">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                  d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15" />
                        </svg>
                        Actualizar lista
                    </button>
                </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <div class="col-12 text-center">
                <x-jet-button wire:click.prevent="guardarEvidencia" class="btn btn-primary me-1 mt-2">
                    Guardar
                </x-jet-button>
                <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                    Cancelar
                </x-jet-button>
            </div>

        </x-slot>
    </x-jet-dialog-modal>

    <x-jet-dialog-modal wire:model="showDocumento" maxWidth="lg">
        <x-slot name="title">
            <h3 class="address-title text-center mb-1"><strong>Datos de la evidencia</strong></h3>
        </x-slot>
        <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
            <div class="row gy-1 gx-2 pb-1">
                @if ($evidencia)
                    <div class="col-12">
                        <label for="nombre_evidencia"><strong>Nombre de la evidencia</strong></label><br>
                        {{ $evidencia['nombre'] }}
                    </div>
                    @if ($evidencia->documento)
                        <div class="col-12">
                            <label for="nombre_evidencia"><strong>Documento</strong></label><br>
                            {{ $evidencia->documento->nombreReferencial }}
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="nombre_evidencia"><strong>Tipo de documento</strong></label><br>
                            {{ $evidencia->documento->tipodocumento ? $evidencia->documento->tipodocumento->detalle : '-' }}
                        </div>
                        <div class="col-6 col-sm-3">
                            <label for="nombre_evidencia"><strong>¿Es interno?</strong></label><br>
                            {{ $evidencia->es_interno ? 'Sí' : 'No' }}
                        </div>
                        <div class="col-6 col-sm-3">
                            <label for="nombre_evidencia"><strong>¿Es general?</strong></label><br>
                            {{ $evidencia->general ? 'Sí' : 'No' }}
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="nombre_evidencia"><strong>Área de origen</strong></label><br>
                            {{ $evidencia->documento->area ? $evidencia->documento->area->nombre : '-' }}
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="nombre_evidencia"><strong>Tiempo de conservación</strong></label><br>
                            {{ $evidencia->documento->conservacion ? $evidencia->documento->conservacion->detalle : '-' }}
                        </div>
                    @endif
                @endif
            </div>
            @if ($evidencia && $evidencia->documento)
                <hr>
                <div class="row">
                    <div class="col-12">
                        <h5>Archivos</h5>
                    </div>
                    <div class="col-12">
                        <div class="list-group">
                            @foreach ($evidencia->documento->archivos as $archivo)
                                <a target="_blank" href="/storage/{{ $archivo->ruta }}" class="list-group-item">
                                    {{ $archivo->nombre }}
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </x-slot>
        <x-slot name="footer">
            <div class="col-12 text-center">
                <!--<x-jet-button wire:click.prevent="guardarEvidencia" class="btn btn-primary me-1 mt-2">
                    Guardar
                </x-jet-button>-->
                <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                    Cancelar
                </x-jet-button>
            </div>

        </x-slot>
    </x-jet-dialog-modal>

    <script>
        function scriptIndexDocumentos() {

            setTimeout(() => {
                selectArea = $("#documento_select");

                asignarSelect2(selectArea, '100%', 'Buscar documento');

                selectArea.on('change', function(e) {
                    $data = selectArea.select2("val");
                    @this.set('documento_id', $data);
                    console.log('documento_id: ' + $data);
                });
            }, 500);
        }
    </script>
</div>
