<!-- Modal -->
<x-jet-dialog-modal wire:model="open" maxWidth="lg">
    <x-slot name="title">
        <h3>Agregar Nueva Medición</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2">
            <div class="col-12 col-md-12">
                <label class="form-label"> <strong> Numerador </strong> </label>
                <input
                    type="text"
                    wire:model="numerador"
                    class="form-control"
                    placeholder="Numerador" maxlength="300"
                />
                @error('numerador')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label"> <strong> Denominador </strong> </label>
                <input
                    type="text"
                    wire:model="denominador"
                    class="form-control"
                    placeholder="Denominador" maxlength="300"
                />
                @error('denominador')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> <span style="color:red">*</span> Indicador </strong> </label>
                <select wire:model="indicador_id" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($indicadores as $indicador)
                    <option value="{{$indicador->id}}">{{ $indicador->nombre }}</option>
                    @endforeach
                  </select>
               @error('indicador_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> Unidad medida </strong> </label>
                <select wire:model="param_und_medida" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($unidadmedidas as $unidadmedida)
                    <option value="{{$unidadmedida->id}}">{{ $unidadmedida->detalle }}</option>
                    @endforeach
                  </select>
               @error('param_und_medida')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>


            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> Frecuencia medición </strong> </label>
                <select wire:model="param_frec_medicion" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($tipoFrecMediciones as $tipoFrecMedicion)
                    <option value="{{$tipoFrecMedicion->id}}">{{ $tipoFrecMedicion->detalle }}</option>
                    @endforeach
                  </select>
               @error('param_frec_medicion')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> Frecuencia reporte </strong> </label>
                <select wire:model="param_frec_reporte" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($tipoFrecReportes as $tipoFrecReporte)
                    <option value="{{$tipoFrecReporte->id}}">{{ $tipoFrecReporte->detalle }}</option>
                    @endforeach
                  </select>
               @error('param_frec_reporte')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> Frecuencia revisión </strong> </label>
                <select wire:model="param_frec_revision" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($tipoFrecRevisiones as $tipoFrecRevision)
                    <option value="{{$tipoFrecRevision->id}}">{{ $tipoFrecRevision->detalle }}</option>
                    @endforeach
                  </select>
               @error('param_frec_revision')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong><span style="color:red">*</span> Año </strong> </label>
                <select wire:model="anio_id" class="form-select">
                    <option value="">--Seleccione--</option>
                    @foreach($anios as $anio)
                    <option value="{{$anio->id}}">{{ $anio->anio }}</option>
                    @endforeach
                  </select>
               @error('anio_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"> <strong> Periodo </strong> </label>
                <select wire:model="param_periodo" class="form-select">
                    <option value="">--Seleccione--</option>
                    @foreach($tipoPeriodos as $tipo)
                    <option value="{{$tipo->id}}">{{ $tipo->detalle }}</option>
                    @endforeach
                  </select>
               @error('param_periodo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>


        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="store()" class="btn btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
