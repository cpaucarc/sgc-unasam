<!-- Modal -->
<x-jet-dialog-modal wire:model="open" maxWidth="lg">
    <x-slot name="title">
        <h3 class="address-title text-center mb-1"><strong>Agregar Evidencias</strong></h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">

        <div class="col-sm-12 col-md-6">
            <label class="form-label"><strong><span style="color: red">*</span> Mecanismo</strong></label>
            <select wire:model="mecanismoId" wire:change="obtenerProcesoMecanismo()" class="form-select">
                <option value="">-- Seleccione --</option>
                @foreach ($mecanismos as $mecanismo)
                    <option value="{{$mecanismo->id}}">{{$mecanismo->nombre}}</option>
                @endforeach
            </select>
            @error('mecanismoId')
            <span class="text-sm text-danger">{{$message}}</span>
            @enderror
        </div>

        {{--@if($procesoMecanismo)--}}
            <div class="col-sm-12 col-md-12">
                <label class="form-label"><strong><span style="color: red">*</span> Objetivo Procesos</strong></label>
                <select wire:model="mecanismoId" wire:change="obtenerProcesoMecanismo()" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($procesoMecanismo as $mecanismo)
                        <option value="{{$mecanismo->id}}">{{$mecanismo->id}} -- {{$mecanismo->idMecanismo}} -- {{$mecanismo->objetivo}}</option>
                    @endforeach
                </select>
                @error('mecanismoId')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        {{--@endif--}}

        <div class="col-12 col-md-12">
            <label class="form-label"><strong><span style="color: red">*</span> Nombre Referencial  </strong></label>
            <input type="number" wire:model="anio" class="form-control" placeholder="Año"
                   maxlength="4" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"/>
            @error('anio')
            <span class="text-sm text-danger">{{$message}}</span>
            @enderror
        </div>

        <div class="col-12 col-md-12">
            <label class="form-label"><strong><span style="color: red">*</span> Descripción  </strong></label>
            <input type="number" wire:model="anio" class="form-control" placeholder="Año"
                   maxlength="4" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"/>
            @error('anio')
            <span class="text-sm text-danger">{{$message}}</span>
            @enderror
        </div>

        <div class="row gy-1 gx-2 pb-1">
            {{--
            <div class="col-12 col-md-12">
                <label class="form-label"><strong><span style="color: red">*</span> Nombre del Año  </strong></label>
                <input type="text" wire:model="nombre" class="form-control" placeholder="Nombre del año"
                maxlength="200"/>
                @error('nombre')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>--}}
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="store()" class="btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
