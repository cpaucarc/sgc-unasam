<!-- Modal -->
<x-jet-dialog-modal wire:model="openVer" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3>Detalle Macroproceso</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2">
            <div class="col-12 col-md-2">
                <label class="form-label"><strong style="color: red">*</strong> Código </label>
                <input type="text" wire:model="codigo" class="form-control" placeholder="Código" maxlength="5" disabled/>
                @error('codigo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-2">
                <label class="form-label"><strong style="color: red">*</strong> Versión </label>
                <select wire:model="version_id" class="form-select" disabled>
                    <option value="">--Seleccione--</option>
                    @foreach($versiones as $version)
                    <option value="{{$version->id}}" disabled>{{ $version->numero }}</option>
                    @endforeach
                  </select>
               @error('version_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-8">
                <label class="form-label"><strong style="color: red">*</strong> Nombre </label>
                <input type="text" wire:model="nombre" class="form-control" placeholder="Nombre" maxlength="150" disabled/>
                @error('nombre')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-2">
                <label class="form-label">Orden</label>
                <input type="number" wire:model="orden" class="form-control" placeholder="Orden" min="0"
                       maxlength="2" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" disabled/>
                @error('orden')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-4">
                <label class="form-label">Tipo</label>
                <select wire:model="param_tipo" class="form-select" disabled>
                    <option value="">--Seleccione--</option>
                    @foreach($tipoMacroprocesos as $tipo)
                    <option value="{{$tipo->id}}">{{ $tipo->detalle }}</option>
                    @endforeach
                  </select>
               @error('param_tipo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label">Responsable</label>
                <select wire:model="responsable_area" class="select2 form-select" disabled>
                    <option value="">--Seleccione--</option>
                    @foreach($areas as $area)
                    <option value="{{$area->id}}">{{ $area->nombre }}</option>
                    @endforeach
                  </select>
               @error('responsable_area')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label">Descripción</label>
                <textarea wire:model="descripcion" class="form-control" placeholder="Descripción" maxlength="4000" rows="3" disabled></textarea>
                @error('descripcion')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label">Objetivo</label>
                <textarea wire:model="objetivo" class="form-control" placeholder="Objetivo" maxlength="4000" rows="3" disabled></textarea>
                @error('objetivo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label">Alcance</label>
                <textarea wire:model="alcance" class="form-control" placeholder="Alcance" maxlength="4000" rows="3" disabled></textarea>
                @error('alcance')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label">Finalidad</label>
                <textarea wire:model="finalidad" class="form-control" placeholder="Finalidad" maxlength="4000" rows="3" disabled></textarea>
                @error('finalidad')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="cerrar()" class="btn btn-primary mt-2">
                Cerrar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
