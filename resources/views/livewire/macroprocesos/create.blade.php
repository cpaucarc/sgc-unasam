<!-- Modal -->
<x-jet-dialog-modal wire:model="open" maxWidth="lg">
    <x-slot name="title">
        <h3>Agregar Nuevo Macroproceso</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2">
            <div class="col-6 col-md-2">
                <label class="form-label"><strong><strong style="color: red">*</strong> Código</strong></label>
                <input type="text" wire:model="codigo"
                    class="form-control" placeholder="Código"
                    maxlength="5"/>
                @error('codigo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-6 col-md-3">
                <label class="form-label"><strong style="color: red">*</strong> Versión</label>
                <select wire:model="version_id" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($versiones as $version)
                    <option value="{{$version->id}}">{{ $version->numero }}</option>
                    @endforeach
                  </select>
               @error('version_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-7">
                <label class="form-label"><strong style="color: red">*</strong> Nombre</label>
                <input type="text" wire:model="nombre" class="form-control" maxlength="150" placeholder="Nombre"/>
                @error('nombre')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-2">
                <label class="form-label">Orden</label>
                <input type="number" wire:model="orden" min="0" class="form-control" placeholder="Orden"
                       maxlength="2" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"/>
                @error('orden')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-4">
                <label class="form-label">Tipo</label>
                <select wire:model="param_tipo" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($tipoMacroprocesos as $tipo)
                    <option value="{{$tipo->id}}">{{ $tipo->detalle }}</option>
                    @endforeach
                  </select>
               @error('param_tipo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label">Responsable</label>
                <select wire:model="responsable_area" class="select2 form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($areas as $area)
                    <option value="{{$area->id}}">{{ $area->nombre }}</option>
                    @endforeach
                  </select>
               @error('responsable_area')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-12">
                <label class="form-label">Descripción</label>
                <textarea wire:model="descripcion" class="form-control" placeholder="Descripción" maxlength="4000"></textarea>
                @error('descripcion')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label">Objetivo</label>
                <textarea wire:model="objetivo" class="form-control" placeholder="Objetivo" maxlength="4000"></textarea>
                @error('objetivo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label">Alcance</label>
                <textarea wire:model="alcance" class="form-control" placeholder="Alcance" maxlength="4000"></textarea>
                @error('alcance')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label">Finalidad</label>
                <textarea wire:model="finalidad" class="form-control" placeholder="Finalidad" maxlength="4000"></textarea>
                @error('finalidad')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label">Tiene api?</label>
                <select wire:model="tiene_api" class="form-select">
                    <option value="1">SI</option>
                    <option value="0">NO</option>
                  </select>
               @error('tiene_api')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="store()" class="btn btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
