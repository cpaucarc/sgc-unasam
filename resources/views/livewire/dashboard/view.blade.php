<div class="card border-black">
    <div class="text-center p-1">
        <h1 class="text-primary mb-0"><strong>DASHBOARD DE INDICADORES DE GESTIÓN</strong></h1>
    </div>
    <div class="card-body pt-0">
        <div class="row gx-2 border-black rounded-2 p-2">
            <div class="col-12 col-md-6">
                <label class="form-label"><strong class="text-primary">NOMBRE DEL MACROPROCESO</strong></label>
                <div wire:ignore>
                    <select wire:model="macroproceso" id="macroproceso" class="form-select">
                        <option value="">-- Seleccione --</option>
                        @foreach ($macroprocesos as $macroproceso)
                            <option value="{{$macroproceso->id}}">{{$macroproceso->codigo}}: {{$macroproceso->nombre}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <label class="form-label"><strong class="text-primary">NOMBRE DEL PROCESO</strong></label>
                <select wire:model="proceso" id="proceso" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($procesos as $proceso)
                        <option value="{{$proceso->id}}">{{$proceso->codigo}}: {{$proceso->nombre}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-12 col-md-2 text-end">
                <label class="form-label"><strong class="text-primary">&nbsp;</strong></label><br>
               <button class="btn btn-primary" id="btn_limpiar">Limpiar</button>
            </div>
        </div>
        <div class="row gx-2 rounded-2 mt-2">
            <div class="col-12 col-md-4 px-0" style="height: 100%">
                <div class="border-black rounded-2 p-2" style="{{$scroll}}">
                    @if($mensaje_indicador)
                        <label class="form-label">
                            <strong class="text-primary">NOMBRE DEL INDICADOR ({{count($indicadors)}})</strong>
                        </label>
                        <div class="row mb-1">
                            <div>
                                <div class="input-group">
                                    <input type="text" wire:model="search" placeholder="Buscar..." class="form-control">
                                    <div class="input-group-append">
                                        <button type="button" wire:click="buscarIndicador()" class="btn btn-primary"> Buscar </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list-group">
                            @foreach ($indicadors as $indicador)
                                <button wire:click="activarIndicador({{$indicador->id}},{{$loop->index}})" class="list-group-item list-group-item-action" id="list-group-item{{$loop->index}}">
                                    {{$indicador->codigo}} - {{$indicador->nombre}}
                                </button>
                            @endforeach
                        </div>
                    @else
                        <div>
                            <span class="text-danger">SELECCIONE EL MACROPROCESO Ó PROCESO</span>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-12 col-md-8">
                <div class="row gx-2">
                    <div class="col-12 col-md-6">
                        <div class="border-black rounded-2 px-2 pt-2">
                            @if($isBloque)
                                <div class="row gx-2 pb-75">
                                    <div class="col-12 col-md-6 text-left">
                                        <label class="form-label"><strong class="text-primary">AÑO: </strong></label>
                                        <select wire:model="anio" wire:change="verAnio()" class="form-select">
                                            <option value="">--Seleccione--</option>
                                            @foreach ($anios as $anio)
                                                <option value="{{$anio->id}}">{{$anio->anio}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if($isPeriodo)
                                        <div class="col-12 col-md-6 text-left">
                                            <label class="form-label"><strong class="text-primary">PERIODO: </strong></label>
                                            <select wire:model="periodo" wire:change="verPeriodo()" class="form-select">
                                                <option value="NULL">--Seleccione--</option>
                                                @foreach ($periodos as $periodo)
                                                    <option value="{{$periodo->id}}">{{$periodo->detalle}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                </div>
                                <div class="row gx-2 pb-75">
                                    <div class="col-12 col-md-12 pb-75">
                                        <label class="form-label"><strong class="text-primary">RESPONSABLE DEL CÁLCULO:</strong></label>
                                        <label class="form-control">{{$calculo}}</label>
                                    </div>
                                    <div class="col-12 col-md-12">
                                        <label class="form-label"><strong class="text-primary">RESPONSABLE DEL ANÁLISIS Y TOMA DE DECISIONES:</strong></label>
                                        <label class="form-control">{{$analisis}}</label>
                                    </div>
                                </div>
                                <div class="row gx-2 pb-75">
                                    <div class="col-12 col-md-6">
                                        <label class="form-label"><strong class="text-primary">TIPO DE INDICADOR</strong></label>
                                        <label class="form-control">{{$tipoindicador}}</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label class="form-label"><strong class="text-primary">FRECUENCIA DE MEDICIÓN</strong></label>
                                        <label class="form-control">{{$medicion}}</label>
                                    </div>
                                </div>
                                <div class="row gx-2 pb-75">
                                    <div class="col-12 col-md-6">
                                        <label class="form-label"><strong class="text-primary">UNIDAD DEL INDICADOR</strong></label>
                                        <label class="form-control">{{$unidad}}</label>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label class="form-label"><strong class="text-primary">META DEL INDICADOR</strong></label>
                                        <label class="form-control">{{$meta}}</label>
                                    </div>
                                </div>
                                <div class="row gx-2 pb-75">
                                    <div class="col-12 col-md-6">
                                        <label class="form-label"><strong class="text-primary">VERSIÓN:</strong></label>
                                        <select wire:model="version" name="version" id="version" class="form-select" wire:change="cambiarVersion">
                                            @foreach($versiones as $v)
                                                <option value="{{$v->id}}">{{$v->version->numero}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <label class="form-label"><strong class="text-primary">VALOR DE INDICADOR EN EL PERIODO ACTUAL: </strong></label>
                                        <label class="form-control"><strong>{{$valor}}</strong></label>
                                    </div>
                                </div>
                            @else
                                <div class="pb-2">
                                    <span class="text-danger">NO HAY INFORMACIÓN QUÉ MOSTRAR.</span>
                                </div>
                            @endif

                        </div>
                    </div>
                    <div class="col-12 col-md-6 border-black rounded-2 pt-2">
                        @if($isBloque)
                            <div class="col-12 text-center">
                                <label class="form-label"><strong class="text-primary">CUMPLIMIENTO DE LA META</strong></label>
                            </div>
                            <div class="col-12">
                                <div class="d-flex justify-content-center" style="height: 250px;">
                                    <div id="myChartGauge" style="width: 600px; height:400px;"></div>
                                </div>
                            </div>
                            @if(count($escalas)>0)
                                <div class="col-12">
                                    <div id="escalas">
                                        <table class="d-flex justify-content-center">
                                            @foreach ($escalas as $escala)
                                                @php
                                                    $obtener_color = json_decode($escala->descripcion);
                                                @endphp
                                                <tr>
                                                    <td>
                                                        <div class="d-flex">
                                                            <div class="colors-container rounded me-75" style="width: 20px; background-color: {{ $obtener_color?$obtener_color->{'color'}:'' }};">
                                                                <span style="color: {{ $obtener_color?$obtener_color->{'color'}:'' }};">-</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="pe-1">
                                                        <span>{{$escala->detalle}}</span>
                                                    </td>
                                                    <td>
                                                        @if($escala->param_likert == '14001')
                                                             <span> [ {{$escala->valor_ini}} - {{$escala->valor_fin}} ]</span>
                                                        @else
                                                            <span>< {{$escala->valor_ini}} - {{$escala->valor_fin}} ]</span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            @endif
                        @else
                            <div class="pb-2">
                                <span class="text-danger">NO HAY GRÁFICO QUE MOSTRAR SELECCIONE LO NECESARIO.</span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row gx-2">
            <div class="col-12 border-black rounded-2 mt-1 pt-1 text-center">
                <h2><strong class="text-primary">EVOLUCIÓN DEL INDICADOR</strong></h2>
                @if($indicador_nombre)
                    <h3><strong class="text-primary">{{$indicador_nombre}}</strong></h3>
                @endif
                <div class="chartCard d-flex justify-content-center">
                    @if($isBloque)
                        <div class="chart-container" style="width: 600px; height:400px;">
                            <div class="wrapper">
                                <canvas id="myChart"></canvas>
                            </div>
                        </div>
                    @else
                        <div class="pb-1">
                            <span class="text-danger">NO HAY GRÁFICO QUE MOSTRAR SELECCIONE LO NECESARIO.</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <script>


        function scriptDashboard() {

            let myChart;

            Livewire.on('listaGroup', function($item){
                $list = $('#list-group-item'+$item).addClass('active');
            });

            selectMacroproceso = $('#macroproceso');
            asignarSelect2(selectMacroproceso,'100%','Buscar macroproceso');
            selectMacroproceso.on('change', function (e) {
                $data = selectMacroproceso.select2("val");
                @this.set('macroproceso', $data);
                @this.set('indicacion', 1);
                $search = '';
                @this.set('search', $search);
                Livewire.emitTo('dashboard','obtenerProcesos');
                console.log($data);
            });
            selectProceso = $('#proceso');

            asignarSelect2(selectProceso,'100%','Buscar proceso');

            Livewire.on('mostrarProcesos', function(){
                asignarSelect2(selectProceso,'100%','Buscar proceso');
            });

            selectProceso.on('change', function (e) {
                $data = selectProceso.select2("val");
                @this.set('proceso', $data);
                @this.set('indicacion', 2);
                $search = '';
                @this.set('search', $search);
                Livewire.emitTo('dashboard','obtenerIndicadores');
                console.log($data);
            });

            Livewire.on('selectProceso', function(){
                asignarSelect2(selectProceso,'100%','Buscar proceso');
            });

            Livewire.on('verVelocimetro', function($unidad, $color,$min,$maximo,$valor, $serie, $divisor, $descendente){
                // Initialize the echarts instance based on the prepared dom
                console.log($color);
                console.log('minimo: '+$min);
                console.log('maximo: '+$maximo);
                console.log('valor: '+$valor);
                console.log('serie: '+$serie);

                $max = 0;
                $split = 0;
                if ($maximo <= 100) {
                    $max = 1;
                    $split = 100;
                }
                if ($maximo > 100) {
                    $max = 1.2;
                    $split = $valor;
                }
                $valor_unidad = '';
                if ($unidad == 'PORCENTAJE') {
                    $valor_unidad = '%';
                }
                $split = $divisor === 0?1:(($maximo - $min)/$divisor);
                var myChartGauge = echarts.init(document.getElementById('myChartGauge'));
                // Specify the configuration items and data for the chart

                    $colores = [ $color[0], $color[1], $color[2], $color[3], $color[4], [1,'#8d8d8d'] ];

                console.log('split: '+$split);
                console.log('color: '+$color);
                option = {
                    series: [{
                        type: 'gauge',
                        startAngle: 180,
                        endAngle: 0,
                        min: $descendente?$maximo:$min,
                        max: $descendente?$min:$maximo,
                        splitNumber: $split,
                        axisLine: {
                            lineStyle: {
                                width: 30,
                                color:  $colores
                            }
                        },
                        pointer: {
                            icon: 'path://M2090.36389,615.30999 L2090.36389,615.30999 C2091.48372,615.30999 2092.40383,616.194028 2092.44859,617.312956 L2096.90698,728.755929 C2097.05155,732.369577 2094.2393,735.416212 2090.62566,735.56078 C2090.53845,735.564269 2090.45117,735.566014 2090.36389,735.566014 L2090.36389,735.566014 C2086.74736,735.566014 2083.81557,732.63423 2083.81557,729.017692 C2083.81557,728.930412 2083.81732,728.84314 2083.82081,728.755929 L2088.2792,617.312956 C2088.32396,616.194028 2089.24407,615.30999 2090.36389,615.30999 Z',
                            length: '45%',
                            width: 10,
                            offsetCenter: [0, '-15%'],
                            itemStyle: {
                                    color: 'auto'
                            }
                        },
                        axisTick: {
                            length: 10,
                            lineStyle: {
                                    color: 'auto',
                                    width: 2
                            }
                        },
                        // splitLine: {
                        //     length: 20,
                        //     lineStyle: {
                        //             color: 'auto',
                        //             width: 5
                        //     }
                        // },
                        axisLabel: {
                            color: '#464646',
                            fontSize: 10,
                            distance: -48,
                            formatter: function (value) {
                             //   console.log(value);
                                for ($i = 0; $i < $serie.length; $i++) {
                                    if (value == $serie[$i]) {
                                        return $serie[$i];
                                    }
                                }
                                // if (value === 20) {
                                //         return '20';
                                // } else if (value === 62.5) {
                                //         return 'B';
                                // } else if (value === 37.5) {
                                //         return 'C';
                                // } else if (value === 70) {
                                //         return '70';
                                // }
                                // return '';
                            }
                        },
                        title: {
                                offsetCenter: [0, '-20%'],
                                fontSize: 20
                        },
                        detail: {
                                fontSize: 20,
                                offsetCenter: [0, '0%'],
                                valueAnimation: true,
                                formatter: function (value) {
                                        return Math.round(value) + $valor_unidad;
                                },
                                color: 'auto'
                        },
                        data: [
                            {
                                value: $valor
                            }
                        ]
                    }]
                };
                // Display the chart using the configuration items and data just specified.
                myChartGauge.setOption(option);

            });

            Livewire.on('verGraficoBarras', function($labels,$metas,$resultados) {
                const data = {
                    labels: $labels.reverse(),
                    datasets: [
                        {
                            label: 'Resultado',
                            data: $resultados.reverse(),
                            backgroundColor: 'rgba(54, 162, 235, 0.2)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            borderWidth: 1,
                            datalabels:{
                                color: 'rgba(0, 50, 255 )',
                                labels: {
                                    title: {
                                        font: {
                                            weight: 'bold'
                                        }
                                    }
                                }
                            }
                        },
                        {
                            label: 'Meta',
                            data: $metas.reverse(),
                            backgroundColor: 'rgba(255, 159, 64, 0.2)',
                            borderColor: 'rgba(255, 159, 64, 1)',
                            borderWidth: 2,
                            datalabels:{
                                color: 'black',
                                labels: {
                                    title: {
                                        font: {
                                            weight: 'bold'
                                        }
                                    }
                                },
                                anchor: 'end',
                                align: 'top',
                                offset: 1
                            },
                            type: "line"

                        }
                    ]
                };

                const legendMargin = {
                    id: 'legendMargin',
                    beforeInit(chart, legend, options){
                        const fitValue = chart.legend.fit;
                        chart.legend.fit  = function fit(){
                            fitValue.bind(chart.legend)();
                            return this.height +=20;
                        }
                    }
                };

                // config
                const config = {
                    type: 'bar',
                    data,
                    plugins: [ChartDataLabels,legendMargin]
                };

                if (myChart) {
                    myChart.destroy();
                }

                // render init block
                myChart = new Chart(
                    document.getElementById('myChart'),
                    config
                );
            });

        }
    </script>
</div>
