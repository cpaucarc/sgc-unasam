<!-- Modal -->
<x-jet-dialog-modal wire:model="updateMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3 class="address-title text-center mb-1"><strong>Actualizar Evaluación</strong></h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-12">
                <label class="form-label"><strong><span style="color:red">*</span> Parámetro Likert </strong></label>
                <select wire:model="param_likert" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($parametro_likerts as $parametro_likert)
                        <option value="{{$parametro_likert->id}}">{{$parametro_likert->detalle}}</option>
                    @endforeach
                </select>
                @error('param_likert')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-12">
                <label class="form-label"><strong><span style="color:red">*</span> Propuesta de mejora </strong></label>
                <textarea type="tex" wire:model="propuesta_mejora" class="form-control" placeholder="Valor Inicial" maxlength="4000"></textarea>
                @error('propuesta_mejora')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-6">
                <label class="form-label"><strong><span style="color:red">*</span> Fecha de propuesta </strong></label>
                <input type="text" wire:model="fecha_propuesta" class="form-control fechaflatpickr" placeholder="dd-mm-aaa"/>
                @error('fecha_propuesta')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-6">
                <label class="form-label"><strong><span style="color:red">*</span> Fecha de cumplimiento </strong></label>
                <input type="text" wire:model="fecha_cumplimiento" class="form-control fechaflatpickr" placeholder="dd-mm-aaa"/>
                @error('fecha_cumplimiento')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-6">
                <label class="form-label"><strong><span style="color:red">*</span> Resultados</strong></label>
                <select wire:model="resultado_id" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($parametro_resultados as $parametro_resultado)
                        <option value="{{$parametro_resultado->id}}">{{$parametro_resultado->param_periodo}}</option>
                    @endforeach
                </select>
                @error('resultado_id')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="update()" class="btn btn-primary me-1 mt-2">
                Actualizar
            </x-jet-button>
            <x-jet-button wire:click.prevent="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
