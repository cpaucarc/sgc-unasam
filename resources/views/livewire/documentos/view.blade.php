<div>
    <div class="card">
        <div class="card-datatable table-responsive">
            <div class="dataTables_wrapper dt-bootstrap5 no-footer">
                <div class="d-flex justify-content-between align-items-center header-actions mx-2 row mt-75">
                    <div class="col-sm-12 col-lg-4 d-flex justify-content-center justify-content-lg-start">
                        <div class="dataTables_length">
                            <label>Mostrar
                                <select wire:model="cantidad" class="form-select">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="{{$documentos->total()}}">Total</option>
                                </select> entradas
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-8">
                        <div class="dt-action-buttons d-flex align-items-center justify-content-lg-end justify-content-center flex-md-nowrap flex-wrap">
                            <div class="me-1">
                                <div id="DataTables_Table_0_filter" class="dataTables_filter">
                                    <label>
                                        Buscar:<input type="search" class="form-control" wire:model="search" placeholder="">
                                    </label>
                                </div>
                            </div>
                            <div class="dt-buttons btn-group flex-wrap">
                                @can('create', App\Models\Documento::class)
                                <x-jet-button wire:click="$set('open',true)" class="btn add-new btn-primary mt-50">
                                    <span>Agregar Documento</span>
                                </x-jet-button>
                                @endcan
                            </div>
                        </div>
                    </div>
                    <div>
                        @include('livewire.documentos.create')
                        @include('livewire.documentos.update')
                        @include('livewire.documentos.show')
                    </div>
                </div>

                @can('viewAny', App\Models\Documento::class)
                <div class="table-responsive px-75">
                    @php
                        $count = 0;
                    @endphp
                    @if($documentos->currentPage() > 1)
                        @php
                            $count = ($documentos->currentPage()-1)*10;
                        @endphp
                    @endif
                    
                    @if($documentos->count())
                        <table class="table table-bordered" role="grid">
                            <thead class="table-light">
                                <tr role="row">
                                    <th class="border-black text-center px-25">Nro</th>
                                    <th class="border-black text-center sorting cursor-pointer" wire:click="ordenar('nombreReferencial')">Nombre Referencial</th>
                                    <th class="border-black text-center">Tipo Documento</th>
                                    <th class="border-black text-center">Es Interno</th>
                                    <th class="border-black text-center">Area</th>
                                    <th class="border-black text-center">Estado</th>
                                    <th class="border-black text-center" width="5%" aria-label="Actions">Acciones
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="border-black">
                                @foreach ($documentos as $documento)
                                    <tr style="border-bottom-color: #b8c2cc;">
                                        <td class="text-center px-25 py-25" rowspan="2">{{ $count + $loop->iteration }}</td>
                                        <td class="px-50 py-25" style="border-right-color: #b8c2cc;">
                                            <strong> {{$documento->nombreReferencial}} </strong>
                                        </td>
                                        @if($documento->param_tipodocumento)
                                            <td class="px-50 py-25" style="border-right-color: #b8c2cc;">{{$documento->tipodocumento->detalle}}</td>
                                        @else
                                            <td class="px-50 py-25" style="border-right-color: #b8c2cc;">-</td>
                                        @endif
                                        <td class="text-center py-25" style="border-right-color: #b8c2cc;">
                                            @if($documento->es_interno==1)
                                                SI
                                            @else 
                                                NO
                                            @endif
                                        </td>
                                        @if($documento->area)
                                            <td class="px-50 py-25" style="border-right-color: #b8c2cc;">{{$documento->area->nombre}}</td>
                                        @else
                                            <td class="px-50 py-25" style="border-right-color: #b8c2cc;">-</td>
                                        @endif
                                        <td class="text-center px-50 py-25" style="border-right-color: #b8c2cc;">
                                            @if($documento->activo == 1)
                                                <span class="badge rounded-pill badge-light-success" text-capitalized="">Activo</span>
                                            @else
                                                <span class="badge rounded-pill badge-light-secondary" text-capitalized="">Inactivo</span>
                                            @endif
                                        </td>
                                        <td class="px-50 py-25">
                                            <div class="d-flex justify-content-between">
                                                @can('view', $documento)
                                                <div class="d-flex align-items-center"> 
                                                    <button wire:click="show({{$documento->id}})" class="btn btn-icon rounded-circle btn-flat-primary waves-effect">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye">
                                                            <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                            <circle cx="12" cy="12" r="3"></circle>
                                                        </svg>
                                                    </button>  
                                                </div>
                                                @endcan
                                                @can('update', $documento)
                                                <div class="d-flex align-items-center"> 
                                                    <button wire:click="edit({{$documento->id}})" class="btn btn-icon rounded-circle btn-flat-success waves-effect">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit font-medium-2 text-body">
                                                            <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                                            <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                                        </svg>
                                                    </button>
                                                </div>
                                                @endcan
                                                @can('delete', $documento)
                                                <div class="d-flex align-items-center">
                                                    <button wire:click="$emit('eliminarDocumento',{{$documento->id}})" class="btn btn-icon rounded-circle btn-flat-danger waves-effect">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
                                                            <polyline points="3 6 5 6 21 6"></polyline>
                                                            <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                                        </svg>
                                                    </button>   
                                                </div>
                                                @endcan 
                                            </div>  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="m-0 p-0" colspan="6">
                                            <div class="card mb-0">
                                                <div class="card-header px-75 p-75">
                                                    <a data-action="collapse" wire:click="viewArchivos({{$documento->id}})" class="text-primary">Ver Archivos <i data-feather="chevron-down"></i></a>
                                                </div>
                                                <div id="card-{{$documento->id}}" class="card-content collapse">
                                                    <div class="card-body">
                                                        @if($esVisible)
                                                            @livewire('documento-archivos', ['documento_id' => $documento->id, 'item' => $loop->index], key($documento->id.time()))
                                                        @endif
                                                        {{-- @livewire('documento-archivos',['documento_id'=>$documento->id]) --}}
                                                        {{-- <livewire:documento-archivos :documento_id="$documento->id" :key="$documento->id"/> --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        No existe ningún registro.                        
                    @endif
                </div>
                <div class="d-flex justify-content-between mx-2 row mb-1">
                    <div class="col-sm-12 col-md-6">
                        <div class="pt-1" aria-live="polite">Mostrar {{$documentos->firstItem()}}
                            a {{$documentos->lastItem()}} de {{$documentos->total()}} entradas
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div id="link-documentos" class="pt-1">
                            {{$documentos->links()}}
                        </div>
                    </div>
                </div>
                @endcan
            </div>
        </div>
    </div>
    <script>
        function scriptIndexDocumentos() {
            selectArea = $('.selectArea');
            selectTipoDocumento = $('.selectTipoDocumento');

            selectAreaEdit = $('.selectArea-edit');
            selectTipoDocumentoEdit = $('.selectTipoDocumento-edit');

            Livewire.on('alertDocumento', function(datos){
				nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
			}); 
            Livewire.on('verViewArchivo', function(dato){
                if ($('#card-'+dato).is(':visible')) {
                    $('#card-'+dato).hide();
                } else {
                    $('#card-'+dato).show();
                }
			}); 
            asignarSelect2(selectArea,'100%','Buscar área');

			selectArea.on('change', function (e) {
                $data = selectArea.select2("val");
                @this.set('area_id', $data);
                console.log('area_id: '+$data);
            });    

            asignarSelect2(selectTipoDocumento,'100%','Buscar área');
            
			selectTipoDocumento.on('change', function (e) {
                $data = selectTipoDocumento.select2("val");
                @this.set('categoria', $data);
                console.log('categoria: '+$data);
            });    
            Livewire.on('eliminarDocumento', documentoId=>{
                Swal.fire({
                    title: '¿Está seguro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'error',
                    showCancelButton: true,
                    confirmButtonText: '¡Sí, elimínalo!',
                    cancelButtonText: 'Cancelar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emitTo('documentos','destroy',documentoId)
                        Swal.fire({
                            icon: 'success',
                            title: '¡Eliminado!',
                            text: 'El Documento se ha sido eliminado.',
                            confirmButtonText: "Aceptar",
                        });
                    }
                })
            });

            Livewire.on('resetearDatos', function(){
                selectArea.val('NULL');
                selectArea.trigger('change');                
                selectTipoDocumento.val('NULL');
                selectTipoDocumento.trigger('change');                
			}); 

            asignarSelect2(selectAreaEdit,'100%','Buscar área');
            selectAreaEdit.on('change', function (e) {
                $data = selectAreaEdit.select2("val");
                @this.set('area_id', $data);
                console.log('area_id: '+$data);
            });  

            asignarSelect2(selectTipoDocumentoEdit,'100%','Buscar documento');
            selectTipoDocumentoEdit.on('change', function (e) {
                $categoria = selectTipoDocumentoEdit.select2("val");
                @this.set('categoria', $categoria);
                console.log('categoria: '+$categoria);
            });  

            Livewire.on('asignarDatos', function(area_id,categoria){
                console.log("entra? -> area_id" + area_id + "-> "+categoria);
                selectAreaEdit.val(area_id).change();
                selectAreaEdit.trigger('change');                
                selectTipoDocumentoEdit.val(categoria).change();
                selectTipoDocumentoEdit.trigger('change');                
			});  

        }

		function scriptArchivos () {
			selectDocumento = $('#documento');
			selectDocumentoEdit = $('#documento_edit');
			selectVersion = $('#version');
			selectAnio = $('#anio');
			selectPeriodo = $('#periodo');
			selectPeriodoEdit = $('#periodo_edit');
			
			Livewire.on('alertArchivos', function(datos){
				nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
			});

			Livewire.on('EliminarArchivo', archivoID=>{
				Swal.fire({
					title: '¿Está seguro?',
					text: "¡No podrás revertir esto!",
					icon: 'error',
					showCancelButton: true,
					confirmButtonText: '¡Sí, elimínalo!',
					cancelButtonText: 'Cancelar'
				}).then((result) => {
					if (result.isConfirmed) {
						Livewire.emitTo('documento-archivos','destroyArchivo',archivoID)            
					}
				})
			});				
		}
    </script>
</div>