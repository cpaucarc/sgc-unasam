<!-- Modal -->
<x-jet-dialog-modal wire:model="showMode" id="showDocumento" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3 class="address-title text-center mb-1"><strong>Datos del Documento</strong></h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div id="form-documentos">
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12">
                    <label class="form-label"><strong>Nombre del documento</strong></label>
                    <div>{{$nombreReferencial}}</div>                
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12">
                    <label class="form-label"><strong>Descripción</strong></label>
                    <div>{{$descripcion}}</div>
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-6">
                    <label class="form-label"><strong>Tipo de Documento</strong></label>
                    <div>{{$tipodocumento}}</div>
                </div>
                <div class="col-12 col-md-3">
                    <label class="form-label"><strong>¿Es interno?</strong></label>
                    <div>
                        @if($es_interno == 1)
                            SI
                        @else
                            NO
                        @endif
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <label class="form-label"><strong>¿Es general?</strong></label>
                    <div>
                        @if($general == 1)
                            SI
                        @else
                            NO
                        @endif
                    </div>
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-9">
                    <label class="form-label"><strong>Área de origen</strong></label>
                    <div>
                        {{$nombre_area}}
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <label class="form-label"><strong>Tiempo de Conservación</strong></label>
                    <div>
                        {{$nombre_conservacion}}
                    </div>
                </div>
            </div>       
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>
    </x-slot>
</x-jet-dialog-modal>
