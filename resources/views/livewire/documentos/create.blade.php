<!-- Modal -->
<x-jet-dialog-modal wire:model="open" id="createDocumento" maxWidth="lg" data-backdrop="static" data-keyboard="false">
    <x-slot name="title">
        <h3 class="address-title text-center mb-1" id="addNewPersonaTitulo"><strong>Agregar Documento</strong></h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div id="form-documentos">
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>Nombre del documento</strong></label>
                    <input type="text" wire:model.defer="nombreReferencial" class="form-control" placeholder="Nombre del documento"/>
                    @error('nombreReferencial')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12">
                    <label class="form-label"><strong>Descripción</strong></label>
                    <textarea 
                        wire:model.defer="descripcion"
                        class="form-control"
                        placeholder="Descripción del documento"
                        rows="2"
                    ></textarea>
                    @error('descripcion')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-6">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>Tipo de Documento</strong></label>
                    <div wire:ignore>
                        <select wire:model.defer="categoria" class="form-select selectTipoDocumento">
                            <option value="NULL">-- Seleccione --</option>
                            @foreach ($categorias as $categoria)
                                <option value="{{$categoria->id}}">{{$categoria->detalle}}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('categoria')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-12 col-md-3">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>¿Es interno?</strong></label>
                    <select wire:model.defer="es_interno" class="form-select">
                        <option value="NULL">-- Seleccione --</option>
                        <option value="1">SI</option>
                        <option value="0">NO</option>
                    </select>
                    @error('es_interno')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-12 col-md-3">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>¿Es general?</strong></label>
                    <select wire:model.defer="general" class="form-select">
                        <option value="NULL">-- Seleccione --</option>
                        <option value="1">SI</option>
                        <option value="0">NO</option>
                    </select>
                    @error('general')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-9">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>Área de origen</strong></label>
                    <div wire:ignore>
                        <select wire:model.defer="area_id"  class="form-select selectArea">
                            <option value="NULL">-- Seleccione --</option>
                            @foreach ($areas as $area)
                                <option value="{{$area->id}}">{{$area->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('area_id')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-12 col-md-3">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>Tiempo de Conservación</strong></label>
                    <div wire:ignore>
                        <select wire:model.defer="conservacion" class="form-select">
                            <option value="NULL">-- Seleccione --</option>
                            @foreach ($conservacions as $conservacion)
                                <option value="{{$conservacion->id}}">{{$conservacion->detalle}}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('conservacion')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-12">
                    <label class="form-label"><strong>Descripción de cómo se nombrá los archivos para este documento</strong></label>
                    <input type="text" wire:model.defer="placeholder" class="form-control" placeholder="Ejemplo: nombre de la carrera y/o área y el año y/o semestre"/>
                    @error('placeholder')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>          
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="store()" class="btn btn-primary me-2 mt-2">
                Guardar
            </x-jet-button>
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>
    </x-slot>
</x-jet-dialog-modal>
