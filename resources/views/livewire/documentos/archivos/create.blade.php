<x-jet-dialog-modal id="modalCrearArchivo-{{$item}}" wire:model="openArchivo" maxWidth="lg">
    <x-slot name="title">
        <h3>Agregar Nuevo Archivo</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div id="form-archivoSubir">
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-9">
                    <label class="form-label"><strong>Documento</strong></label>
                    <div wire:ignore>
                        <select wire:model.defer="documento_id" disabled id="documento_create" class="form-control form-select">
                            <option value="">-- Seleccione --</option>
                            @foreach ($documentos as $documento)
                                @if($documento->general == 1)
                                    <option value="{{$documento->id}}">{{$documento->nombreReferencial}}</option>
                                @else
                                    <option value="{{$documento->id}}">{{$documento->nombreReferencial}} - {{$documento->area->abreviatura}}</option>                                
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div> 
                <div class="col-12 col-md-3">
                    <label class="form-label"><strong>Tiempo de Conservación</strong></label>
                    <strong class="form-control text-primary text-center">{{$conservacion}}</strong>
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-3">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>Año</strong></label>
                        <select wire:model.defer="anio_id" class="form-control form-select">
                            <option value="NULL">-- Seleccione --</option>
                            @foreach ($anios as $anio)
                                <option value="{{$anio->id}}">{{$anio->anio}}</option>
                            @endforeach
                        </select>
                    @error('anio_id')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div> 
                <div class="col-12 col-md-3">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>Periodo</strong></label>
                        <select wire:model.defer="param_periodo" {{ $isDisabled }} class="form-control form-select">
                            <option value="NULL">-- Seleccione --</option>
                            @foreach ($periodos as $periodo)
                                <option value="{{$periodo->id}}">{{$periodo->detalle}}</option>
                            @endforeach
                        </select>
                    @error('param_periodo')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div> 
                <div class="col-12 col-md-3 {{$ocultar1}}">
                    <label class="form-label"><strong>Inicio de Vigencia</strong></label>
                    <input wire:model.defer="fechaIniVigencia" type="date" placeholder="dd-mm-aaaa" class="form-control fechaVigencia">
                    @error('fechaIniVigencia')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div> 
                <div class="col-12 col-md-3 {{$ocultar1}}">
                    <label class="form-label"><strong>Fin de Vigencia</strong></label>
                    <input wire:model.defer="fechaFinVigencia" type="date" placeholder="dd-mm-aaaa" class="form-control fechaVigencia">
                    @error('fechaFinVigencia')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div> 
                <div class="col-12 col-md-3 {{$ocultar3}}">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>Semanas</strong></label>    
                    <select wire:model.defer="semana" class="form-control form-select">
                        <option value="NULL">-- Seleccione --</option>
                        @foreach ($semanas as $semana)
                            <option value="{{$semana->id}}">{{$semana->detalle}}</option>
                        @endforeach
                    </select>    
                    @error('semana')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror                
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-3 {{$ocultar2}}">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>Inicio de Vigencia</strong></label>
                    <input wire:model.defer="fechaIniVigencia" type="date" placeholder="dd-mm-aaaa" class="form-control fechaVigencia">
                    @error('fechaIniVigencia')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div> 
                <div class="col-12 col-md-3 {{$ocultar2}}">
                    <label class="form-label"><strong>Fin de Vigencia</strong></label>
                    <input wire:model.defer="fechaFinVigencia" type="date" placeholder="dd-mm-aaaa" class="form-control fechaVigencia">
                    @error('fechaFinVigencia')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>     
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-12">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>Descripción</strong></label>
                    <textarea 
                        wire:model.defer="descripcion"
                        class="form-control"
                        placeholder="{{$placeholder}}"
                        rows="2"
                    ></textarea>
                    @error('descripcion')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div> 
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-12">
                    <label class="form-label"><strong class="text-danger">* </strong><strong>Seleccione el Archivo</strong></label>
                    <input type="file" wire:model="file_upload" class="form-control">
                    @error('file_upload')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div> 
            </div>  
            <div class="row gy-1 gx-2 pb-1">
                <div wire:loading wire:target="file_upload" class="alert alert-info" role="alert">
                    <h4 class="alert-heading">El archivo se está cargando...</h4>
                    <div class="alert-body">
                        Espere un momento hasta que la archivo se haya guardado.
                    </div>  
                </div>
            </div> 
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="store()" class="btn btn-primary me-2 mt-2">
                Guardar
            </x-jet-button>
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>        
    </x-slot>
</x-jet-dialog-modal>