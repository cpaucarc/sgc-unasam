<x-jet-dialog-modal id="modalShowArchivo{{$documento_id}}" wire:model="showMode" maxWidth="lg" data-backdrop="static">
    <x-slot name="title">
        <h3>Datos del Archivo</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-6">
                    <label class="form-label"><h5>Nombre del Documento</h5></label>
                    <div>
                        {{ $documento }}
                    </div>
                </div> 
                <div class="col-12 col-md-6">
                    <label class="form-label"><h5>Responsable</h5></label>
                    <div>
                        {{ $responsable }}
                    </div>
                </div> 
            </div>
            <div class="row gy-1 gx-2 pb-1">
                {{-- <div class="col-12 col-md-2">
                    <label class="form-label"><h5>Versión</h5></label>
                    <div class="text-center">
                        {{ $version }}
                    </div>
                </div>  --}}
                <div class="col-12 col-md-2">
                    <label class="form-label"><h5>Año</h5></label>
                    <div>
                        {{ $anio }}
                    </div>
                </div> 
                <div class="col-12 col-md-2">
                    <label class="form-label"><h5>Periodo</h5></label>
                    <div>
                        @if($periodo)
                            {{ $periodo }}
                        @else
                            -
                        @endif
                    </div>
                </div> 
                
                @if($semana)
                    <div class="col-12 col-md-2">
                        <label class="form-label"><h5>Semana</h5></label>
                        <div>
                            {{ $semana }}
                        </div>
                    </div>                     
                @endif
                <div class="col-12 col-md-3 text-center">
                    <label class="form-label"><h5>Inicio de Vigencia</h5></label>
                    <div>
                        {{ $fechaIniVigencia }}
                    </div>
                </div> 
                <div class="col-12 col-md-3 text-center">
                    <label class="form-label"><h5>Fin de Vigencia</h5></label>
                    <div>
                        {{ $fechaFinVigencia }}
                    </div>
                </div> 
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-10">
                    <label class="form-label"><h5>Nombre del Archivo</h5></label>
                    <div>
                        <p>
                            {{ $nom_archivo }}
                        </p>
                    </div>
                </div> 
                <div class="col-12 col-md-2">
                    <label class="form-label"><h5>Estado</h5></label>
                    <div>
                        @if($activo == 1)
                            <span class="badge rounded-pill badge-light-success" text-capitalized="">Activo</span>
                        @else
                            <span class="badge rounded-pill badge-light-secondary" text-capitalized="">Inactivo</span>
                        @endif
                    </div>
                </div> 
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12">
                    <label class="form-label"><h5>Descripción del Archivo</h5></label>
                    <div>
                        <p>
                            {{ $descripcion }}
                        </p>
                    </div>
                </div> 
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click="descargar({{$selected_archivo_id}})" class="btn btn-primary me-2 mt-2">
                Descargar Archivo
            </x-jet-button>
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>   
    </x-slot>
</x-jet-dialog-modal>