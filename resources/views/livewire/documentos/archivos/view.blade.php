<div>
    <div class="card">
        <div class="card-datatable table-responsive">
            <div class="dataTables_wrapper dt-bootstrap5 no-footer">
                <div class="d-flex justify-content-between align-items-center header-actions mx-2 row mt-75">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="dataTables_length">
                                <label>Mostrar
                                    <select wire:model="cantidad" class="form-select">
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select> entradas
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="dt-action-buttons d-flex align-items-center justify-content-lg-end justify-content-center flex-md-nowrap flex-wrap">
                                <div class="me-1">
                                    <div id="DataTables_Table_0_filter" class="dataTables_filter">
                                        <label>
                                            Buscar:<input type="search" class="form-control" wire:model="searchArchivo" placeholder="">
                                        </label>
                                    </div>
                                </div>
                                <div class="dt-buttons btn-group flex-wrap">
                                    @can('create', App\Models\Archivo::class)
                                    <button id="{{ $identificador }}" wire:click="openArchivoModal({{ $documento_id }})" class="btn add-new btn-primary mt-50">
                                        <span>Agregar Archivo</span>
                                    </button>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @can('viewAny', App\Models\Archivo::class)
                <div class="text-nowrap">
                    <table class="datatables-permissions table dataTable no-footer dtr-column" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style="width: 1441px;">
                        <thead class="table-light">
                            <tr role="row">
                                <th class="sorting cursor-pointer" wire:click="ordenar('descripcion')">Nombre del Archivo</th>
                                <th>Responsable</th>
                                <th aria-label="Estado">Estado</th>
                                <th aria-label="Acciones" class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($archivos as $archivo)
                                <tr>
                                    <td>{{ $archivo->descripcion }}</td>
                                    @if ($archivo->documento->area_id)
                                        <td>{{ $archivo->documento->area->nombre }}</td>
                                    @else
                                        <td>-</td>
                                    @endif
                                    @if ($archivo->activo == 1)
                                        <td width="5%">
                                            <span class="badge rounded-pill badge-light-success" text-capitalized="">Activo</span>
                                        </td>
                                    @else
                                        <td width="5%">
                                            <span class="badge rounded-pill badge-light-secondary" text-capitalized="">Inactivo</span>
                                        </td>
                                    @endif
                                    @php
                                        $iden = rand();
                                    @endphp
                                    <td width="5%">
                                        @can('download', $archivo)
                                        <button wire:click="descargar({{ $archivo->id }})" class="btn btn-icon rounded-circle btn-flat-primary waves-effect">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                 stroke-linecap="round" stroke-linejoin="round" class="feather feather-download">
                                                <path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>
                                                <polyline points="7 10 12 15 17 10"></polyline>
                                                <line x1="12" y1="15" x2="12" y2="3"></line>
                                            </svg>
                                        </button>
                                        @endcan
                                        @can('view', $archivo)
                                        <button wire:click="show({{ $archivo->id }})" class="btn btn-icon rounded-circle btn-flat-primary waves-effect">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                 stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye">
                                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                <circle cx="12" cy="12" r="3"></circle>
                                            </svg>
                                        </button>
                                        @endcan
                                        @can('update', $archivo)
                                        <button id="{{ $iden }}" wire:click="edit({{ $archivo->id }})" class="btn btn-icon rounded-circle btn-flat-success waves-effect">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                 stroke-linejoin="round" class="feather feather-edit font-medium-2 text-body">
                                                <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                                <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                            </svg>
                                        </button>
                                        @endcan
                                        @can('delete', $archivo)
                                        <button id="{{ rand() }}" wire:click="$emit('EliminarArchivo',{{ $archivo->id }})" class="btn btn-icon rounded-circle btn-flat-danger waves-effect">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                 stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                            </svg>
                                        </button>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-between mx-2 row mb-1">
                        <div class="col-sm-12 col-md-6">
                            <div class="pt-1" aria-live="polite">
                                Mostrar {{ $archivos->firstItem() }} a {{ $archivos->lastItem() }} de {{ $archivos->total() }} entradas
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div id="link-archivos" class="pt-1">
                                {{ $archivos->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                @endcan
                <div>
                    @include('livewire.documentos.archivos.create')
                    @include('livewire.documentos.archivos.update')
                    @include('livewire.documentos.archivos.show')
                </div>
            </div>
        </div>
    </div>
    {{-- <script src="{{ asset(mix('js/scripts/codigo-script.js')) }}"></script> --}}
    <script>
        // function lanzarScriptsArchivosComponent(){
        // 	fechaVigencia = $('.fechaVigencia');
        // 	asignarFecha(fechaVigencia,'d-m-Y','01-01-1940');
        // 	console.log('ver si enttr');
        // }
        console.log('ver si enttr');
        // asignarFecha(fechaVigencia,'d-m-Y','01-01-1940');
    </script>
</div>
