<x-jet-dialog-modal wire:model="updateMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3>Actualizar Persona</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-4">
                <label class="form-label"><strong><span style="color: red">*</span> Nombres</strong></label>
                <input
                    type="text"
                    wire:model="nombres"
                    class="form-control"
                    placeholder="Nombres" maxlength="100"
                />
                @error('nombres')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-4">
                <label class="form-label"><strong><span style="color: red">*</span> Apellido Paterno</strong></label>
                <input
                    type="text"
                    wire:model="ape_paterno"
                    class="form-control"
                    placeholder="apellido paterno" maxlength="45"
                />
                @error('ape_paterno')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-4">
                <label class="form-label"><strong><span style="color: red">*</span> Apellido Materno</strong></label>
                <input
                    type="text"
                    wire:model="ape_materno"
                    class="form-control"
                    placeholder="Apellido materno" maxlength="45"
                />
                @error('ape_materno')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-4">
                <label class="form-label"><strong><span style="color: red">*</span> Tipo de Documento</strong></label>
                <select wire:model="param_tipodoc" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($p_tpdocs as $p_tpdoc)
                        <option value="{{$p_tpdoc->id}}">{{$p_tpdoc->detalle}}</option>
                    @endforeach
                </select>
                @error('param_tipodoc')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-4">
                <label class="form-label"><strong><span style="color: red">*</span> Nro. Documento</strong></label>
                <input
                    type="text"
                    wire:model="nro_documento"
                    class="form-control"
                    placeholder="Nro. Documento" maxlength="15"
                />
                @error('nro_documento')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-4">
                <label class="form-label"><strong><span style="color: red">*</span> Tipo de Persona</strong></label>
                <select wire:model="param_persona" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($p_tppers as $p_tpper)
                        <option value="{{$p_tpper->id}}">{{$p_tpper->detalle}}</option>
                    @endforeach
                </select>
                @error('param_persona')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-3">
                <label class="form-label"><strong>Celular</strong></label>
                <input
                    type="text"
                    wire:model="celular"
                    class="form-control"
                    placeholder="Celular" maxlength="12"
                />
                @error('celular')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-3">
                <label class="form-label"><strong>Fecha de Nacimiento</strong></label>
                <input type="text" wire:model="fecha_nac" class="form-control fechaflatpickr" placeholder="dd-mm-aaa"/>
                @error('fecha_nac')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-3">
                <label class="form-label"><strong>Género</strong></label>
                <select wire:model="genero" class="form-select">
                    <option value="">-- Seleccione --</option>
                    <option value="FEMENINO">FEMENINO</option>
                    <option value="MASCULINO">MASCULINO</option>

                </select>
                @error('genero')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-3">
                <label class="form-label"><strong>UBIGEO</strong></label>
                <input
                    type="text"
                    wire:model="ubigeo"
                    class="form-control"
                    placeholder="ubigeo" maxlength="120"
                />
                @error('ubigeo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-6">
                <label class="form-label"><strong>Dirección</strong></label>
                <input
                    type="text"
                    wire:model="direccion"
                    class="form-control"
                    placeholder="Dirección" maxlength="150"
                />
                @error('direccion')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-6">
                <label class="form-label"><strong>Correo Electrónico</strong></label>
                <input
                    type="text"
                    wire:model="email"
                    class="form-control"
                    placeholder="jhon@ejemplo.com" maxlength="100"
                />
                @error('email')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
            <x-jet-button wire:click.prevent="update()" class="btn btn-primary me-1 mt-2">
                Actualizar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
