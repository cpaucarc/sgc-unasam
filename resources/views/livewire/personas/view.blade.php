<div class="row justify-content-center">
	<div class="card">
		<div class="card-datatable table-responsive">
			<div class="dataTables_wrapper dt-bootstrap5 no-footer">
				<div class="d-flex justify-content-between align-items-center header-actions mx-2 row mt-75">
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<div class="dataTables_length">
								<label>Mostrar
									<select wire:model="cantidad" class="form-select">
										<option value="5">5</option>
										<option value="10">10</option>
										<option value="25">25</option>
										<option value="50">50</option>
										<option value="100">100</option>
									</select> entradas
								</label>
							</div>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="dt-action-buttons d-flex align-items-center justify-content-lg-end justify-content-center flex-md-nowrap flex-wrap">
								<div class="me-1">
									<div id="DataTables_Table_0_filter" class="dataTables_filter">
										<label>
											Buscar:<input type="search" class="form-control" wire:model="search" placeholder="">
										</label>
									</div>
								</div>
								<div class="dt-buttons btn-group flex-wrap">
									@can('create', App\Models\Persona::class)
									<x-jet-button wire:click="$set('open',true)" class="btn add-new btn-primary mt-50">
										<span>Agregar Personas</span>
									</x-jet-button>
									@endcan
								</div>
							</div>
						</div>
						<div>
							@include('livewire.personas.create')
							@include('livewire.personas.update')
							@include('livewire.personas.show')
						</div>
					</div>
				</div>
				
				@can('viewAny', App\Models\Persona::class)
				<div class="table-responsive px-25">
					<table class="datatables-permissions table dataTable no-footer dtr-column table-bordered" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style="width: 1441px;">
						<thead class="table-light">
							<tr role="row">
								<th class="sorting cursor-pointer" wire:click="ordenar('nombres')">Nombres</th>
								<th class="sorting cursor-pointer" wire:click="ordenar('ape_paterno')">Apellido Paterno</th>
								<th class="sorting cursor-pointer" wire:click="ordenar('ape_materno')">Apellido Materno</th>
								<th class="sorting cursor-pointer" wire:click="ordenar('nro_documento')">Nro. Documento</th>
								<th class="sorting cursor-pointer" wire:click="ordenar('celular')">Celular</th>
								<th aria-label="Estado">Estado</th>
								<th aria-label="Acciones">Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($personas as $persona)
								<tr>
									<td>{{$persona->nombres}}</td>
									<td>{{$persona->ape_paterno}}</td>
									<td>{{$persona->ape_materno}}</td>
									<td>{{$persona->nro_documento}}</td>
									<td>{{$persona->celular}}</td>
									@if($persona->activo == 1)
										<td>
											<span class="badge rounded-pill badge-light-success" text-capitalized="">Activo</span>
										</td>
									@else
										<td>
											<span class="badge rounded-pill badge-light-secondary" text-capitalized="">Inactivo</span>
										</td>
									@endif
									<td>
										@can('view', $persona)
                                        <button wire:click="show({{$persona->id}})" class="btn btn-icon btn-flat-secondary waves-effect px-0" data-toggle="tooltip" data-placement="top" title="Ver persona">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                        </button>
										@endcan
                                		@can('update', $persona)   
										<button wire:click="edit({{$persona->id}})" class="btn btn-icon btn-flat-success waves-effect px-0" data-toggle="tooltip" data-placement="top" title="Editar persona">
											<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit font-medium-2 text-body">
												<path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
												<path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
											</svg>
										</button>
										@endcan
										@can('delete', $persona)
										<button wire:click="$emit('EliminarPersona',{{$persona->id}})" class="btn btn-icon btn-flat-danger waves-effect px-0" data-toggle="tooltip" data-placement="top" title="Eliminar persona">
											<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
												<polyline points="3 6 5 6 21 6"></polyline>
												<path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
											</svg>
										</button>
										@endcan
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					<div class="d-flex justify-content-between mx-2 row mb-1">
						<div class="col-sm-12 col-md-6">
							<div class="dataTables_info" aria-live="polite">
								Mostrar {{$personas->firstItem()}} a {{$personas->lastItem()}} de {{$personas->total()}} entradas
							</div>
						</div>
						<div class="col-sm-12 col-md-6">
							{{$personas->links()}}
						</div>
					</div>
				</div>
				@endcan
			</div>
		</div>
	</div>
	<script>
		window.onload = function() {
		    var fechanac = $(".fechaflatpickr").flatpickr({
				dateFormat: "d-m-Y",
				minDate: '01-01-1940',
				allowInput: true,
				allowInvalidPreload: true,
				locale: {
					firstDayOfWeek: 1,
					weekdays: {
						shorthand: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
						longhand: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
					},
					months: {
						shorthand: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Оct', 'Nov', 'Dic'],
						longhand: ['Enero', 'Febreo', 'Мarzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
					},
				}
			});

			Livewire.on('alertPersona', function(datos){
				nitificacion(datos['tipo'],datos['mensaje'],' ¡Acción realizada!',2000);
			});
			Livewire.on('EliminarPersona', personaId=>{
				Swal.fire({
						title: '¿Está seguro?',
						text: "¡No podrás revertir esto!",
						icon: 'error',
						showCancelButton: true,
						confirmButtonText: '¡Sí, elimínalo!',
						cancelButtonText: 'Cancelar'
					}).then((result) => {
					if (result.isConfirmed) {
						Livewire.emitTo('personas','destroy',personaId)
					}
				})
			});
			
			$('[data-toggle="tooltip"]').tooltip();
		}
	</script>
</div>
