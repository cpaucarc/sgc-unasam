<!-- Modal -->
<x-jet-dialog-modal wire:model="showMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3 class="address-title text-center mb-1"><strong>Detalle Año</strong></h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-12">
                <label class="form-label"><strong>Año <span style="color: red">*</span> </strong></label>
                <input type="number" wire:model="anio" class="form-control" placeholder="Nombres"
                       maxlength="4" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" disabled/>
                @error('anio')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label"><strong>Nombre del año <span style="color: red">*</span> </strong></label>
                <input type="text" wire:model="nombre" class="form-control" placeholder="Nombre del año"
                maxlength="200" disabled/>
                @error('nombre')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="cerrar()" class="btn btn-primary me-1 mt-2">
                Cerrar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
