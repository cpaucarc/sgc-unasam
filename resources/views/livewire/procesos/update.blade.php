<!-- Modal -->
<x-jet-dialog-modal wire:model="updateMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3>Actualizar Proceso</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2">
            <div class="col-12 col-md-2">
                <label class="form-label"> <strong><strong style="color: red">*</strong> Código </strong> </label>
                <input type="text" wire:model="codigo" class="form-control" placeholder="Código" maxlength="8"/>
                @error('codigo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-2">
                <label class="form-label"> <strong><strong style="color: red">*</strong> Versión </strong> </label>
                <select wire:model="version_id" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($versiones as $version)
                    <option value="{{$version->id}}">{{ $version->numero }}</option>
                    @endforeach
                  </select>
               @error('version_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-8">
                <label class="form-label"> <strong><strong style="color: red">*</strong> Nombre </strong> </label>
                <input type="text" wire:model="nombre" class="form-control" placeholder="Nombre" maxlength="150"/>
                @error('nombre')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-8">
                <label class="form-label"> <strong><strong style="color: red">*</strong> Macroproceso </strong> </label>
                <select wire:model="macroproceso_id" wire:change="setProcesoSelect()" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($macroprocesos as $macroproceso)
                    <option value="{{$macroproceso->id}}">{{ $macroproceso->nombre }}</option>
                    @endforeach
                  </select>
               @error('macroproceso_id')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
             <div class="col-12 col-md-2">
                <label class="form-label">Orden</label>
                <input type="number" wire:model="orden" class="form-control" placeholder="Orden"
                       maxlength="2" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"/>
                @error('orden')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-2">
                <label class="form-label">Nivel Final</label>
                <select wire:model="nivelfinal" class="form-select" >
                    <option value="">--Seleccione--</option>
                    <option value="1">SI</option>
                    <option value="0">NO</option>
                  </select>
                @error('nivelfinal')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-8">
                <label class="form-label">Responsable</label>
                <select wire:model="responsable_area" class="form-select" >
                    <option value="">--Seleccione--</option>
                    @foreach($areas as $area)
                    <option value="{{$area->id}}">{{ $area->nombre }}</option>
                    @endforeach
                  </select>
                @error('responsable_area')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-4">
                <label class="form-label">Tipo</label>
                <select wire:model="param_tipo" class="form-select" disabled  >
                    <option value="">--Seleccione--</option>
                    @foreach($tipoMacroprocesos as $tipo)
                    <option value="{{$tipo->id}}">{{ $tipo->detalle }}</option>
                    @endforeach
                  </select>
               @error('param_tipo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="col-12 col-md-12">
                <label class="form-label">Descripción</label>
                <textarea wire:model="descripcion" class="form-control" placeholder="Descripción" maxlength="700"></textarea>
                @error('descripcion')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label">Objetivo</label>
                <textarea wire:model="objetivo" class="form-control" placeholder="Objetivo" maxlength="700"></textarea>
                @error('objetivo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label">Alcance</label>
                <textarea wire:model="alcance" class="form-control" placeholder="Alcance" maxlength="700"></textarea>
                @error('alcance')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label">Finalidad</label>
                <textarea wire:model="finalidad" class="form-control" placeholder="Finalidad" maxlength="700"></textarea>
                @error('finalidad')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>

        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="update()" class="btn btn btn-primary me-1 mt-2">
                Actualizar
            </x-jet-button>
            <x-jet-button wire:click.prevent="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
