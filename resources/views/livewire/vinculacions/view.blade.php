<div>
@section('title', __('vinculaciones'))
<h3>Macroprocesos Vinculados</h3>
<div class="card">
	<div class="card-datatable table-responsive">
		<div class="dataTables_wrapper dt-bootstrap5 no-footer">
			<div class="d-flex justify-content-between align-items-center header-actions mx-2 row mt-75">
				<div class="row">
					<div class="col-sm-12 col-md-6">
						<div class="dataTables_length">
							<label>Mostrar
								<select wire:model="cantidad" class="form-select">
									<option value="5">5</option>
									<option value="10">10</option>
									<option value="25">25</option>
									<option value="50">50</option>
									<option value="100">100</option>
								</select> entradas
							</label>
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						<div class="dt-action-buttons d-flex align-items-center justify-content-lg-end justify-content-center flex-md-nowrap flex-wrap">
							<div class="me-1">
								<div id="DataTables_Table_0_filter" class="dataTables_filter">
									<label>
										Buscar:<input type="search" class="form-control" wire:model="search" placeholder="">
									</label>
								</div>
							</div>
							<div class="dt-buttons btn-group flex-wrap">
								<x-jet-button wire:click="$set('open',true)" class="btn add-new btn-primary mt-50">
									<span>Agregar Vinculación</span>
								</x-jet-button>
							</div>
						</div>
					</div>
					<div>
						@include('livewire.vinculacions.create')
						@include('livewire.vinculacions.update')
						@include('livewire.vinculacions.show')
					</div>
				</div>

			</div>
			<div class="text-nowrap">
				<table class="datatables-permissions table dataTable no-footer dtr-column" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info" style="width: 1441px;">
					<thead class="table-light">
						<tr role="row">
							<th class="sorting cursor-pointer" wire:click="ordenar('name')">NOMBRE DEL MACROPROCESO</th>
							<th class="sorting cursor-pointer" wire:click="ordenar('descripcion')">MACROPROCESOS DIRECTAMENTE VINCULADOS</th>
							<th aria-label="Estado">Estado</th>
							<th aria-label="Acciones">Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($vinculaciones as $vinculacion)
							<tr>
								<td>Macroproceso: {{$vinculacion->codigo}} {{$vinculacion->nombre}}</td>
								<td>{{getNombreProceso(1,$vinculacion->vin_proceso_id)}}</td>
								@if($vinculacion->activo == 1)
									<td>
										<span class="badge rounded-pill badge-light-success" text-capitalized="">Activo</span>
									</td>
								@else
									<td>
										<span class="badge rounded-pill badge-light-secondary" text-capitalized="">Inactivo</span>
									</td>
								@endif
								<td>
                                    <button wire:click="show({{$vinculacion->id}})" class="btn btn-icon btn-flat-secondary waves-effect px-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>

                                    </button>
									<button wire:click="edit({{$vinculacion->id}})" class="btn btn-icon btn-flat-success waves-effect">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit font-medium-2 text-body">
                                            <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                                            <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
                                        </svg>
                                    </button>
									<button wire:click="$emit('EliminarVinculacion',{{$vinculacion->id}})" class="btn btn-icon btn-flat-danger waves-effect">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash font-medium-2 text-body">
                                            <polyline points="3 6 5 6 21 6"></polyline>
                                            <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                        </svg>
                                    </button>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				<div class="d-flex justify-content-between mx-2 row mb-1">
					<div class="col-sm-12 col-md-6">
						<div class="dataTables_info" aria-live="polite">
							Mostrar {{$vinculaciones->firstItem()}} a {{$vinculaciones->lastItem()}} de {{$vinculaciones->total()}} entradas
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						{{$vinculaciones->links()}}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

	window.onload = function() {
		select2 = $('#tpProceso');
		selectMultiple = $('#proceso');
		// selectMultiple.select2();
		// selectMultiple.select2();
		// selectMultiple.multiselect();

		Livewire.on('obtenerProcesos', function(){
        	selectMultiple.select2();
		});

		Livewire.on('asignarValor', function(){
			$valores = selectMultiple.select2('data');
			// console.log($valores['id']);
			@this.set('multiselect', $valores);
			Livewire.emit('store')

		});

		Livewire.on('alert', function(menssage){
			Swal.fire(
				'¡Buen trabajo!',
				menssage,
				'success'
			)
		});

		// Livewire.on('asignarValor', function(){
		// 	console.log('sirve');
        // 	$valores = selectMultiple.select2('data');
		// 	@this.set('multiselect', $valores)
		// });

		// selectMultiple.on('select2:select', function (e) {
		// 	$valores = selectMultiple.select2('data');
		// 	@this.set('multiselect', $valores)
		// });

		// selectMultiple.on("click", function () {
		// 	$ver = $(this).select2('data');
		// 	console.log("ok");

		// 	// $exampleMulti.val(["CA", "AL"]).trigger("change");
		// });

		// selectMultiple.on('select2:select', function (e) {
		// 	// $valores = selectMultiple.select2('data')
		// 	// @this.set('multiselect', $valores);

		// 	let data = selectMultiple.select2('data');
		// 	@this.set('multiselect', data);
		// 	console.log(@js($multiselect));
		// });

		// document.addEventListener('DOMContentLoaded', () => {
		// 	Livewire.hook('element.updated', (el, component) => {
		// 		var data = @this.data;
		// 		console.log(data);
		// 	})
		// });


		// selectMultiple.on('change', function (e) {
		// 	selectMultiple.select2();
		// 	let data = $(this).val();
		// 	@this.set('vin_proceso_id', data);
		// });
	}
</script>
</div>
