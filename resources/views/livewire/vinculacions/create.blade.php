<!-- Modal -->
<x-jet-dialog-modal wire:model="open" maxWidth="lg">
    <x-slot name="title">
        <h3>Agregar Nueva Vinculación</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div id="form-vinculaciones">
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-3">
                    <label class="form-label"><strong><span style="color:red">*</span> Nivel de Proceso</strong></label>
                    <select wire:model="nivel_proceso" wire:change="obtenerTipoProcesos()" class="form-select">
                        <option value="NULL">-- Seleccione --</option>
                        @foreach ($niveles_procesos as $niveles_proceso)
                            <option value="{{$niveles_proceso->id}}">{{$niveles_proceso->detalle}}</option>
                        @endforeach
                    </select>
                    @error('nivel_proceso')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-12 col-md-9">
                    <label class="form-label"><strong><span style="color:red">*</span> Nombre del Tipo de Proceso</strong></label>
                    <div>
                        <select  wire:model="proceso_id"  wire:change="obtenerProcesos()" id="tpProceso" class="form-select">
                            <option value="NULL">-- Seleccione --</option>
                            @foreach ($nivelprocesos as $nivelproceso)
                                <option value="{{$nivelproceso->id}}">{{$nivelproceso->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('proceso_id')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12">
                    <label class="form-label"><strong>Macroproceso a Vincular</strong></label>
                    <div wire:ignore.self>
                        <select wire:model="vin_proceso_id" id="proceso" class="form-select" multiple="multiple">
                            <option value="NULL">-- Seleccione --</option>
                            @foreach ($vinprocesos as $vinproceso)
                                <option value="{{$vinproceso->id}}">{{$vinproceso->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('vin_proceso_id')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
            <x-jet-button wire:click.prevent="$emit('asignarValor')" class="btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
