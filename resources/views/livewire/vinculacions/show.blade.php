<!-- Modal -->
<x-jet-dialog-modal wire:model="showMode" maxWidth="lg">
    <x-slot name="title">
        <h3>Detalle Vinculación</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div id="form-edit-vinculaciones">
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-3">
                    <label class="form-label"><strong>Nivel de Proceso</strong></label>
                    <select wire:model="nivel_proceso" wire:change="obtenerTipoProcesos()" class="form-select" disabled>
                        <option value="NULL">-- Seleccione --</option>
                        @foreach ($niveles_procesos as $niveles_proceso)
                            <option value="{{$niveles_proceso->id}}">{{$niveles_proceso->detalle}}</option>
                        @endforeach
                    </select>
                    @error('nivel_proceso')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-12 col-md-9">
                    <label class="form-label"><strong>Nombre del Tipo de Proceso</strong></label>
                    <div>
                        <select  wire:model="proceso_id"  wire:change="obtenerProcesos()" id="tpProceso" class="form-select" disabled>
                            <option value="NULL">-- Seleccione --</option>
                            @foreach ($nivelprocesos as $nivelproceso)
                                <option value="{{$nivelproceso->id}}">{{$nivelproceso->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('proceso_id')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12">
                    <label class="form-label"><strong>Macroproceso a Vincular</strong></label>
                    <div wire:ignore.self>
                        <select wire:model="vin_proceso_id" id="proceso" class="form-select" multiple="multiple" disabled>
                            <option value="NULL">-- Seleccione --</option>
                            @foreach ($vinprocesos as $vinproceso)
                                <option value="{{$vinproceso->id}}">{{$vinproceso->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('vin_proceso_id')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click="cerrar()" class="btn btn-primary mt-2">
                Cerrar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
