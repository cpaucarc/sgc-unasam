<!-- Modal -->
<x-jet-dialog-modal wire:model="updateMode" maxWidth="xl">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3>Actualizar Persona</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">

        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-9">
                <label class="form-label"><strong><span style="color:red">*</span> Nombre del Rol</strong></label>
                <input
                       type="text"
                       wire:model="name"
                       class="form-control"
                       placeholder="Rol" maxlength="75" />
                @error('name')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="col-12 col-md-3">
                <label class="form-label"><strong><span style="color:red">*</span> Estado de Rol {{ $activo }}</strong></label>
                <select wire:model="activo" class="form-select">
                    <option value="">-- Seleccione --</option>
                    <option value="1">ACTIVO</option>
                    <option value="0">INACTIVO</option>
                </select>
                @error('activo')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-12">
                <label class="form-label"><strong><span style="color:red">*</span> Descripción</strong></label>
                <textarea
          wire:model="descripcion"
          class="form-control"
          placeholder="Descripción del rol"
          rows="2" maxlength="400"></textarea>
                @error('descripcion')
                    <span class="text-sm text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <hr>
        @foreach ($grupos as $grupo)
            <div class="row gy-5 gx-2 pb-1">
                <div class="col-12">
                    <div>
                        <h4>{{ $grupo->name }}</h5>
                    </div>
                </div>
                @foreach ($grupo->categories->chunk(3) as $chunk)
                    <div class="row">
                        @foreach ($chunk as $category)
                            <div class="col-12 col-md-4" x-data="{ open: false }">
                                <div class="row">
                                    <div class="col-8">
                                        @if ($category->permissions->pluck('name')->diff($permisos)->count() === 0)
                                            <button wire:click="quitaCategoria('{{ $category->id }}')" class="btn btn-sm btn-success">{{ $category->name }}</button>
                                        @elseif($category->permissions->pluck('name')->diff($permisos)->count() > 0 &&
                                            $category->permissions->pluck('name')->diff($permisos)->count() < $category->permissions->pluck('name')->count())
                                            <button wire:click="asignaCategoria('{{ $category->id }}')" class="btn btn-sm btn-warning ">
                                                <span class="text-black"> {{ $category->name }} </span>
                                            </button>
                                        @else
                                            <button wire:click="asignaCategoria('{{ $category->id }}')" class="btn btn-sm btn-flat-secondary">{{ $category->name }}</button>
                                        @endif
                                    </div>
                                    <div class="col-4 text-right">
                                        <button x-on:click="open = !open" class="btn btn-sm" x-bind:class="!open ? 'btn-flat-primary' : 'btn-primary'">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                 stroke-linejoin="round" class="feather feather-plus">
                                                <line x1="12" y1="5" x2="12" y2="19"></line>
                                                <line x1="5" y1="12" x2="19" y2="12"></line>
                                            </svg>
                                        </button>
                                    </div>
                                    <div class="col-12 my-2" x-show="open">
                                        <table class="table table-sm table-bordered">
                                            <tbody>
                                                @foreach ($category->permissions as $permission)
                                                    <tr>
                                                        <td style="width: 75%">{{ $permission->description }}</td>
                                                        <td class="text-center">
                                                            @if ($permisos->contains($permission->name))
                                                                <button wire:click="quitaPermiso('{{ $permission->name }}')"
                                                                        class="btn btn-sm btn-success">Seleccionado</button>
                                                            @else
                                                                <button wire:click="asignaPermiso('{{ $permission->name }}')"
                                                                        class="btn btn-sm btn-flat-danger">Seleccionar</button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
        @endforeach
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
            <x-jet-button wire:click.prevent="update()" class="btn btn-primary me-1 mt-2">
                Actualizar
            </x-jet-button>
        </div>
    </x-slot>
</x-jet-dialog-modal>
