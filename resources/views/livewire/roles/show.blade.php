<!-- Modal -->
<x-jet-dialog-modal wire:model="showMode" maxWidth="lg">
    <x-slot name="title" class="modal-header bg-transparent">
        <h3>Actualizar Persona</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">

        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-9">
                <label class="form-label"><strong><span style="color:red">*</span> Nombre del Rol</strong></label>
                <input
                    type="text"
                    wire:model="name"
                    class="form-control"
                    placeholder="Rol" maxlength="75" disabled
                />
                @error('name')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-3">
                <label class="form-label"><strong><span style="color:red">*</span> Estado de Rol {{$activo}}</strong></label>
                <select wire:model="activo" class="form-select" disabled>
                    <option value="">-- Seleccione --</option>
                    <option value="1">ACTIVO</option>
                    <option value="0">INACTIVO</option>
                </select>
                @error('activo')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
        <div class="row gy-1 gx-2 pb-1">
            <div class="col-12 col-md-12">
                <label class="form-label"><strong><span style="color:red">*</span> Descripción</strong></label>
                <textarea
                    wire:model="descripcion"
                    class="form-control"
                    placeholder="Descripción del rol"
                    rows="2" maxlength="400" disabled
                ></textarea>
                @error('descripcion')
                    <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click.prevent="cerrar()" class="btn btn-primary me-1 mt-2">
                Cerrar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
