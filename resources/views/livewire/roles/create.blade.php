<!-- Modal -->
<x-jet-dialog-modal wire:model="open" maxWidth="lg">
    <x-slot name="title">
        <h3>Agregar Nuevo Rol</h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div id="rol">
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-9">
                    <label class="form-label"><strong><span style="color:red">*</span> Nombre del Rol</strong></label>
                    <input
                        type="text"
                        wire:model="name"
                        class="form-control"
                        placeholder="Rol" maxlength="75"
                    />
                    @error('name')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
                <div class="col-12 col-md-3">
                    <label class="form-label"><strong><span style="color:red">*</span>  Estado de Rol</strong></label>
                    <select wire:model="activo" class="form-select">
                        <option value="">-- Seleccione --</option>
                        <option value="1">ACTIVO</option>
                        <option value="0">INACTIVO</option>
                    </select>
                    @error('activo')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
            <div class="row gy-1 gx-2 pb-1">
                <div class="col-12 col-md-12">
                    <label class="form-label"><strong><span style="color:red">*</span> Descripción</strong></label>
                    <textarea
                        wire:model="descripcion"
                        class="form-control"
                        placeholder="Descripción del rol"
                        rows="2" maxlength="4000"
                    ></textarea>
                    @error('descripcion')
                        <span class="text-sm text-danger">{{$message}}</span>
                    @enderror
                </div>
            </div>
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>
            <x-jet-button wire:click.prevent="store()" class="btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
        </div>

    </x-slot>
</x-jet-dialog-modal>
