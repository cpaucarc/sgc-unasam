@php
$configData = Helper::applClasses();
@endphp
@extends('layouts/fullLayoutMaster')

@section('title', 'Error 403')

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-misc.css')) }}">
@endsection
@section('content')
<!-- Error page-->
<div class="misc-wrapper">
  <a class="brand-logo" href="/">
    <img src="/images/logo/unasam.png" alt="Logo">

    <h2 class="brand-text text-primary ms-1">SGC UNASAM</h2>
  </a>
  <div class="misc-inner p-2 p-sm-3">
      <div class="w-100 text-center">
          <h2 class="mb-1">
           No autorizado 🔐
          </h2>
          <p class="mb-2">{{ $exception->getMessage() ?: 'No tiene permiso para entrar a esta ubicación' }}</p>
          <a class="btn btn-primary mb-2 btn-sm-block" href="{{url('/')}}">Ir al inicio</a> <br>
          @if($configData['theme'] === 'dark')
          <img class="img-fluid" src="{{asset('images/pages/not-authorized-dark.svg')}}" alt="Error page" />
          @else
          <img class="img-fluid" src="{{asset('images/pages/not-authorized.svg')}}" alt="Error page" />
          @endif
    </div>
  </div>
</div>
<!-- / Error page-->
@endsection
