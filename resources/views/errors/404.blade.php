@php
$configData = Helper::applClasses();
@endphp
@extends('layouts/fullLayoutMaster')

@section('title', 'Error 404')

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-misc.css')) }}">
@endsection
@section('content')
<!-- Error page-->
<div class="misc-wrapper">
  <a class="brand-logo" href="#">
    <img src="/images/logo/unasam.png" height="30" alt="Logo">
    <h2 class="brand-text text-primary ms-1">SGA UNASAM</h2>
  </a>
  <div class="misc-inner p-2 p-sm-3">
      <div class="w-100 text-center">
          <h2 class="mb-1">Página no encontrada 🕵🏻‍♀️</h2>
          <p class="mb-2">Woops! 😖 La URL solicitada no se encontró en este servidor.</p>
          <a class="btn btn-primary mb-2 btn-sm-block" href="{{url('/')}}">Volver atrás</a>
          @if($configData['theme'] === 'dark')
          <img class="img-fluid" src="{{asset('images/pages/error-dark.svg')}}" alt="Error page" />
          @else
          <img class="img-fluid" src="{{asset('images/pages/error.svg')}}" alt="Error page" />
          @endif
    </div>
  </div>
</div>
<!-- / Error page-->
@endsection
