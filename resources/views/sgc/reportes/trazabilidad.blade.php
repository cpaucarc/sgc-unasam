@extends('layouts/contentLayoutMaster')

@section('title', 'Reportes por indicadores')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection
@section('page-style')
    {{-- Page css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card card-transaction">
                    <div class="card-header">
                        <h4 class="card-title">Trazabilidad</h4>
                    </div>
                    <div class="card-body">
                        <div class="transaction-item">
                            <div class="d-flex">
                                <div class="avatar bg-light-primary rounded float-start">
                                    <div class="avatar-content">
                                        1
                                    </div>
                                </div>
                                <div class="transaction-percentage">
                                    <h6 class="transaction-title">ICACIT</h6>
                                    <small>Matriz macroprocesos y modelo de acreditación ICACIT</small>
                                </div>
                            </div>
                            <div class="fw-bolder">
                                <a href="/reportes/trazabilidad/{{ config('trazabilidad.matriz_icacit_pdf') }}" target="_blank"
                                   class="btn btn-flat-danger btn-sm px-0">PDF</a>
                                <a href="/reportes/trazabilidad/{{ config('trazabilidad.matriz_icacit_excel') }}" target="_blank"
                                   class="btn btn-flat-success btn-sm px-0">Excel</a>
                            </div>
                        </div>
                        <div class="transaction-item">
                            <div class="d-flex">
                                <div class="avatar bg-light-primary rounded float-start">
                                    <div class="avatar-content">
                                        2
                                    </div>
                                </div>
                                <div class="transaction-percentage">
                                    <h6 class="transaction-title">ISO</h6>
                                    <small>Matriz macroprocesos y la ISO 9001 2015</small>
                                </div>
                            </div>
                            <div class="fw-bolder">
                                <a href="/reportes/trazabilidad/{{ config('trazabilidad.matriz_iso_pdf') }}" target="_blank"
                                   class="btn btn-flat-danger btn-sm px-0">PDF</a>
                                <a href="/reportes/trazabilidad/{{ config('trazabilidad.matriz_iso_excel') }}" target="_blank"
                                   class="btn btn-flat-success btn-sm px-0">Excel</a>
                            </div>
                        </div>
                        <div class="transaction-item">
                            <div class="d-flex">
                                <div class="avatar bg-light-primary rounded float-start">
                                    <div class="avatar-content">
                                        3
                                    </div>
                                </div>
                                <div class="transaction-percentage">
                                    <h6 class="transaction-title">Licenciamiento institucional</h6>
                                    <small>Matriz macroprocesos y modelo de licenciamiento institucional</small>
                                </div>
                            </div>
                            <div class="fw-bolder">
                                <a href="/reportes/trazabilidad/{{ config('trazabilidad.matriz_licenciamiento_pdf') }}" target="_blank"
                                   class="btn btn-flat-danger btn-sm px-0">PDF</a>
                                <a href="/reportes/trazabilidad/{{ config('trazabilidad.matriz_licenciamiento_excel') }}" target="_blank"
                                   class="btn btn-flat-success btn-sm px-0">Excel</a>
                            </div>
                        </div>
                        <div class="transaction-item">
                            <div class="d-flex">
                                <div class="avatar bg-light-primary rounded float-start">
                                    <div class="avatar-content">
                                        4
                                    </div>
                                </div>
                                <div class="transaction-percentage">
                                    <h6 class="transaction-title">Acreditación institucional</h6>
                                    <small>Matriz macroprocesos y modelo de acreditación institucional</small>
                                </div>
                            </div>
                            <div class="fw-bolder">
                                <a href="/reportes/trazabilidad/{{ config('trazabilidad.matriz_acreditacion_pdf') }}" target="_blank"
                                   class="btn btn-flat-danger btn-sm px-0">PDF</a>
                                <a href="/reportes/trazabilidad/{{ config('trazabilidad.matriz_acreditacion_excel') }}" target="_blank"
                                   class="btn btn-flat-success btn-sm px-0">Excel</a>
                            </div>
                        </div>
                        <div class="transaction-item">
                            <div class="d-flex">
                                <div class="avatar bg-light-primary rounded float-start">
                                    <div class="avatar-content">
                                        5
                                    </div>
                                </div>
                                <div class="transaction-percentage">
                                    <h6 class="transaction-title">SINEACE</h6>
                                    <small>Matriz de macroprocesos y el modelo de acreditación para programas</small>
                                </div>
                            </div>
                            <div class="fw-bolder">
                                <a href="/reportes/trazabilidad/{{ config('trazabilidad.matriz_sineace_pdf') }}" target="_blank"
                                   class="btn btn-flat-danger btn-sm px-0">PDF</a>
                                <a href="/reportes/trazabilidad/{{ config('trazabilidad.matriz_sineace_excel') }}" target="_blank"
                                   class="btn btn-flat-success btn-sm px-0">Excel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/codigo-script.js')) }}"></script>
@endsection
@section('page-script')
    <script type="text/javascript" async
            src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/MathJax.js?config=TeX-MML-AM_CHTML"></script>
    <script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});</script>
@endsection
