@extends('layouts/contentLayoutMaster')

@section('title', 'Caracterización')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
@section('page-style')
    {{-- Page css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <style>
        .border-black {
            border-color: rgb(77, 74, 74);
        }
    </style>
@endsection

@section('content')

    <section id="caracterizacion">
        @livewire('caracterizacion')
    </section>

@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/codigo-script.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/charts/echarts.min.js')) }}"></script>
@endsection
@section('page-script')
    <script>
        function lanzarScripts() {
            scriptCaracterizacion();

            $("#btn_limpiar").on('click', function() {
                $('#macroproceso').val(null).trigger('change');
                $('#proceso').val(null).trigger('change');
            })
        }
        window.onload = lanzarScripts;
    </script>
@endsection
