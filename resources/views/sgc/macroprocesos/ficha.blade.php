@extends('layouts/contentLayoutMaster')

@section('title','Macroprocesos')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection
@section('page-style')
    {{-- Page css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection

@section('content')
    <!-- Dashboard Ecommerce Starts -->

    <section id="macroprocesos">
        <div class="card">
            {{--{{$indicador}}--}}
            {{--{{$indicador->get(0)->nombre}}--}}

            {{--<div class="card-header">
                <h4 class="card-title text-primary"> {{$indicador->codigo}}: {{$indicador->nombre}}</h4>
                <div class="heading-elements">
                    <ul class="list-inline">
                        <li>
                            <a href="" class="btn btn-gradient-primary">Ficha de Caracterización</a>
                            --}}{{--{{route('macroprocesos.caracterizacion',$macroproceso->id)}}--}}{{--
                        </li>
                        <li>
                            <a href="#" class="btn btn-gradient-primary">Ficha de Indicadores</a>
                        </li>
                        <li>
                            <a href="" class="btn btn-gradient-primary">Regresar</a>
                            --}}{{--{{route('macroprocesos.show',$macroproceso->id)}}--}}{{--
                        </li>
                        <li>
                            <a data-action="collapse"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg></a>
                        </li>
                        <li>
                            <a data-action="reload"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-rotate-cw"><polyline points="23 4 23 10 17 10"></polyline><path d="M20.49 15a9 9 0 1 1-2.12-9.36L23 10"></path></svg></a>
                        </li>
                    </ul>
                </div>
            </div>--}}




            <div class="card">
                <div class="card-body">
                    <div class="card-header">
                        <h4 class="card-title text-primary">{{$macroproceso->codigo}}: {{$macroproceso->nombre}}</h4>
                        <div class="heading-elements">
                            <ul class="list-inline">
                                <li>
                                    <a href="" class="btn btn-gradient-primary">Ficha de Caracterización</a>
                                    {{--{{route('macroprocesos.caracterizacion',$macroproceso->id)--}}
                                </li>
                                <li>
                                    <a href="#" class="btn btn-gradient-primary">Ficha de Indicadores</a>
                                </li>
                                <li>
                                    <a href="" class="btn btn-gradient-primary">Regresar</a>
                                    {{--{{route('macroprocesos.show',$macroproceso->id)}}--}}
                                </li>
                                <li>
                                    <a data-action="collapse">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-chevron-down">
                                            <polyline points="6 9 12 15 18 9"></polyline>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a data-action="reload">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-rotate-cw">
                                            <polyline points="23 4 23 10 17 10"></polyline>
                                            <path d="M20.49 15a9 9 0 1 1-2.12-9.36L23 10"></path>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>


                    <div class="card-datatable table-responsive">
                        <table class="table table-bordered">
                            <thead class="table-light">
                            <tr role="row">
                                <th width="90px" class="text-center">Nro</th>
                                <th width="135px" class="">Código</th>
                                <th class="">Nombre Indicador</th>
                                <th width="10%" class="text-center">Versión</th>
                                <th class="">Nivel</th>
                                <th width="130px" class="">Estado</th>
                                <th width="85px" class="" aria-label="Actions">Ver
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($indicadores as $indicador)
                                <tr class="odd">
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td class=" control" tabindex="0" style="display: none;"></td>
                                    <td class="text-center">{{$indicador->codigo}}</td>
                                    <td>{{$indicador->nombre}}</td>
                                    <td class="text-center">{{$indicador->version->numero}}</td>
                                    <td> @if ($indicador->macroproceso_id)
                                        <strong>{{$indicador->macroproceso->codigo}}: </strong>{{$indicador->macroproceso->nombre}}  <br>
                                         @endif
                                         @if ($indicador->proceso_id)
                                        <strong>{{$indicador->proceso->codigo}}: </strong>{{$indicador->proceso->nombre}}  <br>
                                         @endif
                                         @if ($indicador->subproceso_id)
                                        <strong>{{$indicador->subproceso->codigo}} : </strong>{{$indicador->subproceso->nombre}}   <br>
                                         @endif
                                         @if ($indicador->microproceso_id)
                                        <strong>{{$indicador->macriproceso->codigo}}: </strong>{{$indicador->macriproceso->nombre}}  <br>
                                         @endif

                                    </td>
                                    <td>
                                        @if($indicador->activo == 1)
                                            <span class="badge rounded-pill badge-light-success" text-capitalized="">Activo</span>
                                        @else
                                            <span class="badge rounded-pill badge-light-secondary" text-capitalized="">Inactivo</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('show.ficha', ['indicador'=>$indicador->id,'anio'=>getAnioIndicadorActual()])}}" class="btn btn-icon btn-flat-danger waves-effect">
                                            <svg width="20px" height="20px" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M.2 10a11 11 0 0 1 19.6 0A11 11 0 0 1 .2 10zm9.8 4a4 4 0 1 0 0-8 4 4 0 0 0 0 8zm0-2a2 2 0 1 1 0-4 2 2 0 0 1 0 4z"/></svg>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{--<div class="row px-2">

                <div>
                    <div class="container mx-auto">
                        <button type="button"
                                wire:click="$toggle('showDiv')"
                                class="px-4 py-2 font-bold text-purple-100 bg-purple-500">
                            How & hide div
                        </button>
                        @if ($showDiv)
                            <div class="p-4 mt-8 text-green-900 bg-green-200">
                                <p>Show and Hide Dive Elements in laravel livewire</p>
                            </div>
                        @endif
                    </div>
                </div>
                holasssssssssssssssssssssssssssssssss
                holasssssssssssssssssssssssssssssssss
                holasssssssssssssssssssssssssssssssss
                holasssssssssssssssssssssssssssssssss
                holasssssssssssssssssssssssssssssssss
                holasssssssssssssssssssssssssssssssss
                holasssssssssssssssssssssssssssssssss
                holasssssssssssssssssssssssssssssssss
                holasssssssssssssssssssssssssssssssss
                holasssssssssssssssssssssssssssssssss
            </div>--}}




            {{--<div class="row px-2">
                <div class="py-1">
                    <table>
                        <tr>
                            <td>
                                <div class="col-12" style="padding-left: 20px">
                                    <table style="width: 95%; border: 2px solid #636e72;  border-collapse: collapse;">
                                        <tr style="border: 1px solid black;  border-collapse: collapse;">
                                            <td style="background-color: #dfe6e9; width: 40%; border: 2px solid #636e72;  border-collapse: collapse;">
                                                <div class="col-12 text-center">NOMBRE MACROPROCESO</div>
                                            </td>
                                            <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                                PM07: Investigación - Desarrollo - Tecnologia - Innovación
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;  border-collapse: collapse;">
                                            <td style="background-color: #dfe6e9; width: 40%; border: 2px solid #636e72;  border-collapse: collapse;">
                                                <div class="col-12 text-center">NOMBRE DEL INDICADOR</div>
                                            </td>
                                            <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                                Eficacia en la gestión de I+D+I
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                                <div class="col-12 text-center">TIPO</div>
                                            </td>
                                            <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                                Eficacia
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td style="width: 430px;">
                                <table style="width: 95%; border: 2px solid #636e72;  border-collapse: collapse;">
                                    <tr style="border: 1px solid black;  border-collapse: collapse;">
                                        <td style="background-color: #dfe6e9; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                                            <div class="col-12">CÓDIGO</div>
                                        </td>
                                        <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                            <input class="inputcentrado" style="width:100%;border-style: hidden;" type="text"
                                                   name="name" value="FC-PM07"/>
                                        </td>
                                    </tr>
                                    <tr style="border: 1px solid black;  border-collapse: collapse;">
                                        <td style="background-color: #dfe6e9; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                                            VERSIÓN
                                        </td>
                                        <td class="inputcentrado" style="border: 2px solid #636e72;  border-collapse: collapse;">2.5</td>
                                        <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                            VIGENCIA
                                        </td>
                                        <td class="inputcentrado" style="width: 30%; border: 2px solid #636e72;  border-collapse: collapse;">22/12/2021</td>
                                    </tr>

                                    <tr style="border: 1px solid black;  border-collapse: collapse;">
                                        <td style="background-color: #dfe6e9; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                                            PERIODO
                                        </td>
                                        <td class="inputcentrado">2021</td>
                                        <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                            PERIODO
                                        </td>
                                        <td class="inputcentrado" style="width: 30%">2021</td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <hr>
                <div class="row py-1" style="padding-left: 20px">
                    <div class="col-6">
                        <h4 class="text-primary"><strong>DEFINICIÓN:</strong></h4>
                        <table style="width: 98%; border: 2px solid #d3dadd;  border-collapse: collapse; padding-left: 20px">
                            <tr>
                                <td>
                                    <div class="col-12 p-1">
                                        <input wire:model="definicion" class="inputcentrado" style="width:100%;border-style: hidden;" type="text"
                                               name="name" value="{{$indicador->definicion}}"/>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-6">
                        <h4 class="text-primary"><strong>OBJETIVO DEL INDICADOR:</strong></h4>
                        <table style="width: 98%; border: 2px solid #d3dadd;  border-collapse: collapse; padding-left: 20px">
                            <tr>
                                <td>

                                    <div class="col-12 p-1">
                                        <input wire:model="objetivo" class="inputcentrado" style="width:100%;border-style: hidden;" type="text"
                                               name="name" value="{{$indicador->objetivo}}"/>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
                <hr>

                <div class="col-12" style="padding-left: 20px; padding-right: 20px">
                    <div class="row">
                        <div class="col-6">
                            <h4 class="text-secondary"><strong>RESPONSABLES DE LA INFORMACIÓN:</strong></h4>
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="areas-tab" data-bs-toggle="tab" href="#areas"
                                                   aria-controls="home" role="tab" aria-selected="true" >Áreas</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="procesos-tab" data-bs-toggle="tab" href="#procesos" aria-controls="profile"
                                                   role="tab" aria-selected="false">Procesos</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="responsables-tab" data-bs-toggle="tab" href="#responsables"
                                                   aria-controls="about" role="tab" aria-selected="false">Responsables</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="usuarios-tab" data-bs-toggle="tab" href="#usuarios"
                                                   aria-controls="about" role="tab" aria-selected="false">Usuarios</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="areas" aria-labelledby="areas-tab" role="tabpanel">

                                                @livewire('ficha.suministro-area',['indicador_id'=>$indicador->id])

                                            </div>
                                            <div class="tab-pane" id="procesos" aria-labelledby="procesos-tab" role="tabpanel">

                                                @livewire('ficha.suministro-proceso',['indicador_id'=>$indicador->id])

                                            </div>
                                            <div class="tab-pane" id="responsables" aria-labelledby="responsables-tab" role="tabpanel">

                                                @livewire('ficha.responsables',['indicador_id'=>$indicador->id])

                                            </div>
                                            <div class="tab-pane" id="usuarios" aria-labelledby="usuarios-tab" role="tabpanel">

                                                @livewire('ficha.usuario',['indicador_id'=>$indicador->id])

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6">
                            @livewire('ficha.restricciones',['indicador_id'=>$indicador->id])
                        </div>

                        <div class="col-6">
                            @livewire('ficha.formula',['indicador_id'=>$indicador->id])
                        </div>

                        @livewire('ficha.medicion',['indicador_id'=>$indicador->id])


                    </div>
                </div>
            </div>--}}
            <hr>
        </div>
        {{--<h3>Procesos de {{$macroproceso->nombre}}</h3>--}}
    </section>
    <!-- Dashboard Ecommerce ends -->
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
@endsection

<style>
    #combo {
        font-family: Tahoma, Verdana, Arial;
        font-size: 11px;
        color: #707070;
        background-color: #FFFFFF;
        border-width: 0;
    }

    input:focus,
    select:focus,
    textarea:focus,
    button:focus {
        outline: none;
    }

    .inputcentrado {
        text-align: center
    }

    table{
        width:100%;
        table-layout: fixed;
        overflow-wrap: break-word;
    }
</style>
