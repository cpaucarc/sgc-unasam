<!-- Modal -->
<x-jet-dialog-modal wire:model="open" maxWidth="lg">
    <x-slot name="title">
        <h3 class="address-title text-center mb-1"><strong>Agregar Año</strong></h3>
    </x-slot>
    <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
        <div class="row gy-1 gx-2 pb-1">
            {{--<div class="col-12 col-md-12">
                <label class="form-label"><strong>Año <span style="color: red">*</span> </strong></label>
                <input type="text" wire:model="anio" class="form-control" placeholder="Año"/>
                @error('anio')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>
            <div class="col-12 col-md-12">
                <label class="form-label"><strong>Nombre del Año <span style="color: red">*</span> </strong></label>
                <input type="text" wire:model="nombre" class="form-control" placeholder="Nombre del año"/>
                @error('nombre')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>--}}
        </div>
        <div class="row gy-1 gx-2 pb-1">
            {{--<div class="col-12 col-md-4">
                <label class="form-label"><strong>Tipo de Documento</strong></label>
                <select wire:model="param_tipodoc" class="form-select">
                    <option value="">-- Seleccione --</option>
                    @foreach ($p_tpdocs as $p_tpdoc)
                        <option value="{{$p_tpdoc->id}}">{{$p_tpdoc->detalle}}</option>
                    @endforeach
                </select>
                @error('param_tipodoc')
                <span class="text-sm text-danger">{{$message}}</span>
                @enderror
            </div>--}}
        </div>
    </x-slot>
    <x-slot name="footer">
        <div class="col-12 text-center">
            {{--<x-jet-button wire:click.prevent="store()" class="btn btn-primary me-1 mt-2">
                Guardar
            </x-jet-button>
            <x-jet-button wire:click="cancel()" class="btn btn-danger mt-2">
                Cancelar
            </x-jet-button>--}}
        </div>

    </x-slot>
</x-jet-dialog-modal>
