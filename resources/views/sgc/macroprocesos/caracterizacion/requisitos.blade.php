<tr>
    <th rowspan="{{ $macroproceso->longitud_requisitos }}">4. REQUISITOS {{ $macroproceso->longitud_requisitos }}</th>
    <th rowspan="{{ $macroproceso->clientes->count() }}">Del cliente: {{ $macroproceso->clientes->count() }}</th>
    <td colspan="10">{{ $macroproceso->clientes->first()?$macroproceso->clientes->first()->descripcion:'' }}</td>
</tr>
@foreach ($macroproceso->clientes as $cliente)
    @if (!$loop->first)
        <tr>
            <td colspan="10">{{ $cliente->descripcion }}</td>
        </tr>
    @endif
@endforeach
<tr>
    <th rowspan="{{ $macroproceso->isos->count() }}">Norma ISO: {{ $macroproceso->isos->count() }}</th>
    <td colspan="10">{{ $macroproceso->isos->first()?$macroproceso->isos->first()->descripcion:'' }}</td>
</tr>
@foreach ($macroproceso->isos as $iso)
    @if (!$loop->first)
        <tr>
            <td colspan="10">{{ $iso->descripcion }}</td>
        </tr>
    @endif
@endforeach
<tr>
    <th rowspan="{{ $macroproceso->legales->count() }}">Legales: {{ $macroproceso->legales->count() }}</th>
    <td colspan="10">{{ $macroproceso->legales->first()?$macroproceso->legales->first()->descripcion:'' }}</td>
</tr>
@foreach ($macroproceso->legales as $legal)
    @if (!$loop->first)
        <tr>
            <td colspan="10">{{ $legal->descripcion }}</td>
        </tr>
    @endif
@endforeach
