<table class="table table-bordered" style="width: 100% !important">
    <tr>
        <td rowspan="{{ $longitud_total }}"></td>
        <td colspan="12"></td>
        <td rowspan="{{ $longitud_total }}"></td>
    </tr>
    <tr>
        <th rowspan="2"></th>
        <th colspan="9">SISTEMA DE GESTIÓN DE CALIDAD (SGC-UNASAM)</th>
        <th rowspan="2" colspan="2"></th>
    </tr>
    <tr>
        <th colspan="9">FICHA DE CARACTERIZACIÓN - NIVEL {{$macroproceso->nivel}} </th>
    </tr>
    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <th>NOMBRE</th>
        <td colspan="2"> {{ $macroproceso->nombre_completo }}</td>
        <th>TIPO</th>
        <td>{{ $macroproceso->detalle->detalle }}</td>
        <th>CÓDIGO</th>
        <td colspan="2">FC - {{ $macroproceso->codigo }}</td>
        <th>VERSIÓN</th>
        <td>{{ $macroproceso->version->numero }}</td>
        <th>FECHA APROBACIÓN</th>
        <td>{{ $macroproceso->version->fecha_vigencia->format('d/m/Y') }}</td>
    </tr>
    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <th>1. OBJETIVO</th>
        <td colspan="11">{{ $macroproceso->objetivo }}</td>
    </tr>
    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <th>2. ALCANCE</th>
        <th>Empieza: </th>
        <td colspan="5">{{ $macroproceso->hijos->first() ? $macroproceso->hijos->first()->nombre_completo : '' }}</td>
        <th>Termina: </th>
        <td colspan="4">{{ $macroproceso->hijos->last() ? $macroproceso->hijos->last()->nombre_completo : '' }}</td>
    </tr>
    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <th colspan="2">3. LÍDER DEL MACROPROCESO</th>
        <td colspan="10">{{ $macroproceso->area->nombre }}</td>
    </tr>
    <tr>
        <td colspan="12"></td>
    </tr>
    @if ($macroproceso->longitud_requisitos === 3)
        <tr>
            <th rowspan="3">4. REQUISITOS</th>
            <th>Del cliente: </th>
            <td colspan="10">Según lo estipulado en el ítem 4.1 del manual de {{ $macroproceso->nombre_completo }}</td>
        </tr>
        <tr>
            <th>Norma ISO: </th>
            <td colspan="10">Según lo estipulado en el ítem 4.2 del manual de {{ $macroproceso->nombre_completo }}</td>
        </tr>
        <tr>
            <th>Legales: </th>
            <td colspan="10">Según lo estipulado en el ítem 4.3 del manual de {{ $macroproceso->nombre_completo }}</td>
        </tr>
    @else
        @include('sgc.macroprocesos.caracterizacion.requisitos')
    @endif
    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <th rowspan="2" colspan="2">5. MACROPROCESOS DIRECTAMENTE VINCULADOS</th>
        <td colspan="2">{{ isset($macroproceso->vinculacions[0]) ? $macroproceso->vinculacions[0]->padre->nombre_completo : '' }}</td>
        <td colspan="3">{{ isset($macroproceso->vinculacions[1]) ? $macroproceso->vinculacions[1]->padre->nombre_completo : '' }}</td>
        <td colspan="2">{{ isset($macroproceso->vinculacions[2]) ? $macroproceso->vinculacions[2]->padre->nombre_completo : '' }}</td>
        <td colspan="3">{{ isset($macroproceso->vinculacions[3]) ? $macroproceso->vinculacions[3]->padre->nombre_completo : '' }}</td>
    </tr>
    <tr>

        <td colspan="2">{{ isset($macroproceso->vinculacions[4]) ? $macroproceso->vinculacions[4]->padre->nombre_completo : '' }}</td>
        <td colspan="3">{{ isset($macroproceso->vinculacions[5]) ? $macroproceso->vinculacions[5]->padre->nombre_completo : '' }}</td>
        <td colspan="2">{{ isset($macroproceso->vinculacions[6]) ? $macroproceso->vinculacions[6]->padre->nombre_completo : '' }}</td>
        <td colspan="3">{{ isset($macroproceso->vinculacions[7]) ? $macroproceso->vinculacions[7]->padre->nombre_completo : '' }}</td>
    </tr>
    <tr>
        <th colspan="12">6. DESCRIPCIÓN</th>
    </tr>
    <tr>
        <td colspan="12">
            Según lo estipulado en el ítem 7 del manual de {{ $macroproceso->nombre_completo }}
        </td>
    </tr>
    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <th>PROVEEDOR</th>
        <th colspan="2">ENTRADA</th>
        <th colspan="5">PROCESOS</th>
        <th colspan="2">SALIDA</th>
        <th colspan="2">CLIENTE</th>
    </tr>
    @foreach ($macroproceso->subentradas as $entrada)
        <tr>
            <td>{{ $entrada->interesado->nombre }}</td>
            <td colspan="2">{{ $entrada->documento->nombreReferencial }}</td>
            <td colspan="5">{{ $entrada->proceso->nombre_completo }}</td>
            <td colspan="2">{{ $entrada->entrada->documento->nombreReferencial }}</td>
            <td colspan="2">{{ $entrada->entrada->interesado->nombre }}</td>
        </tr>
    @endforeach
    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <th colspan="12">7. RECURSOS</th>
    </tr>
    <tr>
        <th colspan="4">Talento humano</th>
        <th>Otros </th>
        <th colspan="4">Infraestructura</th>
        <th colspan="3">Software y comunicaciones</th>
    </tr>
    @for ($i = 0; $i < $macroproceso->longitud_recursos; $i++)
        @php
            $j2 = ceil($macroproceso->talentos_humanos->count() / 2) + $i;
            $k2 = ceil($macroproceso->infraestructuras->count() / 2) + $i;
            $l2 = ceil($macroproceso->softwares->count() / 2) + $i;

            $j1 = $i < ceil($macroproceso->talentos_humanos->count() / 2);
            $k1 = $i < ceil($macroproceso->infraestructuras->count() / 2);
            $l1 = $i < ceil($macroproceso->softwares->count() / 2);

        @endphp
        <tr>
            <td colspan="2">{{ $j1 ? (isset($macroproceso->talentos_humanos[$i]) ? $macroproceso->talentos_humanos[$i]->nombre : '') : '' }}</td>
            <td colspan="2">{{ isset($macroproceso->talentos_humanos[$j2]) ? $macroproceso->talentos_humanos[$j2]->nombre : '' }}</td>
            <td>{{ isset($macroproceso->otros_recursos[$i]) ? $macroproceso->otros_recursos[$i]->nombre : '' }}</td>
            <td colspan="2">{{ $k1 ? (isset($macroproceso->infraestructuras[$i]) ? $macroproceso->infraestructuras[$i]->nombre : '') : '' }}</td>
            <td colspan="2">{{ isset($macroproceso->infraestructuras[$k2]) ? $macroproceso->infraestructuras[$k2]->nombre : '' }}</td>
            <td>{{ $l1 ? (isset($macroproceso->softwares[$i]) ? $macroproceso->softwares[$i]->nombre : '') : '' }}</td>
            <td colspan="2">{{ isset($macroproceso->softwares[$l2]) ? $macroproceso->softwares[$l2]->nombre : '' }}</td>
        </tr>
    @endfor
    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <th colspan="5">8. INDICADORES (KPI's)</th>
        <th colspan="7">9. RIESGOS</th>
    </tr>
    <tr>
        <th>EFICIENCIA</th>
        <td colspan="4">No aplica</td>
        <td colspan="7"></td>
    </tr>
    <tr>
        <th>EFICACIA</th>
        <td colspan="4">No aplica</td>
        <td colspan="7"></td>
    </tr>
    <tr>
        <th>EFECTIVIDAD</th>
        <td colspan="4">No aplica</td>
        <td colspan="7"></td>
    </tr>
    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <th colspan="12">10. CONTROL DE CAMBIOS</th>
    </tr>

    <tr>
        <th colspan="2">VERSIÓN</th>
        <th colspan="2">FECHA</th>
        <th colspan="8">DESCRIPCIÓN DEL CAMBIO</th>
    </tr>

    @if ($macroproceso->versiones->count() > 0)
        @foreach ($macroproceso->versiones as $version)
            <tr>
                <td colspan="2">{{ $version->version->numero }}</td>
                <td colspan="2">{{ $version->version->fecha_vigencia->format('d/m/Y') }}</td>
                <td colspan="8">{{ $version->version->descripcion }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="2">{{ $macroproceso->version->numero }}</td>
            <td>{{ $macroproceso->version->fecha_vigencia->format('d/m/Y') }}</td>
            <td colspan="4">{{ $macroproceso->version->descripcion }}</td>
        </tr>
    @endif
    <tr>
        <th colspan="12">11. APROBACIÓN</th>
    </tr>
    <tr>
        <th colspan="2">ACCIÓN</th>
        <th colspan="3">NOMBRE</th>
        <th colspan="4">CARGO</th>
        <th colspan="2">FIRMA</th>
        <th>FECHA</th>
    </tr>
    <tr>
        <td colspan="2">ELABORADO POR:</td>
        <td colspan="3"></td>
        <td colspan="4"></td>
        <td colspan="2"></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2">REVISADO POR:</td>
        <td colspan="3"></td>
        <td colspan="4"></td>
        <td colspan="2"></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2">APROBADO POR:</td>
        <td colspan="3"></td>
        <td colspan="4"></td>
        <td colspan="2"></td>
        <td></td>
    </tr>
    <tr>
        <th colspan="12">SGC - UNASAM</th>
    </tr>
</table>
