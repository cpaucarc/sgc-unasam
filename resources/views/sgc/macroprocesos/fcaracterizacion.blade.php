
@extends('layouts/contentLayoutMaster')

@section('title','Macroprocesos')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection
@section('page-style')
    {{-- Page css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/dashboard-ecommerce.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection

@section('content')
    <!-- Dashboard Ecommerce Starts -->

    <section id="macroprocesos">
        <div class="card">
            <br>
            <table>
                <tr>
                    <td colspan="3">
                        <div class="col-12" style="padding-left: 20px">
                            <strong class="text-secondary">PM07: INVESTIGACIÓN - DESARROLLO TECNOLÓGICO - INNOVACIÓN</strong>
                        </div>
                    </td>
                    <td style="width: 140px;">
                        <x-jet-button class="btn btn-primary btn-sm mt-50">
                            <span>Regresar</span>
                        </x-jet-button>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <div class="col-12" style="padding-left: 20px">
                            <strong class="text-primary">CONTROL DE CAMBIOS DE LA FICHA DE CARACTERIZACIÓN DE PROCESO</strong>
                        </div>
                    </td>
                    <td style="width: 230px;">
                        <x-jet-button class="btn btn-primary btn-sm mt-50">
                            <span>Ficha de Caracterización</span>
                        </x-jet-button>
                    </td>
                    <td style="width: 200px;">
                        <x-jet-button class="btn btn-primary btn-sm mt-50">
                            <span>Ficha de Indicadores</span>
                        </x-jet-button>
                    </td>
                </tr>
            </table>
            <br>
            <table >
                <tr>
                    <td>
                        <div class="col-12" style="padding-left: 20px">
                            <table style="width: 95%; border: 2px solid #636e72;  border-collapse: collapse;" >
                                <tr style="border: 1px solid black;  border-collapse: collapse;">
                                    <td style="background-color: #dfe6e9; width: 40%; border: 2px solid #636e72;  border-collapse: collapse;">
                                        <div class="col-12 text-center">NOMBRE MACROPROCESO</div>
                                    </td>
                                    <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                        <input class="inputcentrado" style="width:100%;border-style: hidden;" type="text" name="name" value="FC-PM07" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                        <div class="col-12 text-center">TIPO</div>
                                    </td>
                                    <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                        <input class="inputcentrado" style="width:100%;border-style: hidden;" type="text" name="name" value="FC-PM07" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td style="width: 430px;">
                        <table style="width: 95%; border: 2px solid #636e72;  border-collapse: collapse;" >
                            <tr style="border: 1px solid black;  border-collapse: collapse;">
                                <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    <div class="col-12">CÓDIGO</div>
                                </td>
                                <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                    <input class="inputcentrado" style="width:100%;border-style: hidden;" type="text" name="name" value="FC-PM07" />
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">VERSIÓN</td>
                                <td>
                                    <input class="inputcentrado" style="width:100%;border-style: hidden;" type="text" name="name" value="2.5" />
                                </td>
                                <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">VIGENCIA</td>
                                <td>
                                    <input class="inputcentrado" style="width:100%;border-style: hidden;" type="text" name="name" value="22/12/2021" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <hr>

            <table>
                <tr>
                    <td>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <p class="text-secondary"><strong>CONTROL DE CAMBIOS</strong></p>
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">Versión</th>
                                                    <th class="text-center">Fecha</th>
                                                    <th class="text-center">Descripción de Cambio</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td class="text-center">1.0</td>
                                                    <td class="text-center">02/05/2020</td>
                                                    <td>Se realizo el cambio en los objetivos de los procesos.</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center">2.0</td>
                                                    <td class="text-center">02/05/2020</td>
                                                    <td>Se realizo el cambio en los objetivos de los procesos.</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center">3.0</td>
                                                    <td class="text-center">02/05/2020</td>
                                                    <td>Se realizo el cambio en los objetivos de los procesos.</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <hr>
            <br>
        </div>
        {{--<h3>Procesos de {{$macroproceso->nombre}}</h3>--}}
        <div class="row">

        </div>
    </section>
    <!-- Dashboard Ecommerce ends -->
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/pages/dashboard-ecommerce.js')) }}"></script>
@endsection

<style>
    #combo{
        font-family: Tahoma, Verdana, Arial;
        font-size: 11px;
        color: #707070;
        background-color: #FFFFFF;
        border-width:0;
    }
    input:focus,
    select:focus,
    textarea:focus,
    button:focus {
        outline: none;
    }
    .inputcentrado {
        text-align: center
    }
</style>
