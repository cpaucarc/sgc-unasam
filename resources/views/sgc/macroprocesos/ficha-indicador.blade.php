@extends('layouts/contentLayoutMaster')

@section('title','Macroprocesos')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection
@section('page-style')
    {{-- Page css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/dashboard-ecommerce.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection

@section('content')
    <!-- Dashboard Ecommerce Starts -->

    <section id="macroprocesos">
        <div class="card">
            <br>
            <table>
                <tr>
                    <td colspan="3">
                        <div class="col-12" style="padding-left: 20px">
                            <strong class="text-secondary">PM07: INVESTIGACIÓN - DESARROLLO TECNOLÓGICO - INNOVACIÓN</strong>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <table>
                <tr>
                    <td>
                        <div class="col-12" style="padding-left: 20px">
                            <br>
                            <p><strong class="text-primary">FICHA DE INDICADORES (KPI'S) DE PROCESO - NIVEL 1</strong></p>
                        </div>
                    </td>
                    <td style="width: 230px;">
                        <x-jet-button class="btn btn-primary btn-sm mt-50">
                            <span>Ficha de Caracterización</span>
                        </x-jet-button>
                    </td>
                </tr>
            </table>


            <table>
                <tr>
                    <td>
                        <div class="col-12" style="padding-left: 20px">
                            <table style="width: 95%; border: 2px solid #636e72;  border-collapse: collapse;">
                                <tr style="border: 1px solid black;  border-collapse: collapse;">
                                    <td style="background-color: #dfe6e9; width: 40%; border: 2px solid #636e72;  border-collapse: collapse;">
                                        <div class="col-12 text-center">NOMBRE MACROPROCESO</div>
                                    </td>
                                    <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                        PM07: Investigación - Desarrollo - Tecnologia - Innovación
                                    </td>
                                </tr>
                                <tr style="border: 1px solid black;  border-collapse: collapse;">
                                    <td style="background-color: #dfe6e9; width: 40%; border: 2px solid #636e72;  border-collapse: collapse;">
                                        <div class="col-12 text-center">NOMBRE DEL INDICADOR</div>
                                    </td>
                                    <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                        Eficacia en la gestión de I+D+I
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                        <div class="col-12 text-center">TIPO</div>
                                    </td>
                                    <td colspan="3" style="border: 2px solid #636e72;  border-collaps: collapse;">
                                        Eficacia
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td style="width: 430px;">
                        <table style="width: 95%; border: 2px solid #636e72;  border-collapse: collapse;">
                            <tr style="border: 1px solid black;  border-collapse: collapse;">
                                <td style="background-color: #dfe6e9; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    <div class="col-12">CÓDIGO</div>
                                </td>
                                <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                    <input class="inputcentrado" style="width:100%;border-style: hidden;" type="text"
                                           name="name" value="FC-PM07"/>
                                </td>
                            </tr>
                            <tr style="border: 1px solid black;  border-collapse: collapse;">
                                <td style="background-color: #dfe6e9; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    VERSIÓN
                                </td>
                                <td class="inputcentrado" style="border: 2px solid #636e72;  border-collapse: collapse;">2.5</td>
                                <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    VIGENCIA
                                </td>
                                <td class="inputcentrado" style="width: 30%; border: 2px solid #636e72;  border-collapse: collapse;">22/12/2021</td>
                            </tr>

                            <tr style="border: 1px solid black;  border-collapse: collapse;">
                                <td style="background-color: #dfe6e9; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    PERIODO
                                </td>
                                <td class="inputcentrado">2021</td>
                                <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    PERIODO
                                </td>
                                <td class="inputcentrado" style="width: 30%">2021</td>
                            </tr>

                        </table>
                    </td>
                </tr>
            </table>
            <hr>








            <div class="row py-1" style="padding-left: 20px">
                <div class="col-6">
                    <h4 class="text-secondary"><strong>DEFINICIÓN:</strong></h4>
                    <table style="width: 98%; border: 2px solid #d3dadd;  border-collapse: collapse; padding-left: 20px">
                        <tr>
                            <td>
                                <div class="col-12 p-1">
                                    <input wire:model="definicion" class="inputcentrado" style="width:100%;border-style: hidden;" type="text"
                                           name="name" value="{{$ficha->definicion}}"/>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-6">
                    <h4 class="text-secondary"><strong>OBJETIVO DEL INDICADOR:</strong></h4>
                    <table style="width: 98%; border: 2px solid #d3dadd;  border-collapse: collapse; padding-left: 20px">
                        <tr>
                            <td>

                                <div class="col-12 p-1">
                                    <input wire:model="objetivo" class="inputcentrado" style="width:100%;border-style: hidden;" type="text"
                                           name="name" value="{{$ficha->objetivo}}"/>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>

            <hr>
            <div class="col-12" style="padding-left: 20px; padding-right: 20px">
                <div class="row">
                    <div class="col-6">
                        <h4 class="text-secondary"><strong>RESPONSABLES DE LA INFORMACIÓN:</strong></h4>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="areas-tab" data-bs-toggle="tab" href="#areas"
                                               aria-controls="home" role="tab" aria-selected="true" >Áreas</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="procesos-tab" data-bs-toggle="tab" href="#procesos" aria-controls="profile"
                                               role="tab" aria-selected="false">Procesos</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="responsables-tab" data-bs-toggle="tab" href="#responsables"
                                               aria-controls="about" role="tab" aria-selected="false">Responsables</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="usuarios-tab" data-bs-toggle="tab" href="#usuarios"
                                               aria-controls="about" role="tab" aria-selected="false">Usuarios</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="areas" aria-labelledby="areas-tab" role="tabpanel">
                                            <div class="table-responsive">
                                                <table class="table table-bordered" style="width: 100%">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 100px" class="text-center">Nro. </th>
                                                        <th class="text-center">ÁREAS QUE SUMINISTRAN INFORMACIÓN</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($suministro_area as $sarea)
                                                        <tr class="odd">
                                                            <td class="text-center">{{ $loop->iteration }}</td>
                                                            <td>{{$sarea->abreviatura}}: {{$sarea->nombre}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="procesos" aria-labelledby="procesos-tab" role="tabpanel">
                                            <div class="table-responsive">
                                                <table class="table table-bordered" style="width: 100%">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 100px" class="text-center">Nro. </th>
                                                        <th class="text-center">PROCESOS QUE SUMINISTRAN INFORMACIÓN</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($suministro_proceso as $sproceso)

                                                        <tr class="odd">
                                                            <td class="text-center">{{ $loop->iteration }}</td>
                                                            <td>{{$sproceso->codigo}}: {{$sproceso->nombre}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="responsables" aria-labelledby="responsables-tab" role="tabpanel">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered" style="width: 100%">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center">Responsable del Cálculo </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td class="text-center">
                                                                <div class="col-12 col-md-12">
                                                                    <select wire:model="responsable_calculo" class="form-select">
                                                                        <option value="">-- Seleccione --</option>
                                                                        @foreach ($responsables as $resCaculo)
                                                                            <option value="{{$resCaculo->id}}">{{$resCaculo->nombre}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @error('responsable_calculo')
                                                                    <span class="text-sm text-danger">{{$message}}</span>
                                                                    @enderror
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <br>
                                                    <table class="table table-bordered" style="width: 100%">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center">Responsable del Análisis y Toma de Deciciones</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td class="text-center">
                                                                <div class="col-12 col-md-12">
                                                                    <select wire:model="responsable_analisis" class="form-select">
                                                                        <option value="">-- Seleccione --</option>
                                                                        @foreach ($responsables_analisis as $resAnalisis)
                                                                            <option value="{{$resAnalisis->id}}">{{$resAnalisis->nombre}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @error('responsable_analisis')
                                                                    <span class="text-sm text-danger">{{$message}}</span>
                                                                    @enderror
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="usuarios" aria-labelledby="usuarios-tab" role="tabpanel">

                                            <div class="table-responsive">
                                                <table class="table table-bordered" style="width: 100%">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 100px" class="text-center">Nro. </th>
                                                        <th class="text-center">USUARIOS</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($responsable_usuarios as $resUsario)

                                                        <tr class="odd">
                                                            <td class="text-center">{{ $loop->iteration }}</td>
                                                            <td>{{$resUsario->nombre}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <h4 class="text-secondary"><strong>RESTRICCIONES LIMITACIONES RIESGO:</strong></h4>
                        <div class="table-responsive">
                            <table class="table table-bordered" style="width: 100%">
                                <thead>
                                <tr>
                                    <th style="width: 100px" class="text-center">Nro. </th>
                                    <th class="text-center">USUARIOS</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($restricciones as $restriccion)

                                    <tr class="odd">
                                        <td class="text-center">{{ $loop->iteration }}</td>
                                        <td>{{$restriccion->nombre}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-6">
                        <table class="table table-bordered" style="width: 100%">
                            <thead>
                            <tr>
                                <th colspan="4" class="text-center">FORMULA (NUMERADOR/DENOMINADOR)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>

                                <td rowspan="2">
                                    <div class="row text-center">
                                        Eficacia en la gestión de I+D+I
                                    </div>
                                </td>
                                <td colspan="2">
                                    <table class="table table-bordered" style="width: 100%">
                                        <thead>
                                        <tr>
                                            <th class="text-center">Numerador</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <input class="inputcentrado" style="width:100%;border-style: hidden;" type="number"
                                                       name="name" placeholder="Valor Numerador"/>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td rowspan="2">
                                    <input class="inputcentrado" style="width:100%;border-style: hidden;" type="number"
                                           name="name" placeholder="Resultado"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table class="table table-bordered" style="width: 100%">
                                        <thead>
                                        <tr>
                                            <th class="text-center">Denominador</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <input class="inputcentrado" style="width:100%;border-style: hidden;" type="number"
                                                       name="name" placeholder="Valor Denominador" />

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-6">
                        <table class="table table-bordered" style="width: 100%">

                            <tr style="border: 1px solid black;  border-collapse: collapse;">
                                <td style="background-color: #dfe6e9; width: 220px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    Unidad de Medida
                                </td>
                                <td class="inputcentrado">
                                    <div class="col-12 col-md-12">
                                        <select wire:model="responsable_calculo" class="form-select">
                                            <option value="">-- Seleccione --</option>
                                            @foreach ($mediciones as $medicion)
                                                <option value="{{$medicion->indicador_id}}">{{$medicion->paraUnidadMedida}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr style="border: 1px solid black;  border-collapse: collapse;">
                                <td style="background-color: #dfe6e9; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    Meta
                                </td>
                                <td class="inputcentrado">

                                    <div class="col-12 col-md-12">
                                        <select wire:model="responsable_calculo" class="form-select">
                                            <option value="">-- Seleccione --</option>
                                            @foreach ($metas as $meta)
                                                <option value="{{$meta->id}}">{{$meta->meta}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr style="border: 1px solid black;  border-collapse: collapse;">
                                <td style="background-color: #dfe6e9; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    Frecuencia de Medición
                                </td>
                                <td class="inputcentrado">
                                    <div class="col-12 col-md-12">
                                        <select wire:model="responsable_calculo" class="form-select">
                                            <option value="">-- Seleccione --</option>
                                            @foreach ($mediciones as $medicion)
                                                <option value="{{$medicion->indicador_id}}">{{$medicion->paraFrecMedicion}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                            </tr>

                            <tr style="border: 1px solid black;  border-collapse: collapse;">
                                <td style="background-color: #dfe6e9; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    Frecuencia Reporte
                                </td>
                                <td class="inputcentrado">
                                    <div class="col-12 col-md-12">
                                        <select wire:model="responsable_calculo" class="form-select">
                                            <option value="">-- Seleccione --</option>
                                            @foreach ($mediciones as $medicion)
                                                <option value="{{$medicion->indicador_id}}">{{$medicion->paraFrecReporte}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr style="border: 1px solid black;  border-collapse: collapse;">
                                <td style="background-color: #dfe6e9; width: 85px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    Frecuencia Revisión
                                </td>
                                <td class="inputcentrado">
                                    <div class="col-12 col-md-12">
                                        <select wire:model="responsable_calculo" class="form-select">
                                            <option value="">-- Seleccione --</option>
                                            @foreach ($mediciones as $medicion)
                                                <option value="{{$medicion->indicador_id}}">{{$medicion->paraFrecRevision}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>






            <br>
            <hr>
            <div class="col-12" style="padding-left: 20px; padding-right: 20px">
                <div class="row">
                    <div class="col-6">

                        <div class="card">
                            <div class="card-header pt-0">
                                <h4 class="card-title text-primary"><strong>ESCALAS DE VALORACIÓN:</strong></h4>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li>
                                            {{--<button type="button" class="btn btn-outline-primary waves-effect" wire:click="$set('open',true)">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                                                <span>Agregar</span>
                                            </button>--}}
                                            <div class="dt-buttons btn-group flex-wrap">
                                                <x-jet-button wire:click="$set('open',true)" class="btn add-new btn-primary mt-50">
                                                    <span>Agregar Año</span>
                                                </x-jet-button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div>
                                    {{--@include('sgc.macroprocesos.createFicha')--}}
                                    {{--@include('livewire.anios.update')--}}
                                </div>

                            </div>
                            <div class="card-body>">
                                <div class="table-responsive">
                                    <table class="table table-bordered" style="width: 100%">
                                        <thead>
                                        <tr>
                                            <th class="text-center">Likert</th>
                                            <th class="text-center">Valor'Inf.</th>
                                            <th class="text-center">Valor-sup</th>
                                            <th class="text-center">Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="text-center">Muy malo</td>
                                            <td>0.00</td>
                                            <td>0.24</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">Malo</td>
                                            <td>0.24</td>
                                            <td>0.48</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">Regular</td>
                                            <td>0.48</td>
                                            <td>0.72</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">Satisfactorio</td>
                                            <td>0.72</td>
                                            <td>0.78</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">Muy Satisfactorio</td>
                                            <td>0.78</td>
                                            <td>0.80</td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>





            <table>
                <tr>
                    <td>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <p class="text-secondary"><strong>RESULTADOS DE LA MEDICIÓN</strong></p>
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">Periodo</th>
                                                    <th class="text-center">Valor-Numerador</th>
                                                    <th class="text-center">Valor-Denominador</th>
                                                    <th class="text-center">Valor-Indicador</th>
                                                    <th class="text-center">Meta</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td class="text-center">Año 2020</td>
                                                    <td class="text-center"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center">Año 2021</td>
                                                    <td class="text-center"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"></td>
                                                    <td class="text-center"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <hr>
            <br>
        </div>
        {{--<h3>Procesos de {{$macroproceso->nombre}}</h3>--}}
        <div class="row">

        </div>
    </section>
    <!-- Dashboard Ecommerce ends -->
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/pages/dashboard-ecommerce.js')) }}"></script>
@endsection

<style>
    #combo {
        font-family: Tahoma, Verdana, Arial;
        font-size: 11px;
        color: #707070;
        background-color: #FFFFFF;
        border-width: 0;
    }

    input:focus,
    select:focus,
    textarea:focus,
    button:focus {
        outline: none;
    }

    .inputcentrado {
        text-align: center
    }

    table{
        width:100%;
        table-layout: fixed;
        overflow-wrap: break-word;
    }
</style>
