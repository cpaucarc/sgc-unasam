@extends('layouts/contentLayoutMaster')

@section('title','Macroprocesos')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
@section('page-style')
    {{-- Page css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <style>
        .border-black {
            border-color: rgb(77, 74, 74);
        }
    </style>
@endsection

@section('content')

<section id="macroprocesos">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-primary">{{$macroproceso->codigo}}: {{$macroproceso->nombre}}</h4>
            <div class="heading-elements">
                <ul class="list-inline">
                    <li>
                      <a href="{{route('macroprocesos.show',$macroproceso->id)}}" class="btn btn-gradient-primary">Regresar</a>
                    </li>
                    <li>
                        <a data-action="collapse"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg></a>
                    </li>
                    <li>
                        <a data-action="reload"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-rotate-cw"><polyline points="23 4 23 10 17 10"></polyline><path d="M20.49 15a9 9 0 1 1-2.12-9.36L23 10"></path></svg></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row px-2">
            <div class="col-12">
                <h4 class="text-primary"><strong>CONTROL DE CAMBIOS DE LA FICHA DE CARACTERIZACIÓN DE PROCESO</strong></h4>
            </div>
            <div class="row">
                <div class="col-12 col-md-7">
                    <div class="table-responsive">
                        <table class="table" style="border: 2px solid #636e72;  border-collapse: collapse;">
                            <tr style="border: 1px solid black;  border-collapse: collapse;">
                                <td class="py-0 px-1" class="text-center" style="background-color: #dfe6e9;border: 2px solid #636e72;  border-collapse: collapse; width: 230px;">
                                    NOMBRE DEL MACROPROCESO
                                </td>
                                <td class="py-0 px-1" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                    {{$macroproceso->nombre}}
                                </td>
                            </tr>
                            <tr>
                                <td class="py-0 px-1" class="text-center" style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    TIPO
                                </td>
                                <td class="py-0 px-1" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                    {{$macroproceso->detalle->detalle}}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
               @livewire('caracterizacion.versiones', ['macroproceso' => $macroproceso])
            </div>
        </div>
        <hr>
        <div class="row px-2">
            <div class="col-12">
                <h4 class="text-primary"><strong>OBJETIVO:</strong></h4>
                <table style="width: 98%; border: 2px solid #d3dadd;  border-collapse: collapse; padding-left: 15px">
                    <tr>
                        <td>
                            <div class="col-12 p-1">
                                <p class="mb-0"> {{$macroproceso->objetivo}} </p>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <hr>
        <div class="row px-2">
            <div class="col-12">
                <h4 class="text-primary"><strong>ALCANCE:</strong></h4>
            </div>
            @php
                $inicia = $macroproceso->procesos()->orderBy('orden','asc')->first();
                $termina = $macroproceso->procesos()->orderBy('orden','desc')->first();
            @endphp
            <div class="col-12 col-md-6">
                <h4><strong>Inicia:</strong> {{$inicia->codigo}} {{$inicia->nombre}}</h4>
            </div>
            <div class="col-12 col-md-6">
                <h4><strong>Termina:</strong> {{$termina->codigo}} {{$termina->nombre}}</h4>
            </div>
        </div>
        <hr>
        <div class="row px-2">
            <div class="col-12">
                <h4 class="text-primary"><strong>LÍDER DEL MACROPROCESO:</strong> <strong class="text-secondary">{{$macroproceso->area->nombre}}</strong></h4>
            </div>
        </div>
        <hr>
        <div class="row px-2">
            <div class="col-12 col-md-6">
                <h4 class="text-primary"><strong>REQUISITOS:</strong></h4>
                @livewire('caracterizacion.requisitos',['nivel_proceso'=>15001,'id_proceso'=>$macroproceso->id, 'area' => $macroproceso->area])
            </div>
            <div class="col-12 col-md-6">
                @livewire('caracterizacion.vinculacions',['nivel_proceso'=>15001,'id_proceso'=>$macroproceso->id, 'area' => $macroproceso->area])
            </div>
        </div>
        <hr>
        <div class="row px-2">
            @livewire('caracterizacion.entradas',['nivel_proceso'=>15002,'id_proceso'=>$macroproceso->id, 'area' => $macroproceso->area])
        </div>
        <hr>
        <div class="row px-2">
            <div class="col-12 col-md-6">
                <h4 class="text-primary"><strong>RECURSOS</strong></h4>
                @livewire('caracterizacion.recursos',['nivel_proceso'=>15001,'id_proceso'=>$macroproceso->id, 'area' => $macroproceso->area])
            </div>
            <div class="col-12 col-md-6">
                @livewire('caracterizacion.riesgos',['nivel_proceso'=>15001,'id_proceso'=>$macroproceso->id, 'area' => $macroproceso->area])
            </div>
        </div>
        <hr>
        <div class="row px-2">
            <div class="col-12 col-md-6">
                @livewire('caracterizacion.indicadors',['nivel_proceso'=>15001,'id_proceso'=>$macroproceso->id])
            </div>
        </div>
    </div>
</section>

@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/codigo-script.js')) }}"></script>
@endsection
@section('page-script')
    <script>
        function lanzarScripts(){
            scriptVinculacion();
            scriptRequisito();
            scriptRiesgo();
            scriptEntrada();
            scriptRecurso();
        }
        window.onload = lanzarScripts;
    </script>
@endsection
