@extends('layouts/contentLayoutMaster')

@section('title','Macroprocesos')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection
@section('page-style')
    {{-- Page css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/dashboard-ecommerce.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection

@section('content')
    <!-- Dashboard Ecommerce Starts -->

    <section id="macroprocesos">
        <div class="card">
            <br>
            <table>
                <tr>
                    <td colspan="3">
                        <div class="col-12" style="padding-left: 20px">
                            <strong class="text-secondary">PM07: INVESTIGACIÓN - DESARROLLO TECNOLÓGICO -
                                INNOVACIÓN</strong>
                        </div>
                    </td>
                    <td style="width: 140px;">
                        <x-jet-button class="btn btn-primary btn-sm mt-50">
                            <span>Regresar</span>
                        </x-jet-button>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <div class="col-12" style="padding-left: 20px">
                            <strong class="text-primary">CONTROL DE CAMBIOS DE LA FICHA DE CARACTERIZACIÓN DE
                                PROCESO</strong>
                        </div>
                    </td>
                    <td style="width: 230px;">
                        <x-jet-button class="btn btn-primary btn-sm mt-50">
                            <span>Ficha de Caracterización</span>
                        </x-jet-button>
                    </td>
                    <td style="width: 200px;">
                        <x-jet-button class="btn btn-primary btn-sm mt-50">
                            <span>Ficha de Indicadores</span>
                        </x-jet-button>
                    </td>
                </tr>
            </table>
            <br>
            <table>
                <tr>
                    <td>
                        <div class="col-12" style="padding-left: 20px">
                            <table style="width: 95%; border: 2px solid #636e72;  border-collapse: collapse;">
                                <tr style="border: 1px solid black;  border-collapse: collapse;">
                                    <td style="background-color: #dfe6e9; width: 40%; border: 2px solid #636e72;  border-collapse: collapse;">
                                        <div class="col-12 text-center">NOMBRE MACROPROCESO</div>
                                    </td>
                                    <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                        <input class="inputcentrado" style="width:100%;border-style: hidden;"
                                               type="text" name="name" value="FC-PM07"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                        <div class="col-12 text-center">TIPO</div>
                                    </td>
                                    <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                        <input class="inputcentrado" style="width:100%;border-style: hidden;"
                                               type="text" name="name" value="FC-PM07"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td style="width: 430px;">
                        <table style="width: 95%; border: 2px solid #636e72;  border-collapse: collapse;">
                            <tr style="border: 1px solid black;  border-collapse: collapse;">
                                <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    <div class="col-12">CÓDIGO</div>
                                </td>
                                <td colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                    <input class="inputcentrado" style="width:100%;border-style: hidden;" type="text"
                                           name="name" value="FC-PM07"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    VERSIÓN
                                </td>
                                <td class="inputcentrado">2.5</td>
                                <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    VIGENCIA
                                </td>
                                <td class="inputcentrado" style="width: 30%">22/12/2021</td>
                            </tr>

                            <tr style="border: 1px solid black;  border-collapse: collapse;">
                                <td style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    <div class="col-12">CÓDIGO</div>
                                </td>
                                <td class="inputcentrado" colspan="3"
                                    style="border: 2px solid #636e72;  border-collapse: collapse;">2.5
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
            </table>
            <hr>

            <div class="col-12" style="padding-left: 20px">
                <h4 class="text-secondary"><strong>OBJETIVO:</strong></h4>
                <table style="width: 98%; border: 2px solid #d3dadd;  border-collapse: collapse; padding-left: 20px">
                    <tr>
                        <td>
                            <div class="col-12 p-1">
                                <p>Whether you are new to PHP or web frameworks or have years of experience,
                                        Laravel is a framework that can grow with you. We'll help you take your first
                                        steps as a web developer or give you a boost as you take your expertise to the
                                        next level. We can't wait to see what you build.</p>
                                <p>Whether you are new to PHP or web frameworks or have years of experience,
                                        Laravel is a framework that can grow with you. We'll help you take your first
                                        steps as a web developer or give you a boost as you take your expertise to the
                                        next level. We can't wait to see what you build.</p>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <br>
            <div class="col-12" style="padding-left: 20px; padding-right: 20px">

                <div class="row">
                    <div class="col-6">
                        <h4 class="text-secondary"><strong>ALCANCE:</strong></h4>
                        <div class="col-12 col-md-12">
                            <label class="form-label"><strong>Inicia</strong></label>
                            <select  class="form-select">
                                <option value="">Gestión De Investigación</option>
                                <option value="">Gestión De Investigación1</option>
                            </select>
                        </div>
                        <div class="col-12 col-md-12">
                            <label class="form-label"><strong>Termina</strong></label>
                            <select  class="form-select">
                                <option value="">Difusión - Resultados - Propiedad Intelectual</option>
                                <option value="">Difusión - Resultados - Propiedad Intelectual</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-6">
                        <h4 class="text-secondary"><strong>LIDER DEL MACROPROCESO:</strong></h4>
                        <div class="col-12 col-md-12">
                            <br>
                            <select  class="form-select">
                                <option value="">Vicerrectorado De Investigación</option>
                                <option value="">Vicerrectorado De Investigación1</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
            <br>
            <div class="col-12" style="padding-left: 20px; padding-right: 20px">
                <div class="row">
                    <div class="col-6">
                        <h4 class="text-secondary"><strong>REQUISITOS:</strong></h4>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="homeIcon-tab" data-bs-toggle="tab" href="#homeIcon"
                                               aria-controls="home" role="tab" aria-selected="true" >Del Cliente</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="profileIcon-tab" data-bs-toggle="tab" href="#profileIcon" aria-controls="profile"
                                               role="tab" aria-selected="false">Norma ISO</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="aboutIcon-tab" data-bs-toggle="tab" href="#aboutIcon"
                                                aria-controls="about" role="tab" aria-selected="false">Legales</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="homeIcon" aria-labelledby="homeIcon-tab" role="tabpanel">
                                            <p>Item 1</p>
                                            <p>Item 2</p>
                                        </div>
                                        <div class="tab-pane" id="profileIcon" aria-labelledby="profileIcon-tab" role="tabpanel">
                                            <p>Item 3</p>
                                            <p>Item 4</p>
                                        </div>
                                        <div class="tab-pane" id="aboutIcon" aria-labelledby="aboutIcon-tab" role="tabpanel">
                                            <p>Item 5</p>
                                            <p>Item 6</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <h4 class="text-secondary"><strong>MACRO PROCESOS DIRECTAMENTE VINCULADOS:</strong></h4>
                        <table>
                            <tr>
                                <td>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered" style="width: 100%">
                                                            <thead>
                                                            <tr>
                                                                <th class="text-center">Nro. </th>
                                                                <th class="text-center">Nombre</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td class="text-center">1</td>
                                                                <td>Nombre 111.</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-center">2</td>
                                                                <td>Nombre 222.</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-center">3</td>
                                                                <td>Nombre 333</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <table>
                <tr>
                    <td>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <p class="text-secondary"><strong>ENTRADA - PROCESO - SALIDA</strong></p>
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">PROVEEDOR</th>
                                                    <th class="text-center">ENTRADA</th>
                                                    <th class="text-center">PROCESOS</th>
                                                    <th class="text-center">SALIDA</th>
                                                    <th class="text-center">CLIENTE</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td class="text-center">Vicerrectorado de investigacion</td>
                                                    <td class="text-center">Politica I+D+i Fondos para investigación ...</td>
                                                    <td>PM07.0.1 Gestion d einvestigacion</td>
                                                    <td>Repositorio institucional</td>
                                                    <td>Repositorio institucionalVicerrectorado de investigacion</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <div class="col-12" style="padding-left: 20px; padding-right: 20px">
                <div class="row">
                    <div class="col-6">
                        <h4 class="text-secondary"><strong>RECURSOS</strong></h4>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="talento" data-bs-toggle="tab" href="#talento-tab"
                                               aria-controls="home" role="tab" aria-selected="true" >Talento Humano</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="recurso" data-bs-toggle="tab" href="#recurso-tab" aria-controls="profile"
                                               role="tab" aria-selected="false">Otros rescursos</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="infraestructura" data-bs-toggle="tab" href="#infraestructura-tab"
                                               aria-controls="about" role="tab" aria-selected="false">Infraestructura</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="software" data-bs-toggle="tab" href="#software-tab"
                                               aria-controls="about" role="tab" aria-selected="false">Software y Comunicaciones</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="talento-tab" aria-labelledby="talento-tab" role="tabpanel">
                                            <table class="table table-bordered" style="width: 100%">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">Nro. </th>
                                                    <th class="text-center">Nombre</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td class="text-center">1</td>
                                                    <td>Nombre 111.</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center">2</td>
                                                    <td>Nombre 222.</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center">3</td>
                                                    <td>Nombre 333</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="recurso-tab" aria-labelledby="recurso-tab" role="tabpanel">
                                            <p>Item 3</p>
                                            <p>Item 4</p>
                                        </div>
                                        <div class="tab-pane" id="infraestructura-tab" aria-labelledby="infraestructura-tab" role="tabpanel">
                                            <p>Item 5</p>
                                            <p>Item 6</p>
                                        </div>
                                        <div class="tab-pane" id="software-tab" aria-labelledby="software-tab" role="tabpanel">
                                            <p>Item 1</p>
                                            <p>Item 2</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <h4 class="text-secondary"><strong>RIESGOS:</strong></h4>
                        <table>
                            <tr>
                                <td>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered" style="width: 100%">
                                                            <thead>
                                                            <tr>
                                                                <th class="text-center">Nro. </th>
                                                                <th class="text-center">Nombre DEL RIESGO</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td class="text-center">1</td>
                                                                <td>Nombre 111.</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-center">2</td>
                                                                <td>Nombre 222.</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-center">3</td>
                                                                <td>Nombre 333</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <br>
            <br>
            <div class="col-12" style="padding-left: 20px; padding-right: 20px">
                <div class="row">
                    <div class="col-6">
                        <h4 class="text-secondary"><strong>INDICADORES (KPI'S):</strong></h4>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <table class="table table-bordered" style="width: 100%">
                                        <tbody>
                                        <tr>
                                            <td style="background-color: #dfe6e9; width: 170px; border: 2px solid #636e72;  border-collapse: collapse;" class="text-center">
                                                Eficiencia
                                            </td>
                                            <td> </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #dfe6e9; width: 170px; border: 2px solid #636e72;  border-collapse: collapse;" class="text-center">
                                                Eficacia
                                            </td>
                                            <td>Eficiencia de la investigacion</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #dfe6e9; width: 170px; border: 2px solid #636e72;  border-collapse: collapse;" class="text-center">
                                                Efectividad
                                            </td>
                                            <td> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        {{--<h4 class="text-secondary"><strong>RIESGOS:</strong></h4>--}}
                        cargar imagenes
                    </div>
                </div>
            </div>
            <hr>
            <br>
        </div>
        {{--<h3>Procesos de {{$macroproceso->nombre}}</h3>--}}
        <div class="row">

        </div>
    </section>
    <!-- Dashboard Ecommerce ends -->
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/pages/dashboard-ecommerce.js')) }}"></script>
@endsection

<style>
    #combo {
        font-family: Tahoma, Verdana, Arial;
        font-size: 11px;
        color: #707070;
        background-color: #FFFFFF;
        border-width: 0;
    }

    input:focus,
    select:focus,
    textarea:focus,
    button:focus {
        outline: none;
    }

    .inputcentrado {
        text-align: center
    }

    table{
        width:100%;
        table-layout: fixed;
        overflow-wrap: break-word;
    }
</style>
