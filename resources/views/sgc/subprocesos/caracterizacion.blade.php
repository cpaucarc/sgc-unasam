@extends('layouts/contentLayoutMaster')

@section('title','Subprocesos')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection
@section('page-style')
    {{-- Page css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <style>
        .border-black {
            border-color: rgb(77, 74, 74);
        }
    </style>
@endsection

@section('content')

<section id="subprocesos">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-primary">{{$subproceso->codigo}}: {{$subproceso->nombre}}</h4>
            <div class="heading-elements">            
                <ul class="list-inline">
                    <li>
                        <a href="#" class="btn btn-gradient-primary">Ficha de Indicadores</a>
                    </li>
                    <li>
                      <a href="{{route('procesos.show',$subproceso->id)}}" class="btn btn-gradient-primary">Regresar</a>
                    </li>
                    <li>
                        <a data-action="collapse"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg></a>
                    </li>
                    <li>
                        <a data-action="reload"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-rotate-cw"><polyline points="23 4 23 10 17 10"></polyline><path d="M20.49 15a9 9 0 1 1-2.12-9.36L23 10"></path></svg></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row px-2">
            <div class="col-12">
                <h4 class="text-primary"><strong>CONTROL DE CAMBIOS DE LA FICHA DE CARACTERIZACIÓN DE SUBPROCESO</strong></h4>
            </div>
            <div class="row">
                <div class="col-12 col-md-7">
                    <div class="table-responsive">
                        <table class="table" style="border: 2px solid #636e72;  border-collapse: collapse;">
                            <tr style="border: 1px solid black;  border-collapse: collapse;">
                                <td class="py-0 px-1" class="text-center" style="background-color: #dfe6e9;border: 2px solid #636e72;  border-collapse: collapse; width: 230px;">
                                    NOMBRE DEL SUBPROCESO
                                </td>
                                <td class="py-0 px-1" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                    {{$subproceso->nombre}}
                                </td>
                            </tr>
                            <tr>
                                <td class="py-0 px-1" class="text-center" style="background-color: #dfe6e9; width: 80px; border: 2px solid #636e72;  border-collapse: collapse;">
                                    TIPO
                                </td>
                                <td class="py-0 px-1" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                    {{$subproceso->detalle->detalle}}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-12 col-md-5">
                    <div class="table-responsive">
                        <table class="table" style="border: 2px solid #636e72;  border-collapse: collapse;">
                            <tr style="border: 1px solid black;  border-collapse: collapse;">
                                <td class="py-0 px-1" style="background-color: #dfe6e9;border: 2px solid #636e72; border-collapse: collapse;">
                                    CÓDIGO
                                </td>
                                <td class="py-0 px-1" colspan="3" style="border: 2px solid #636e72;  border-collapse: collapse;">
                                    {{$subproceso->codigo}}
                                </td>
                            </tr>
                            <tr>
                                <td class="py-0 px-1" style="background-color: #dfe6e9;border: 2px solid #636e72; border-collapse: collapse;">
                                    VERSIÓN
                                </td>
                                <td class="py-0 px-1" class="inputcentrado">{{$subproceso->version->numero}}</td>
                                <td class="py-0 px-1" style="background-color: #dfe6e9; border: 2px solid #636e72; border-collapse: collapse;">
                                    VIGENCIA
                                </td>
                                <td class="py-0 px-1" class="inputcentrado">{{ date('Y-m-d', strtotime($subproceso->version->fecha_vigencia)) }}</td>
                            </tr>   
                        </table>
                    </div>
                </div>                
            </div>
        </div>
        <hr>
        <div class="row px-2">
            <div class="col-12">
                <h4 class="text-primary"><strong>OBJETIVO:</strong></h4>
                <table style="width: 98%; border: 2px solid #d3dadd;  border-collapse: collapse; padding-left: 15px">
                    <tr>
                        <td>
                            <div class="col-12 p-1">
                                <p class="mb-0"> {{$subproceso->objetivo}} </p>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <hr>        
        <div class="row px-2">
            <div class="col-12">
                <h4 class="text-primary"><strong>ALCANCE:</strong></h4>
            </div>
            @php
                $inicia = $subproceso->microprocesos()->orderBy('orden','asc')->first();
                $termina = $subproceso->microprocesos()->orderBy('orden','desc')->first();
            @endphp
            <div class="col-12 col-md-6">
                @if($inicia)
                    <h4><strong>Inicia:</strong> {{$inicia->codigo}} {{$inicia->nombre}}</h4>
                @else
                    <h4><strong>Inicia:</strong> {{$subproceso->codigo}} {{$subproceso->nombre}}</h4>
                @endif
            </div>
            <div class="col-12 col-md-6">
                @if($termina)
                    <h4><strong>Termina:</strong> {{$termina->codigo}} {{$termina->nombre}}</h4>
                @else
                    <h4><strong>Termina:</strong> {{$subproceso->codigo}} {{$subproceso->nombre}}</h4>
                @endif
            </div>
        </div>
        <hr>
        <div class="row px-2">
            <div class="col-12">
                <h4 class="text-primary"><strong>LIDER DEL PROCESO:</strong> <strong class="text-secondary">{{$subproceso->area->nombre}}</strong></h4>
            </div>
        </div>
        <hr>
        <div class="row px-2">
            <div class="col-12 col-md-6">
                <h4 class="text-primary"><strong>REQUISITOS:</strong></h4> 
                @livewire('caracterizacion.requisitos',['nivel_proceso'=>15002,'id_proceso'=>$subproceso->id])
            </div>
            <div class="col-12 col-md-6">
                @livewire('caracterizacion.vinculacions',['nivel_proceso'=>15002,'id_proceso'=>$subproceso->id])
            </div>
        </div>
        <hr>
        <div class="row px-2">
            @if($subproceso->nivelfinal == 1)
                @livewire('caracterizacion.actividads',['nivel_proceso'=>15002,'id_proceso'=>$subproceso->id])
            @else
                @livewire('caracterizacion.entradas',['nivel_proceso'=>15003,'id_proceso'=>$subproceso->id])
            @endif
        </div>
        <hr>       
        <div class="row px-2">
            <div class="col-12 col-md-6">
                <h4 class="text-primary"><strong>RECURSOS</strong></h4>
                @livewire('caracterizacion.recursos',['nivel_proceso'=>15002,'id_proceso'=>$subproceso->id])
            </div>
            <div class="col-12 col-md-6">
                @livewire('caracterizacion.riesgos',['nivel_proceso'=>15002,'id_proceso'=>$subproceso->id])
            </div>
        </div>
        <hr>
    </div>

</section>

@endsection

@section('vendor-script')
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/codigo-script.js')) }}"></script>
@endsection
@section('page-script')
    <script>
        function lanzarScripts(){
            scriptVinculacion();
            scriptRequisito();
            scriptRiesgo();
            scriptEntrada();
            scriptRecurso();
        }
        window.onload = lanzarScripts;
    </script>
@endsection