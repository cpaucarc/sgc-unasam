<table class="table table-bordered">
    <tr>
        <td rowspan="{{ $longitud_total }}"></td>
        <td colspan="12"></td>
        <td rowspan="{{ $longitud_total }}"></td>
    </tr>
    <tr>
        <th rowspan="2"></th>
        <th colspan="9">SISTEMA DE GESTIÓN DE CALIDAD (SGC-UNASAM)</th>
        <th rowspan="2" colspan="2"></th>
    </tr>
    <tr>
        <th colspan="9">FICHA DE INDICADORES (KPI's) DE PROCESO - NIVEL {{ $indicador->parametros['nivel'] }} </th>
    </tr>
    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <th>NOMBRE {{ $indicador->parametros['nombre'] }}</th>
        <td>{{ $indicador->parametros['codigo'] }}: {{ $indicador->parametros['descripcion'] }}</td>
        <th>NOMBRE INDICADOR</th>
        <td>{{ $indicador->nombre }}</td>
        <th>TIPO</th>
        <td>{{ $indicador->detalle->abreviatura }}</td>
        <th>CÓDIGO</th>
        <td>{{ $indicador->codigo }}</td>
        <th>VERSIÓN</th>
        <td>{{ $indicador->version->numero }}</td>
        <th>FECHA APROBACIÓN</th>
        <td>{{ $indicador->version->fecha_vigencia->format('d/m/Y') }}</td>
    </tr>
    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <th rowspan="2">1</th>
        <th colspan="3">DEFINICIÓN DEL INDICADOR</th>
        <th rowspan="2" colspan="2">OBJETIVO DEL INDICADOR</th>
        <td rowspan="2" colspan="6">{{ $indicador->objetivo }}</td>
    </tr>
    <tr>
        <td colspan="3">{{ $indicador->nombre }}</td>
    </tr>
    <tr>
        <th colspan="12">RESPONSABLES DE LA INFORMACIÓN:</th>
    </tr>
    <tr>
        <th colspan="2">ÁREAS QUE SUMINISTRAN INFORMACIÓN</th>
        <th colspan="2">SUB PROCESOS QUE SUMINISTRAN INFORMACIÓN</th>
        <th colspan="5">RESPONSABLE DEL CÁLCULO</th>
        <th colspan="3">USUARIOS</th>
    </tr>
    <tr>
        <td colspan="2">{{ isset($indicador->responsables[0]) ? $indicador->responsables[0]->nombre : '' }}</td>
        <td colspan="2">{{ isset($indicador->suministraninfos[0]) ? $indicador->suministraninfos[0]->nombre_completo : '' }}</td>
        <td rowspan="{{ $indicador->longitud_responsable_calculo - 1 }}" colspan="5">{{ $indicador->longitud_responsable_calculo- 1 }}</td>
        <td colspan="3">{{ isset($indicador->usuarios[0]) ? $indicador->usuarios[0]->nombre : '' }}</td>
    </tr>
    @for ($i = 1; $i <= $indicador->longitud_responsable_calculo - 2; $i++)
        <tr>
            <td colspan="2">{{ isset($indicador->responsables[$i]) ? $indicador->responsables[$i]->nombre : '' }}</td>
            <td colspan="2">{{ isset($indicador->suministraninfos[$i]) ? $indicador->suministraninfos[$i]->nombre_completo : '' }}</td>
            <td colspan="3">{{ isset($indicador->usuarios[$i]) ? $indicador->usuarios[$i]->nombre : '' }}</td>
        </tr>
    @endfor
    <tr>
        <td colspan="2">{{ isset($indicador->responsables[$indicador->longitud_responsable_calculo - 1]) ? $indicador->responsables[$indicador->longitud_responsable_calculo - 1]->nombre : '' }}
        </td>
        <td colspan="2">
            {{ isset($indicador->suministraninfos[$indicador->longitud_responsable_calculo - 1]) ? $indicador->suministraninfos[$indicador->longitud_responsable_calculo - 1]->nombre_completo : '' }}
        </td>
        <th colspan="5">RESPONSABLE DEL ANÁLISIS Y TOMA DE DECISIONES</th>
        <td colspan="3">{{ isset($indicador->usuarios[$indicador->longitud_responsable_calculo - 1]) ? $indicador->usuarios[$indicador->longitud_responsable_calculo - 1]->nombre : '' }}</td>
    </tr>
    <tr>
        <td colspan="2">{{ isset($indicador->responsables[$indicador->longitud_responsable_calculo]) ? $indicador->responsables[$indicador->longitud_responsable_calculo]->nombre : '' }}</td>
        <td colspan="2">
            {{ isset($indicador->suministraninfos[$indicador->longitud_responsable_calculo]) ? $indicador->suministraninfos[$indicador->longitud_responsable_calculo]->nombre_completo : '' }}</td>
        <td rowspan="{{ $indicador->longitud_responsable_analisis }}" colspan="5">{{ $indicador->longitud_responsable_analisis }}</td>
        <td colspan="3">{{ isset($indicador->usuarios[$indicador->longitud_responsable_calculo]) ? $indicador->usuarios[$indicador->longitud_responsable_calculo]->nombre : '' }}</td>
    </tr>
    @for ($i = 1; $i <= $indicador->longitud_responsable_analisis - 1; $i++)
        <tr>
            <td colspan="2">
                {{ isset($indicador->responsables[$indicador->longitud_responsable_calculo + $i]) ? $indicador->responsables[$indicador->longitud_responsable_calculo + $i]->nombre : '' }}</td>
            <td colspan="2">
                {{ isset($indicador->suministraninfos[$indicador->longitud_responsable_calculo + $i]) ? $indicador->suministraninfos[$indicador->longitud_responsable_calculo + $i]->nombre_completo : '' }}
            </td>
            <td colspan="3">{{ isset($indicador->usuarios[$indicador->longitud_responsable_calculo + $i]) ? $indicador->usuarios[$indicador->longitud_responsable_calculo + $i]->nombre : '' }}</td>
        </tr>
    @endfor
    <tr>
        <th colspan="12">PARÁMETROS DE LA MEDICIÓN</th>
    </tr>
    <tr>
        <th colspan="5">FÓRMULA (NUMERADOR / DENOMINADOR)</th>
        <th colspan="2">UNIDAD DE MEDIDA</th>
        <th colspan="2">META</th>
        <th colspan="3">RESTRICCIONES-LIMITACIONES-RIESGOS</th>
    </tr>
    <tr>
        <th rowspan="8"> {{ $indicador->nombre }} </th>
        <th colspan="4">Numerador</th>
        <td rowspan="2" colspan="2"> {{ $indicador->medicion ? $indicador->medicion->detallemedida->detalle : '' }} </td>
        <td rowspan="2" colspan="2"> {{ $indicador->meta_anio ? $indicador->meta_anio->meta : '' }} </td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td rowspan="3" colspan="4"> {{ $indicador->formula ? $indicador->formula->formula_numerador_simple : '' }} </td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <th colspan="2">FRECUENCIA DE MEDICIÓN</th>
        <th colspan="2">ESCALA DE VALORACIÓN</th>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td colspan="2">{{ $indicador->medicion ? $indicador->medicion->detallemedicion->detalle : '' }}</td>
        <td>MUY MALO</td>
        <td>{{ $indicador->escalas['muy_malo'] }}</td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <th colspan="4">Denominador</th>
        <th colspan="2">FRECUENCIA DE REPORTE</th>
        <td>MALO</td>
        <td>{{ $indicador->escalas['malo'] }}</td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td rowspan="3" colspan="4"> {{ $indicador->formula ? $indicador->formula->formula_denominador_simple : '' }} </td>
        <td colspan="2">{{ $indicador->medicion ? $indicador->medicion->detallereporte->detalle : '' }}</td>
        <td>REGULAR</td>
        <td>{{ $indicador->escalas['regular'] }}</td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <th colspan="2">FRECUENCIA DE REVISIÓN</th>
        <td>SATISFACTORIO</td>
        <td>{{ $indicador->escalas['satisfactorio'] }}</td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td colspan="2">{{ $indicador->medicion ? $indicador->medicion->detallerevision->detalle : '' }}</td>
        <td>MUY SATISFACTORIO</td>
        <td>{{ $indicador->escalas['muy_satisfactorio'] }}</td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <th colspan="5">RESULTADOS DE LA MEDICIÓN</th>
        <th colspan="7">EVALUACIÓN DE LA MEDICIÓN</th>
    </tr>
    <tr>
        <th rowspan="2" colspan="3">COMPORTAMIENTO DEL INDICADOR</th>
        <th colspan="2">RESULTADOS</th>
        <th rowspan="3">VALORACIÓN DEL DESEMPEÑO</th>
        <th rowspan="2" colspan="6">ACCIONES DE MEJORA</th>
    </tr>
    <tr>
        <th colspan="2"> {{ $indicador->medicion ? $indicador->medicion->detallemedida->detalle : '' }} </th>
    </tr>
    <tr>
        <th>PERIODO</th>
        <th>VALOR - NUMERADOR</th>
        <th>VALOR - DENOMINADOR</th>
        <th>VALOR - INDICADOR</th>
        <th>META</th>
        <th colspan="4">PROPUESTA</th>
        <th>FECHA DE PROPUESTA</th>
        <th>FECHA DE CUMPLIMIENTO</th>
    </tr>

    @foreach ($indicador->resultados as $resultado)
        <tr>
            <td>{{ $resultado->detalleperiodo->detalle }}</td>
            <td>{{ $resultado->valor_numerador }}</td>
            <td>{{ $resultado->valor_denominador }}</td>
            <td>{{ $resultado->valor_indicador }}</td>
            <td>{{ $indicador->metas->where('param_periodo', $resultado->param_periodo)->first() ? $indicador->metas->where('param_periodo', $resultado->param_periodo)->first()->meta : '' }}</td>
            <td>{{ $resultado->valor_indicador }}</td>
            <td colspan="4">{{ $resultado->evaluacion ? $resultado->evaluacion->propuesta_mejora : '' }}</td>
            <td>{{ $resultado->evaluacion ? $resultado->evaluacion->fecha_propuesta : '' }}</td>
            <td>{{ $resultado->evaluacion ? $resultado->evaluacion->fecha_cumplimiento : '' }}</td>
        </tr>
    @endforeach
    <tr>
        <td colspan="12"></td>
    </tr>
    <tr>
        <th colspan="5">GRÁFICA</th>
        <th colspan="7">CONTROL DE CAMBIOS</th>
    </tr>
    <tr>
        <td rowspan="{{ $indicador->versiones_count + 6 }}" colspan="5"></td>
        <th colspan="2">VERSIÓN</th>
        <th>FECHA</th>
        <th colspan="4">DESCRIPCIÓN DEL CAMBIO</th>
    </tr>
    @if ($indicador->versiones->count() > 0)
        @foreach ($indicador->versiones as $version)
            <tr>
                <td colspan="2">{{ $version->version->numero }}</td>
                <td>{{ $version->version->fecha_vigencia->format('d/m/Y') }}</td>
                <td colspan="4">{{ $version->version->descripcion }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="2">{{ $indicador->version->numero }}</td>
            <td>{{ $indicador->version->fecha_vigencia->format('d/m/Y') }}</td>
            <td colspan="4">{{ $indicador->version->descripcion }}</td>
        </tr>
    @endif
    <tr>
        <th colspan="7">APROBACIÓN</th>
    </tr>
    <tr>
        <th colspan="2">ACCIÓN</th>
        <th colspan="2">NOMBRE</th>
        <th>CARGO</th>
        <th>FIRMA</th>
        <th>FECHA</th>
    </tr>
    <tr>
        <td colspan="2">ELABORADO POR:</td>
        <td colspan="2"></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2">REVISADO POR:</td>
        <td colspan="2"></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2">APROBADO POR:</td>
        <td colspan="2"></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th colspan="12">SGC - UNASAM</th>
    </tr>
</table>
