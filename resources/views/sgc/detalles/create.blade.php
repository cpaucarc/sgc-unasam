<div>
    <x-jet-button wire:click="$set('open',true)" class="btn add-new btn-primary mt-50">
        <span>Agregar Detalle</span>
    </x-jet-button>
    <x-jet-dialog-modal wire:model="open" maxWidth="lg">
        <x-slot name="title" class="modal-header bg-transparent">
        </x-slot>
        <x-slot name="content" class="modal-body pb-5 px-sm-4 mx-50">
            <h1 class="address-title text-center mb-1" id="addNewPersonaTitulo">Agregar Nuevo Detalle</h1>
            <p class="address-subtitle text-center mb-2 pb-75">Agregar los datos</p>
            <div class="row gy-1 gx-2">
                <div class="col-12 col-md-4">
                    <label class="form-label">Nombres</label>
                    <input
                        type="text"
                        wire:model="nombres"
                        class="form-control"
                        placeholder="Nombres"
                        data-msg="Por favor, introduzca sus nombres completos"
                    />
                </div>
                <div class="col-12 col-md-4">
                    <label class="form-label">Apellido Paterno</label>
                    <input
                        type="text"
                        wire:model="ape_paterno"
                        class="form-control"
                        placeholder="apellido paterno"
                        data-msg="Ingrese su apellido paterno"
                    />
                </div>
                <div class="col-12 col-md-4">
                    <label class="form-label">Apellido Materno</label>
                    <input
                        type="text"
                        wire:model="ape_materno"
                        class="form-control"
                        placeholder="Apellido materno"
                        data-msg="Ingrese su apellido materno"
                    />
                </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <div class="col-12 text-center">
                <x-jet-button wire:click="$set('open',false)" class="btn btn-danger mt-2">
                    Cancelar
                </x-jet-button>
                <x-jet-button wire:click.prevent="save" class="btn btn-success me-1 mt-2">
                    Guardar
                </x-jet-button>
                <span wire:loading wire:target="save">Cargando</span>
            </div>

        </x-slot>
    </x-jet-dialog-modal>

</div>
