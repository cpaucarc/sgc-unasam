@extends('layouts/contentLayoutMaster')

@section('title', 'Dashboard')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
@section('page-style')
    {{-- Page css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <style>
        .border-black {
            border-color: rgb(77, 74, 74);
        }
    </style>
@endsection

@section('content')
    <!-- Dashboard Ecommerce Starts -->
    <section id="dashboard">
        @livewire('dashboard')
    </section>

    <!-- Dashboard Ecommerce ends -->
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>

    {{-- <script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.0/dist/chart.min.js"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2"></script>


    <script src="{{ asset(mix('js/scripts/codigo-script.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/charts/echarts.min.js')) }}"></script>
@endsection
@section('page-script')
    <script>
        $user = @js(Auth::user()->name);
        if ($user) {
            nitificacion('success', 'Has ingresado al dashboard del SGC. ¡Ahora puedes empezar a explorar!', '👋 ¡Bienvenido!', 2000);
        }

        function lanzarScripts() {
            scriptDashboard();


            $("#btn_limpiar").on('click', function() {
                $('#macroproceso').val(null).trigger('change');
                $('#proceso').val(null).trigger('change');
            })

        }
        window.onload = lanzarScripts;
    </script>
@endsection
