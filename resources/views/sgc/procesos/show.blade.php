@extends('layouts/contentLayoutMaster')

@section('title', 'Procesos')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection
@section('page-style')
    {{-- Page css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/dashboard-ecommerce.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <style>
        .nav-tabs2 {
            border-bottom: 1px solid #a8c6e35d !important;
        }

        .nav-link2 {
            border: 2px solid #a8c6e3 !important;
            border-top-left-radius: 0.25rem !important;
            border-top-right-radius: 0.25rem !important;
        }

        .nav-tabs2 .nav-link2.active {
            border: 0px !important;
            border-bottom: 3px solid #228bf4 !important;
        }
    </style>
@endsection

@section('content')
    <!-- Dashboard Ecommerce Starts -->
    <section id="Procesos">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title text-primary">{{ $proceso->codigo }}: {{ $proceso->nombre }}</h4>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li>
                            <a href="{{ route('procesos.caracterizacion', $proceso->id) }}" class="btn btn-gradient-primary">Ficha de Caracterización</a>
                        </li>
                        <li>
                            <a data-action="collapse"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none"
                                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                                    <polyline points="6 9 12 15 18 9"></polyline>
                                </svg></a>
                        </li>
                        <li>
                            <a data-action="reload"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none"
                                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-rotate-cw">
                                    <polyline points="23 4 23 10 17 10"></polyline>
                                    <path d="M20.49 15a9 9 0 1 1-2.12-9.36L23 10"></path>
                                </svg></a>
                        </li>
                        {{-- <li>
                      <a data-action="close"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg></a>
                  </li> --}}
                    </ul>
                </div>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <ul class="nav nav-tabs nav-tabs2" role="tablist">
                        <li class="nav-item">
                            <a
                               class="nav-link nav-link2 active"
                               id="generales-tab"
                               data-bs-toggle="tab"
                               href="#generales"
                               aria-controls="generales"
                               role="tab"
                               aria-selected="true">Datos generales</a>
                        </li>
                        <li class="nav-item">
                            <a
                               class="nav-link nav-link2"
                               id="diagrama-tab"
                               data-bs-toggle="tab"
                               href="#diagrama"
                               aria-controls="diagrama"
                               role="tab"
                               aria-selected="false">Diagrama de flujo</a>
                        </li>
                        <li class="nav-item">
                            <a
                               class="nav-link nav-link2"
                               id="requisitos-tab"
                               data-bs-toggle="tab"
                               href="#requisitos"
                               aria-controls="requisitos"
                               role="tab"
                               aria-selected="false">Requisitos</a>
                        </li>
                        <li class="nav-item">
                            <a
                               class="nav-link nav-link2"
                               id="terminos-tab"
                               data-bs-toggle="tab"
                               href="#terminos"
                               aria-controls="terminos"
                               role="tab"
                               aria-selected="false">Términos y definiciones</a>
                        </li>
                        {{-- <li class="nav-item">
                        <a
                          class="nav-link nav-link2"
                          id="ginteres-tab"
                          data-bs-toggle="tab"
                          href="#ginteres"
                          aria-controls="ginteres"
                          role="tab"
                          aria-selected="false"
                          >Grupo de Interés</a
                        >
                      </li> --}}
                        <li class="nav-item">
                            <a
                               class="nav-link nav-link2"
                               id="mecanismo-tab"
                               data-bs-toggle="tab"
                               href="#mecanismo"
                               aria-controls="mecanismo"
                               role="tab"
                               aria-selected="false">Mecanismos</a>
                        </li>
                        <li class="nav-item">
                            <a
                               class="nav-link nav-link2"
                               id="indicador-tab"
                               data-bs-toggle="tab"
                               href="#indicador"
                               aria-controls="indicador"
                               role="tab"
                               aria-selected="false">Indicadores</a>
                        </li>
                        {{--<li class="nav-item">
                            <a
                               class="nav-link nav-link2"
                               id="controlDocumental-tab"
                               data-bs-toggle="tab"
                               href="#controlDocumental"
                               aria-controls="controlDocumental"
                               role="tab"
                               aria-selected="false">Control documental</a>
                        </li>--}}
                        <li class="nav-item">
                            <a
                               class="nav-link nav-link2"
                               id="manual-tab"
                               data-bs-toggle="tab"
                               href="#manual"
                               aria-controls="manual"
                               role="tab"
                               aria-selected="false">Manual</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="generales" aria-labelledby="generales-tab" role="tabpanel">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Versión</th>
                                                    <th class="text-center">Código</th>
                                                    <th class="text-center">Tipo de Proceso</th>
                                                    <th class="text-center">Área Responsable</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-center">{{ $proceso->version_id }}</td>
                                                    <td class="text-center">{{ $proceso->codigo }}</td>
                                                    <td class="text-center">
                                                        {{ $proceso->detalle->detalle }}
                                                    </td>
                                                    <td>{{ $proceso->area->nombre }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row pt-1">
                                <div class="col-sm-12">
                                    @if ($proceso->descripcion)
                                        <h4 class="card-title text-primary" style="margin-bottom: 4px;">DESCRIPCIÓN</h4>
                                        <p class="card-text text-dark">
                                            {{ $proceso->descripcion }}
                                        </p>
                                    @endif

                                    @if ($proceso->objetivo)
                                        <h4 class="card-title text-primary" style="margin-bottom: 4px;">OBJETIVO</h4>
                                        <p class="card-text text-dark">
                                            {{ $proceso->objetivo }}
                                        </p>
                                    @endif

                                    @if ($proceso->alcance)
                                        <h4 class="card-title text-primary" style="margin-bottom: 4px;">ALCANCE</h4>
                                        <p class="card-text text-dark">
                                            {{ $proceso->alcance }}
                                        </p>
                                    @endif

                                    @if ($proceso->finalidiad)
                                        <h4 class="card-title text-primary" style="margin-bottom: 4px;">FINALIDAD</h4>
                                        <p class="card-text text-dark">
                                            {{ $proceso->finalidiad }}
                                        </p>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="diagrama" aria-labelledby="diagrama-tab" role="tabpanel">
                            @livewire('panelproceso.diagramas', ['nivel_proceso' => 15002, 'id_proceso' => $proceso->id])
                        </div>
                        <div class="tab-pane" id="requisitos" aria-labelledby="requisitos-tab" role="tabpanel">
                            <div class="row">
                                <h4 class="card-title text-primary" style="margin-bottom: 4px;">REQUISITOS</h4>
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">REQUISITO</th>
                                                    <th class="text-center">TIPO</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($requisitos as $requisito)
                                                    <tr>
                                                        <td>{{ $requisito->descripcion }}</td>
                                                        @if ($requisito->tipo)
                                                            <td>{{ $requisito->tipo->detalle }}</td>
                                                        @else
                                                            <td> - </td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="terminos" aria-labelledby="terminos-tab" role="tabpanel">
                            @livewire('panelproceso.terminos', ['nivel_proceso' => 15001, 'id_proceso' => $proceso->macroproceso_id])
                        </div>
                        <div class="tab-pane" id="ginteres" aria-labelledby="ginteres-tab" role="tabpanel">
                            <p>
                                ginteres
                            </p>
                        </div>
                        <div class="tab-pane" id="mecanismo" aria-labelledby="mecanismo-tab" role="tabpanel">
                            <div class="row">
                                <h4 class="card-title text-primary" style="margin-bottom: 4px;">MECANISMO</h4>
                                <div class="col-sm-12">
                                    <livewire:mecanismo.edit :relacionado_id="$proceso->id" :relacionado_type="'Proceso'"></livewire:mecanismo.edit>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="indicador" aria-labelledby="indicador-tab" role="tabpanel">
                            <div class="row">
                                <h4 class="card-title text-primary" style="margin-bottom: 4px;">INDICADORES</h4>
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Código</th>
                                                    <th class="text-center">Nombre</th>
                                                    <th class="text-center">Versión</th>
                                                    <th class="text-center">Tipo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($indicadores as $indicador)
                                                    <tr>
                                                        <td>{{ $indicador->codigo }}</td>
                                                        <td>{{ $indicador->nombre }}</td>
                                                        <td>{{ $indicador->version->descripcion }}</td>
                                                        <td>{{ $indicador->detalle->detalle }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="controlDocumental" aria-labelledby="controlDocumental-tab" role="tabpanel">
                            <p>
                                controlDocumental
                            </p>
                        </div>
                        <div class="tab-pane" id="manual" aria-labelledby="manual-tab" role="tabpanel">
                            @livewire('panelproceso.manuales', ['nivel_proceso' => 15002, 'id_proceso' => $proceso->id])
                        </div>
                    </div>

                </div>
            </div>
        </div>
        @if ($subprocesos->count() > 0)
            <h3>Subprocesos del Proceso de {{ $proceso->nombre }}</h3>
            <div class="row">
                @foreach ($subprocesos as $subproceso)
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="card-body d-flex align-items-center justify-content-between">
                                <div>
                                    <h3 class="fw-bolder mb-75">{{ $subproceso->codigo }}</h3>
                                    <strong>{{ $subproceso->nombre }}</strong>
                                </div>
                                <div class="avatar bg-light-primary p-50">
                                    <a href="{{ route('subprocesos.show', $subproceso->id) }}">
                                        <span class="avatar-content">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                      d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1" />
                                            </svg>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif

    </section>
    <!-- Dashboard Ecommerce ends -->
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/codigo-script.js')) }}"></script>
@endsection
@section('page-script')
    <script>
        function lanzarScripts() {
            scriptDiagramas();
            scriptManuales();
        }
        window.onload = lanzarScripts;
    </script>
@endsection
