@extends('layouts/contentLayoutMaster')

@section('title', 'Manuales de Software')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">    
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
@section('page-style')
    {{-- Page css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
    <style>
      .border-black {
          border-color: rgb(77, 74, 74);
      }
      
  </style>
@endsection

@section('content')

<section id="caracterizacion">
    <div class="card border-black">
        <div class="text-center p-1">
            <h1 class="text-primary mb-0"><strong>LISTADO DE MANUALES DE SOFTWARE</strong></h1>
        </div>
        <div class="card-body pt-0">
            <div class="row gx-2 border-black rounded-2 p-2">
                <div class="card card-transaction px-0 mb-0">
                    <table class="table b-table">
                        <tr>
                            <td><strong>Manual del sistema del Macroproceso PE02: Gestión de la Calidad</strong></td>
                            <td>
                                <a href="{{route('manuales.software.view','PE02')}}" class="btn btn-primary" target="_blank">Ver Manual</a>
                            </td>
                        </tr> 
                        <tr>
                            <td><strong>Manual del sistema del Macroproceso PM06: Formación Profesional</strong></td>
                            <td><a href="{{route('manuales.software.view','PM06')}}" class="btn btn-primary" target="_blank">Ver Manual</a></td>
                        </tr> 
                        <tr>
                            <td><strong>Manual del sistema del Macroproceso PM07: Investigación, Desarrollo Tecnológico e Innovación</strong></td>
                            <td><a href="{{route('manuales.software.view','PM07')}}" class="btn btn-primary" target="_blank">Ver Manual</a></td>
                        </tr> 
                        <tr>
                            <td><strong>Manual del sistema del Macroproceso PM08: Responsabilidad Social Universitaria</strong></td>
                            <td><a href="{{route('manuales.software.view','PM08')}}" class="btn btn-primary" target="_blank">Ver Manual</a></td>
                        </tr>   
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('vendor-script')
  {{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/codigo-script.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/charts/echarts.min.js')) }}"></script>
@endsection
@section('page-script')

@endsection