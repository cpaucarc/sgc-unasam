
@extends('layouts/contentLayoutMaster')

@section('title', 'Inicio')

@section('vendor-style')
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection
@section('page-style')
  {{-- Page css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/dashboard-ecommerce.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/charts/chart-apex.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
  <style>
    .disabled {
      --bs-bg-opacity: 1;
      background-color: rgba(155, 155, 155,0.2) !important;
    }
  </style>
@endsection

@section('content')
<!-- Dashboard Ecommerce Starts -->
<section id="home">

  <div class="row match-height">
    <!-- Browser States Card -->
    <div class="col-lg-2 col-md-6 col-12">
      <div class="card">
        <div class="card-body text-center" style="writing-mode: vertical-rl; transform: rotate(180deg);">
          <div class="border border-primary">
            <h4 class="card-title">NECESIDADES Y EXPECTATIVAS</h4>
            <h4 class="card-title">GRUPOS DE INTERÉS UNASAM</h4>
          </div>
          <svg viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg" height="60">
            <rect height="30" transform="translate(130 100) rotate(-180)" width="40" x="50" y="35"/>
            <polygon points="50 80 50 20 20 50 50 80"/>
          </svg>
        </div>
      </div>
    </div>
    <!--/ Browser States Card -->

    <!-- Goal Overview Card -->
    <div class="col-lg-8 col-md-6 col-12">
      <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center pb-0">
          <h4 class="card-title">PROCESOS ESTRATÉGICOS</h4>
        </div>
        <div class="card-body p-2 m-2 border border-secundary">
            <div class="row">
              <div class="col-md-3">
                <a href="#" class="btn btn-outline-primary waves-effect d-flex bg-light disabled" style="height: 80px; align-items: center;">Gestión de la Dirección</a>
              </div>
              <div class="col-md-3">
                <a href="{{route('macroprocesos.show',2)}}" class="btn btn-outline-primary waves-effect d-flex" style="height: 80px; align-items: center;">Gestión de la Calidad</a>
              </div>
              <div class="col-md-3">
                <a href="#" class="btn btn-outline-primary waves-effect disabled">Gestión de La Comunicación y Relaciones Estratégicas</a>
              </div>
              <div class="col-md-3">
                <a href="#" class="btn btn-outline-primary waves-effect d-flex disabled" style="height: 80px; align-items: center;">Gestión del Servicio Educativo</a>
              </div>
            </div>
        </div>
        <div class="card-header d-flex justify-content-between align-items-center pb-0">
          <h4 class="card-title">PROCESOS MISIONALES</h4>
        </div>
        <div class="card-body p-2 m-2 border border-secundary">
          <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-2">
              <a href="{{route('macroprocesos.show',7)}}" class="btn btn-outline-primary waves-effect d-flex justify-content-center align-items-center" style="height: 50px;">I+D+i </a>
            </div>
            <div class="col-md-1 justify-content-center" style="height: 50px; align-items: center;">
              <svg height="38" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                <rect height="30" transform="translate(100 100) rotate(-180)" width="30" x="35" y="35"/>
                <polygon points="40 80 40 20 10 50 40 80"/>
                <polygon points="60 80 60 20 90 50 60 80"/>
              </svg>
            </div>
            <div class="col-md-2">
              <a href="{{route('macroprocesos.show',8)}}" class="btn btn-outline-primary waves-effect d-flex justify-content-center align-items-center" style="height: 50px;">RSU</a>
            </div>
            <div class="col-md-4"></div>
          </div>
          <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-2">
              <!-- <div class="d-flex justify-content-center" style="text-align: center; display: inline-block;"> -->
              <div class="d-flex justify-content-end" style="text-align: center; display: inline-block;">
                <svg height="38" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" style="transform: rotate(90deg);">
                  <rect height="30" transform="translate(100 100) rotate(-180)" width="30" x="35" y="35"/>
                  <polygon points="40 80 40 20 10 50 40 80"/>
                  <polygon points="60 80 60 20 90 50 60 80"/>
                </svg>
              </div>
            </div>
              <div class="col-md-1 d-none d-md-block"></div>
              <div class="col-md-2 d-none d-md-block">
                <div class="d-flex justify-content-start" style="text-align: center; display: inline-block;">
                  <svg height="38" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" style="transform: rotate(90deg);">
                    <rect height="30" transform="translate(100 100) rotate(-180)" width="30" x="35" y="35"/>
                    <polygon points="40 80 40 20 10 50 40 80"/>
                    <polygon points="60 80 60 20 90 50 60 80"/>
                  </svg>
                </div>
              </div>
            <!-- </div> -->
            <div class="col-md-4"></div>
          </div>
          <div class="row">
            <div class="col-md-1"></div>            
            <div class="col-md-2">
              <a href="#" class="btn btn-outline-primary waves-effect d-flex justify-content-center align-items-center disabled" style="height: 50px;">Admisión</a>
            </div>
            <div class="col-md-1">
              <svg height="38" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                <rect height="30" width="30" x="20" y="35"/>
                <polygon points="50 80 50 20 80 50 50 80"/>
              </svg>
            </div>
            <div class="col-md-3">
              <a href="{{route('macroprocesos.show',6)}}" class="btn btn-outline-primary waves-effect d-flex justify-content-center align-items-center" style="height: 50px;">Formación Profesional</a>
            </div>
            <div class="col-md-1 ">
              <svg height="38" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                <rect height="30" width="30" x="20" y="35"/>
                <polygon points="50 80 50 20 80 50 50 80"/>
              </svg>
            </div>
            <div class="col-md-3">
              <a href="#" class="btn btn-outline-primary waves-effect d-flex justify-content-center align-items-center disabled" style="height: 50px;">Graduación Titulación</a>
            </div>
            <div class="col-md-1"></div>
          </div>
          <!-- <div class="row">
            <div class="col-md-12 justify-content-center" style="text-align: center; margin-left: -45px;">
              <a href="{{route('macroprocesos.show',7)}}" class="btn btn-outline-primary waves-effect">I+D+i </a>
              <svg height="38" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                <rect height="30" transform="translate(100 100) rotate(-180)" width="30" x="35" y="35"/>
                <polygon points="40 80 40 20 10 50 40 80"/>
                <polygon points="60 80 60 20 90 50 60 80"/>
              </svg>
              <a href="{{route('macroprocesos.show',8)}}" class="btn btn-outline-primary waves-effect">RSU</a>
            </div>
          </div>
          <div class="row">
            <div class="d-flex justify-content-center" style="text-align: center; display: inline-block;">
                <div style="margin-left: -90px; margin-right: 80px;">
                  <svg height="38" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" style="transform: rotate(90deg);">
                    <rect height="30" transform="translate(100 100) rotate(-180)" width="30" x="35" y="35"/>
                    <polygon points="40 80 40 20 10 50 40 80"/>
                    <polygon points="60 80 60 20 90 50 60 80"/>
                  </svg>
                </div>
                <div>
                  <svg height="38" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg" style="transform: rotate(90deg);">
                    <rect height="30" transform="translate(100 100) rotate(-180)" width="30" x="35" y="35"/>
                    <polygon points="40 80 40 20 10 50 40 80"/>
                    <polygon points="60 80 60 20 90 50 60 80"/>
                  </svg>
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 justify-content-center" style="text-align: center;">
              <a href="#" class="btn btn-outline-primary waves-effect">Admisión</a>
              <svg height="38" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                <rect height="30" width="30" x="20" y="35"/>
                <polygon points="50 80 50 20 80 50 50 80"/>
              </svg>
              <a href="{{route('macroprocesos.show',6)}}" class="btn btn-outline-primary waves-effect">Formación Profesional</a>
              <svg height="38" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                <rect height="30" width="30" x="20" y="35"/>
                <polygon points="50 80 50 20 80 50 50 80"/>
              </svg>
              <a href="#" class="btn btn-outline-primary waves-effect">Graduación Titulación</a>
            </div>
          </div> -->

        </div>

        <div class="card-body p-2 m-2 border border-secundary">
          <div class="row">
            <div class="col-md-2 justify-content-center" style="text-align: center;">
              <a href="#" class="btn btn-outline-primary waves-effect d-flex disabled px-50" style="height: 105px; align-items: center;">Bienestar Universitario</a>
            </div>
            <div class="col-md-8 justify-content-center border border-primary " style="text-align: center;">
              <div class="row">
                <div class="text-center" style="margin-top: 5px;">
                  <h5>Recursos de Apoyo</h5>
                </div>
              </div>
              <div class="row" style="margin-bottom: 6px;">
                  <div class="col-md-4">
                    <a href="#" class="btn btn-outline-primary waves-effect d-flex disabled" style="height: 66px; align-items: center;">
                      Infraestructura y equipamiento
                    </a>
                  </div>
                  <div class="col-md-4">
                    <a href="#" class="btn btn-outline-primary waves-effect d-flex disabled">
                      Tecnología de la información y comunicación
                    </a>
                  </div>
                  <div class="col-md-4">
                    <a href="#" class="btn btn-outline-primary waves-effect d-flex disabled" style="height: 66px; align-items: center;">
                      Biblioteca especializada
                    </a>
                  </div>
                  {{-- <div class="col-md-4 text-center">
                    <a href="#" class="btn btn-outline-primary waves-effect d-flex text-center" style="height: 66px; align-items: center;">Biblioteca</a>
                  </div> --}}
              </div>
            </div>
            <div class="col-md-2 justify-content-center" style="text-align: center;">
              <a href="#" class="btn btn-outline-primary waves-effect d-flex disabled px-50" style="height: 105px; align-items: center;">Recursos Humanos</a>
            </div>
          </div>
        </div>        
        <div class="card-header d-flex justify-content-between align-items-center pt-0">
          <h4 class="card-title">PROCESOS DE SOPORTE</h4>
        </div>
      </div>
    </div>

    <div class="col-lg-2 col-md-6 col-12">
      <div class="card card-transaction">
        <div class="card-body text-center " style="writing-mode: vertical-rl; transform: rotate(180deg);">
          <svg viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg" height="60">
            <rect height="30" transform="translate(130 100) rotate(-180)" width="40" x="50" y="35"/>
            <polygon points="50 80 50 20 20 50 50 80"/>
          </svg>
          <div class="border border-primary">
            <h4 class="card-title">SATISFACCIÓN DE NECESIDADES Y EXPECTATIVAS</h4>
            <h4 class="card-title">GRUPOS DE INTERÉS UNASAM</h4>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>
<!-- Dashboard Ecommerce ends -->
@endsection

@section('vendor-script')
  {{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
  @endsection
@section('page-script')
  <script>
    $user = @js(Auth::user()->name);
    var isRtl = $('html').attr('data-textdirection') === 'rtl';
    /*if ($user) {
      toastr['success'](
        'Has iniciado sesión con éxito al SGC. ¡Ahora puedes empezar a explorar!',
        '👋 ¡Bienvenido '+$user+'!',
        {
            closeButton: true,
            tapToDismiss: false,
            closeDuration: 2000,
            rtl: isRtl
        }
      );
    }*/
  </script>
@endsection
