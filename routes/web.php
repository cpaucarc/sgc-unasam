<?php

use App\Exports\generarCaracterizacion;
use App\Exports\IndicadorExport;
use App\Exports\MacroprocesoExport;
use App\Exports\Reportes\PorTipoExport;
use App\Exports\Reportes\PorProcesosExport;
use App\Exports\Reportes\PorOrganicasExport;
use App\Exports\Reportes\IndicadoresExport;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AppsController;
use App\Http\Controllers\UserInterfaceController;
use App\Http\Controllers\CardsController;
use App\Http\Controllers\ComponentsController;
use App\Http\Controllers\ExtensionController;
use App\Http\Controllers\PageLayoutController;
use App\Http\Controllers\FormsController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\MiscellaneousController;
use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\ChartsController;
use App\Http\Controllers\ExportsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ReportesController;
/*Controladores de tablas*/
use App\Http\Controllers\sgc\ActividadController;
use App\Http\Controllers\sgc\AnioController;
use App\Http\Controllers\sgc\ArchivoController;
use App\Http\Controllers\sgc\AreaController;
use App\Http\Controllers\sgc\CargoController;
use App\Http\Controllers\sgc\DetalleController;
use App\Http\Controllers\sgc\DocumentodetalleController;
use App\Http\Controllers\sgc\DocumentoController;
use App\Http\Controllers\sgc\EscalaController;
use App\Http\Controllers\sgc\EvaluacionController;
use App\Http\Controllers\sgc\EvidenciaController;
use App\Http\Controllers\sgc\IndicadorController;
use App\Http\Controllers\sgc\InteresadoController;
use App\Http\Controllers\sgc\MacroprocesoController;
use App\Http\Controllers\sgc\MecanismoController;
use App\Http\Controllers\sgc\MedicionController;
use App\Http\Controllers\sgc\MicroprocesoController;
use App\Http\Controllers\sgc\ParametroController;
use App\Http\Controllers\sgc\PersonaController;
use App\Http\Controllers\sgc\ProcesomecanismoController;
use App\Http\Controllers\sgc\ProcesoController;
use App\Http\Controllers\sgc\RecursoController;
use App\Http\Controllers\sgc\RequisitoController;
use App\Http\Controllers\sgc\RestriccionController;
use App\Http\Controllers\sgc\ResultadodetalleController;
use App\Http\Controllers\sgc\ResultadoController;
use App\Http\Controllers\sgc\RoleController;
use App\Http\Controllers\sgc\SubprocesoController;
use App\Http\Controllers\sgc\SuministrainfoController;
use App\Http\Controllers\sgc\TerminoController;
use App\Http\Controllers\sgc\TrabajadorController;
use App\Http\Controllers\sgc\VersionController;


use App\Http\livewire\ActividadComponent;
use App\Http\livewire\AnioComponent;
use App\Http\livewire\AreaComponent;
use App\Http\livewire\CargoComponent;
use App\Http\livewire\DetalleComponent;
use App\Http\livewire\DocumentoComponent;
use App\Http\livewire\TrabajadorComponent;

use App\Http\livewire\EscalaComponent;
use App\Http\livewire\EvaluacionComponent;
use App\Http\livewire\ResultadoComponent;
use App\Http\livewire\SuministroComponent;
use App\Http\livewire\EvidenciasComponent;
use App\Models\Indicador;
use App\Models\Subproceso;

// Main Page Route
Route::get('/', function () {
    return redirect()->route('login');
});

//Link personalizados
Route::get('/home', [HomeController::class, 'index'])->name('home-index');
Route::get('/cambiarmirol/{id}', function ($id) {
    cambiarRol($id);
});

Route::middleware(['auth','revisa_asignacion'])->group(function () {
    Route::resource('archivos', ArchivoController::class);
    // Route::resource('actividads', ActividadController::class);
    // Route::resource('anios', AnioController::class);
    // Route::resource('areas', AreaController::class);
    // Route::resource('cargos', CargoController::class);
    // Route::resource('detalles', DetalleController::class);
    Route::resource('documentos', DocumentoController::class);
    // Route::resource('escalas', EscalaController::class);
    // Route::resource('evaluacions', EvaluacionController::class);
    //Route::resource('evidencias', EvidenciaController::class);
    Route::resource('evidencias', EvidenciasComponent::class);
    // Route::view('indicadores', 'livewire.indicadors.index')->name('indicadors.index');
    Route::get('indicadores', [IndicadorController::class, 'index'])->name('indicadors.index');
    Route::get('indicadores/audit', [IndicadorController::class, 'audit'])->name('indicadors.audit');
    //Route::view('interesados', 'livewire.interesados.index')->name('interesados.index');
    Route::resource('interesados', InteresadoController::class);
    Route::resource('macroprocesos', MacroprocesoController::class);
    Route::get('macroprocesos/{macroproceso}/caracterizacion', [MacroprocesoController::class, 'caracterizacion'])->name('macroprocesos.caracterizacion');
    //Route::get('macroprocesos/{macroproceso}/ficha', [\App\Http\Livewire\Indicadors::class, 'ficha'])->name('macroprocesos.ficha');
    Route::get('macroprocesos/{macroproceso}/ficha', [\App\Http\Livewire\FichaIndicador::class, 'ficha'])->name('macroprocesos.ficha');
    Route::get('macroprocesos/{indicador}/{anio}/showFicha', [\App\Http\Livewire\FichaIndicador::class, 'showFicha'])->name('show.ficha');
    Route::view('macroprocesos', 'livewire.macroprocesos.index')->name('macroprocesos.index');
    Route::view('mecanismos', 'livewire.mecanismos.index')->name('mecanismos.index');
    Route::view('mediciones', 'livewire.medicions.index');
    // Route::view('microprocesos', 'livewire.microprocesos.index')->name('microprocesos.index');
    Route::resource('microprocesos', MicroprocesoController::class);
    Route::view('parametros', 'livewire.parametros.index')->name('parametros.index');
    Route::resource('personas', PersonaController::class);
    Route::view('usuarios', 'livewire.users.index')->name('users.index');
    Route::view('procesomecanismos', 'livewire.procesomecanismos.index')->name('procesomecanismos.index');
    Route::resource('procesos', ProcesoController::class);
    Route::get('procesos/{proceso}/caracterizacion', [ProcesoController::class, 'caracterizacion'])->name('procesos.caracterizacion');
    // Route::view('procesos', 'livewire.procesos.index')->name('procesos.index');
    Route::resource('recursos', RecursoController::class);
    // Route::view('recursos', 'livewire.recursos.index')->name('recursos.index');
    Route::resource('requisitos', RequisitoController::class);
    Route::resource('restricciones', RestriccionController::class);
    // Route::view('restricciones', 'livewire.restriccions.index');
    Route::resource('resultadodetalles', ResultadodetalleController::class);
    Route::resource('resultados', ResultadoController::class);
    //Route::view('resultados', 'livewire.resultados.index')->name('resultados.index');
    Route::resource('roles', RoleController::class);
    //Route::view('roles', 'livewire.roles.index')->name('roles.index');
    Route::resource('subprocesos', SubprocesoController::class);
    Route::get('subprocesos/{subproceso}/caracterizacion', [SubprocesoController::class, 'caracterizacion'])->name('subprocesos.caracterizacion');
    Route::resource('suministrainfos', SuministrainfoController::class);
    Route::resource('terminos', TerminoController::class);
    // Route::view('terminos', 'livewire.terminos.index');
    Route::resource('trabajadores', TrabajadorController::class);

    Route::resource('versiones', VersionController::class);
    Route::view('versiones', 'livewire.versions.index');
    Route::view('vinculaciones', 'livewire.vinculacions.index')->name('vinculacions.index');

    Route::resource('macroprocesos', MacroprocesoController::class);

    Route::resource('actividads', ActividadComponent::class);
    //Route::resource('anios', AnioComponent::class);
    Route::resource('anios', AnioController::class);
    //Route::resource('areas', AreaComponent::class);
    Route::resource('areas', AreaController::class);
    // Route::resource('cargos', CargoComponent::class);
    Route::resource('cargos', CargoController::class);
    Route::resource('detalles', DetalleComponent::class);
    // Route::resource('documentos', DocumentoComponent::class);
    // Route::resource('trabajadores', TrabajadorComponent::class);
    Route::resource('trabajadores', TrabajadorController::class);
    // Route::resource('documentosDetalles', ShowDocumentodetalles::class);
    Route::resource('escalas', EscalaComponent::class);
    Route::resource('evaluaciones', EvaluacionComponent::class);
    //Route::resource('evaluaciones', EvaluacionComponent::class);
    // Route::resource('resultados', ResultadoComponent::class);
    Route::resource('suministros', SuministroComponent::class);

    Route::get('mfichacaracterizacion', [MacroprocesoController::class, 'fichaCaracterizacion'])->name('ficha-caracterizacion');
    //la nínea siguiente no sirve
    Route::get('fichacaracterizacion', [MacroprocesoController::class, 'fichaCaracterizacionProcesos'])->name('ficha-caracterizacion-procesos');
    Route::get('fichaindicador/{id}', [MacroprocesoController::class, 'fichaIndicador'])->name('ficha.indicador');

    Route::get('dashboard', [HomeController::class, 'dashboard'])->name('dashboard.index');
    Route::get('caracterizacion', [HomeController::class, 'caracterizacion'])->name('caracterizacion.index');
    Route::get('manuales/proceso', [HomeController::class, 'mProceso'])->name('manuales.proceso');

    Route::get('manuales/macroproceso/{macroproceso}', [HomeController::class, 'mMacroProcesoVer'])->name('manuales.macroproceso.view');
    Route::get('manuales/proceso/{proceso}', [HomeController::class, 'mProcesoVer'])->name('manuales.proceso.view');

    Route::get('manuales/software', [HomeController::class, 'mSoftware'])->name('manuales.software');
    Route::get('manuales/software/{id}', [HomeController::class, 'mSoftwareVer'])->name('manuales.software.view');
});

// locale Route
Route::get('lang/{locale}', [LanguageController::class, 'swap']);

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

Route::get('create_policies', [HomeController::class, 'create_policies']);


/** Reportes y descargas en excel */

Route::get('reportes/macroprocesos/{macroproceso}', [ExportsController::class, 'porMacroproceso'])->name('reportes.macroprocesos.excel');
Route::get('reportes/indicadores/{indicador}', [ExportsController::class, 'porIndicador'])->name('reportes.indicadores.excel');
Route::get('reportes/indicadores/{indicador}/prevista', [ExportsController::class, 'previstaPorIndicador']);
Route::get('reportes/caracterizacion/{macroproceso}', [ExportsController::class, 'porCaracterizacion'])->name('reportes.caracterizacion.excel');
Route::get('reportes/caracterizacion/macroproceso/{macroproceso}', [ExportsController::class, 'porCaracterizacionMacroproceso'])->name('reportes.caracterizacion.macroproceso.excel');
Route::get('reportes/caracterizacion/proceso/{proceso}', [ExportsController::class, 'porCaracterizacionProceso'])->name('reportes.caracterizacion.proceso.excel');
Route::get('reportes/caracterizacion/{macroproceso}/prevista', [ExportsController::class, 'previstaPorCaracterizacion']);

Route::get('reportes/por_procesos/excel', [ExportsController::class, 'porProcesos']);
Route::get('reportes/por_tipo/excel', [ExportsController::class, 'porTipo']);
Route::get('reportes/por_organicas/excel', [ExportsController::class, 'porOrganicas']);
Route::get('reportes/por_indicadores/excel', [ExportsController::class, 'porIndicadoresGeneral']);


Route::get('descargarMacroproceso/{macroproceso}', function () {
    //return Excel::download(new generarCaracterizacion(15001, 6), 'MacroprocesoP06.xlsx');
});


Route::get('reportes/trazabilidad', [ReportesController::class, 'trazabilidad']);
Route::get('reportes/trazabilidad/{id}', [ReportesController::class, 'descargarArchivo']);

Route::get('reportes/por_procesos', [ReportesController::class, 'por_procesos']);
Route::get('reportes', [ReportesController::class, 'index']);
Route::get('reportes/por_tipo', [ReportesController::class, 'por_tipo']);
Route::get('reportes/por_organicas', [ReportesController::class, 'por_organicas']);
Route::get('reportes/por_indicadores', [ReportesController::class, 'indicadores']);
