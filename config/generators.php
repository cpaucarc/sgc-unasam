<?php

$base_path = 'app/';
//$base_path = 'addons/seguimiento_egresados/';
$base_model_namespace = '';
//$base_model_namespace = '';
$base_namespace = '';
//$base_namespace = '';
$prefix_namespace = '';
//$prefix_namespace = 'SeguimientoEgresados';

return [

    /*
    |--------------------------------------------------------------------------
    | The singular resource words that will not be pluralized
    | For Example: $ php artisan generate:resource admin.bar
    | The url will be /admin/bars and not /admins/bars
    |--------------------------------------------------------------------------
    */

    'reserve_words' => ['app', 'website', 'admin'],

    /*
    |--------------------------------------------------------------------------
    | The default keys and values for the settings of each type to be generated
    |--------------------------------------------------------------------------
    */

    'defaults' => [
        'namespace'           => '',
        'path'                => './app/',
        'dependence'          => '',
        'prefix'              => '',
        'postfix'             => '',
        'file_type'           => '.php',
        'dump_autoload'       => false,
        'directory_format'    => '',
        'directory_namespace' => false,
    ],

    /*
    |--------------------------------------------------------------------------
    | Types of files that can be generated
    |--------------------------------------------------------------------------
    */

    'settings' => [
        'view'         => [
            'path'                => "./resources/views/{$base_path}",
           // 'file_type'           => '.vue',
            'file_type'           => '.blade.php',
            'directory_format'    => 'strtolower',
            'directory_namespace' => true
        ],
        'router' => [
            'path' => './resources/assets/components/app/',
            'file_type' => '.js',
            'directory_format' => 'strtolower',
            'directory_namespace' => true
        ],
        'model'        => ['namespace' => "{$prefix_namespace}\Models".$base_namespace, 'path' => './'.$base_path.'Models/'.$base_model_namespace],
        'controller'   => [
            'namespace'           => $prefix_namespace.'\Http\Controllers\App',
            'path'                => './'.$base_path.'/Http/Controllers/App/',
            'postfix'             => 'Controller',
            'directory_namespace' => true,
            'dump_autoload'       => false,
            'repository_contract' => true,
        ],
        'web_controller'   => [
            'namespace'           => $prefix_namespace.'\Http\Controllers\Web',
            'path'                => './'.$base_path.'/Http/Controllers/Web/',
            'postfix'             => 'Controller',
            'directory_namespace' => true,
            'dump_autoload'       => false,
            'repository_contract' => true,
        ],
        'request'     => [
            'directory_namespace' => true,
            'namespace'           => $prefix_namespace.'\Http\Requests'.$base_namespace,
            'postfix'             => 'Request',
            'path'                => './'.$base_path.'/Http/Requests/'.$base_model_namespace,
        ],
        'policy' => [
            'directory_namespace' => true,
            'namespace' => $prefix_namespace.'\Policies'.$base_namespace,
            'postfix' => 'Policy',
            'path' => base_path() . './'.$base_path.'/Policies/'.$base_model_namespace,
        ],
        'seed'         => ['path' => './database/seeds/', 'postfix' => 'TableSeeder'],
        'migration'    => ['path' => './database/migrations/'],
        'notification' => [
            'directory_namespace' => true,
            'namespace'           => '\Notifications',
            'path'                => './app/Notifications/'
        ],
        'event'        => [
            'directory_namespace' => true,
            'namespace'           => '\Events',
            'path'                => './app/Events/'
        ],
        'listener'     => [
            'directory_namespace' => true,
            'namespace'           => '\Listeners',
            'path'                => './app/Listeners/'
        ],
        'trait'        => [
            'directory_namespace' => true,
        ],
        'job'          => [
            'directory_namespace' => true,
            'namespace'           => '\Jobs',
            'path'                => './app/Jobs/'
        ],
        'console'      => [
            'directory_namespace' => true,
            'namespace'           => '\Console\Commands',
            'path'                => './app/Console/Commands/'
        ],
        'middleware'   => [
            'directory_namespace' => true,
            'namespace'           => '\Http\Middleware',
            'path'                => './app/Http/Middleware/'
        ],
        'repository'   => [
            'directory_namespace' => true,
            'postfix'             => 'Repository',
            'namespace'           => '\Repositories',
            'path'                => './app/Repositories/'
        ],
        'contract'     => [
            'directory_namespace' => true,
            'namespace'           => '\Contracts',
            'postfix'             => 'Repository',
            'path'                => './app/Contracts/',
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Resource Views [stub_key | name of the file]
    |--------------------------------------------------------------------------
    */

    'resource_views' => [
        'view_index'       => 'index',
        'view_create'      => 'create',
        'view_edit'        => 'edit',
        'view_show'        => 'show',
      //  'view_create_edit' => 'create_edit',
        //'vue_routes'         => 'routes',

    //    'vue_main'           => 'main',
     //   'vue_index'          => 'index',
     //   'vue_create'         => 'create',
      //  'vue_edit'           => 'edit',
       // 'vue_show'           => 'show'
    ],

    'resource_router' => [
        // 'view_index'       => 'index',
        //'view_create'      => 'create',
        //'view_edit'        => 'edit',
        //  'view_show'        => 'show',
        //  'view_create_edit' => 'create_edit',
      //  'vue_routes' => 'routes'
    ],

    /*
    |--------------------------------------------------------------------------
    | Where the stubs for the generators are stored
    |--------------------------------------------------------------------------
    */

    'example_stub'                => base_path() . '/resources/stubs/example.stub',
    'model_stub'                  => base_path() . '/resources/stubs/model.stub',
    'model_plain_stub'            => base_path() . '/resources/stubs/model.plain.stub',
    'migration_stub'              => base_path() . '/resources/stubs/migration.stub',
    'migration_plain_stub'        => base_path() . '/resources/stubs/migration.plain.stub',
    'controller_stub'             => base_path() . '/resources/stubs/controller.stub',
    'web_controller_stub'         => base_path() . '/resources/stubs/web_controller.stub',
    'controller_plain_stub'       => base_path() . '/resources/stubs/controller.plain.stub',
    'controller_admin_stub'       => base_path() . '/resources/stubs/controller_admin.stub',
    'controller_repository_stub'  => base_path() . '/resources/stubs/controller_repository.stub',
    'pivot_stub'                  => base_path() . '/resources/stubs/pivot.stub',
    'seed_stub'                   => base_path() . '/resources/stubs/seed.stub',
    'seed_plain_stub'             => base_path() . '/resources/stubs/seed.plain.stub',
    'view_stub'                   => base_path() . '/resources/stubs/blade_index.stub',
    'view_index_stub'             => base_path() . '/resources/stubs/blade_index.stub',
    'view_show_stub'              => base_path() . '/resources/stubs/blade_show.stub',
    'view_create_stub'            => base_path() . '/resources/stubs/blade_create.stub',
    'view_edit_stub'              => base_path() . '/resources/stubs/blade_edit.stub',
    'view_create_edit_stub'       => base_path() . '/resources/stubs/view.create_edit.stub',
    'schema_create_stub'          => base_path() . '/resources/stubs/schema-create.stub',
    'schema_change_stub'          => base_path() . '/resources/stubs/schema-change.stub',
    'notification_stub'           => base_path() . '/resources/stubs/notification.stub',
    'event_stub'                  => base_path() . '/resources/stubs/event.stub',
    'listener_stub'               => base_path() . '/resources/stubs/listener.stub',
    'many_many_relationship_stub' => base_path() . '/resources/stubs/many_many_relationship.stub',
    'trait_stub'                  => base_path() . '/resources/stubs/trait.stub',
    'job_stub'                    => base_path() . '/resources/stubs/job.stub',
    'console_stub'                => base_path() . '/resources/stubs/console.stub',
    'middleware_stub'             => base_path() . '/resources/stubs/middleware.stub',
    'repository_stub'             => base_path() . '/resources/stubs/repository.stub',
    'contract_stub'               => base_path() . '/resources/stubs/contract.stub',
    'request_stub'                => base_path() . '/resources/stubs/request.stub',
    'policy_stub'                 => base_path() . '/resources/stubs/policy.stub',
    'vue_routes_stub'             => base_path() . '/resources/stubs/vue_routes.stub',
    'vue_main_stub'               => base_path() . '/resources/stubs/vue_main.stub',
    'vue_index_stub'              => base_path() . '/resources/stubs/vue_index.stub',
    'vue_create_stub'             => base_path() . '/resources/stubs/vue_create.stub',
    'vue_edit_stub'               => base_path() . '/resources/stubs/vue_edit.stub',
    'vue_show_stub'               => base_path() . '/resources/stubs/vue_show.stub'
];
