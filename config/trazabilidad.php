<?php

return [
    'matriz_icacit_pdf' => 1,
    'matriz_icacit_excel' => 2,
    'matriz_iso_pdf' => 3,
    'matriz_iso_excel' => 4,
    'matriz_licenciamiento_pdf' => 5,
    'matriz_licenciamiento_excel' => 6,
    'matriz_acreditacion_pdf' => 7,
    'matriz_acreditacion_excel' => 8,
    'matriz_sineace_pdf' => 9,
    'matriz_sineace_excel' => 10,
    
];
